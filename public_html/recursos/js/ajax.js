
function objXMLHttp(){

    var xmlhttp;
	
    if(typeof  XMLHttpRequest == 'undefined'){
	
        try { 
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); 
        } catch (e) { 
            try { 
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); 
            } catch (E) { 
                xmlhttp = false; 
            } 
        } 	    
    }

    if  (!xmlhttp && typeof  XMLHttpRequest != 'undefined' ) { 
        try  { 
            xmlhttp = new  XMLHttpRequest(); 
        } catch  (e) { 
            xmlhttp = false ; 
        } 
    }

    return xmlhttp;
	  
}

function limparDivContainer(idElemento){
	
    var elemento = null;
	
    if(idElemento.id){
	
        elemento = document.getElementById(idElemento);
	
    }
    else{
	
        elemento = idElemento;
			
    }

    var condicao = true;
	
    while(condicao){

        if(elemento.parentNode == null)
            break;
		
        if(elemento.parentNode.tagName == "DIV"){
		
            elemento.parentNode.parentNode.removeChild(elemento.parentNode);
            condicao = false;
			
        }
        else{
			
            elemento = elemento.parentNode;
			
        }

    }

}

function carregarComboBoxSQL(compPai, compFilho, labelPai, query, valorSelecionado, variaveisQuery, niveisRaiz){

    var ajax = objXMLHttp();

    var strNiveis = strNiveisRaiz(niveisRaiz);
	
    if(ajax){
	
        ajax.open("GET", strNiveis + "recursos/php/ajaxComboBoxSQL.php?query=" + query + "&valor=" + variaveisQuery, true);
		
        ajax.onreadystatechange = function()
        {
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {

                $("#" + compFilho).addClass("carregamento_combobox");
				
            }
            if(ajax.readyState == 4)
            {
				
                $("#" + compFilho).removeClass("carregamento_combobox");
				
                var dataArray  = ajax.responseXML.getElementsByTagName("valor");

                document.getElementById(compFilho).innerHTML = "";
                document.getElementById(compFilho).options.length = 0;
				
                if(dataArray.length > 0) {
				   
                    var novo = document.createElement("option");
                    novo.text = "Selecione uma op��o";
                    novo.value = "";
                    document.getElementById(compFilho).options.add(novo);

                    for(var i = 0 ; i <dataArray.length ; i++) {
				   
                        var item = dataArray[i];
				      
                        //cont�udo dos campos no arquivo XML
                        var codigo    =  item.getElementsByTagName("codigo")[0].firstChild.nodeValue;
                        var descricao =  item.getElementsByTagName("descricao")[0].firstChild.nodeValue;
                        var cssClass =  item.getElementsByTagName("css")[0].firstChild.nodeValue;

                        var novo = document.createElement("option");
				      
                        //atribui um valor
                        novo.value = codigo;
                        //atribui um texto
                        novo.text  = descricao;
			          
                        novo.className = cssClass;
			      
                        if(codigo == valorSelecionado)
                            novo.selected = "selected";
			          
                        document.getElementById(compFilho).options.add(novo);

                    }
				   
                    //finalmente adiciona o novo elemento
		          
                    document.getElementById(compPai).className = "input_text";
                    document.getElementById(compFilho).className = "focus_text";
                    document.getElementById(compFilho).focus();
				   
                }
                else {
				

                    var mensagem = document.createElement("option");
										
                    //caso o XML volte vazio, imprime a mensagem abaixo
                    mensagem.text = "Selecione primeiro um(a) " + labelPai;
                    mensagem.value = "";
                    document.getElementById(compFilho).options.add(mensagem);
				  
                }  
				
            }
        };
    }
	
    ajax.send(null);
	
}

function carregarComboBox(valor, id1, id2, niveisRaiz){

    var ajax = objXMLHttp();
	
    var idOptions = "options_" + id2;
    var compOptions = document.createElement("option");
	
    var strNiveis = strNiveisRaiz(niveisRaiz);
	
    if(ajax){
	
        ajax.open("GET", strNiveis + "recursos/php/ajaxComboBox.php?valor=" + valor + "&id1=" + id1 + "&id2=" + id2, true);
		
        ajax.onreadystatechange = function()
        {
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
			
                document.getElementById(id).innerHTML = "<div id='img_carregando'></div>";
				
            }
            if(ajax.readyState == 4)
            {
				
                var dataArray   = ajax.responseXML.getElementsByTagName("valor");
  
                document.getElementById(id2).options.length = 0;
				
                if(dataArray.length > 0) {

                    compOptions.innerHTML = "Selecione uma op��o";
                    document.getElementById(id2).options.add(compOptions);
				   					
                    for(var i = 0 ; i <dataArray.length ; i++) {
				   
                        var item = dataArray[i];
				      
                        //cont�udo dos campos no arquivo XML
                        var codigo    =  item.getElementsByTagName("codigo")[0].firstChild.nodeValue;
                        var descricao =  item.getElementsByTagName("descricao")[0].firstChild.nodeValue;
                        var cssClass =  item.getElementsByTagName("css")[0].firstChild.nodeValue;
				      
                        //cria um novo option dinamicamente  
                        var novo = document.createElement("option");
                        //atribui um valor
                        novo.value = codigo;
                        //atribui um texto
                        novo.text  = descricao;
                        //atribui uma classe css
                        novo.className = cssClass;
                        //finalmente adiciona o novo elemento
                        document.getElementById(id2).options.add(novo);

                    }
				   
                }
                else{
				
                    //caso o XML volte vazio, printa a mensagem abaixo
                    compOptions.innerHTML = "Selecione primeiro o projeto da etapa";
                    compOptions.value = "";
                    document.getElementById(id2).options.add(compOptions);
				  
                }  
				
            }
        }
    }
	
    ajax.send(null);
	
}


function carregarValorRemotoAsyncPost(endereco, seletorJquery, funcaoASerChamada, pData) {
    //data: { name: "John", location: "Boston" }
    $.ajax({
        url: endereco,
        type: "POST",
        data: pData,
        dataType: 'json',
        success: function (data) {
            if (data != null
                    && data.length
                    && seletorJquery != null
                    && seletorJquery.length > 0) {
                $(seletorJquery).html(data);
            }
            return data;
        }
    }).done(function () {
        var arg = arguments[0];
        funcaoASerChamada(arg);
    });
}

function carregarValorRemoto(classe, funcaoComParametros, niveisRaiz, funcaoPosExecucao){
	
    var ajax = objXMLHttp();

    var strNiveis = niveisRaiz;
    var retorno = "";
    if(ajax){
	
        ajax.open("GET", strNiveis + "adm/ajax.php?class=" + classe + "&funcao=" + funcaoComParametros, false);
		
        ajax.onreadystatechange = function()
        {
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
			
            //document.getElementById(id).innerHTML = "<div id='img_carregando'></div>";
				
            }
            if(ajax.readyState == 4)
            {
				
                if(funcaoPosExecucao != null && funcaoPosExecucao.length){
					
                    funcaoPosExecucao += "(" + ajax.responseText + ")";
                    eval(funcaoPosExecucao);
					 
                }
					
                retorno = ajax.responseText;

            }
        };
    }
	
    ajax.send(null);
    return retorno; 

}

function carregarCampoAjax(pagina, strFields, valorMestre, idCampo, niveisRaiz){
	
    var ajax = objXMLHttp();

    var strNiveis = strNiveisRaiz(niveisRaiz);
	
    if(ajax){
	
        ajax.open("GET", strNiveis + "adm/ajax.php?tipo=ajax_request&page=" + pagina + "&" + strFields + valorMestre, true);
		
        ajax.onreadystatechange = function()
        {
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
			
            //document.getElementById(id).innerHTML = "<div id='img_carregando'></div>";
				
            }
            if(ajax.readyState == 4)
            {

                document.getElementById(idCampo).value  = ajax.responseText;
                eval(funcaoPosExecucao);
  				
            }
        };
    }
	
    ajax.send(null);

}

function carregarListAjax(tipo, pagina, idDivAjax, funcaoStrFields, funcaoPosExecucao, niveisRaiz){
	
    if(tipo == "")
        tipo = "ajax_forms";

    var ajax = objXMLHttp();
    var strNiveis = niveisRaiz;
    
    var strNiveis = strNiveisRaiz(niveisRaiz);
    

        
    if(typeof funcaoStrFields == 'function'){
		
        strFields = funcaoStrFields();
		
    }
    else if(funcaoStrFields.indexOf("&") != -1){
            
        strFields = funcaoStrFields;
            
    }
    else if(funcaoStrFields != ""){
		
        strFields = eval(funcaoStrFields);
	
    }
    else{
	
        strFields = "";
	
    }
		
    if(ajax){
	
        ajax.open("GET", strNiveis + "adm/ajax.php?tipo=" + tipo + "&page=" + pagina + "&" + strFields, true);
		
        ajax.onreadystatechange = function()
        {
			
            var elementoContainer = document.getElementById(idDivAjax);
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
				
                elementoContainer.innerHTML = "<div style=\"vertical-align: middle;\" width=\"400\" height=\"500\"><div width=\"400\" height=\"500\" class=\"div_carregamento_ajax\">&nbsp;</div></div>";

            }
            if(ajax.readyState == 4 && ajax.status == 200)
            {

                var rt = ajax.responseText; 
                elementoContainer.innerHTML  = rt;

                ajax = null;    
				 
                processarCodigoJavascript(elementoContainer);
				 
                if(typeof funcaoPosExecucao == 'function'){
				 
                    funcaoPosExecucao();
					 
                }
                else if(funcaoPosExecucao != ""){
							
                    eval(funcaoPosExecucao);
					 
                }
				 
            }
			
        };
		
    }
	
    ajax.send(null);

}






function carregarListAjaxSync(tipo, pagina, idDivAjax, funcaoStrFields, funcaoPosExecucao, strNiveis){
	
    if(tipo == "")
        tipo = "ajax_forms";

    var ajax = objXMLHttp();
    
    
    

    if(funcaoStrFields == null)   {
        strFields = "";
    }
    else if(typeof funcaoStrFields == 'function'){
		
        strFields = funcaoStrFields();
		
    }
    else if(funcaoStrFields.indexOf("&") != -1){
            
        strFields = funcaoStrFields;
            
    }
    else if(funcaoStrFields != ""){
		
        strFields = funcaoStrFields;
	
    }
    else{
	
        strFields = "";
	
    }
    
    if(ajax){
	
        ajax.open("GET", strNiveis + "adm/ajax.php?tipo=" + tipo + "&page=" + pagina + "&" + strFields, false);
		
        ajax.onreadystatechange = function()
        {
			
            var elementoContainer = document.getElementById(idDivAjax);
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
				
                elementoContainer.innerHTML = "<div style=\"vertical-align: middle;\" width=\"400\" height=\"500\"><div width=\"400\" height=\"500\" class=\"div_carregamento_ajax\">&nbsp;</div></div>";

            }
            if(ajax.readyState == 4 && ajax.status == 200)
            {

                var rt = ajax.responseText; 
                
                elementoContainer.innerHTML  = rt;

                ajax = null;    
				 
                processarCodigoJavascript(elementoContainer);
				 
                if(typeof funcaoPosExecucao == 'function'){
				 
                    funcaoPosExecucao();
					 
                }
                else if(funcaoPosExecucao != ""){
							
                    eval(funcaoPosExecucao);
					 
                }
				 
            }
			
        };
		
    }
	
    ajax.send(null);

}


function carregarValorRemotoSincrona(classe, funcaoComParametros, niveisRaiz, funcaoPosExecucao){
	
    var ajax = objXMLHttp();

    var strNiveis = niveisRaiz;
    var retorno = "";
    if(ajax){
	
        ajax.open("GET", strNiveis + "adm/ajax.php?class=" + classe + "&funcao=" + funcaoComParametros, false);
		
        ajax.onreadystatechange = function()
        {
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
			
            //document.getElementById(id).innerHTML = "<div id='img_carregando'></div>";
				
            }
            if(ajax.readyState == 4)
            {
				
                if(funcaoPosExecucao != null && funcaoPosExecucao.length){
					
                    funcaoPosExecucao += "(" + ajax.responseText + ")";
                    eval(funcaoPosExecucao);
					 
                }
					
                retorno = ajax.responseText;

            }
        };
    }
	
    ajax.send(null);
    

}


function carregarAjaxTabSync(idDivAjax, funcaoStrFields, funcaoPosExecucao, strNiveis){
	
    
    tipo = "ajax_tab";

    var ajax = objXMLHttp();
    
    
    

    if(funcaoStrFields == null)   {
        strFields = "";
    }
    else if(typeof funcaoStrFields == 'function'){
		
        strFields = funcaoStrFields();
		
    }
    else if(funcaoStrFields.indexOf("&") != -1){
            
        strFields = funcaoStrFields;
            
    }
    else if(funcaoStrFields != ""){
		
        strFields = eval(funcaoStrFields);
	
    }
    else{
	
        strFields = "";
	
    }
    
    if(ajax){
	
        ajax.open("GET", strNiveis + "adm/ajax_tab.php?tipo=" + tipo + "&" + strFields, false);
		
        ajax.onreadystatechange = function()
        {
			
            var elementoContainer = document.getElementById(idDivAjax);
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
				
                elementoContainer.innerHTML = "<div style=\"vertical-align: middle;\" width=\"400\" height=\"500\"><div width=\"400\" height=\"500\" class=\"div_carregamento_ajax\">&nbsp;</div></div>";

            }
            if(ajax.readyState == 4 && ajax.status == 200)
            {

                var rt = ajax.responseText; 
                
                elementoContainer.innerHTML  = rt;

                ajax = null;    
				 
                processarCodigoJavascript(elementoContainer);
				 
                if(typeof funcaoPosExecucao == 'function'){
				 
                    funcaoPosExecucao();
					 
                }
                else if(funcaoPosExecucao != ""){
							
                    eval(funcaoPosExecucao);
					 
                }
				 
            }
			
        };
		
    }
	
    ajax.send(null);

}

function posCarregarListAjax(){

    document.getElementById("ajax_next_action").style.display = "block";
    document.getElementById("ajax_botoes").style.display = "block";
	
    document.getElementById("botao_ok").value = "Recarregar Lista";
    document.getElementById("botao_ok").onclick = function(){
		
        if(confirmarReset('O formul�rio ser� limpado, deseja prosseguir?')) 
            carregarAjaxAtual(false);
		
    };

}

function processarCodigoJavascript(elementoContainer){
	
    var javascripts = elementoContainer.getElementsByTagName("script");

    var i;
    for(i = 0; i < javascripts.length; i++){

        eval(javascripts[i].innerHTML);

    }
	
}

function carregarDivAjaxEmLista(tipo, pagina, seletorJQueryDiv){
	
    var contador = parseInt($(seletorJQueryDiv).attr('contador'));
    var idDivAjax = $(seletorJQueryDiv).attr('id');

    $(seletorJQueryDiv).css('display', 'block');
    $(seletorJQueryDiv).attr({
        'class': 'container_da_lista'
    });

    var funcaoStrFields = function(){

        return "contador=" + contador + "&container=" + seletorJQueryDiv;

    };

    var funcaoPosExecucao = function(){

        var novoContador = contador+1;

        $(seletorJQueryDiv).after("<div style='display: none;' id='" + idDivAjax + "_temp'></div>");
        $(seletorJQueryDiv).attr('id', '');
        $("#" + idDivAjax + "_temp").attr({
            'id': idDivAjax, 
            'contador': novoContador
        });

    };

    carregarListAjax(tipo, pagina, idDivAjax, funcaoStrFields, funcaoPosExecucao, 1);

}

function removerDivAjaxEmLista(botaoDeRemover){
    
    var novoValor = $(botaoDeRemover).parents(".container_da_lista").children("input[type=hidden]").attr('value') + "_remover";
    $(botaoDeRemover).parents(".container_da_lista").children("input[type=hidden]").attr('value', novoValor);
    $(botaoDeRemover).parents(".container_da_lista").children("br").remove();
    $(botaoDeRemover).parents(".container_da_lista").children(".tabela_form").html("");

}

function enviarFormularioPorPOSTAjax(idElementoContainer, idDivRetorno, urlAction){
	
    var ajax = objXMLHttp();
	
    if(ajax){
		
        if(urlAction === undefined){
			
            urlAction = "actions.php";
			
        }
	
        var objFormulario = document.getElementById(idElementoContainer);
        var params = "";
		
        var inputs = objFormulario.getElementsByTagName("input");
        var selects = objFormulario.getElementsByTagName("select");
        var textareas = objFormulario.getElementsByTagName("textarea");
		
        for(var i=0; i < inputs.length; i++){
			
            if(inputs[i].type == "checkbox"){
				
                if(inputs[i].checked == true){
			
                    params += inputs[i].name + "=" + inputs[i].value + "&";
			
                }
				
            }
            else if(inputs[i].type == "radio"){
				
                if(inputs[i].checked == true){
					
                    params += inputs[i].name + "=" + inputs[i].value + "&";
			
                }
				
            }
            else if(inputs[i].type == "hidden" && inputs[i].name.indexOf("_HTML") > -1){
				
                params += inputs[i].name + "=" + FCKeditorAPI.GetInstance(inputs[i].name).GetHTML() + "&";
				
            }
            else{
				
                params += inputs[i].name + "=" + inputs[i].value + "&";
				
            }
			
        }
		
        for(var i=0; i < selects.length; i++){
			
            params += selects[i].name + "=" + selects[i].value + "&";
			
        }
		
        for(var i=0; i < textareas.length; i++){
			
            params += textareas[i].name + "=" + textareas[i].value + "&";
			
        }		
		
        ajax.open("POST", urlAction, true)
		
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=ISO-8859-1;");
        ajax.setRequestHeader("Content-length", params.length);
        ajax.setRequestHeader("Connection", "close");
				
        ajax.onreadystatechange = function()
        {
            //enquanto estiver processando...emite a msg de carregando
            if(ajax.readyState == 1)
            {
				
                document.getElementById(idDivRetorno).innerHTML = "<div style=\"vertical-align: middle;\" width=\"400\" height=\"500\"><div width=\"400\" height=\"500\" class=\"div_carregamento_ajax\">&nbsp;</div></div>";

            }
			
            if(ajax.readyState == 4 && ajax.status == 200)
            {

                var rt = ajax.responseText; 
                document.getElementById(idDivRetorno).innerHTML  = rt;

                ajax = null;    
				 
                var javascripts = document.getElementById(idDivRetorno).getElementsByTagName("script");

                var i;
                for(i = 0; i < javascripts.length; i++){
				
                    eval(javascripts[i].innerHTML);
					 					 
                }
				 
            }
			
        };
		
        ajax.send(params);
		
    }
		
}