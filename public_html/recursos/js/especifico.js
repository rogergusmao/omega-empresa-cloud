
function adicionarEnderecoDeEmailNaLista(enderecoDeEmail){
	
	var valorCorrente = parent.document.getElementById('enderecos_email').value;
	
	if(valorCorrente.indexOf(enderecoDeEmail) == -1){
	
		parent.document.getElementById('enderecos_email').value += enderecoDeEmail + '\n';	
	
	}
	
}

function carregaURL(retJson, indice, raiz){
    retJson = decodeURIComponent(retJson);
    var objJsonPagina = JSON.parse(retJson);
    var objPagina = objJsonPagina.objPaginas[indice];
    carregarListAjaxSync(
        objPagina.tipoPagina,
        objPagina.pagina,
        "tabDiv", 
        objJsonPagina.get,
        "", 
        raiz );
}
function imprimeMensagemEstadoOperacaoSistema(raiz, div, idOSM){
    var msg = carregarValorRemoto(
        "EXTDAO_Operacao_sistema_mobile", 
        "imprimirMensagemStatus('" + idOSM +  "')", 
        raiz, 
        null);
    var containerHTML = document.getElementById(div);
    containerHTML.innerHTML = msg;

}

function onChangeVersaoParaReinstalar(){
    var dropBox = document.getElementById("reinstalar_a_versao_id_INT");
    var indice =  dropBox.selectedIndex;
    var link = $("#link_download_apk");
    if(indice != 0){
        var idSPVSPM =  dropBox.options[indice].value;
        link.attr("href", "actions.php?class=Servico_mobile&action=downloadApkAndroid&sistema_projetos_versao_sistema_produto_mobile=" + idSPVSPM);
        
        link.css({visibility: 'visible'});
    } else {
        link.css({visibility: 'hidden'});
    }
        
    
}

function imprimeMensagemEstadoReinstalaVersao(raiz, div, idMI){
    var msg = carregarValorRemoto(
        "EXTDAO_Mobile_identificadorUsuario", 
        "imprimirMensagemStatusReinstalacao('" + idMI +  "')", 
        raiz, 
        null);
    var containerHTML = document.getElementById(div);
    containerHTML.innerHTML = msg;

}


    
function abrirNovaJanela(tipoPagina, pagina, pGet){
    window.open("popup.php?tipo=" + tipoPagina + "&page=" + pagina + "&" + pGet);
} 

function windowOpen(url){
    window.open(url);
} 

function getValorSelecionadoEstruturaEmComboBoxAjax(){
	
	var numeroIteracoes = NUMERO_NIVEIS_ESTRUTURA_ORGANIZACIONAL;
	var valorRetorno = false;

	for(i=numeroIteracoes; i >= 1; i--){

		if(document.getElementById("comboNivel" + i) != null && document.getElementById("comboNivel" + i).value.length > 0){
			
			valorRetorno = document.getElementById("comboNivel" + i).value;
			break;
			
		}
		
	}
	
	return valorRetorno;
	
}


//INICIO: PAGINA VISUALIZAR HIERARQUIA (SUBNIVEIS DA HIERARQUIA)
function carregarCadastroSubniveis(id_nivel, id_div){
	
	carregarListAjax('ajax_forms', 'subniveis_hierarquia', id_div, "gerarStringGETSubniveis(" + id_nivel + ")", "alterarValoresCadastroSubniveis('" + id_div + "', " + id_nivel + ")", 1);
    		
}

function gerarStringGETSubniveis(id_nivel, id_div){
	
	strRetorno = "id_nivel=" + id_nivel;
	strRetorno += "&ja_carregadas=" + document.getElementById("ja_carregadas_" + id_nivel).value;
	
	return strRetorno;
	
}

function alterarValoresCadastroSubniveis(id_div, id_nivel){
	
	//document.getElementById("div_adicionar_subniveis_" + id_nivel).id = "div_passsada";
	document.getElementById("ja_carregadas_" + id_nivel).value =  parseInt(document.getElementById("ja_carregadas_" + id_nivel).value) + 1;
	document.getElementById("ajax_carregado_" + id_div).value = "1";	
	
}
//FIM: PAGINA VISUALIZAR HIERARQUIA (SUBNIVEIS DA HIERARQUIA)


//INICIO: PAGINA DE CONFIRMACAO DA EXCLUSAO DE NIVEL DA ESTRUTURA ORGANIZACIONAL
function carregarRemoverNivel(id_nivel, id_div){
	
	carregarListAjax('forms', 'nodo_nivel_estrutura_remover', id_div, "gerarStringGETRemoverNivel(" + id_nivel + ")", "alterarValoresRemoverNivel('" + id_div + "')", 1);
    		
}

function gerarStringGETRemoverNivel(id_nivel){
	
	strRetorno = "id_nivel=" + id_nivel;
	
	return strRetorno;
	
}

function alterarValoresRemoverNivel(id_div){
	
	document.getElementById("ajax_carregado_" + id_div).value = "1";
		
}
//FIM: PAGINA DE CONFIRMACAO DA EXCLUSAO DE NIVEL DA ESTRUTURA ORGANIZACIONAL


//INICIO: PAGINA GERENCIAMENTO DE VAGAS (SUBNIVEIS DA HIERARQUIA)
function carregarGerenciamentoVagas(id_nivel, id_div){
	
	carregarListAjax('forms', 'vaga', id_div, "gerarStringGETVagas(" + id_nivel + ")", "alterarValoresCadastroVagas('" + id_div + "')", 1);
		
}

function gerarStringGETVagas(id_nivel){
	
	strRetorno = "id_nivel=" + id_nivel;
	
	return strRetorno;
	
}

function alterarValoresCadastroVagas(id_div){
	
	document.getElementById("ajax_carregado_" + id_div).value = "1";

}

//FIM: PAGINA GERENCIAMENTO DE VAGAS (SUBNIVEIS DA HIERARQUIA)



//INICIO: PAGINA GERENCIAMENTO DE VAGAS (SUBNIVEIS DA HIERARQUIA)
function carregarEdicaoNiveis(id_nivel, id_div){
	
	carregarListAjax('forms', 'nodo_nivel_estrutura', id_div, "gerarStringGETNiveis(" + id_nivel + ")", "alterarValoresEdicaoNiveis('" + id_div + "')", 1);
		
}

function gerarStringGETNiveis(id_nivel){
	
	strRetorno = "id1=" + id_nivel;
	
	return strRetorno;
	
}

function alterarValoresEdicaoNiveis(id_div){
	
	document.getElementById("ajax_carregado_" + id_div).value = "1";

}

//FIM: PAGINA GERENCIAMENTO DE VAGAS (SUBNIVEIS DA HIERARQUIA)



//INICIO: LISTA DE NIVEIS DO FILTRO ESTILO EXCEL
function carregarFiltroDeSelecaoMultipla(nivel_vertical, id_div){
	
	carregarListAjax('addons', 'filtro_niveis_logica', id_div, "gerarStringGETFiltroDeSelecaoMultipla(" + nivel_vertical + ")", "alterarValoresFiltroDeSelecaoMultipla('" + id_div + "')", 1);
    		
}

function gerarStringGETFiltroDeSelecaoMultipla(nivel_vertical){
	
	var nivel_busca = nivel_vertical -1;
	
	var strGet = "";
	
	for(i=0; i < 100; i++){
		
		if(document.getElementById("checkbox_" + nivel_busca + "_" + i)){
			
			if(document.getElementById("checkbox_" + nivel_busca + "_" + i).checked){

				strGet += "niveis[]=" + document.getElementById("checkbox_" + nivel_busca + "_" + i).value + "&";
				
			}
			
		}
		else{
			
			break;
			
		}
		
	}
		
	var strPesquisa = "";
	
	if(document.getElementById("somente_areas_envolvidas")){
		
		strPesquisa = "&somente_areas_envolvidas=true";
		
	}
	
	strRetorno = strGet + "ajax_request=true&nivel_vertical=" + nivel_vertical + strPesquisa;
	
	return strRetorno;
	
}

function alterarValoresFiltroDeSelecaoMultipla(id_div){
	
	//document.getElementById("ajax_carregado_" + id_div).value = "1";
		
}
//FIM: PAGINA DE CONFIRMACAO DA EXCLUSAO DE NIVEL DA ESTRUTURA ORGANIZACIONAL


function criarCaminhoOrganograma(nivelOrigem, celulaOrigem, celulaDestino, numeroLinhasVerticais){
	
	var prefixoTd = "nivel_" + nivelOrigem + "_";
		
	var i;
	for(i=0; i < numeroLinhasVerticais - (celulaOrigem); i++){
		
		document.getElementById(prefixoTd + i + "_" + ((celulaOrigem-1)*2)).className = "td_nivel_ligacao_vertical";
		
	}
	
	var linhaVertical =  numeroLinhasVerticais - (celulaOrigem) - 1;
	
	for(i=(celulaOrigem*2)-1; i < ((celulaDestino)*2)-1; i++){
		
		document.getElementById(prefixoTd + linhaVertical + "_" + (i)).className = "td_nivel_ligacao_horizontal";
		
	}
	
	var coluna = (celulaDestino-1)*2;
	var linhaOrigem = numeroLinhasVerticais - (celulaOrigem);
	
	for(i=linhaOrigem; i < numeroLinhasVerticais; i++){
		
		document.getElementById(prefixoTd + i + "_" + (coluna)).className = "td_nivel_ligacao_vertical";
		
	}
	
}

function carregarPadraoNivelSimilaridade(id_nivel, id_div){
	
	if(!isNaN(id_nivel)){

        carregarListAjax('views', 'padrao_nivel_similaridade', id_div, "montarStrFieldsPadraoNivelSimilaridade(" + id_nivel + ")", "mostrarDivPadraoNivelSimilaridade('" + id_div + "')", 1);
        
    }
    else if(!load){
    
    	//limpar div
        document.getElementById(id_div).innerHTML = "";            
    
    }

}

function montarStrFieldsPadraoNivelSimilaridade(id_nivel){

    var strRetorno = "";

	strRetorno += "id_nivel=" + id_nivel;

    return strRetorno;

}

function mostrarDivPadraoNivelSimilaridade(id_div){
	
	document.getElementById(id_div).style.width = document.body.scrollWidth * 0.8;
	document.getElementById(id_div).style.display = "block";
	
	if (document.addEventListener){

		document.addEventListener("scroll", function(){
			
			document.getElementById(id_div).style.top =  document.body.scrollTop + 15;
			
		}, true);
		
	}
	else if(document.attachEvent){
		
		var result = window.attachEvent("onscroll",  function(){
			
			document.getElementById(id_div).style.top =  document.body.scrollTop + 15;
			
		});
		
	}
	
	document.getElementById(id_div).style.top =  document.body.scrollTop + 15;

}

function limparExperienciaPratica(id_experiencia, mensagem){
	
	if(confirm(mensagem)){
	    
		//limpa os campos
		document.getElementById("empresa" + id_experiencia).value = "";
		document.getElementById("funcao_na_empresa" + id_experiencia).value = "";
		document.getElementById("data_inicio_funcao_DATE" + id_experiencia).value = "";
		document.getElementById("data_termino_funcao_DATE" + id_experiencia).value = "";
		
		for(var i=0; document.getElementById("nivel_similaridade_id_INT" + id_experiencia + "_" + i ) != null; i++){
		
			document.getElementById("nivel_similaridade_id_INT" + id_experiencia + "_" + i ).checked = false;
						
		}
		
		var fckEditor = FCKeditorAPI.GetInstance("descricao_funcao_HTML" + id_experiencia); 
		fckEditor.EditorDocument.body.innerHTML = "";

        
    }
	else{
		
		return false;
		
	}

}

function filtroSelecaoAreasParticipantes(componente){

	var estado = true;
	
	if(componente.checked == true)
		estado = true;
	else
		estado = false;
	
	var id_nivel = componente.value;	
		
	var a = document.getElementsByTagName('input') ;
	var string = "_" + id_nivel + "_";
	
	if(estado == true){
	
		var pedacos = componente.id.split("_");
				
		var numeroPais = pedacos.length - 2;
		
		var idComponenteAtual = componente.id;
		var componenteAtual = componente;
		
		for(var i=0; i < numeroPais; i++){
			
			idComponenteAtual = idComponenteAtual.replace("_" + componenteAtual.value, "");
			
			if(document.getElementById(idComponenteAtual)){
				
				componenteAtual = document.getElementById(idComponenteAtual);
				componenteAtual.checked = estado;
				
			}
						
		}
		
	}
	
	for(var i=0; i < a.length; i++){

		 if(a[i].id.indexOf(string) != -1 && a[i].id != componente.id){ 

			 a[i].checked = estado;

		  }

	} 

}

function desmarcarClassificacaoCurriculo(idExperiencia){
	
	var i;
	for(i=0; i < 4; i++){
	
		document.getElementById("nivel_similaridade_id_INT" + idExperiencia + "_" + i).checked=false;
	
	}
	
	return true;

}

function pegarValorEUnidadeDeTamanho(valorComUnidade){
	
	var valor;
	var unidade;
	
	var arrayUnidades = new Array('px', 'pt', 'cm', 'in');
	
	for(var i=0; i < arrayUnidades.length; i++){
	
		if(valorComUnidade.indexOf(arrayUnidades[i]) != -1){
			
			unidade = arrayUnidades[i];
			valor = valor.trim(valorComUnidade.replace(unidade, ""));
			break;
			
		}
	
	}

	return new Array(valor, unidade);
	
}

function removerElementos(nomeDoElemento){
	
	var arrElementos = document.getElementsByName(nomeDoElemento);
	
	var i;
	for(i=0; arrElementos.length > 0; i++){
		
		arrElementos[0].parentNode.removeChild(arrElementos[0]);
		
	}

}

function setPositionElemento(elemento, position){
	
	elemento.style.position = position;
	
}

function setPosicionamentoAbsolutoMantendoPosicao(elemento, manterExatamenteNaPosicao){

	var jQueryElem = $(elemento);
	
	if(jQueryElem.css('position') != "absolute"){
		
		var larguraOriginal = jQueryElem.width();
		var alturaOriginal = jQueryElem.height();
		
		var correcaoAltura = 0;
		var correcaoEsquerda = 5;
				
		if(elemento.tagName == "TD"){

			var emBranco = document.createElement("TD");
			emBranco.innerHTML = "&nbsp;";
			
			emBranco.style.width = larguraPai;
			emBranco.style.height = alturaPai;
			
			elemento.parentNode.insertBefore(emBranco, elemento);
			correcaoAltura = -15;
			
		}
		else{
			
			jQueryElem.parent().css({
				
				width: larguraOriginal,
				height: alturaOriginal
				
			});
			
		}
		
		if(manterExatamenteNaPosicao === true){
			
			correcaoAltura = 0;
			correcaoEsquerda = 0;
			
		}
		
		jQueryElem.addClass('celula_pos_absoluto');
		
		jQueryElem.css({
			
			position: 'absolute',
			top: jQueryElem.offset().top,
			left: jQueryElem.offset().left + correcaoEsquerda,
			//height: alturaOriginal + correcaoAltura,
			width: larguraOriginal
						
		});
		
	}
		
}

function ligarElementos(elemento1, elemento2, tudoComPosAbsoluto){

	var gapY = 2;

	var coordenadas1, coordenadas2;

	coordenadas1 = getPosicaoCentralDoElemento(elemento1, true, false);
	coordenadas2 = getPosicaoCentralDoElemento(elemento2, false, true);

	var coordenadas1Conferencia = getPosicaoCentralDoElemento(elemento1, false, false);
	var coordenadas2Conferencia = getPosicaoCentralDoElemento(elemento2, false, true);
	
	var coordenadasTopLeftOriginais1 = getPosicaoCentralDoElemento(elemento1, true, true);
	var coordenadasTopLeftOriginais2 = getPosicaoCentralDoElemento(elemento2, true, true);
	
	var nomeTabela2 = elemento2.id.replace('celula', 'tabela');
	var nomeTabela1 = elemento1.id.replace('celula', 'tabela');
	
	if(document.getElementById(nomeTabela2)){

		setPosicionamentoAbsolutoMantendoPosicao(document.getElementById(nomeTabela2));
		
	}
	
	if(tudoComPosAbsoluto && document.getElementById(nomeTabela1)){
		
		setPosicionamentoAbsolutoMantendoPosicao(document.getElementById(nomeTabela1));
		
	}
	
	var correcaoYMenor = 0;
	
	if(coordenadas1Conferencia.left < (coordenadas2Conferencia.left + elemento2.offsetWidth) &&
			coordenadas1Conferencia.left > coordenadas2Conferencia.left){
	
		correcaoYMenor = Math.floor(elemento2.offsetHeight / 2)  - gapY - 6;
		
	}
	else{
	
		correcaoYMenor = 0;
		
	}
	
	var xMenor, xMaior, yMenor, yMaior;
	
	if(coordenadas1.left - 30 < coordenadas2.left){
		
		xMenor = coordenadas1.left;
		xMaior = coordenadas2.left;		
		
	}
	else{
		
		xMenor = coordenadas2.left + elemento2.offsetWidth;
		xMaior = coordenadas1.left;		
		
	}
	
	if(Math.abs(xMenor - xMaior) <= 30){
				
		var deslocamento = 0;
		correcaoYMenor = 0;
		
		if(coordenadasTopLeftOriginais1.left < coordenadasTopLeftOriginais2.left){
			
			deslocamento = -30;
			xMenor += deslocamento;	
			
		}
		else if(coordenadasTopLeftOriginais1.left > coordenadasTopLeftOriginais2.left){
			
			deslocamento = +30;
			xMaior += deslocamento;
			
		}
		
		coordenadas1.left += deslocamento;	

	}	
	
	if(coordenadas1.top < coordenadas2.top){
	
		yMenor = coordenadas1.top + correcaoYMenor - gapY;
		yMaior = coordenadas2.top;		
		
	}
	else{
		
		yMenor = coordenadas2.top + correcaoYMenor;
		yMaior = coordenadas1.top + gapY;		
		
	}
	
	var i, j;
	var elementoCorrente;	
	
	var nomeArquivoCurva = "";
	var deslocamentoCurva = 0;
	
	if(coordenadas1.left < coordenadas2.left){
		
		nomeArquivoCurva = "imgs/padrao/curva_esquerda.png";
		deslocamentoCurva = 0;
		xMenor += 2;
		
	}
	else{
		
		nomeArquivoCurva = "imgs/padrao/curva_direita.png";
		deslocamentoCurva = -5;
		xMaior -= 2;
		
	}
	
	var tracoHorizontal = document.createElement("img");
	tracoHorizontal.src = "imgs/padrao/ponto.png";
	tracoHorizontal.style.position = "absolute";
	tracoHorizontal.style.zIndex = "99";
	tracoHorizontal.border = "0";
	tracoHorizontal.style.left = xMenor;
	tracoHorizontal.style.top = coordenadas2.top;
	tracoHorizontal.width = xMaior - xMenor;
	tracoHorizontal.height = "1";
	tracoHorizontal.name = "traco_ligacao";
	
	document.getElementById("div_organograma").appendChild(tracoHorizontal);
	
	var tracoVertical = document.createElement("img");
	tracoVertical.src = "imgs/padrao/ponto.png";
	tracoVertical.style.position = "absolute";
	tracoVertical.style.zIndex = "99";
	tracoVertical.border = "0";
	tracoVertical.style.left = coordenadas1.left;
	tracoVertical.style.top = yMenor + 5;
	tracoVertical.width = "1";
	tracoVertical.height = yMaior - (yMenor + 5);
	tracoVertical.name = "traco_ligacao";
	
	document.getElementById("div_organograma").appendChild(tracoVertical);
	
	if(correcaoYMenor == 0){
		
		var curva = document.createElement("img");
		curva.src = nomeArquivoCurva;
		curva.style.position = "absolute";
		curva.style.zIndex = "100";
		curva.border = "0";
		curva.style.left = coordenadas1.left + deslocamentoCurva;
		curva.style.top = coordenadas2.top;
		curva.width = "5";
		curva.height = "5";
		curva.name = "traco_ligacao";
		
		document.getElementById("div_organograma").appendChild(curva);
			
	}
	
}

function zoom(elemento, valorAumentar){
	
	//internet explorer
	if(document.all){
		
		var novoZoom = "";
		
		if(elemento.style.zoom.length > 0){
			
			novoZoom = elemento.style.zoom.substring(0, elemento.style.zoom.length-1);

			var intNovoZoom = parseInt(novoZoom);
			var intNovoZoom = intNovoZoom + valorAumentar;
			
			novoZoom = intNovoZoom + "%";
						
		}
		else{
		
			var intNovoZoom = 100 + valorAumentar;
			novoZoom = intNovoZoom + "%";
			
		}

		elemento.style.zoom = novoZoom;
		
		if(novoZoom == "100%"){
		
			gerarTracosLigacao();
			
		}
		else{
		
			removerTracosLigacao();
			
		}

	}
	else{

		var fatorDeEscala = ((100 + valorAumentar) / 100);
		
		if(typeof elemento.offsetWidth != 'undefined' && 
				typeof elemento.offsetHeight != 'undefined' &&
					elemento.offsetWidth != 0 && 
						elemento.offsetHeight != 0){
			
			var tamanhoWidth = (elemento.offsetWidth);
			tamanhoWidth = tamanhoWidth * fatorDeEscala;
			
			var tamanhoHeight = (elemento.offsetHeight);
			tamanhoHeight = tamanhoHeight * fatorDeEscala;

			elemento.style.width = (tamanhoWidth);
			elemento.style.height = (tamanhoHeight);
						
		}
		
		if(typeof elemento.style != 'undefined' && typeof elemento.style.fontSize != 'undefined'){
			
			var tamanhoFonte = pegarValorEUnidadeDeTamanho(elemento.style.fontSize);
			
			tamanhoFonte[0] = parseFloat(tamanhoFonte[0]) * fatorDeEscala;
			
			elemento.style.fontSize = (tamanhoFonte[0]) + tamanhoFonte[1];
		
		}
		
		for(var i=0; i < elemento.childNodes.length; i++){
			
			zoom(elemento.childNodes[i], valorAumentar);
			
		}
		
	}
	
}

function selecionarTodosCheckboxesDoFiltroDeSelecaoMultipla(id_checkbox, nivel_estrutura, inicio, fim){

	var componente = document.getElementById("checkboxtodos" + id_checkbox + "_" + nivel_estrutura);

	var fimInfinito = false;

		if(inicio == null)
			inicio = 0;

		if(fim == null){

			fimInfinito = true;
			fim = 0;

		}

		for(var i=inicio; i <= fim || fimInfinito; i++){

			if(document.getElementById("checkbox_" + nivel_estrutura + "_" + i) != null){

				document.getElementById("checkbox_" + nivel_estrutura + "_" + i).checked = componente.checked;

			}
			else{

				break;

			}

		}

		for(var i=inicio; i <= fim || fimInfinito; i++){

			if(document.getElementById("checkbox_coletivo_" + nivel_estrutura + "_" + i) != null){

				document.getElementById("checkbox_coletivo_" + nivel_estrutura + "_" + i).checked = componente.checked;

			}
			else{

				break;

			}

		}

}