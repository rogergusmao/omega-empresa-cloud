<?php

define('PATH_RELATIVO_PROJETO', 'adm/');
define('NOME_DIRETORIO_LOG', 'Biblioteca Nuvem');
define('ARQUIVO_XML_ESTRUTURA', 'arquivo_xml_estrutura.xml');
define('ARQUIVO_SCRIPT_ESTRUTURA', 'arquivo_script_estrutura.xml');
define('IDENTIFICADOR_SISTEMA', 'BIB');
define('TITULO_PAGINAS_CLIENTE', "Cloud - WorkOffline.com.br");
define('MODO_BANCO_COM_CORPORACAO', false);
define('BANCO_COM_TRATAMENTO_EXCLUSAO', false);
//identificador padrão do projeto que estará no ar para um determinado cliente
//no caso do projeto do ponto eletronico temos o id = 1
//PONTO ELETRONICO
define('ID_PROJETOS', 34 ); 

define('TEMPO_DE_ESPERA_PARA_INICIO_DO_MONITORAMENTO_DE_TELA', 5 * 60); //em segundos

//Tempo necessario para o sistema considerar o telefone offline
define('TEMPO_TOTAL_EM_SEGUNDOS_TELFONE_FICAR_OFFLINE', 5 * 60);

define('PAGINA_INICIAL_PADRAO', "pages/seleciona_projetos.php");

define('LIMITE_SEGUNDOS_PARA_DESCONECTAR_SUPORTE', (5 * 60));
define('LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE', (30 * 60));

