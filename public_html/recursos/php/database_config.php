<?php

class SingletonRaizWorkspace{
    public static $raizWorkspace = null;
}

function acharRaizWorkspace()
{
    if (SingletonRaizWorkspace::$raizWorkspace != null)
    {
        return SingletonRaizWorkspace::$raizWorkspace;
    }
    $pathDir = '';
    $niveis = 0;

    if (php_sapi_name() != 'cli')
    {

        $pathDir = $_SERVER["SCRIPT_FILENAME"];
        $niveis = substr_count($pathDir, "/");
    }
    else
    {

        $pathDir = getcwd();
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        {
            $niveis = substr_count($pathDir, "\\");
        }
        else
        {
            $niveis = substr_count($pathDir, "/");
        }

    }

    $root = "";

    for ($i = 0; $i < $niveis; $i++)
    {
        if (is_dir("{$root}__workspaceRoot"))
        {

            SingletonRaizWorkspace::$raizWorkspace = $root;
            return SingletonRaizWorkspace::$raizWorkspace;

        }
        else
        {
            $root .= "../";
        }
    }
    SingletonRaizWorkspace::$raizWorkspace = "";
    return SingletonRaizWorkspace::$raizWorkspace;
}
if (php_sapi_name() != 'cli'){
    if( substr_count($_SERVER["HTTP_HOST"],  "workoffline.com.br") >= 1){

        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "database1.workoffline.com.br");
        define('REDIS_PORT', 7333);

        define('IDENTIFICADOR_SESSAO', "BN_");
        define('TITULO_PAGINAS', "Cloud - WorkOffline.com.br");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "cloud");
        define('NOME_BANCO_DE_DADOS_BACKUP', "");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "");

        define('BANCO_DE_DADOS_HOST', "database1.workoffline.com.br");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "cobige@4");

        define('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
        define('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
        define('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
        define('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

        define('DOMINIO_DE_ACESSO', 'http://cloud.workoffline.com.br');
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');
    }
    else {

        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "127.0.0.1");
        define('REDIS_PORT', 6379);
        if( substr_count($_SERVER["HTTP_HOST"],  "cloud.empresa.omegasoftware.com.br") >= 1){

            define('IDENTIFICADOR_SESSAO', "BN_");
            define('TITULO_PAGINAS', "WorkOffline - Cloud");

            define('NOME_BANCO_DE_DADOS_PRINCIPAL', "prd_cloud");
            define('NOME_BANCO_DE_DADOS_BACKUP', "prd_cloud_backup");
            define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "prd_cloud_parametros");

            define('BANCO_DE_DADOS_HOST', "vps.omegasoftware.com.br");
            define('BANCO_DE_DADOS_PORTA', "3307");
            define('BANCO_DE_DADOS_USUARIO', "root");
            define('BANCO_DE_DADOS_SENHA', "EeXKjzeC4Rum");

            define('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
            define('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
            define('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
            define('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

            define('DOMINIO_DE_ACESSO', 'http://127.0.0.1/BibliotecaNuvem/10003Corporacao/');
            define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/adm/webservice.php?class=Servicos_web&action=");
            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
        }
        elseif (strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp/www/") && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br") {

            define('IDENTIFICADOR_SESSAO', "BN_");
            define('TITULO_PAGINAS', "Biblioteca Nuvem Eduardo LocalHost");

            define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "biblioteca_nuvem");
            define('NOME_BANCO_DE_DADOS_PRINCIPAL', "biblioteca_nuvem");
            define('NOME_BANCO_DE_DADOS_BACKUP', "biblioteca_nuvem");

            define('BANCO_DE_DADOS_HOST', "localhost");
            define('BANCO_DE_DADOS_PORTA', "3306");
            define('BANCO_DE_DADOS_USUARIO', "root");
            define('BANCO_DE_DADOS_SENHA', "");

            define('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
            define('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
            define('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
            define('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

            define('DOMINIO_DE_ACESSO', 'http://127.0.0.1/OmegaEmpresa/BibliotecaNuvem/Trunk/');
            define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');

        }else{

            define('IDENTIFICADOR_SESSAO', "BN_");
            define('TITULO_PAGINAS', "Biblioteca Nuvem Roger LocalHost");

            define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "biblioteca_nuvem_corporacao");
            define('NOME_BANCO_DE_DADOS_PRINCIPAL', "biblioteca_nuvem_corporacao");
            define('NOME_BANCO_DE_DADOS_BACKUP', "biblioteca_nuvem_corporacao");

            define('BANCO_DE_DADOS_HOST', "localhost");
            define('BANCO_DE_DADOS_PORTA', "3306");
            define('BANCO_DE_DADOS_USUARIO', "root");
            define('BANCO_DE_DADOS_SENHA', "");

            define('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
            define('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
            define('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
            define('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

            define('DOMINIO_DE_ACESSO', 'http://127.0.0.1/BibliotecaNuvem/BN10003Corporacao/');
            define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
        }

    }
} else {
    define('REDIS_SCHEME', "tcp");
    define('REDIS_HOST', "127.0.0.1");
    define('REDIS_PORT', 6379);

    define('IDENTIFICADOR_SESSAO', "BN_");
    define('TITULO_PAGINAS', "Biblioteca Nuvem Roger LocalHost");

    define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "biblioteca_nuvem_corporacao");
    define('NOME_BANCO_DE_DADOS_PRINCIPAL', "biblioteca_nuvem_corporacao");
    define('NOME_BANCO_DE_DADOS_BACKUP', "biblioteca_nuvem_corporacao");

    define('BANCO_DE_DADOS_HOST', "localhost");
    define('BANCO_DE_DADOS_PORTA', "3306");
    define('BANCO_DE_DADOS_USUARIO', "root");
    define('BANCO_DE_DADOS_SENHA', "");

    define('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
    define('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
    define('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
    define('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

    define('DOMINIO_DE_ACESSO', 'http://127.0.0.1/BibliotecaNuvem/BN10003Corporacao/');
    define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
    define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
}

define('ENDERECO_DE_ACESSO', "http://" + DOMINIO_DE_ACESSO + "/");
define('ENDERECO_DE_ACESSO_SSL', "https://" + DOMINIO_DE_ACESSO + "/");

