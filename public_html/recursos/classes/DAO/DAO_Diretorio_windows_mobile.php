<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Diretorio_windows_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Diretorio_windows_mobile.php
    * TABELA MYSQL:    diretorio_windows_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Diretorio_windows_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_versao_banco_banco_id_INT;
	public $obj;
	public $DAO;
	public $EXTDAO;
	public $forms;
	public $filtros;
	public $lists;
	public $adapter;
	public $itemList;
	public $detail;


    public $nomeEntidade;



    

	public $label_id;
	public $label_projetos_versao_banco_banco_id_INT;
	public $label_DAO;
	public $label_EXTDAO;
	public $label_forms;
	public $label_filtros;
	public $label_lists;
	public $label_adapter;
	public $label_itemList;
	public $label_detail;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "diretorio_windows_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_versao_banco_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->obj->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_diretorio_windows_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_diretorio_windows_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    public function getDAO()
    {
    	return $this->DAO;
    }
    
    public function getEXTDAO()
    {
    	return $this->EXTDAO;
    }
    
    public function getForms()
    {
    	return $this->forms;
    }
    
    public function getFiltros()
    {
    	return $this->filtros;
    }
    
    public function getLists()
    {
    	return $this->lists;
    }
    
    public function getAdapter()
    {
    	return $this->adapter;
    }
    
    public function getItemList()
    {
    	return $this->itemList;
    }
    
    public function getDetail()
    {
    	return $this->detail;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setDAO($val)
    {
    	$this->DAO =  $val;
    }
    
    function setEXTDAO($val)
    {
    	$this->EXTDAO =  $val;
    }
    
    function setForms($val)
    {
    	$this->forms =  $val;
    }
    
    function setFiltros($val)
    {
    	$this->filtros =  $val;
    }
    
    function setLists($val)
    {
    	$this->lists =  $val;
    }
    
    function setAdapter($val)
    {
    	$this->adapter =  $val;
    }
    
    function setItemList($val)
    {
    	$this->itemList =  $val;
    }
    
    function setDetail($val)
    {
    	$this->detail =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM diretorio_windows_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

        $this->DAO = $row->DAO;
        
        $this->EXTDAO = $row->EXTDAO;
        
        $this->forms = $row->forms;
        
        $this->filtros = $row->filtros;
        
        $this->lists = $row->lists;
        
        $this->adapter = $row->adapter;
        
        $this->itemList = $row->itemList;
        
        $this->detail = $row->detail;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM diretorio_windows_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO diretorio_windows_mobile ( projetos_versao_banco_banco_id_INT , DAO , EXTDAO , forms , filtros , lists , adapter , itemList , detail ) VALUES ( {$this->projetos_versao_banco_banco_id_INT} , {$this->DAO} , {$this->EXTDAO} , {$this->forms} , {$this->filtros} , {$this->lists} , {$this->adapter} , {$this->itemList} , {$this->detail} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoDAO(){ 

		return "DAO";

	}

	public function nomeCampoEXTDAO(){ 

		return "EXTDAO";

	}

	public function nomeCampoForms(){ 

		return "forms";

	}

	public function nomeCampoFiltros(){ 

		return "filtros";

	}

	public function nomeCampoLists(){ 

		return "lists";

	}

	public function nomeCampoAdapter(){ 

		return "adapter";

	}

	public function nomeCampoItemList(){ 

		return "itemList";

	}

	public function nomeCampoDetail(){ 

		return "detail";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoDAO($objArguments){

		$objArguments->nome = "DAO";
		$objArguments->id = "DAO";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoEXTDAO($objArguments){

		$objArguments->nome = "EXTDAO";
		$objArguments->id = "EXTDAO";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoForms($objArguments){

		$objArguments->nome = "forms";
		$objArguments->id = "forms";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoFiltros($objArguments){

		$objArguments->nome = "filtros";
		$objArguments->id = "filtros";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoLists($objArguments){

		$objArguments->nome = "lists";
		$objArguments->id = "lists";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoAdapter($objArguments){

		$objArguments->nome = "adapter";
		$objArguments->id = "adapter";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoItemList($objArguments){

		$objArguments->nome = "itemList";
		$objArguments->id = "itemList";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDetail($objArguments){

		$objArguments->nome = "detail";
		$objArguments->id = "detail";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}

			$this->DAO = $this->formatarDadosParaSQL($this->DAO);
			$this->EXTDAO = $this->formatarDadosParaSQL($this->EXTDAO);
			$this->forms = $this->formatarDadosParaSQL($this->forms);
			$this->filtros = $this->formatarDadosParaSQL($this->filtros);
			$this->lists = $this->formatarDadosParaSQL($this->lists);
			$this->adapter = $this->formatarDadosParaSQL($this->adapter);
			$this->itemList = $this->formatarDadosParaSQL($this->itemList);
			$this->detail = $this->formatarDadosParaSQL($this->detail);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;
		$this->DAO = null; 
		$this->EXTDAO = null; 
		$this->forms = null; 
		$this->filtros = null; 
		$this->lists = null; 
		$this->adapter = null; 
		$this->itemList = null; 
		$this->detail = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 
		Helper::setSession("DAO", $this->DAO); 
		Helper::setSession("EXTDAO", $this->EXTDAO); 
		Helper::setSession("forms", $this->forms); 
		Helper::setSession("filtros", $this->filtros); 
		Helper::setSession("lists", $this->lists); 
		Helper::setSession("adapter", $this->adapter); 
		Helper::setSession("itemList", $this->itemList); 
		Helper::setSession("detail", $this->detail); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");
		Helper::clearSession("DAO");
		Helper::clearSession("EXTDAO");
		Helper::clearSession("forms");
		Helper::clearSession("filtros");
		Helper::clearSession("lists");
		Helper::clearSession("adapter");
		Helper::clearSession("itemList");
		Helper::clearSession("detail");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->DAO = Helper::SESSION("DAO{$numReg}"); 
		$this->EXTDAO = Helper::SESSION("EXTDAO{$numReg}"); 
		$this->forms = Helper::SESSION("forms{$numReg}"); 
		$this->filtros = Helper::SESSION("filtros{$numReg}"); 
		$this->lists = Helper::SESSION("lists{$numReg}"); 
		$this->adapter = Helper::SESSION("adapter{$numReg}"); 
		$this->itemList = Helper::SESSION("itemList{$numReg}"); 
		$this->detail = Helper::SESSION("detail{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->DAO = Helper::POST("DAO{$numReg}"); 
		$this->EXTDAO = Helper::POST("EXTDAO{$numReg}"); 
		$this->forms = Helper::POST("forms{$numReg}"); 
		$this->filtros = Helper::POST("filtros{$numReg}"); 
		$this->lists = Helper::POST("lists{$numReg}"); 
		$this->adapter = Helper::POST("adapter{$numReg}"); 
		$this->itemList = Helper::POST("itemList{$numReg}"); 
		$this->detail = Helper::POST("detail{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->DAO = Helper::GET("DAO{$numReg}"); 
		$this->EXTDAO = Helper::GET("EXTDAO{$numReg}"); 
		$this->forms = Helper::GET("forms{$numReg}"); 
		$this->filtros = Helper::GET("filtros{$numReg}"); 
		$this->lists = Helper::GET("lists{$numReg}"); 
		$this->adapter = Helper::GET("adapter{$numReg}"); 
		$this->itemList = Helper::GET("itemList{$numReg}"); 
		$this->detail = Helper::GET("detail{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["DAO{$numReg}"]) || $tipo == null){

		$upd.= "DAO = $this->DAO, ";

	} 

	if(isset($tipo["EXTDAO{$numReg}"]) || $tipo == null){

		$upd.= "EXTDAO = $this->EXTDAO, ";

	} 

	if(isset($tipo["forms{$numReg}"]) || $tipo == null){

		$upd.= "forms = $this->forms, ";

	} 

	if(isset($tipo["filtros{$numReg}"]) || $tipo == null){

		$upd.= "filtros = $this->filtros, ";

	} 

	if(isset($tipo["lists{$numReg}"]) || $tipo == null){

		$upd.= "lists = $this->lists, ";

	} 

	if(isset($tipo["adapter{$numReg}"]) || $tipo == null){

		$upd.= "adapter = $this->adapter, ";

	} 

	if(isset($tipo["itemList{$numReg}"]) || $tipo == null){

		$upd.= "itemList = $this->itemList, ";

	} 

	if(isset($tipo["detail{$numReg}"]) || $tipo == null){

		$upd.= "detail = $this->detail, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE diretorio_windows_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    