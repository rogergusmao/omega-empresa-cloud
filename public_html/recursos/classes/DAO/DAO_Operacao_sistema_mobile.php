<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_sistema_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Operacao_sistema_mobile.php
    * TABELA MYSQL:    operacao_sistema_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_sistema_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $tipo_operacao_sistema_id_INT;
	public $obj;
	public $estado_operacao_sistema_mobile_id_INT;
	public $objTipo_operacao_sistema;
	public $mobile_identificador_id_INT;
	public $objEstado_operacao_sistema_mobile;
	public $data_abertura_DATETIME;
	public $data_processamento_DATETIME;
	public $data_conclusao_DATETIME;


    public $nomeEntidade;

	public $data_abertura_DATETIME_UNIX;
	public $data_processamento_DATETIME_UNIX;
	public $data_conclusao_DATETIME_UNIX;


    

	public $label_id;
	public $label_tipo_operacao_sistema_id_INT;
	public $label_estado_operacao_sistema_mobile_id_INT;
	public $label_mobile_identificador_id_INT;
	public $label_data_abertura_DATETIME;
	public $label_data_processamento_DATETIME;
	public $label_data_conclusao_DATETIME;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "operacao_sistema_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTipo_operacao_sistema(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tipo_operacao_sistema($this->getDatabase());
		if($this->tipo_operacao_sistema_id_INT != null) 
		$this->obj->select($this->tipo_operacao_sistema_id_INT);
	}
	return $this->obj ;
}
function getFkObjEstado_operacao_sistema_mobile(){
	if($this->objTipo_operacao_sistema ==null){
		$this->objTipo_operacao_sistema = new EXTDAO_Estado_operacao_sistema_mobile($this->getDatabase());
		if($this->estado_operacao_sistema_mobile_id_INT != null) 
		$this->objTipo_operacao_sistema->select($this->estado_operacao_sistema_mobile_id_INT);
	}
	return $this->objTipo_operacao_sistema ;
}
function getFkObjMobile_identificador(){
	if($this->objEstado_operacao_sistema_mobile ==null){
		$this->objEstado_operacao_sistema_mobile = new EXTDAO_Mobile_identificador($this->getDatabase());
		if($this->mobile_identificador_id_INT != null) 
		$this->objEstado_operacao_sistema_mobile->select($this->mobile_identificador_id_INT);
	}
	return $this->objEstado_operacao_sistema_mobile ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTipo_operacao_sistema($objArgumentos){

		$objArgumentos->nome="tipo_operacao_sistema_id_INT";
		$objArgumentos->id="tipo_operacao_sistema_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_sistema()->getComboBox($objArgumentos);

	}

public function getComboBoxAllEstado_operacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="estado_operacao_sistema_mobile_id_INT";
		$objArgumentos->id="estado_operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjEstado_operacao_sistema_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllMobile_identificador($objArgumentos){

		$objArgumentos->nome="mobile_identificador_id_INT";
		$objArgumentos->id="mobile_identificador_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjMobile_identificador()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_operacao_sistema_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_sistema_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTipo_operacao_sistema_id_INT()
    {
    	return $this->tipo_operacao_sistema_id_INT;
    }
    
    public function getEstado_operacao_sistema_mobile_id_INT()
    {
    	return $this->estado_operacao_sistema_mobile_id_INT;
    }
    
    public function getMobile_identificador_id_INT()
    {
    	return $this->mobile_identificador_id_INT;
    }
    
    function getData_abertura_DATETIME_UNIX()
    {
    	return $this->data_abertura_DATETIME_UNIX;
    }
    
    public function getData_abertura_DATETIME()
    {
    	return $this->data_abertura_DATETIME;
    }
    
    function getData_processamento_DATETIME_UNIX()
    {
    	return $this->data_processamento_DATETIME_UNIX;
    }
    
    public function getData_processamento_DATETIME()
    {
    	return $this->data_processamento_DATETIME;
    }
    
    function getData_conclusao_DATETIME_UNIX()
    {
    	return $this->data_conclusao_DATETIME_UNIX;
    }
    
    public function getData_conclusao_DATETIME()
    {
    	return $this->data_conclusao_DATETIME;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTipo_operacao_sistema_id_INT($val)
    {
    	$this->tipo_operacao_sistema_id_INT =  $val;
    }
    
    function setEstado_operacao_sistema_mobile_id_INT($val)
    {
    	$this->estado_operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setMobile_identificador_id_INT($val)
    {
    	$this->mobile_identificador_id_INT =  $val;
    }
    
    function setData_abertura_DATETIME($val)
    {
    	$this->data_abertura_DATETIME =  $val;
    }
    
    function setData_processamento_DATETIME($val)
    {
    	$this->data_processamento_DATETIME =  $val;
    }
    
    function setData_conclusao_DATETIME($val)
    {
    	$this->data_conclusao_DATETIME =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_abertura_DATETIME) AS data_abertura_DATETIME_UNIX, UNIX_TIMESTAMP(data_processamento_DATETIME) AS data_processamento_DATETIME_UNIX, UNIX_TIMESTAMP(data_conclusao_DATETIME) AS data_conclusao_DATETIME_UNIX FROM operacao_sistema_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->tipo_operacao_sistema_id_INT = $row->tipo_operacao_sistema_id_INT;
        if(isset($this->objTipo_operacao_sistema))
			$this->objTipo_operacao_sistema->select($this->tipo_operacao_sistema_id_INT);

        $this->estado_operacao_sistema_mobile_id_INT = $row->estado_operacao_sistema_mobile_id_INT;
        if(isset($this->objEstado_operacao_sistema_mobile))
			$this->objEstado_operacao_sistema_mobile->select($this->estado_operacao_sistema_mobile_id_INT);

        $this->mobile_identificador_id_INT = $row->mobile_identificador_id_INT;
        if(isset($this->objMobile_identificador))
			$this->objMobile_identificador->select($this->mobile_identificador_id_INT);

        $this->data_abertura_DATETIME = $row->data_abertura_DATETIME;
        $this->data_abertura_DATETIME_UNIX = $row->data_abertura_DATETIME_UNIX;

        $this->data_processamento_DATETIME = $row->data_processamento_DATETIME;
        $this->data_processamento_DATETIME_UNIX = $row->data_processamento_DATETIME_UNIX;

        $this->data_conclusao_DATETIME = $row->data_conclusao_DATETIME;
        $this->data_conclusao_DATETIME_UNIX = $row->data_conclusao_DATETIME_UNIX;

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM operacao_sistema_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO operacao_sistema_mobile ( tipo_operacao_sistema_id_INT , estado_operacao_sistema_mobile_id_INT , mobile_identificador_id_INT , data_abertura_DATETIME , data_processamento_DATETIME , data_conclusao_DATETIME ) VALUES ( {$this->tipo_operacao_sistema_id_INT} , {$this->estado_operacao_sistema_mobile_id_INT} , {$this->mobile_identificador_id_INT} , {$this->data_abertura_DATETIME} , {$this->data_processamento_DATETIME} , {$this->data_conclusao_DATETIME} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTipo_operacao_sistema_id_INT(){ 

		return "tipo_operacao_sistema_id_INT";

	}

	public function nomeCampoEstado_operacao_sistema_mobile_id_INT(){ 

		return "estado_operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoMobile_identificador_id_INT(){ 

		return "mobile_identificador_id_INT";

	}

	public function nomeCampoData_abertura_DATETIME(){ 

		return "data_abertura_DATETIME";

	}

	public function nomeCampoData_processamento_DATETIME(){ 

		return "data_processamento_DATETIME";

	}

	public function nomeCampoData_conclusao_DATETIME(){ 

		return "data_conclusao_DATETIME";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTipo_operacao_sistema_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_sistema_id_INT";
		$objArguments->id = "tipo_operacao_sistema_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoEstado_operacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "estado_operacao_sistema_mobile_id_INT";
		$objArguments->id = "estado_operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMobile_identificador_id_INT($objArguments){

		$objArguments->nome = "mobile_identificador_id_INT";
		$objArguments->id = "mobile_identificador_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_abertura_DATETIME($objArguments){

		$objArguments->nome = "data_abertura_DATETIME";
		$objArguments->id = "data_abertura_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_processamento_DATETIME($objArguments){

		$objArguments->nome = "data_processamento_DATETIME";
		$objArguments->id = "data_processamento_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_conclusao_DATETIME($objArguments){

		$objArguments->nome = "data_conclusao_DATETIME";
		$objArguments->id = "data_conclusao_DATETIME";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->tipo_operacao_sistema_id_INT == ""){

			$this->tipo_operacao_sistema_id_INT = "null";

		}

		if($this->estado_operacao_sistema_mobile_id_INT == ""){

			$this->estado_operacao_sistema_mobile_id_INT = "null";

		}

		if($this->mobile_identificador_id_INT == ""){

			$this->mobile_identificador_id_INT = "null";

		}



	$this->data_abertura_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_abertura_DATETIME); 
	$this->data_processamento_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_processamento_DATETIME); 
	$this->data_conclusao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_conclusao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_abertura_DATETIME = $this->formatarDataTimeParaExibicao($this->data_abertura_DATETIME); 
	$this->data_processamento_DATETIME = $this->formatarDataTimeParaExibicao($this->data_processamento_DATETIME); 
	$this->data_conclusao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_conclusao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->tipo_operacao_sistema_id_INT = null; 
		$this->objTipo_operacao_sistema= null;
		$this->estado_operacao_sistema_mobile_id_INT = null; 
		$this->objEstado_operacao_sistema_mobile= null;
		$this->mobile_identificador_id_INT = null; 
		$this->objMobile_identificador= null;
		$this->data_abertura_DATETIME = null; 
		$this->data_processamento_DATETIME = null; 
		$this->data_conclusao_DATETIME = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("tipo_operacao_sistema_id_INT", $this->tipo_operacao_sistema_id_INT); 
		Helper::setSession("estado_operacao_sistema_mobile_id_INT", $this->estado_operacao_sistema_mobile_id_INT); 
		Helper::setSession("mobile_identificador_id_INT", $this->mobile_identificador_id_INT); 
		Helper::setSession("data_abertura_DATETIME", $this->data_abertura_DATETIME); 
		Helper::setSession("data_processamento_DATETIME", $this->data_processamento_DATETIME); 
		Helper::setSession("data_conclusao_DATETIME", $this->data_conclusao_DATETIME); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("tipo_operacao_sistema_id_INT");
		Helper::clearSession("estado_operacao_sistema_mobile_id_INT");
		Helper::clearSession("mobile_identificador_id_INT");
		Helper::clearSession("data_abertura_DATETIME");
		Helper::clearSession("data_processamento_DATETIME");
		Helper::clearSession("data_conclusao_DATETIME");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->tipo_operacao_sistema_id_INT = Helper::SESSION("tipo_operacao_sistema_id_INT{$numReg}"); 
		$this->estado_operacao_sistema_mobile_id_INT = Helper::SESSION("estado_operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::SESSION("mobile_identificador_id_INT{$numReg}"); 
		$this->data_abertura_DATETIME = Helper::SESSION("data_abertura_DATETIME{$numReg}"); 
		$this->data_processamento_DATETIME = Helper::SESSION("data_processamento_DATETIME{$numReg}"); 
		$this->data_conclusao_DATETIME = Helper::SESSION("data_conclusao_DATETIME{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->tipo_operacao_sistema_id_INT = Helper::POST("tipo_operacao_sistema_id_INT{$numReg}"); 
		$this->estado_operacao_sistema_mobile_id_INT = Helper::POST("estado_operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::POST("mobile_identificador_id_INT{$numReg}"); 
		$this->data_abertura_DATETIME = Helper::POST("data_abertura_DATETIME{$numReg}"); 
		$this->data_processamento_DATETIME = Helper::POST("data_processamento_DATETIME{$numReg}"); 
		$this->data_conclusao_DATETIME = Helper::POST("data_conclusao_DATETIME{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->tipo_operacao_sistema_id_INT = Helper::GET("tipo_operacao_sistema_id_INT{$numReg}"); 
		$this->estado_operacao_sistema_mobile_id_INT = Helper::GET("estado_operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::GET("mobile_identificador_id_INT{$numReg}"); 
		$this->data_abertura_DATETIME = Helper::GET("data_abertura_DATETIME{$numReg}"); 
		$this->data_processamento_DATETIME = Helper::GET("data_processamento_DATETIME{$numReg}"); 
		$this->data_conclusao_DATETIME = Helper::GET("data_conclusao_DATETIME{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["tipo_operacao_sistema_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_sistema_id_INT = $this->tipo_operacao_sistema_id_INT, ";

	} 

	if(isset($tipo["estado_operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "estado_operacao_sistema_mobile_id_INT = $this->estado_operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["mobile_identificador_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "mobile_identificador_id_INT = $this->mobile_identificador_id_INT, ";

	} 

	if(isset($tipo["data_abertura_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_abertura_DATETIME = $this->data_abertura_DATETIME, ";

	} 

	if(isset($tipo["data_processamento_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_processamento_DATETIME = $this->data_processamento_DATETIME, ";

	} 

	if(isset($tipo["data_conclusao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_conclusao_DATETIME = $this->data_conclusao_DATETIME, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE operacao_sistema_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    