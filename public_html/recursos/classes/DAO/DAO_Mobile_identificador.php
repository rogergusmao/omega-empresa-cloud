<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Mobile_identificador
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Mobile_identificador.php
    * TABELA MYSQL:    mobile_identificador
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Mobile_identificador extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $mobile_id_INT;
	public $obj;
	public $sistema_projetos_versao_sistema_produto_mobile_id_INT;
	public $objMobile;
	public $data_cadastro_DATETIME;
	public $requisicao_modo_suporte_DATETIME;
	public $requisicao_usuario_id_INT;
	public $objSistema_projetos_versao_sistema_produto_mobile;
	public $mobile_resposta_suporte_DATETIME;
	public $is_hospedeiro_BOOLEAN;
	public $reinstalar_a_versao_id_INT;
	public $objRequisicao_usuario;
	public $atualizacao_versao_ativa_BOOLEAN;


    public $nomeEntidade;

	public $data_cadastro_DATETIME_UNIX;
	public $requisicao_modo_suporte_DATETIME_UNIX;
	public $mobile_resposta_suporte_DATETIME_UNIX;


    

	public $label_id;
	public $label_mobile_id_INT;
	public $label_sistema_projetos_versao_sistema_produto_mobile_id_INT;
	public $label_data_cadastro_DATETIME;
	public $label_requisicao_modo_suporte_DATETIME;
	public $label_requisicao_usuario_id_INT;
	public $label_mobile_resposta_suporte_DATETIME;
	public $label_is_hospedeiro_BOOLEAN;
	public $label_reinstalar_a_versao_id_INT;
	public $label_atualizacao_versao_ativa_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "mobile_identificador";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjMobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Mobile($this->getDatabase());
		if($this->mobile_id_INT != null) 
		$this->obj->select($this->mobile_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_projetos_versao_sistema_produto_mobile(){
	if($this->objMobile ==null){
		$this->objMobile = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile($this->getDatabase());
		if($this->sistema_projetos_versao_sistema_produto_mobile_id_INT != null) 
		$this->objMobile->select($this->sistema_projetos_versao_sistema_produto_mobile_id_INT);
	}
	return $this->objMobile ;
}
function getFkObjRequisicao_usuario(){
	if($this->objSistema_projetos_versao_sistema_produto_mobile ==null){
		$this->objSistema_projetos_versao_sistema_produto_mobile = new EXTDAO_Usuario($this->getDatabase());
		if($this->requisicao_usuario_id_INT != null) 
		$this->objSistema_projetos_versao_sistema_produto_mobile->select($this->requisicao_usuario_id_INT);
	}
	return $this->objSistema_projetos_versao_sistema_produto_mobile ;
}
function getFkObjReinstalar_a_versao(){
	if($this->objRequisicao_usuario ==null){
		$this->objRequisicao_usuario = new EXTDAO_($this->getDatabase());
		if($this->reinstalar_a_versao_id_INT != null) 
		$this->objRequisicao_usuario->select($this->reinstalar_a_versao_id_INT);
	}
	return $this->objRequisicao_usuario ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllMobile($objArgumentos){

		$objArgumentos->nome="mobile_id_INT";
		$objArgumentos->id="mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjMobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao_sistema_produto_mobile($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_sistema_produto_mobile_id_INT";
		$objArgumentos->id="sistema_projetos_versao_sistema_produto_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao_sistema_produto_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllRequisicao_usuario($objArgumentos){

		$objArgumentos->nome="requisicao_usuario_id_INT";
		$objArgumentos->id="requisicao_usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjRequisicao_usuario()->getComboBox($objArgumentos);

	}

public function getComboBoxAllReinstalar_a_versao($objArgumentos){

		$objArgumentos->nome="reinstalar_a_versao_id_INT";
		$objArgumentos->id="reinstalar_a_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjReinstalar_a_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_mobile_identificador", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_mobile_identificador", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getMobile_id_INT()
    {
    	return $this->mobile_id_INT;
    }
    
    public function getSistema_projetos_versao_sistema_produto_mobile_id_INT()
    {
    	return $this->sistema_projetos_versao_sistema_produto_mobile_id_INT;
    }
    
    function getData_cadastro_DATETIME_UNIX()
    {
    	return $this->data_cadastro_DATETIME_UNIX;
    }
    
    public function getData_cadastro_DATETIME()
    {
    	return $this->data_cadastro_DATETIME;
    }
    
    function getRequisicao_modo_suporte_DATETIME_UNIX()
    {
    	return $this->requisicao_modo_suporte_DATETIME_UNIX;
    }
    
    public function getRequisicao_modo_suporte_DATETIME()
    {
    	return $this->requisicao_modo_suporte_DATETIME;
    }
    
    public function getRequisicao_usuario_id_INT()
    {
    	return $this->requisicao_usuario_id_INT;
    }
    
    function getMobile_resposta_suporte_DATETIME_UNIX()
    {
    	return $this->mobile_resposta_suporte_DATETIME_UNIX;
    }
    
    public function getMobile_resposta_suporte_DATETIME()
    {
    	return $this->mobile_resposta_suporte_DATETIME;
    }
    
    public function getIs_hospedeiro_BOOLEAN()
    {
    	return $this->is_hospedeiro_BOOLEAN;
    }
    
    public function getReinstalar_a_versao_id_INT()
    {
    	return $this->reinstalar_a_versao_id_INT;
    }
    
    public function getAtualizacao_versao_ativa_BOOLEAN()
    {
    	return $this->atualizacao_versao_ativa_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setMobile_id_INT($val)
    {
    	$this->mobile_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_sistema_produto_mobile_id_INT($val)
    {
    	$this->sistema_projetos_versao_sistema_produto_mobile_id_INT =  $val;
    }
    
    function setData_cadastro_DATETIME($val)
    {
    	$this->data_cadastro_DATETIME =  $val;
    }
    
    function setRequisicao_modo_suporte_DATETIME($val)
    {
    	$this->requisicao_modo_suporte_DATETIME =  $val;
    }
    
    function setRequisicao_usuario_id_INT($val)
    {
    	$this->requisicao_usuario_id_INT =  $val;
    }
    
    function setMobile_resposta_suporte_DATETIME($val)
    {
    	$this->mobile_resposta_suporte_DATETIME =  $val;
    }
    
    function setIs_hospedeiro_BOOLEAN($val)
    {
    	$this->is_hospedeiro_BOOLEAN =  $val;
    }
    
    function setReinstalar_a_versao_id_INT($val)
    {
    	$this->reinstalar_a_versao_id_INT =  $val;
    }
    
    function setAtualizacao_versao_ativa_BOOLEAN($val)
    {
    	$this->atualizacao_versao_ativa_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_cadastro_DATETIME) AS data_cadastro_DATETIME_UNIX, UNIX_TIMESTAMP(requisicao_modo_suporte_DATETIME) AS requisicao_modo_suporte_DATETIME_UNIX, UNIX_TIMESTAMP(mobile_resposta_suporte_DATETIME) AS mobile_resposta_suporte_DATETIME_UNIX FROM mobile_identificador WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->mobile_id_INT = $row->mobile_id_INT;
        if(isset($this->objMobile))
			$this->objMobile->select($this->mobile_id_INT);

        $this->sistema_projetos_versao_sistema_produto_mobile_id_INT = $row->sistema_projetos_versao_sistema_produto_mobile_id_INT;
        if(isset($this->objSistema_projetos_versao_sistema_produto_mobile))
			$this->objSistema_projetos_versao_sistema_produto_mobile->select($this->sistema_projetos_versao_sistema_produto_mobile_id_INT);

        $this->data_cadastro_DATETIME = $row->data_cadastro_DATETIME;
        $this->data_cadastro_DATETIME_UNIX = $row->data_cadastro_DATETIME_UNIX;

        $this->requisicao_modo_suporte_DATETIME = $row->requisicao_modo_suporte_DATETIME;
        $this->requisicao_modo_suporte_DATETIME_UNIX = $row->requisicao_modo_suporte_DATETIME_UNIX;

        $this->requisicao_usuario_id_INT = $row->requisicao_usuario_id_INT;
        if(isset($this->objRequisicao_usuario))
			$this->objRequisicao_usuario->select($this->requisicao_usuario_id_INT);

        $this->mobile_resposta_suporte_DATETIME = $row->mobile_resposta_suporte_DATETIME;
        $this->mobile_resposta_suporte_DATETIME_UNIX = $row->mobile_resposta_suporte_DATETIME_UNIX;

        $this->is_hospedeiro_BOOLEAN = $row->is_hospedeiro_BOOLEAN;
        
        $this->reinstalar_a_versao_id_INT = $row->reinstalar_a_versao_id_INT;
        if(isset($this->objReinstalar_a_versao))
			$this->objReinstalar_a_versao->select($this->reinstalar_a_versao_id_INT);

        $this->atualizacao_versao_ativa_BOOLEAN = $row->atualizacao_versao_ativa_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM mobile_identificador WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO mobile_identificador ( mobile_id_INT , sistema_projetos_versao_sistema_produto_mobile_id_INT , data_cadastro_DATETIME , requisicao_modo_suporte_DATETIME , requisicao_usuario_id_INT , mobile_resposta_suporte_DATETIME , is_hospedeiro_BOOLEAN , reinstalar_a_versao_id_INT , atualizacao_versao_ativa_BOOLEAN ) VALUES ( {$this->mobile_id_INT} , {$this->sistema_projetos_versao_sistema_produto_mobile_id_INT} , {$this->data_cadastro_DATETIME} , {$this->requisicao_modo_suporte_DATETIME} , {$this->requisicao_usuario_id_INT} , {$this->mobile_resposta_suporte_DATETIME} , {$this->is_hospedeiro_BOOLEAN} , {$this->reinstalar_a_versao_id_INT} , {$this->atualizacao_versao_ativa_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoMobile_id_INT(){ 

		return "mobile_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_sistema_produto_mobile_id_INT(){ 

		return "sistema_projetos_versao_sistema_produto_mobile_id_INT";

	}

	public function nomeCampoData_cadastro_DATETIME(){ 

		return "data_cadastro_DATETIME";

	}

	public function nomeCampoRequisicao_modo_suporte_DATETIME(){ 

		return "requisicao_modo_suporte_DATETIME";

	}

	public function nomeCampoRequisicao_usuario_id_INT(){ 

		return "requisicao_usuario_id_INT";

	}

	public function nomeCampoMobile_resposta_suporte_DATETIME(){ 

		return "mobile_resposta_suporte_DATETIME";

	}

	public function nomeCampoIs_hospedeiro_BOOLEAN(){ 

		return "is_hospedeiro_BOOLEAN";

	}

	public function nomeCampoReinstalar_a_versao_id_INT(){ 

		return "reinstalar_a_versao_id_INT";

	}

	public function nomeCampoAtualizacao_versao_ativa_BOOLEAN(){ 

		return "atualizacao_versao_ativa_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoMobile_id_INT($objArguments){

		$objArguments->nome = "mobile_id_INT";
		$objArguments->id = "mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_sistema_produto_mobile_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_sistema_produto_mobile_id_INT";
		$objArguments->id = "sistema_projetos_versao_sistema_produto_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_cadastro_DATETIME($objArguments){

		$objArguments->nome = "data_cadastro_DATETIME";
		$objArguments->id = "data_cadastro_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoRequisicao_modo_suporte_DATETIME($objArguments){

		$objArguments->nome = "requisicao_modo_suporte_DATETIME";
		$objArguments->id = "requisicao_modo_suporte_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoRequisicao_usuario_id_INT($objArguments){

		$objArguments->nome = "requisicao_usuario_id_INT";
		$objArguments->id = "requisicao_usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMobile_resposta_suporte_DATETIME($objArguments){

		$objArguments->nome = "mobile_resposta_suporte_DATETIME";
		$objArguments->id = "mobile_resposta_suporte_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoIs_hospedeiro_BOOLEAN($objArguments){

		$objArguments->nome = "is_hospedeiro_BOOLEAN";
		$objArguments->id = "is_hospedeiro_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoReinstalar_a_versao_id_INT($objArguments){

		$objArguments->nome = "reinstalar_a_versao_id_INT";
		$objArguments->id = "reinstalar_a_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoAtualizacao_versao_ativa_BOOLEAN($objArguments){

		$objArguments->nome = "atualizacao_versao_ativa_BOOLEAN";
		$objArguments->id = "atualizacao_versao_ativa_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->mobile_id_INT == ""){

			$this->mobile_id_INT = "null";

		}

		if($this->sistema_projetos_versao_sistema_produto_mobile_id_INT == ""){

			$this->sistema_projetos_versao_sistema_produto_mobile_id_INT = "null";

		}

		if($this->requisicao_usuario_id_INT == ""){

			$this->requisicao_usuario_id_INT = "null";

		}

		if($this->is_hospedeiro_BOOLEAN == ""){

			$this->is_hospedeiro_BOOLEAN = "null";

		}

		if($this->reinstalar_a_versao_id_INT == ""){

			$this->reinstalar_a_versao_id_INT = "null";

		}

		if($this->atualizacao_versao_ativa_BOOLEAN == ""){

			$this->atualizacao_versao_ativa_BOOLEAN = "null";

		}



	$this->data_cadastro_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_cadastro_DATETIME); 
	$this->requisicao_modo_suporte_DATETIME = $this->formatarDataTimeParaComandoSQL($this->requisicao_modo_suporte_DATETIME); 
	$this->mobile_resposta_suporte_DATETIME = $this->formatarDataTimeParaComandoSQL($this->mobile_resposta_suporte_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_cadastro_DATETIME = $this->formatarDataTimeParaExibicao($this->data_cadastro_DATETIME); 
	$this->requisicao_modo_suporte_DATETIME = $this->formatarDataTimeParaExibicao($this->requisicao_modo_suporte_DATETIME); 
	$this->mobile_resposta_suporte_DATETIME = $this->formatarDataTimeParaExibicao($this->mobile_resposta_suporte_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->mobile_id_INT = null; 
		$this->objMobile= null;
		$this->sistema_projetos_versao_sistema_produto_mobile_id_INT = null; 
		$this->objSistema_projetos_versao_sistema_produto_mobile= null;
		$this->data_cadastro_DATETIME = null; 
		$this->requisicao_modo_suporte_DATETIME = null; 
		$this->requisicao_usuario_id_INT = null; 
		$this->objRequisicao_usuario= null;
		$this->mobile_resposta_suporte_DATETIME = null; 
		$this->is_hospedeiro_BOOLEAN = null; 
		$this->reinstalar_a_versao_id_INT = null; 
		$this->objReinstalar_a_versao= null;
		$this->atualizacao_versao_ativa_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("mobile_id_INT", $this->mobile_id_INT); 
		Helper::setSession("sistema_projetos_versao_sistema_produto_mobile_id_INT", $this->sistema_projetos_versao_sistema_produto_mobile_id_INT); 
		Helper::setSession("data_cadastro_DATETIME", $this->data_cadastro_DATETIME); 
		Helper::setSession("requisicao_modo_suporte_DATETIME", $this->requisicao_modo_suporte_DATETIME); 
		Helper::setSession("requisicao_usuario_id_INT", $this->requisicao_usuario_id_INT); 
		Helper::setSession("mobile_resposta_suporte_DATETIME", $this->mobile_resposta_suporte_DATETIME); 
		Helper::setSession("is_hospedeiro_BOOLEAN", $this->is_hospedeiro_BOOLEAN); 
		Helper::setSession("reinstalar_a_versao_id_INT", $this->reinstalar_a_versao_id_INT); 
		Helper::setSession("atualizacao_versao_ativa_BOOLEAN", $this->atualizacao_versao_ativa_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("mobile_id_INT");
		Helper::clearSession("sistema_projetos_versao_sistema_produto_mobile_id_INT");
		Helper::clearSession("data_cadastro_DATETIME");
		Helper::clearSession("requisicao_modo_suporte_DATETIME");
		Helper::clearSession("requisicao_usuario_id_INT");
		Helper::clearSession("mobile_resposta_suporte_DATETIME");
		Helper::clearSession("is_hospedeiro_BOOLEAN");
		Helper::clearSession("reinstalar_a_versao_id_INT");
		Helper::clearSession("atualizacao_versao_ativa_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->mobile_id_INT = Helper::SESSION("mobile_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_sistema_produto_mobile_id_INT = Helper::SESSION("sistema_projetos_versao_sistema_produto_mobile_id_INT{$numReg}"); 
		$this->data_cadastro_DATETIME = Helper::SESSION("data_cadastro_DATETIME{$numReg}"); 
		$this->requisicao_modo_suporte_DATETIME = Helper::SESSION("requisicao_modo_suporte_DATETIME{$numReg}"); 
		$this->requisicao_usuario_id_INT = Helper::SESSION("requisicao_usuario_id_INT{$numReg}"); 
		$this->mobile_resposta_suporte_DATETIME = Helper::SESSION("mobile_resposta_suporte_DATETIME{$numReg}"); 
		$this->is_hospedeiro_BOOLEAN = Helper::SESSION("is_hospedeiro_BOOLEAN{$numReg}"); 
		$this->reinstalar_a_versao_id_INT = Helper::SESSION("reinstalar_a_versao_id_INT{$numReg}"); 
		$this->atualizacao_versao_ativa_BOOLEAN = Helper::SESSION("atualizacao_versao_ativa_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->mobile_id_INT = Helper::POST("mobile_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_sistema_produto_mobile_id_INT = Helper::POST("sistema_projetos_versao_sistema_produto_mobile_id_INT{$numReg}"); 
		$this->data_cadastro_DATETIME = Helper::POST("data_cadastro_DATETIME{$numReg}"); 
		$this->requisicao_modo_suporte_DATETIME = Helper::POST("requisicao_modo_suporte_DATETIME{$numReg}"); 
		$this->requisicao_usuario_id_INT = Helper::POST("requisicao_usuario_id_INT{$numReg}"); 
		$this->mobile_resposta_suporte_DATETIME = Helper::POST("mobile_resposta_suporte_DATETIME{$numReg}"); 
		$this->is_hospedeiro_BOOLEAN = Helper::POST("is_hospedeiro_BOOLEAN{$numReg}"); 
		$this->reinstalar_a_versao_id_INT = Helper::POST("reinstalar_a_versao_id_INT{$numReg}"); 
		$this->atualizacao_versao_ativa_BOOLEAN = Helper::POST("atualizacao_versao_ativa_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->mobile_id_INT = Helper::GET("mobile_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_sistema_produto_mobile_id_INT = Helper::GET("sistema_projetos_versao_sistema_produto_mobile_id_INT{$numReg}"); 
		$this->data_cadastro_DATETIME = Helper::GET("data_cadastro_DATETIME{$numReg}"); 
		$this->requisicao_modo_suporte_DATETIME = Helper::GET("requisicao_modo_suporte_DATETIME{$numReg}"); 
		$this->requisicao_usuario_id_INT = Helper::GET("requisicao_usuario_id_INT{$numReg}"); 
		$this->mobile_resposta_suporte_DATETIME = Helper::GET("mobile_resposta_suporte_DATETIME{$numReg}"); 
		$this->is_hospedeiro_BOOLEAN = Helper::GET("is_hospedeiro_BOOLEAN{$numReg}"); 
		$this->reinstalar_a_versao_id_INT = Helper::GET("reinstalar_a_versao_id_INT{$numReg}"); 
		$this->atualizacao_versao_ativa_BOOLEAN = Helper::GET("atualizacao_versao_ativa_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "mobile_id_INT = $this->mobile_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_sistema_produto_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_sistema_produto_mobile_id_INT = $this->sistema_projetos_versao_sistema_produto_mobile_id_INT, ";

	} 

	if(isset($tipo["data_cadastro_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_cadastro_DATETIME = $this->data_cadastro_DATETIME, ";

	} 

	if(isset($tipo["requisicao_modo_suporte_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "requisicao_modo_suporte_DATETIME = $this->requisicao_modo_suporte_DATETIME, ";

	} 

	if(isset($tipo["requisicao_usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "requisicao_usuario_id_INT = $this->requisicao_usuario_id_INT, ";

	} 

	if(isset($tipo["mobile_resposta_suporte_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "mobile_resposta_suporte_DATETIME = $this->mobile_resposta_suporte_DATETIME, ";

	} 

	if(isset($tipo["is_hospedeiro_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "is_hospedeiro_BOOLEAN = $this->is_hospedeiro_BOOLEAN, ";

	} 

	if(isset($tipo["reinstalar_a_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "reinstalar_a_versao_id_INT = $this->reinstalar_a_versao_id_INT, ";

	} 

	if(isset($tipo["atualizacao_versao_ativa_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "atualizacao_versao_ativa_BOOLEAN = $this->atualizacao_versao_ativa_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE mobile_identificador SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    