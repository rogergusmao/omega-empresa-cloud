<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Campo_campo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Campo_campo.php
    * TABELA MYSQL:    campo_campo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Campo_campo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $homologacao_campo_id_INT;
	public $obj;
	public $producao_campo_id_INT;
	public $objHomologacao_campo;
	public $tipo_operacao_atualizacao_banco_id_INT;
	public $objProducao_campo;
	public $lista_lista_id_INT;
	public $objTipo_operacao_atualizacao_banco;
	public $projetos_versao_banco_banco_id_INT;
	public $objLista_lista;


    public $nomeEntidade;



    

	public $label_id;
	public $label_homologacao_campo_id_INT;
	public $label_producao_campo_id_INT;
	public $label_tipo_operacao_atualizacao_banco_id_INT;
	public $label_lista_lista_id_INT;
	public $label_projetos_versao_banco_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "campo_campo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjHomologacao_campo(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Campo($this->getDatabase());
		if($this->homologacao_campo_id_INT != null) 
		$this->obj->select($this->homologacao_campo_id_INT);
	}
	return $this->obj ;
}
function getFkObjProducao_campo(){
	if($this->objHomologacao_campo ==null){
		$this->objHomologacao_campo = new EXTDAO_Campo($this->getDatabase());
		if($this->producao_campo_id_INT != null) 
		$this->objHomologacao_campo->select($this->producao_campo_id_INT);
	}
	return $this->objHomologacao_campo ;
}
function getFkObjTipo_operacao_atualizacao_banco(){
	if($this->objProducao_campo ==null){
		$this->objProducao_campo = new EXTDAO_Tipo_operacao_atualizacao_banco($this->getDatabase());
		if($this->tipo_operacao_atualizacao_banco_id_INT != null) 
		$this->objProducao_campo->select($this->tipo_operacao_atualizacao_banco_id_INT);
	}
	return $this->objProducao_campo ;
}
function getFkObjLista_lista(){
	if($this->objTipo_operacao_atualizacao_banco ==null){
		$this->objTipo_operacao_atualizacao_banco = new EXTDAO_Lista_lista($this->getDatabase());
		if($this->lista_lista_id_INT != null) 
		$this->objTipo_operacao_atualizacao_banco->select($this->lista_lista_id_INT);
	}
	return $this->objTipo_operacao_atualizacao_banco ;
}
function getFkObjProjetos_versao_banco_banco(){
	if($this->objLista_lista ==null){
		$this->objLista_lista = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->objLista_lista->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->objLista_lista ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllHomologacao_campo($objArgumentos){

		$objArgumentos->nome="homologacao_campo_id_INT";
		$objArgumentos->id="homologacao_campo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjHomologacao_campo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProducao_campo($objArgumentos){

		$objArgumentos->nome="producao_campo_id_INT";
		$objArgumentos->id="producao_campo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProducao_campo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_atualizacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllLista_lista($objArgumentos){

		$objArgumentos->nome="lista_lista_id_INT";
		$objArgumentos->id="lista_lista_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjLista_lista()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_campo_campo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_campo_campo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHomologacao_campo_id_INT()
    {
    	return $this->homologacao_campo_id_INT;
    }
    
    public function getProducao_campo_id_INT()
    {
    	return $this->producao_campo_id_INT;
    }
    
    public function getTipo_operacao_atualizacao_banco_id_INT()
    {
    	return $this->tipo_operacao_atualizacao_banco_id_INT;
    }
    
    public function getLista_lista_id_INT()
    {
    	return $this->lista_lista_id_INT;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHomologacao_campo_id_INT($val)
    {
    	$this->homologacao_campo_id_INT =  $val;
    }
    
    function setProducao_campo_id_INT($val)
    {
    	$this->producao_campo_id_INT =  $val;
    }
    
    function setTipo_operacao_atualizacao_banco_id_INT($val)
    {
    	$this->tipo_operacao_atualizacao_banco_id_INT =  $val;
    }
    
    function setLista_lista_id_INT($val)
    {
    	$this->lista_lista_id_INT =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM campo_campo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->homologacao_campo_id_INT = $row->homologacao_campo_id_INT;
        if(isset($this->objHomologacao_campo))
			$this->objHomologacao_campo->select($this->homologacao_campo_id_INT);

        $this->producao_campo_id_INT = $row->producao_campo_id_INT;
        if(isset($this->objProducao_campo))
			$this->objProducao_campo->select($this->producao_campo_id_INT);

        $this->tipo_operacao_atualizacao_banco_id_INT = $row->tipo_operacao_atualizacao_banco_id_INT;
        if(isset($this->objTipo_operacao_atualizacao_banco))
			$this->objTipo_operacao_atualizacao_banco->select($this->tipo_operacao_atualizacao_banco_id_INT);

        $this->lista_lista_id_INT = $row->lista_lista_id_INT;
        if(isset($this->objLista_lista))
			$this->objLista_lista->select($this->lista_lista_id_INT);

        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM campo_campo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	

    	$sql = "INSERT INTO campo_campo ( id , homologacao_campo_id_INT , producao_campo_id_INT , tipo_operacao_atualizacao_banco_id_INT , lista_lista_id_INT , projetos_versao_banco_banco_id_INT ) VALUES ( {$this->id} , {$this->homologacao_campo_id_INT} , {$this->producao_campo_id_INT} , {$this->tipo_operacao_atualizacao_banco_id_INT} , {$this->lista_lista_id_INT} , {$this->projetos_versao_banco_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHomologacao_campo_id_INT(){ 

		return "homologacao_campo_id_INT";

	}

	public function nomeCampoProducao_campo_id_INT(){ 

		return "producao_campo_id_INT";

	}

	public function nomeCampoTipo_operacao_atualizacao_banco_id_INT(){ 

		return "tipo_operacao_atualizacao_banco_id_INT";

	}

	public function nomeCampoLista_lista_id_INT(){ 

		return "lista_lista_id_INT";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHomologacao_campo_id_INT($objArguments){

		$objArguments->nome = "homologacao_campo_id_INT";
		$objArguments->id = "homologacao_campo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProducao_campo_id_INT($objArguments){

		$objArguments->nome = "producao_campo_id_INT";
		$objArguments->id = "producao_campo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao_atualizacao_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_atualizacao_banco_id_INT";
		$objArguments->id = "tipo_operacao_atualizacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoLista_lista_id_INT($objArguments){

		$objArguments->nome = "lista_lista_id_INT";
		$objArguments->id = "lista_lista_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->homologacao_campo_id_INT == ""){

			$this->homologacao_campo_id_INT = "null";

		}

		if($this->producao_campo_id_INT == ""){

			$this->producao_campo_id_INT = "null";

		}

		if($this->tipo_operacao_atualizacao_banco_id_INT == ""){

			$this->tipo_operacao_atualizacao_banco_id_INT = "null";

		}

		if($this->lista_lista_id_INT == ""){

			$this->lista_lista_id_INT = "null";

		}

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->homologacao_campo_id_INT = null; 
		$this->objHomologacao_campo= null;
		$this->producao_campo_id_INT = null; 
		$this->objProducao_campo= null;
		$this->tipo_operacao_atualizacao_banco_id_INT = null; 
		$this->objTipo_operacao_atualizacao_banco= null;
		$this->lista_lista_id_INT = null; 
		$this->objLista_lista= null;
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("homologacao_campo_id_INT", $this->homologacao_campo_id_INT); 
		Helper::setSession("producao_campo_id_INT", $this->producao_campo_id_INT); 
		Helper::setSession("tipo_operacao_atualizacao_banco_id_INT", $this->tipo_operacao_atualizacao_banco_id_INT); 
		Helper::setSession("lista_lista_id_INT", $this->lista_lista_id_INT); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("homologacao_campo_id_INT");
		Helper::clearSession("producao_campo_id_INT");
		Helper::clearSession("tipo_operacao_atualizacao_banco_id_INT");
		Helper::clearSession("lista_lista_id_INT");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->homologacao_campo_id_INT = Helper::SESSION("homologacao_campo_id_INT{$numReg}"); 
		$this->producao_campo_id_INT = Helper::SESSION("producao_campo_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::SESSION("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->lista_lista_id_INT = Helper::SESSION("lista_lista_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->homologacao_campo_id_INT = Helper::POST("homologacao_campo_id_INT{$numReg}"); 
		$this->producao_campo_id_INT = Helper::POST("producao_campo_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::POST("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->lista_lista_id_INT = Helper::POST("lista_lista_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->homologacao_campo_id_INT = Helper::GET("homologacao_campo_id_INT{$numReg}"); 
		$this->producao_campo_id_INT = Helper::GET("producao_campo_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::GET("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->lista_lista_id_INT = Helper::GET("lista_lista_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["homologacao_campo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "homologacao_campo_id_INT = $this->homologacao_campo_id_INT, ";

	} 

	if(isset($tipo["producao_campo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "producao_campo_id_INT = $this->producao_campo_id_INT, ";

	} 

	if(isset($tipo["tipo_operacao_atualizacao_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_atualizacao_banco_id_INT = $this->tipo_operacao_atualizacao_banco_id_INT, ";

	} 

	if(isset($tipo["lista_lista_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "lista_lista_id_INT = $this->lista_lista_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE campo_campo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    