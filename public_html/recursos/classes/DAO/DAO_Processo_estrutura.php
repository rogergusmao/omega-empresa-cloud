<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Processo_estrutura
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Processo_estrutura.php
    * TABELA MYSQL:    processo_estrutura
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Processo_estrutura extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $utiliza_copia_projetos_versao_BOOLEAN;
	public $fluxo_independente_BOOLEAN;
	public $json_constante;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_utiliza_copia_projetos_versao_BOOLEAN;
	public $label_fluxo_independente_BOOLEAN;
	public $label_json_constante;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "processo_estrutura";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_processo_estrutura", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_processo_estrutura", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getUtiliza_copia_projetos_versao_BOOLEAN()
    {
    	return $this->utiliza_copia_projetos_versao_BOOLEAN;
    }
    
    public function getFluxo_independente_BOOLEAN()
    {
    	return $this->fluxo_independente_BOOLEAN;
    }
    
    public function getJson_constante()
    {
    	return $this->json_constante;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setUtiliza_copia_projetos_versao_BOOLEAN($val)
    {
    	$this->utiliza_copia_projetos_versao_BOOLEAN =  $val;
    }
    
    function setFluxo_independente_BOOLEAN($val)
    {
    	$this->fluxo_independente_BOOLEAN =  $val;
    }
    
    function setJson_constante($val)
    {
    	$this->json_constante =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM processo_estrutura WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->utiliza_copia_projetos_versao_BOOLEAN = $row->utiliza_copia_projetos_versao_BOOLEAN;
        
        $this->fluxo_independente_BOOLEAN = $row->fluxo_independente_BOOLEAN;
        
        $this->json_constante = $row->json_constante;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM processo_estrutura WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO processo_estrutura ( nome , utiliza_copia_projetos_versao_BOOLEAN , fluxo_independente_BOOLEAN , json_constante ) VALUES ( {$this->nome} , {$this->utiliza_copia_projetos_versao_BOOLEAN} , {$this->fluxo_independente_BOOLEAN} , {$this->json_constante} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoUtiliza_copia_projetos_versao_BOOLEAN(){ 

		return "utiliza_copia_projetos_versao_BOOLEAN";

	}

	public function nomeCampoFluxo_independente_BOOLEAN(){ 

		return "fluxo_independente_BOOLEAN";

	}

	public function nomeCampoJson_constante(){ 

		return "json_constante";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUtiliza_copia_projetos_versao_BOOLEAN($objArguments){

		$objArguments->nome = "utiliza_copia_projetos_versao_BOOLEAN";
		$objArguments->id = "utiliza_copia_projetos_versao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoFluxo_independente_BOOLEAN($objArguments){

		$objArguments->nome = "fluxo_independente_BOOLEAN";
		$objArguments->id = "fluxo_independente_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoJson_constante($objArguments){

		$objArguments->nome = "json_constante";
		$objArguments->id = "json_constante";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
		if($this->utiliza_copia_projetos_versao_BOOLEAN == ""){

			$this->utiliza_copia_projetos_versao_BOOLEAN = "null";

		}

		if($this->fluxo_independente_BOOLEAN == ""){

			$this->fluxo_independente_BOOLEAN = "null";

		}

			$this->json_constante = $this->formatarDadosParaSQL($this->json_constante);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->utiliza_copia_projetos_versao_BOOLEAN = null; 
		$this->fluxo_independente_BOOLEAN = null; 
		$this->json_constante = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("utiliza_copia_projetos_versao_BOOLEAN", $this->utiliza_copia_projetos_versao_BOOLEAN); 
		Helper::setSession("fluxo_independente_BOOLEAN", $this->fluxo_independente_BOOLEAN); 
		Helper::setSession("json_constante", $this->json_constante); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("utiliza_copia_projetos_versao_BOOLEAN");
		Helper::clearSession("fluxo_independente_BOOLEAN");
		Helper::clearSession("json_constante");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->utiliza_copia_projetos_versao_BOOLEAN = Helper::SESSION("utiliza_copia_projetos_versao_BOOLEAN{$numReg}"); 
		$this->fluxo_independente_BOOLEAN = Helper::SESSION("fluxo_independente_BOOLEAN{$numReg}"); 
		$this->json_constante = Helper::SESSION("json_constante{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->utiliza_copia_projetos_versao_BOOLEAN = Helper::POST("utiliza_copia_projetos_versao_BOOLEAN{$numReg}"); 
		$this->fluxo_independente_BOOLEAN = Helper::POST("fluxo_independente_BOOLEAN{$numReg}"); 
		$this->json_constante = Helper::POST("json_constante{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->utiliza_copia_projetos_versao_BOOLEAN = Helper::GET("utiliza_copia_projetos_versao_BOOLEAN{$numReg}"); 
		$this->fluxo_independente_BOOLEAN = Helper::GET("fluxo_independente_BOOLEAN{$numReg}"); 
		$this->json_constante = Helper::GET("json_constante{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["utiliza_copia_projetos_versao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "utiliza_copia_projetos_versao_BOOLEAN = $this->utiliza_copia_projetos_versao_BOOLEAN, ";

	} 

	if(isset($tipo["fluxo_independente_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "fluxo_independente_BOOLEAN = $this->fluxo_independente_BOOLEAN, ";

	} 

	if(isset($tipo["json_constante{$numReg}"]) || $tipo == null){

		$upd.= "json_constante = $this->json_constante, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE processo_estrutura SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    