<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Uf
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Uf.php
    * TABELA MYSQL:    uf
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Uf extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $sigla;
	public $dataCadastro;
	public $dataEdicao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_sigla;
	public $label_dataCadastro;
	public $label_dataEdicao;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "uf";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_uf", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_uf", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getSigla()
    {
    	return $this->sigla;
    }
    
    public function getDataCadastro()
    {
    	return $this->dataCadastro;
    }
    
    public function getDataEdicao()
    {
    	return $this->dataEdicao;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setSigla($val)
    {
    	$this->sigla =  $val;
    }
    
    function setDataCadastro($val)
    {
    	$this->dataCadastro =  $val;
    }
    
    function setDataEdicao($val)
    {
    	$this->dataEdicao =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM uf WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->sigla = $row->sigla;
        
        $this->dataCadastro = $row->dataCadastro;
        
        $this->dataEdicao = $row->dataEdicao;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM uf WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO uf ( nome , sigla , dataCadastro , dataEdicao ) VALUES ( {$this->nome} , {$this->sigla} , NOW() , NOW() )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoSigla(){ 

		return "sigla";

	}

	public function nomeCampoDataCadastro(){ 

		return "dataCadastro";

	}

	public function nomeCampoDataEdicao(){ 

		return "dataEdicao";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSigla($objArguments){

		$objArguments->nome = "sigla";
		$objArguments->id = "sigla";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDataCadastro($objArguments){

		$objArguments->nome = "dataCadastro";
		$objArguments->id = "dataCadastro";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoDataEdicao($objArguments){

		$objArguments->nome = "dataEdicao";
		$objArguments->id = "dataEdicao";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->sigla = $this->formatarDadosParaSQL($this->sigla);


	$this->dataCadastro = $this->formatarDataTimeParaComandoSQL($this->dataCadastro); 
	$this->dataEdicao = $this->formatarDataTimeParaComandoSQL($this->dataEdicao); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->dataCadastro = $this->formatarDataTimeParaExibicao($this->dataCadastro); 
	$this->dataEdicao = $this->formatarDataTimeParaExibicao($this->dataEdicao); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->sigla = null; 
		$this->dataCadastro = null; 
		$this->dataEdicao = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("sigla", $this->sigla); 
		Helper::setSession("dataCadastro", $this->dataCadastro); 
		Helper::setSession("dataEdicao", $this->dataEdicao); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("sigla");
		Helper::clearSession("dataCadastro");
		Helper::clearSession("dataEdicao");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->sigla = Helper::SESSION("sigla{$numReg}"); 
		$this->dataCadastro = Helper::SESSION("dataCadastro{$numReg}"); 
		$this->dataEdicao = Helper::SESSION("dataEdicao{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->sigla = Helper::POST("sigla{$numReg}"); 
		$this->dataCadastro = Helper::POST("dataCadastro{$numReg}"); 
		$this->dataEdicao = Helper::POST("dataEdicao{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->sigla = Helper::GET("sigla{$numReg}"); 
		$this->dataCadastro = Helper::GET("dataCadastro{$numReg}"); 
		$this->dataEdicao = Helper::GET("dataEdicao{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["sigla{$numReg}"]) || $tipo == null){

		$upd.= "sigla = $this->sigla, ";

	} 

	if(isset($tipo["dataCadastro{$numReg}"]) || $tipo == null){

		$upd.= "dataCadastro = $this->dataCadastro, ";

	} 

		$upd.= "dataEdicao = NOW(), ";

	if(isset($tipo["dataEdicao{$numReg}"]) || $tipo == null){

		$upd.= "dataEdicao = $this->dataEdicao, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE uf SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    