<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Trunk_historico_conjunto_analise
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Trunk_historico_conjunto_analise.php
    * TABELA MYSQL:    trunk_historico_conjunto_analise
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Trunk_historico_conjunto_analise extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $trunk_historico_id_INT;
	public $obj;
	public $conjunto_analise_id_INT;
	public $objTrunk_historico;
	public $projetos_versao_id_INT;
	public $objConjunto_analise;


    public $nomeEntidade;



    

	public $label_id;
	public $label_trunk_historico_id_INT;
	public $label_conjunto_analise_id_INT;
	public $label_projetos_versao_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "trunk_historico_conjunto_analise";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTrunk_historico(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Trunk_historico($this->getDatabase());
		if($this->trunk_historico_id_INT != null) 
		$this->obj->select($this->trunk_historico_id_INT);
	}
	return $this->obj ;
}
function getFkObjConjunto_analise(){
	if($this->objTrunk_historico ==null){
		$this->objTrunk_historico = new EXTDAO_Conjunto_analise($this->getDatabase());
		if($this->conjunto_analise_id_INT != null) 
		$this->objTrunk_historico->select($this->conjunto_analise_id_INT);
	}
	return $this->objTrunk_historico ;
}
function getFkObjProjetos_versao(){
	if($this->objConjunto_analise ==null){
		$this->objConjunto_analise = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objConjunto_analise->select($this->projetos_versao_id_INT);
	}
	return $this->objConjunto_analise ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTrunk_historico($objArgumentos){

		$objArgumentos->nome="trunk_historico_id_INT";
		$objArgumentos->id="trunk_historico_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTrunk_historico()->getComboBox($objArgumentos);

	}

public function getComboBoxAllConjunto_analise($objArgumentos){

		$objArgumentos->nome="conjunto_analise_id_INT";
		$objArgumentos->id="conjunto_analise_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjConjunto_analise()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_trunk_historico_conjunto_analise", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_trunk_historico_conjunto_analise", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTrunk_historico_id_INT()
    {
    	return $this->trunk_historico_id_INT;
    }
    
    public function getConjunto_analise_id_INT()
    {
    	return $this->conjunto_analise_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTrunk_historico_id_INT($val)
    {
    	$this->trunk_historico_id_INT =  $val;
    }
    
    function setConjunto_analise_id_INT($val)
    {
    	$this->conjunto_analise_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM trunk_historico_conjunto_analise WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->trunk_historico_id_INT = $row->trunk_historico_id_INT;
        if(isset($this->objTrunk_historico))
			$this->objTrunk_historico->select($this->trunk_historico_id_INT);

        $this->conjunto_analise_id_INT = $row->conjunto_analise_id_INT;
        if(isset($this->objConjunto_analise))
			$this->objConjunto_analise->select($this->conjunto_analise_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM trunk_historico_conjunto_analise WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	

    	$sql = "INSERT INTO trunk_historico_conjunto_analise ( id , trunk_historico_id_INT , conjunto_analise_id_INT , projetos_versao_id_INT ) VALUES ( {$this->id} , {$this->trunk_historico_id_INT} , {$this->conjunto_analise_id_INT} , {$this->projetos_versao_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTrunk_historico_id_INT(){ 

		return "trunk_historico_id_INT";

	}

	public function nomeCampoConjunto_analise_id_INT(){ 

		return "conjunto_analise_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTrunk_historico_id_INT($objArguments){

		$objArguments->nome = "trunk_historico_id_INT";
		$objArguments->id = "trunk_historico_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoConjunto_analise_id_INT($objArguments){

		$objArguments->nome = "conjunto_analise_id_INT";
		$objArguments->id = "conjunto_analise_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->trunk_historico_id_INT == ""){

			$this->trunk_historico_id_INT = "null";

		}

		if($this->conjunto_analise_id_INT == ""){

			$this->conjunto_analise_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->trunk_historico_id_INT = null; 
		$this->objTrunk_historico= null;
		$this->conjunto_analise_id_INT = null; 
		$this->objConjunto_analise= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("trunk_historico_id_INT", $this->trunk_historico_id_INT); 
		Helper::setSession("conjunto_analise_id_INT", $this->conjunto_analise_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("trunk_historico_id_INT");
		Helper::clearSession("conjunto_analise_id_INT");
		Helper::clearSession("projetos_versao_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->trunk_historico_id_INT = Helper::SESSION("trunk_historico_id_INT{$numReg}"); 
		$this->conjunto_analise_id_INT = Helper::SESSION("conjunto_analise_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->trunk_historico_id_INT = Helper::POST("trunk_historico_id_INT{$numReg}"); 
		$this->conjunto_analise_id_INT = Helper::POST("conjunto_analise_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->trunk_historico_id_INT = Helper::GET("trunk_historico_id_INT{$numReg}"); 
		$this->conjunto_analise_id_INT = Helper::GET("conjunto_analise_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["trunk_historico_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "trunk_historico_id_INT = $this->trunk_historico_id_INT, ";

	} 

	if(isset($tipo["conjunto_analise_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "conjunto_analise_id_INT = $this->conjunto_analise_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE trunk_historico_conjunto_analise SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    