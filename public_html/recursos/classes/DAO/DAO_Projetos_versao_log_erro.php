<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_versao_log_erro
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_versao_log_erro.php
    * TABELA MYSQL:    projetos_versao_log_erro
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_versao_log_erro extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $classe;
	public $funcao;
	public $descricao_HTML;
	public $data_ocorrida_DATETIME;
	public $usuario_id_INT;
	public $obj;
	public $projetos_tipo_log_erro_id_INT;
	public $objUsuario;
	public $projetos_versao_banco_banco_id_INT;
	public $objProjetos_tipo_log_erro;


    public $nomeEntidade;

	public $data_ocorrida_DATETIME_UNIX;


    

	public $label_id;
	public $label_classe;
	public $label_funcao;
	public $label_descricao_HTML;
	public $label_data_ocorrida_DATETIME;
	public $label_usuario_id_INT;
	public $label_projetos_tipo_log_erro_id_INT;
	public $label_projetos_versao_banco_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_versao_log_erro";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjUsuario(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Usuario($this->getDatabase());
		if($this->usuario_id_INT != null) 
		$this->obj->select($this->usuario_id_INT);
	}
	return $this->obj ;
}
function getFkObjProjetos_tipo_log_erro(){
	if($this->objUsuario ==null){
		$this->objUsuario = new EXTDAO_Projetos_tipo_log_erro($this->getDatabase());
		if($this->projetos_tipo_log_erro_id_INT != null) 
		$this->objUsuario->select($this->projetos_tipo_log_erro_id_INT);
	}
	return $this->objUsuario ;
}
function getFkObjProjetos_versao_banco_banco(){
	if($this->objProjetos_tipo_log_erro ==null){
		$this->objProjetos_tipo_log_erro = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->objProjetos_tipo_log_erro->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->objProjetos_tipo_log_erro ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllUsuario($objArgumentos){

		$objArgumentos->nome="usuario_id_INT";
		$objArgumentos->id="usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_tipo_log_erro($objArgumentos){

		$objArgumentos->nome="projetos_tipo_log_erro_id_INT";
		$objArgumentos->id="projetos_tipo_log_erro_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_tipo_log_erro()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_versao_log_erro", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_versao_log_erro", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getClasse()
    {
    	return $this->classe;
    }
    
    public function getFuncao()
    {
    	return $this->funcao;
    }
    
    public function getDescricao_HTML()
    {
    	return $this->descricao_HTML;
    }
    
    function getData_ocorrida_DATETIME_UNIX()
    {
    	return $this->data_ocorrida_DATETIME_UNIX;
    }
    
    public function getData_ocorrida_DATETIME()
    {
    	return $this->data_ocorrida_DATETIME;
    }
    
    public function getUsuario_id_INT()
    {
    	return $this->usuario_id_INT;
    }
    
    public function getProjetos_tipo_log_erro_id_INT()
    {
    	return $this->projetos_tipo_log_erro_id_INT;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setClasse($val)
    {
    	$this->classe =  $val;
    }
    
    function setFuncao($val)
    {
    	$this->funcao =  $val;
    }
    
    function setDescricao_HTML($val)
    {
    	$this->descricao_HTML =  $val;
    }
    
    function setData_ocorrida_DATETIME($val)
    {
    	$this->data_ocorrida_DATETIME =  $val;
    }
    
    function setUsuario_id_INT($val)
    {
    	$this->usuario_id_INT =  $val;
    }
    
    function setProjetos_tipo_log_erro_id_INT($val)
    {
    	$this->projetos_tipo_log_erro_id_INT =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_ocorrida_DATETIME) AS data_ocorrida_DATETIME_UNIX FROM projetos_versao_log_erro WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->classe = $row->classe;
        
        $this->funcao = $row->funcao;
        
        $this->descricao_HTML = $row->descricao_HTML;
        
        $this->data_ocorrida_DATETIME = $row->data_ocorrida_DATETIME;
        $this->data_ocorrida_DATETIME_UNIX = $row->data_ocorrida_DATETIME_UNIX;

        $this->usuario_id_INT = $row->usuario_id_INT;
        if(isset($this->objUsuario))
			$this->objUsuario->select($this->usuario_id_INT);

        $this->projetos_tipo_log_erro_id_INT = $row->projetos_tipo_log_erro_id_INT;
        if(isset($this->objProjetos_tipo_log_erro))
			$this->objProjetos_tipo_log_erro->select($this->projetos_tipo_log_erro_id_INT);

        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_versao_log_erro WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_versao_log_erro ( classe , funcao , descricao_HTML , data_ocorrida_DATETIME , usuario_id_INT , projetos_tipo_log_erro_id_INT , projetos_versao_banco_banco_id_INT ) VALUES ( {$this->classe} , {$this->funcao} , {$this->descricao_HTML} , {$this->data_ocorrida_DATETIME} , {$this->usuario_id_INT} , {$this->projetos_tipo_log_erro_id_INT} , {$this->projetos_versao_banco_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoClasse(){ 

		return "classe";

	}

	public function nomeCampoFuncao(){ 

		return "funcao";

	}

	public function nomeCampoDescricao_HTML(){ 

		return "descricao_HTML";

	}

	public function nomeCampoData_ocorrida_DATETIME(){ 

		return "data_ocorrida_DATETIME";

	}

	public function nomeCampoUsuario_id_INT(){ 

		return "usuario_id_INT";

	}

	public function nomeCampoProjetos_tipo_log_erro_id_INT(){ 

		return "projetos_tipo_log_erro_id_INT";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoClasse($objArguments){

		$objArguments->nome = "classe";
		$objArguments->id = "classe";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoFuncao($objArguments){

		$objArguments->nome = "funcao";
		$objArguments->id = "funcao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDescricao_HTML($objArguments){

		$objArguments->nome = "descricao_HTML";
		$objArguments->id = "descricao_HTML";

	}

	public function imprimirCampoData_ocorrida_DATETIME($objArguments){

		$objArguments->nome = "data_ocorrida_DATETIME";
		$objArguments->id = "data_ocorrida_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoUsuario_id_INT($objArguments){

		$objArguments->nome = "usuario_id_INT";
		$objArguments->id = "usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_tipo_log_erro_id_INT($objArguments){

		$objArguments->nome = "projetos_tipo_log_erro_id_INT";
		$objArguments->id = "projetos_tipo_log_erro_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->classe = $this->formatarDadosParaSQL($this->classe);
			$this->funcao = $this->formatarDadosParaSQL($this->funcao);
		if($this->usuario_id_INT == ""){

			$this->usuario_id_INT = "null";

		}

		if($this->projetos_tipo_log_erro_id_INT == ""){

			$this->projetos_tipo_log_erro_id_INT = "null";

		}

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}



$this->descricao_HTML = htmlentities($this->descricao_HTML, ENT_QUOTES);
	$this->data_ocorrida_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ocorrida_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->descricao_HTML = html_entity_decode($this->descricao_HTML); 
	$this->data_ocorrida_DATETIME = $this->formatarDataTimeParaExibicao($this->data_ocorrida_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->classe = null; 
		$this->funcao = null; 
		$this->descricao_HTML = null; 
		$this->data_ocorrida_DATETIME = null; 
		$this->usuario_id_INT = null; 
		$this->objUsuario= null;
		$this->projetos_tipo_log_erro_id_INT = null; 
		$this->objProjetos_tipo_log_erro= null;
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("classe", $this->classe); 
		Helper::setSession("funcao", $this->funcao); 
		Helper::setSession("descricao_HTML", $this->descricao_HTML); 
		Helper::setSession("data_ocorrida_DATETIME", $this->data_ocorrida_DATETIME); 
		Helper::setSession("usuario_id_INT", $this->usuario_id_INT); 
		Helper::setSession("projetos_tipo_log_erro_id_INT", $this->projetos_tipo_log_erro_id_INT); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("classe");
		Helper::clearSession("funcao");
		Helper::clearSession("descricao_HTML");
		Helper::clearSession("data_ocorrida_DATETIME");
		Helper::clearSession("usuario_id_INT");
		Helper::clearSession("projetos_tipo_log_erro_id_INT");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->classe = Helper::SESSION("classe{$numReg}"); 
		$this->funcao = Helper::SESSION("funcao{$numReg}"); 
		$this->descricao_HTML = Helper::SESSION("descricao_HTML{$numReg}"); 
		$this->data_ocorrida_DATETIME = Helper::SESSION("data_ocorrida_DATETIME{$numReg}"); 
		$this->usuario_id_INT = Helper::SESSION("usuario_id_INT{$numReg}"); 
		$this->projetos_tipo_log_erro_id_INT = Helper::SESSION("projetos_tipo_log_erro_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->classe = Helper::POST("classe{$numReg}"); 
		$this->funcao = Helper::POST("funcao{$numReg}"); 
		$this->descricao_HTML = Helper::POST("descricao_HTML{$numReg}"); 
		$this->data_ocorrida_DATETIME = Helper::POST("data_ocorrida_DATETIME{$numReg}"); 
		$this->usuario_id_INT = Helper::POST("usuario_id_INT{$numReg}"); 
		$this->projetos_tipo_log_erro_id_INT = Helper::POST("projetos_tipo_log_erro_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->classe = Helper::GET("classe{$numReg}"); 
		$this->funcao = Helper::GET("funcao{$numReg}"); 
		$this->descricao_HTML = Helper::GET("descricao_HTML{$numReg}"); 
		$this->data_ocorrida_DATETIME = Helper::GET("data_ocorrida_DATETIME{$numReg}"); 
		$this->usuario_id_INT = Helper::GET("usuario_id_INT{$numReg}"); 
		$this->projetos_tipo_log_erro_id_INT = Helper::GET("projetos_tipo_log_erro_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["classe{$numReg}"]) || $tipo == null){

		$upd.= "classe = $this->classe, ";

	} 

	if(isset($tipo["funcao{$numReg}"]) || $tipo == null){

		$upd.= "funcao = $this->funcao, ";

	} 

	if(isset($tipo["descricao_HTML{$numReg}"]) || $tipo == null){

		$upd.= "descricao_HTML = $this->descricao_HTML, ";

	} 

	if(isset($tipo["data_ocorrida_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_ocorrida_DATETIME = $this->data_ocorrida_DATETIME, ";

	} 

	if(isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_id_INT = $this->usuario_id_INT, ";

	} 

	if(isset($tipo["projetos_tipo_log_erro_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_tipo_log_erro_id_INT = $this->projetos_tipo_log_erro_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_versao_log_erro SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    