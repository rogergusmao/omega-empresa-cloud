<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Permissao_sistema_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Permissao_sistema_mobile.php
    * TABELA MYSQL:    permissao_sistema_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Permissao_sistema_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $tag;
	public $pai_permissao_id_INT;
	public $obj;
	public $tipo_permissao_id_INT;
	public $objPai_permissao;
	public $icone_32_ARQUIVO;
	public $icone_64_ARQUIVO;
	public $icone_128_ARQUIVO;
	public $icone_256_ARQUIVO;
	public $projetos_versao_banco_banco_id_INT;
	public $objTipo_permissao;
	public $excluido_BOOLEAN;
	public $excluido_DATETIME;


    public $nomeEntidade;

	public $excluido_DATETIME_UNIX;


    

	public $label_id;
	public $label_nome;
	public $label_tag;
	public $label_pai_permissao_id_INT;
	public $label_tipo_permissao_id_INT;
	public $label_icone_32_ARQUIVO;
	public $label_icone_64_ARQUIVO;
	public $label_icone_128_ARQUIVO;
	public $label_icone_256_ARQUIVO;
	public $label_projetos_versao_banco_banco_id_INT;
	public $label_excluido_BOOLEAN;
	public $label_excluido_DATETIME;


	public $diretorio_icone_32_ARQUIVO;
	public $diretorio_icone_64_ARQUIVO;
	public $diretorio_icone_128_ARQUIVO;
	public $diretorio_icone_256_ARQUIVO;




    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "permissao_sistema_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjPai_permissao(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_($this->getDatabase());
		if($this->pai_permissao_id_INT != null) 
		$this->obj->select($this->pai_permissao_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_permissao(){
	if($this->objPai_permissao ==null){
		$this->objPai_permissao = new EXTDAO_Tipo_permissao($this->getDatabase());
		if($this->tipo_permissao_id_INT != null) 
		$this->objPai_permissao->select($this->tipo_permissao_id_INT);
	}
	return $this->objPai_permissao ;
}
function getFkObjProjetos_versao_banco_banco(){
	if($this->objTipo_permissao ==null){
		$this->objTipo_permissao = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->objTipo_permissao->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->objTipo_permissao ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllPai_permissao($objArgumentos){

		$objArgumentos->nome="pai_permissao_id_INT";
		$objArgumentos->id="pai_permissao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjPai_permissao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_permissao($objArgumentos){

		$objArgumentos->nome="tipo_permissao_id_INT";
		$objArgumentos->id="tipo_permissao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_permissao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Icone_32_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_32_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_32_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_32_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_64_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_64_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_64_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_64_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_128_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_128_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_128_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_128_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_256_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_256_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_256_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_256_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Icone_32_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_32_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_32_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_32_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_64_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_64_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_64_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_64_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_128_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_128_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_128_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_128_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_256_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_256_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_256_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_256_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Icone_32_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_32_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_32_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_32_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_64_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_64_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_64_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_64_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_128_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_128_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_128_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_128_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Icone_256_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_256_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_256_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_256_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_permissao_sistema_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_permissao_sistema_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMO��O DO ARQUIVO Icone_32_ARQUIVO
            $pathArquivo = $this->diretorio_icone_32_ARQUIVO . $this->getIcone_32_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Icone_64_ARQUIVO
            $pathArquivo = $this->diretorio_icone_64_ARQUIVO . $this->getIcone_64_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Icone_128_ARQUIVO
            $pathArquivo = $this->diretorio_icone_128_ARQUIVO . $this->getIcone_128_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Icone_256_ARQUIVO
            $pathArquivo = $this->diretorio_icone_256_ARQUIVO . $this->getIcone_256_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadIcone_32_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_icone_32_ARQUIVO;
            $labelCampo = $this->label_icone_32_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["icone_32_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadIcone_64_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_icone_64_ARQUIVO;
            $labelCampo = $this->label_icone_64_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["icone_64_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_2." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadIcone_128_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_icone_128_ARQUIVO;
            $labelCampo = $this->label_icone_128_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["icone_128_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_3." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadIcone_256_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_icone_256_ARQUIVO;
            $labelCampo = $this->label_icone_256_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["icone_256_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_4." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getTag()
    {
    	return $this->tag;
    }
    
    public function getPai_permissao_id_INT()
    {
    	return $this->pai_permissao_id_INT;
    }
    
    public function getTipo_permissao_id_INT()
    {
    	return $this->tipo_permissao_id_INT;
    }
    
    public function getIcone_32_ARQUIVO()
    {
    	return $this->icone_32_ARQUIVO;
    }
    
    public function getIcone_64_ARQUIVO()
    {
    	return $this->icone_64_ARQUIVO;
    }
    
    public function getIcone_128_ARQUIVO()
    {
    	return $this->icone_128_ARQUIVO;
    }
    
    public function getIcone_256_ARQUIVO()
    {
    	return $this->icone_256_ARQUIVO;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    public function getExcluido_BOOLEAN()
    {
    	return $this->excluido_BOOLEAN;
    }
    
    function getExcluido_DATETIME_UNIX()
    {
    	return $this->excluido_DATETIME_UNIX;
    }
    
    public function getExcluido_DATETIME()
    {
    	return $this->excluido_DATETIME;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setTag($val)
    {
    	$this->tag =  $val;
    }
    
    function setPai_permissao_id_INT($val)
    {
    	$this->pai_permissao_id_INT =  $val;
    }
    
    function setTipo_permissao_id_INT($val)
    {
    	$this->tipo_permissao_id_INT =  $val;
    }
    
    function setIcone_32_ARQUIVO($val)
    {
    	$this->icone_32_ARQUIVO =  $val;
    }
    
    function setIcone_64_ARQUIVO($val)
    {
    	$this->icone_64_ARQUIVO =  $val;
    }
    
    function setIcone_128_ARQUIVO($val)
    {
    	$this->icone_128_ARQUIVO =  $val;
    }
    
    function setIcone_256_ARQUIVO($val)
    {
    	$this->icone_256_ARQUIVO =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setExcluido_BOOLEAN($val)
    {
    	$this->excluido_BOOLEAN =  $val;
    }
    
    function setExcluido_DATETIME($val)
    {
    	$this->excluido_DATETIME =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM permissao_sistema_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->tag = $row->tag;
        
        $this->pai_permissao_id_INT = $row->pai_permissao_id_INT;
        if(isset($this->objPai_permissao))
			$this->objPai_permissao->select($this->pai_permissao_id_INT);

        $this->tipo_permissao_id_INT = $row->tipo_permissao_id_INT;
        if(isset($this->objTipo_permissao))
			$this->objTipo_permissao->select($this->tipo_permissao_id_INT);

        $this->icone_32_ARQUIVO = $row->icone_32_ARQUIVO;
        
        $this->icone_64_ARQUIVO = $row->icone_64_ARQUIVO;
        
        $this->icone_128_ARQUIVO = $row->icone_128_ARQUIVO;
        
        $this->icone_256_ARQUIVO = $row->icone_256_ARQUIVO;
        
        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

        $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;
        
        $this->excluido_DATETIME = $row->excluido_DATETIME;
        $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM permissao_sistema_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO permissao_sistema_mobile ( nome , tag , pai_permissao_id_INT , tipo_permissao_id_INT , icone_32_ARQUIVO , icone_64_ARQUIVO , icone_128_ARQUIVO , icone_256_ARQUIVO , projetos_versao_banco_banco_id_INT , excluido_BOOLEAN , excluido_DATETIME ) VALUES ( {$this->nome} , {$this->tag} , {$this->pai_permissao_id_INT} , {$this->tipo_permissao_id_INT} , {$this->icone_32_ARQUIVO} , {$this->icone_64_ARQUIVO} , {$this->icone_128_ARQUIVO} , {$this->icone_256_ARQUIVO} , {$this->projetos_versao_banco_banco_id_INT} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoTag(){ 

		return "tag";

	}

	public function nomeCampoPai_permissao_id_INT(){ 

		return "pai_permissao_id_INT";

	}

	public function nomeCampoTipo_permissao_id_INT(){ 

		return "tipo_permissao_id_INT";

	}

	public function nomeCampoIcone_32_ARQUIVO(){ 

		return "icone_32_ARQUIVO";

	}

	public function nomeCampoIcone_64_ARQUIVO(){ 

		return "icone_64_ARQUIVO";

	}

	public function nomeCampoIcone_128_ARQUIVO(){ 

		return "icone_128_ARQUIVO";

	}

	public function nomeCampoIcone_256_ARQUIVO(){ 

		return "icone_256_ARQUIVO";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoExcluido_BOOLEAN(){ 

		return "excluido_BOOLEAN";

	}

	public function nomeCampoExcluido_DATETIME(){ 

		return "excluido_DATETIME";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTag($objArguments){

		$objArguments->nome = "tag";
		$objArguments->id = "tag";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoPai_permissao_id_INT($objArguments){

		$objArguments->nome = "pai_permissao_id_INT";
		$objArguments->id = "pai_permissao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_permissao_id_INT($objArguments){

		$objArguments->nome = "tipo_permissao_id_INT";
		$objArguments->id = "tipo_permissao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoIcone_32_ARQUIVO($objArguments){

		$objArguments->nome = "icone_32_ARQUIVO";
		$objArguments->id = "icone_32_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoIcone_64_ARQUIVO($objArguments){

		$objArguments->nome = "icone_64_ARQUIVO";
		$objArguments->id = "icone_64_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoIcone_128_ARQUIVO($objArguments){

		$objArguments->nome = "icone_128_ARQUIVO";
		$objArguments->id = "icone_128_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoIcone_256_ARQUIVO($objArguments){

		$objArguments->nome = "icone_256_ARQUIVO";
		$objArguments->id = "icone_256_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoExcluido_BOOLEAN($objArguments){

		$objArguments->nome = "excluido_BOOLEAN";
		$objArguments->id = "excluido_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoExcluido_DATETIME($objArguments){

		$objArguments->nome = "excluido_DATETIME";
		$objArguments->id = "excluido_DATETIME";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->tag = $this->formatarDadosParaSQL($this->tag);
		if($this->pai_permissao_id_INT == ""){

			$this->pai_permissao_id_INT = "null";

		}

		if($this->tipo_permissao_id_INT == ""){

			$this->tipo_permissao_id_INT = "null";

		}

			$this->icone_32_ARQUIVO = $this->formatarDadosParaSQL($this->icone_32_ARQUIVO);
			$this->icone_64_ARQUIVO = $this->formatarDadosParaSQL($this->icone_64_ARQUIVO);
			$this->icone_128_ARQUIVO = $this->formatarDadosParaSQL($this->icone_128_ARQUIVO);
			$this->icone_256_ARQUIVO = $this->formatarDadosParaSQL($this->icone_256_ARQUIVO);
		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->excluido_BOOLEAN == ""){

			$this->excluido_BOOLEAN = "null";

		}



	$this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->tag = null; 
		$this->pai_permissao_id_INT = null; 
		$this->objPai_permissao= null;
		$this->tipo_permissao_id_INT = null; 
		$this->objTipo_permissao= null;
		$this->icone_32_ARQUIVO = null; 
		$this->icone_64_ARQUIVO = null; 
		$this->icone_128_ARQUIVO = null; 
		$this->icone_256_ARQUIVO = null; 
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;
		$this->excluido_BOOLEAN = null; 
		$this->excluido_DATETIME = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("tag", $this->tag); 
		Helper::setSession("pai_permissao_id_INT", $this->pai_permissao_id_INT); 
		Helper::setSession("tipo_permissao_id_INT", $this->tipo_permissao_id_INT); 
		Helper::setSession("icone_32_ARQUIVO", $this->icone_32_ARQUIVO); 
		Helper::setSession("icone_64_ARQUIVO", $this->icone_64_ARQUIVO); 
		Helper::setSession("icone_128_ARQUIVO", $this->icone_128_ARQUIVO); 
		Helper::setSession("icone_256_ARQUIVO", $this->icone_256_ARQUIVO); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 
		Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN); 
		Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("tag");
		Helper::clearSession("pai_permissao_id_INT");
		Helper::clearSession("tipo_permissao_id_INT");
		Helper::clearSession("icone_32_ARQUIVO");
		Helper::clearSession("icone_64_ARQUIVO");
		Helper::clearSession("icone_128_ARQUIVO");
		Helper::clearSession("icone_256_ARQUIVO");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");
		Helper::clearSession("excluido_BOOLEAN");
		Helper::clearSession("excluido_DATETIME");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->tag = Helper::SESSION("tag{$numReg}"); 
		$this->pai_permissao_id_INT = Helper::SESSION("pai_permissao_id_INT{$numReg}"); 
		$this->tipo_permissao_id_INT = Helper::SESSION("tipo_permissao_id_INT{$numReg}"); 
		$this->icone_32_ARQUIVO = Helper::SESSION("icone_32_ARQUIVO{$numReg}"); 
		$this->icone_64_ARQUIVO = Helper::SESSION("icone_64_ARQUIVO{$numReg}"); 
		$this->icone_128_ARQUIVO = Helper::SESSION("icone_128_ARQUIVO{$numReg}"); 
		$this->icone_256_ARQUIVO = Helper::SESSION("icone_256_ARQUIVO{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->excluido_BOOLEAN = Helper::SESSION("excluido_BOOLEAN{$numReg}"); 
		$this->excluido_DATETIME = Helper::SESSION("excluido_DATETIME{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->tag = Helper::POST("tag{$numReg}"); 
		$this->pai_permissao_id_INT = Helper::POST("pai_permissao_id_INT{$numReg}"); 
		$this->tipo_permissao_id_INT = Helper::POST("tipo_permissao_id_INT{$numReg}"); 
		$this->icone_32_ARQUIVO = Helper::POST("icone_32_ARQUIVO{$numReg}"); 
		$this->icone_64_ARQUIVO = Helper::POST("icone_64_ARQUIVO{$numReg}"); 
		$this->icone_128_ARQUIVO = Helper::POST("icone_128_ARQUIVO{$numReg}"); 
		$this->icone_256_ARQUIVO = Helper::POST("icone_256_ARQUIVO{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->excluido_BOOLEAN = Helper::POST("excluido_BOOLEAN{$numReg}"); 
		$this->excluido_DATETIME = Helper::POST("excluido_DATETIME{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->tag = Helper::GET("tag{$numReg}"); 
		$this->pai_permissao_id_INT = Helper::GET("pai_permissao_id_INT{$numReg}"); 
		$this->tipo_permissao_id_INT = Helper::GET("tipo_permissao_id_INT{$numReg}"); 
		$this->icone_32_ARQUIVO = Helper::GET("icone_32_ARQUIVO{$numReg}"); 
		$this->icone_64_ARQUIVO = Helper::GET("icone_64_ARQUIVO{$numReg}"); 
		$this->icone_128_ARQUIVO = Helper::GET("icone_128_ARQUIVO{$numReg}"); 
		$this->icone_256_ARQUIVO = Helper::GET("icone_256_ARQUIVO{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->excluido_BOOLEAN = Helper::GET("excluido_BOOLEAN{$numReg}"); 
		$this->excluido_DATETIME = Helper::GET("excluido_DATETIME{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["tag{$numReg}"]) || $tipo == null){

		$upd.= "tag = $this->tag, ";

	} 

	if(isset($tipo["pai_permissao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "pai_permissao_id_INT = $this->pai_permissao_id_INT, ";

	} 

	if(isset($tipo["tipo_permissao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_permissao_id_INT = $this->tipo_permissao_id_INT, ";

	} 

	if(isset($tipo["icone_32_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "icone_32_ARQUIVO = $this->icone_32_ARQUIVO, ";

	} 

	if(isset($tipo["icone_64_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "icone_64_ARQUIVO = $this->icone_64_ARQUIVO, ";

	} 

	if(isset($tipo["icone_128_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "icone_128_ARQUIVO = $this->icone_128_ARQUIVO, ";

	} 

	if(isset($tipo["icone_256_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "icone_256_ARQUIVO = $this->icone_256_ARQUIVO, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";

	} 

	if(isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "excluido_DATETIME = $this->excluido_DATETIME, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE permissao_sistema_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    