<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tabela_chave
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Tabela_chave.php
    * TABELA MYSQL:    tabela_chave
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Tabela_chave extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $index_type;
	public $index_method;
	public $tabela_id_INT;
	public $obj;
	public $non_unique_BOOLEAN;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_index_type;
	public $label_index_method;
	public $label_tabela_id_INT;
	public $label_non_unique_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "tabela_chave";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTabela(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tabela($this->getDatabase());
		if($this->tabela_id_INT != null) 
		$this->obj->select($this->tabela_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTabela($objArgumentos){

		$objArgumentos->nome="tabela_id_INT";
		$objArgumentos->id="tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_tabela_chave", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tabela_chave", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getIndex_type()
    {
    	return $this->index_type;
    }
    
    public function getIndex_method()
    {
    	return $this->index_method;
    }
    
    public function getTabela_id_INT()
    {
    	return $this->tabela_id_INT;
    }
    
    public function getNon_unique_BOOLEAN()
    {
    	return $this->non_unique_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setIndex_type($val)
    {
    	$this->index_type =  $val;
    }
    
    function setIndex_method($val)
    {
    	$this->index_method =  $val;
    }
    
    function setTabela_id_INT($val)
    {
    	$this->tabela_id_INT =  $val;
    }
    
    function setNon_unique_BOOLEAN($val)
    {
    	$this->non_unique_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM tabela_chave WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->index_type = $row->index_type;
        
        $this->index_method = $row->index_method;
        
        $this->tabela_id_INT = $row->tabela_id_INT;
        if(isset($this->objTabela))
			$this->objTabela->select($this->tabela_id_INT);

        $this->non_unique_BOOLEAN = $row->non_unique_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM tabela_chave WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO tabela_chave ( nome , index_type , index_method , tabela_id_INT , non_unique_BOOLEAN ) VALUES ( {$this->nome} , {$this->index_type} , {$this->index_method} , {$this->tabela_id_INT} , {$this->non_unique_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoIndex_type(){ 

		return "index_type";

	}

	public function nomeCampoIndex_method(){ 

		return "index_method";

	}

	public function nomeCampoTabela_id_INT(){ 

		return "tabela_id_INT";

	}

	public function nomeCampoNon_unique_BOOLEAN(){ 

		return "non_unique_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoIndex_type($objArguments){

		$objArguments->nome = "index_type";
		$objArguments->id = "index_type";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoIndex_method($objArguments){

		$objArguments->nome = "index_method";
		$objArguments->id = "index_method";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTabela_id_INT($objArguments){

		$objArguments->nome = "tabela_id_INT";
		$objArguments->id = "tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoNon_unique_BOOLEAN($objArguments){

		$objArguments->nome = "non_unique_BOOLEAN";
		$objArguments->id = "non_unique_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->index_type = $this->formatarDadosParaSQL($this->index_type);
			$this->index_method = $this->formatarDadosParaSQL($this->index_method);
		if($this->tabela_id_INT == ""){

			$this->tabela_id_INT = "null";

		}

		if($this->non_unique_BOOLEAN == ""){

			$this->non_unique_BOOLEAN = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->index_type = null; 
		$this->index_method = null; 
		$this->tabela_id_INT = null; 
		$this->objTabela= null;
		$this->non_unique_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("index_type", $this->index_type); 
		Helper::setSession("index_method", $this->index_method); 
		Helper::setSession("tabela_id_INT", $this->tabela_id_INT); 
		Helper::setSession("non_unique_BOOLEAN", $this->non_unique_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("index_type");
		Helper::clearSession("index_method");
		Helper::clearSession("tabela_id_INT");
		Helper::clearSession("non_unique_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->index_type = Helper::SESSION("index_type{$numReg}"); 
		$this->index_method = Helper::SESSION("index_method{$numReg}"); 
		$this->tabela_id_INT = Helper::SESSION("tabela_id_INT{$numReg}"); 
		$this->non_unique_BOOLEAN = Helper::SESSION("non_unique_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->index_type = Helper::POST("index_type{$numReg}"); 
		$this->index_method = Helper::POST("index_method{$numReg}"); 
		$this->tabela_id_INT = Helper::POST("tabela_id_INT{$numReg}"); 
		$this->non_unique_BOOLEAN = Helper::POST("non_unique_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->index_type = Helper::GET("index_type{$numReg}"); 
		$this->index_method = Helper::GET("index_method{$numReg}"); 
		$this->tabela_id_INT = Helper::GET("tabela_id_INT{$numReg}"); 
		$this->non_unique_BOOLEAN = Helper::GET("non_unique_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["index_type{$numReg}"]) || $tipo == null){

		$upd.= "index_type = $this->index_type, ";

	} 

	if(isset($tipo["index_method{$numReg}"]) || $tipo == null){

		$upd.= "index_method = $this->index_method, ";

	} 

	if(isset($tipo["tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_id_INT = $this->tabela_id_INT, ";

	} 

	if(isset($tipo["non_unique_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "non_unique_BOOLEAN = $this->non_unique_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE tabela_chave SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    