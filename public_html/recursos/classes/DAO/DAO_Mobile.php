<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Mobile.php
    * TABELA MYSQL:    mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $identificador;
	public $imei;
	public $modelo;
	public $marca;
	public $cpu;


    public $nomeEntidade;



    

	public $label_id;
	public $label_identificador;
	public $label_imei;
	public $label_modelo;
	public $label_marca;
	public $label_cpu;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getIdentificador()
    {
    	return $this->identificador;
    }
    
    public function getImei()
    {
    	return $this->imei;
    }
    
    public function getModelo()
    {
    	return $this->modelo;
    }
    
    public function getMarca()
    {
    	return $this->marca;
    }
    
    public function getCpu()
    {
    	return $this->cpu;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setIdentificador($val)
    {
    	$this->identificador =  $val;
    }
    
    function setImei($val)
    {
    	$this->imei =  $val;
    }
    
    function setModelo($val)
    {
    	$this->modelo =  $val;
    }
    
    function setMarca($val)
    {
    	$this->marca =  $val;
    }
    
    function setCpu($val)
    {
    	$this->cpu =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->identificador = $row->identificador;
        
        $this->imei = $row->imei;
        
        $this->modelo = $row->modelo;
        
        $this->marca = $row->marca;
        
        $this->cpu = $row->cpu;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO mobile ( identificador , imei , modelo , marca , cpu ) VALUES ( {$this->identificador} , {$this->imei} , {$this->modelo} , {$this->marca} , {$this->cpu} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoIdentificador(){ 

		return "identificador";

	}

	public function nomeCampoImei(){ 

		return "imei";

	}

	public function nomeCampoModelo(){ 

		return "modelo";

	}

	public function nomeCampoMarca(){ 

		return "marca";

	}

	public function nomeCampoCpu(){ 

		return "cpu";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoIdentificador($objArguments){

		$objArguments->nome = "identificador";
		$objArguments->id = "identificador";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoImei($objArguments){

		$objArguments->nome = "imei";
		$objArguments->id = "imei";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoModelo($objArguments){

		$objArguments->nome = "modelo";
		$objArguments->id = "modelo";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoMarca($objArguments){

		$objArguments->nome = "marca";
		$objArguments->id = "marca";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoCpu($objArguments){

		$objArguments->nome = "cpu";
		$objArguments->id = "cpu";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->identificador = $this->formatarDadosParaSQL($this->identificador);
			$this->imei = $this->formatarDadosParaSQL($this->imei);
			$this->modelo = $this->formatarDadosParaSQL($this->modelo);
			$this->marca = $this->formatarDadosParaSQL($this->marca);
			$this->cpu = $this->formatarDadosParaSQL($this->cpu);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->identificador = null; 
		$this->imei = null; 
		$this->modelo = null; 
		$this->marca = null; 
		$this->cpu = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("identificador", $this->identificador); 
		Helper::setSession("imei", $this->imei); 
		Helper::setSession("modelo", $this->modelo); 
		Helper::setSession("marca", $this->marca); 
		Helper::setSession("cpu", $this->cpu); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("identificador");
		Helper::clearSession("imei");
		Helper::clearSession("modelo");
		Helper::clearSession("marca");
		Helper::clearSession("cpu");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->identificador = Helper::SESSION("identificador{$numReg}"); 
		$this->imei = Helper::SESSION("imei{$numReg}"); 
		$this->modelo = Helper::SESSION("modelo{$numReg}"); 
		$this->marca = Helper::SESSION("marca{$numReg}"); 
		$this->cpu = Helper::SESSION("cpu{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->identificador = Helper::POST("identificador{$numReg}"); 
		$this->imei = Helper::POST("imei{$numReg}"); 
		$this->modelo = Helper::POST("modelo{$numReg}"); 
		$this->marca = Helper::POST("marca{$numReg}"); 
		$this->cpu = Helper::POST("cpu{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->identificador = Helper::GET("identificador{$numReg}"); 
		$this->imei = Helper::GET("imei{$numReg}"); 
		$this->modelo = Helper::GET("modelo{$numReg}"); 
		$this->marca = Helper::GET("marca{$numReg}"); 
		$this->cpu = Helper::GET("cpu{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["identificador{$numReg}"]) || $tipo == null){

		$upd.= "identificador = $this->identificador, ";

	} 

	if(isset($tipo["imei{$numReg}"]) || $tipo == null){

		$upd.= "imei = $this->imei, ";

	} 

	if(isset($tipo["modelo{$numReg}"]) || $tipo == null){

		$upd.= "modelo = $this->modelo, ";

	} 

	if(isset($tipo["marca{$numReg}"]) || $tipo == null){

		$upd.= "marca = $this->marca, ";

	} 

	if(isset($tipo["cpu{$numReg}"]) || $tipo == null){

		$upd.= "cpu = $this->cpu, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    