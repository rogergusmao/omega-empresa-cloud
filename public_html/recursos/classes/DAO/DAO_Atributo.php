<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Atributo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Atributo.php
    * TABELA MYSQL:    atributo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Atributo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $nome_exibicao;
	public $tabela_id_INT;
	public $obj;
	public $tipo_sql;
	public $tipo_sql_ficticio;
	public $tamanho_INT;
	public $decimal_INT;
	public $not_null_BOOLEAN;
	public $primary_key_BOOLEAN;
	public $auto_increment_BOOLEAN;
	public $valor_default;
	public $fk_tabela_id_INT;
	public $objTabela;
	public $fk_atributo_id_INT;
	public $objFk_tabela;
	public $fk_nome;
	public $update_tipo_fk;
	public $delete_tipo_fk;
	public $label;
	public $unique_BOOLEAN;
	public $unique_nome;
	public $banco_id_INT;
	public $objFk_atributo;
	public $projetos_versao_id_INT;
	public $objBanco;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_nome_exibicao;
	public $label_tabela_id_INT;
	public $label_tipo_sql;
	public $label_tipo_sql_ficticio;
	public $label_tamanho_INT;
	public $label_decimal_INT;
	public $label_not_null_BOOLEAN;
	public $label_primary_key_BOOLEAN;
	public $label_auto_increment_BOOLEAN;
	public $label_valor_default;
	public $label_fk_tabela_id_INT;
	public $label_fk_atributo_id_INT;
	public $label_fk_nome;
	public $label_update_tipo_fk;
	public $label_delete_tipo_fk;
	public $label_label;
	public $label_unique_BOOLEAN;
	public $label_unique_nome;
	public $label_banco_id_INT;
	public $label_projetos_versao_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "atributo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTabela(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tabela($this->getDatabase());
		if($this->tabela_id_INT != null) 
		$this->obj->select($this->tabela_id_INT);
	}
	return $this->obj ;
}
function getFkObjFk_tabela(){
	if($this->objTabela ==null){
		$this->objTabela = new EXTDAO_Tabela($this->getDatabase());
		if($this->fk_tabela_id_INT != null) 
		$this->objTabela->select($this->fk_tabela_id_INT);
	}
	return $this->objTabela ;
}
function getFkObjFk_atributo(){
	if($this->objFk_tabela ==null){
		$this->objFk_tabela = new EXTDAO_Atributo($this->getDatabase());
		if($this->fk_atributo_id_INT != null) 
		$this->objFk_tabela->select($this->fk_atributo_id_INT);
	}
	return $this->objFk_tabela ;
}
function getFkObjBanco(){
	if($this->objFk_atributo ==null){
		$this->objFk_atributo = new EXTDAO_Banco($this->getDatabase());
		if($this->banco_id_INT != null) 
		$this->objFk_atributo->select($this->banco_id_INT);
	}
	return $this->objFk_atributo ;
}
function getFkObjProjetos_versao(){
	if($this->objBanco ==null){
		$this->objBanco = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objBanco->select($this->projetos_versao_id_INT);
	}
	return $this->objBanco ;
}


    public function valorCampoLabel(){

    	return $this->getNome();

    }

    

        public function getComboBoxAllTabela($objArgumentos){

		$objArgumentos->nome="tabela_id_INT";
		$objArgumentos->id="tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllFk_tabela($objArgumentos){

		$objArgumentos->nome="fk_tabela_id_INT";
		$objArgumentos->id="fk_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjFk_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllFk_atributo($objArgumentos){

		$objArgumentos->nome="fk_atributo_id_INT";
		$objArgumentos->id="fk_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjFk_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllBanco($objArgumentos){

		$objArgumentos->nome="banco_id_INT";
		$objArgumentos->id="banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjBanco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_atributo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_atributo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getNome_exibicao()
    {
    	return $this->nome_exibicao;
    }
    
    public function getTabela_id_INT()
    {
    	return $this->tabela_id_INT;
    }
    
    public function getTipo_sql()
    {
    	return $this->tipo_sql;
    }
    
    public function getTipo_sql_ficticio()
    {
    	return $this->tipo_sql_ficticio;
    }
    
    public function getTamanho_INT()
    {
    	return $this->tamanho_INT;
    }
    
    public function getDecimal_INT()
    {
    	return $this->decimal_INT;
    }
    
    public function getNot_null_BOOLEAN()
    {
    	return $this->not_null_BOOLEAN;
    }
    
    public function getPrimary_key_BOOLEAN()
    {
    	return $this->primary_key_BOOLEAN;
    }
    
    public function getAuto_increment_BOOLEAN()
    {
    	return $this->auto_increment_BOOLEAN;
    }
    
    public function getValor_default()
    {
    	return $this->valor_default;
    }
    
    public function getFk_tabela_id_INT()
    {
    	return $this->fk_tabela_id_INT;
    }
    
    public function getFk_atributo_id_INT()
    {
    	return $this->fk_atributo_id_INT;
    }
    
    public function getFk_nome()
    {
    	return $this->fk_nome;
    }
    
    public function getUpdate_tipo_fk()
    {
    	return $this->update_tipo_fk;
    }
    
    public function getDelete_tipo_fk()
    {
    	return $this->delete_tipo_fk;
    }
    
    public function getLabel()
    {
    	return $this->label;
    }
    
    public function getUnique_BOOLEAN()
    {
    	return $this->unique_BOOLEAN;
    }
    
    public function getUnique_nome()
    {
    	return $this->unique_nome;
    }
    
    public function getBanco_id_INT()
    {
    	return $this->banco_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setNome_exibicao($val)
    {
    	$this->nome_exibicao =  $val;
    }
    
    function setTabela_id_INT($val)
    {
    	$this->tabela_id_INT =  $val;
    }
    
    function setTipo_sql($val)
    {
    	$this->tipo_sql =  $val;
    }
    
    function setTipo_sql_ficticio($val)
    {
    	$this->tipo_sql_ficticio =  $val;
    }
    
    function setTamanho_INT($val)
    {
    	$this->tamanho_INT =  $val;
    }
    
    function setDecimal_INT($val)
    {
    	$this->decimal_INT =  $val;
    }
    
    function setNot_null_BOOLEAN($val)
    {
    	$this->not_null_BOOLEAN =  $val;
    }
    
    function setPrimary_key_BOOLEAN($val)
    {
    	$this->primary_key_BOOLEAN =  $val;
    }
    
    function setAuto_increment_BOOLEAN($val)
    {
    	$this->auto_increment_BOOLEAN =  $val;
    }
    
    function setValor_default($val)
    {
    	$this->valor_default =  $val;
    }
    
    function setFk_tabela_id_INT($val)
    {
    	$this->fk_tabela_id_INT =  $val;
    }
    
    function setFk_atributo_id_INT($val)
    {
    	$this->fk_atributo_id_INT =  $val;
    }
    
    function setFk_nome($val)
    {
    	$this->fk_nome =  $val;
    }
    
    function setUpdate_tipo_fk($val)
    {
    	$this->update_tipo_fk =  $val;
    }
    
    function setDelete_tipo_fk($val)
    {
    	$this->delete_tipo_fk =  $val;
    }
    
    function setLabel($val)
    {
    	$this->label =  $val;
    }
    
    function setUnique_BOOLEAN($val)
    {
    	$this->unique_BOOLEAN =  $val;
    }
    
    function setUnique_nome($val)
    {
    	$this->unique_nome =  $val;
    }
    
    function setBanco_id_INT($val)
    {
    	$this->banco_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM atributo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->nome_exibicao = $row->nome_exibicao;
        
        $this->tabela_id_INT = $row->tabela_id_INT;
        if(isset($this->objTabela))
			$this->objTabela->select($this->tabela_id_INT);

        $this->tipo_sql = $row->tipo_sql;
        
        $this->tipo_sql_ficticio = $row->tipo_sql_ficticio;
        
        $this->tamanho_INT = $row->tamanho_INT;
        
        $this->decimal_INT = $row->decimal_INT;
        
        $this->not_null_BOOLEAN = $row->not_null_BOOLEAN;
        
        $this->primary_key_BOOLEAN = $row->primary_key_BOOLEAN;
        
        $this->auto_increment_BOOLEAN = $row->auto_increment_BOOLEAN;
        
        $this->valor_default = $row->valor_default;
        
        $this->fk_tabela_id_INT = $row->fk_tabela_id_INT;
        if(isset($this->objFk_tabela))
			$this->objFk_tabela->select($this->fk_tabela_id_INT);

        $this->fk_atributo_id_INT = $row->fk_atributo_id_INT;
        if(isset($this->objFk_atributo))
			$this->objFk_atributo->select($this->fk_atributo_id_INT);

        $this->fk_nome = $row->fk_nome;
        
        $this->update_tipo_fk = $row->update_tipo_fk;
        
        $this->delete_tipo_fk = $row->delete_tipo_fk;
        
        $this->label = $row->label;
        
        $this->unique_BOOLEAN = $row->unique_BOOLEAN;
        
        $this->unique_nome = $row->unique_nome;
        
        $this->banco_id_INT = $row->banco_id_INT;
        if(isset($this->objBanco))
			$this->objBanco->select($this->banco_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM atributo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO atributo ( nome , nome_exibicao , tabela_id_INT , tipo_sql , tipo_sql_ficticio , tamanho_INT , decimal_INT , not_null_BOOLEAN , primary_key_BOOLEAN , auto_increment_BOOLEAN , valor_default , fk_tabela_id_INT , fk_atributo_id_INT , fk_nome , update_tipo_fk , delete_tipo_fk , label , unique_BOOLEAN , unique_nome , banco_id_INT , projetos_versao_id_INT ) VALUES ( {$this->nome} , {$this->nome_exibicao} , {$this->tabela_id_INT} , {$this->tipo_sql} , {$this->tipo_sql_ficticio} , {$this->tamanho_INT} , {$this->decimal_INT} , {$this->not_null_BOOLEAN} , {$this->primary_key_BOOLEAN} , {$this->auto_increment_BOOLEAN} , {$this->valor_default} , {$this->fk_tabela_id_INT} , {$this->fk_atributo_id_INT} , {$this->fk_nome} , {$this->update_tipo_fk} , {$this->delete_tipo_fk} , {$this->label} , {$this->unique_BOOLEAN} , {$this->unique_nome} , {$this->banco_id_INT} , {$this->projetos_versao_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoNome_exibicao(){ 

		return "nome_exibicao";

	}

	public function nomeCampoTabela_id_INT(){ 

		return "tabela_id_INT";

	}

	public function nomeCampoTipo_sql(){ 

		return "tipo_sql";

	}

	public function nomeCampoTipo_sql_ficticio(){ 

		return "tipo_sql_ficticio";

	}

	public function nomeCampoTamanho_INT(){ 

		return "tamanho_INT";

	}

	public function nomeCampoDecimal_INT(){ 

		return "decimal_INT";

	}

	public function nomeCampoNot_null_BOOLEAN(){ 

		return "not_null_BOOLEAN";

	}

	public function nomeCampoPrimary_key_BOOLEAN(){ 

		return "primary_key_BOOLEAN";

	}

	public function nomeCampoAuto_increment_BOOLEAN(){ 

		return "auto_increment_BOOLEAN";

	}

	public function nomeCampoValor_default(){ 

		return "valor_default";

	}

	public function nomeCampoFk_tabela_id_INT(){ 

		return "fk_tabela_id_INT";

	}

	public function nomeCampoFk_atributo_id_INT(){ 

		return "fk_atributo_id_INT";

	}

	public function nomeCampoFk_nome(){ 

		return "fk_nome";

	}

	public function nomeCampoUpdate_tipo_fk(){ 

		return "update_tipo_fk";

	}

	public function nomeCampoDelete_tipo_fk(){ 

		return "delete_tipo_fk";

	}

	public function nomeCampoLabel(){ 

		return "label";

	}

	public function nomeCampoUnique_BOOLEAN(){ 

		return "unique_BOOLEAN";

	}

	public function nomeCampoUnique_nome(){ 

		return "unique_nome";

	}

	public function nomeCampoBanco_id_INT(){ 

		return "banco_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoNome_exibicao($objArguments){

		$objArguments->nome = "nome_exibicao";
		$objArguments->id = "nome_exibicao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTabela_id_INT($objArguments){

		$objArguments->nome = "tabela_id_INT";
		$objArguments->id = "tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_sql($objArguments){

		$objArguments->nome = "tipo_sql";
		$objArguments->id = "tipo_sql";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTipo_sql_ficticio($objArguments){

		$objArguments->nome = "tipo_sql_ficticio";
		$objArguments->id = "tipo_sql_ficticio";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTamanho_INT($objArguments){

		$objArguments->nome = "tamanho_INT";
		$objArguments->id = "tamanho_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoDecimal_INT($objArguments){

		$objArguments->nome = "decimal_INT";
		$objArguments->id = "decimal_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoNot_null_BOOLEAN($objArguments){

		$objArguments->nome = "not_null_BOOLEAN";
		$objArguments->id = "not_null_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoPrimary_key_BOOLEAN($objArguments){

		$objArguments->nome = "primary_key_BOOLEAN";
		$objArguments->id = "primary_key_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoAuto_increment_BOOLEAN($objArguments){

		$objArguments->nome = "auto_increment_BOOLEAN";
		$objArguments->id = "auto_increment_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoValor_default($objArguments){

		$objArguments->nome = "valor_default";
		$objArguments->id = "valor_default";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoFk_tabela_id_INT($objArguments){

		$objArguments->nome = "fk_tabela_id_INT";
		$objArguments->id = "fk_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoFk_atributo_id_INT($objArguments){

		$objArguments->nome = "fk_atributo_id_INT";
		$objArguments->id = "fk_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoFk_nome($objArguments){

		$objArguments->nome = "fk_nome";
		$objArguments->id = "fk_nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUpdate_tipo_fk($objArguments){

		$objArguments->nome = "update_tipo_fk";
		$objArguments->id = "update_tipo_fk";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDelete_tipo_fk($objArguments){

		$objArguments->nome = "delete_tipo_fk";
		$objArguments->id = "delete_tipo_fk";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoLabel($objArguments){

		$objArguments->nome = "label";
		$objArguments->id = "label";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUnique_BOOLEAN($objArguments){

		$objArguments->nome = "unique_BOOLEAN";
		$objArguments->id = "unique_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoUnique_nome($objArguments){

		$objArguments->nome = "unique_nome";
		$objArguments->id = "unique_nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoBanco_id_INT($objArguments){

		$objArguments->nome = "banco_id_INT";
		$objArguments->id = "banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->nome_exibicao = $this->formatarDadosParaSQL($this->nome_exibicao);
		if($this->tabela_id_INT == ""){

			$this->tabela_id_INT = "null";

		}

			$this->tipo_sql = $this->formatarDadosParaSQL($this->tipo_sql);
			$this->tipo_sql_ficticio = $this->formatarDadosParaSQL($this->tipo_sql_ficticio);
		if($this->tamanho_INT == ""){

			$this->tamanho_INT = "null";

		}

		if($this->decimal_INT == ""){

			$this->decimal_INT = "null";

		}

		if($this->not_null_BOOLEAN == ""){

			$this->not_null_BOOLEAN = "null";

		}

		if($this->primary_key_BOOLEAN == ""){

			$this->primary_key_BOOLEAN = "null";

		}

		if($this->auto_increment_BOOLEAN == ""){

			$this->auto_increment_BOOLEAN = "null";

		}

			$this->valor_default = $this->formatarDadosParaSQL($this->valor_default);
		if($this->fk_tabela_id_INT == ""){

			$this->fk_tabela_id_INT = "null";

		}

		if($this->fk_atributo_id_INT == ""){

			$this->fk_atributo_id_INT = "null";

		}

			$this->fk_nome = $this->formatarDadosParaSQL($this->fk_nome);
			$this->update_tipo_fk = $this->formatarDadosParaSQL($this->update_tipo_fk);
			$this->delete_tipo_fk = $this->formatarDadosParaSQL($this->delete_tipo_fk);
			$this->label = $this->formatarDadosParaSQL($this->label);
		if($this->unique_BOOLEAN == ""){

			$this->unique_BOOLEAN = "null";

		}

			$this->unique_nome = $this->formatarDadosParaSQL($this->unique_nome);
		if($this->banco_id_INT == ""){

			$this->banco_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->nome_exibicao = null; 
		$this->tabela_id_INT = null; 
		$this->objTabela= null;
		$this->tipo_sql = null; 
		$this->tipo_sql_ficticio = null; 
		$this->tamanho_INT = null; 
		$this->decimal_INT = null; 
		$this->not_null_BOOLEAN = null; 
		$this->primary_key_BOOLEAN = null; 
		$this->auto_increment_BOOLEAN = null; 
		$this->valor_default = null; 
		$this->fk_tabela_id_INT = null; 
		$this->objFk_tabela= null;
		$this->fk_atributo_id_INT = null; 
		$this->objFk_atributo= null;
		$this->fk_nome = null; 
		$this->update_tipo_fk = null; 
		$this->delete_tipo_fk = null; 
		$this->label = null; 
		$this->unique_BOOLEAN = null; 
		$this->unique_nome = null; 
		$this->banco_id_INT = null; 
		$this->objBanco= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("nome_exibicao", $this->nome_exibicao); 
		Helper::setSession("tabela_id_INT", $this->tabela_id_INT); 
		Helper::setSession("tipo_sql", $this->tipo_sql); 
		Helper::setSession("tipo_sql_ficticio", $this->tipo_sql_ficticio); 
		Helper::setSession("tamanho_INT", $this->tamanho_INT); 
		Helper::setSession("decimal_INT", $this->decimal_INT); 
		Helper::setSession("not_null_BOOLEAN", $this->not_null_BOOLEAN); 
		Helper::setSession("primary_key_BOOLEAN", $this->primary_key_BOOLEAN); 
		Helper::setSession("auto_increment_BOOLEAN", $this->auto_increment_BOOLEAN); 
		Helper::setSession("valor_default", $this->valor_default); 
		Helper::setSession("fk_tabela_id_INT", $this->fk_tabela_id_INT); 
		Helper::setSession("fk_atributo_id_INT", $this->fk_atributo_id_INT); 
		Helper::setSession("fk_nome", $this->fk_nome); 
		Helper::setSession("update_tipo_fk", $this->update_tipo_fk); 
		Helper::setSession("delete_tipo_fk", $this->delete_tipo_fk); 
		Helper::setSession("label", $this->label); 
		Helper::setSession("unique_BOOLEAN", $this->unique_BOOLEAN); 
		Helper::setSession("unique_nome", $this->unique_nome); 
		Helper::setSession("banco_id_INT", $this->banco_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("nome_exibicao");
		Helper::clearSession("tabela_id_INT");
		Helper::clearSession("tipo_sql");
		Helper::clearSession("tipo_sql_ficticio");
		Helper::clearSession("tamanho_INT");
		Helper::clearSession("decimal_INT");
		Helper::clearSession("not_null_BOOLEAN");
		Helper::clearSession("primary_key_BOOLEAN");
		Helper::clearSession("auto_increment_BOOLEAN");
		Helper::clearSession("valor_default");
		Helper::clearSession("fk_tabela_id_INT");
		Helper::clearSession("fk_atributo_id_INT");
		Helper::clearSession("fk_nome");
		Helper::clearSession("update_tipo_fk");
		Helper::clearSession("delete_tipo_fk");
		Helper::clearSession("label");
		Helper::clearSession("unique_BOOLEAN");
		Helper::clearSession("unique_nome");
		Helper::clearSession("banco_id_INT");
		Helper::clearSession("projetos_versao_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->nome_exibicao = Helper::SESSION("nome_exibicao{$numReg}"); 
		$this->tabela_id_INT = Helper::SESSION("tabela_id_INT{$numReg}"); 
		$this->tipo_sql = Helper::SESSION("tipo_sql{$numReg}"); 
		$this->tipo_sql_ficticio = Helper::SESSION("tipo_sql_ficticio{$numReg}"); 
		$this->tamanho_INT = Helper::SESSION("tamanho_INT{$numReg}"); 
		$this->decimal_INT = Helper::SESSION("decimal_INT{$numReg}"); 
		$this->not_null_BOOLEAN = Helper::SESSION("not_null_BOOLEAN{$numReg}"); 
		$this->primary_key_BOOLEAN = Helper::SESSION("primary_key_BOOLEAN{$numReg}"); 
		$this->auto_increment_BOOLEAN = Helper::SESSION("auto_increment_BOOLEAN{$numReg}"); 
		$this->valor_default = Helper::SESSION("valor_default{$numReg}"); 
		$this->fk_tabela_id_INT = Helper::SESSION("fk_tabela_id_INT{$numReg}"); 
		$this->fk_atributo_id_INT = Helper::SESSION("fk_atributo_id_INT{$numReg}"); 
		$this->fk_nome = Helper::SESSION("fk_nome{$numReg}"); 
		$this->update_tipo_fk = Helper::SESSION("update_tipo_fk{$numReg}"); 
		$this->delete_tipo_fk = Helper::SESSION("delete_tipo_fk{$numReg}"); 
		$this->label = Helper::SESSION("label{$numReg}"); 
		$this->unique_BOOLEAN = Helper::SESSION("unique_BOOLEAN{$numReg}"); 
		$this->unique_nome = Helper::SESSION("unique_nome{$numReg}"); 
		$this->banco_id_INT = Helper::SESSION("banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->nome_exibicao = Helper::POST("nome_exibicao{$numReg}"); 
		$this->tabela_id_INT = Helper::POST("tabela_id_INT{$numReg}"); 
		$this->tipo_sql = Helper::POST("tipo_sql{$numReg}"); 
		$this->tipo_sql_ficticio = Helper::POST("tipo_sql_ficticio{$numReg}"); 
		$this->tamanho_INT = Helper::POST("tamanho_INT{$numReg}"); 
		$this->decimal_INT = Helper::POST("decimal_INT{$numReg}"); 
		$this->not_null_BOOLEAN = Helper::POST("not_null_BOOLEAN{$numReg}"); 
		$this->primary_key_BOOLEAN = Helper::POST("primary_key_BOOLEAN{$numReg}"); 
		$this->auto_increment_BOOLEAN = Helper::POST("auto_increment_BOOLEAN{$numReg}"); 
		$this->valor_default = Helper::POST("valor_default{$numReg}"); 
		$this->fk_tabela_id_INT = Helper::POST("fk_tabela_id_INT{$numReg}"); 
		$this->fk_atributo_id_INT = Helper::POST("fk_atributo_id_INT{$numReg}"); 
		$this->fk_nome = Helper::POST("fk_nome{$numReg}"); 
		$this->update_tipo_fk = Helper::POST("update_tipo_fk{$numReg}"); 
		$this->delete_tipo_fk = Helper::POST("delete_tipo_fk{$numReg}"); 
		$this->label = Helper::POST("label{$numReg}"); 
		$this->unique_BOOLEAN = Helper::POST("unique_BOOLEAN{$numReg}"); 
		$this->unique_nome = Helper::POST("unique_nome{$numReg}"); 
		$this->banco_id_INT = Helper::POST("banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->nome_exibicao = Helper::GET("nome_exibicao{$numReg}"); 
		$this->tabela_id_INT = Helper::GET("tabela_id_INT{$numReg}"); 
		$this->tipo_sql = Helper::GET("tipo_sql{$numReg}"); 
		$this->tipo_sql_ficticio = Helper::GET("tipo_sql_ficticio{$numReg}"); 
		$this->tamanho_INT = Helper::GET("tamanho_INT{$numReg}"); 
		$this->decimal_INT = Helper::GET("decimal_INT{$numReg}"); 
		$this->not_null_BOOLEAN = Helper::GET("not_null_BOOLEAN{$numReg}"); 
		$this->primary_key_BOOLEAN = Helper::GET("primary_key_BOOLEAN{$numReg}"); 
		$this->auto_increment_BOOLEAN = Helper::GET("auto_increment_BOOLEAN{$numReg}"); 
		$this->valor_default = Helper::GET("valor_default{$numReg}"); 
		$this->fk_tabela_id_INT = Helper::GET("fk_tabela_id_INT{$numReg}"); 
		$this->fk_atributo_id_INT = Helper::GET("fk_atributo_id_INT{$numReg}"); 
		$this->fk_nome = Helper::GET("fk_nome{$numReg}"); 
		$this->update_tipo_fk = Helper::GET("update_tipo_fk{$numReg}"); 
		$this->delete_tipo_fk = Helper::GET("delete_tipo_fk{$numReg}"); 
		$this->label = Helper::GET("label{$numReg}"); 
		$this->unique_BOOLEAN = Helper::GET("unique_BOOLEAN{$numReg}"); 
		$this->unique_nome = Helper::GET("unique_nome{$numReg}"); 
		$this->banco_id_INT = Helper::GET("banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["nome_exibicao{$numReg}"]) || $tipo == null){

		$upd.= "nome_exibicao = $this->nome_exibicao, ";

	} 

	if(isset($tipo["tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_id_INT = $this->tabela_id_INT, ";

	} 

	if(isset($tipo["tipo_sql{$numReg}"]) || $tipo == null){

		$upd.= "tipo_sql = $this->tipo_sql, ";

	} 

	if(isset($tipo["tipo_sql_ficticio{$numReg}"]) || $tipo == null){

		$upd.= "tipo_sql_ficticio = $this->tipo_sql_ficticio, ";

	} 

	if(isset($tipo["tamanho_INT{$numReg}"]) || $tipo == null){

		$upd.= "tamanho_INT = $this->tamanho_INT, ";

	} 

	if(isset($tipo["decimal_INT{$numReg}"]) || $tipo == null){

		$upd.= "decimal_INT = $this->decimal_INT, ";

	} 

	if(isset($tipo["not_null_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "not_null_BOOLEAN = $this->not_null_BOOLEAN, ";

	} 

	if(isset($tipo["primary_key_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "primary_key_BOOLEAN = $this->primary_key_BOOLEAN, ";

	} 

	if(isset($tipo["auto_increment_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "auto_increment_BOOLEAN = $this->auto_increment_BOOLEAN, ";

	} 

	if(isset($tipo["valor_default{$numReg}"]) || $tipo == null){

		$upd.= "valor_default = $this->valor_default, ";

	} 

	if(isset($tipo["fk_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "fk_tabela_id_INT = $this->fk_tabela_id_INT, ";

	} 

	if(isset($tipo["fk_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "fk_atributo_id_INT = $this->fk_atributo_id_INT, ";

	} 

	if(isset($tipo["fk_nome{$numReg}"]) || $tipo == null){

		$upd.= "fk_nome = $this->fk_nome, ";

	} 

	if(isset($tipo["update_tipo_fk{$numReg}"]) || $tipo == null){

		$upd.= "update_tipo_fk = $this->update_tipo_fk, ";

	} 

	if(isset($tipo["delete_tipo_fk{$numReg}"]) || $tipo == null){

		$upd.= "delete_tipo_fk = $this->delete_tipo_fk, ";

	} 

	if(isset($tipo["label{$numReg}"]) || $tipo == null){

		$upd.= "label = $this->label, ";

	} 

	if(isset($tipo["unique_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "unique_BOOLEAN = $this->unique_BOOLEAN, ";

	} 

	if(isset($tipo["unique_nome{$numReg}"]) || $tipo == null){

		$upd.= "unique_nome = $this->unique_nome, ";

	} 

	if(isset($tipo["banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "banco_id_INT = $this->banco_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE atributo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    