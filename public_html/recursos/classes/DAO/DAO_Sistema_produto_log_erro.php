<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_produto_log_erro
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_produto_log_erro.php
    * TABELA MYSQL:    sistema_produto_log_erro
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_produto_log_erro extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $classe;
	public $funcao;
	public $linha_INT;
	public $nome_arquivo;
	public $identificador_erro;
	public $descricao;
	public $stacktrace;
	public $data_visualizacao_DATETIME;
	public $data_ocorrida_DATETIME;
	public $sistema_tipo_log_erro_id_INT;
	public $obj;
	public $sistema_projetos_versao_produto_id_INT;
	public $objSistema_tipo_log_erro;
	public $id_usuario_INT;
	public $id_corporacao_INT;
	public $sistema_log_identificador_id_INT;
	public $objSistema_projetos_versao_produto;
	public $sistema_projetos_versao_id_INT;
	public $objSistema_log_identificador;
	public $mobile_conectado_id_INT;
	public $objSistema_projetos_versao;
	public $operacao_sistema_mobile_id_INT;
	public $objMobile_conectado;
	public $script_comando_banco_id_INT;
	public $objOperacao_sistema_mobile;
	public $total_ocorrencia_INT;
	public $sincronizado_BOOLEAN;


    public $nomeEntidade;

	public $data_visualizacao_DATETIME_UNIX;
	public $data_ocorrida_DATETIME_UNIX;


    

	public $label_id;
	public $label_classe;
	public $label_funcao;
	public $label_linha_INT;
	public $label_nome_arquivo;
	public $label_identificador_erro;
	public $label_descricao;
	public $label_stacktrace;
	public $label_data_visualizacao_DATETIME;
	public $label_data_ocorrida_DATETIME;
	public $label_sistema_tipo_log_erro_id_INT;
	public $label_sistema_projetos_versao_produto_id_INT;
	public $label_id_usuario_INT;
	public $label_id_corporacao_INT;
	public $label_sistema_log_identificador_id_INT;
	public $label_sistema_projetos_versao_id_INT;
	public $label_mobile_conectado_id_INT;
	public $label_operacao_sistema_mobile_id_INT;
	public $label_script_comando_banco_id_INT;
	public $label_total_ocorrencia_INT;
	public $label_sincronizado_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_produto_log_erro";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_tipo_log_erro(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_tipo_log_erro($this->getDatabase());
		if($this->sistema_tipo_log_erro_id_INT != null) 
		$this->obj->select($this->sistema_tipo_log_erro_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_projetos_versao_produto(){
	if($this->objSistema_tipo_log_erro ==null){
		$this->objSistema_tipo_log_erro = new EXTDAO_Sistema_projetos_versao_produto($this->getDatabase());
		if($this->sistema_projetos_versao_produto_id_INT != null) 
		$this->objSistema_tipo_log_erro->select($this->sistema_projetos_versao_produto_id_INT);
	}
	return $this->objSistema_tipo_log_erro ;
}
function getFkObjSistema_log_identificador(){
	if($this->objSistema_projetos_versao_produto ==null){
		$this->objSistema_projetos_versao_produto = new EXTDAO_Sistema_log_identificador($this->getDatabase());
		if($this->sistema_log_identificador_id_INT != null) 
		$this->objSistema_projetos_versao_produto->select($this->sistema_log_identificador_id_INT);
	}
	return $this->objSistema_projetos_versao_produto ;
}
function getFkObjSistema_projetos_versao(){
	if($this->objSistema_log_identificador ==null){
		$this->objSistema_log_identificador = new EXTDAO_Sistema_projetos_versao($this->getDatabase());
		if($this->sistema_projetos_versao_id_INT != null) 
		$this->objSistema_log_identificador->select($this->sistema_projetos_versao_id_INT);
	}
	return $this->objSistema_log_identificador ;
}
function getFkObjMobile_conectado(){
	if($this->objSistema_projetos_versao ==null){
		$this->objSistema_projetos_versao = new EXTDAO_Mobile_conectado($this->getDatabase());
		if($this->mobile_conectado_id_INT != null) 
		$this->objSistema_projetos_versao->select($this->mobile_conectado_id_INT);
	}
	return $this->objSistema_projetos_versao ;
}
function getFkObjOperacao_sistema_mobile(){
	if($this->objMobile_conectado ==null){
		$this->objMobile_conectado = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->objMobile_conectado->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->objMobile_conectado ;
}
function getFkObjScript_comando_banco(){
	if($this->objOperacao_sistema_mobile ==null){
		$this->objOperacao_sistema_mobile = new EXTDAO_Script_comando_banco($this->getDatabase());
		if($this->script_comando_banco_id_INT != null) 
		$this->objOperacao_sistema_mobile->select($this->script_comando_banco_id_INT);
	}
	return $this->objOperacao_sistema_mobile ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_tipo_log_erro($objArgumentos){

		$objArgumentos->nome="sistema_tipo_log_erro_id_INT";
		$objArgumentos->id="sistema_tipo_log_erro_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_tipo_log_erro()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao_produto($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->id="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao_produto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_log_identificador($objArgumentos){

		$objArgumentos->nome="sistema_log_identificador_id_INT";
		$objArgumentos->id="sistema_log_identificador_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_log_identificador()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllMobile_conectado($objArgumentos){

		$objArgumentos->nome="mobile_conectado_id_INT";
		$objArgumentos->id="mobile_conectado_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjMobile_conectado()->getComboBox($objArgumentos);

	}

public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllScript_comando_banco($objArgumentos){

		$objArgumentos->nome="script_comando_banco_id_INT";
		$objArgumentos->id="script_comando_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjScript_comando_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_produto_log_erro", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_produto_log_erro", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getClasse()
    {
    	return $this->classe;
    }
    
    public function getFuncao()
    {
    	return $this->funcao;
    }
    
    public function getLinha_INT()
    {
    	return $this->linha_INT;
    }
    
    public function getNome_arquivo()
    {
    	return $this->nome_arquivo;
    }
    
    public function getIdentificador_erro()
    {
    	return $this->identificador_erro;
    }
    
    public function getDescricao()
    {
    	return $this->descricao;
    }
    
    public function getStacktrace()
    {
    	return $this->stacktrace;
    }
    
    function getData_visualizacao_DATETIME_UNIX()
    {
    	return $this->data_visualizacao_DATETIME_UNIX;
    }
    
    public function getData_visualizacao_DATETIME()
    {
    	return $this->data_visualizacao_DATETIME;
    }
    
    function getData_ocorrida_DATETIME_UNIX()
    {
    	return $this->data_ocorrida_DATETIME_UNIX;
    }
    
    public function getData_ocorrida_DATETIME()
    {
    	return $this->data_ocorrida_DATETIME;
    }
    
    public function getSistema_tipo_log_erro_id_INT()
    {
    	return $this->sistema_tipo_log_erro_id_INT;
    }
    
    public function getSistema_projetos_versao_produto_id_INT()
    {
    	return $this->sistema_projetos_versao_produto_id_INT;
    }
    
    public function getId_usuario_INT()
    {
    	return $this->id_usuario_INT;
    }
    
    public function getId_corporacao_INT()
    {
    	return $this->id_corporacao_INT;
    }
    
    public function getSistema_log_identificador_id_INT()
    {
    	return $this->sistema_log_identificador_id_INT;
    }
    
    public function getSistema_projetos_versao_id_INT()
    {
    	return $this->sistema_projetos_versao_id_INT;
    }
    
    public function getMobile_conectado_id_INT()
    {
    	return $this->mobile_conectado_id_INT;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    public function getScript_comando_banco_id_INT()
    {
    	return $this->script_comando_banco_id_INT;
    }
    
    public function getTotal_ocorrencia_INT()
    {
    	return $this->total_ocorrencia_INT;
    }
    
    public function getSincronizado_BOOLEAN()
    {
    	return $this->sincronizado_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setClasse($val)
    {
    	$this->classe =  $val;
    }
    
    function setFuncao($val)
    {
    	$this->funcao =  $val;
    }
    
    function setLinha_INT($val)
    {
    	$this->linha_INT =  $val;
    }
    
    function setNome_arquivo($val)
    {
    	$this->nome_arquivo =  $val;
    }
    
    function setIdentificador_erro($val)
    {
    	$this->identificador_erro =  $val;
    }
    
    function setDescricao($val)
    {
    	$this->descricao =  $val;
    }
    
    function setStacktrace($val)
    {
    	$this->stacktrace =  $val;
    }
    
    function setData_visualizacao_DATETIME($val)
    {
    	$this->data_visualizacao_DATETIME =  $val;
    }
    
    function setData_ocorrida_DATETIME($val)
    {
    	$this->data_ocorrida_DATETIME =  $val;
    }
    
    function setSistema_tipo_log_erro_id_INT($val)
    {
    	$this->sistema_tipo_log_erro_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_produto_id_INT($val)
    {
    	$this->sistema_projetos_versao_produto_id_INT =  $val;
    }
    
    function setId_usuario_INT($val)
    {
    	$this->id_usuario_INT =  $val;
    }
    
    function setId_corporacao_INT($val)
    {
    	$this->id_corporacao_INT =  $val;
    }
    
    function setSistema_log_identificador_id_INT($val)
    {
    	$this->sistema_log_identificador_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_id_INT($val)
    {
    	$this->sistema_projetos_versao_id_INT =  $val;
    }
    
    function setMobile_conectado_id_INT($val)
    {
    	$this->mobile_conectado_id_INT =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setScript_comando_banco_id_INT($val)
    {
    	$this->script_comando_banco_id_INT =  $val;
    }
    
    function setTotal_ocorrencia_INT($val)
    {
    	$this->total_ocorrencia_INT =  $val;
    }
    
    function setSincronizado_BOOLEAN($val)
    {
    	$this->sincronizado_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_visualizacao_DATETIME) AS data_visualizacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_ocorrida_DATETIME) AS data_ocorrida_DATETIME_UNIX FROM sistema_produto_log_erro WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->classe = $row->classe;
        
        $this->funcao = $row->funcao;
        
        $this->linha_INT = $row->linha_INT;
        
        $this->nome_arquivo = $row->nome_arquivo;
        
        $this->identificador_erro = $row->identificador_erro;
        
        $this->descricao = $row->descricao;
        
        $this->stacktrace = $row->stacktrace;
        
        $this->data_visualizacao_DATETIME = $row->data_visualizacao_DATETIME;
        $this->data_visualizacao_DATETIME_UNIX = $row->data_visualizacao_DATETIME_UNIX;

        $this->data_ocorrida_DATETIME = $row->data_ocorrida_DATETIME;
        $this->data_ocorrida_DATETIME_UNIX = $row->data_ocorrida_DATETIME_UNIX;

        $this->sistema_tipo_log_erro_id_INT = $row->sistema_tipo_log_erro_id_INT;
        if(isset($this->objSistema_tipo_log_erro))
			$this->objSistema_tipo_log_erro->select($this->sistema_tipo_log_erro_id_INT);

        $this->sistema_projetos_versao_produto_id_INT = $row->sistema_projetos_versao_produto_id_INT;
        if(isset($this->objSistema_projetos_versao_produto))
			$this->objSistema_projetos_versao_produto->select($this->sistema_projetos_versao_produto_id_INT);

        $this->id_usuario_INT = $row->id_usuario_INT;
        
        $this->id_corporacao_INT = $row->id_corporacao_INT;
        
        $this->sistema_log_identificador_id_INT = $row->sistema_log_identificador_id_INT;
        if(isset($this->objSistema_log_identificador))
			$this->objSistema_log_identificador->select($this->sistema_log_identificador_id_INT);

        $this->sistema_projetos_versao_id_INT = $row->sistema_projetos_versao_id_INT;
        if(isset($this->objSistema_projetos_versao))
			$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_id_INT);

        $this->mobile_conectado_id_INT = $row->mobile_conectado_id_INT;
        if(isset($this->objMobile_conectado))
			$this->objMobile_conectado->select($this->mobile_conectado_id_INT);

        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

        $this->script_comando_banco_id_INT = $row->script_comando_banco_id_INT;
        if(isset($this->objScript_comando_banco))
			$this->objScript_comando_banco->select($this->script_comando_banco_id_INT);

        $this->total_ocorrencia_INT = $row->total_ocorrencia_INT;
        
        $this->sincronizado_BOOLEAN = $row->sincronizado_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_produto_log_erro WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_produto_log_erro ( classe , funcao , linha_INT , nome_arquivo , identificador_erro , descricao , stacktrace , data_visualizacao_DATETIME , data_ocorrida_DATETIME , sistema_tipo_log_erro_id_INT , sistema_projetos_versao_produto_id_INT , id_usuario_INT , id_corporacao_INT , sistema_log_identificador_id_INT , sistema_projetos_versao_id_INT , mobile_conectado_id_INT , operacao_sistema_mobile_id_INT , script_comando_banco_id_INT , total_ocorrencia_INT , sincronizado_BOOLEAN ) VALUES ( {$this->classe} , {$this->funcao} , {$this->linha_INT} , {$this->nome_arquivo} , {$this->identificador_erro} , {$this->descricao} , {$this->stacktrace} , {$this->data_visualizacao_DATETIME} , {$this->data_ocorrida_DATETIME} , {$this->sistema_tipo_log_erro_id_INT} , {$this->sistema_projetos_versao_produto_id_INT} , {$this->id_usuario_INT} , {$this->id_corporacao_INT} , {$this->sistema_log_identificador_id_INT} , {$this->sistema_projetos_versao_id_INT} , {$this->mobile_conectado_id_INT} , {$this->operacao_sistema_mobile_id_INT} , {$this->script_comando_banco_id_INT} , {$this->total_ocorrencia_INT} , {$this->sincronizado_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoClasse(){ 

		return "classe";

	}

	public function nomeCampoFuncao(){ 

		return "funcao";

	}

	public function nomeCampoLinha_INT(){ 

		return "linha_INT";

	}

	public function nomeCampoNome_arquivo(){ 

		return "nome_arquivo";

	}

	public function nomeCampoIdentificador_erro(){ 

		return "identificador_erro";

	}

	public function nomeCampoDescricao(){ 

		return "descricao";

	}

	public function nomeCampoStacktrace(){ 

		return "stacktrace";

	}

	public function nomeCampoData_visualizacao_DATETIME(){ 

		return "data_visualizacao_DATETIME";

	}

	public function nomeCampoData_ocorrida_DATETIME(){ 

		return "data_ocorrida_DATETIME";

	}

	public function nomeCampoSistema_tipo_log_erro_id_INT(){ 

		return "sistema_tipo_log_erro_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_produto_id_INT(){ 

		return "sistema_projetos_versao_produto_id_INT";

	}

	public function nomeCampoId_usuario_INT(){ 

		return "id_usuario_INT";

	}

	public function nomeCampoId_corporacao_INT(){ 

		return "id_corporacao_INT";

	}

	public function nomeCampoSistema_log_identificador_id_INT(){ 

		return "sistema_log_identificador_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_id_INT(){ 

		return "sistema_projetos_versao_id_INT";

	}

	public function nomeCampoMobile_conectado_id_INT(){ 

		return "mobile_conectado_id_INT";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoScript_comando_banco_id_INT(){ 

		return "script_comando_banco_id_INT";

	}

	public function nomeCampoTotal_ocorrencia_INT(){ 

		return "total_ocorrencia_INT";

	}

	public function nomeCampoSincronizado_BOOLEAN(){ 

		return "sincronizado_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoClasse($objArguments){

		$objArguments->nome = "classe";
		$objArguments->id = "classe";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoFuncao($objArguments){

		$objArguments->nome = "funcao";
		$objArguments->id = "funcao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoLinha_INT($objArguments){

		$objArguments->nome = "linha_INT";
		$objArguments->id = "linha_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoNome_arquivo($objArguments){

		$objArguments->nome = "nome_arquivo";
		$objArguments->id = "nome_arquivo";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoIdentificador_erro($objArguments){

		$objArguments->nome = "identificador_erro";
		$objArguments->id = "identificador_erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDescricao($objArguments){

		$objArguments->nome = "descricao";
		$objArguments->id = "descricao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoStacktrace($objArguments){

		$objArguments->nome = "stacktrace";
		$objArguments->id = "stacktrace";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoData_visualizacao_DATETIME($objArguments){

		$objArguments->nome = "data_visualizacao_DATETIME";
		$objArguments->id = "data_visualizacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_ocorrida_DATETIME($objArguments){

		$objArguments->nome = "data_ocorrida_DATETIME";
		$objArguments->id = "data_ocorrida_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoSistema_tipo_log_erro_id_INT($objArguments){

		$objArguments->nome = "sistema_tipo_log_erro_id_INT";
		$objArguments->id = "sistema_tipo_log_erro_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_produto_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_produto_id_INT";
		$objArguments->id = "sistema_projetos_versao_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_usuario_INT($objArguments){

		$objArguments->nome = "id_usuario_INT";
		$objArguments->id = "id_usuario_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_corporacao_INT($objArguments){

		$objArguments->nome = "id_corporacao_INT";
		$objArguments->id = "id_corporacao_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_log_identificador_id_INT($objArguments){

		$objArguments->nome = "sistema_log_identificador_id_INT";
		$objArguments->id = "sistema_log_identificador_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_id_INT";
		$objArguments->id = "sistema_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMobile_conectado_id_INT($objArguments){

		$objArguments->nome = "mobile_conectado_id_INT";
		$objArguments->id = "mobile_conectado_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoScript_comando_banco_id_INT($objArguments){

		$objArguments->nome = "script_comando_banco_id_INT";
		$objArguments->id = "script_comando_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTotal_ocorrencia_INT($objArguments){

		$objArguments->nome = "total_ocorrencia_INT";
		$objArguments->id = "total_ocorrencia_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSincronizado_BOOLEAN($objArguments){

		$objArguments->nome = "sincronizado_BOOLEAN";
		$objArguments->id = "sincronizado_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->classe = $this->formatarDadosParaSQL($this->classe);
			$this->funcao = $this->formatarDadosParaSQL($this->funcao);
		if($this->linha_INT == ""){

			$this->linha_INT = "null";

		}

			$this->nome_arquivo = $this->formatarDadosParaSQL($this->nome_arquivo);
			$this->identificador_erro = $this->formatarDadosParaSQL($this->identificador_erro);
			$this->descricao = $this->formatarDadosParaSQL($this->descricao);
			$this->stacktrace = $this->formatarDadosParaSQL($this->stacktrace);
		if($this->sistema_tipo_log_erro_id_INT == ""){

			$this->sistema_tipo_log_erro_id_INT = "null";

		}

		if($this->sistema_projetos_versao_produto_id_INT == ""){

			$this->sistema_projetos_versao_produto_id_INT = "null";

		}

		if($this->id_usuario_INT == ""){

			$this->id_usuario_INT = "null";

		}

		if($this->id_corporacao_INT == ""){

			$this->id_corporacao_INT = "null";

		}

		if($this->sistema_log_identificador_id_INT == ""){

			$this->sistema_log_identificador_id_INT = "null";

		}

		if($this->sistema_projetos_versao_id_INT == ""){

			$this->sistema_projetos_versao_id_INT = "null";

		}

		if($this->mobile_conectado_id_INT == ""){

			$this->mobile_conectado_id_INT = "null";

		}

		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}

		if($this->script_comando_banco_id_INT == ""){

			$this->script_comando_banco_id_INT = "null";

		}

		if($this->total_ocorrencia_INT == ""){

			$this->total_ocorrencia_INT = "null";

		}

		if($this->sincronizado_BOOLEAN == ""){

			$this->sincronizado_BOOLEAN = "null";

		}



	$this->data_visualizacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_visualizacao_DATETIME); 
	$this->data_ocorrida_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ocorrida_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_visualizacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_visualizacao_DATETIME); 
	$this->data_ocorrida_DATETIME = $this->formatarDataTimeParaExibicao($this->data_ocorrida_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->classe = null; 
		$this->funcao = null; 
		$this->linha_INT = null; 
		$this->nome_arquivo = null; 
		$this->identificador_erro = null; 
		$this->descricao = null; 
		$this->stacktrace = null; 
		$this->data_visualizacao_DATETIME = null; 
		$this->data_ocorrida_DATETIME = null; 
		$this->sistema_tipo_log_erro_id_INT = null; 
		$this->objSistema_tipo_log_erro= null;
		$this->sistema_projetos_versao_produto_id_INT = null; 
		$this->objSistema_projetos_versao_produto= null;
		$this->id_usuario_INT = null; 
		$this->id_corporacao_INT = null; 
		$this->sistema_log_identificador_id_INT = null; 
		$this->objSistema_log_identificador= null;
		$this->sistema_projetos_versao_id_INT = null; 
		$this->objSistema_projetos_versao= null;
		$this->mobile_conectado_id_INT = null; 
		$this->objMobile_conectado= null;
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;
		$this->script_comando_banco_id_INT = null; 
		$this->objScript_comando_banco= null;
		$this->total_ocorrencia_INT = null; 
		$this->sincronizado_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("classe", $this->classe); 
		Helper::setSession("funcao", $this->funcao); 
		Helper::setSession("linha_INT", $this->linha_INT); 
		Helper::setSession("nome_arquivo", $this->nome_arquivo); 
		Helper::setSession("identificador_erro", $this->identificador_erro); 
		Helper::setSession("descricao", $this->descricao); 
		Helper::setSession("stacktrace", $this->stacktrace); 
		Helper::setSession("data_visualizacao_DATETIME", $this->data_visualizacao_DATETIME); 
		Helper::setSession("data_ocorrida_DATETIME", $this->data_ocorrida_DATETIME); 
		Helper::setSession("sistema_tipo_log_erro_id_INT", $this->sistema_tipo_log_erro_id_INT); 
		Helper::setSession("sistema_projetos_versao_produto_id_INT", $this->sistema_projetos_versao_produto_id_INT); 
		Helper::setSession("id_usuario_INT", $this->id_usuario_INT); 
		Helper::setSession("id_corporacao_INT", $this->id_corporacao_INT); 
		Helper::setSession("sistema_log_identificador_id_INT", $this->sistema_log_identificador_id_INT); 
		Helper::setSession("sistema_projetos_versao_id_INT", $this->sistema_projetos_versao_id_INT); 
		Helper::setSession("mobile_conectado_id_INT", $this->mobile_conectado_id_INT); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 
		Helper::setSession("script_comando_banco_id_INT", $this->script_comando_banco_id_INT); 
		Helper::setSession("total_ocorrencia_INT", $this->total_ocorrencia_INT); 
		Helper::setSession("sincronizado_BOOLEAN", $this->sincronizado_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("classe");
		Helper::clearSession("funcao");
		Helper::clearSession("linha_INT");
		Helper::clearSession("nome_arquivo");
		Helper::clearSession("identificador_erro");
		Helper::clearSession("descricao");
		Helper::clearSession("stacktrace");
		Helper::clearSession("data_visualizacao_DATETIME");
		Helper::clearSession("data_ocorrida_DATETIME");
		Helper::clearSession("sistema_tipo_log_erro_id_INT");
		Helper::clearSession("sistema_projetos_versao_produto_id_INT");
		Helper::clearSession("id_usuario_INT");
		Helper::clearSession("id_corporacao_INT");
		Helper::clearSession("sistema_log_identificador_id_INT");
		Helper::clearSession("sistema_projetos_versao_id_INT");
		Helper::clearSession("mobile_conectado_id_INT");
		Helper::clearSession("operacao_sistema_mobile_id_INT");
		Helper::clearSession("script_comando_banco_id_INT");
		Helper::clearSession("total_ocorrencia_INT");
		Helper::clearSession("sincronizado_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->classe = Helper::SESSION("classe{$numReg}"); 
		$this->funcao = Helper::SESSION("funcao{$numReg}"); 
		$this->linha_INT = Helper::SESSION("linha_INT{$numReg}"); 
		$this->nome_arquivo = Helper::SESSION("nome_arquivo{$numReg}"); 
		$this->identificador_erro = Helper::SESSION("identificador_erro{$numReg}"); 
		$this->descricao = Helper::SESSION("descricao{$numReg}"); 
		$this->stacktrace = Helper::SESSION("stacktrace{$numReg}"); 
		$this->data_visualizacao_DATETIME = Helper::SESSION("data_visualizacao_DATETIME{$numReg}"); 
		$this->data_ocorrida_DATETIME = Helper::SESSION("data_ocorrida_DATETIME{$numReg}"); 
		$this->sistema_tipo_log_erro_id_INT = Helper::SESSION("sistema_tipo_log_erro_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::SESSION("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->id_usuario_INT = Helper::SESSION("id_usuario_INT{$numReg}"); 
		$this->id_corporacao_INT = Helper::SESSION("id_corporacao_INT{$numReg}"); 
		$this->sistema_log_identificador_id_INT = Helper::SESSION("sistema_log_identificador_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::SESSION("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->mobile_conectado_id_INT = Helper::SESSION("mobile_conectado_id_INT{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::SESSION("script_comando_banco_id_INT{$numReg}"); 
		$this->total_ocorrencia_INT = Helper::SESSION("total_ocorrencia_INT{$numReg}"); 
		$this->sincronizado_BOOLEAN = Helper::SESSION("sincronizado_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->classe = Helper::POST("classe{$numReg}"); 
		$this->funcao = Helper::POST("funcao{$numReg}"); 
		$this->linha_INT = Helper::POST("linha_INT{$numReg}"); 
		$this->nome_arquivo = Helper::POST("nome_arquivo{$numReg}"); 
		$this->identificador_erro = Helper::POST("identificador_erro{$numReg}"); 
		$this->descricao = Helper::POST("descricao{$numReg}"); 
		$this->stacktrace = Helper::POST("stacktrace{$numReg}"); 
		$this->data_visualizacao_DATETIME = Helper::POST("data_visualizacao_DATETIME{$numReg}"); 
		$this->data_ocorrida_DATETIME = Helper::POST("data_ocorrida_DATETIME{$numReg}"); 
		$this->sistema_tipo_log_erro_id_INT = Helper::POST("sistema_tipo_log_erro_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::POST("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->id_usuario_INT = Helper::POST("id_usuario_INT{$numReg}"); 
		$this->id_corporacao_INT = Helper::POST("id_corporacao_INT{$numReg}"); 
		$this->sistema_log_identificador_id_INT = Helper::POST("sistema_log_identificador_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::POST("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->mobile_conectado_id_INT = Helper::POST("mobile_conectado_id_INT{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::POST("script_comando_banco_id_INT{$numReg}"); 
		$this->total_ocorrencia_INT = Helper::POST("total_ocorrencia_INT{$numReg}"); 
		$this->sincronizado_BOOLEAN = Helper::POST("sincronizado_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->classe = Helper::GET("classe{$numReg}"); 
		$this->funcao = Helper::GET("funcao{$numReg}"); 
		$this->linha_INT = Helper::GET("linha_INT{$numReg}"); 
		$this->nome_arquivo = Helper::GET("nome_arquivo{$numReg}"); 
		$this->identificador_erro = Helper::GET("identificador_erro{$numReg}"); 
		$this->descricao = Helper::GET("descricao{$numReg}"); 
		$this->stacktrace = Helper::GET("stacktrace{$numReg}"); 
		$this->data_visualizacao_DATETIME = Helper::GET("data_visualizacao_DATETIME{$numReg}"); 
		$this->data_ocorrida_DATETIME = Helper::GET("data_ocorrida_DATETIME{$numReg}"); 
		$this->sistema_tipo_log_erro_id_INT = Helper::GET("sistema_tipo_log_erro_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::GET("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->id_usuario_INT = Helper::GET("id_usuario_INT{$numReg}"); 
		$this->id_corporacao_INT = Helper::GET("id_corporacao_INT{$numReg}"); 
		$this->sistema_log_identificador_id_INT = Helper::GET("sistema_log_identificador_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::GET("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->mobile_conectado_id_INT = Helper::GET("mobile_conectado_id_INT{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::GET("script_comando_banco_id_INT{$numReg}"); 
		$this->total_ocorrencia_INT = Helper::GET("total_ocorrencia_INT{$numReg}"); 
		$this->sincronizado_BOOLEAN = Helper::GET("sincronizado_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["classe{$numReg}"]) || $tipo == null){

		$upd.= "classe = $this->classe, ";

	} 

	if(isset($tipo["funcao{$numReg}"]) || $tipo == null){

		$upd.= "funcao = $this->funcao, ";

	} 

	if(isset($tipo["linha_INT{$numReg}"]) || $tipo == null){

		$upd.= "linha_INT = $this->linha_INT, ";

	} 

	if(isset($tipo["nome_arquivo{$numReg}"]) || $tipo == null){

		$upd.= "nome_arquivo = $this->nome_arquivo, ";

	} 

	if(isset($tipo["identificador_erro{$numReg}"]) || $tipo == null){

		$upd.= "identificador_erro = $this->identificador_erro, ";

	} 

	if(isset($tipo["descricao{$numReg}"]) || $tipo == null){

		$upd.= "descricao = $this->descricao, ";

	} 

	if(isset($tipo["stacktrace{$numReg}"]) || $tipo == null){

		$upd.= "stacktrace = $this->stacktrace, ";

	} 

	if(isset($tipo["data_visualizacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_visualizacao_DATETIME = $this->data_visualizacao_DATETIME, ";

	} 

	if(isset($tipo["data_ocorrida_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_ocorrida_DATETIME = $this->data_ocorrida_DATETIME, ";

	} 

	if(isset($tipo["sistema_tipo_log_erro_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_tipo_log_erro_id_INT = $this->sistema_tipo_log_erro_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_produto_id_INT = $this->sistema_projetos_versao_produto_id_INT, ";

	} 

	if(isset($tipo["id_usuario_INT{$numReg}"]) || $tipo == null){

		$upd.= "id_usuario_INT = $this->id_usuario_INT, ";

	} 

	if(isset($tipo["id_corporacao_INT{$numReg}"]) || $tipo == null){

		$upd.= "id_corporacao_INT = $this->id_corporacao_INT, ";

	} 

	if(isset($tipo["sistema_log_identificador_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_log_identificador_id_INT = $this->sistema_log_identificador_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_id_INT = $this->sistema_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["mobile_conectado_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "mobile_conectado_id_INT = $this->mobile_conectado_id_INT, ";

	} 

	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["script_comando_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "script_comando_banco_id_INT = $this->script_comando_banco_id_INT, ";

	} 

	if(isset($tipo["total_ocorrencia_INT{$numReg}"]) || $tipo == null){

		$upd.= "total_ocorrencia_INT = $this->total_ocorrencia_INT, ";

	} 

	if(isset($tipo["sincronizado_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "sincronizado_BOOLEAN = $this->sincronizado_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_produto_log_erro SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    