<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_tipo_conjunto_analise
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_tipo_conjunto_analise.php
    * TABELA MYSQL:    projetos_tipo_conjunto_analise
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_tipo_conjunto_analise extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_id_INT;
	public $obj;
	public $tipo_conjunto_analise_id_INT;
	public $objProjetos;


    public $nomeEntidade;



    

	public $label_id;
	public $label_projetos_id_INT;
	public $label_tipo_conjunto_analise_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_tipo_conjunto_analise";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos($this->getDatabase());
		if($this->projetos_id_INT != null) 
		$this->obj->select($this->projetos_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_conjunto_analise(){
	if($this->objProjetos ==null){
		$this->objProjetos = new EXTDAO_Tipo_conjunto_analise($this->getDatabase());
		if($this->tipo_conjunto_analise_id_INT != null) 
		$this->objProjetos->select($this->tipo_conjunto_analise_id_INT);
	}
	return $this->objProjetos ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos($objArgumentos){

		$objArgumentos->nome="projetos_id_INT";
		$objArgumentos->id="projetos_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_conjunto_analise($objArgumentos){

		$objArgumentos->nome="tipo_conjunto_analise_id_INT";
		$objArgumentos->id="tipo_conjunto_analise_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_conjunto_analise()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_tipo_conjunto_analise", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_tipo_conjunto_analise", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_id_INT()
    {
    	return $this->projetos_id_INT;
    }
    
    public function getTipo_conjunto_analise_id_INT()
    {
    	return $this->tipo_conjunto_analise_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_id_INT($val)
    {
    	$this->projetos_id_INT =  $val;
    }
    
    function setTipo_conjunto_analise_id_INT($val)
    {
    	$this->tipo_conjunto_analise_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM projetos_tipo_conjunto_analise WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_id_INT = $row->projetos_id_INT;
        if(isset($this->objProjetos))
			$this->objProjetos->select($this->projetos_id_INT);

        $this->tipo_conjunto_analise_id_INT = $row->tipo_conjunto_analise_id_INT;
        if(isset($this->objTipo_conjunto_analise))
			$this->objTipo_conjunto_analise->select($this->tipo_conjunto_analise_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_tipo_conjunto_analise WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_tipo_conjunto_analise ( projetos_id_INT , tipo_conjunto_analise_id_INT ) VALUES ( {$this->projetos_id_INT} , {$this->tipo_conjunto_analise_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_id_INT(){ 

		return "projetos_id_INT";

	}

	public function nomeCampoTipo_conjunto_analise_id_INT(){ 

		return "tipo_conjunto_analise_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_id_INT($objArguments){

		$objArguments->nome = "projetos_id_INT";
		$objArguments->id = "projetos_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_conjunto_analise_id_INT($objArguments){

		$objArguments->nome = "tipo_conjunto_analise_id_INT";
		$objArguments->id = "tipo_conjunto_analise_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_id_INT == ""){

			$this->projetos_id_INT = "null";

		}

		if($this->tipo_conjunto_analise_id_INT == ""){

			$this->tipo_conjunto_analise_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_id_INT = null; 
		$this->objProjetos= null;
		$this->tipo_conjunto_analise_id_INT = null; 
		$this->objTipo_conjunto_analise= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_id_INT", $this->projetos_id_INT); 
		Helper::setSession("tipo_conjunto_analise_id_INT", $this->tipo_conjunto_analise_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_id_INT");
		Helper::clearSession("tipo_conjunto_analise_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_id_INT = Helper::SESSION("projetos_id_INT{$numReg}"); 
		$this->tipo_conjunto_analise_id_INT = Helper::SESSION("tipo_conjunto_analise_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_id_INT = Helper::POST("projetos_id_INT{$numReg}"); 
		$this->tipo_conjunto_analise_id_INT = Helper::POST("tipo_conjunto_analise_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_id_INT = Helper::GET("projetos_id_INT{$numReg}"); 
		$this->tipo_conjunto_analise_id_INT = Helper::GET("tipo_conjunto_analise_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_id_INT = $this->projetos_id_INT, ";

	} 

	if(isset($tipo["tipo_conjunto_analise_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_conjunto_analise_id_INT = $this->tipo_conjunto_analise_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_tipo_conjunto_analise SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    