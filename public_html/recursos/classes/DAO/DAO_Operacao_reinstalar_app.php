<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_reinstalar_app
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Operacao_reinstalar_app.php
    * TABELA MYSQL:    operacao_reinstalar_app
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_reinstalar_app extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $versao_spvspm_id_INT;
	public $obj;
	public $observacao;
	public $operacao_sistema_mobile_id_INT;
	public $objVersao_spvspm;
	public $mensagem_ao_usuario;


    public $nomeEntidade;



    

	public $label_id;
	public $label_versao_spvspm_id_INT;
	public $label_observacao;
	public $label_operacao_sistema_mobile_id_INT;
	public $label_mensagem_ao_usuario;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "operacao_reinstalar_app";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjVersao_spvspm(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_($this->getDatabase());
		if($this->versao_spvspm_id_INT != null) 
		$this->obj->select($this->versao_spvspm_id_INT);
	}
	return $this->obj ;
}
function getFkObjOperacao_sistema_mobile(){
	if($this->objVersao_spvspm ==null){
		$this->objVersao_spvspm = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->objVersao_spvspm->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->objVersao_spvspm ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllVersao_spvspm($objArgumentos){

		$objArgumentos->nome="versao_spvspm_id_INT";
		$objArgumentos->id="versao_spvspm_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjVersao_spvspm()->getComboBox($objArgumentos);

	}

public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_operacao_reinstalar_app", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_reinstalar_app", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getVersao_spvspm_id_INT()
    {
    	return $this->versao_spvspm_id_INT;
    }
    
    public function getObservacao()
    {
    	return $this->observacao;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    public function getMensagem_ao_usuario()
    {
    	return $this->mensagem_ao_usuario;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setVersao_spvspm_id_INT($val)
    {
    	$this->versao_spvspm_id_INT =  $val;
    }
    
    function setObservacao($val)
    {
    	$this->observacao =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setMensagem_ao_usuario($val)
    {
    	$this->mensagem_ao_usuario =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM operacao_reinstalar_app WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->versao_spvspm_id_INT = $row->versao_spvspm_id_INT;
        if(isset($this->objVersao_spvspm))
			$this->objVersao_spvspm->select($this->versao_spvspm_id_INT);

        $this->observacao = $row->observacao;
        
        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

        $this->mensagem_ao_usuario = $row->mensagem_ao_usuario;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM operacao_reinstalar_app WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO operacao_reinstalar_app ( versao_spvspm_id_INT , observacao , operacao_sistema_mobile_id_INT , mensagem_ao_usuario ) VALUES ( {$this->versao_spvspm_id_INT} , {$this->observacao} , {$this->operacao_sistema_mobile_id_INT} , {$this->mensagem_ao_usuario} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoVersao_spvspm_id_INT(){ 

		return "versao_spvspm_id_INT";

	}

	public function nomeCampoObservacao(){ 

		return "observacao";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoMensagem_ao_usuario(){ 

		return "mensagem_ao_usuario";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoVersao_spvspm_id_INT($objArguments){

		$objArguments->nome = "versao_spvspm_id_INT";
		$objArguments->id = "versao_spvspm_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoObservacao($objArguments){

		$objArguments->nome = "observacao";
		$objArguments->id = "observacao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMensagem_ao_usuario($objArguments){

		$objArguments->nome = "mensagem_ao_usuario";
		$objArguments->id = "mensagem_ao_usuario";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->versao_spvspm_id_INT == ""){

			$this->versao_spvspm_id_INT = "null";

		}

			$this->observacao = $this->formatarDadosParaSQL($this->observacao);
		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}

			$this->mensagem_ao_usuario = $this->formatarDadosParaSQL($this->mensagem_ao_usuario);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->versao_spvspm_id_INT = null; 
		$this->objVersao_spvspm= null;
		$this->observacao = null; 
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;
		$this->mensagem_ao_usuario = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("versao_spvspm_id_INT", $this->versao_spvspm_id_INT); 
		Helper::setSession("observacao", $this->observacao); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 
		Helper::setSession("mensagem_ao_usuario", $this->mensagem_ao_usuario); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("versao_spvspm_id_INT");
		Helper::clearSession("observacao");
		Helper::clearSession("operacao_sistema_mobile_id_INT");
		Helper::clearSession("mensagem_ao_usuario");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->versao_spvspm_id_INT = Helper::SESSION("versao_spvspm_id_INT{$numReg}"); 
		$this->observacao = Helper::SESSION("observacao{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->mensagem_ao_usuario = Helper::SESSION("mensagem_ao_usuario{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->versao_spvspm_id_INT = Helper::POST("versao_spvspm_id_INT{$numReg}"); 
		$this->observacao = Helper::POST("observacao{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->mensagem_ao_usuario = Helper::POST("mensagem_ao_usuario{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->versao_spvspm_id_INT = Helper::GET("versao_spvspm_id_INT{$numReg}"); 
		$this->observacao = Helper::GET("observacao{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->mensagem_ao_usuario = Helper::GET("mensagem_ao_usuario{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["versao_spvspm_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "versao_spvspm_id_INT = $this->versao_spvspm_id_INT, ";

	} 

	if(isset($tipo["observacao{$numReg}"]) || $tipo == null){

		$upd.= "observacao = $this->observacao, ";

	} 

	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["mensagem_ao_usuario{$numReg}"]) || $tipo == null){

		$upd.= "mensagem_ao_usuario = $this->mensagem_ao_usuario, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE operacao_reinstalar_app SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    