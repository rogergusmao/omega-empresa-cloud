<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Usuario
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Usuario.php
    * TABELA MYSQL:    usuario
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Usuario extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $email;
	public $senha;
	public $usuario_tipo_id_INT;
	public $obj;
	public $status_BOOLEAN;
	public $pagina_inicial;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_email;
	public $label_senha;
	public $label_usuario_tipo_id_INT;
	public $label_status_BOOLEAN;
	public $label_pagina_inicial;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "usuario";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjUsuario_tipo(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Usuario_tipo($this->getDatabase());
		if($this->usuario_tipo_id_INT != null) 
		$this->obj->select($this->usuario_tipo_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllUsuario_tipo($objArgumentos){

		$objArgumentos->nome="usuario_tipo_id_INT";
		$objArgumentos->id="usuario_tipo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario_tipo()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_usuario", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_usuario", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getEmail()
    {
    	return $this->email;
    }
    
    public function getSenha()
    {
    	return $this->senha;
    }
    
    public function getUsuario_tipo_id_INT()
    {
    	return $this->usuario_tipo_id_INT;
    }
    
    public function getStatus_BOOLEAN()
    {
    	return $this->status_BOOLEAN;
    }
    
    public function getPagina_inicial()
    {
    	return $this->pagina_inicial;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setEmail($val)
    {
    	$this->email =  $val;
    }
    
    function setSenha($val)
    {
    	$this->senha =  $val;
    }
    
    function setUsuario_tipo_id_INT($val)
    {
    	$this->usuario_tipo_id_INT =  $val;
    }
    
    function setStatus_BOOLEAN($val)
    {
    	$this->status_BOOLEAN =  $val;
    }
    
    function setPagina_inicial($val)
    {
    	$this->pagina_inicial =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM usuario WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->email = $row->email;
        
        $this->senha = $row->senha;
        
        $this->usuario_tipo_id_INT = $row->usuario_tipo_id_INT;
        if(isset($this->objUsuario_tipo))
			$this->objUsuario_tipo->select($this->usuario_tipo_id_INT);

        $this->status_BOOLEAN = $row->status_BOOLEAN;
        
        $this->pagina_inicial = $row->pagina_inicial;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM usuario WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO usuario ( nome , email , senha , usuario_tipo_id_INT , status_BOOLEAN , pagina_inicial ) VALUES ( {$this->nome} , {$this->email} , {$this->senha} , {$this->usuario_tipo_id_INT} , {$this->status_BOOLEAN} , {$this->pagina_inicial} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoEmail(){ 

		return "email";

	}

	public function nomeCampoSenha(){ 

		return "senha";

	}

	public function nomeCampoUsuario_tipo_id_INT(){ 

		return "usuario_tipo_id_INT";

	}

	public function nomeCampoStatus_BOOLEAN(){ 

		return "status_BOOLEAN";

	}

	public function nomeCampoPagina_inicial(){ 

		return "pagina_inicial";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoEmail($objArguments){

		$objArguments->nome = "email";
		$objArguments->id = "email";

		return $this->campoEmail($objArguments);

	}

	public function imprimirCampoSenha($objArguments){

		$objArguments->nome = "senha";
		$objArguments->id = "senha";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUsuario_tipo_id_INT($objArguments){

		$objArguments->nome = "usuario_tipo_id_INT";
		$objArguments->id = "usuario_tipo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoStatus_BOOLEAN($objArguments){

		$objArguments->nome = "status_BOOLEAN";
		$objArguments->id = "status_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoPagina_inicial($objArguments){

		$objArguments->nome = "pagina_inicial";
		$objArguments->id = "pagina_inicial";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->email = $this->formatarDadosParaSQL($this->email);
			$this->senha = $this->formatarDadosParaSQL($this->senha);
		if($this->usuario_tipo_id_INT == ""){

			$this->usuario_tipo_id_INT = "null";

		}

		if($this->status_BOOLEAN == ""){

			$this->status_BOOLEAN = "null";

		}

			$this->pagina_inicial = $this->formatarDadosParaSQL($this->pagina_inicial);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->email = null; 
		$this->senha = null; 
		$this->usuario_tipo_id_INT = null; 
		$this->objUsuario_tipo= null;
		$this->status_BOOLEAN = null; 
		$this->pagina_inicial = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("email", $this->email); 
		Helper::setSession("senha", $this->senha); 
		Helper::setSession("usuario_tipo_id_INT", $this->usuario_tipo_id_INT); 
		Helper::setSession("status_BOOLEAN", $this->status_BOOLEAN); 
		Helper::setSession("pagina_inicial", $this->pagina_inicial); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("email");
		Helper::clearSession("senha");
		Helper::clearSession("usuario_tipo_id_INT");
		Helper::clearSession("status_BOOLEAN");
		Helper::clearSession("pagina_inicial");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->email = Helper::SESSION("email{$numReg}"); 
		$this->senha = Helper::SESSION("senha{$numReg}"); 
		$this->usuario_tipo_id_INT = Helper::SESSION("usuario_tipo_id_INT{$numReg}"); 
		$this->status_BOOLEAN = Helper::SESSION("status_BOOLEAN{$numReg}"); 
		$this->pagina_inicial = Helper::SESSION("pagina_inicial{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->email = Helper::POST("email{$numReg}"); 
		$this->senha = Helper::POST("senha{$numReg}"); 
		$this->usuario_tipo_id_INT = Helper::POST("usuario_tipo_id_INT{$numReg}"); 
		$this->status_BOOLEAN = Helper::POST("status_BOOLEAN{$numReg}"); 
		$this->pagina_inicial = Helper::POST("pagina_inicial{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->email = Helper::GET("email{$numReg}"); 
		$this->senha = Helper::GET("senha{$numReg}"); 
		$this->usuario_tipo_id_INT = Helper::GET("usuario_tipo_id_INT{$numReg}"); 
		$this->status_BOOLEAN = Helper::GET("status_BOOLEAN{$numReg}"); 
		$this->pagina_inicial = Helper::GET("pagina_inicial{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["email{$numReg}"]) || $tipo == null){

		$upd.= "email = $this->email, ";

	} 

	if(isset($tipo["senha{$numReg}"]) || $tipo == null){

		$upd.= "senha = $this->senha, ";

	} 

	if(isset($tipo["usuario_tipo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_tipo_id_INT = $this->usuario_tipo_id_INT, ";

	} 

	if(isset($tipo["status_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "status_BOOLEAN = $this->status_BOOLEAN, ";

	} 

	if(isset($tipo["pagina_inicial{$numReg}"]) || $tipo == null){

		$upd.= "pagina_inicial = $this->pagina_inicial, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE usuario SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    