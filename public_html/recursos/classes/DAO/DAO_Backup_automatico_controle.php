<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Backup_automatico_controle
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Backup_automatico_controle.php
    * TABELA MYSQL:    backup_automatico_controle
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Backup_automatico_controle extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $hora_base_TIME;
	public $hora_real_TIME;
	public $data_DATE;


    public $nomeEntidade;

	public $data_DATE_UNIX;


    

	public $label_id;
	public $label_hora_base_TIME;
	public $label_hora_real_TIME;
	public $label_data_DATE;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "backup_automatico_controle";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_backup_automatico_controle", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_backup_automatico_controle", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHora_base_TIME()
    {
    	return $this->hora_base_TIME;
    }
    
    public function getHora_real_TIME()
    {
    	return $this->hora_real_TIME;
    }
    
    function getData_DATE_UNIX()
    {
    	return $this->data_DATE_UNIX;
    }
    
    public function getData_DATE()
    {
    	return $this->data_DATE;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHora_base_TIME($val)
    {
    	$this->hora_base_TIME =  $val;
    }
    
    function setHora_real_TIME($val)
    {
    	$this->hora_real_TIME =  $val;
    }
    
    function setData_DATE($val)
    {
    	$this->data_DATE =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_DATE) AS data_DATE_UNIX FROM backup_automatico_controle WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->hora_base_TIME = $row->hora_base_TIME;
        
        $this->hora_real_TIME = $row->hora_real_TIME;
        
        $this->data_DATE = $row->data_DATE;
        $this->data_DATE_UNIX = $row->data_DATE_UNIX;

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM backup_automatico_controle WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO backup_automatico_controle ( hora_base_TIME , hora_real_TIME , data_DATE ) VALUES ( {$this->hora_base_TIME} , {$this->hora_real_TIME} , {$this->data_DATE} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHora_base_TIME(){ 

		return "hora_base_TIME";

	}

	public function nomeCampoHora_real_TIME(){ 

		return "hora_real_TIME";

	}

	public function nomeCampoData_DATE(){ 

		return "data_DATE";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHora_base_TIME($objArguments){

		$objArguments->nome = "hora_base_TIME";
		$objArguments->id = "hora_base_TIME";

		return $this->campoHora($objArguments);

	}

	public function imprimirCampoHora_real_TIME($objArguments){

		$objArguments->nome = "hora_real_TIME";
		$objArguments->id = "hora_real_TIME";

		return $this->campoHora($objArguments);

	}

	public function imprimirCampoData_DATE($objArguments){

		$objArguments->nome = "data_DATE";
		$objArguments->id = "data_DATE";

		return $this->campoData($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){



	$this->hora_base_TIME = $this->formatarHoraParaComandoSQL($this->hora_base_TIME); 
	$this->hora_real_TIME = $this->formatarHoraParaComandoSQL($this->hora_real_TIME); 
	$this->data_DATE = $this->formatarDataParaComandoSQL($this->data_DATE); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->hora_base_TIME = $this->formatarHoraParaExibicao($this->hora_base_TIME); 
	$this->hora_real_TIME = $this->formatarHoraParaExibicao($this->hora_real_TIME); 
	$this->data_DATE = $this->formatarDataParaExibicao($this->data_DATE); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->hora_base_TIME = null; 
		$this->hora_real_TIME = null; 
		$this->data_DATE = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("hora_base_TIME", $this->hora_base_TIME); 
		Helper::setSession("hora_real_TIME", $this->hora_real_TIME); 
		Helper::setSession("data_DATE", $this->data_DATE); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("hora_base_TIME");
		Helper::clearSession("hora_real_TIME");
		Helper::clearSession("data_DATE");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->hora_base_TIME = Helper::SESSION("hora_base_TIME{$numReg}"); 
		$this->hora_real_TIME = Helper::SESSION("hora_real_TIME{$numReg}"); 
		$this->data_DATE = Helper::SESSION("data_DATE{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->hora_base_TIME = Helper::POST("hora_base_TIME{$numReg}"); 
		$this->hora_real_TIME = Helper::POST("hora_real_TIME{$numReg}"); 
		$this->data_DATE = Helper::POST("data_DATE{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->hora_base_TIME = Helper::GET("hora_base_TIME{$numReg}"); 
		$this->hora_real_TIME = Helper::GET("hora_real_TIME{$numReg}"); 
		$this->data_DATE = Helper::GET("data_DATE{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["hora_base_TIME{$numReg}"]) || $tipo == null){

		$upd.= "hora_base_TIME = $this->hora_base_TIME, ";

	} 

	if(isset($tipo["hora_real_TIME{$numReg}"]) || $tipo == null){

		$upd.= "hora_real_TIME = $this->hora_real_TIME, ";

	} 

	if(isset($tipo["data_DATE{$numReg}"]) || $tipo == null){

		$upd.= "data_DATE = $this->data_DATE, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE backup_automatico_controle SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    