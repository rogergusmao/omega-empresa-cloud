<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Script_comando_banco_cache
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Script_comando_banco_cache.php
    * TABELA MYSQL:    script_comando_banco_cache
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Script_comando_banco_cache extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $script_comando_banco_id_INT;
	public $obj;
	public $tabela_chave_id_INT;
	public $objScript_comando_banco;
	public $atributo_id_INT;
	public $objTabela_chave;
	public $nome_chave;


    public $nomeEntidade;



    

	public $label_id;
	public $label_script_comando_banco_id_INT;
	public $label_tabela_chave_id_INT;
	public $label_atributo_id_INT;
	public $label_nome_chave;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "script_comando_banco_cache";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjScript_comando_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Script_comando_banco($this->getDatabase());
		if($this->script_comando_banco_id_INT != null) 
		$this->obj->select($this->script_comando_banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjTabela_chave(){
	if($this->objScript_comando_banco ==null){
		$this->objScript_comando_banco = new EXTDAO_Tabela_chave($this->getDatabase());
		if($this->tabela_chave_id_INT != null) 
		$this->objScript_comando_banco->select($this->tabela_chave_id_INT);
	}
	return $this->objScript_comando_banco ;
}
function getFkObjAtributo(){
	if($this->objTabela_chave ==null){
		$this->objTabela_chave = new EXTDAO_Atributo($this->getDatabase());
		if($this->atributo_id_INT != null) 
		$this->objTabela_chave->select($this->atributo_id_INT);
	}
	return $this->objTabela_chave ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllScript_comando_banco($objArgumentos){

		$objArgumentos->nome="script_comando_banco_id_INT";
		$objArgumentos->id="script_comando_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjScript_comando_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTabela_chave($objArgumentos){

		$objArgumentos->nome="tabela_chave_id_INT";
		$objArgumentos->id="tabela_chave_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela_chave()->getComboBox($objArgumentos);

	}

public function getComboBoxAllAtributo($objArgumentos){

		$objArgumentos->nome="atributo_id_INT";
		$objArgumentos->id="atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjAtributo()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_script_comando_banco_cache", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_script_comando_banco_cache", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getScript_comando_banco_id_INT()
    {
    	return $this->script_comando_banco_id_INT;
    }
    
    public function getTabela_chave_id_INT()
    {
    	return $this->tabela_chave_id_INT;
    }
    
    public function getAtributo_id_INT()
    {
    	return $this->atributo_id_INT;
    }
    
    public function getNome_chave()
    {
    	return $this->nome_chave;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setScript_comando_banco_id_INT($val)
    {
    	$this->script_comando_banco_id_INT =  $val;
    }
    
    function setTabela_chave_id_INT($val)
    {
    	$this->tabela_chave_id_INT =  $val;
    }
    
    function setAtributo_id_INT($val)
    {
    	$this->atributo_id_INT =  $val;
    }
    
    function setNome_chave($val)
    {
    	$this->nome_chave =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM script_comando_banco_cache WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->script_comando_banco_id_INT = $row->script_comando_banco_id_INT;
        if(isset($this->objScript_comando_banco))
			$this->objScript_comando_banco->select($this->script_comando_banco_id_INT);

        $this->tabela_chave_id_INT = $row->tabela_chave_id_INT;
        if(isset($this->objTabela_chave))
			$this->objTabela_chave->select($this->tabela_chave_id_INT);

        $this->atributo_id_INT = $row->atributo_id_INT;
        if(isset($this->objAtributo))
			$this->objAtributo->select($this->atributo_id_INT);

        $this->nome_chave = $row->nome_chave;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM script_comando_banco_cache WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO script_comando_banco_cache ( script_comando_banco_id_INT , tabela_chave_id_INT , atributo_id_INT , nome_chave ) VALUES ( {$this->script_comando_banco_id_INT} , {$this->tabela_chave_id_INT} , {$this->atributo_id_INT} , {$this->nome_chave} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoScript_comando_banco_id_INT(){ 

		return "script_comando_banco_id_INT";

	}

	public function nomeCampoTabela_chave_id_INT(){ 

		return "tabela_chave_id_INT";

	}

	public function nomeCampoAtributo_id_INT(){ 

		return "atributo_id_INT";

	}

	public function nomeCampoNome_chave(){ 

		return "nome_chave";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoScript_comando_banco_id_INT($objArguments){

		$objArguments->nome = "script_comando_banco_id_INT";
		$objArguments->id = "script_comando_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTabela_chave_id_INT($objArguments){

		$objArguments->nome = "tabela_chave_id_INT";
		$objArguments->id = "tabela_chave_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoAtributo_id_INT($objArguments){

		$objArguments->nome = "atributo_id_INT";
		$objArguments->id = "atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoNome_chave($objArguments){

		$objArguments->nome = "nome_chave";
		$objArguments->id = "nome_chave";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->script_comando_banco_id_INT == ""){

			$this->script_comando_banco_id_INT = "null";

		}

		if($this->tabela_chave_id_INT == ""){

			$this->tabela_chave_id_INT = "null";

		}

		if($this->atributo_id_INT == ""){

			$this->atributo_id_INT = "null";

		}

			$this->nome_chave = $this->formatarDadosParaSQL($this->nome_chave);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->script_comando_banco_id_INT = null; 
		$this->objScript_comando_banco= null;
		$this->tabela_chave_id_INT = null; 
		$this->objTabela_chave= null;
		$this->atributo_id_INT = null; 
		$this->objAtributo= null;
		$this->nome_chave = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("script_comando_banco_id_INT", $this->script_comando_banco_id_INT); 
		Helper::setSession("tabela_chave_id_INT", $this->tabela_chave_id_INT); 
		Helper::setSession("atributo_id_INT", $this->atributo_id_INT); 
		Helper::setSession("nome_chave", $this->nome_chave); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("script_comando_banco_id_INT");
		Helper::clearSession("tabela_chave_id_INT");
		Helper::clearSession("atributo_id_INT");
		Helper::clearSession("nome_chave");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::SESSION("script_comando_banco_id_INT{$numReg}"); 
		$this->tabela_chave_id_INT = Helper::SESSION("tabela_chave_id_INT{$numReg}"); 
		$this->atributo_id_INT = Helper::SESSION("atributo_id_INT{$numReg}"); 
		$this->nome_chave = Helper::SESSION("nome_chave{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::POST("script_comando_banco_id_INT{$numReg}"); 
		$this->tabela_chave_id_INT = Helper::POST("tabela_chave_id_INT{$numReg}"); 
		$this->atributo_id_INT = Helper::POST("atributo_id_INT{$numReg}"); 
		$this->nome_chave = Helper::POST("nome_chave{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::GET("script_comando_banco_id_INT{$numReg}"); 
		$this->tabela_chave_id_INT = Helper::GET("tabela_chave_id_INT{$numReg}"); 
		$this->atributo_id_INT = Helper::GET("atributo_id_INT{$numReg}"); 
		$this->nome_chave = Helper::GET("nome_chave{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["script_comando_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "script_comando_banco_id_INT = $this->script_comando_banco_id_INT, ";

	} 

	if(isset($tipo["tabela_chave_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_chave_id_INT = $this->tabela_chave_id_INT, ";

	} 

	if(isset($tipo["atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "atributo_id_INT = $this->atributo_id_INT, ";

	} 

	if(isset($tipo["nome_chave{$numReg}"]) || $tipo == null){

		$upd.= "nome_chave = $this->nome_chave, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE script_comando_banco_cache SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    