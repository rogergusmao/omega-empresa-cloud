<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Banco_banco
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Banco_banco.php
    * TABELA MYSQL:    banco_banco
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Banco_banco extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $homologacao_banco_id_INT;
	public $obj;
	public $producao_banco_id_INT;
	public $objHomologacao_banco;
	public $simulador_banco_id_INT;
	public $objProducao_banco;
	public $tipo_banco_id_INT;
	public $objSimulador_banco;
	public $projetos_id_INT;
	public $objTipo_banco;


    public $nomeEntidade;



    

	public $label_id;
	public $label_homologacao_banco_id_INT;
	public $label_producao_banco_id_INT;
	public $label_simulador_banco_id_INT;
	public $label_tipo_banco_id_INT;
	public $label_projetos_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "banco_banco";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjHomologacao_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Banco($this->getDatabase());
		if($this->homologacao_banco_id_INT != null) 
		$this->obj->select($this->homologacao_banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjProducao_banco(){
	if($this->objHomologacao_banco ==null){
		$this->objHomologacao_banco = new EXTDAO_Banco($this->getDatabase());
		if($this->producao_banco_id_INT != null) 
		$this->objHomologacao_banco->select($this->producao_banco_id_INT);
	}
	return $this->objHomologacao_banco ;
}
function getFkObjSimulador_banco(){
	if($this->objProducao_banco ==null){
		$this->objProducao_banco = new EXTDAO_Banco($this->getDatabase());
		if($this->simulador_banco_id_INT != null) 
		$this->objProducao_banco->select($this->simulador_banco_id_INT);
	}
	return $this->objProducao_banco ;
}
function getFkObjTipo_banco(){
	if($this->objSimulador_banco ==null){
		$this->objSimulador_banco = new EXTDAO_Tipo_banco($this->getDatabase());
		if($this->tipo_banco_id_INT != null) 
		$this->objSimulador_banco->select($this->tipo_banco_id_INT);
	}
	return $this->objSimulador_banco ;
}
function getFkObjProjetos(){
	if($this->objTipo_banco ==null){
		$this->objTipo_banco = new EXTDAO_Projetos($this->getDatabase());
		if($this->projetos_id_INT != null) 
		$this->objTipo_banco->select($this->projetos_id_INT);
	}
	return $this->objTipo_banco ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllHomologacao_banco($objArgumentos){

		$objArgumentos->nome="homologacao_banco_id_INT";
		$objArgumentos->id="homologacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjHomologacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProducao_banco($objArgumentos){

		$objArgumentos->nome="producao_banco_id_INT";
		$objArgumentos->id="producao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProducao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSimulador_banco($objArgumentos){

		$objArgumentos->nome="simulador_banco_id_INT";
		$objArgumentos->id="simulador_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSimulador_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_banco($objArgumentos){

		$objArgumentos->nome="tipo_banco_id_INT";
		$objArgumentos->id="tipo_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos($objArgumentos){

		$objArgumentos->nome="projetos_id_INT";
		$objArgumentos->id="projetos_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_banco_banco", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_banco_banco", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHomologacao_banco_id_INT()
    {
    	return $this->homologacao_banco_id_INT;
    }
    
    public function getProducao_banco_id_INT()
    {
    	return $this->producao_banco_id_INT;
    }
    
    public function getSimulador_banco_id_INT()
    {
    	return $this->simulador_banco_id_INT;
    }
    
    public function getTipo_banco_id_INT()
    {
    	return $this->tipo_banco_id_INT;
    }
    
    public function getProjetos_id_INT()
    {
    	return $this->projetos_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHomologacao_banco_id_INT($val)
    {
    	$this->homologacao_banco_id_INT =  $val;
    }
    
    function setProducao_banco_id_INT($val)
    {
    	$this->producao_banco_id_INT =  $val;
    }
    
    function setSimulador_banco_id_INT($val)
    {
    	$this->simulador_banco_id_INT =  $val;
    }
    
    function setTipo_banco_id_INT($val)
    {
    	$this->tipo_banco_id_INT =  $val;
    }
    
    function setProjetos_id_INT($val)
    {
    	$this->projetos_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM banco_banco WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->homologacao_banco_id_INT = $row->homologacao_banco_id_INT;
        if(isset($this->objHomologacao_banco))
			$this->objHomologacao_banco->select($this->homologacao_banco_id_INT);

        $this->producao_banco_id_INT = $row->producao_banco_id_INT;
        if(isset($this->objProducao_banco))
			$this->objProducao_banco->select($this->producao_banco_id_INT);

        $this->simulador_banco_id_INT = $row->simulador_banco_id_INT;
        if(isset($this->objSimulador_banco))
			$this->objSimulador_banco->select($this->simulador_banco_id_INT);

        $this->tipo_banco_id_INT = $row->tipo_banco_id_INT;
        if(isset($this->objTipo_banco))
			$this->objTipo_banco->select($this->tipo_banco_id_INT);

        $this->projetos_id_INT = $row->projetos_id_INT;
        if(isset($this->objProjetos))
			$this->objProjetos->select($this->projetos_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM banco_banco WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO banco_banco ( homologacao_banco_id_INT , producao_banco_id_INT , simulador_banco_id_INT , tipo_banco_id_INT , projetos_id_INT ) VALUES ( {$this->homologacao_banco_id_INT} , {$this->producao_banco_id_INT} , {$this->simulador_banco_id_INT} , {$this->tipo_banco_id_INT} , {$this->projetos_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHomologacao_banco_id_INT(){ 

		return "homologacao_banco_id_INT";

	}

	public function nomeCampoProducao_banco_id_INT(){ 

		return "producao_banco_id_INT";

	}

	public function nomeCampoSimulador_banco_id_INT(){ 

		return "simulador_banco_id_INT";

	}

	public function nomeCampoTipo_banco_id_INT(){ 

		return "tipo_banco_id_INT";

	}

	public function nomeCampoProjetos_id_INT(){ 

		return "projetos_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHomologacao_banco_id_INT($objArguments){

		$objArguments->nome = "homologacao_banco_id_INT";
		$objArguments->id = "homologacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProducao_banco_id_INT($objArguments){

		$objArguments->nome = "producao_banco_id_INT";
		$objArguments->id = "producao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSimulador_banco_id_INT($objArguments){

		$objArguments->nome = "simulador_banco_id_INT";
		$objArguments->id = "simulador_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_banco_id_INT";
		$objArguments->id = "tipo_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_id_INT($objArguments){

		$objArguments->nome = "projetos_id_INT";
		$objArguments->id = "projetos_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->homologacao_banco_id_INT == ""){

			$this->homologacao_banco_id_INT = "null";

		}

		if($this->producao_banco_id_INT == ""){

			$this->producao_banco_id_INT = "null";

		}

		if($this->simulador_banco_id_INT == ""){

			$this->simulador_banco_id_INT = "null";

		}

		if($this->tipo_banco_id_INT == ""){

			$this->tipo_banco_id_INT = "null";

		}

		if($this->projetos_id_INT == ""){

			$this->projetos_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->homologacao_banco_id_INT = null; 
		$this->objHomologacao_banco= null;
		$this->producao_banco_id_INT = null; 
		$this->objProducao_banco= null;
		$this->simulador_banco_id_INT = null; 
		$this->objSimulador_banco= null;
		$this->tipo_banco_id_INT = null; 
		$this->objTipo_banco= null;
		$this->projetos_id_INT = null; 
		$this->objProjetos= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("homologacao_banco_id_INT", $this->homologacao_banco_id_INT); 
		Helper::setSession("producao_banco_id_INT", $this->producao_banco_id_INT); 
		Helper::setSession("simulador_banco_id_INT", $this->simulador_banco_id_INT); 
		Helper::setSession("tipo_banco_id_INT", $this->tipo_banco_id_INT); 
		Helper::setSession("projetos_id_INT", $this->projetos_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("homologacao_banco_id_INT");
		Helper::clearSession("producao_banco_id_INT");
		Helper::clearSession("simulador_banco_id_INT");
		Helper::clearSession("tipo_banco_id_INT");
		Helper::clearSession("projetos_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->homologacao_banco_id_INT = Helper::SESSION("homologacao_banco_id_INT{$numReg}"); 
		$this->producao_banco_id_INT = Helper::SESSION("producao_banco_id_INT{$numReg}"); 
		$this->simulador_banco_id_INT = Helper::SESSION("simulador_banco_id_INT{$numReg}"); 
		$this->tipo_banco_id_INT = Helper::SESSION("tipo_banco_id_INT{$numReg}"); 
		$this->projetos_id_INT = Helper::SESSION("projetos_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->homologacao_banco_id_INT = Helper::POST("homologacao_banco_id_INT{$numReg}"); 
		$this->producao_banco_id_INT = Helper::POST("producao_banco_id_INT{$numReg}"); 
		$this->simulador_banco_id_INT = Helper::POST("simulador_banco_id_INT{$numReg}"); 
		$this->tipo_banco_id_INT = Helper::POST("tipo_banco_id_INT{$numReg}"); 
		$this->projetos_id_INT = Helper::POST("projetos_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->homologacao_banco_id_INT = Helper::GET("homologacao_banco_id_INT{$numReg}"); 
		$this->producao_banco_id_INT = Helper::GET("producao_banco_id_INT{$numReg}"); 
		$this->simulador_banco_id_INT = Helper::GET("simulador_banco_id_INT{$numReg}"); 
		$this->tipo_banco_id_INT = Helper::GET("tipo_banco_id_INT{$numReg}"); 
		$this->projetos_id_INT = Helper::GET("projetos_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["homologacao_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "homologacao_banco_id_INT = $this->homologacao_banco_id_INT, ";

	} 

	if(isset($tipo["producao_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "producao_banco_id_INT = $this->producao_banco_id_INT, ";

	} 

	if(isset($tipo["simulador_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "simulador_banco_id_INT = $this->simulador_banco_id_INT, ";

	} 

	if(isset($tipo["tipo_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_banco_id_INT = $this->tipo_banco_id_INT, ";

	} 

	if(isset($tipo["projetos_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_id_INT = $this->projetos_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE banco_banco SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    