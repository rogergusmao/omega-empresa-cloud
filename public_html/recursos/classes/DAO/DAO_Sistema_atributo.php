<?php

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:  DAO_Sistema_atributo
* DATA DE GERA��O: 19.07.2014
* ARQUIVO:         DAO_Sistema_atributo.php
* TABELA MYSQL:    sistema_atributo
* BANCO DE DADOS:  sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARA��O DA CLASSE
// **********************

class DAO_Sistema_atributo extends Generic_DAO
{


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

    public $id;
    public $nome;
    public $sistema_tabela_id_INT;
    public $objSistema_tabela;
    public $tipo_sql;
    public $tamanho_INT;
    public $decimal_INT;
    public $not_null_BOOLEAN;
    public $primary_key_BOOLEAN;
    public $auto_increment_BOOLEAN;
    public $valor_default;
    public $fk_sistema_tabela_id_INT;
    public $objFk_sistema_tabela;
    public $atributo_fk;
    public $fk_nome;
    public $update_tipo_fk;
    public $delete_tipo_fk;
    public $label;
    public $unique_BOOLEAN;
    public $unique_nome;
    public $seq_INT;
    public $excluido_BOOLEAN;


    public $nomeEntidade;





    public $label_id;
    public $label_nome;
    public $label_sistema_tabela_id_INT;
    public $label_tipo_sql;
    public $label_tamanho_INT;
    public $label_decimal_INT;
    public $label_not_null_BOOLEAN;
    public $label_primary_key_BOOLEAN;
    public $label_auto_increment_BOOLEAN;
    public $label_valor_default;
    public $label_fk_sistema_tabela_id_INT;
    public $label_atributo_fk;
    public $label_fk_nome;
    public $label_update_tipo_fk;
    public $label_delete_tipo_fk;
    public $label_label;
    public $label_unique_BOOLEAN;
    public $label_unique_nome;
    public $label_seq_INT;
    public $label_excluido_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($configDAO)
    {

        parent::__construct($configDAO);

        $this->nomeEntidade = "";
        $this->nomeTabela = "sistema_atributo";
        $this->campoId = "id";
        $this->campoLabel = "nome";

        $this->objSistema_tabela = new EXTDAO_Sistema_tabela($configDAO);
        $this->objFk_sistema_tabela = new EXTDAO_Sistema_tabela($configDAO);


    }

    public function valorCampoLabel(){

        return $this->getNome();

    }



    public function getComboBoxAllSistema_tabela($objArgumentos){

        $objArgumentos->nome="sistema_tabela_id_INT";
        $objArgumentos->id="sistema_tabela_id_INT";
        $objArgumentos->valueReplaceId=false;

        return $this->objSistema_tabela->getComboBox($objArgumentos);

    }

    public function getComboBoxAllFk_sistema_tabela($objArgumentos){

        $objArgumentos->nome="fk_sistema_tabela_id_INT";
        $objArgumentos->id="fk_sistema_tabela_id_INT";
        $objArgumentos->valueReplaceId=false;

        return $this->objFk_sistema_tabela->getComboBox($objArgumentos);

    }



    public function __actionAdd(){

        $mensagemSucesso = "";

        $numeroRegistros = Helper::POST("numeroRegs");

        $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
        $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

        for($i=1; $i <= $numeroRegistros; $i++){

            $this->setByPost($i);
            $this->formatarParaSQL();

            $this->insert();
            $this->selectUltimoRegistroInserido();




        }

        return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

    }

    public function __actionAddAjax(){

        $mensagemSucesso = "";

        $numeroRegistros = Helper::POST("numero_registros_ajax");

        $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
        $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

        for($i=1; $i <= $numeroRegistros; $i++){

            $this->setByPost($i);



            $this->formatarParaSQL();

            $this->insert();
            $this->selectUltimoRegistroInserido();



        }

        return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

    }

    public function __actionEdit(){

        $mensagemSucesso = "";
        $numeroRegistros = Helper::POST("numeroRegs");

        $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
        $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

        for($i=1; $i <= $numeroRegistros; $i++){

            $this->setByPost($i);
            $this->formatarParaSQL();

            $this->update($this->getId(), $_POST, $i);

            $this->select($this->getId());




        }

        return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

    }

    public function __actionRemove(){

        $mensagemSucesso = "";

        $urlSuccess = Helper::getUrlAction("list_sistema_atributo", Helper::GET("id"));
        $urlErro = Helper::getUrlAction("list_sistema_atributo", Helper::GET("id"));

        $registroRemover = Helper::GET("id");

        $this->delete($registroRemover);



        return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

    }




    // **********************
    // M�TODOS GETTER's
    // **********************


    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getSistema_tabela_id_INT()
    {
        return $this->sistema_tabela_id_INT;
    }

    public function getTipo_sql()
    {
        return $this->tipo_sql;
    }

    public function getTamanho_INT()
    {
        return $this->tamanho_INT;
    }

    public function getDecimal_INT()
    {
        return $this->decimal_INT;
    }

    public function getNot_null_BOOLEAN()
    {
        return $this->not_null_BOOLEAN;
    }

    public function getPrimary_key_BOOLEAN()
    {
        return $this->primary_key_BOOLEAN;
    }

    public function getAuto_increment_BOOLEAN()
    {
        return $this->auto_increment_BOOLEAN;
    }

    public function getValor_default()
    {
        return $this->valor_default;
    }

    public function getFk_sistema_tabela_id_INT()
    {
        return $this->fk_sistema_tabela_id_INT;
    }

    public function getAtributo_fk()
    {
        return $this->atributo_fk;
    }

    public function getFk_nome()
    {
        return $this->fk_nome;
    }

    public function getUpdate_tipo_fk()
    {
        return $this->update_tipo_fk;
    }

    public function getDelete_tipo_fk()
    {
        return $this->delete_tipo_fk;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getUnique_BOOLEAN()
    {
        return $this->unique_BOOLEAN;
    }

    public function getUnique_nome()
    {
        return $this->unique_nome;
    }

    public function getSeq_INT()
    {
        return $this->seq_INT;
    }

    public function getExcluido_BOOLEAN()
    {
        return $this->excluido_BOOLEAN;
    }

    // **********************
    // M�TODOS SETTER's
    // **********************


    function setId($val)
    {
        $this->id =  $val;
    }

    function setNome($val)
    {
        $this->nome =  $val;
    }

    function setSistema_tabela_id_INT($val)
    {
        $this->sistema_tabela_id_INT =  $val;
    }

    function setTipo_sql($val)
    {
        $this->tipo_sql =  $val;
    }

    function setTamanho_INT($val)
    {
        $this->tamanho_INT =  $val;
    }

    function setDecimal_INT($val)
    {
        $this->decimal_INT =  $val;
    }

    function setNot_null_BOOLEAN($val)
    {
        $this->not_null_BOOLEAN =  $val;
    }

    function setPrimary_key_BOOLEAN($val)
    {
        $this->primary_key_BOOLEAN =  $val;
    }

    function setAuto_increment_BOOLEAN($val)
    {
        $this->auto_increment_BOOLEAN =  $val;
    }

    function setValor_default($val)
    {
        $this->valor_default =  $val;
    }

    function setFk_sistema_tabela_id_INT($val)
    {
        $this->fk_sistema_tabela_id_INT =  $val;
    }

    function setAtributo_fk($val)
    {
        $this->atributo_fk =  $val;
    }

    function setFk_nome($val)
    {
        $this->fk_nome =  $val;
    }

    function setUpdate_tipo_fk($val)
    {
        $this->update_tipo_fk =  $val;
    }

    function setDelete_tipo_fk($val)
    {
        $this->delete_tipo_fk =  $val;
    }

    function setLabel($val)
    {
        $this->label =  $val;
    }

    function setUnique_BOOLEAN($val)
    {
        $this->unique_BOOLEAN =  $val;
    }

    function setUnique_nome($val)
    {
        $this->unique_nome =  $val;
    }

    function setSeq_INT($val)
    {
        $this->seq_INT =  $val;
    }

    function setExcluido_BOOLEAN($val)
    {
        $this->excluido_BOOLEAN =  $val;
    }


    // **********************
    // SELECT
    // **********************

    function select($id)
    {

        $sql =  "SELECT *  FROM sistema_atributo WHERE id = $id;";
        $this->database->query($sql);
        $result = $this->database->result;
        $row = $this->database->fetchObject($result);


        $this->id = $row->id;

        $this->nome = $row->nome;

        $this->sistema_tabela_id_INT = $row->sistema_tabela_id_INT;
        if($this->sistema_tabela_id_INT)
            $this->objSistema_tabela->select($this->sistema_tabela_id_INT);

        $this->tipo_sql = $row->tipo_sql;

        $this->tamanho_INT = $row->tamanho_INT;

        $this->decimal_INT = $row->decimal_INT;

        $this->not_null_BOOLEAN = $row->not_null_BOOLEAN;

        $this->primary_key_BOOLEAN = $row->primary_key_BOOLEAN;

        $this->auto_increment_BOOLEAN = $row->auto_increment_BOOLEAN;

        $this->valor_default = $row->valor_default;

        $this->fk_sistema_tabela_id_INT = $row->fk_sistema_tabela_id_INT;
        if($this->fk_sistema_tabela_id_INT)
            $this->objFk_sistema_tabela->select($this->fk_sistema_tabela_id_INT);

        $this->atributo_fk = $row->atributo_fk;

        $this->fk_nome = $row->fk_nome;

        $this->update_tipo_fk = $row->update_tipo_fk;

        $this->delete_tipo_fk = $row->delete_tipo_fk;

        $this->label = $row->label;

        $this->unique_BOOLEAN = $row->unique_BOOLEAN;

        $this->unique_nome = $row->unique_nome;

        $this->seq_INT = $row->seq_INT;

        $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;


    }


    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
        $sql = "DELETE FROM sistema_atributo WHERE id = $id;";
        $this->database->query($sql);

    }

    // **********************
    // INSERT
    // **********************

    public function insert()
    {

        $this->id = ""; //limpar chave com autoincremento

        $sql = "INSERT INTO sistema_atributo ( nome,sistema_tabela_id_INT,tipo_sql,tamanho_INT,decimal_INT,not_null_BOOLEAN,primary_key_BOOLEAN,auto_increment_BOOLEAN,valor_default,fk_sistema_tabela_id_INT,atributo_fk,fk_nome,update_tipo_fk,delete_tipo_fk,label,unique_BOOLEAN,unique_nome,seq_INT,excluido_BOOLEAN ) VALUES ( '$this->nome',$this->sistema_tabela_id_INT,'$this->tipo_sql',$this->tamanho_INT,$this->decimal_INT,$this->not_null_BOOLEAN,$this->primary_key_BOOLEAN,$this->auto_increment_BOOLEAN,'$this->valor_default',$this->fk_sistema_tabela_id_INT,'$this->atributo_fk','$this->fk_nome','$this->update_tipo_fk','$this->delete_tipo_fk','$this->label',$this->unique_BOOLEAN,'$this->unique_nome',$this->seq_INT,$this->excluido_BOOLEAN )";
        $this->database->query($sql);


    }


    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

    public function nomeCampoId(){

        return "id";

    }

    public function nomeCampoNome(){

        return "nome";

    }

    public function nomeCampoSistema_tabela_id_INT(){

        return "sistema_tabela_id_INT";

    }

    public function nomeCampoTipo_sql(){

        return "tipo_sql";

    }

    public function nomeCampoTamanho_INT(){

        return "tamanho_INT";

    }

    public function nomeCampoDecimal_INT(){

        return "decimal_INT";

    }

    public function nomeCampoNot_null_BOOLEAN(){

        return "not_null_BOOLEAN";

    }

    public function nomeCampoPrimary_key_BOOLEAN(){

        return "primary_key_BOOLEAN";

    }

    public function nomeCampoAuto_increment_BOOLEAN(){

        return "auto_increment_BOOLEAN";

    }

    public function nomeCampoValor_default(){

        return "valor_default";

    }

    public function nomeCampoFk_sistema_tabela_id_INT(){

        return "fk_sistema_tabela_id_INT";

    }

    public function nomeCampoAtributo_fk(){

        return "atributo_fk";

    }

    public function nomeCampoFk_nome(){

        return "fk_nome";

    }

    public function nomeCampoUpdate_tipo_fk(){

        return "update_tipo_fk";

    }

    public function nomeCampoDelete_tipo_fk(){

        return "delete_tipo_fk";

    }

    public function nomeCampoLabel(){

        return "label";

    }

    public function nomeCampoUnique_BOOLEAN(){

        return "unique_BOOLEAN";

    }

    public function nomeCampoUnique_nome(){

        return "unique_nome";

    }

    public function nomeCampoSeq_INT(){

        return "seq_INT";

    }

    public function nomeCampoExcluido_BOOLEAN(){

        return "excluido_BOOLEAN";

    }




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

    public function imprimirCampoNome($objArguments){

        $objArguments->nome = "nome";
        $objArguments->id = "nome";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoSistema_tabela_id_INT($objArguments){

        $objArguments->nome = "sistema_tabela_id_INT";
        $objArguments->id = "sistema_tabela_id_INT";

        return $this->campoInteiro($objArguments);

    }

    public function imprimirCampoTipo_sql($objArguments){

        $objArguments->nome = "tipo_sql";
        $objArguments->id = "tipo_sql";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoTamanho_INT($objArguments){

        $objArguments->nome = "tamanho_INT";
        $objArguments->id = "tamanho_INT";

        return $this->campoInteiro($objArguments);

    }

    public function imprimirCampoDecimal_INT($objArguments){

        $objArguments->nome = "decimal_INT";
        $objArguments->id = "decimal_INT";

        return $this->campoInteiro($objArguments);

    }

    public function imprimirCampoNot_null_BOOLEAN($objArguments){

        $objArguments->nome = "not_null_BOOLEAN";
        $objArguments->id = "not_null_BOOLEAN";

        return $this->campoBoolean($objArguments);

    }

    public function imprimirCampoPrimary_key_BOOLEAN($objArguments){

        $objArguments->nome = "primary_key_BOOLEAN";
        $objArguments->id = "primary_key_BOOLEAN";

        return $this->campoBoolean($objArguments);

    }

    public function imprimirCampoAuto_increment_BOOLEAN($objArguments){

        $objArguments->nome = "auto_increment_BOOLEAN";
        $objArguments->id = "auto_increment_BOOLEAN";

        return $this->campoBoolean($objArguments);

    }

    public function imprimirCampoValor_default($objArguments){

        $objArguments->nome = "valor_default";
        $objArguments->id = "valor_default";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoFk_sistema_tabela_id_INT($objArguments){

        $objArguments->nome = "fk_sistema_tabela_id_INT";
        $objArguments->id = "fk_sistema_tabela_id_INT";

        return $this->campoInteiro($objArguments);

    }

    public function imprimirCampoAtributo_fk($objArguments){

        $objArguments->nome = "atributo_fk";
        $objArguments->id = "atributo_fk";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoFk_nome($objArguments){

        $objArguments->nome = "fk_nome";
        $objArguments->id = "fk_nome";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoUpdate_tipo_fk($objArguments){

        $objArguments->nome = "update_tipo_fk";
        $objArguments->id = "update_tipo_fk";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoDelete_tipo_fk($objArguments){

        $objArguments->nome = "delete_tipo_fk";
        $objArguments->id = "delete_tipo_fk";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoLabel($objArguments){

        $objArguments->nome = "label";
        $objArguments->id = "label";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoUnique_BOOLEAN($objArguments){

        $objArguments->nome = "unique_BOOLEAN";
        $objArguments->id = "unique_BOOLEAN";

        return $this->campoBoolean($objArguments);

    }

    public function imprimirCampoUnique_nome($objArguments){

        $objArguments->nome = "unique_nome";
        $objArguments->id = "unique_nome";

        return $this->campoTexto($objArguments);

    }

    public function imprimirCampoSeq_INT($objArguments){

        $objArguments->nome = "seq_INT";
        $objArguments->id = "seq_INT";

        return $this->campoInteiro($objArguments);

    }

    public function imprimirCampoExcluido_BOOLEAN($objArguments){

        $objArguments->nome = "excluido_BOOLEAN";
        $objArguments->id = "excluido_BOOLEAN";

        return $this->campoBoolean($objArguments);

    }




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

        if($this->sistema_tabela_id_INT == ""){

            $this->sistema_tabela_id_INT = "null";

        }

        if($this->tamanho_INT == ""){

            $this->tamanho_INT = "null";

        }

        if($this->decimal_INT == ""){

            $this->decimal_INT = "null";

        }

        if($this->not_null_BOOLEAN == ""){

            $this->not_null_BOOLEAN = "null";

        }

        if($this->primary_key_BOOLEAN == ""){

            $this->primary_key_BOOLEAN = "null";

        }

        if($this->auto_increment_BOOLEAN == ""){

            $this->auto_increment_BOOLEAN = "null";

        }

        if($this->fk_sistema_tabela_id_INT == ""){

            $this->fk_sistema_tabela_id_INT = "null";

        }

        if($this->unique_BOOLEAN == ""){

            $this->unique_BOOLEAN = "null";

        }

        if($this->seq_INT == ""){

            $this->seq_INT = "null";

        }

        if($this->excluido_BOOLEAN == ""){

            $this->excluido_BOOLEAN = "null";

        }





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }


    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

        $_SESSION["id"] = $this->id;
        $_SESSION["nome"] = $this->nome;
        $_SESSION["sistema_tabela_id_INT"] = $this->sistema_tabela_id_INT;
        $_SESSION["tipo_sql"] = $this->tipo_sql;
        $_SESSION["tamanho_INT"] = $this->tamanho_INT;
        $_SESSION["decimal_INT"] = $this->decimal_INT;
        $_SESSION["not_null_BOOLEAN"] = $this->not_null_BOOLEAN;
        $_SESSION["primary_key_BOOLEAN"] = $this->primary_key_BOOLEAN;
        $_SESSION["auto_increment_BOOLEAN"] = $this->auto_increment_BOOLEAN;
        $_SESSION["valor_default"] = $this->valor_default;
        $_SESSION["fk_sistema_tabela_id_INT"] = $this->fk_sistema_tabela_id_INT;
        $_SESSION["atributo_fk"] = $this->atributo_fk;
        $_SESSION["fk_nome"] = $this->fk_nome;
        $_SESSION["update_tipo_fk"] = $this->update_tipo_fk;
        $_SESSION["delete_tipo_fk"] = $this->delete_tipo_fk;
        $_SESSION["label"] = $this->label;
        $_SESSION["unique_BOOLEAN"] = $this->unique_BOOLEAN;
        $_SESSION["unique_nome"] = $this->unique_nome;
        $_SESSION["seq_INT"] = $this->seq_INT;
        $_SESSION["excluido_BOOLEAN"] = $this->excluido_BOOLEAN;


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

        unset($_SESSION["id"]);
        unset($_SESSION["nome"]);
        unset($_SESSION["sistema_tabela_id_INT"]);
        unset($_SESSION["tipo_sql"]);
        unset($_SESSION["tamanho_INT"]);
        unset($_SESSION["decimal_INT"]);
        unset($_SESSION["not_null_BOOLEAN"]);
        unset($_SESSION["primary_key_BOOLEAN"]);
        unset($_SESSION["auto_increment_BOOLEAN"]);
        unset($_SESSION["valor_default"]);
        unset($_SESSION["fk_sistema_tabela_id_INT"]);
        unset($_SESSION["atributo_fk"]);
        unset($_SESSION["fk_nome"]);
        unset($_SESSION["update_tipo_fk"]);
        unset($_SESSION["delete_tipo_fk"]);
        unset($_SESSION["label"]);
        unset($_SESSION["unique_BOOLEAN"]);
        unset($_SESSION["unique_nome"]);
        unset($_SESSION["seq_INT"]);
        unset($_SESSION["excluido_BOOLEAN"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

        $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
        $this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]);
        $this->sistema_tabela_id_INT = $this->formatarDados($_SESSION["sistema_tabela_id_INT{$numReg}"]);
        $this->tipo_sql = $this->formatarDados($_SESSION["tipo_sql{$numReg}"]);
        $this->tamanho_INT = $this->formatarDados($_SESSION["tamanho_INT{$numReg}"]);
        $this->decimal_INT = $this->formatarDados($_SESSION["decimal_INT{$numReg}"]);
        $this->not_null_BOOLEAN = $this->formatarDados($_SESSION["not_null_BOOLEAN{$numReg}"]);
        $this->primary_key_BOOLEAN = $this->formatarDados($_SESSION["primary_key_BOOLEAN{$numReg}"]);
        $this->auto_increment_BOOLEAN = $this->formatarDados($_SESSION["auto_increment_BOOLEAN{$numReg}"]);
        $this->valor_default = $this->formatarDados($_SESSION["valor_default{$numReg}"]);
        $this->fk_sistema_tabela_id_INT = $this->formatarDados($_SESSION["fk_sistema_tabela_id_INT{$numReg}"]);
        $this->atributo_fk = $this->formatarDados($_SESSION["atributo_fk{$numReg}"]);
        $this->fk_nome = $this->formatarDados($_SESSION["fk_nome{$numReg}"]);
        $this->update_tipo_fk = $this->formatarDados($_SESSION["update_tipo_fk{$numReg}"]);
        $this->delete_tipo_fk = $this->formatarDados($_SESSION["delete_tipo_fk{$numReg}"]);
        $this->label = $this->formatarDados($_SESSION["label{$numReg}"]);
        $this->unique_BOOLEAN = $this->formatarDados($_SESSION["unique_BOOLEAN{$numReg}"]);
        $this->unique_nome = $this->formatarDados($_SESSION["unique_nome{$numReg}"]);
        $this->seq_INT = $this->formatarDados($_SESSION["seq_INT{$numReg}"]);
        $this->excluido_BOOLEAN = $this->formatarDados($_SESSION["excluido_BOOLEAN{$numReg}"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

        $this->id = $this->formatarDados($_POST["id{$numReg}"]);
        $this->nome = $this->formatarDados($_POST["nome{$numReg}"]);
        $this->sistema_tabela_id_INT = $this->formatarDados($_POST["sistema_tabela_id_INT{$numReg}"]);
        $this->tipo_sql = $this->formatarDados($_POST["tipo_sql{$numReg}"]);
        $this->tamanho_INT = $this->formatarDados($_POST["tamanho_INT{$numReg}"]);
        $this->decimal_INT = $this->formatarDados($_POST["decimal_INT{$numReg}"]);
        $this->not_null_BOOLEAN = $this->formatarDados($_POST["not_null_BOOLEAN{$numReg}"]);
        $this->primary_key_BOOLEAN = $this->formatarDados($_POST["primary_key_BOOLEAN{$numReg}"]);
        $this->auto_increment_BOOLEAN = $this->formatarDados($_POST["auto_increment_BOOLEAN{$numReg}"]);
        $this->valor_default = $this->formatarDados($_POST["valor_default{$numReg}"]);
        $this->fk_sistema_tabela_id_INT = $this->formatarDados($_POST["fk_sistema_tabela_id_INT{$numReg}"]);
        $this->atributo_fk = $this->formatarDados($_POST["atributo_fk{$numReg}"]);
        $this->fk_nome = $this->formatarDados($_POST["fk_nome{$numReg}"]);
        $this->update_tipo_fk = $this->formatarDados($_POST["update_tipo_fk{$numReg}"]);
        $this->delete_tipo_fk = $this->formatarDados($_POST["delete_tipo_fk{$numReg}"]);
        $this->label = $this->formatarDados($_POST["label{$numReg}"]);
        $this->unique_BOOLEAN = $this->formatarDados($_POST["unique_BOOLEAN{$numReg}"]);
        $this->unique_nome = $this->formatarDados($_POST["unique_nome{$numReg}"]);
        $this->seq_INT = $this->formatarDados($_POST["seq_INT{$numReg}"]);
        $this->excluido_BOOLEAN = $this->formatarDados($_POST["excluido_BOOLEAN{$numReg}"]);


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

        $this->id = $this->formatarDados($_GET["id{$numReg}"]);
        $this->nome = $this->formatarDados($_GET["nome{$numReg}"]);
        $this->sistema_tabela_id_INT = $this->formatarDados($_GET["sistema_tabela_id_INT{$numReg}"]);
        $this->tipo_sql = $this->formatarDados($_GET["tipo_sql{$numReg}"]);
        $this->tamanho_INT = $this->formatarDados($_GET["tamanho_INT{$numReg}"]);
        $this->decimal_INT = $this->formatarDados($_GET["decimal_INT{$numReg}"]);
        $this->not_null_BOOLEAN = $this->formatarDados($_GET["not_null_BOOLEAN{$numReg}"]);
        $this->primary_key_BOOLEAN = $this->formatarDados($_GET["primary_key_BOOLEAN{$numReg}"]);
        $this->auto_increment_BOOLEAN = $this->formatarDados($_GET["auto_increment_BOOLEAN{$numReg}"]);
        $this->valor_default = $this->formatarDados($_GET["valor_default{$numReg}"]);
        $this->fk_sistema_tabela_id_INT = $this->formatarDados($_GET["fk_sistema_tabela_id_INT{$numReg}"]);
        $this->atributo_fk = $this->formatarDados($_GET["atributo_fk{$numReg}"]);
        $this->fk_nome = $this->formatarDados($_GET["fk_nome{$numReg}"]);
        $this->update_tipo_fk = $this->formatarDados($_GET["update_tipo_fk{$numReg}"]);
        $this->delete_tipo_fk = $this->formatarDados($_GET["delete_tipo_fk{$numReg}"]);
        $this->label = $this->formatarDados($_GET["label{$numReg}"]);
        $this->unique_BOOLEAN = $this->formatarDados($_GET["unique_BOOLEAN{$numReg}"]);
        $this->unique_nome = $this->formatarDados($_GET["unique_nome{$numReg}"]);
        $this->seq_INT = $this->formatarDados($_GET["seq_INT{$numReg}"]);
        $this->excluido_BOOLEAN = $this->formatarDados($_GET["excluido_BOOLEAN{$numReg}"]);


    }

    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

        if(isset($tipo["nome{$numReg}"]) || $tipo == "vazio"){

            $upd.= "nome = '$this->nome', ";

        }

        if(isset($tipo["sistema_tabela_id_INT{$numReg}"]) || $tipo == "vazio"){

            $upd.= "sistema_tabela_id_INT = $this->sistema_tabela_id_INT, ";

        }

        if(isset($tipo["tipo_sql{$numReg}"]) || $tipo == "vazio"){

            $upd.= "tipo_sql = '$this->tipo_sql', ";

        }

        if(isset($tipo["tamanho_INT{$numReg}"]) || $tipo == "vazio"){

            $upd.= "tamanho_INT = $this->tamanho_INT, ";

        }

        if(isset($tipo["decimal_INT{$numReg}"]) || $tipo == "vazio"){

            $upd.= "decimal_INT = $this->decimal_INT, ";

        }

        if(isset($tipo["not_null_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

            $upd.= "not_null_BOOLEAN = $this->not_null_BOOLEAN, ";

        }

        if(isset($tipo["primary_key_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

            $upd.= "primary_key_BOOLEAN = $this->primary_key_BOOLEAN, ";

        }

        if(isset($tipo["auto_increment_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

            $upd.= "auto_increment_BOOLEAN = $this->auto_increment_BOOLEAN, ";

        }

        if(isset($tipo["valor_default{$numReg}"]) || $tipo == "vazio"){

            $upd.= "valor_default = '$this->valor_default', ";

        }

        if(isset($tipo["fk_sistema_tabela_id_INT{$numReg}"]) || $tipo == "vazio"){

            $upd.= "fk_sistema_tabela_id_INT = $this->fk_sistema_tabela_id_INT, ";

        }

        if(isset($tipo["atributo_fk{$numReg}"]) || $tipo == "vazio"){

            $upd.= "atributo_fk = '$this->atributo_fk', ";

        }

        if(isset($tipo["fk_nome{$numReg}"]) || $tipo == "vazio"){

            $upd.= "fk_nome = '$this->fk_nome', ";

        }

        if(isset($tipo["update_tipo_fk{$numReg}"]) || $tipo == "vazio"){

            $upd.= "update_tipo_fk = '$this->update_tipo_fk', ";

        }

        if(isset($tipo["delete_tipo_fk{$numReg}"]) || $tipo == "vazio"){

            $upd.= "delete_tipo_fk = '$this->delete_tipo_fk', ";

        }

        if(isset($tipo["label{$numReg}"]) || $tipo == "vazio"){

            $upd.= "label = '$this->label', ";

        }

        if(isset($tipo["unique_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

            $upd.= "unique_BOOLEAN = $this->unique_BOOLEAN, ";

        }

        if(isset($tipo["unique_nome{$numReg}"]) || $tipo == "vazio"){

            $upd.= "unique_nome = '$this->unique_nome', ";

        }

        if(isset($tipo["seq_INT{$numReg}"]) || $tipo == "vazio"){

            $upd.= "seq_INT = $this->seq_INT, ";

        }

        if(isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

            $upd.= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";

        }

        $upd = substr($upd, 0, -2);

        $sql = " UPDATE sistema_atributo SET $upd WHERE id = $id ";

        $result = $this->database->query($sql);



    }


} // classe: fim

?>
