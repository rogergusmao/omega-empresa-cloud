<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_usuario_mensagem
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_usuario_mensagem.php
    * TABELA MYSQL:    sistema_usuario_mensagem
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_usuario_mensagem extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $tag;
	public $mensagem;
	public $sistema_tipo_usuario_mensagem_id_INT;
	public $obj;
	public $usuario_INT;
	public $corporacao_INT;
	public $sistema_projetos_versao_id_INT;
	public $objSistema_tipo_usuario_mensagem;


    public $nomeEntidade;



    

	public $label_id;
	public $label_tag;
	public $label_mensagem;
	public $label_sistema_tipo_usuario_mensagem_id_INT;
	public $label_usuario_INT;
	public $label_corporacao_INT;
	public $label_sistema_projetos_versao_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_usuario_mensagem";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_tipo_usuario_mensagem(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_tipo_usuario_mensagem($this->getDatabase());
		if($this->sistema_tipo_usuario_mensagem_id_INT != null) 
		$this->obj->select($this->sistema_tipo_usuario_mensagem_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_projetos_versao(){
	if($this->objSistema_tipo_usuario_mensagem ==null){
		$this->objSistema_tipo_usuario_mensagem = new EXTDAO_Sistema_projetos_versao($this->getDatabase());
		if($this->sistema_projetos_versao_id_INT != null) 
		$this->objSistema_tipo_usuario_mensagem->select($this->sistema_projetos_versao_id_INT);
	}
	return $this->objSistema_tipo_usuario_mensagem ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_tipo_usuario_mensagem($objArgumentos){

		$objArgumentos->nome="sistema_tipo_usuario_mensagem_id_INT";
		$objArgumentos->id="sistema_tipo_usuario_mensagem_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_tipo_usuario_mensagem()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_usuario_mensagem", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_usuario_mensagem", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTag()
    {
    	return $this->tag;
    }
    
    public function getMensagem()
    {
    	return $this->mensagem;
    }
    
    public function getSistema_tipo_usuario_mensagem_id_INT()
    {
    	return $this->sistema_tipo_usuario_mensagem_id_INT;
    }
    
    public function getUsuario_INT()
    {
    	return $this->usuario_INT;
    }
    
    public function getCorporacao_INT()
    {
    	return $this->corporacao_INT;
    }
    
    public function getSistema_projetos_versao_id_INT()
    {
    	return $this->sistema_projetos_versao_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTag($val)
    {
    	$this->tag =  $val;
    }
    
    function setMensagem($val)
    {
    	$this->mensagem =  $val;
    }
    
    function setSistema_tipo_usuario_mensagem_id_INT($val)
    {
    	$this->sistema_tipo_usuario_mensagem_id_INT =  $val;
    }
    
    function setUsuario_INT($val)
    {
    	$this->usuario_INT =  $val;
    }
    
    function setCorporacao_INT($val)
    {
    	$this->corporacao_INT =  $val;
    }
    
    function setSistema_projetos_versao_id_INT($val)
    {
    	$this->sistema_projetos_versao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema_usuario_mensagem WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->tag = $row->tag;
        
        $this->mensagem = $row->mensagem;
        
        $this->sistema_tipo_usuario_mensagem_id_INT = $row->sistema_tipo_usuario_mensagem_id_INT;
        if(isset($this->objSistema_tipo_usuario_mensagem))
			$this->objSistema_tipo_usuario_mensagem->select($this->sistema_tipo_usuario_mensagem_id_INT);

        $this->usuario_INT = $row->usuario_INT;
        
        $this->corporacao_INT = $row->corporacao_INT;
        
        $this->sistema_projetos_versao_id_INT = $row->sistema_projetos_versao_id_INT;
        if(isset($this->objSistema_projetos_versao))
			$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_usuario_mensagem WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_usuario_mensagem ( tag , mensagem , sistema_tipo_usuario_mensagem_id_INT , usuario_INT , corporacao_INT , sistema_projetos_versao_id_INT ) VALUES ( {$this->tag} , {$this->mensagem} , {$this->sistema_tipo_usuario_mensagem_id_INT} , {$this->usuario_INT} , {$this->corporacao_INT} , {$this->sistema_projetos_versao_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTag(){ 

		return "tag";

	}

	public function nomeCampoMensagem(){ 

		return "mensagem";

	}

	public function nomeCampoSistema_tipo_usuario_mensagem_id_INT(){ 

		return "sistema_tipo_usuario_mensagem_id_INT";

	}

	public function nomeCampoUsuario_INT(){ 

		return "usuario_INT";

	}

	public function nomeCampoCorporacao_INT(){ 

		return "corporacao_INT";

	}

	public function nomeCampoSistema_projetos_versao_id_INT(){ 

		return "sistema_projetos_versao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTag($objArguments){

		$objArguments->nome = "tag";
		$objArguments->id = "tag";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoMensagem($objArguments){

		$objArguments->nome = "mensagem";
		$objArguments->id = "mensagem";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSistema_tipo_usuario_mensagem_id_INT($objArguments){

		$objArguments->nome = "sistema_tipo_usuario_mensagem_id_INT";
		$objArguments->id = "sistema_tipo_usuario_mensagem_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUsuario_INT($objArguments){

		$objArguments->nome = "usuario_INT";
		$objArguments->id = "usuario_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorporacao_INT($objArguments){

		$objArguments->nome = "corporacao_INT";
		$objArguments->id = "corporacao_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_id_INT";
		$objArguments->id = "sistema_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->tag = $this->formatarDadosParaSQL($this->tag);
			$this->mensagem = $this->formatarDadosParaSQL($this->mensagem);
		if($this->sistema_tipo_usuario_mensagem_id_INT == ""){

			$this->sistema_tipo_usuario_mensagem_id_INT = "null";

		}

		if($this->usuario_INT == ""){

			$this->usuario_INT = "null";

		}

		if($this->corporacao_INT == ""){

			$this->corporacao_INT = "null";

		}

		if($this->sistema_projetos_versao_id_INT == ""){

			$this->sistema_projetos_versao_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->tag = null; 
		$this->mensagem = null; 
		$this->sistema_tipo_usuario_mensagem_id_INT = null; 
		$this->objSistema_tipo_usuario_mensagem= null;
		$this->usuario_INT = null; 
		$this->corporacao_INT = null; 
		$this->sistema_projetos_versao_id_INT = null; 
		$this->objSistema_projetos_versao= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("tag", $this->tag); 
		Helper::setSession("mensagem", $this->mensagem); 
		Helper::setSession("sistema_tipo_usuario_mensagem_id_INT", $this->sistema_tipo_usuario_mensagem_id_INT); 
		Helper::setSession("usuario_INT", $this->usuario_INT); 
		Helper::setSession("corporacao_INT", $this->corporacao_INT); 
		Helper::setSession("sistema_projetos_versao_id_INT", $this->sistema_projetos_versao_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("tag");
		Helper::clearSession("mensagem");
		Helper::clearSession("sistema_tipo_usuario_mensagem_id_INT");
		Helper::clearSession("usuario_INT");
		Helper::clearSession("corporacao_INT");
		Helper::clearSession("sistema_projetos_versao_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->tag = Helper::SESSION("tag{$numReg}"); 
		$this->mensagem = Helper::SESSION("mensagem{$numReg}"); 
		$this->sistema_tipo_usuario_mensagem_id_INT = Helper::SESSION("sistema_tipo_usuario_mensagem_id_INT{$numReg}"); 
		$this->usuario_INT = Helper::SESSION("usuario_INT{$numReg}"); 
		$this->corporacao_INT = Helper::SESSION("corporacao_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::SESSION("sistema_projetos_versao_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->tag = Helper::POST("tag{$numReg}"); 
		$this->mensagem = Helper::POST("mensagem{$numReg}"); 
		$this->sistema_tipo_usuario_mensagem_id_INT = Helper::POST("sistema_tipo_usuario_mensagem_id_INT{$numReg}"); 
		$this->usuario_INT = Helper::POST("usuario_INT{$numReg}"); 
		$this->corporacao_INT = Helper::POST("corporacao_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::POST("sistema_projetos_versao_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->tag = Helper::GET("tag{$numReg}"); 
		$this->mensagem = Helper::GET("mensagem{$numReg}"); 
		$this->sistema_tipo_usuario_mensagem_id_INT = Helper::GET("sistema_tipo_usuario_mensagem_id_INT{$numReg}"); 
		$this->usuario_INT = Helper::GET("usuario_INT{$numReg}"); 
		$this->corporacao_INT = Helper::GET("corporacao_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::GET("sistema_projetos_versao_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["tag{$numReg}"]) || $tipo == null){

		$upd.= "tag = $this->tag, ";

	} 

	if(isset($tipo["mensagem{$numReg}"]) || $tipo == null){

		$upd.= "mensagem = $this->mensagem, ";

	} 

	if(isset($tipo["sistema_tipo_usuario_mensagem_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_tipo_usuario_mensagem_id_INT = $this->sistema_tipo_usuario_mensagem_id_INT, ";

	} 

	if(isset($tipo["usuario_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_INT = $this->usuario_INT, ";

	} 

	if(isset($tipo["corporacao_INT{$numReg}"]) || $tipo == null){

		$upd.= "corporacao_INT = $this->corporacao_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_id_INT = $this->sistema_projetos_versao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_usuario_mensagem SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    