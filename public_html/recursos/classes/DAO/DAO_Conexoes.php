<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Conexoes
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Conexoes.php
    * TABELA MYSQL:    conexoes
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Conexoes extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $hostnameBanco;
	public $usuarioBanco;
	public $senhaBanco;
	public $portaBanco;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_hostnameBanco;
	public $label_usuarioBanco;
	public $label_senhaBanco;
	public $label_portaBanco;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "conexoes";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_conexoes", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_conexoes", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getHostnameBanco()
    {
    	return $this->hostnameBanco;
    }
    
    public function getUsuarioBanco()
    {
    	return $this->usuarioBanco;
    }
    
    public function getSenhaBanco()
    {
    	return $this->senhaBanco;
    }
    
    public function getPortaBanco()
    {
    	return $this->portaBanco;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setHostnameBanco($val)
    {
    	$this->hostnameBanco =  $val;
    }
    
    function setUsuarioBanco($val)
    {
    	$this->usuarioBanco =  $val;
    }
    
    function setSenhaBanco($val)
    {
    	$this->senhaBanco =  $val;
    }
    
    function setPortaBanco($val)
    {
    	$this->portaBanco =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM conexoes WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->hostnameBanco = $row->hostnameBanco;
        
        $this->usuarioBanco = $row->usuarioBanco;
        
        $this->senhaBanco = $row->senhaBanco;
        
        $this->portaBanco = $row->portaBanco;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM conexoes WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO conexoes ( nome , hostnameBanco , usuarioBanco , senhaBanco , portaBanco ) VALUES ( {$this->nome} , {$this->hostnameBanco} , {$this->usuarioBanco} , {$this->senhaBanco} , {$this->portaBanco} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoHostnameBanco(){ 

		return "hostnameBanco";

	}

	public function nomeCampoUsuarioBanco(){ 

		return "usuarioBanco";

	}

	public function nomeCampoSenhaBanco(){ 

		return "senhaBanco";

	}

	public function nomeCampoPortaBanco(){ 

		return "portaBanco";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoHostnameBanco($objArguments){

		$objArguments->nome = "hostnameBanco";
		$objArguments->id = "hostnameBanco";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUsuarioBanco($objArguments){

		$objArguments->nome = "usuarioBanco";
		$objArguments->id = "usuarioBanco";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSenhaBanco($objArguments){

		$objArguments->nome = "senhaBanco";
		$objArguments->id = "senhaBanco";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoPortaBanco($objArguments){

		$objArguments->nome = "portaBanco";
		$objArguments->id = "portaBanco";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->hostnameBanco = $this->formatarDadosParaSQL($this->hostnameBanco);
			$this->usuarioBanco = $this->formatarDadosParaSQL($this->usuarioBanco);
			$this->senhaBanco = $this->formatarDadosParaSQL($this->senhaBanco);
			$this->portaBanco = $this->formatarDadosParaSQL($this->portaBanco);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->hostnameBanco = null; 
		$this->usuarioBanco = null; 
		$this->senhaBanco = null; 
		$this->portaBanco = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("hostnameBanco", $this->hostnameBanco); 
		Helper::setSession("usuarioBanco", $this->usuarioBanco); 
		Helper::setSession("senhaBanco", $this->senhaBanco); 
		Helper::setSession("portaBanco", $this->portaBanco); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("hostnameBanco");
		Helper::clearSession("usuarioBanco");
		Helper::clearSession("senhaBanco");
		Helper::clearSession("portaBanco");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->hostnameBanco = Helper::SESSION("hostnameBanco{$numReg}"); 
		$this->usuarioBanco = Helper::SESSION("usuarioBanco{$numReg}"); 
		$this->senhaBanco = Helper::SESSION("senhaBanco{$numReg}"); 
		$this->portaBanco = Helper::SESSION("portaBanco{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->hostnameBanco = Helper::POST("hostnameBanco{$numReg}"); 
		$this->usuarioBanco = Helper::POST("usuarioBanco{$numReg}"); 
		$this->senhaBanco = Helper::POST("senhaBanco{$numReg}"); 
		$this->portaBanco = Helper::POST("portaBanco{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->hostnameBanco = Helper::GET("hostnameBanco{$numReg}"); 
		$this->usuarioBanco = Helper::GET("usuarioBanco{$numReg}"); 
		$this->senhaBanco = Helper::GET("senhaBanco{$numReg}"); 
		$this->portaBanco = Helper::GET("portaBanco{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["hostnameBanco{$numReg}"]) || $tipo == null){

		$upd.= "hostnameBanco = $this->hostnameBanco, ";

	} 

	if(isset($tipo["usuarioBanco{$numReg}"]) || $tipo == null){

		$upd.= "usuarioBanco = $this->usuarioBanco, ";

	} 

	if(isset($tipo["senhaBanco{$numReg}"]) || $tipo == null){

		$upd.= "senhaBanco = $this->senhaBanco, ";

	} 

	if(isset($tipo["portaBanco{$numReg}"]) || $tipo == null){

		$upd.= "portaBanco = $this->portaBanco, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE conexoes SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    