<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tabela_tabela
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Tabela_tabela.php
    * TABELA MYSQL:    tabela_tabela
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Tabela_tabela extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $homologacao_tabela_id_INT;
	public $obj;
	public $producao_tabela_id_INT;
	public $objHomologacao_tabela;
	public $tipo_operacao_atualizacao_banco_id_INT;
	public $objProducao_tabela;
	public $projetos_versao_id_INT;
	public $objTipo_operacao_atualizacao_banco;
	public $status_verificacao_BOOLEAN;
	public $inserir_tuplas_na_homologacao_BOOLEAN;
	public $remover_tuplas_na_homologacao_BOOLEAN;
	public $editar_tuplas_na_homologacao_BOOLEAN;
	public $tabela_sistema_BOOLEAN;
	public $projetos_versao_banco_banco_id_INT;
	public $objProjetos_versao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_homologacao_tabela_id_INT;
	public $label_producao_tabela_id_INT;
	public $label_tipo_operacao_atualizacao_banco_id_INT;
	public $label_projetos_versao_id_INT;
	public $label_status_verificacao_BOOLEAN;
	public $label_inserir_tuplas_na_homologacao_BOOLEAN;
	public $label_remover_tuplas_na_homologacao_BOOLEAN;
	public $label_editar_tuplas_na_homologacao_BOOLEAN;
	public $label_tabela_sistema_BOOLEAN;
	public $label_projetos_versao_banco_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "tabela_tabela";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjHomologacao_tabela(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tabela($this->getDatabase());
		if($this->homologacao_tabela_id_INT != null) 
		$this->obj->select($this->homologacao_tabela_id_INT);
	}
	return $this->obj ;
}
function getFkObjProducao_tabela(){
	if($this->objHomologacao_tabela ==null){
		$this->objHomologacao_tabela = new EXTDAO_Tabela($this->getDatabase());
		if($this->producao_tabela_id_INT != null) 
		$this->objHomologacao_tabela->select($this->producao_tabela_id_INT);
	}
	return $this->objHomologacao_tabela ;
}
function getFkObjTipo_operacao_atualizacao_banco(){
	if($this->objProducao_tabela ==null){
		$this->objProducao_tabela = new EXTDAO_Tipo_operacao_atualizacao_banco($this->getDatabase());
		if($this->tipo_operacao_atualizacao_banco_id_INT != null) 
		$this->objProducao_tabela->select($this->tipo_operacao_atualizacao_banco_id_INT);
	}
	return $this->objProducao_tabela ;
}
function getFkObjProjetos_versao(){
	if($this->objTipo_operacao_atualizacao_banco ==null){
		$this->objTipo_operacao_atualizacao_banco = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objTipo_operacao_atualizacao_banco->select($this->projetos_versao_id_INT);
	}
	return $this->objTipo_operacao_atualizacao_banco ;
}
function getFkObjProjetos_versao_banco_banco(){
	if($this->objProjetos_versao ==null){
		$this->objProjetos_versao = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->objProjetos_versao->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->objProjetos_versao ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllHomologacao_tabela($objArgumentos){

		$objArgumentos->nome="homologacao_tabela_id_INT";
		$objArgumentos->id="homologacao_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjHomologacao_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProducao_tabela($objArgumentos){

		$objArgumentos->nome="producao_tabela_id_INT";
		$objArgumentos->id="producao_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProducao_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_atualizacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_tabela_tabela", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tabela_tabela", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHomologacao_tabela_id_INT()
    {
    	return $this->homologacao_tabela_id_INT;
    }
    
    public function getProducao_tabela_id_INT()
    {
    	return $this->producao_tabela_id_INT;
    }
    
    public function getTipo_operacao_atualizacao_banco_id_INT()
    {
    	return $this->tipo_operacao_atualizacao_banco_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    public function getStatus_verificacao_BOOLEAN()
    {
    	return $this->status_verificacao_BOOLEAN;
    }
    
    public function getInserir_tuplas_na_homologacao_BOOLEAN()
    {
    	return $this->inserir_tuplas_na_homologacao_BOOLEAN;
    }
    
    public function getRemover_tuplas_na_homologacao_BOOLEAN()
    {
    	return $this->remover_tuplas_na_homologacao_BOOLEAN;
    }
    
    public function getEditar_tuplas_na_homologacao_BOOLEAN()
    {
    	return $this->editar_tuplas_na_homologacao_BOOLEAN;
    }
    
    public function getTabela_sistema_BOOLEAN()
    {
    	return $this->tabela_sistema_BOOLEAN;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHomologacao_tabela_id_INT($val)
    {
    	$this->homologacao_tabela_id_INT =  $val;
    }
    
    function setProducao_tabela_id_INT($val)
    {
    	$this->producao_tabela_id_INT =  $val;
    }
    
    function setTipo_operacao_atualizacao_banco_id_INT($val)
    {
    	$this->tipo_operacao_atualizacao_banco_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    
    function setStatus_verificacao_BOOLEAN($val)
    {
    	$this->status_verificacao_BOOLEAN =  $val;
    }
    
    function setInserir_tuplas_na_homologacao_BOOLEAN($val)
    {
    	$this->inserir_tuplas_na_homologacao_BOOLEAN =  $val;
    }
    
    function setRemover_tuplas_na_homologacao_BOOLEAN($val)
    {
    	$this->remover_tuplas_na_homologacao_BOOLEAN =  $val;
    }
    
    function setEditar_tuplas_na_homologacao_BOOLEAN($val)
    {
    	$this->editar_tuplas_na_homologacao_BOOLEAN =  $val;
    }
    
    function setTabela_sistema_BOOLEAN($val)
    {
    	$this->tabela_sistema_BOOLEAN =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM tabela_tabela WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->homologacao_tabela_id_INT = $row->homologacao_tabela_id_INT;
        if(isset($this->objHomologacao_tabela))
			$this->objHomologacao_tabela->select($this->homologacao_tabela_id_INT);

        $this->producao_tabela_id_INT = $row->producao_tabela_id_INT;
        if(isset($this->objProducao_tabela))
			$this->objProducao_tabela->select($this->producao_tabela_id_INT);

        $this->tipo_operacao_atualizacao_banco_id_INT = $row->tipo_operacao_atualizacao_banco_id_INT;
        if(isset($this->objTipo_operacao_atualizacao_banco))
			$this->objTipo_operacao_atualizacao_banco->select($this->tipo_operacao_atualizacao_banco_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

        $this->status_verificacao_BOOLEAN = $row->status_verificacao_BOOLEAN;
        
        $this->inserir_tuplas_na_homologacao_BOOLEAN = $row->inserir_tuplas_na_homologacao_BOOLEAN;
        
        $this->remover_tuplas_na_homologacao_BOOLEAN = $row->remover_tuplas_na_homologacao_BOOLEAN;
        
        $this->editar_tuplas_na_homologacao_BOOLEAN = $row->editar_tuplas_na_homologacao_BOOLEAN;
        
        $this->tabela_sistema_BOOLEAN = $row->tabela_sistema_BOOLEAN;
        
        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM tabela_tabela WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO tabela_tabela ( homologacao_tabela_id_INT , producao_tabela_id_INT , tipo_operacao_atualizacao_banco_id_INT , projetos_versao_id_INT , status_verificacao_BOOLEAN , inserir_tuplas_na_homologacao_BOOLEAN , remover_tuplas_na_homologacao_BOOLEAN , editar_tuplas_na_homologacao_BOOLEAN , tabela_sistema_BOOLEAN , projetos_versao_banco_banco_id_INT ) VALUES ( {$this->homologacao_tabela_id_INT} , {$this->producao_tabela_id_INT} , {$this->tipo_operacao_atualizacao_banco_id_INT} , {$this->projetos_versao_id_INT} , {$this->status_verificacao_BOOLEAN} , {$this->inserir_tuplas_na_homologacao_BOOLEAN} , {$this->remover_tuplas_na_homologacao_BOOLEAN} , {$this->editar_tuplas_na_homologacao_BOOLEAN} , {$this->tabela_sistema_BOOLEAN} , {$this->projetos_versao_banco_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHomologacao_tabela_id_INT(){ 

		return "homologacao_tabela_id_INT";

	}

	public function nomeCampoProducao_tabela_id_INT(){ 

		return "producao_tabela_id_INT";

	}

	public function nomeCampoTipo_operacao_atualizacao_banco_id_INT(){ 

		return "tipo_operacao_atualizacao_banco_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}

	public function nomeCampoStatus_verificacao_BOOLEAN(){ 

		return "status_verificacao_BOOLEAN";

	}

	public function nomeCampoInserir_tuplas_na_homologacao_BOOLEAN(){ 

		return "inserir_tuplas_na_homologacao_BOOLEAN";

	}

	public function nomeCampoRemover_tuplas_na_homologacao_BOOLEAN(){ 

		return "remover_tuplas_na_homologacao_BOOLEAN";

	}

	public function nomeCampoEditar_tuplas_na_homologacao_BOOLEAN(){ 

		return "editar_tuplas_na_homologacao_BOOLEAN";

	}

	public function nomeCampoTabela_sistema_BOOLEAN(){ 

		return "tabela_sistema_BOOLEAN";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHomologacao_tabela_id_INT($objArguments){

		$objArguments->nome = "homologacao_tabela_id_INT";
		$objArguments->id = "homologacao_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProducao_tabela_id_INT($objArguments){

		$objArguments->nome = "producao_tabela_id_INT";
		$objArguments->id = "producao_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao_atualizacao_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_atualizacao_banco_id_INT";
		$objArguments->id = "tipo_operacao_atualizacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoStatus_verificacao_BOOLEAN($objArguments){

		$objArguments->nome = "status_verificacao_BOOLEAN";
		$objArguments->id = "status_verificacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoInserir_tuplas_na_homologacao_BOOLEAN($objArguments){

		$objArguments->nome = "inserir_tuplas_na_homologacao_BOOLEAN";
		$objArguments->id = "inserir_tuplas_na_homologacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoRemover_tuplas_na_homologacao_BOOLEAN($objArguments){

		$objArguments->nome = "remover_tuplas_na_homologacao_BOOLEAN";
		$objArguments->id = "remover_tuplas_na_homologacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoEditar_tuplas_na_homologacao_BOOLEAN($objArguments){

		$objArguments->nome = "editar_tuplas_na_homologacao_BOOLEAN";
		$objArguments->id = "editar_tuplas_na_homologacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoTabela_sistema_BOOLEAN($objArguments){

		$objArguments->nome = "tabela_sistema_BOOLEAN";
		$objArguments->id = "tabela_sistema_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->homologacao_tabela_id_INT == ""){

			$this->homologacao_tabela_id_INT = "null";

		}

		if($this->producao_tabela_id_INT == ""){

			$this->producao_tabela_id_INT = "null";

		}

		if($this->tipo_operacao_atualizacao_banco_id_INT == ""){

			$this->tipo_operacao_atualizacao_banco_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}

		if($this->status_verificacao_BOOLEAN == ""){

			$this->status_verificacao_BOOLEAN = "null";

		}

		if($this->inserir_tuplas_na_homologacao_BOOLEAN == ""){

			$this->inserir_tuplas_na_homologacao_BOOLEAN = "null";

		}

		if($this->remover_tuplas_na_homologacao_BOOLEAN == ""){

			$this->remover_tuplas_na_homologacao_BOOLEAN = "null";

		}

		if($this->editar_tuplas_na_homologacao_BOOLEAN == ""){

			$this->editar_tuplas_na_homologacao_BOOLEAN = "null";

		}

		if($this->tabela_sistema_BOOLEAN == ""){

			$this->tabela_sistema_BOOLEAN = "null";

		}

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->homologacao_tabela_id_INT = null; 
		$this->objHomologacao_tabela= null;
		$this->producao_tabela_id_INT = null; 
		$this->objProducao_tabela= null;
		$this->tipo_operacao_atualizacao_banco_id_INT = null; 
		$this->objTipo_operacao_atualizacao_banco= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;
		$this->status_verificacao_BOOLEAN = null; 
		$this->inserir_tuplas_na_homologacao_BOOLEAN = null; 
		$this->remover_tuplas_na_homologacao_BOOLEAN = null; 
		$this->editar_tuplas_na_homologacao_BOOLEAN = null; 
		$this->tabela_sistema_BOOLEAN = null; 
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("homologacao_tabela_id_INT", $this->homologacao_tabela_id_INT); 
		Helper::setSession("producao_tabela_id_INT", $this->producao_tabela_id_INT); 
		Helper::setSession("tipo_operacao_atualizacao_banco_id_INT", $this->tipo_operacao_atualizacao_banco_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 
		Helper::setSession("status_verificacao_BOOLEAN", $this->status_verificacao_BOOLEAN); 
		Helper::setSession("inserir_tuplas_na_homologacao_BOOLEAN", $this->inserir_tuplas_na_homologacao_BOOLEAN); 
		Helper::setSession("remover_tuplas_na_homologacao_BOOLEAN", $this->remover_tuplas_na_homologacao_BOOLEAN); 
		Helper::setSession("editar_tuplas_na_homologacao_BOOLEAN", $this->editar_tuplas_na_homologacao_BOOLEAN); 
		Helper::setSession("tabela_sistema_BOOLEAN", $this->tabela_sistema_BOOLEAN); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("homologacao_tabela_id_INT");
		Helper::clearSession("producao_tabela_id_INT");
		Helper::clearSession("tipo_operacao_atualizacao_banco_id_INT");
		Helper::clearSession("projetos_versao_id_INT");
		Helper::clearSession("status_verificacao_BOOLEAN");
		Helper::clearSession("inserir_tuplas_na_homologacao_BOOLEAN");
		Helper::clearSession("remover_tuplas_na_homologacao_BOOLEAN");
		Helper::clearSession("editar_tuplas_na_homologacao_BOOLEAN");
		Helper::clearSession("tabela_sistema_BOOLEAN");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->homologacao_tabela_id_INT = Helper::SESSION("homologacao_tabela_id_INT{$numReg}"); 
		$this->producao_tabela_id_INT = Helper::SESSION("producao_tabela_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::SESSION("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 
		$this->status_verificacao_BOOLEAN = Helper::SESSION("status_verificacao_BOOLEAN{$numReg}"); 
		$this->inserir_tuplas_na_homologacao_BOOLEAN = Helper::SESSION("inserir_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->remover_tuplas_na_homologacao_BOOLEAN = Helper::SESSION("remover_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->editar_tuplas_na_homologacao_BOOLEAN = Helper::SESSION("editar_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->tabela_sistema_BOOLEAN = Helper::SESSION("tabela_sistema_BOOLEAN{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->homologacao_tabela_id_INT = Helper::POST("homologacao_tabela_id_INT{$numReg}"); 
		$this->producao_tabela_id_INT = Helper::POST("producao_tabela_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::POST("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 
		$this->status_verificacao_BOOLEAN = Helper::POST("status_verificacao_BOOLEAN{$numReg}"); 
		$this->inserir_tuplas_na_homologacao_BOOLEAN = Helper::POST("inserir_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->remover_tuplas_na_homologacao_BOOLEAN = Helper::POST("remover_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->editar_tuplas_na_homologacao_BOOLEAN = Helper::POST("editar_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->tabela_sistema_BOOLEAN = Helper::POST("tabela_sistema_BOOLEAN{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->homologacao_tabela_id_INT = Helper::GET("homologacao_tabela_id_INT{$numReg}"); 
		$this->producao_tabela_id_INT = Helper::GET("producao_tabela_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::GET("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 
		$this->status_verificacao_BOOLEAN = Helper::GET("status_verificacao_BOOLEAN{$numReg}"); 
		$this->inserir_tuplas_na_homologacao_BOOLEAN = Helper::GET("inserir_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->remover_tuplas_na_homologacao_BOOLEAN = Helper::GET("remover_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->editar_tuplas_na_homologacao_BOOLEAN = Helper::GET("editar_tuplas_na_homologacao_BOOLEAN{$numReg}"); 
		$this->tabela_sistema_BOOLEAN = Helper::GET("tabela_sistema_BOOLEAN{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["homologacao_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "homologacao_tabela_id_INT = $this->homologacao_tabela_id_INT, ";

	} 

	if(isset($tipo["producao_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "producao_tabela_id_INT = $this->producao_tabela_id_INT, ";

	} 

	if(isset($tipo["tipo_operacao_atualizacao_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_atualizacao_banco_id_INT = $this->tipo_operacao_atualizacao_banco_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

	if(isset($tipo["status_verificacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "status_verificacao_BOOLEAN = $this->status_verificacao_BOOLEAN, ";

	} 

	if(isset($tipo["inserir_tuplas_na_homologacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "inserir_tuplas_na_homologacao_BOOLEAN = $this->inserir_tuplas_na_homologacao_BOOLEAN, ";

	} 

	if(isset($tipo["remover_tuplas_na_homologacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "remover_tuplas_na_homologacao_BOOLEAN = $this->remover_tuplas_na_homologacao_BOOLEAN, ";

	} 

	if(isset($tipo["editar_tuplas_na_homologacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "editar_tuplas_na_homologacao_BOOLEAN = $this->editar_tuplas_na_homologacao_BOOLEAN, ";

	} 

	if(isset($tipo["tabela_sistema_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "tabela_sistema_BOOLEAN = $this->tabela_sistema_BOOLEAN, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE tabela_tabela SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    