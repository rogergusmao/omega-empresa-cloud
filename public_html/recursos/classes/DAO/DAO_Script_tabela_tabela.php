<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Script_tabela_tabela
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Script_tabela_tabela.php
    * TABELA MYSQL:    script_tabela_tabela
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Script_tabela_tabela extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $tipo_operacao_atualizacao_banco_id_INT;
	public $obj;
	public $tabela_tabela_id_INT;
	public $objTipo_operacao_atualizacao_banco;
	public $script_projetos_versao_banco_banco_id_INT;
	public $objTabela_tabela;


    public $nomeEntidade;



    

	public $label_id;
	public $label_tipo_operacao_atualizacao_banco_id_INT;
	public $label_tabela_tabela_id_INT;
	public $label_script_projetos_versao_banco_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "script_tabela_tabela";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTipo_operacao_atualizacao_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tipo_operacao_atualizacao_banco($this->getDatabase());
		if($this->tipo_operacao_atualizacao_banco_id_INT != null) 
		$this->obj->select($this->tipo_operacao_atualizacao_banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjTabela_tabela(){
	if($this->objTipo_operacao_atualizacao_banco ==null){
		$this->objTipo_operacao_atualizacao_banco = new EXTDAO_Tabela_tabela($this->getDatabase());
		if($this->tabela_tabela_id_INT != null) 
		$this->objTipo_operacao_atualizacao_banco->select($this->tabela_tabela_id_INT);
	}
	return $this->objTipo_operacao_atualizacao_banco ;
}
function getFkObjScript_projetos_versao_banco_banco(){
	if($this->objTabela_tabela ==null){
		$this->objTabela_tabela = new EXTDAO_Script_projetos_versao_banco_banco($this->getDatabase());
		if($this->script_projetos_versao_banco_banco_id_INT != null) 
		$this->objTabela_tabela->select($this->script_projetos_versao_banco_banco_id_INT);
	}
	return $this->objTabela_tabela ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTipo_operacao_atualizacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTabela_tabela($objArgumentos){

		$objArgumentos->nome="tabela_tabela_id_INT";
		$objArgumentos->id="tabela_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllScript_projetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="script_projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="script_projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjScript_projetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_script_tabela_tabela", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_script_tabela_tabela", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTipo_operacao_atualizacao_banco_id_INT()
    {
    	return $this->tipo_operacao_atualizacao_banco_id_INT;
    }
    
    public function getTabela_tabela_id_INT()
    {
    	return $this->tabela_tabela_id_INT;
    }
    
    public function getScript_projetos_versao_banco_banco_id_INT()
    {
    	return $this->script_projetos_versao_banco_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTipo_operacao_atualizacao_banco_id_INT($val)
    {
    	$this->tipo_operacao_atualizacao_banco_id_INT =  $val;
    }
    
    function setTabela_tabela_id_INT($val)
    {
    	$this->tabela_tabela_id_INT =  $val;
    }
    
    function setScript_projetos_versao_banco_banco_id_INT($val)
    {
    	$this->script_projetos_versao_banco_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM script_tabela_tabela WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->tipo_operacao_atualizacao_banco_id_INT = $row->tipo_operacao_atualizacao_banco_id_INT;
        if(isset($this->objTipo_operacao_atualizacao_banco))
			$this->objTipo_operacao_atualizacao_banco->select($this->tipo_operacao_atualizacao_banco_id_INT);

        $this->tabela_tabela_id_INT = $row->tabela_tabela_id_INT;
        if(isset($this->objTabela_tabela))
			$this->objTabela_tabela->select($this->tabela_tabela_id_INT);

        $this->script_projetos_versao_banco_banco_id_INT = $row->script_projetos_versao_banco_banco_id_INT;
        if(isset($this->objScript_projetos_versao_banco_banco))
			$this->objScript_projetos_versao_banco_banco->select($this->script_projetos_versao_banco_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM script_tabela_tabela WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO script_tabela_tabela ( tipo_operacao_atualizacao_banco_id_INT , tabela_tabela_id_INT , script_projetos_versao_banco_banco_id_INT ) VALUES ( {$this->tipo_operacao_atualizacao_banco_id_INT} , {$this->tabela_tabela_id_INT} , {$this->script_projetos_versao_banco_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTipo_operacao_atualizacao_banco_id_INT(){ 

		return "tipo_operacao_atualizacao_banco_id_INT";

	}

	public function nomeCampoTabela_tabela_id_INT(){ 

		return "tabela_tabela_id_INT";

	}

	public function nomeCampoScript_projetos_versao_banco_banco_id_INT(){ 

		return "script_projetos_versao_banco_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTipo_operacao_atualizacao_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_atualizacao_banco_id_INT";
		$objArguments->id = "tipo_operacao_atualizacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTabela_tabela_id_INT($objArguments){

		$objArguments->nome = "tabela_tabela_id_INT";
		$objArguments->id = "tabela_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoScript_projetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "script_projetos_versao_banco_banco_id_INT";
		$objArguments->id = "script_projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->tipo_operacao_atualizacao_banco_id_INT == ""){

			$this->tipo_operacao_atualizacao_banco_id_INT = "null";

		}

		if($this->tabela_tabela_id_INT == ""){

			$this->tabela_tabela_id_INT = "null";

		}

		if($this->script_projetos_versao_banco_banco_id_INT == ""){

			$this->script_projetos_versao_banco_banco_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->tipo_operacao_atualizacao_banco_id_INT = null; 
		$this->objTipo_operacao_atualizacao_banco= null;
		$this->tabela_tabela_id_INT = null; 
		$this->objTabela_tabela= null;
		$this->script_projetos_versao_banco_banco_id_INT = null; 
		$this->objScript_projetos_versao_banco_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("tipo_operacao_atualizacao_banco_id_INT", $this->tipo_operacao_atualizacao_banco_id_INT); 
		Helper::setSession("tabela_tabela_id_INT", $this->tabela_tabela_id_INT); 
		Helper::setSession("script_projetos_versao_banco_banco_id_INT", $this->script_projetos_versao_banco_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("tipo_operacao_atualizacao_banco_id_INT");
		Helper::clearSession("tabela_tabela_id_INT");
		Helper::clearSession("script_projetos_versao_banco_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::SESSION("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->tabela_tabela_id_INT = Helper::SESSION("tabela_tabela_id_INT{$numReg}"); 
		$this->script_projetos_versao_banco_banco_id_INT = Helper::SESSION("script_projetos_versao_banco_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::POST("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->tabela_tabela_id_INT = Helper::POST("tabela_tabela_id_INT{$numReg}"); 
		$this->script_projetos_versao_banco_banco_id_INT = Helper::POST("script_projetos_versao_banco_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::GET("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->tabela_tabela_id_INT = Helper::GET("tabela_tabela_id_INT{$numReg}"); 
		$this->script_projetos_versao_banco_banco_id_INT = Helper::GET("script_projetos_versao_banco_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["tipo_operacao_atualizacao_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_atualizacao_banco_id_INT = $this->tipo_operacao_atualizacao_banco_id_INT, ";

	} 

	if(isset($tipo["tabela_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_tabela_id_INT = $this->tabela_tabela_id_INT, ";

	} 

	if(isset($tipo["script_projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "script_projetos_versao_banco_banco_id_INT = $this->script_projetos_versao_banco_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE script_tabela_tabela SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    