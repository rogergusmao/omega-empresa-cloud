<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Trunk_historico
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Trunk_historico.php
    * TABELA MYSQL:    trunk_historico
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Trunk_historico extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_tipo_conjunto_analise_id_INT;
	public $obj;
	public $inicio_DATETIME;
	public $fim_DATETIME;
	public $seq_INT;


    public $nomeEntidade;

	public $inicio_DATETIME_UNIX;
	public $fim_DATETIME_UNIX;


    

	public $label_id;
	public $label_projetos_tipo_conjunto_analise_id_INT;
	public $label_inicio_DATETIME;
	public $label_fim_DATETIME;
	public $label_seq_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "trunk_historico";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_tipo_conjunto_analise(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_tipo_conjunto_analise($this->getDatabase());
		if($this->projetos_tipo_conjunto_analise_id_INT != null) 
		$this->obj->select($this->projetos_tipo_conjunto_analise_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_tipo_conjunto_analise($objArgumentos){

		$objArgumentos->nome="projetos_tipo_conjunto_analise_id_INT";
		$objArgumentos->id="projetos_tipo_conjunto_analise_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_tipo_conjunto_analise()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_trunk_historico", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_trunk_historico", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_tipo_conjunto_analise_id_INT()
    {
    	return $this->projetos_tipo_conjunto_analise_id_INT;
    }
    
    function getInicio_DATETIME_UNIX()
    {
    	return $this->inicio_DATETIME_UNIX;
    }
    
    public function getInicio_DATETIME()
    {
    	return $this->inicio_DATETIME;
    }
    
    function getFim_DATETIME_UNIX()
    {
    	return $this->fim_DATETIME_UNIX;
    }
    
    public function getFim_DATETIME()
    {
    	return $this->fim_DATETIME;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_tipo_conjunto_analise_id_INT($val)
    {
    	$this->projetos_tipo_conjunto_analise_id_INT =  $val;
    }
    
    function setInicio_DATETIME($val)
    {
    	$this->inicio_DATETIME =  $val;
    }
    
    function setFim_DATETIME($val)
    {
    	$this->fim_DATETIME =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(inicio_DATETIME) AS inicio_DATETIME_UNIX, UNIX_TIMESTAMP(fim_DATETIME) AS fim_DATETIME_UNIX FROM trunk_historico WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_tipo_conjunto_analise_id_INT = $row->projetos_tipo_conjunto_analise_id_INT;
        if(isset($this->objProjetos_tipo_conjunto_analise))
			$this->objProjetos_tipo_conjunto_analise->select($this->projetos_tipo_conjunto_analise_id_INT);

        $this->inicio_DATETIME = $row->inicio_DATETIME;
        $this->inicio_DATETIME_UNIX = $row->inicio_DATETIME_UNIX;

        $this->fim_DATETIME = $row->fim_DATETIME;
        $this->fim_DATETIME_UNIX = $row->fim_DATETIME_UNIX;

        $this->seq_INT = $row->seq_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM trunk_historico WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	

    	$sql = "INSERT INTO trunk_historico ( id , projetos_tipo_conjunto_analise_id_INT , inicio_DATETIME , fim_DATETIME , seq_INT ) VALUES ( {$this->id} , {$this->projetos_tipo_conjunto_analise_id_INT} , {$this->inicio_DATETIME} , {$this->fim_DATETIME} , {$this->seq_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_tipo_conjunto_analise_id_INT(){ 

		return "projetos_tipo_conjunto_analise_id_INT";

	}

	public function nomeCampoInicio_DATETIME(){ 

		return "inicio_DATETIME";

	}

	public function nomeCampoFim_DATETIME(){ 

		return "fim_DATETIME";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_tipo_conjunto_analise_id_INT($objArguments){

		$objArguments->nome = "projetos_tipo_conjunto_analise_id_INT";
		$objArguments->id = "projetos_tipo_conjunto_analise_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoInicio_DATETIME($objArguments){

		$objArguments->nome = "inicio_DATETIME";
		$objArguments->id = "inicio_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoFim_DATETIME($objArguments){

		$objArguments->nome = "fim_DATETIME";
		$objArguments->id = "fim_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_tipo_conjunto_analise_id_INT == ""){

			$this->projetos_tipo_conjunto_analise_id_INT = "null";

		}

		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}



	$this->inicio_DATETIME = $this->formatarDataTimeParaComandoSQL($this->inicio_DATETIME); 
	$this->fim_DATETIME = $this->formatarDataTimeParaComandoSQL($this->fim_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->inicio_DATETIME = $this->formatarDataTimeParaExibicao($this->inicio_DATETIME); 
	$this->fim_DATETIME = $this->formatarDataTimeParaExibicao($this->fim_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_tipo_conjunto_analise_id_INT = null; 
		$this->objProjetos_tipo_conjunto_analise= null;
		$this->inicio_DATETIME = null; 
		$this->fim_DATETIME = null; 
		$this->seq_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_tipo_conjunto_analise_id_INT", $this->projetos_tipo_conjunto_analise_id_INT); 
		Helper::setSession("inicio_DATETIME", $this->inicio_DATETIME); 
		Helper::setSession("fim_DATETIME", $this->fim_DATETIME); 
		Helper::setSession("seq_INT", $this->seq_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_tipo_conjunto_analise_id_INT");
		Helper::clearSession("inicio_DATETIME");
		Helper::clearSession("fim_DATETIME");
		Helper::clearSession("seq_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_tipo_conjunto_analise_id_INT = Helper::SESSION("projetos_tipo_conjunto_analise_id_INT{$numReg}"); 
		$this->inicio_DATETIME = Helper::SESSION("inicio_DATETIME{$numReg}"); 
		$this->fim_DATETIME = Helper::SESSION("fim_DATETIME{$numReg}"); 
		$this->seq_INT = Helper::SESSION("seq_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_tipo_conjunto_analise_id_INT = Helper::POST("projetos_tipo_conjunto_analise_id_INT{$numReg}"); 
		$this->inicio_DATETIME = Helper::POST("inicio_DATETIME{$numReg}"); 
		$this->fim_DATETIME = Helper::POST("fim_DATETIME{$numReg}"); 
		$this->seq_INT = Helper::POST("seq_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_tipo_conjunto_analise_id_INT = Helper::GET("projetos_tipo_conjunto_analise_id_INT{$numReg}"); 
		$this->inicio_DATETIME = Helper::GET("inicio_DATETIME{$numReg}"); 
		$this->fim_DATETIME = Helper::GET("fim_DATETIME{$numReg}"); 
		$this->seq_INT = Helper::GET("seq_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_tipo_conjunto_analise_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_tipo_conjunto_analise_id_INT = $this->projetos_tipo_conjunto_analise_id_INT, ";

	} 

	if(isset($tipo["inicio_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "inicio_DATETIME = $this->inicio_DATETIME, ";

	} 

	if(isset($tipo["fim_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "fim_DATETIME = $this->fim_DATETIME, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == null){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE trunk_historico SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    