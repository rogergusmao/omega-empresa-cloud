<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_tabela
    * DATA DE GERAÇÃO: 23.06.2014
    * ARQUIVO:         DAO_Sistema_tabela.php
    * TABELA MYSQL:    sistema_tabela
    * BANCO DE DADOS:  sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Sistema_tabela extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $data_modificacao_estrutura_DATETIME;
	public $frequencia_sincronizador_INT;
	public $banco_mobile_BOOLEAN;
	public $banco_web_BOOLEAN;
	public $transmissao_web_para_mobile_BOOLEAN;
	public $transmissao_mobile_para_web_BOOLEAN;
	public $tabela_sistema_BOOLEAN;
	public $excluida_BOOLEAN;
	public $atributos_chave_unica;


    public $nomeEntidade;

	public $data_modificacao_estrutura_DATETIME_UNIX;


    

	public $label_id;
	public $label_nome;
	public $label_data_modificacao_estrutura_DATETIME;
	public $label_frequencia_sincronizador_INT;
	public $label_banco_mobile_BOOLEAN;
	public $label_banco_web_BOOLEAN;
	public $label_transmissao_web_para_mobile_BOOLEAN;
	public $label_transmissao_mobile_para_web_BOOLEAN;
	public $label_tabela_sistema_BOOLEAN;
	public $label_excluida_BOOLEAN;
	public $label_atributos_chave_unica;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2, $database=null)
    {

    	parent::__construct($niveisRaiz, $database);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_tabela";
    	$this->campoId = "id";
    	$this->campoLabel = "nome";



    }

    public function valorCampoLabel(){

    	return $this->getNome();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_tabela", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_tabela", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    function getData_modificacao_estrutura_DATETIME_UNIX()
    {
    	return $this->data_modificacao_estrutura_DATETIME_UNIX;
    }
    
    public function getData_modificacao_estrutura_DATETIME()
    {
    	return $this->data_modificacao_estrutura_DATETIME;
    }
    
    public function getFrequencia_sincronizador_INT()
    {
    	return $this->frequencia_sincronizador_INT;
    }
    
    public function getBanco_mobile_BOOLEAN()
    {
    	return $this->banco_mobile_BOOLEAN;
    }
    
    public function getBanco_web_BOOLEAN()
    {
    	return $this->banco_web_BOOLEAN;
    }
    
    public function getTransmissao_web_para_mobile_BOOLEAN()
    {
    	return $this->transmissao_web_para_mobile_BOOLEAN;
    }
    
    public function getTransmissao_mobile_para_web_BOOLEAN()
    {
    	return $this->transmissao_mobile_para_web_BOOLEAN;
    }
    
    public function getTabela_sistema_BOOLEAN()
    {
    	return $this->tabela_sistema_BOOLEAN;
    }
    
    public function getExcluida_BOOLEAN()
    {
    	return $this->excluida_BOOLEAN;
    }
    
    public function getAtributos_chave_unica()
    {
    	return $this->atributos_chave_unica;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setData_modificacao_estrutura_DATETIME($val)
    {
    	$this->data_modificacao_estrutura_DATETIME =  $val;
    }
    
    function setFrequencia_sincronizador_INT($val)
    {
    	$this->frequencia_sincronizador_INT =  $val;
    }
    
    function setBanco_mobile_BOOLEAN($val)
    {
    	$this->banco_mobile_BOOLEAN =  $val;
    }
    
    function setBanco_web_BOOLEAN($val)
    {
    	$this->banco_web_BOOLEAN =  $val;
    }
    
    function setTransmissao_web_para_mobile_BOOLEAN($val)
    {
    	$this->transmissao_web_para_mobile_BOOLEAN =  $val;
    }
    
    function setTransmissao_mobile_para_web_BOOLEAN($val)
    {
    	$this->transmissao_mobile_para_web_BOOLEAN =  $val;
    }
    
    function setTabela_sistema_BOOLEAN($val)
    {
    	$this->tabela_sistema_BOOLEAN =  $val;
    }
    
    function setExcluida_BOOLEAN($val)
    {
    	$this->excluida_BOOLEAN =  $val;
    }
    
    function setAtributos_chave_unica($val)
    {
    	$this->atributos_chave_unica =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_modificacao_estrutura_DATETIME) AS data_modificacao_estrutura_DATETIME_UNIX FROM sistema_tabela WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->data_modificacao_estrutura_DATETIME = $row->data_modificacao_estrutura_DATETIME;
        $this->data_modificacao_estrutura_DATETIME_UNIX = $row->data_modificacao_estrutura_DATETIME_UNIX;

        $this->frequencia_sincronizador_INT = $row->frequencia_sincronizador_INT;
        
        $this->banco_mobile_BOOLEAN = $row->banco_mobile_BOOLEAN;
        
        $this->banco_web_BOOLEAN = $row->banco_web_BOOLEAN;
        
        $this->transmissao_web_para_mobile_BOOLEAN = $row->transmissao_web_para_mobile_BOOLEAN;
        
        $this->transmissao_mobile_para_web_BOOLEAN = $row->transmissao_mobile_para_web_BOOLEAN;
        
        $this->tabela_sistema_BOOLEAN = $row->tabela_sistema_BOOLEAN;
        
        $this->excluida_BOOLEAN = $row->excluida_BOOLEAN;
        
        $this->atributos_chave_unica = $row->atributos_chave_unica;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_tabela WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_tabela ( nome,data_modificacao_estrutura_DATETIME,frequencia_sincronizador_INT,banco_mobile_BOOLEAN,banco_web_BOOLEAN,transmissao_web_para_mobile_BOOLEAN,transmissao_mobile_para_web_BOOLEAN,tabela_sistema_BOOLEAN,excluida_BOOLEAN,atributos_chave_unica ) VALUES ( '$this->nome',$this->data_modificacao_estrutura_DATETIME,$this->frequencia_sincronizador_INT,$this->banco_mobile_BOOLEAN,$this->banco_web_BOOLEAN,$this->transmissao_web_para_mobile_BOOLEAN,$this->transmissao_mobile_para_web_BOOLEAN,$this->tabela_sistema_BOOLEAN,$this->excluida_BOOLEAN,'$this->atributos_chave_unica' )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoData_modificacao_estrutura_DATETIME(){ 

		return "data_modificacao_estrutura_DATETIME";

	}

	public function nomeCampoFrequencia_sincronizador_INT(){ 

		return "frequencia_sincronizador_INT";

	}

	public function nomeCampoBanco_mobile_BOOLEAN(){ 

		return "banco_mobile_BOOLEAN";

	}

	public function nomeCampoBanco_web_BOOLEAN(){ 

		return "banco_web_BOOLEAN";

	}

	public function nomeCampoTransmissao_web_para_mobile_BOOLEAN(){ 

		return "transmissao_web_para_mobile_BOOLEAN";

	}

	public function nomeCampoTransmissao_mobile_para_web_BOOLEAN(){ 

		return "transmissao_mobile_para_web_BOOLEAN";

	}

	public function nomeCampoTabela_sistema_BOOLEAN(){ 

		return "tabela_sistema_BOOLEAN";

	}

	public function nomeCampoExcluida_BOOLEAN(){ 

		return "excluida_BOOLEAN";

	}

	public function nomeCampoAtributos_chave_unica(){ 

		return "atributos_chave_unica";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoData_modificacao_estrutura_DATETIME($objArguments){

		$objArguments->nome = "data_modificacao_estrutura_DATETIME";
		$objArguments->id = "data_modificacao_estrutura_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoFrequencia_sincronizador_INT($objArguments){

		$objArguments->nome = "frequencia_sincronizador_INT";
		$objArguments->id = "frequencia_sincronizador_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoBanco_mobile_BOOLEAN($objArguments){

		$objArguments->nome = "banco_mobile_BOOLEAN";
		$objArguments->id = "banco_mobile_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoBanco_web_BOOLEAN($objArguments){

		$objArguments->nome = "banco_web_BOOLEAN";
		$objArguments->id = "banco_web_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoTransmissao_web_para_mobile_BOOLEAN($objArguments){

		$objArguments->nome = "transmissao_web_para_mobile_BOOLEAN";
		$objArguments->id = "transmissao_web_para_mobile_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoTransmissao_mobile_para_web_BOOLEAN($objArguments){

		$objArguments->nome = "transmissao_mobile_para_web_BOOLEAN";
		$objArguments->id = "transmissao_mobile_para_web_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoTabela_sistema_BOOLEAN($objArguments){

		$objArguments->nome = "tabela_sistema_BOOLEAN";
		$objArguments->id = "tabela_sistema_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoExcluida_BOOLEAN($objArguments){

		$objArguments->nome = "excluida_BOOLEAN";
		$objArguments->id = "excluida_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoAtributos_chave_unica($objArguments){

		$objArguments->nome = "atributos_chave_unica";
		$objArguments->id = "atributos_chave_unica";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->frequencia_sincronizador_INT == ""){

			$this->frequencia_sincronizador_INT = "null";

		}

		if($this->banco_mobile_BOOLEAN == ""){

			$this->banco_mobile_BOOLEAN = "null";

		}

		if($this->banco_web_BOOLEAN == ""){

			$this->banco_web_BOOLEAN = "null";

		}

		if($this->transmissao_web_para_mobile_BOOLEAN == ""){

			$this->transmissao_web_para_mobile_BOOLEAN = "null";

		}

		if($this->transmissao_mobile_para_web_BOOLEAN == ""){

			$this->transmissao_mobile_para_web_BOOLEAN = "null";

		}

		if($this->tabela_sistema_BOOLEAN == ""){

			$this->tabela_sistema_BOOLEAN = "null";

		}

		if($this->excluida_BOOLEAN == ""){

			$this->excluida_BOOLEAN = "null";

		}



	$this->data_modificacao_estrutura_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_modificacao_estrutura_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_modificacao_estrutura_DATETIME = $this->formatarDataTimeParaExibicao($this->data_modificacao_estrutura_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["nome"] = $this->nome; 
		$_SESSION["data_modificacao_estrutura_DATETIME"] = $this->data_modificacao_estrutura_DATETIME; 
		$_SESSION["frequencia_sincronizador_INT"] = $this->frequencia_sincronizador_INT; 
		$_SESSION["banco_mobile_BOOLEAN"] = $this->banco_mobile_BOOLEAN; 
		$_SESSION["banco_web_BOOLEAN"] = $this->banco_web_BOOLEAN; 
		$_SESSION["transmissao_web_para_mobile_BOOLEAN"] = $this->transmissao_web_para_mobile_BOOLEAN; 
		$_SESSION["transmissao_mobile_para_web_BOOLEAN"] = $this->transmissao_mobile_para_web_BOOLEAN; 
		$_SESSION["tabela_sistema_BOOLEAN"] = $this->tabela_sistema_BOOLEAN; 
		$_SESSION["excluida_BOOLEAN"] = $this->excluida_BOOLEAN; 
		$_SESSION["atributos_chave_unica"] = $this->atributos_chave_unica; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["nome"]);
		unset($_SESSION["data_modificacao_estrutura_DATETIME"]);
		unset($_SESSION["frequencia_sincronizador_INT"]);
		unset($_SESSION["banco_mobile_BOOLEAN"]);
		unset($_SESSION["banco_web_BOOLEAN"]);
		unset($_SESSION["transmissao_web_para_mobile_BOOLEAN"]);
		unset($_SESSION["transmissao_mobile_para_web_BOOLEAN"]);
		unset($_SESSION["tabela_sistema_BOOLEAN"]);
		unset($_SESSION["excluida_BOOLEAN"]);
		unset($_SESSION["atributos_chave_unica"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]); 
		$this->data_modificacao_estrutura_DATETIME = $this->formatarDados($_SESSION["data_modificacao_estrutura_DATETIME{$numReg}"]); 
		$this->frequencia_sincronizador_INT = $this->formatarDados($_SESSION["frequencia_sincronizador_INT{$numReg}"]); 
		$this->banco_mobile_BOOLEAN = $this->formatarDados($_SESSION["banco_mobile_BOOLEAN{$numReg}"]); 
		$this->banco_web_BOOLEAN = $this->formatarDados($_SESSION["banco_web_BOOLEAN{$numReg}"]); 
		$this->transmissao_web_para_mobile_BOOLEAN = $this->formatarDados($_SESSION["transmissao_web_para_mobile_BOOLEAN{$numReg}"]); 
		$this->transmissao_mobile_para_web_BOOLEAN = $this->formatarDados($_SESSION["transmissao_mobile_para_web_BOOLEAN{$numReg}"]); 
		$this->tabela_sistema_BOOLEAN = $this->formatarDados($_SESSION["tabela_sistema_BOOLEAN{$numReg}"]); 
		$this->excluida_BOOLEAN = $this->formatarDados($_SESSION["excluida_BOOLEAN{$numReg}"]); 
		$this->atributos_chave_unica = $this->formatarDados($_SESSION["atributos_chave_unica{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_POST["nome{$numReg}"]); 
		$this->data_modificacao_estrutura_DATETIME = $this->formatarDados($_POST["data_modificacao_estrutura_DATETIME{$numReg}"]); 
		$this->frequencia_sincronizador_INT = $this->formatarDados($_POST["frequencia_sincronizador_INT{$numReg}"]); 
		$this->banco_mobile_BOOLEAN = $this->formatarDados($_POST["banco_mobile_BOOLEAN{$numReg}"]); 
		$this->banco_web_BOOLEAN = $this->formatarDados($_POST["banco_web_BOOLEAN{$numReg}"]); 
		$this->transmissao_web_para_mobile_BOOLEAN = $this->formatarDados($_POST["transmissao_web_para_mobile_BOOLEAN{$numReg}"]); 
		$this->transmissao_mobile_para_web_BOOLEAN = $this->formatarDados($_POST["transmissao_mobile_para_web_BOOLEAN{$numReg}"]); 
		$this->tabela_sistema_BOOLEAN = $this->formatarDados($_POST["tabela_sistema_BOOLEAN{$numReg}"]); 
		$this->excluida_BOOLEAN = $this->formatarDados($_POST["excluida_BOOLEAN{$numReg}"]); 
		$this->atributos_chave_unica = $this->formatarDados($_POST["atributos_chave_unica{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_GET["nome{$numReg}"]); 
		$this->data_modificacao_estrutura_DATETIME = $this->formatarDados($_GET["data_modificacao_estrutura_DATETIME{$numReg}"]); 
		$this->frequencia_sincronizador_INT = $this->formatarDados($_GET["frequencia_sincronizador_INT{$numReg}"]); 
		$this->banco_mobile_BOOLEAN = $this->formatarDados($_GET["banco_mobile_BOOLEAN{$numReg}"]); 
		$this->banco_web_BOOLEAN = $this->formatarDados($_GET["banco_web_BOOLEAN{$numReg}"]); 
		$this->transmissao_web_para_mobile_BOOLEAN = $this->formatarDados($_GET["transmissao_web_para_mobile_BOOLEAN{$numReg}"]); 
		$this->transmissao_mobile_para_web_BOOLEAN = $this->formatarDados($_GET["transmissao_mobile_para_web_BOOLEAN{$numReg}"]); 
		$this->tabela_sistema_BOOLEAN = $this->formatarDados($_GET["tabela_sistema_BOOLEAN{$numReg}"]); 
		$this->excluida_BOOLEAN = $this->formatarDados($_GET["excluida_BOOLEAN{$numReg}"]); 
		$this->atributos_chave_unica = $this->formatarDados($_GET["atributos_chave_unica{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["nome{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome = '$this->nome', ";

	} 

	if(isset($tipo["data_modificacao_estrutura_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_modificacao_estrutura_DATETIME = $this->data_modificacao_estrutura_DATETIME, ";

	} 

	if(isset($tipo["frequencia_sincronizador_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "frequencia_sincronizador_INT = $this->frequencia_sincronizador_INT, ";

	} 

	if(isset($tipo["banco_mobile_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "banco_mobile_BOOLEAN = $this->banco_mobile_BOOLEAN, ";

	} 

	if(isset($tipo["banco_web_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "banco_web_BOOLEAN = $this->banco_web_BOOLEAN, ";

	} 

	if(isset($tipo["transmissao_web_para_mobile_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "transmissao_web_para_mobile_BOOLEAN = $this->transmissao_web_para_mobile_BOOLEAN, ";

	} 

	if(isset($tipo["transmissao_mobile_para_web_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "transmissao_mobile_para_web_BOOLEAN = $this->transmissao_mobile_para_web_BOOLEAN, ";

	} 

	if(isset($tipo["tabela_sistema_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "tabela_sistema_BOOLEAN = $this->tabela_sistema_BOOLEAN, ";

	} 

	if(isset($tipo["excluida_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "excluida_BOOLEAN = $this->excluida_BOOLEAN, ";

	} 

	if(isset($tipo["atributos_chave_unica{$numReg}"]) || $tipo == "vazio"){

		$upd.= "atributos_chave_unica = '$this->atributos_chave_unica', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_tabela SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>
