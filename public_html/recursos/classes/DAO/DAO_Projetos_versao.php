<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_versao
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_versao.php
    * TABELA MYSQL:    projetos_versao
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_versao extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $versao;
	public $projetos_id_INT;
	public $obj;
	public $data_homologacao_DATETIME;
	public $data_producao_DATETIME;
	public $copia_do_projetos_versao_id_INT;
	public $objProjetos;
	public $tipo_analise_projeto_id_INT;
	public $objCopia_do_projetos_versao;
	public $sinc_do_projetos_versao_id_INT;
	public $objTipo_analise_projeto;
	public $descricao;


    public $nomeEntidade;

	public $data_homologacao_DATETIME_UNIX;
	public $data_producao_DATETIME_UNIX;


    

	public $label_id;
	public $label_nome;
	public $label_versao;
	public $label_projetos_id_INT;
	public $label_data_homologacao_DATETIME;
	public $label_data_producao_DATETIME;
	public $label_copia_do_projetos_versao_id_INT;
	public $label_tipo_analise_projeto_id_INT;
	public $label_sinc_do_projetos_versao_id_INT;
	public $label_descricao;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_versao";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos($this->getDatabase());
		if($this->projetos_id_INT != null) 
		$this->obj->select($this->projetos_id_INT);
	}
	return $this->obj ;
}
function getFkObjCopia_do_projetos_versao(){
	if($this->objProjetos ==null){
		$this->objProjetos = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->copia_do_projetos_versao_id_INT != null) 
		$this->objProjetos->select($this->copia_do_projetos_versao_id_INT);
	}
	return $this->objProjetos ;
}
function getFkObjTipo_analise_projeto(){
	if($this->objCopia_do_projetos_versao ==null){
		$this->objCopia_do_projetos_versao = new EXTDAO_Tipo_analise_projeto($this->getDatabase());
		if($this->tipo_analise_projeto_id_INT != null) 
		$this->objCopia_do_projetos_versao->select($this->tipo_analise_projeto_id_INT);
	}
	return $this->objCopia_do_projetos_versao ;
}
function getFkObjSinc_do_projetos_versao(){
	if($this->objTipo_analise_projeto ==null){
		$this->objTipo_analise_projeto = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->sinc_do_projetos_versao_id_INT != null) 
		$this->objTipo_analise_projeto->select($this->sinc_do_projetos_versao_id_INT);
	}
	return $this->objTipo_analise_projeto ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos($objArgumentos){

		$objArgumentos->nome="projetos_id_INT";
		$objArgumentos->id="projetos_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCopia_do_projetos_versao($objArgumentos){

		$objArgumentos->nome="copia_do_projetos_versao_id_INT";
		$objArgumentos->id="copia_do_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCopia_do_projetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_analise_projeto($objArgumentos){

		$objArgumentos->nome="tipo_analise_projeto_id_INT";
		$objArgumentos->id="tipo_analise_projeto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_analise_projeto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSinc_do_projetos_versao($objArgumentos){

		$objArgumentos->nome="sinc_do_projetos_versao_id_INT";
		$objArgumentos->id="sinc_do_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSinc_do_projetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_versao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_versao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getVersao()
    {
    	return $this->versao;
    }
    
    public function getProjetos_id_INT()
    {
    	return $this->projetos_id_INT;
    }
    
    function getData_homologacao_DATETIME_UNIX()
    {
    	return $this->data_homologacao_DATETIME_UNIX;
    }
    
    public function getData_homologacao_DATETIME()
    {
    	return $this->data_homologacao_DATETIME;
    }
    
    function getData_producao_DATETIME_UNIX()
    {
    	return $this->data_producao_DATETIME_UNIX;
    }
    
    public function getData_producao_DATETIME()
    {
    	return $this->data_producao_DATETIME;
    }
    
    public function getCopia_do_projetos_versao_id_INT()
    {
    	return $this->copia_do_projetos_versao_id_INT;
    }
    
    public function getTipo_analise_projeto_id_INT()
    {
    	return $this->tipo_analise_projeto_id_INT;
    }
    
    public function getSinc_do_projetos_versao_id_INT()
    {
    	return $this->sinc_do_projetos_versao_id_INT;
    }
    
    public function getDescricao()
    {
    	return $this->descricao;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setVersao($val)
    {
    	$this->versao =  $val;
    }
    
    function setProjetos_id_INT($val)
    {
    	$this->projetos_id_INT =  $val;
    }
    
    function setData_homologacao_DATETIME($val)
    {
    	$this->data_homologacao_DATETIME =  $val;
    }
    
    function setData_producao_DATETIME($val)
    {
    	$this->data_producao_DATETIME =  $val;
    }
    
    function setCopia_do_projetos_versao_id_INT($val)
    {
    	$this->copia_do_projetos_versao_id_INT =  $val;
    }
    
    function setTipo_analise_projeto_id_INT($val)
    {
    	$this->tipo_analise_projeto_id_INT =  $val;
    }
    
    function setSinc_do_projetos_versao_id_INT($val)
    {
    	$this->sinc_do_projetos_versao_id_INT =  $val;
    }
    
    function setDescricao($val)
    {
    	$this->descricao =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_homologacao_DATETIME) AS data_homologacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_producao_DATETIME) AS data_producao_DATETIME_UNIX FROM projetos_versao WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->versao = $row->versao;
        
        $this->projetos_id_INT = $row->projetos_id_INT;
        if(isset($this->objProjetos))
			$this->objProjetos->select($this->projetos_id_INT);

        $this->data_homologacao_DATETIME = $row->data_homologacao_DATETIME;
        $this->data_homologacao_DATETIME_UNIX = $row->data_homologacao_DATETIME_UNIX;

        $this->data_producao_DATETIME = $row->data_producao_DATETIME;
        $this->data_producao_DATETIME_UNIX = $row->data_producao_DATETIME_UNIX;

        $this->copia_do_projetos_versao_id_INT = $row->copia_do_projetos_versao_id_INT;
        if(isset($this->objCopia_do_projetos_versao))
			$this->objCopia_do_projetos_versao->select($this->copia_do_projetos_versao_id_INT);

        $this->tipo_analise_projeto_id_INT = $row->tipo_analise_projeto_id_INT;
        if(isset($this->objTipo_analise_projeto))
			$this->objTipo_analise_projeto->select($this->tipo_analise_projeto_id_INT);

        $this->sinc_do_projetos_versao_id_INT = $row->sinc_do_projetos_versao_id_INT;
        if(isset($this->objSinc_do_projetos_versao))
			$this->objSinc_do_projetos_versao->select($this->sinc_do_projetos_versao_id_INT);

        $this->descricao = $row->descricao;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_versao WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_versao ( nome , versao , projetos_id_INT , data_homologacao_DATETIME , data_producao_DATETIME , copia_do_projetos_versao_id_INT , tipo_analise_projeto_id_INT , sinc_do_projetos_versao_id_INT , descricao ) VALUES ( {$this->nome} , {$this->versao} , {$this->projetos_id_INT} , {$this->data_homologacao_DATETIME} , {$this->data_producao_DATETIME} , {$this->copia_do_projetos_versao_id_INT} , {$this->tipo_analise_projeto_id_INT} , {$this->sinc_do_projetos_versao_id_INT} , {$this->descricao} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoVersao(){ 

		return "versao";

	}

	public function nomeCampoProjetos_id_INT(){ 

		return "projetos_id_INT";

	}

	public function nomeCampoData_homologacao_DATETIME(){ 

		return "data_homologacao_DATETIME";

	}

	public function nomeCampoData_producao_DATETIME(){ 

		return "data_producao_DATETIME";

	}

	public function nomeCampoCopia_do_projetos_versao_id_INT(){ 

		return "copia_do_projetos_versao_id_INT";

	}

	public function nomeCampoTipo_analise_projeto_id_INT(){ 

		return "tipo_analise_projeto_id_INT";

	}

	public function nomeCampoSinc_do_projetos_versao_id_INT(){ 

		return "sinc_do_projetos_versao_id_INT";

	}

	public function nomeCampoDescricao(){ 

		return "descricao";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoVersao($objArguments){

		$objArguments->nome = "versao";
		$objArguments->id = "versao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoProjetos_id_INT($objArguments){

		$objArguments->nome = "projetos_id_INT";
		$objArguments->id = "projetos_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_homologacao_DATETIME($objArguments){

		$objArguments->nome = "data_homologacao_DATETIME";
		$objArguments->id = "data_homologacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_producao_DATETIME($objArguments){

		$objArguments->nome = "data_producao_DATETIME";
		$objArguments->id = "data_producao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoCopia_do_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "copia_do_projetos_versao_id_INT";
		$objArguments->id = "copia_do_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_analise_projeto_id_INT($objArguments){

		$objArguments->nome = "tipo_analise_projeto_id_INT";
		$objArguments->id = "tipo_analise_projeto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSinc_do_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sinc_do_projetos_versao_id_INT";
		$objArguments->id = "sinc_do_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoDescricao($objArguments){

		$objArguments->nome = "descricao";
		$objArguments->id = "descricao";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->versao = $this->formatarDadosParaSQL($this->versao);
		if($this->projetos_id_INT == ""){

			$this->projetos_id_INT = "null";

		}

		if($this->copia_do_projetos_versao_id_INT == ""){

			$this->copia_do_projetos_versao_id_INT = "null";

		}

		if($this->tipo_analise_projeto_id_INT == ""){

			$this->tipo_analise_projeto_id_INT = "null";

		}

		if($this->sinc_do_projetos_versao_id_INT == ""){

			$this->sinc_do_projetos_versao_id_INT = "null";

		}

			$this->descricao = $this->formatarDadosParaSQL($this->descricao);


	$this->data_homologacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_homologacao_DATETIME); 
	$this->data_producao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_producao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_homologacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_homologacao_DATETIME); 
	$this->data_producao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_producao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->versao = null; 
		$this->projetos_id_INT = null; 
		$this->objProjetos= null;
		$this->data_homologacao_DATETIME = null; 
		$this->data_producao_DATETIME = null; 
		$this->copia_do_projetos_versao_id_INT = null; 
		$this->objCopia_do_projetos_versao= null;
		$this->tipo_analise_projeto_id_INT = null; 
		$this->objTipo_analise_projeto= null;
		$this->sinc_do_projetos_versao_id_INT = null; 
		$this->objSinc_do_projetos_versao= null;
		$this->descricao = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("versao", $this->versao); 
		Helper::setSession("projetos_id_INT", $this->projetos_id_INT); 
		Helper::setSession("data_homologacao_DATETIME", $this->data_homologacao_DATETIME); 
		Helper::setSession("data_producao_DATETIME", $this->data_producao_DATETIME); 
		Helper::setSession("copia_do_projetos_versao_id_INT", $this->copia_do_projetos_versao_id_INT); 
		Helper::setSession("tipo_analise_projeto_id_INT", $this->tipo_analise_projeto_id_INT); 
		Helper::setSession("sinc_do_projetos_versao_id_INT", $this->sinc_do_projetos_versao_id_INT); 
		Helper::setSession("descricao", $this->descricao); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("versao");
		Helper::clearSession("projetos_id_INT");
		Helper::clearSession("data_homologacao_DATETIME");
		Helper::clearSession("data_producao_DATETIME");
		Helper::clearSession("copia_do_projetos_versao_id_INT");
		Helper::clearSession("tipo_analise_projeto_id_INT");
		Helper::clearSession("sinc_do_projetos_versao_id_INT");
		Helper::clearSession("descricao");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->versao = Helper::SESSION("versao{$numReg}"); 
		$this->projetos_id_INT = Helper::SESSION("projetos_id_INT{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::SESSION("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::SESSION("data_producao_DATETIME{$numReg}"); 
		$this->copia_do_projetos_versao_id_INT = Helper::SESSION("copia_do_projetos_versao_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::SESSION("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->sinc_do_projetos_versao_id_INT = Helper::SESSION("sinc_do_projetos_versao_id_INT{$numReg}"); 
		$this->descricao = Helper::SESSION("descricao{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->versao = Helper::POST("versao{$numReg}"); 
		$this->projetos_id_INT = Helper::POST("projetos_id_INT{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::POST("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::POST("data_producao_DATETIME{$numReg}"); 
		$this->copia_do_projetos_versao_id_INT = Helper::POST("copia_do_projetos_versao_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::POST("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->sinc_do_projetos_versao_id_INT = Helper::POST("sinc_do_projetos_versao_id_INT{$numReg}"); 
		$this->descricao = Helper::POST("descricao{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->versao = Helper::GET("versao{$numReg}"); 
		$this->projetos_id_INT = Helper::GET("projetos_id_INT{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::GET("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::GET("data_producao_DATETIME{$numReg}"); 
		$this->copia_do_projetos_versao_id_INT = Helper::GET("copia_do_projetos_versao_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::GET("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->sinc_do_projetos_versao_id_INT = Helper::GET("sinc_do_projetos_versao_id_INT{$numReg}"); 
		$this->descricao = Helper::GET("descricao{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["versao{$numReg}"]) || $tipo == null){

		$upd.= "versao = $this->versao, ";

	} 

	if(isset($tipo["projetos_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_id_INT = $this->projetos_id_INT, ";

	} 

	if(isset($tipo["data_homologacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_homologacao_DATETIME = $this->data_homologacao_DATETIME, ";

	} 

	if(isset($tipo["data_producao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_producao_DATETIME = $this->data_producao_DATETIME, ";

	} 

	if(isset($tipo["copia_do_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "copia_do_projetos_versao_id_INT = $this->copia_do_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["tipo_analise_projeto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_analise_projeto_id_INT = $this->tipo_analise_projeto_id_INT, ";

	} 

	if(isset($tipo["sinc_do_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sinc_do_projetos_versao_id_INT = $this->sinc_do_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["descricao{$numReg}"]) || $tipo == null){

		$upd.= "descricao = $this->descricao, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_versao SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    