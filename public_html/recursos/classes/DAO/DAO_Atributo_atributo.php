<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Atributo_atributo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Atributo_atributo.php
    * TABELA MYSQL:    atributo_atributo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Atributo_atributo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $homologacao_atributo_id_INT;
	public $obj;
	public $producao_atributo_id_INT;
	public $objHomologacao_atributo;
	public $tipo_operacao_atualizacao_banco_id_INT;
	public $objProducao_atributo;
	public $status_verificacao_BOOLEAN;
	public $tabela_tabela_id_INT;
	public $objTipo_operacao_atualizacao_banco;
	public $projetos_versao_id_INT;
	public $objTabela_tabela;
	public $projetos_versao_banco_banco_id_INT;
	public $objProjetos_versao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_homologacao_atributo_id_INT;
	public $label_producao_atributo_id_INT;
	public $label_tipo_operacao_atualizacao_banco_id_INT;
	public $label_status_verificacao_BOOLEAN;
	public $label_tabela_tabela_id_INT;
	public $label_projetos_versao_id_INT;
	public $label_projetos_versao_banco_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "atributo_atributo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjHomologacao_atributo(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Atributo($this->getDatabase());
		if($this->homologacao_atributo_id_INT != null) 
		$this->obj->select($this->homologacao_atributo_id_INT);
	}
	return $this->obj ;
}
function getFkObjProducao_atributo(){
	if($this->objHomologacao_atributo ==null){
		$this->objHomologacao_atributo = new EXTDAO_Atributo($this->getDatabase());
		if($this->producao_atributo_id_INT != null) 
		$this->objHomologacao_atributo->select($this->producao_atributo_id_INT);
	}
	return $this->objHomologacao_atributo ;
}
function getFkObjTipo_operacao_atualizacao_banco(){
	if($this->objProducao_atributo ==null){
		$this->objProducao_atributo = new EXTDAO_Tipo_operacao_atualizacao_banco($this->getDatabase());
		if($this->tipo_operacao_atualizacao_banco_id_INT != null) 
		$this->objProducao_atributo->select($this->tipo_operacao_atualizacao_banco_id_INT);
	}
	return $this->objProducao_atributo ;
}
function getFkObjTabela_tabela(){
	if($this->objTipo_operacao_atualizacao_banco ==null){
		$this->objTipo_operacao_atualizacao_banco = new EXTDAO_Tabela_tabela($this->getDatabase());
		if($this->tabela_tabela_id_INT != null) 
		$this->objTipo_operacao_atualizacao_banco->select($this->tabela_tabela_id_INT);
	}
	return $this->objTipo_operacao_atualizacao_banco ;
}
function getFkObjProjetos_versao(){
	if($this->objTabela_tabela ==null){
		$this->objTabela_tabela = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objTabela_tabela->select($this->projetos_versao_id_INT);
	}
	return $this->objTabela_tabela ;
}
function getFkObjProjetos_versao_banco_banco(){
	if($this->objProjetos_versao ==null){
		$this->objProjetos_versao = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->objProjetos_versao->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->objProjetos_versao ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllHomologacao_atributo($objArgumentos){

		$objArgumentos->nome="homologacao_atributo_id_INT";
		$objArgumentos->id="homologacao_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjHomologacao_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProducao_atributo($objArgumentos){

		$objArgumentos->nome="producao_atributo_id_INT";
		$objArgumentos->id="producao_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProducao_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_atualizacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTabela_tabela($objArgumentos){

		$objArgumentos->nome="tabela_tabela_id_INT";
		$objArgumentos->id="tabela_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_atributo_atributo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_atributo_atributo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHomologacao_atributo_id_INT()
    {
    	return $this->homologacao_atributo_id_INT;
    }
    
    public function getProducao_atributo_id_INT()
    {
    	return $this->producao_atributo_id_INT;
    }
    
    public function getTipo_operacao_atualizacao_banco_id_INT()
    {
    	return $this->tipo_operacao_atualizacao_banco_id_INT;
    }
    
    public function getStatus_verificacao_BOOLEAN()
    {
    	return $this->status_verificacao_BOOLEAN;
    }
    
    public function getTabela_tabela_id_INT()
    {
    	return $this->tabela_tabela_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHomologacao_atributo_id_INT($val)
    {
    	$this->homologacao_atributo_id_INT =  $val;
    }
    
    function setProducao_atributo_id_INT($val)
    {
    	$this->producao_atributo_id_INT =  $val;
    }
    
    function setTipo_operacao_atualizacao_banco_id_INT($val)
    {
    	$this->tipo_operacao_atualizacao_banco_id_INT =  $val;
    }
    
    function setStatus_verificacao_BOOLEAN($val)
    {
    	$this->status_verificacao_BOOLEAN =  $val;
    }
    
    function setTabela_tabela_id_INT($val)
    {
    	$this->tabela_tabela_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM atributo_atributo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->homologacao_atributo_id_INT = $row->homologacao_atributo_id_INT;
        if(isset($this->objHomologacao_atributo))
			$this->objHomologacao_atributo->select($this->homologacao_atributo_id_INT);

        $this->producao_atributo_id_INT = $row->producao_atributo_id_INT;
        if(isset($this->objProducao_atributo))
			$this->objProducao_atributo->select($this->producao_atributo_id_INT);

        $this->tipo_operacao_atualizacao_banco_id_INT = $row->tipo_operacao_atualizacao_banco_id_INT;
        if(isset($this->objTipo_operacao_atualizacao_banco))
			$this->objTipo_operacao_atualizacao_banco->select($this->tipo_operacao_atualizacao_banco_id_INT);

        $this->status_verificacao_BOOLEAN = $row->status_verificacao_BOOLEAN;
        
        $this->tabela_tabela_id_INT = $row->tabela_tabela_id_INT;
        if(isset($this->objTabela_tabela))
			$this->objTabela_tabela->select($this->tabela_tabela_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM atributo_atributo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO atributo_atributo ( homologacao_atributo_id_INT , producao_atributo_id_INT , tipo_operacao_atualizacao_banco_id_INT , status_verificacao_BOOLEAN , tabela_tabela_id_INT , projetos_versao_id_INT , projetos_versao_banco_banco_id_INT ) VALUES ( {$this->homologacao_atributo_id_INT} , {$this->producao_atributo_id_INT} , {$this->tipo_operacao_atualizacao_banco_id_INT} , {$this->status_verificacao_BOOLEAN} , {$this->tabela_tabela_id_INT} , {$this->projetos_versao_id_INT} , {$this->projetos_versao_banco_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHomologacao_atributo_id_INT(){ 

		return "homologacao_atributo_id_INT";

	}

	public function nomeCampoProducao_atributo_id_INT(){ 

		return "producao_atributo_id_INT";

	}

	public function nomeCampoTipo_operacao_atualizacao_banco_id_INT(){ 

		return "tipo_operacao_atualizacao_banco_id_INT";

	}

	public function nomeCampoStatus_verificacao_BOOLEAN(){ 

		return "status_verificacao_BOOLEAN";

	}

	public function nomeCampoTabela_tabela_id_INT(){ 

		return "tabela_tabela_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHomologacao_atributo_id_INT($objArguments){

		$objArguments->nome = "homologacao_atributo_id_INT";
		$objArguments->id = "homologacao_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProducao_atributo_id_INT($objArguments){

		$objArguments->nome = "producao_atributo_id_INT";
		$objArguments->id = "producao_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao_atualizacao_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_atualizacao_banco_id_INT";
		$objArguments->id = "tipo_operacao_atualizacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoStatus_verificacao_BOOLEAN($objArguments){

		$objArguments->nome = "status_verificacao_BOOLEAN";
		$objArguments->id = "status_verificacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoTabela_tabela_id_INT($objArguments){

		$objArguments->nome = "tabela_tabela_id_INT";
		$objArguments->id = "tabela_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->homologacao_atributo_id_INT == ""){

			$this->homologacao_atributo_id_INT = "null";

		}

		if($this->producao_atributo_id_INT == ""){

			$this->producao_atributo_id_INT = "null";

		}

		if($this->tipo_operacao_atualizacao_banco_id_INT == ""){

			$this->tipo_operacao_atualizacao_banco_id_INT = "null";

		}

		if($this->status_verificacao_BOOLEAN == ""){

			$this->status_verificacao_BOOLEAN = "null";

		}

		if($this->tabela_tabela_id_INT == ""){

			$this->tabela_tabela_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->homologacao_atributo_id_INT = null; 
		$this->objHomologacao_atributo= null;
		$this->producao_atributo_id_INT = null; 
		$this->objProducao_atributo= null;
		$this->tipo_operacao_atualizacao_banco_id_INT = null; 
		$this->objTipo_operacao_atualizacao_banco= null;
		$this->status_verificacao_BOOLEAN = null; 
		$this->tabela_tabela_id_INT = null; 
		$this->objTabela_tabela= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("homologacao_atributo_id_INT", $this->homologacao_atributo_id_INT); 
		Helper::setSession("producao_atributo_id_INT", $this->producao_atributo_id_INT); 
		Helper::setSession("tipo_operacao_atualizacao_banco_id_INT", $this->tipo_operacao_atualizacao_banco_id_INT); 
		Helper::setSession("status_verificacao_BOOLEAN", $this->status_verificacao_BOOLEAN); 
		Helper::setSession("tabela_tabela_id_INT", $this->tabela_tabela_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("homologacao_atributo_id_INT");
		Helper::clearSession("producao_atributo_id_INT");
		Helper::clearSession("tipo_operacao_atualizacao_banco_id_INT");
		Helper::clearSession("status_verificacao_BOOLEAN");
		Helper::clearSession("tabela_tabela_id_INT");
		Helper::clearSession("projetos_versao_id_INT");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->homologacao_atributo_id_INT = Helper::SESSION("homologacao_atributo_id_INT{$numReg}"); 
		$this->producao_atributo_id_INT = Helper::SESSION("producao_atributo_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::SESSION("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->status_verificacao_BOOLEAN = Helper::SESSION("status_verificacao_BOOLEAN{$numReg}"); 
		$this->tabela_tabela_id_INT = Helper::SESSION("tabela_tabela_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->homologacao_atributo_id_INT = Helper::POST("homologacao_atributo_id_INT{$numReg}"); 
		$this->producao_atributo_id_INT = Helper::POST("producao_atributo_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::POST("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->status_verificacao_BOOLEAN = Helper::POST("status_verificacao_BOOLEAN{$numReg}"); 
		$this->tabela_tabela_id_INT = Helper::POST("tabela_tabela_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->homologacao_atributo_id_INT = Helper::GET("homologacao_atributo_id_INT{$numReg}"); 
		$this->producao_atributo_id_INT = Helper::GET("producao_atributo_id_INT{$numReg}"); 
		$this->tipo_operacao_atualizacao_banco_id_INT = Helper::GET("tipo_operacao_atualizacao_banco_id_INT{$numReg}"); 
		$this->status_verificacao_BOOLEAN = Helper::GET("status_verificacao_BOOLEAN{$numReg}"); 
		$this->tabela_tabela_id_INT = Helper::GET("tabela_tabela_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["homologacao_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "homologacao_atributo_id_INT = $this->homologacao_atributo_id_INT, ";

	} 

	if(isset($tipo["producao_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "producao_atributo_id_INT = $this->producao_atributo_id_INT, ";

	} 

	if(isset($tipo["tipo_operacao_atualizacao_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_atualizacao_banco_id_INT = $this->tipo_operacao_atualizacao_banco_id_INT, ";

	} 

	if(isset($tipo["status_verificacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "status_verificacao_BOOLEAN = $this->status_verificacao_BOOLEAN, ";

	} 

	if(isset($tipo["tabela_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_tabela_id_INT = $this->tabela_tabela_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE atributo_atributo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    