<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_crud_aleatorio
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Operacao_crud_aleatorio.php
    * TABELA MYSQL:    operacao_crud_aleatorio
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_crud_aleatorio extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $operacao_sistema_mobile_id_INT;
	public $obj;
	public $total_insercao_INT;
	public $porcentagem_remocao_FLOAT;
	public $porcentagem_edicao_FLOAT;
	public $sincronizar_BOOLEAN;


    public $nomeEntidade;



    

	public $label_id;
	public $label_operacao_sistema_mobile_id_INT;
	public $label_total_insercao_INT;
	public $label_porcentagem_remocao_FLOAT;
	public $label_porcentagem_edicao_FLOAT;
	public $label_sincronizar_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "operacao_crud_aleatorio";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjOperacao_sistema_mobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->obj->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_operacao_crud_aleatorio", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_crud_aleatorio", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    public function getTotal_insercao_INT()
    {
    	return $this->total_insercao_INT;
    }
    
    public function getPorcentagem_remocao_FLOAT()
    {
    	return $this->porcentagem_remocao_FLOAT;
    }
    
    public function getPorcentagem_edicao_FLOAT()
    {
    	return $this->porcentagem_edicao_FLOAT;
    }
    
    public function getSincronizar_BOOLEAN()
    {
    	return $this->sincronizar_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setTotal_insercao_INT($val)
    {
    	$this->total_insercao_INT =  $val;
    }
    
    function setPorcentagem_remocao_FLOAT($val)
    {
    	$this->porcentagem_remocao_FLOAT =  $val;
    }
    
    function setPorcentagem_edicao_FLOAT($val)
    {
    	$this->porcentagem_edicao_FLOAT =  $val;
    }
    
    function setSincronizar_BOOLEAN($val)
    {
    	$this->sincronizar_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM operacao_crud_aleatorio WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

        $this->total_insercao_INT = $row->total_insercao_INT;
        
        $this->porcentagem_remocao_FLOAT = $row->porcentagem_remocao_FLOAT;
        
        $this->porcentagem_edicao_FLOAT = $row->porcentagem_edicao_FLOAT;
        
        $this->sincronizar_BOOLEAN = $row->sincronizar_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM operacao_crud_aleatorio WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO operacao_crud_aleatorio ( operacao_sistema_mobile_id_INT , total_insercao_INT , porcentagem_remocao_FLOAT , porcentagem_edicao_FLOAT , sincronizar_BOOLEAN ) VALUES ( {$this->operacao_sistema_mobile_id_INT} , {$this->total_insercao_INT} , {$this->porcentagem_remocao_FLOAT} , {$this->porcentagem_edicao_FLOAT} , {$this->sincronizar_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoTotal_insercao_INT(){ 

		return "total_insercao_INT";

	}

	public function nomeCampoPorcentagem_remocao_FLOAT(){ 

		return "porcentagem_remocao_FLOAT";

	}

	public function nomeCampoPorcentagem_edicao_FLOAT(){ 

		return "porcentagem_edicao_FLOAT";

	}

	public function nomeCampoSincronizar_BOOLEAN(){ 

		return "sincronizar_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTotal_insercao_INT($objArguments){

		$objArguments->nome = "total_insercao_INT";
		$objArguments->id = "total_insercao_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPorcentagem_remocao_FLOAT($objArguments){

		$objArguments->nome = "porcentagem_remocao_FLOAT";
		$objArguments->id = "porcentagem_remocao_FLOAT";

		return $this->campoMoeda($objArguments);

	}

	public function imprimirCampoPorcentagem_edicao_FLOAT($objArguments){

		$objArguments->nome = "porcentagem_edicao_FLOAT";
		$objArguments->id = "porcentagem_edicao_FLOAT";

		return $this->campoMoeda($objArguments);

	}

	public function imprimirCampoSincronizar_BOOLEAN($objArguments){

		$objArguments->nome = "sincronizar_BOOLEAN";
		$objArguments->id = "sincronizar_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}

		if($this->total_insercao_INT == ""){

			$this->total_insercao_INT = "null";

		}

	if($this->getporcentagem_remocao_FLOAT() == ""){

 		$this->porcentagem_remocao_FLOAT = "null";

	}

	if($this->getporcentagem_edicao_FLOAT() == ""){

 		$this->porcentagem_edicao_FLOAT = "null";

	}

		if($this->sincronizar_BOOLEAN == ""){

			$this->sincronizar_BOOLEAN = "null";

		}



	$this->porcentagem_remocao_FLOAT = $this->formatarFloatParaComandoSQL($this->porcentagem_remocao_FLOAT); 
	$this->porcentagem_edicao_FLOAT = $this->formatarFloatParaComandoSQL($this->porcentagem_edicao_FLOAT); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->porcentagem_remocao_FLOAT = $this->formatarFloatParaExibicao($this->porcentagem_remocao_FLOAT); 
	$this->porcentagem_edicao_FLOAT = $this->formatarFloatParaExibicao($this->porcentagem_edicao_FLOAT); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;
		$this->total_insercao_INT = null; 
		$this->porcentagem_remocao_FLOAT = null; 
		$this->porcentagem_edicao_FLOAT = null; 
		$this->sincronizar_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 
		Helper::setSession("total_insercao_INT", $this->total_insercao_INT); 
		Helper::setSession("porcentagem_remocao_FLOAT", $this->porcentagem_remocao_FLOAT); 
		Helper::setSession("porcentagem_edicao_FLOAT", $this->porcentagem_edicao_FLOAT); 
		Helper::setSession("sincronizar_BOOLEAN", $this->sincronizar_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("operacao_sistema_mobile_id_INT");
		Helper::clearSession("total_insercao_INT");
		Helper::clearSession("porcentagem_remocao_FLOAT");
		Helper::clearSession("porcentagem_edicao_FLOAT");
		Helper::clearSession("sincronizar_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->total_insercao_INT = Helper::SESSION("total_insercao_INT{$numReg}"); 
		$this->porcentagem_remocao_FLOAT = Helper::SESSION("porcentagem_remocao_FLOAT{$numReg}"); 
		$this->porcentagem_edicao_FLOAT = Helper::SESSION("porcentagem_edicao_FLOAT{$numReg}"); 
		$this->sincronizar_BOOLEAN = Helper::SESSION("sincronizar_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->total_insercao_INT = Helper::POST("total_insercao_INT{$numReg}"); 
		$this->porcentagem_remocao_FLOAT = Helper::POST("porcentagem_remocao_FLOAT{$numReg}"); 
		$this->porcentagem_edicao_FLOAT = Helper::POST("porcentagem_edicao_FLOAT{$numReg}"); 
		$this->sincronizar_BOOLEAN = Helper::POST("sincronizar_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->total_insercao_INT = Helper::GET("total_insercao_INT{$numReg}"); 
		$this->porcentagem_remocao_FLOAT = Helper::GET("porcentagem_remocao_FLOAT{$numReg}"); 
		$this->porcentagem_edicao_FLOAT = Helper::GET("porcentagem_edicao_FLOAT{$numReg}"); 
		$this->sincronizar_BOOLEAN = Helper::GET("sincronizar_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["total_insercao_INT{$numReg}"]) || $tipo == null){

		$upd.= "total_insercao_INT = $this->total_insercao_INT, ";

	} 

	if(isset($tipo["porcentagem_remocao_FLOAT{$numReg}"]) || $tipo == null){

		$upd.= "porcentagem_remocao_FLOAT = $this->porcentagem_remocao_FLOAT, ";

	} 

	if(isset($tipo["porcentagem_edicao_FLOAT{$numReg}"]) || $tipo == null){

		$upd.= "porcentagem_edicao_FLOAT = $this->porcentagem_edicao_FLOAT, ";

	} 

	if(isset($tipo["sincronizar_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "sincronizar_BOOLEAN = $this->sincronizar_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE operacao_crud_aleatorio SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    