<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Mobile_conectado
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Mobile_conectado.php
    * TABELA MYSQL:    mobile_conectado
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Mobile_conectado extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $data_login_DATETIME;
	public $data_atualizacao_DATETIME;
	public $data_logout_DATETIME;
	public $mobile_identificador_id_INT;
	public $obj;
	public $usuario_INT;
	public $corporacao_INT;


    public $nomeEntidade;

	public $data_login_DATETIME_UNIX;
	public $data_atualizacao_DATETIME_UNIX;
	public $data_logout_DATETIME_UNIX;


    

	public $label_id;
	public $label_data_login_DATETIME;
	public $label_data_atualizacao_DATETIME;
	public $label_data_logout_DATETIME;
	public $label_mobile_identificador_id_INT;
	public $label_usuario_INT;
	public $label_corporacao_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "mobile_conectado";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjMobile_identificador(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Mobile_identificador($this->getDatabase());
		if($this->mobile_identificador_id_INT != null) 
		$this->obj->select($this->mobile_identificador_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllMobile_identificador($objArgumentos){

		$objArgumentos->nome="mobile_identificador_id_INT";
		$objArgumentos->id="mobile_identificador_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjMobile_identificador()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_mobile_conectado", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_mobile_conectado", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    function getData_login_DATETIME_UNIX()
    {
    	return $this->data_login_DATETIME_UNIX;
    }
    
    public function getData_login_DATETIME()
    {
    	return $this->data_login_DATETIME;
    }
    
    function getData_atualizacao_DATETIME_UNIX()
    {
    	return $this->data_atualizacao_DATETIME_UNIX;
    }
    
    public function getData_atualizacao_DATETIME()
    {
    	return $this->data_atualizacao_DATETIME;
    }
    
    function getData_logout_DATETIME_UNIX()
    {
    	return $this->data_logout_DATETIME_UNIX;
    }
    
    public function getData_logout_DATETIME()
    {
    	return $this->data_logout_DATETIME;
    }
    
    public function getMobile_identificador_id_INT()
    {
    	return $this->mobile_identificador_id_INT;
    }
    
    public function getUsuario_INT()
    {
    	return $this->usuario_INT;
    }
    
    public function getCorporacao_INT()
    {
    	return $this->corporacao_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setData_login_DATETIME($val)
    {
    	$this->data_login_DATETIME =  $val;
    }
    
    function setData_atualizacao_DATETIME($val)
    {
    	$this->data_atualizacao_DATETIME =  $val;
    }
    
    function setData_logout_DATETIME($val)
    {
    	$this->data_logout_DATETIME =  $val;
    }
    
    function setMobile_identificador_id_INT($val)
    {
    	$this->mobile_identificador_id_INT =  $val;
    }
    
    function setUsuario_INT($val)
    {
    	$this->usuario_INT =  $val;
    }
    
    function setCorporacao_INT($val)
    {
    	$this->corporacao_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_login_DATETIME) AS data_login_DATETIME_UNIX, UNIX_TIMESTAMP(data_atualizacao_DATETIME) AS data_atualizacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_logout_DATETIME) AS data_logout_DATETIME_UNIX FROM mobile_conectado WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->data_login_DATETIME = $row->data_login_DATETIME;
        $this->data_login_DATETIME_UNIX = $row->data_login_DATETIME_UNIX;

        $this->data_atualizacao_DATETIME = $row->data_atualizacao_DATETIME;
        $this->data_atualizacao_DATETIME_UNIX = $row->data_atualizacao_DATETIME_UNIX;

        $this->data_logout_DATETIME = $row->data_logout_DATETIME;
        $this->data_logout_DATETIME_UNIX = $row->data_logout_DATETIME_UNIX;

        $this->mobile_identificador_id_INT = $row->mobile_identificador_id_INT;
        if(isset($this->objMobile_identificador))
			$this->objMobile_identificador->select($this->mobile_identificador_id_INT);

        $this->usuario_INT = $row->usuario_INT;
        
        $this->corporacao_INT = $row->corporacao_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM mobile_conectado WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO mobile_conectado ( data_login_DATETIME , data_atualizacao_DATETIME , data_logout_DATETIME , mobile_identificador_id_INT , usuario_INT , corporacao_INT ) VALUES ( {$this->data_login_DATETIME} , {$this->data_atualizacao_DATETIME} , {$this->data_logout_DATETIME} , {$this->mobile_identificador_id_INT} , {$this->usuario_INT} , {$this->corporacao_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoData_login_DATETIME(){ 

		return "data_login_DATETIME";

	}

	public function nomeCampoData_atualizacao_DATETIME(){ 

		return "data_atualizacao_DATETIME";

	}

	public function nomeCampoData_logout_DATETIME(){ 

		return "data_logout_DATETIME";

	}

	public function nomeCampoMobile_identificador_id_INT(){ 

		return "mobile_identificador_id_INT";

	}

	public function nomeCampoUsuario_INT(){ 

		return "usuario_INT";

	}

	public function nomeCampoCorporacao_INT(){ 

		return "corporacao_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoData_login_DATETIME($objArguments){

		$objArguments->nome = "data_login_DATETIME";
		$objArguments->id = "data_login_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_atualizacao_DATETIME($objArguments){

		$objArguments->nome = "data_atualizacao_DATETIME";
		$objArguments->id = "data_atualizacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_logout_DATETIME($objArguments){

		$objArguments->nome = "data_logout_DATETIME";
		$objArguments->id = "data_logout_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoMobile_identificador_id_INT($objArguments){

		$objArguments->nome = "mobile_identificador_id_INT";
		$objArguments->id = "mobile_identificador_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUsuario_INT($objArguments){

		$objArguments->nome = "usuario_INT";
		$objArguments->id = "usuario_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorporacao_INT($objArguments){

		$objArguments->nome = "corporacao_INT";
		$objArguments->id = "corporacao_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->mobile_identificador_id_INT == ""){

			$this->mobile_identificador_id_INT = "null";

		}

		if($this->usuario_INT == ""){

			$this->usuario_INT = "null";

		}

		if($this->corporacao_INT == ""){

			$this->corporacao_INT = "null";

		}



	$this->data_login_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_login_DATETIME); 
	$this->data_atualizacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_atualizacao_DATETIME); 
	$this->data_logout_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_logout_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_login_DATETIME = $this->formatarDataTimeParaExibicao($this->data_login_DATETIME); 
	$this->data_atualizacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_atualizacao_DATETIME); 
	$this->data_logout_DATETIME = $this->formatarDataTimeParaExibicao($this->data_logout_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->data_login_DATETIME = null; 
		$this->data_atualizacao_DATETIME = null; 
		$this->data_logout_DATETIME = null; 
		$this->mobile_identificador_id_INT = null; 
		$this->objMobile_identificador= null;
		$this->usuario_INT = null; 
		$this->corporacao_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("data_login_DATETIME", $this->data_login_DATETIME); 
		Helper::setSession("data_atualizacao_DATETIME", $this->data_atualizacao_DATETIME); 
		Helper::setSession("data_logout_DATETIME", $this->data_logout_DATETIME); 
		Helper::setSession("mobile_identificador_id_INT", $this->mobile_identificador_id_INT); 
		Helper::setSession("usuario_INT", $this->usuario_INT); 
		Helper::setSession("corporacao_INT", $this->corporacao_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("data_login_DATETIME");
		Helper::clearSession("data_atualizacao_DATETIME");
		Helper::clearSession("data_logout_DATETIME");
		Helper::clearSession("mobile_identificador_id_INT");
		Helper::clearSession("usuario_INT");
		Helper::clearSession("corporacao_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->data_login_DATETIME = Helper::SESSION("data_login_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::SESSION("data_atualizacao_DATETIME{$numReg}"); 
		$this->data_logout_DATETIME = Helper::SESSION("data_logout_DATETIME{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::SESSION("mobile_identificador_id_INT{$numReg}"); 
		$this->usuario_INT = Helper::SESSION("usuario_INT{$numReg}"); 
		$this->corporacao_INT = Helper::SESSION("corporacao_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->data_login_DATETIME = Helper::POST("data_login_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::POST("data_atualizacao_DATETIME{$numReg}"); 
		$this->data_logout_DATETIME = Helper::POST("data_logout_DATETIME{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::POST("mobile_identificador_id_INT{$numReg}"); 
		$this->usuario_INT = Helper::POST("usuario_INT{$numReg}"); 
		$this->corporacao_INT = Helper::POST("corporacao_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->data_login_DATETIME = Helper::GET("data_login_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::GET("data_atualizacao_DATETIME{$numReg}"); 
		$this->data_logout_DATETIME = Helper::GET("data_logout_DATETIME{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::GET("mobile_identificador_id_INT{$numReg}"); 
		$this->usuario_INT = Helper::GET("usuario_INT{$numReg}"); 
		$this->corporacao_INT = Helper::GET("corporacao_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["data_login_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_login_DATETIME = $this->data_login_DATETIME, ";

	} 

	if(isset($tipo["data_atualizacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_atualizacao_DATETIME = $this->data_atualizacao_DATETIME, ";

	} 

	if(isset($tipo["data_logout_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_logout_DATETIME = $this->data_logout_DATETIME, ";

	} 

	if(isset($tipo["mobile_identificador_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "mobile_identificador_id_INT = $this->mobile_identificador_id_INT, ";

	} 

	if(isset($tipo["usuario_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_INT = $this->usuario_INT, ";

	} 

	if(isset($tipo["corporacao_INT{$numReg}"]) || $tipo == null){

		$upd.= "corporacao_INT = $this->corporacao_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE mobile_conectado SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    