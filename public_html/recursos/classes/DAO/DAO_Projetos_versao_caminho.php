<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_versao_caminho
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_versao_caminho.php
    * TABELA MYSQL:    projetos_versao_caminho
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_versao_caminho extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_versao_processo_estrutura_id_INT;
	public $obj;
	public $processo_estrutura_caminho_id_INT;
	public $objProjetos_versao_processo_estrutura;
	public $data_inicio_DATETIME;
	public $data_fim_DATETIME;
	public $data_atualizacao_DATETIME;
	public $tempo_total_gasto_seg_INT;
	public $total_execucao_INT;


    public $nomeEntidade;

	public $data_inicio_DATETIME_UNIX;
	public $data_fim_DATETIME_UNIX;
	public $data_atualizacao_DATETIME_UNIX;


    

	public $label_id;
	public $label_projetos_versao_processo_estrutura_id_INT;
	public $label_processo_estrutura_caminho_id_INT;
	public $label_data_inicio_DATETIME;
	public $label_data_fim_DATETIME;
	public $label_data_atualizacao_DATETIME;
	public $label_tempo_total_gasto_seg_INT;
	public $label_total_execucao_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_versao_caminho";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_versao_processo_estrutura(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_versao_processo_estrutura($this->getDatabase());
		if($this->projetos_versao_processo_estrutura_id_INT != null) 
		$this->obj->select($this->projetos_versao_processo_estrutura_id_INT);
	}
	return $this->obj ;
}
function getFkObjProcesso_estrutura_caminho(){
	if($this->objProjetos_versao_processo_estrutura ==null){
		$this->objProjetos_versao_processo_estrutura = new EXTDAO_Processo_estrutura_caminho($this->getDatabase());
		if($this->processo_estrutura_caminho_id_INT != null) 
		$this->objProjetos_versao_processo_estrutura->select($this->processo_estrutura_caminho_id_INT);
	}
	return $this->objProjetos_versao_processo_estrutura ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_versao_processo_estrutura($objArgumentos){

		$objArgumentos->nome="projetos_versao_processo_estrutura_id_INT";
		$objArgumentos->id="projetos_versao_processo_estrutura_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_processo_estrutura()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProcesso_estrutura_caminho($objArgumentos){

		$objArgumentos->nome="processo_estrutura_caminho_id_INT";
		$objArgumentos->id="processo_estrutura_caminho_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProcesso_estrutura_caminho()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_versao_caminho", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_versao_caminho", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_versao_processo_estrutura_id_INT()
    {
    	return $this->projetos_versao_processo_estrutura_id_INT;
    }
    
    public function getProcesso_estrutura_caminho_id_INT()
    {
    	return $this->processo_estrutura_caminho_id_INT;
    }
    
    function getData_inicio_DATETIME_UNIX()
    {
    	return $this->data_inicio_DATETIME_UNIX;
    }
    
    public function getData_inicio_DATETIME()
    {
    	return $this->data_inicio_DATETIME;
    }
    
    function getData_fim_DATETIME_UNIX()
    {
    	return $this->data_fim_DATETIME_UNIX;
    }
    
    public function getData_fim_DATETIME()
    {
    	return $this->data_fim_DATETIME;
    }
    
    function getData_atualizacao_DATETIME_UNIX()
    {
    	return $this->data_atualizacao_DATETIME_UNIX;
    }
    
    public function getData_atualizacao_DATETIME()
    {
    	return $this->data_atualizacao_DATETIME;
    }
    
    public function getTempo_total_gasto_seg_INT()
    {
    	return $this->tempo_total_gasto_seg_INT;
    }
    
    public function getTotal_execucao_INT()
    {
    	return $this->total_execucao_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_versao_processo_estrutura_id_INT($val)
    {
    	$this->projetos_versao_processo_estrutura_id_INT =  $val;
    }
    
    function setProcesso_estrutura_caminho_id_INT($val)
    {
    	$this->processo_estrutura_caminho_id_INT =  $val;
    }
    
    function setData_inicio_DATETIME($val)
    {
    	$this->data_inicio_DATETIME =  $val;
    }
    
    function setData_fim_DATETIME($val)
    {
    	$this->data_fim_DATETIME =  $val;
    }
    
    function setData_atualizacao_DATETIME($val)
    {
    	$this->data_atualizacao_DATETIME =  $val;
    }
    
    function setTempo_total_gasto_seg_INT($val)
    {
    	$this->tempo_total_gasto_seg_INT =  $val;
    }
    
    function setTotal_execucao_INT($val)
    {
    	$this->total_execucao_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_inicio_DATETIME) AS data_inicio_DATETIME_UNIX, UNIX_TIMESTAMP(data_fim_DATETIME) AS data_fim_DATETIME_UNIX, UNIX_TIMESTAMP(data_atualizacao_DATETIME) AS data_atualizacao_DATETIME_UNIX FROM projetos_versao_caminho WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_versao_processo_estrutura_id_INT = $row->projetos_versao_processo_estrutura_id_INT;
        if(isset($this->objProjetos_versao_processo_estrutura))
			$this->objProjetos_versao_processo_estrutura->select($this->projetos_versao_processo_estrutura_id_INT);

        $this->processo_estrutura_caminho_id_INT = $row->processo_estrutura_caminho_id_INT;
        if(isset($this->objProcesso_estrutura_caminho))
			$this->objProcesso_estrutura_caminho->select($this->processo_estrutura_caminho_id_INT);

        $this->data_inicio_DATETIME = $row->data_inicio_DATETIME;
        $this->data_inicio_DATETIME_UNIX = $row->data_inicio_DATETIME_UNIX;

        $this->data_fim_DATETIME = $row->data_fim_DATETIME;
        $this->data_fim_DATETIME_UNIX = $row->data_fim_DATETIME_UNIX;

        $this->data_atualizacao_DATETIME = $row->data_atualizacao_DATETIME;
        $this->data_atualizacao_DATETIME_UNIX = $row->data_atualizacao_DATETIME_UNIX;

        $this->tempo_total_gasto_seg_INT = $row->tempo_total_gasto_seg_INT;
        
        $this->total_execucao_INT = $row->total_execucao_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_versao_caminho WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_versao_caminho ( projetos_versao_processo_estrutura_id_INT , processo_estrutura_caminho_id_INT , data_inicio_DATETIME , data_fim_DATETIME , data_atualizacao_DATETIME , tempo_total_gasto_seg_INT , total_execucao_INT ) VALUES ( {$this->projetos_versao_processo_estrutura_id_INT} , {$this->processo_estrutura_caminho_id_INT} , {$this->data_inicio_DATETIME} , {$this->data_fim_DATETIME} , {$this->data_atualizacao_DATETIME} , {$this->tempo_total_gasto_seg_INT} , {$this->total_execucao_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_versao_processo_estrutura_id_INT(){ 

		return "projetos_versao_processo_estrutura_id_INT";

	}

	public function nomeCampoProcesso_estrutura_caminho_id_INT(){ 

		return "processo_estrutura_caminho_id_INT";

	}

	public function nomeCampoData_inicio_DATETIME(){ 

		return "data_inicio_DATETIME";

	}

	public function nomeCampoData_fim_DATETIME(){ 

		return "data_fim_DATETIME";

	}

	public function nomeCampoData_atualizacao_DATETIME(){ 

		return "data_atualizacao_DATETIME";

	}

	public function nomeCampoTempo_total_gasto_seg_INT(){ 

		return "tempo_total_gasto_seg_INT";

	}

	public function nomeCampoTotal_execucao_INT(){ 

		return "total_execucao_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_versao_processo_estrutura_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_processo_estrutura_id_INT";
		$objArguments->id = "projetos_versao_processo_estrutura_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProcesso_estrutura_caminho_id_INT($objArguments){

		$objArguments->nome = "processo_estrutura_caminho_id_INT";
		$objArguments->id = "processo_estrutura_caminho_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_inicio_DATETIME($objArguments){

		$objArguments->nome = "data_inicio_DATETIME";
		$objArguments->id = "data_inicio_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_fim_DATETIME($objArguments){

		$objArguments->nome = "data_fim_DATETIME";
		$objArguments->id = "data_fim_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_atualizacao_DATETIME($objArguments){

		$objArguments->nome = "data_atualizacao_DATETIME";
		$objArguments->id = "data_atualizacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoTempo_total_gasto_seg_INT($objArguments){

		$objArguments->nome = "tempo_total_gasto_seg_INT";
		$objArguments->id = "tempo_total_gasto_seg_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTotal_execucao_INT($objArguments){

		$objArguments->nome = "total_execucao_INT";
		$objArguments->id = "total_execucao_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_versao_processo_estrutura_id_INT == ""){

			$this->projetos_versao_processo_estrutura_id_INT = "null";

		}

		if($this->processo_estrutura_caminho_id_INT == ""){

			$this->processo_estrutura_caminho_id_INT = "null";

		}

		if($this->tempo_total_gasto_seg_INT == ""){

			$this->tempo_total_gasto_seg_INT = "null";

		}

		if($this->total_execucao_INT == ""){

			$this->total_execucao_INT = "null";

		}



	$this->data_inicio_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicio_DATETIME); 
	$this->data_fim_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_fim_DATETIME); 
	$this->data_atualizacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_atualizacao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_inicio_DATETIME = $this->formatarDataTimeParaExibicao($this->data_inicio_DATETIME); 
	$this->data_fim_DATETIME = $this->formatarDataTimeParaExibicao($this->data_fim_DATETIME); 
	$this->data_atualizacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_atualizacao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_versao_processo_estrutura_id_INT = null; 
		$this->objProjetos_versao_processo_estrutura= null;
		$this->processo_estrutura_caminho_id_INT = null; 
		$this->objProcesso_estrutura_caminho= null;
		$this->data_inicio_DATETIME = null; 
		$this->data_fim_DATETIME = null; 
		$this->data_atualizacao_DATETIME = null; 
		$this->tempo_total_gasto_seg_INT = null; 
		$this->total_execucao_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_versao_processo_estrutura_id_INT", $this->projetos_versao_processo_estrutura_id_INT); 
		Helper::setSession("processo_estrutura_caminho_id_INT", $this->processo_estrutura_caminho_id_INT); 
		Helper::setSession("data_inicio_DATETIME", $this->data_inicio_DATETIME); 
		Helper::setSession("data_fim_DATETIME", $this->data_fim_DATETIME); 
		Helper::setSession("data_atualizacao_DATETIME", $this->data_atualizacao_DATETIME); 
		Helper::setSession("tempo_total_gasto_seg_INT", $this->tempo_total_gasto_seg_INT); 
		Helper::setSession("total_execucao_INT", $this->total_execucao_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_versao_processo_estrutura_id_INT");
		Helper::clearSession("processo_estrutura_caminho_id_INT");
		Helper::clearSession("data_inicio_DATETIME");
		Helper::clearSession("data_fim_DATETIME");
		Helper::clearSession("data_atualizacao_DATETIME");
		Helper::clearSession("tempo_total_gasto_seg_INT");
		Helper::clearSession("total_execucao_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_versao_processo_estrutura_id_INT = Helper::SESSION("projetos_versao_processo_estrutura_id_INT{$numReg}"); 
		$this->processo_estrutura_caminho_id_INT = Helper::SESSION("processo_estrutura_caminho_id_INT{$numReg}"); 
		$this->data_inicio_DATETIME = Helper::SESSION("data_inicio_DATETIME{$numReg}"); 
		$this->data_fim_DATETIME = Helper::SESSION("data_fim_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::SESSION("data_atualizacao_DATETIME{$numReg}"); 
		$this->tempo_total_gasto_seg_INT = Helper::SESSION("tempo_total_gasto_seg_INT{$numReg}"); 
		$this->total_execucao_INT = Helper::SESSION("total_execucao_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_versao_processo_estrutura_id_INT = Helper::POST("projetos_versao_processo_estrutura_id_INT{$numReg}"); 
		$this->processo_estrutura_caminho_id_INT = Helper::POST("processo_estrutura_caminho_id_INT{$numReg}"); 
		$this->data_inicio_DATETIME = Helper::POST("data_inicio_DATETIME{$numReg}"); 
		$this->data_fim_DATETIME = Helper::POST("data_fim_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::POST("data_atualizacao_DATETIME{$numReg}"); 
		$this->tempo_total_gasto_seg_INT = Helper::POST("tempo_total_gasto_seg_INT{$numReg}"); 
		$this->total_execucao_INT = Helper::POST("total_execucao_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_versao_processo_estrutura_id_INT = Helper::GET("projetos_versao_processo_estrutura_id_INT{$numReg}"); 
		$this->processo_estrutura_caminho_id_INT = Helper::GET("processo_estrutura_caminho_id_INT{$numReg}"); 
		$this->data_inicio_DATETIME = Helper::GET("data_inicio_DATETIME{$numReg}"); 
		$this->data_fim_DATETIME = Helper::GET("data_fim_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::GET("data_atualizacao_DATETIME{$numReg}"); 
		$this->tempo_total_gasto_seg_INT = Helper::GET("tempo_total_gasto_seg_INT{$numReg}"); 
		$this->total_execucao_INT = Helper::GET("total_execucao_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_versao_processo_estrutura_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_processo_estrutura_id_INT = $this->projetos_versao_processo_estrutura_id_INT, ";

	} 

	if(isset($tipo["processo_estrutura_caminho_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "processo_estrutura_caminho_id_INT = $this->processo_estrutura_caminho_id_INT, ";

	} 

	if(isset($tipo["data_inicio_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_inicio_DATETIME = $this->data_inicio_DATETIME, ";

	} 

	if(isset($tipo["data_fim_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_fim_DATETIME = $this->data_fim_DATETIME, ";

	} 

	if(isset($tipo["data_atualizacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_atualizacao_DATETIME = $this->data_atualizacao_DATETIME, ";

	} 

	if(isset($tipo["tempo_total_gasto_seg_INT{$numReg}"]) || $tipo == null){

		$upd.= "tempo_total_gasto_seg_INT = $this->tempo_total_gasto_seg_INT, ";

	} 

	if(isset($tipo["total_execucao_INT{$numReg}"]) || $tipo == null){

		$upd.= "total_execucao_INT = $this->total_execucao_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_versao_caminho SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    