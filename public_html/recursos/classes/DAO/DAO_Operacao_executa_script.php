<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_executa_script
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Operacao_executa_script.php
    * TABELA MYSQL:    operacao_executa_script
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_executa_script extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $xml_script_sql_banco_ARQUIVO;
	public $operacao_sistema_mobile_id_INT;
	public $obj;


    public $nomeEntidade;



    

	public $label_id;
	public $label_xml_script_sql_banco_ARQUIVO;
	public $label_operacao_sistema_mobile_id_INT;


	public $diretorio_xml_script_sql_banco_ARQUIVO;




    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "operacao_executa_script";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjOperacao_sistema_mobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->obj->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Xml_script_sql_banco_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_script_sql_banco_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_script_sql_banco_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_script_sql_banco_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Xml_script_sql_banco_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_script_sql_banco_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_script_sql_banco_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_script_sql_banco_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Xml_script_sql_banco_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_script_sql_banco_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_script_sql_banco_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_script_sql_banco_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_operacao_executa_script", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_executa_script", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMO��O DO ARQUIVO Xml_script_sql_banco_ARQUIVO
            $pathArquivo = $this->diretorio_xml_script_sql_banco_ARQUIVO . $this->getXml_script_sql_banco_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadXml_script_sql_banco_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_xml_script_sql_banco_ARQUIVO;
            $labelCampo = $this->label_xml_script_sql_banco_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["xml_script_sql_banco_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getXml_script_sql_banco_ARQUIVO()
    {
    	return $this->xml_script_sql_banco_ARQUIVO;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setXml_script_sql_banco_ARQUIVO($val)
    {
    	$this->xml_script_sql_banco_ARQUIVO =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM operacao_executa_script WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->xml_script_sql_banco_ARQUIVO = $row->xml_script_sql_banco_ARQUIVO;
        
        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM operacao_executa_script WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO operacao_executa_script ( xml_script_sql_banco_ARQUIVO , operacao_sistema_mobile_id_INT ) VALUES ( {$this->xml_script_sql_banco_ARQUIVO} , {$this->operacao_sistema_mobile_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoXml_script_sql_banco_ARQUIVO(){ 

		return "xml_script_sql_banco_ARQUIVO";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoXml_script_sql_banco_ARQUIVO($objArguments){

		$objArguments->nome = "xml_script_sql_banco_ARQUIVO";
		$objArguments->id = "xml_script_sql_banco_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->xml_script_sql_banco_ARQUIVO = $this->formatarDadosParaSQL($this->xml_script_sql_banco_ARQUIVO);
		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->xml_script_sql_banco_ARQUIVO = null; 
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("xml_script_sql_banco_ARQUIVO", $this->xml_script_sql_banco_ARQUIVO); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("xml_script_sql_banco_ARQUIVO");
		Helper::clearSession("operacao_sistema_mobile_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->xml_script_sql_banco_ARQUIVO = Helper::SESSION("xml_script_sql_banco_ARQUIVO{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->xml_script_sql_banco_ARQUIVO = Helper::POST("xml_script_sql_banco_ARQUIVO{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->xml_script_sql_banco_ARQUIVO = Helper::GET("xml_script_sql_banco_ARQUIVO{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["xml_script_sql_banco_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "xml_script_sql_banco_ARQUIVO = $this->xml_script_sql_banco_ARQUIVO, ";

	} 

	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE operacao_executa_script SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    