<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Configs_salvas
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Configs_salvas.php
    * TABELA MYSQL:    configs_salvas
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Configs_salvas extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_id_INT;
	public $obj;
	public $tabela_nome;
	public $valores_campos_texto;


    public $nomeEntidade;



    

	public $label_id;
	public $label_projetos_id_INT;
	public $label_tabela_nome;
	public $label_valores_campos_texto;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "configs_salvas";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos($this->getDatabase());
		if($this->projetos_id_INT != null) 
		$this->obj->select($this->projetos_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos($objArgumentos){

		$objArgumentos->nome="projetos_id_INT";
		$objArgumentos->id="projetos_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_configs_salvas", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_configs_salvas", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_id_INT()
    {
    	return $this->projetos_id_INT;
    }
    
    public function getTabela_nome()
    {
    	return $this->tabela_nome;
    }
    
    public function getValores_campos_texto()
    {
    	return $this->valores_campos_texto;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_id_INT($val)
    {
    	$this->projetos_id_INT =  $val;
    }
    
    function setTabela_nome($val)
    {
    	$this->tabela_nome =  $val;
    }
    
    function setValores_campos_texto($val)
    {
    	$this->valores_campos_texto =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM configs_salvas WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_id_INT = $row->projetos_id_INT;
        if(isset($this->objProjetos))
			$this->objProjetos->select($this->projetos_id_INT);

        $this->tabela_nome = $row->tabela_nome;
        
        $this->valores_campos_texto = $row->valores_campos_texto;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM configs_salvas WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO configs_salvas ( projetos_id_INT , tabela_nome , valores_campos_texto ) VALUES ( {$this->projetos_id_INT} , {$this->tabela_nome} , {$this->valores_campos_texto} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_id_INT(){ 

		return "projetos_id_INT";

	}

	public function nomeCampoTabela_nome(){ 

		return "tabela_nome";

	}

	public function nomeCampoValores_campos_texto(){ 

		return "valores_campos_texto";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_id_INT($objArguments){

		$objArguments->nome = "projetos_id_INT";
		$objArguments->id = "projetos_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTabela_nome($objArguments){

		$objArguments->nome = "tabela_nome";
		$objArguments->id = "tabela_nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoValores_campos_texto($objArguments){

		$objArguments->nome = "valores_campos_texto";
		$objArguments->id = "valores_campos_texto";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_id_INT == ""){

			$this->projetos_id_INT = "null";

		}

			$this->tabela_nome = $this->formatarDadosParaSQL($this->tabela_nome);
			$this->valores_campos_texto = $this->formatarDadosParaSQL($this->valores_campos_texto);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_id_INT = null; 
		$this->objProjetos= null;
		$this->tabela_nome = null; 
		$this->valores_campos_texto = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_id_INT", $this->projetos_id_INT); 
		Helper::setSession("tabela_nome", $this->tabela_nome); 
		Helper::setSession("valores_campos_texto", $this->valores_campos_texto); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_id_INT");
		Helper::clearSession("tabela_nome");
		Helper::clearSession("valores_campos_texto");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_id_INT = Helper::SESSION("projetos_id_INT{$numReg}"); 
		$this->tabela_nome = Helper::SESSION("tabela_nome{$numReg}"); 
		$this->valores_campos_texto = Helper::SESSION("valores_campos_texto{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_id_INT = Helper::POST("projetos_id_INT{$numReg}"); 
		$this->tabela_nome = Helper::POST("tabela_nome{$numReg}"); 
		$this->valores_campos_texto = Helper::POST("valores_campos_texto{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_id_INT = Helper::GET("projetos_id_INT{$numReg}"); 
		$this->tabela_nome = Helper::GET("tabela_nome{$numReg}"); 
		$this->valores_campos_texto = Helper::GET("valores_campos_texto{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_id_INT = $this->projetos_id_INT, ";

	} 

	if(isset($tipo["tabela_nome{$numReg}"]) || $tipo == null){

		$upd.= "tabela_nome = $this->tabela_nome, ";

	} 

	if(isset($tipo["valores_campos_texto{$numReg}"]) || $tipo == null){

		$upd.= "valores_campos_texto = $this->valores_campos_texto, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE configs_salvas SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    