<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_sistema
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Operacao_sistema.php
    * TABELA MYSQL:    operacao_sistema
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_sistema extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $usuario_id_INT;
	public $obj;
	public $tipo_operacao;
	public $pagina_operacao;
	public $entidade_operacao;
	public $chave_registro_operacao_INT;
	public $descricao_operacao;
	public $data_operacao_DATETIME;
	public $url_completa;


    public $nomeEntidade;

	public $data_operacao_DATETIME_UNIX;


    

	public $label_id;
	public $label_usuario_id_INT;
	public $label_tipo_operacao;
	public $label_pagina_operacao;
	public $label_entidade_operacao;
	public $label_chave_registro_operacao_INT;
	public $label_descricao_operacao;
	public $label_data_operacao_DATETIME;
	public $label_url_completa;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "operacao_sistema";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjUsuario(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Usuario($this->getDatabase());
		if($this->usuario_id_INT != null) 
		$this->obj->select($this->usuario_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllUsuario($objArgumentos){

		$objArgumentos->nome="usuario_id_INT";
		$objArgumentos->id="usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_operacao_sistema", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_sistema", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getUsuario_id_INT()
    {
    	return $this->usuario_id_INT;
    }
    
    public function getTipo_operacao()
    {
    	return $this->tipo_operacao;
    }
    
    public function getPagina_operacao()
    {
    	return $this->pagina_operacao;
    }
    
    public function getEntidade_operacao()
    {
    	return $this->entidade_operacao;
    }
    
    public function getChave_registro_operacao_INT()
    {
    	return $this->chave_registro_operacao_INT;
    }
    
    public function getDescricao_operacao()
    {
    	return $this->descricao_operacao;
    }
    
    function getData_operacao_DATETIME_UNIX()
    {
    	return $this->data_operacao_DATETIME_UNIX;
    }
    
    public function getData_operacao_DATETIME()
    {
    	return $this->data_operacao_DATETIME;
    }
    
    public function getUrl_completa()
    {
    	return $this->url_completa;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setUsuario_id_INT($val)
    {
    	$this->usuario_id_INT =  $val;
    }
    
    function setTipo_operacao($val)
    {
    	$this->tipo_operacao =  $val;
    }
    
    function setPagina_operacao($val)
    {
    	$this->pagina_operacao =  $val;
    }
    
    function setEntidade_operacao($val)
    {
    	$this->entidade_operacao =  $val;
    }
    
    function setChave_registro_operacao_INT($val)
    {
    	$this->chave_registro_operacao_INT =  $val;
    }
    
    function setDescricao_operacao($val)
    {
    	$this->descricao_operacao =  $val;
    }
    
    function setData_operacao_DATETIME($val)
    {
    	$this->data_operacao_DATETIME =  $val;
    }
    
    function setUrl_completa($val)
    {
    	$this->url_completa =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_operacao_DATETIME) AS data_operacao_DATETIME_UNIX FROM operacao_sistema WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->usuario_id_INT = $row->usuario_id_INT;
        if(isset($this->objUsuario))
			$this->objUsuario->select($this->usuario_id_INT);

        $this->tipo_operacao = $row->tipo_operacao;
        
        $this->pagina_operacao = $row->pagina_operacao;
        
        $this->entidade_operacao = $row->entidade_operacao;
        
        $this->chave_registro_operacao_INT = $row->chave_registro_operacao_INT;
        
        $this->descricao_operacao = $row->descricao_operacao;
        
        $this->data_operacao_DATETIME = $row->data_operacao_DATETIME;
        $this->data_operacao_DATETIME_UNIX = $row->data_operacao_DATETIME_UNIX;

        $this->url_completa = $row->url_completa;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM operacao_sistema WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO operacao_sistema ( usuario_id_INT , tipo_operacao , pagina_operacao , entidade_operacao , chave_registro_operacao_INT , descricao_operacao , data_operacao_DATETIME , url_completa ) VALUES ( {$this->usuario_id_INT} , {$this->tipo_operacao} , {$this->pagina_operacao} , {$this->entidade_operacao} , {$this->chave_registro_operacao_INT} , {$this->descricao_operacao} , {$this->data_operacao_DATETIME} , {$this->url_completa} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoUsuario_id_INT(){ 

		return "usuario_id_INT";

	}

	public function nomeCampoTipo_operacao(){ 

		return "tipo_operacao";

	}

	public function nomeCampoPagina_operacao(){ 

		return "pagina_operacao";

	}

	public function nomeCampoEntidade_operacao(){ 

		return "entidade_operacao";

	}

	public function nomeCampoChave_registro_operacao_INT(){ 

		return "chave_registro_operacao_INT";

	}

	public function nomeCampoDescricao_operacao(){ 

		return "descricao_operacao";

	}

	public function nomeCampoData_operacao_DATETIME(){ 

		return "data_operacao_DATETIME";

	}

	public function nomeCampoUrl_completa(){ 

		return "url_completa";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoUsuario_id_INT($objArguments){

		$objArguments->nome = "usuario_id_INT";
		$objArguments->id = "usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao($objArguments){

		$objArguments->nome = "tipo_operacao";
		$objArguments->id = "tipo_operacao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoPagina_operacao($objArguments){

		$objArguments->nome = "pagina_operacao";
		$objArguments->id = "pagina_operacao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoEntidade_operacao($objArguments){

		$objArguments->nome = "entidade_operacao";
		$objArguments->id = "entidade_operacao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoChave_registro_operacao_INT($objArguments){

		$objArguments->nome = "chave_registro_operacao_INT";
		$objArguments->id = "chave_registro_operacao_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoDescricao_operacao($objArguments){

		$objArguments->nome = "descricao_operacao";
		$objArguments->id = "descricao_operacao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoData_operacao_DATETIME($objArguments){

		$objArguments->nome = "data_operacao_DATETIME";
		$objArguments->id = "data_operacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoUrl_completa($objArguments){

		$objArguments->nome = "url_completa";
		$objArguments->id = "url_completa";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->usuario_id_INT == ""){

			$this->usuario_id_INT = "null";

		}

			$this->tipo_operacao = $this->formatarDadosParaSQL($this->tipo_operacao);
			$this->pagina_operacao = $this->formatarDadosParaSQL($this->pagina_operacao);
			$this->entidade_operacao = $this->formatarDadosParaSQL($this->entidade_operacao);
		if($this->chave_registro_operacao_INT == ""){

			$this->chave_registro_operacao_INT = "null";

		}

			$this->descricao_operacao = $this->formatarDadosParaSQL($this->descricao_operacao);
			$this->url_completa = $this->formatarDadosParaSQL($this->url_completa);


	$this->data_operacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_operacao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_operacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_operacao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->usuario_id_INT = null; 
		$this->objUsuario= null;
		$this->tipo_operacao = null; 
		$this->pagina_operacao = null; 
		$this->entidade_operacao = null; 
		$this->chave_registro_operacao_INT = null; 
		$this->descricao_operacao = null; 
		$this->data_operacao_DATETIME = null; 
		$this->url_completa = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("usuario_id_INT", $this->usuario_id_INT); 
		Helper::setSession("tipo_operacao", $this->tipo_operacao); 
		Helper::setSession("pagina_operacao", $this->pagina_operacao); 
		Helper::setSession("entidade_operacao", $this->entidade_operacao); 
		Helper::setSession("chave_registro_operacao_INT", $this->chave_registro_operacao_INT); 
		Helper::setSession("descricao_operacao", $this->descricao_operacao); 
		Helper::setSession("data_operacao_DATETIME", $this->data_operacao_DATETIME); 
		Helper::setSession("url_completa", $this->url_completa); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("usuario_id_INT");
		Helper::clearSession("tipo_operacao");
		Helper::clearSession("pagina_operacao");
		Helper::clearSession("entidade_operacao");
		Helper::clearSession("chave_registro_operacao_INT");
		Helper::clearSession("descricao_operacao");
		Helper::clearSession("data_operacao_DATETIME");
		Helper::clearSession("url_completa");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->usuario_id_INT = Helper::SESSION("usuario_id_INT{$numReg}"); 
		$this->tipo_operacao = Helper::SESSION("tipo_operacao{$numReg}"); 
		$this->pagina_operacao = Helper::SESSION("pagina_operacao{$numReg}"); 
		$this->entidade_operacao = Helper::SESSION("entidade_operacao{$numReg}"); 
		$this->chave_registro_operacao_INT = Helper::SESSION("chave_registro_operacao_INT{$numReg}"); 
		$this->descricao_operacao = Helper::SESSION("descricao_operacao{$numReg}"); 
		$this->data_operacao_DATETIME = Helper::SESSION("data_operacao_DATETIME{$numReg}"); 
		$this->url_completa = Helper::SESSION("url_completa{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->usuario_id_INT = Helper::POST("usuario_id_INT{$numReg}"); 
		$this->tipo_operacao = Helper::POST("tipo_operacao{$numReg}"); 
		$this->pagina_operacao = Helper::POST("pagina_operacao{$numReg}"); 
		$this->entidade_operacao = Helper::POST("entidade_operacao{$numReg}"); 
		$this->chave_registro_operacao_INT = Helper::POST("chave_registro_operacao_INT{$numReg}"); 
		$this->descricao_operacao = Helper::POST("descricao_operacao{$numReg}"); 
		$this->data_operacao_DATETIME = Helper::POST("data_operacao_DATETIME{$numReg}"); 
		$this->url_completa = Helper::POST("url_completa{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->usuario_id_INT = Helper::GET("usuario_id_INT{$numReg}"); 
		$this->tipo_operacao = Helper::GET("tipo_operacao{$numReg}"); 
		$this->pagina_operacao = Helper::GET("pagina_operacao{$numReg}"); 
		$this->entidade_operacao = Helper::GET("entidade_operacao{$numReg}"); 
		$this->chave_registro_operacao_INT = Helper::GET("chave_registro_operacao_INT{$numReg}"); 
		$this->descricao_operacao = Helper::GET("descricao_operacao{$numReg}"); 
		$this->data_operacao_DATETIME = Helper::GET("data_operacao_DATETIME{$numReg}"); 
		$this->url_completa = Helper::GET("url_completa{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_id_INT = $this->usuario_id_INT, ";

	} 

	if(isset($tipo["tipo_operacao{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao = $this->tipo_operacao, ";

	} 

	if(isset($tipo["pagina_operacao{$numReg}"]) || $tipo == null){

		$upd.= "pagina_operacao = $this->pagina_operacao, ";

	} 

	if(isset($tipo["entidade_operacao{$numReg}"]) || $tipo == null){

		$upd.= "entidade_operacao = $this->entidade_operacao, ";

	} 

	if(isset($tipo["chave_registro_operacao_INT{$numReg}"]) || $tipo == null){

		$upd.= "chave_registro_operacao_INT = $this->chave_registro_operacao_INT, ";

	} 

	if(isset($tipo["descricao_operacao{$numReg}"]) || $tipo == null){

		$upd.= "descricao_operacao = $this->descricao_operacao, ";

	} 

	if(isset($tipo["data_operacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_operacao_DATETIME = $this->data_operacao_DATETIME, ";

	} 

	if(isset($tipo["url_completa{$numReg}"]) || $tipo == null){

		$upd.= "url_completa = $this->url_completa, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE operacao_sistema SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    