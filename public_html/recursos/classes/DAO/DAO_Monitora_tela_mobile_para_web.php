<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Monitora_tela_mobile_para_web
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Monitora_tela_mobile_para_web.php
    * TABELA MYSQL:    monitora_tela_mobile_para_web
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Monitora_tela_mobile_para_web extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $operacao_sistema_mobile_id_INT;
	public $obj;
	public $sequencia_operacao_INT;
	public $programa_aberto_BOOLEAN;
	public $altura_tela_INT;
	public $largura_tela_INT;
	public $printscreen_ARQUIVO;
	public $data_ocorrencia_DATETIME;
	public $tipo_operacao_monitora_tela_id_INT;
	public $objOperacao_sistema_mobile;
	public $mobile_identificador_id_INT;
	public $objTipo_operacao_monitora_tela;
	public $teclado_ativo_BOOLEAN;
	public $ocorreu_erro_BOOLEAN;
	public $motivo_erro;


    public $nomeEntidade;

	public $data_ocorrencia_DATETIME_UNIX;


    

	public $label_id;
	public $label_operacao_sistema_mobile_id_INT;
	public $label_sequencia_operacao_INT;
	public $label_programa_aberto_BOOLEAN;
	public $label_altura_tela_INT;
	public $label_largura_tela_INT;
	public $label_printscreen_ARQUIVO;
	public $label_data_ocorrencia_DATETIME;
	public $label_tipo_operacao_monitora_tela_id_INT;
	public $label_mobile_identificador_id_INT;
	public $label_teclado_ativo_BOOLEAN;
	public $label_ocorreu_erro_BOOLEAN;
	public $label_motivo_erro;


	public $diretorio_printscreen_ARQUIVO;




    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "monitora_tela_mobile_para_web";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjOperacao_sistema_mobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->obj->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_operacao_monitora_tela(){
	if($this->objOperacao_sistema_mobile ==null){
		$this->objOperacao_sistema_mobile = new EXTDAO_Tipo_operacao_monitora_tela($this->getDatabase());
		if($this->tipo_operacao_monitora_tela_id_INT != null) 
		$this->objOperacao_sistema_mobile->select($this->tipo_operacao_monitora_tela_id_INT);
	}
	return $this->objOperacao_sistema_mobile ;
}
function getFkObjMobile_identificador(){
	if($this->objTipo_operacao_monitora_tela ==null){
		$this->objTipo_operacao_monitora_tela = new EXTDAO_Mobile_identificador($this->getDatabase());
		if($this->mobile_identificador_id_INT != null) 
		$this->objTipo_operacao_monitora_tela->select($this->mobile_identificador_id_INT);
	}
	return $this->objTipo_operacao_monitora_tela ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_monitora_tela($objArgumentos){

		$objArgumentos->nome="tipo_operacao_monitora_tela_id_INT";
		$objArgumentos->id="tipo_operacao_monitora_tela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_monitora_tela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllMobile_identificador($objArgumentos){

		$objArgumentos->nome="mobile_identificador_id_INT";
		$objArgumentos->id="mobile_identificador_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjMobile_identificador()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Printscreen_ARQUIVO
            if(Helper::verificarUploadArquivo("printscreen_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPrintscreen_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "printscreen_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Printscreen_ARQUIVO
            if(Helper::verificarUploadArquivo("printscreen_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPrintscreen_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "printscreen_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Printscreen_ARQUIVO
            if(Helper::verificarUploadArquivo("printscreen_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPrintscreen_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "printscreen_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_monitora_tela_mobile_para_web", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_monitora_tela_mobile_para_web", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMO��O DO ARQUIVO Printscreen_ARQUIVO
            $pathArquivo = $this->diretorio_printscreen_ARQUIVO . $this->getPrintscreen_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadPrintscreen_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_printscreen_ARQUIVO;
            $labelCampo = $this->label_printscreen_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["printscreen_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    public function getSequencia_operacao_INT()
    {
    	return $this->sequencia_operacao_INT;
    }
    
    public function getPrograma_aberto_BOOLEAN()
    {
    	return $this->programa_aberto_BOOLEAN;
    }
    
    public function getAltura_tela_INT()
    {
    	return $this->altura_tela_INT;
    }
    
    public function getLargura_tela_INT()
    {
    	return $this->largura_tela_INT;
    }
    
    public function getPrintscreen_ARQUIVO()
    {
    	return $this->printscreen_ARQUIVO;
    }
    
    function getData_ocorrencia_DATETIME_UNIX()
    {
    	return $this->data_ocorrencia_DATETIME_UNIX;
    }
    
    public function getData_ocorrencia_DATETIME()
    {
    	return $this->data_ocorrencia_DATETIME;
    }
    
    public function getTipo_operacao_monitora_tela_id_INT()
    {
    	return $this->tipo_operacao_monitora_tela_id_INT;
    }
    
    public function getMobile_identificador_id_INT()
    {
    	return $this->mobile_identificador_id_INT;
    }
    
    public function getTeclado_ativo_BOOLEAN()
    {
    	return $this->teclado_ativo_BOOLEAN;
    }
    
    public function getOcorreu_erro_BOOLEAN()
    {
    	return $this->ocorreu_erro_BOOLEAN;
    }
    
    public function getMotivo_erro()
    {
    	return $this->motivo_erro;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setSequencia_operacao_INT($val)
    {
    	$this->sequencia_operacao_INT =  $val;
    }
    
    function setPrograma_aberto_BOOLEAN($val)
    {
    	$this->programa_aberto_BOOLEAN =  $val;
    }
    
    function setAltura_tela_INT($val)
    {
    	$this->altura_tela_INT =  $val;
    }
    
    function setLargura_tela_INT($val)
    {
    	$this->largura_tela_INT =  $val;
    }
    
    function setPrintscreen_ARQUIVO($val)
    {
    	$this->printscreen_ARQUIVO =  $val;
    }
    
    function setData_ocorrencia_DATETIME($val)
    {
    	$this->data_ocorrencia_DATETIME =  $val;
    }
    
    function setTipo_operacao_monitora_tela_id_INT($val)
    {
    	$this->tipo_operacao_monitora_tela_id_INT =  $val;
    }
    
    function setMobile_identificador_id_INT($val)
    {
    	$this->mobile_identificador_id_INT =  $val;
    }
    
    function setTeclado_ativo_BOOLEAN($val)
    {
    	$this->teclado_ativo_BOOLEAN =  $val;
    }
    
    function setOcorreu_erro_BOOLEAN($val)
    {
    	$this->ocorreu_erro_BOOLEAN =  $val;
    }
    
    function setMotivo_erro($val)
    {
    	$this->motivo_erro =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_ocorrencia_DATETIME) AS data_ocorrencia_DATETIME_UNIX FROM monitora_tela_mobile_para_web WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

        $this->sequencia_operacao_INT = $row->sequencia_operacao_INT;
        
        $this->programa_aberto_BOOLEAN = $row->programa_aberto_BOOLEAN;
        
        $this->altura_tela_INT = $row->altura_tela_INT;
        
        $this->largura_tela_INT = $row->largura_tela_INT;
        
        $this->printscreen_ARQUIVO = $row->printscreen_ARQUIVO;
        
        $this->data_ocorrencia_DATETIME = $row->data_ocorrencia_DATETIME;
        $this->data_ocorrencia_DATETIME_UNIX = $row->data_ocorrencia_DATETIME_UNIX;

        $this->tipo_operacao_monitora_tela_id_INT = $row->tipo_operacao_monitora_tela_id_INT;
        if(isset($this->objTipo_operacao_monitora_tela))
			$this->objTipo_operacao_monitora_tela->select($this->tipo_operacao_monitora_tela_id_INT);

        $this->mobile_identificador_id_INT = $row->mobile_identificador_id_INT;
        if(isset($this->objMobile_identificador))
			$this->objMobile_identificador->select($this->mobile_identificador_id_INT);

        $this->teclado_ativo_BOOLEAN = $row->teclado_ativo_BOOLEAN;
        
        $this->ocorreu_erro_BOOLEAN = $row->ocorreu_erro_BOOLEAN;
        
        $this->motivo_erro = $row->motivo_erro;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM monitora_tela_mobile_para_web WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO monitora_tela_mobile_para_web ( operacao_sistema_mobile_id_INT , sequencia_operacao_INT , programa_aberto_BOOLEAN , altura_tela_INT , largura_tela_INT , printscreen_ARQUIVO , data_ocorrencia_DATETIME , tipo_operacao_monitora_tela_id_INT , mobile_identificador_id_INT , teclado_ativo_BOOLEAN , ocorreu_erro_BOOLEAN , motivo_erro ) VALUES ( {$this->operacao_sistema_mobile_id_INT} , {$this->sequencia_operacao_INT} , {$this->programa_aberto_BOOLEAN} , {$this->altura_tela_INT} , {$this->largura_tela_INT} , {$this->printscreen_ARQUIVO} , {$this->data_ocorrencia_DATETIME} , {$this->tipo_operacao_monitora_tela_id_INT} , {$this->mobile_identificador_id_INT} , {$this->teclado_ativo_BOOLEAN} , {$this->ocorreu_erro_BOOLEAN} , {$this->motivo_erro} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoSequencia_operacao_INT(){ 

		return "sequencia_operacao_INT";

	}

	public function nomeCampoPrograma_aberto_BOOLEAN(){ 

		return "programa_aberto_BOOLEAN";

	}

	public function nomeCampoAltura_tela_INT(){ 

		return "altura_tela_INT";

	}

	public function nomeCampoLargura_tela_INT(){ 

		return "largura_tela_INT";

	}

	public function nomeCampoPrintscreen_ARQUIVO(){ 

		return "printscreen_ARQUIVO";

	}

	public function nomeCampoData_ocorrencia_DATETIME(){ 

		return "data_ocorrencia_DATETIME";

	}

	public function nomeCampoTipo_operacao_monitora_tela_id_INT(){ 

		return "tipo_operacao_monitora_tela_id_INT";

	}

	public function nomeCampoMobile_identificador_id_INT(){ 

		return "mobile_identificador_id_INT";

	}

	public function nomeCampoTeclado_ativo_BOOLEAN(){ 

		return "teclado_ativo_BOOLEAN";

	}

	public function nomeCampoOcorreu_erro_BOOLEAN(){ 

		return "ocorreu_erro_BOOLEAN";

	}

	public function nomeCampoMotivo_erro(){ 

		return "motivo_erro";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSequencia_operacao_INT($objArguments){

		$objArguments->nome = "sequencia_operacao_INT";
		$objArguments->id = "sequencia_operacao_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPrograma_aberto_BOOLEAN($objArguments){

		$objArguments->nome = "programa_aberto_BOOLEAN";
		$objArguments->id = "programa_aberto_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoAltura_tela_INT($objArguments){

		$objArguments->nome = "altura_tela_INT";
		$objArguments->id = "altura_tela_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoLargura_tela_INT($objArguments){

		$objArguments->nome = "largura_tela_INT";
		$objArguments->id = "largura_tela_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPrintscreen_ARQUIVO($objArguments){

		$objArguments->nome = "printscreen_ARQUIVO";
		$objArguments->id = "printscreen_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoData_ocorrencia_DATETIME($objArguments){

		$objArguments->nome = "data_ocorrencia_DATETIME";
		$objArguments->id = "data_ocorrencia_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoTipo_operacao_monitora_tela_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_monitora_tela_id_INT";
		$objArguments->id = "tipo_operacao_monitora_tela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMobile_identificador_id_INT($objArguments){

		$objArguments->nome = "mobile_identificador_id_INT";
		$objArguments->id = "mobile_identificador_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTeclado_ativo_BOOLEAN($objArguments){

		$objArguments->nome = "teclado_ativo_BOOLEAN";
		$objArguments->id = "teclado_ativo_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoOcorreu_erro_BOOLEAN($objArguments){

		$objArguments->nome = "ocorreu_erro_BOOLEAN";
		$objArguments->id = "ocorreu_erro_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoMotivo_erro($objArguments){

		$objArguments->nome = "motivo_erro";
		$objArguments->id = "motivo_erro";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}

		if($this->sequencia_operacao_INT == ""){

			$this->sequencia_operacao_INT = "null";

		}

		if($this->programa_aberto_BOOLEAN == ""){

			$this->programa_aberto_BOOLEAN = "null";

		}

		if($this->altura_tela_INT == ""){

			$this->altura_tela_INT = "null";

		}

		if($this->largura_tela_INT == ""){

			$this->largura_tela_INT = "null";

		}

			$this->printscreen_ARQUIVO = $this->formatarDadosParaSQL($this->printscreen_ARQUIVO);
		if($this->tipo_operacao_monitora_tela_id_INT == ""){

			$this->tipo_operacao_monitora_tela_id_INT = "null";

		}

		if($this->mobile_identificador_id_INT == ""){

			$this->mobile_identificador_id_INT = "null";

		}

		if($this->teclado_ativo_BOOLEAN == ""){

			$this->teclado_ativo_BOOLEAN = "null";

		}

		if($this->ocorreu_erro_BOOLEAN == ""){

			$this->ocorreu_erro_BOOLEAN = "null";

		}

			$this->motivo_erro = $this->formatarDadosParaSQL($this->motivo_erro);


	$this->data_ocorrencia_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ocorrencia_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_ocorrencia_DATETIME = $this->formatarDataTimeParaExibicao($this->data_ocorrencia_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;
		$this->sequencia_operacao_INT = null; 
		$this->programa_aberto_BOOLEAN = null; 
		$this->altura_tela_INT = null; 
		$this->largura_tela_INT = null; 
		$this->printscreen_ARQUIVO = null; 
		$this->data_ocorrencia_DATETIME = null; 
		$this->tipo_operacao_monitora_tela_id_INT = null; 
		$this->objTipo_operacao_monitora_tela= null;
		$this->mobile_identificador_id_INT = null; 
		$this->objMobile_identificador= null;
		$this->teclado_ativo_BOOLEAN = null; 
		$this->ocorreu_erro_BOOLEAN = null; 
		$this->motivo_erro = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 
		Helper::setSession("sequencia_operacao_INT", $this->sequencia_operacao_INT); 
		Helper::setSession("programa_aberto_BOOLEAN", $this->programa_aberto_BOOLEAN); 
		Helper::setSession("altura_tela_INT", $this->altura_tela_INT); 
		Helper::setSession("largura_tela_INT", $this->largura_tela_INT); 
		Helper::setSession("printscreen_ARQUIVO", $this->printscreen_ARQUIVO); 
		Helper::setSession("data_ocorrencia_DATETIME", $this->data_ocorrencia_DATETIME); 
		Helper::setSession("tipo_operacao_monitora_tela_id_INT", $this->tipo_operacao_monitora_tela_id_INT); 
		Helper::setSession("mobile_identificador_id_INT", $this->mobile_identificador_id_INT); 
		Helper::setSession("teclado_ativo_BOOLEAN", $this->teclado_ativo_BOOLEAN); 
		Helper::setSession("ocorreu_erro_BOOLEAN", $this->ocorreu_erro_BOOLEAN); 
		Helper::setSession("motivo_erro", $this->motivo_erro); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("operacao_sistema_mobile_id_INT");
		Helper::clearSession("sequencia_operacao_INT");
		Helper::clearSession("programa_aberto_BOOLEAN");
		Helper::clearSession("altura_tela_INT");
		Helper::clearSession("largura_tela_INT");
		Helper::clearSession("printscreen_ARQUIVO");
		Helper::clearSession("data_ocorrencia_DATETIME");
		Helper::clearSession("tipo_operacao_monitora_tela_id_INT");
		Helper::clearSession("mobile_identificador_id_INT");
		Helper::clearSession("teclado_ativo_BOOLEAN");
		Helper::clearSession("ocorreu_erro_BOOLEAN");
		Helper::clearSession("motivo_erro");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->sequencia_operacao_INT = Helper::SESSION("sequencia_operacao_INT{$numReg}"); 
		$this->programa_aberto_BOOLEAN = Helper::SESSION("programa_aberto_BOOLEAN{$numReg}"); 
		$this->altura_tela_INT = Helper::SESSION("altura_tela_INT{$numReg}"); 
		$this->largura_tela_INT = Helper::SESSION("largura_tela_INT{$numReg}"); 
		$this->printscreen_ARQUIVO = Helper::SESSION("printscreen_ARQUIVO{$numReg}"); 
		$this->data_ocorrencia_DATETIME = Helper::SESSION("data_ocorrencia_DATETIME{$numReg}"); 
		$this->tipo_operacao_monitora_tela_id_INT = Helper::SESSION("tipo_operacao_monitora_tela_id_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::SESSION("mobile_identificador_id_INT{$numReg}"); 
		$this->teclado_ativo_BOOLEAN = Helper::SESSION("teclado_ativo_BOOLEAN{$numReg}"); 
		$this->ocorreu_erro_BOOLEAN = Helper::SESSION("ocorreu_erro_BOOLEAN{$numReg}"); 
		$this->motivo_erro = Helper::SESSION("motivo_erro{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->sequencia_operacao_INT = Helper::POST("sequencia_operacao_INT{$numReg}"); 
		$this->programa_aberto_BOOLEAN = Helper::POST("programa_aberto_BOOLEAN{$numReg}"); 
		$this->altura_tela_INT = Helper::POST("altura_tela_INT{$numReg}"); 
		$this->largura_tela_INT = Helper::POST("largura_tela_INT{$numReg}"); 
		$this->printscreen_ARQUIVO = Helper::POST("printscreen_ARQUIVO{$numReg}"); 
		$this->data_ocorrencia_DATETIME = Helper::POST("data_ocorrencia_DATETIME{$numReg}"); 
		$this->tipo_operacao_monitora_tela_id_INT = Helper::POST("tipo_operacao_monitora_tela_id_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::POST("mobile_identificador_id_INT{$numReg}"); 
		$this->teclado_ativo_BOOLEAN = Helper::POST("teclado_ativo_BOOLEAN{$numReg}"); 
		$this->ocorreu_erro_BOOLEAN = Helper::POST("ocorreu_erro_BOOLEAN{$numReg}"); 
		$this->motivo_erro = Helper::POST("motivo_erro{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->sequencia_operacao_INT = Helper::GET("sequencia_operacao_INT{$numReg}"); 
		$this->programa_aberto_BOOLEAN = Helper::GET("programa_aberto_BOOLEAN{$numReg}"); 
		$this->altura_tela_INT = Helper::GET("altura_tela_INT{$numReg}"); 
		$this->largura_tela_INT = Helper::GET("largura_tela_INT{$numReg}"); 
		$this->printscreen_ARQUIVO = Helper::GET("printscreen_ARQUIVO{$numReg}"); 
		$this->data_ocorrencia_DATETIME = Helper::GET("data_ocorrencia_DATETIME{$numReg}"); 
		$this->tipo_operacao_monitora_tela_id_INT = Helper::GET("tipo_operacao_monitora_tela_id_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::GET("mobile_identificador_id_INT{$numReg}"); 
		$this->teclado_ativo_BOOLEAN = Helper::GET("teclado_ativo_BOOLEAN{$numReg}"); 
		$this->ocorreu_erro_BOOLEAN = Helper::GET("ocorreu_erro_BOOLEAN{$numReg}"); 
		$this->motivo_erro = Helper::GET("motivo_erro{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["sequencia_operacao_INT{$numReg}"]) || $tipo == null){

		$upd.= "sequencia_operacao_INT = $this->sequencia_operacao_INT, ";

	} 

	if(isset($tipo["programa_aberto_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "programa_aberto_BOOLEAN = $this->programa_aberto_BOOLEAN, ";

	} 

	if(isset($tipo["altura_tela_INT{$numReg}"]) || $tipo == null){

		$upd.= "altura_tela_INT = $this->altura_tela_INT, ";

	} 

	if(isset($tipo["largura_tela_INT{$numReg}"]) || $tipo == null){

		$upd.= "largura_tela_INT = $this->largura_tela_INT, ";

	} 

	if(isset($tipo["printscreen_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "printscreen_ARQUIVO = $this->printscreen_ARQUIVO, ";

	} 

	if(isset($tipo["data_ocorrencia_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_ocorrencia_DATETIME = $this->data_ocorrencia_DATETIME, ";

	} 

	if(isset($tipo["tipo_operacao_monitora_tela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_monitora_tela_id_INT = $this->tipo_operacao_monitora_tela_id_INT, ";

	} 

	if(isset($tipo["mobile_identificador_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "mobile_identificador_id_INT = $this->mobile_identificador_id_INT, ";

	} 

	if(isset($tipo["teclado_ativo_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "teclado_ativo_BOOLEAN = $this->teclado_ativo_BOOLEAN, ";

	} 

	if(isset($tipo["ocorreu_erro_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "ocorreu_erro_BOOLEAN = $this->ocorreu_erro_BOOLEAN, ";

	} 

	if(isset($tipo["motivo_erro{$numReg}"]) || $tipo == null){

		$upd.= "motivo_erro = $this->motivo_erro, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE monitora_tela_mobile_para_web SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    