<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_produto_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_produto_mobile.php
    * TABELA MYSQL:    sistema_produto_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_produto_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_produto_id_INT;
	public $obj;
	public $sistema_tipo_mobile_id_INT;
	public $objSistema_produto;
	public $certificado;
	public $senha_certificado;
	public $package;
	public $nome_banco;


    public $nomeEntidade;



    

	public $label_id;
	public $label_sistema_produto_id_INT;
	public $label_sistema_tipo_mobile_id_INT;
	public $label_certificado;
	public $label_senha_certificado;
	public $label_package;
	public $label_nome_banco;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_produto_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_produto(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_produto($this->getDatabase());
		if($this->sistema_produto_id_INT != null) 
		$this->obj->select($this->sistema_produto_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_tipo_mobile(){
	if($this->objSistema_produto ==null){
		$this->objSistema_produto = new EXTDAO_Sistema_tipo_mobile($this->getDatabase());
		if($this->sistema_tipo_mobile_id_INT != null) 
		$this->objSistema_produto->select($this->sistema_tipo_mobile_id_INT);
	}
	return $this->objSistema_produto ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_produto($objArgumentos){

		$objArgumentos->nome="sistema_produto_id_INT";
		$objArgumentos->id="sistema_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_produto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_tipo_mobile($objArgumentos){

		$objArgumentos->nome="sistema_tipo_mobile_id_INT";
		$objArgumentos->id="sistema_tipo_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_tipo_mobile()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_produto_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_produto_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_produto_id_INT()
    {
    	return $this->sistema_produto_id_INT;
    }
    
    public function getSistema_tipo_mobile_id_INT()
    {
    	return $this->sistema_tipo_mobile_id_INT;
    }
    
    public function getCertificado()
    {
    	return $this->certificado;
    }
    
    public function getSenha_certificado()
    {
    	return $this->senha_certificado;
    }
    
    public function getPackage()
    {
    	return $this->package;
    }
    
    public function getNome_banco()
    {
    	return $this->nome_banco;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_produto_id_INT($val)
    {
    	$this->sistema_produto_id_INT =  $val;
    }
    
    function setSistema_tipo_mobile_id_INT($val)
    {
    	$this->sistema_tipo_mobile_id_INT =  $val;
    }
    
    function setCertificado($val)
    {
    	$this->certificado =  $val;
    }
    
    function setSenha_certificado($val)
    {
    	$this->senha_certificado =  $val;
    }
    
    function setPackage($val)
    {
    	$this->package =  $val;
    }
    
    function setNome_banco($val)
    {
    	$this->nome_banco =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema_produto_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->sistema_produto_id_INT = $row->sistema_produto_id_INT;
        if(isset($this->objSistema_produto))
			$this->objSistema_produto->select($this->sistema_produto_id_INT);

        $this->sistema_tipo_mobile_id_INT = $row->sistema_tipo_mobile_id_INT;
        if(isset($this->objSistema_tipo_mobile))
			$this->objSistema_tipo_mobile->select($this->sistema_tipo_mobile_id_INT);

        $this->certificado = $row->certificado;
        
        $this->senha_certificado = $row->senha_certificado;
        
        $this->package = $row->package;
        
        $this->nome_banco = $row->nome_banco;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_produto_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_produto_mobile ( sistema_produto_id_INT , sistema_tipo_mobile_id_INT , certificado , senha_certificado , package , nome_banco ) VALUES ( {$this->sistema_produto_id_INT} , {$this->sistema_tipo_mobile_id_INT} , {$this->certificado} , {$this->senha_certificado} , {$this->package} , {$this->nome_banco} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_produto_id_INT(){ 

		return "sistema_produto_id_INT";

	}

	public function nomeCampoSistema_tipo_mobile_id_INT(){ 

		return "sistema_tipo_mobile_id_INT";

	}

	public function nomeCampoCertificado(){ 

		return "certificado";

	}

	public function nomeCampoSenha_certificado(){ 

		return "senha_certificado";

	}

	public function nomeCampoPackage(){ 

		return "package";

	}

	public function nomeCampoNome_banco(){ 

		return "nome_banco";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_produto_id_INT($objArguments){

		$objArguments->nome = "sistema_produto_id_INT";
		$objArguments->id = "sistema_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_tipo_mobile_id_INT($objArguments){

		$objArguments->nome = "sistema_tipo_mobile_id_INT";
		$objArguments->id = "sistema_tipo_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCertificado($objArguments){

		$objArguments->nome = "certificado";
		$objArguments->id = "certificado";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSenha_certificado($objArguments){

		$objArguments->nome = "senha_certificado";
		$objArguments->id = "senha_certificado";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoPackage($objArguments){

		$objArguments->nome = "package";
		$objArguments->id = "package";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoNome_banco($objArguments){

		$objArguments->nome = "nome_banco";
		$objArguments->id = "nome_banco";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_produto_id_INT == ""){

			$this->sistema_produto_id_INT = "null";

		}

		if($this->sistema_tipo_mobile_id_INT == ""){

			$this->sistema_tipo_mobile_id_INT = "null";

		}

			$this->certificado = $this->formatarDadosParaSQL($this->certificado);
			$this->senha_certificado = $this->formatarDadosParaSQL($this->senha_certificado);
			$this->package = $this->formatarDadosParaSQL($this->package);
			$this->nome_banco = $this->formatarDadosParaSQL($this->nome_banco);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->sistema_produto_id_INT = null; 
		$this->objSistema_produto= null;
		$this->sistema_tipo_mobile_id_INT = null; 
		$this->objSistema_tipo_mobile= null;
		$this->certificado = null; 
		$this->senha_certificado = null; 
		$this->package = null; 
		$this->nome_banco = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("sistema_produto_id_INT", $this->sistema_produto_id_INT); 
		Helper::setSession("sistema_tipo_mobile_id_INT", $this->sistema_tipo_mobile_id_INT); 
		Helper::setSession("certificado", $this->certificado); 
		Helper::setSession("senha_certificado", $this->senha_certificado); 
		Helper::setSession("package", $this->package); 
		Helper::setSession("nome_banco", $this->nome_banco); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("sistema_produto_id_INT");
		Helper::clearSession("sistema_tipo_mobile_id_INT");
		Helper::clearSession("certificado");
		Helper::clearSession("senha_certificado");
		Helper::clearSession("package");
		Helper::clearSession("nome_banco");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->sistema_produto_id_INT = Helper::SESSION("sistema_produto_id_INT{$numReg}"); 
		$this->sistema_tipo_mobile_id_INT = Helper::SESSION("sistema_tipo_mobile_id_INT{$numReg}"); 
		$this->certificado = Helper::SESSION("certificado{$numReg}"); 
		$this->senha_certificado = Helper::SESSION("senha_certificado{$numReg}"); 
		$this->package = Helper::SESSION("package{$numReg}"); 
		$this->nome_banco = Helper::SESSION("nome_banco{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->sistema_produto_id_INT = Helper::POST("sistema_produto_id_INT{$numReg}"); 
		$this->sistema_tipo_mobile_id_INT = Helper::POST("sistema_tipo_mobile_id_INT{$numReg}"); 
		$this->certificado = Helper::POST("certificado{$numReg}"); 
		$this->senha_certificado = Helper::POST("senha_certificado{$numReg}"); 
		$this->package = Helper::POST("package{$numReg}"); 
		$this->nome_banco = Helper::POST("nome_banco{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->sistema_produto_id_INT = Helper::GET("sistema_produto_id_INT{$numReg}"); 
		$this->sistema_tipo_mobile_id_INT = Helper::GET("sistema_tipo_mobile_id_INT{$numReg}"); 
		$this->certificado = Helper::GET("certificado{$numReg}"); 
		$this->senha_certificado = Helper::GET("senha_certificado{$numReg}"); 
		$this->package = Helper::GET("package{$numReg}"); 
		$this->nome_banco = Helper::GET("nome_banco{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_produto_id_INT = $this->sistema_produto_id_INT, ";

	} 

	if(isset($tipo["sistema_tipo_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_tipo_mobile_id_INT = $this->sistema_tipo_mobile_id_INT, ";

	} 

	if(isset($tipo["certificado{$numReg}"]) || $tipo == null){

		$upd.= "certificado = $this->certificado, ";

	} 

	if(isset($tipo["senha_certificado{$numReg}"]) || $tipo == null){

		$upd.= "senha_certificado = $this->senha_certificado, ";

	} 

	if(isset($tipo["package{$numReg}"]) || $tipo == null){

		$upd.= "package = $this->package, ";

	} 

	if(isset($tipo["nome_banco{$numReg}"]) || $tipo == null){

		$upd.= "nome_banco = $this->nome_banco, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_produto_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    