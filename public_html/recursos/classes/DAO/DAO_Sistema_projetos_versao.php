<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_projetos_versao
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_projetos_versao.php
    * TABELA MYSQL:    sistema_projetos_versao
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_projetos_versao extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_id_INT;
	public $obj;
	public $projetos_versao_id_INT;
	public $objSistema;
	public $data_homologacao_DATETIME;
	public $data_producao_DATETIME;
	public $trunk_historico_id_INT;
	public $objProjetos_versao;


    public $nomeEntidade;

	public $data_homologacao_DATETIME_UNIX;
	public $data_producao_DATETIME_UNIX;


    

	public $label_id;
	public $label_sistema_id_INT;
	public $label_projetos_versao_id_INT;
	public $label_data_homologacao_DATETIME;
	public $label_data_producao_DATETIME;
	public $label_trunk_historico_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_projetos_versao";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema($this->getDatabase());
		if($this->sistema_id_INT != null) 
		$this->obj->select($this->sistema_id_INT);
	}
	return $this->obj ;
}
function getFkObjProjetos_versao(){
	if($this->objSistema ==null){
		$this->objSistema = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objSistema->select($this->projetos_versao_id_INT);
	}
	return $this->objSistema ;
}
function getFkObjTrunk_historico(){
	if($this->objProjetos_versao ==null){
		$this->objProjetos_versao = new EXTDAO_Trunk_historico($this->getDatabase());
		if($this->trunk_historico_id_INT != null) 
		$this->objProjetos_versao->select($this->trunk_historico_id_INT);
	}
	return $this->objProjetos_versao ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema($objArgumentos){

		$objArgumentos->nome="sistema_id_INT";
		$objArgumentos->id="sistema_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTrunk_historico($objArgumentos){

		$objArgumentos->nome="trunk_historico_id_INT";
		$objArgumentos->id="trunk_historico_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTrunk_historico()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_projetos_versao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_projetos_versao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_id_INT()
    {
    	return $this->sistema_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    function getData_homologacao_DATETIME_UNIX()
    {
    	return $this->data_homologacao_DATETIME_UNIX;
    }
    
    public function getData_homologacao_DATETIME()
    {
    	return $this->data_homologacao_DATETIME;
    }
    
    function getData_producao_DATETIME_UNIX()
    {
    	return $this->data_producao_DATETIME_UNIX;
    }
    
    public function getData_producao_DATETIME()
    {
    	return $this->data_producao_DATETIME;
    }
    
    public function getTrunk_historico_id_INT()
    {
    	return $this->trunk_historico_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_id_INT($val)
    {
    	$this->sistema_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    
    function setData_homologacao_DATETIME($val)
    {
    	$this->data_homologacao_DATETIME =  $val;
    }
    
    function setData_producao_DATETIME($val)
    {
    	$this->data_producao_DATETIME =  $val;
    }
    
    function setTrunk_historico_id_INT($val)
    {
    	$this->trunk_historico_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_homologacao_DATETIME) AS data_homologacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_producao_DATETIME) AS data_producao_DATETIME_UNIX FROM sistema_projetos_versao WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->sistema_id_INT = $row->sistema_id_INT;
        if(isset($this->objSistema))
			$this->objSistema->select($this->sistema_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

        $this->data_homologacao_DATETIME = $row->data_homologacao_DATETIME;
        $this->data_homologacao_DATETIME_UNIX = $row->data_homologacao_DATETIME_UNIX;

        $this->data_producao_DATETIME = $row->data_producao_DATETIME;
        $this->data_producao_DATETIME_UNIX = $row->data_producao_DATETIME_UNIX;

        $this->trunk_historico_id_INT = $row->trunk_historico_id_INT;
        if(isset($this->objTrunk_historico))
			$this->objTrunk_historico->select($this->trunk_historico_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_projetos_versao WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_projetos_versao ( sistema_id_INT , projetos_versao_id_INT , data_homologacao_DATETIME , data_producao_DATETIME , trunk_historico_id_INT ) VALUES ( {$this->sistema_id_INT} , {$this->projetos_versao_id_INT} , {$this->data_homologacao_DATETIME} , {$this->data_producao_DATETIME} , {$this->trunk_historico_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_id_INT(){ 

		return "sistema_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}

	public function nomeCampoData_homologacao_DATETIME(){ 

		return "data_homologacao_DATETIME";

	}

	public function nomeCampoData_producao_DATETIME(){ 

		return "data_producao_DATETIME";

	}

	public function nomeCampoTrunk_historico_id_INT(){ 

		return "trunk_historico_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_id_INT($objArguments){

		$objArguments->nome = "sistema_id_INT";
		$objArguments->id = "sistema_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_homologacao_DATETIME($objArguments){

		$objArguments->nome = "data_homologacao_DATETIME";
		$objArguments->id = "data_homologacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_producao_DATETIME($objArguments){

		$objArguments->nome = "data_producao_DATETIME";
		$objArguments->id = "data_producao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoTrunk_historico_id_INT($objArguments){

		$objArguments->nome = "trunk_historico_id_INT";
		$objArguments->id = "trunk_historico_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_id_INT == ""){

			$this->sistema_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}

		if($this->trunk_historico_id_INT == ""){

			$this->trunk_historico_id_INT = "null";

		}



	$this->data_homologacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_homologacao_DATETIME); 
	$this->data_producao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_producao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_homologacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_homologacao_DATETIME); 
	$this->data_producao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_producao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->sistema_id_INT = null; 
		$this->objSistema= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;
		$this->data_homologacao_DATETIME = null; 
		$this->data_producao_DATETIME = null; 
		$this->trunk_historico_id_INT = null; 
		$this->objTrunk_historico= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("sistema_id_INT", $this->sistema_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 
		Helper::setSession("data_homologacao_DATETIME", $this->data_homologacao_DATETIME); 
		Helper::setSession("data_producao_DATETIME", $this->data_producao_DATETIME); 
		Helper::setSession("trunk_historico_id_INT", $this->trunk_historico_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("sistema_id_INT");
		Helper::clearSession("projetos_versao_id_INT");
		Helper::clearSession("data_homologacao_DATETIME");
		Helper::clearSession("data_producao_DATETIME");
		Helper::clearSession("trunk_historico_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->sistema_id_INT = Helper::SESSION("sistema_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::SESSION("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::SESSION("data_producao_DATETIME{$numReg}"); 
		$this->trunk_historico_id_INT = Helper::SESSION("trunk_historico_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->sistema_id_INT = Helper::POST("sistema_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::POST("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::POST("data_producao_DATETIME{$numReg}"); 
		$this->trunk_historico_id_INT = Helper::POST("trunk_historico_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->sistema_id_INT = Helper::GET("sistema_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::GET("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::GET("data_producao_DATETIME{$numReg}"); 
		$this->trunk_historico_id_INT = Helper::GET("trunk_historico_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_id_INT = $this->sistema_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

	if(isset($tipo["data_homologacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_homologacao_DATETIME = $this->data_homologacao_DATETIME, ";

	} 

	if(isset($tipo["data_producao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_producao_DATETIME = $this->data_producao_DATETIME, ";

	} 

	if(isset($tipo["trunk_historico_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "trunk_historico_id_INT = $this->trunk_historico_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_projetos_versao SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    