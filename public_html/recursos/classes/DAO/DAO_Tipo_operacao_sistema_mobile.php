<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tipo_operacao_sistema_mobile
    * DATA DE GERAÇÃO: 29.03.2013
    * ARQUIVO:         DAO_Tipo_operacao_sistema_mobile.php
    * TABELA MYSQL:    tipo_operacao_sistema_mobile
    * BANCO DE DADOS:  biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Tipo_operacao_sistema_mobile extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "tipo_operacao_sistema_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "nome";



    }

    public function valorCampoLabel(){

    	return $this->getNome();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_tipo_operacao_sistema_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tipo_operacao_sistema_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM tipo_operacao_sistema_mobile WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM tipo_operacao_sistema_mobile WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO tipo_operacao_sistema_mobile ( nome ) VALUES ( '$this->nome' )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["nome"] = $this->nome; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["nome"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_POST["nome{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_GET["nome{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["nome{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome = '$this->nome', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE tipo_operacao_sistema_mobile SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>
