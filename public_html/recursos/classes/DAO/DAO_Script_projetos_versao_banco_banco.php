<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Script_projetos_versao_banco_banco
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Script_projetos_versao_banco_banco.php
    * TABELA MYSQL:    script_projetos_versao_banco_banco
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Script_projetos_versao_banco_banco extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $is_atualizacao_BOOLEAN;
	public $projetos_versao_banco_banco_id_INT;
	public $obj;
	public $tipo_script_banco_id_INT;
	public $objProjetos_versao_banco_banco;
	public $data_criacao_DATETIME;
	public $data_atualizacao_DATETIME;


    public $nomeEntidade;

	public $data_criacao_DATETIME_UNIX;
	public $data_atualizacao_DATETIME_UNIX;


    

	public $label_id;
	public $label_is_atualizacao_BOOLEAN;
	public $label_projetos_versao_banco_banco_id_INT;
	public $label_tipo_script_banco_id_INT;
	public $label_data_criacao_DATETIME;
	public $label_data_atualizacao_DATETIME;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "script_projetos_versao_banco_banco";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_versao_banco_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->obj->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_script_banco(){
	if($this->objProjetos_versao_banco_banco ==null){
		$this->objProjetos_versao_banco_banco = new EXTDAO_Tipo_script_banco($this->getDatabase());
		if($this->tipo_script_banco_id_INT != null) 
		$this->objProjetos_versao_banco_banco->select($this->tipo_script_banco_id_INT);
	}
	return $this->objProjetos_versao_banco_banco ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_script_banco($objArgumentos){

		$objArgumentos->nome="tipo_script_banco_id_INT";
		$objArgumentos->id="tipo_script_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_script_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_script_projetos_versao_banco_banco", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_script_projetos_versao_banco_banco", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getIs_atualizacao_BOOLEAN()
    {
    	return $this->is_atualizacao_BOOLEAN;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    public function getTipo_script_banco_id_INT()
    {
    	return $this->tipo_script_banco_id_INT;
    }
    
    function getData_criacao_DATETIME_UNIX()
    {
    	return $this->data_criacao_DATETIME_UNIX;
    }
    
    public function getData_criacao_DATETIME()
    {
    	return $this->data_criacao_DATETIME;
    }
    
    function getData_atualizacao_DATETIME_UNIX()
    {
    	return $this->data_atualizacao_DATETIME_UNIX;
    }
    
    public function getData_atualizacao_DATETIME()
    {
    	return $this->data_atualizacao_DATETIME;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setIs_atualizacao_BOOLEAN($val)
    {
    	$this->is_atualizacao_BOOLEAN =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setTipo_script_banco_id_INT($val)
    {
    	$this->tipo_script_banco_id_INT =  $val;
    }
    
    function setData_criacao_DATETIME($val)
    {
    	$this->data_criacao_DATETIME =  $val;
    }
    
    function setData_atualizacao_DATETIME($val)
    {
    	$this->data_atualizacao_DATETIME =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_criacao_DATETIME) AS data_criacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_atualizacao_DATETIME) AS data_atualizacao_DATETIME_UNIX FROM script_projetos_versao_banco_banco WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->is_atualizacao_BOOLEAN = $row->is_atualizacao_BOOLEAN;
        
        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

        $this->tipo_script_banco_id_INT = $row->tipo_script_banco_id_INT;
        if(isset($this->objTipo_script_banco))
			$this->objTipo_script_banco->select($this->tipo_script_banco_id_INT);

        $this->data_criacao_DATETIME = $row->data_criacao_DATETIME;
        $this->data_criacao_DATETIME_UNIX = $row->data_criacao_DATETIME_UNIX;

        $this->data_atualizacao_DATETIME = $row->data_atualizacao_DATETIME;
        $this->data_atualizacao_DATETIME_UNIX = $row->data_atualizacao_DATETIME_UNIX;

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM script_projetos_versao_banco_banco WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO script_projetos_versao_banco_banco ( is_atualizacao_BOOLEAN , projetos_versao_banco_banco_id_INT , tipo_script_banco_id_INT , data_criacao_DATETIME , data_atualizacao_DATETIME ) VALUES ( {$this->is_atualizacao_BOOLEAN} , {$this->projetos_versao_banco_banco_id_INT} , {$this->tipo_script_banco_id_INT} , {$this->data_criacao_DATETIME} , {$this->data_atualizacao_DATETIME} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoIs_atualizacao_BOOLEAN(){ 

		return "is_atualizacao_BOOLEAN";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoTipo_script_banco_id_INT(){ 

		return "tipo_script_banco_id_INT";

	}

	public function nomeCampoData_criacao_DATETIME(){ 

		return "data_criacao_DATETIME";

	}

	public function nomeCampoData_atualizacao_DATETIME(){ 

		return "data_atualizacao_DATETIME";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoIs_atualizacao_BOOLEAN($objArguments){

		$objArguments->nome = "is_atualizacao_BOOLEAN";
		$objArguments->id = "is_atualizacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_script_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_script_banco_id_INT";
		$objArguments->id = "tipo_script_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_criacao_DATETIME($objArguments){

		$objArguments->nome = "data_criacao_DATETIME";
		$objArguments->id = "data_criacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_atualizacao_DATETIME($objArguments){

		$objArguments->nome = "data_atualizacao_DATETIME";
		$objArguments->id = "data_atualizacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->is_atualizacao_BOOLEAN == ""){

			$this->is_atualizacao_BOOLEAN = "null";

		}

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->tipo_script_banco_id_INT == ""){

			$this->tipo_script_banco_id_INT = "null";

		}



	$this->data_criacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_criacao_DATETIME); 
	$this->data_atualizacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_atualizacao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_criacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_criacao_DATETIME); 
	$this->data_atualizacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_atualizacao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->is_atualizacao_BOOLEAN = null; 
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;
		$this->tipo_script_banco_id_INT = null; 
		$this->objTipo_script_banco= null;
		$this->data_criacao_DATETIME = null; 
		$this->data_atualizacao_DATETIME = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("is_atualizacao_BOOLEAN", $this->is_atualizacao_BOOLEAN); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 
		Helper::setSession("tipo_script_banco_id_INT", $this->tipo_script_banco_id_INT); 
		Helper::setSession("data_criacao_DATETIME", $this->data_criacao_DATETIME); 
		Helper::setSession("data_atualizacao_DATETIME", $this->data_atualizacao_DATETIME); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("is_atualizacao_BOOLEAN");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");
		Helper::clearSession("tipo_script_banco_id_INT");
		Helper::clearSession("data_criacao_DATETIME");
		Helper::clearSession("data_atualizacao_DATETIME");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->is_atualizacao_BOOLEAN = Helper::SESSION("is_atualizacao_BOOLEAN{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_script_banco_id_INT = Helper::SESSION("tipo_script_banco_id_INT{$numReg}"); 
		$this->data_criacao_DATETIME = Helper::SESSION("data_criacao_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::SESSION("data_atualizacao_DATETIME{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->is_atualizacao_BOOLEAN = Helper::POST("is_atualizacao_BOOLEAN{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_script_banco_id_INT = Helper::POST("tipo_script_banco_id_INT{$numReg}"); 
		$this->data_criacao_DATETIME = Helper::POST("data_criacao_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::POST("data_atualizacao_DATETIME{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->is_atualizacao_BOOLEAN = Helper::GET("is_atualizacao_BOOLEAN{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_script_banco_id_INT = Helper::GET("tipo_script_banco_id_INT{$numReg}"); 
		$this->data_criacao_DATETIME = Helper::GET("data_criacao_DATETIME{$numReg}"); 
		$this->data_atualizacao_DATETIME = Helper::GET("data_atualizacao_DATETIME{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["is_atualizacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "is_atualizacao_BOOLEAN = $this->is_atualizacao_BOOLEAN, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["tipo_script_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_script_banco_id_INT = $this->tipo_script_banco_id_INT, ";

	} 

	if(isset($tipo["data_criacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_criacao_DATETIME = $this->data_criacao_DATETIME, ";

	} 

	if(isset($tipo["data_atualizacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_atualizacao_DATETIME = $this->data_atualizacao_DATETIME, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE script_projetos_versao_banco_banco SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    