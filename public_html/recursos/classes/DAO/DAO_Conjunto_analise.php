<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Conjunto_analise
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Conjunto_analise.php
    * TABELA MYSQL:    conjunto_analise
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Conjunto_analise extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $tipo_conjunto_analise_id_INT;
	public $obj;
	public $tipo_analise_projeto_id_INT;
	public $objTipo_conjunto_analise;
	public $tipo_banco_id_INT;
	public $objTipo_analise_projeto;
	public $seq_INT;


    public $nomeEntidade;



    

	public $label_id;
	public $label_tipo_conjunto_analise_id_INT;
	public $label_tipo_analise_projeto_id_INT;
	public $label_tipo_banco_id_INT;
	public $label_seq_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "conjunto_analise";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTipo_conjunto_analise(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tipo_conjunto_analise($this->getDatabase());
		if($this->tipo_conjunto_analise_id_INT != null) 
		$this->obj->select($this->tipo_conjunto_analise_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_analise_projeto(){
	if($this->objTipo_conjunto_analise ==null){
		$this->objTipo_conjunto_analise = new EXTDAO_Tipo_analise_projeto($this->getDatabase());
		if($this->tipo_analise_projeto_id_INT != null) 
		$this->objTipo_conjunto_analise->select($this->tipo_analise_projeto_id_INT);
	}
	return $this->objTipo_conjunto_analise ;
}
function getFkObjTipo_banco(){
	if($this->objTipo_analise_projeto ==null){
		$this->objTipo_analise_projeto = new EXTDAO_Tipo_banco($this->getDatabase());
		if($this->tipo_banco_id_INT != null) 
		$this->objTipo_analise_projeto->select($this->tipo_banco_id_INT);
	}
	return $this->objTipo_analise_projeto ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTipo_conjunto_analise($objArgumentos){

		$objArgumentos->nome="tipo_conjunto_analise_id_INT";
		$objArgumentos->id="tipo_conjunto_analise_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_conjunto_analise()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_analise_projeto($objArgumentos){

		$objArgumentos->nome="tipo_analise_projeto_id_INT";
		$objArgumentos->id="tipo_analise_projeto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_analise_projeto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_banco($objArgumentos){

		$objArgumentos->nome="tipo_banco_id_INT";
		$objArgumentos->id="tipo_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_conjunto_analise", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_conjunto_analise", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTipo_conjunto_analise_id_INT()
    {
    	return $this->tipo_conjunto_analise_id_INT;
    }
    
    public function getTipo_analise_projeto_id_INT()
    {
    	return $this->tipo_analise_projeto_id_INT;
    }
    
    public function getTipo_banco_id_INT()
    {
    	return $this->tipo_banco_id_INT;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTipo_conjunto_analise_id_INT($val)
    {
    	$this->tipo_conjunto_analise_id_INT =  $val;
    }
    
    function setTipo_analise_projeto_id_INT($val)
    {
    	$this->tipo_analise_projeto_id_INT =  $val;
    }
    
    function setTipo_banco_id_INT($val)
    {
    	$this->tipo_banco_id_INT =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM conjunto_analise WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->tipo_conjunto_analise_id_INT = $row->tipo_conjunto_analise_id_INT;
        if(isset($this->objTipo_conjunto_analise))
			$this->objTipo_conjunto_analise->select($this->tipo_conjunto_analise_id_INT);

        $this->tipo_analise_projeto_id_INT = $row->tipo_analise_projeto_id_INT;
        if(isset($this->objTipo_analise_projeto))
			$this->objTipo_analise_projeto->select($this->tipo_analise_projeto_id_INT);

        $this->tipo_banco_id_INT = $row->tipo_banco_id_INT;
        if(isset($this->objTipo_banco))
			$this->objTipo_banco->select($this->tipo_banco_id_INT);

        $this->seq_INT = $row->seq_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM conjunto_analise WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO conjunto_analise ( tipo_conjunto_analise_id_INT , tipo_analise_projeto_id_INT , tipo_banco_id_INT , seq_INT ) VALUES ( {$this->tipo_conjunto_analise_id_INT} , {$this->tipo_analise_projeto_id_INT} , {$this->tipo_banco_id_INT} , {$this->seq_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTipo_conjunto_analise_id_INT(){ 

		return "tipo_conjunto_analise_id_INT";

	}

	public function nomeCampoTipo_analise_projeto_id_INT(){ 

		return "tipo_analise_projeto_id_INT";

	}

	public function nomeCampoTipo_banco_id_INT(){ 

		return "tipo_banco_id_INT";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTipo_conjunto_analise_id_INT($objArguments){

		$objArguments->nome = "tipo_conjunto_analise_id_INT";
		$objArguments->id = "tipo_conjunto_analise_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_analise_projeto_id_INT($objArguments){

		$objArguments->nome = "tipo_analise_projeto_id_INT";
		$objArguments->id = "tipo_analise_projeto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_banco_id_INT";
		$objArguments->id = "tipo_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->tipo_conjunto_analise_id_INT == ""){

			$this->tipo_conjunto_analise_id_INT = "null";

		}

		if($this->tipo_analise_projeto_id_INT == ""){

			$this->tipo_analise_projeto_id_INT = "null";

		}

		if($this->tipo_banco_id_INT == ""){

			$this->tipo_banco_id_INT = "null";

		}

		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->tipo_conjunto_analise_id_INT = null; 
		$this->objTipo_conjunto_analise= null;
		$this->tipo_analise_projeto_id_INT = null; 
		$this->objTipo_analise_projeto= null;
		$this->tipo_banco_id_INT = null; 
		$this->objTipo_banco= null;
		$this->seq_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("tipo_conjunto_analise_id_INT", $this->tipo_conjunto_analise_id_INT); 
		Helper::setSession("tipo_analise_projeto_id_INT", $this->tipo_analise_projeto_id_INT); 
		Helper::setSession("tipo_banco_id_INT", $this->tipo_banco_id_INT); 
		Helper::setSession("seq_INT", $this->seq_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("tipo_conjunto_analise_id_INT");
		Helper::clearSession("tipo_analise_projeto_id_INT");
		Helper::clearSession("tipo_banco_id_INT");
		Helper::clearSession("seq_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->tipo_conjunto_analise_id_INT = Helper::SESSION("tipo_conjunto_analise_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::SESSION("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->tipo_banco_id_INT = Helper::SESSION("tipo_banco_id_INT{$numReg}"); 
		$this->seq_INT = Helper::SESSION("seq_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->tipo_conjunto_analise_id_INT = Helper::POST("tipo_conjunto_analise_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::POST("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->tipo_banco_id_INT = Helper::POST("tipo_banco_id_INT{$numReg}"); 
		$this->seq_INT = Helper::POST("seq_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->tipo_conjunto_analise_id_INT = Helper::GET("tipo_conjunto_analise_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::GET("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->tipo_banco_id_INT = Helper::GET("tipo_banco_id_INT{$numReg}"); 
		$this->seq_INT = Helper::GET("seq_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["tipo_conjunto_analise_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_conjunto_analise_id_INT = $this->tipo_conjunto_analise_id_INT, ";

	} 

	if(isset($tipo["tipo_analise_projeto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_analise_projeto_id_INT = $this->tipo_analise_projeto_id_INT, ";

	} 

	if(isset($tipo["tipo_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_banco_id_INT = $this->tipo_banco_id_INT, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == null){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE conjunto_analise SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    