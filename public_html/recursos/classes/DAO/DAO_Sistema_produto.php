<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_produto
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_produto.php
    * TABELA MYSQL:    sistema_produto
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_produto extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $url_svn;
	public $sistema_id_INT;
	public $obj;
	public $is_mobile_BOOLEAN;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_url_svn;
	public $label_sistema_id_INT;
	public $label_is_mobile_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_produto";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema($this->getDatabase());
		if($this->sistema_id_INT != null) 
		$this->obj->select($this->sistema_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema($objArgumentos){

		$objArgumentos->nome="sistema_id_INT";
		$objArgumentos->id="sistema_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_produto", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_produto", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getUrl_svn()
    {
    	return $this->url_svn;
    }
    
    public function getSistema_id_INT()
    {
    	return $this->sistema_id_INT;
    }
    
    public function getIs_mobile_BOOLEAN()
    {
    	return $this->is_mobile_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setUrl_svn($val)
    {
    	$this->url_svn =  $val;
    }
    
    function setSistema_id_INT($val)
    {
    	$this->sistema_id_INT =  $val;
    }
    
    function setIs_mobile_BOOLEAN($val)
    {
    	$this->is_mobile_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema_produto WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->url_svn = $row->url_svn;
        
        $this->sistema_id_INT = $row->sistema_id_INT;
        if(isset($this->objSistema))
			$this->objSistema->select($this->sistema_id_INT);

        $this->is_mobile_BOOLEAN = $row->is_mobile_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_produto WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_produto ( nome , url_svn , sistema_id_INT , is_mobile_BOOLEAN ) VALUES ( {$this->nome} , {$this->url_svn} , {$this->sistema_id_INT} , {$this->is_mobile_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoUrl_svn(){ 

		return "url_svn";

	}

	public function nomeCampoSistema_id_INT(){ 

		return "sistema_id_INT";

	}

	public function nomeCampoIs_mobile_BOOLEAN(){ 

		return "is_mobile_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUrl_svn($objArguments){

		$objArguments->nome = "url_svn";
		$objArguments->id = "url_svn";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSistema_id_INT($objArguments){

		$objArguments->nome = "sistema_id_INT";
		$objArguments->id = "sistema_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoIs_mobile_BOOLEAN($objArguments){

		$objArguments->nome = "is_mobile_BOOLEAN";
		$objArguments->id = "is_mobile_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->url_svn = $this->formatarDadosParaSQL($this->url_svn);
		if($this->sistema_id_INT == ""){

			$this->sistema_id_INT = "null";

		}

		if($this->is_mobile_BOOLEAN == ""){

			$this->is_mobile_BOOLEAN = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->url_svn = null; 
		$this->sistema_id_INT = null; 
		$this->objSistema= null;
		$this->is_mobile_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("url_svn", $this->url_svn); 
		Helper::setSession("sistema_id_INT", $this->sistema_id_INT); 
		Helper::setSession("is_mobile_BOOLEAN", $this->is_mobile_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("url_svn");
		Helper::clearSession("sistema_id_INT");
		Helper::clearSession("is_mobile_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->url_svn = Helper::SESSION("url_svn{$numReg}"); 
		$this->sistema_id_INT = Helper::SESSION("sistema_id_INT{$numReg}"); 
		$this->is_mobile_BOOLEAN = Helper::SESSION("is_mobile_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->url_svn = Helper::POST("url_svn{$numReg}"); 
		$this->sistema_id_INT = Helper::POST("sistema_id_INT{$numReg}"); 
		$this->is_mobile_BOOLEAN = Helper::POST("is_mobile_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->url_svn = Helper::GET("url_svn{$numReg}"); 
		$this->sistema_id_INT = Helper::GET("sistema_id_INT{$numReg}"); 
		$this->is_mobile_BOOLEAN = Helper::GET("is_mobile_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["url_svn{$numReg}"]) || $tipo == null){

		$upd.= "url_svn = $this->url_svn, ";

	} 

	if(isset($tipo["sistema_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_id_INT = $this->sistema_id_INT, ";

	} 

	if(isset($tipo["is_mobile_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "is_mobile_BOOLEAN = $this->is_mobile_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_produto SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    