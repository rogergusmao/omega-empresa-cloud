<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Monitora_tela_web_para_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Monitora_tela_web_para_mobile.php
    * TABELA MYSQL:    monitora_tela_web_para_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Monitora_tela_web_para_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $operacao_sistema_mobile_id_INT;
	public $obj;
	public $desenha_circulo_BOOLEAN;
	public $desenha_quadrado_BOOLEAN;
	public $acao_clique_BOOLEAN;
	public $coordenada_x_INT;
	public $coordenada_y_INT;
	public $mobile_identificador_id_INT;
	public $objOperacao_sistema_mobile;
	public $tipo_operacao_monitora_tela_id_INT;
	public $objMobile_identificador;
	public $sequencia_operacao_INT;
	public $data_ocorrencia_DATETIME;
	public $desliga_monitoramento_BOOLEAN;


    public $nomeEntidade;

	public $data_ocorrencia_DATETIME_UNIX;


    

	public $label_id;
	public $label_operacao_sistema_mobile_id_INT;
	public $label_desenha_circulo_BOOLEAN;
	public $label_desenha_quadrado_BOOLEAN;
	public $label_acao_clique_BOOLEAN;
	public $label_coordenada_x_INT;
	public $label_coordenada_y_INT;
	public $label_mobile_identificador_id_INT;
	public $label_tipo_operacao_monitora_tela_id_INT;
	public $label_sequencia_operacao_INT;
	public $label_data_ocorrencia_DATETIME;
	public $label_desliga_monitoramento_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "monitora_tela_web_para_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjOperacao_sistema_mobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->obj->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->obj ;
}
function getFkObjMobile_identificador(){
	if($this->objOperacao_sistema_mobile ==null){
		$this->objOperacao_sistema_mobile = new EXTDAO_Mobile_identificador($this->getDatabase());
		if($this->mobile_identificador_id_INT != null) 
		$this->objOperacao_sistema_mobile->select($this->mobile_identificador_id_INT);
	}
	return $this->objOperacao_sistema_mobile ;
}
function getFkObjTipo_operacao_monitora_tela(){
	if($this->objMobile_identificador ==null){
		$this->objMobile_identificador = new EXTDAO_Tipo_operacao_monitora_tela($this->getDatabase());
		if($this->tipo_operacao_monitora_tela_id_INT != null) 
		$this->objMobile_identificador->select($this->tipo_operacao_monitora_tela_id_INT);
	}
	return $this->objMobile_identificador ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllMobile_identificador($objArgumentos){

		$objArgumentos->nome="mobile_identificador_id_INT";
		$objArgumentos->id="mobile_identificador_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjMobile_identificador()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_monitora_tela($objArgumentos){

		$objArgumentos->nome="tipo_operacao_monitora_tela_id_INT";
		$objArgumentos->id="tipo_operacao_monitora_tela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_operacao_monitora_tela()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_monitora_tela_web_para_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_monitora_tela_web_para_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    public function getDesenha_circulo_BOOLEAN()
    {
    	return $this->desenha_circulo_BOOLEAN;
    }
    
    public function getDesenha_quadrado_BOOLEAN()
    {
    	return $this->desenha_quadrado_BOOLEAN;
    }
    
    public function getAcao_clique_BOOLEAN()
    {
    	return $this->acao_clique_BOOLEAN;
    }
    
    public function getCoordenada_x_INT()
    {
    	return $this->coordenada_x_INT;
    }
    
    public function getCoordenada_y_INT()
    {
    	return $this->coordenada_y_INT;
    }
    
    public function getMobile_identificador_id_INT()
    {
    	return $this->mobile_identificador_id_INT;
    }
    
    public function getTipo_operacao_monitora_tela_id_INT()
    {
    	return $this->tipo_operacao_monitora_tela_id_INT;
    }
    
    public function getSequencia_operacao_INT()
    {
    	return $this->sequencia_operacao_INT;
    }
    
    function getData_ocorrencia_DATETIME_UNIX()
    {
    	return $this->data_ocorrencia_DATETIME_UNIX;
    }
    
    public function getData_ocorrencia_DATETIME()
    {
    	return $this->data_ocorrencia_DATETIME;
    }
    
    public function getDesliga_monitoramento_BOOLEAN()
    {
    	return $this->desliga_monitoramento_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setDesenha_circulo_BOOLEAN($val)
    {
    	$this->desenha_circulo_BOOLEAN =  $val;
    }
    
    function setDesenha_quadrado_BOOLEAN($val)
    {
    	$this->desenha_quadrado_BOOLEAN =  $val;
    }
    
    function setAcao_clique_BOOLEAN($val)
    {
    	$this->acao_clique_BOOLEAN =  $val;
    }
    
    function setCoordenada_x_INT($val)
    {
    	$this->coordenada_x_INT =  $val;
    }
    
    function setCoordenada_y_INT($val)
    {
    	$this->coordenada_y_INT =  $val;
    }
    
    function setMobile_identificador_id_INT($val)
    {
    	$this->mobile_identificador_id_INT =  $val;
    }
    
    function setTipo_operacao_monitora_tela_id_INT($val)
    {
    	$this->tipo_operacao_monitora_tela_id_INT =  $val;
    }
    
    function setSequencia_operacao_INT($val)
    {
    	$this->sequencia_operacao_INT =  $val;
    }
    
    function setData_ocorrencia_DATETIME($val)
    {
    	$this->data_ocorrencia_DATETIME =  $val;
    }
    
    function setDesliga_monitoramento_BOOLEAN($val)
    {
    	$this->desliga_monitoramento_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_ocorrencia_DATETIME) AS data_ocorrencia_DATETIME_UNIX FROM monitora_tela_web_para_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

        $this->desenha_circulo_BOOLEAN = $row->desenha_circulo_BOOLEAN;
        
        $this->desenha_quadrado_BOOLEAN = $row->desenha_quadrado_BOOLEAN;
        
        $this->acao_clique_BOOLEAN = $row->acao_clique_BOOLEAN;
        
        $this->coordenada_x_INT = $row->coordenada_x_INT;
        
        $this->coordenada_y_INT = $row->coordenada_y_INT;
        
        $this->mobile_identificador_id_INT = $row->mobile_identificador_id_INT;
        if(isset($this->objMobile_identificador))
			$this->objMobile_identificador->select($this->mobile_identificador_id_INT);

        $this->tipo_operacao_monitora_tela_id_INT = $row->tipo_operacao_monitora_tela_id_INT;
        if(isset($this->objTipo_operacao_monitora_tela))
			$this->objTipo_operacao_monitora_tela->select($this->tipo_operacao_monitora_tela_id_INT);

        $this->sequencia_operacao_INT = $row->sequencia_operacao_INT;
        
        $this->data_ocorrencia_DATETIME = $row->data_ocorrencia_DATETIME;
        $this->data_ocorrencia_DATETIME_UNIX = $row->data_ocorrencia_DATETIME_UNIX;

        $this->desliga_monitoramento_BOOLEAN = $row->desliga_monitoramento_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM monitora_tela_web_para_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO monitora_tela_web_para_mobile ( operacao_sistema_mobile_id_INT , desenha_circulo_BOOLEAN , desenha_quadrado_BOOLEAN , acao_clique_BOOLEAN , coordenada_x_INT , coordenada_y_INT , mobile_identificador_id_INT , tipo_operacao_monitora_tela_id_INT , sequencia_operacao_INT , data_ocorrencia_DATETIME , desliga_monitoramento_BOOLEAN ) VALUES ( {$this->operacao_sistema_mobile_id_INT} , {$this->desenha_circulo_BOOLEAN} , {$this->desenha_quadrado_BOOLEAN} , {$this->acao_clique_BOOLEAN} , {$this->coordenada_x_INT} , {$this->coordenada_y_INT} , {$this->mobile_identificador_id_INT} , {$this->tipo_operacao_monitora_tela_id_INT} , {$this->sequencia_operacao_INT} , {$this->data_ocorrencia_DATETIME} , {$this->desliga_monitoramento_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoDesenha_circulo_BOOLEAN(){ 

		return "desenha_circulo_BOOLEAN";

	}

	public function nomeCampoDesenha_quadrado_BOOLEAN(){ 

		return "desenha_quadrado_BOOLEAN";

	}

	public function nomeCampoAcao_clique_BOOLEAN(){ 

		return "acao_clique_BOOLEAN";

	}

	public function nomeCampoCoordenada_x_INT(){ 

		return "coordenada_x_INT";

	}

	public function nomeCampoCoordenada_y_INT(){ 

		return "coordenada_y_INT";

	}

	public function nomeCampoMobile_identificador_id_INT(){ 

		return "mobile_identificador_id_INT";

	}

	public function nomeCampoTipo_operacao_monitora_tela_id_INT(){ 

		return "tipo_operacao_monitora_tela_id_INT";

	}

	public function nomeCampoSequencia_operacao_INT(){ 

		return "sequencia_operacao_INT";

	}

	public function nomeCampoData_ocorrencia_DATETIME(){ 

		return "data_ocorrencia_DATETIME";

	}

	public function nomeCampoDesliga_monitoramento_BOOLEAN(){ 

		return "desliga_monitoramento_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoDesenha_circulo_BOOLEAN($objArguments){

		$objArguments->nome = "desenha_circulo_BOOLEAN";
		$objArguments->id = "desenha_circulo_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoDesenha_quadrado_BOOLEAN($objArguments){

		$objArguments->nome = "desenha_quadrado_BOOLEAN";
		$objArguments->id = "desenha_quadrado_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoAcao_clique_BOOLEAN($objArguments){

		$objArguments->nome = "acao_clique_BOOLEAN";
		$objArguments->id = "acao_clique_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoCoordenada_x_INT($objArguments){

		$objArguments->nome = "coordenada_x_INT";
		$objArguments->id = "coordenada_x_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCoordenada_y_INT($objArguments){

		$objArguments->nome = "coordenada_y_INT";
		$objArguments->id = "coordenada_y_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMobile_identificador_id_INT($objArguments){

		$objArguments->nome = "mobile_identificador_id_INT";
		$objArguments->id = "mobile_identificador_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao_monitora_tela_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_monitora_tela_id_INT";
		$objArguments->id = "tipo_operacao_monitora_tela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSequencia_operacao_INT($objArguments){

		$objArguments->nome = "sequencia_operacao_INT";
		$objArguments->id = "sequencia_operacao_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_ocorrencia_DATETIME($objArguments){

		$objArguments->nome = "data_ocorrencia_DATETIME";
		$objArguments->id = "data_ocorrencia_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoDesliga_monitoramento_BOOLEAN($objArguments){

		$objArguments->nome = "desliga_monitoramento_BOOLEAN";
		$objArguments->id = "desliga_monitoramento_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}

		if($this->desenha_circulo_BOOLEAN == ""){

			$this->desenha_circulo_BOOLEAN = "null";

		}

		if($this->desenha_quadrado_BOOLEAN == ""){

			$this->desenha_quadrado_BOOLEAN = "null";

		}

		if($this->acao_clique_BOOLEAN == ""){

			$this->acao_clique_BOOLEAN = "null";

		}

		if($this->coordenada_x_INT == ""){

			$this->coordenada_x_INT = "null";

		}

		if($this->coordenada_y_INT == ""){

			$this->coordenada_y_INT = "null";

		}

		if($this->mobile_identificador_id_INT == ""){

			$this->mobile_identificador_id_INT = "null";

		}

		if($this->tipo_operacao_monitora_tela_id_INT == ""){

			$this->tipo_operacao_monitora_tela_id_INT = "null";

		}

		if($this->sequencia_operacao_INT == ""){

			$this->sequencia_operacao_INT = "null";

		}

		if($this->desliga_monitoramento_BOOLEAN == ""){

			$this->desliga_monitoramento_BOOLEAN = "null";

		}



	$this->data_ocorrencia_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ocorrencia_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_ocorrencia_DATETIME = $this->formatarDataTimeParaExibicao($this->data_ocorrencia_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;
		$this->desenha_circulo_BOOLEAN = null; 
		$this->desenha_quadrado_BOOLEAN = null; 
		$this->acao_clique_BOOLEAN = null; 
		$this->coordenada_x_INT = null; 
		$this->coordenada_y_INT = null; 
		$this->mobile_identificador_id_INT = null; 
		$this->objMobile_identificador= null;
		$this->tipo_operacao_monitora_tela_id_INT = null; 
		$this->objTipo_operacao_monitora_tela= null;
		$this->sequencia_operacao_INT = null; 
		$this->data_ocorrencia_DATETIME = null; 
		$this->desliga_monitoramento_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 
		Helper::setSession("desenha_circulo_BOOLEAN", $this->desenha_circulo_BOOLEAN); 
		Helper::setSession("desenha_quadrado_BOOLEAN", $this->desenha_quadrado_BOOLEAN); 
		Helper::setSession("acao_clique_BOOLEAN", $this->acao_clique_BOOLEAN); 
		Helper::setSession("coordenada_x_INT", $this->coordenada_x_INT); 
		Helper::setSession("coordenada_y_INT", $this->coordenada_y_INT); 
		Helper::setSession("mobile_identificador_id_INT", $this->mobile_identificador_id_INT); 
		Helper::setSession("tipo_operacao_monitora_tela_id_INT", $this->tipo_operacao_monitora_tela_id_INT); 
		Helper::setSession("sequencia_operacao_INT", $this->sequencia_operacao_INT); 
		Helper::setSession("data_ocorrencia_DATETIME", $this->data_ocorrencia_DATETIME); 
		Helper::setSession("desliga_monitoramento_BOOLEAN", $this->desliga_monitoramento_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("operacao_sistema_mobile_id_INT");
		Helper::clearSession("desenha_circulo_BOOLEAN");
		Helper::clearSession("desenha_quadrado_BOOLEAN");
		Helper::clearSession("acao_clique_BOOLEAN");
		Helper::clearSession("coordenada_x_INT");
		Helper::clearSession("coordenada_y_INT");
		Helper::clearSession("mobile_identificador_id_INT");
		Helper::clearSession("tipo_operacao_monitora_tela_id_INT");
		Helper::clearSession("sequencia_operacao_INT");
		Helper::clearSession("data_ocorrencia_DATETIME");
		Helper::clearSession("desliga_monitoramento_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->desenha_circulo_BOOLEAN = Helper::SESSION("desenha_circulo_BOOLEAN{$numReg}"); 
		$this->desenha_quadrado_BOOLEAN = Helper::SESSION("desenha_quadrado_BOOLEAN{$numReg}"); 
		$this->acao_clique_BOOLEAN = Helper::SESSION("acao_clique_BOOLEAN{$numReg}"); 
		$this->coordenada_x_INT = Helper::SESSION("coordenada_x_INT{$numReg}"); 
		$this->coordenada_y_INT = Helper::SESSION("coordenada_y_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::SESSION("mobile_identificador_id_INT{$numReg}"); 
		$this->tipo_operacao_monitora_tela_id_INT = Helper::SESSION("tipo_operacao_monitora_tela_id_INT{$numReg}"); 
		$this->sequencia_operacao_INT = Helper::SESSION("sequencia_operacao_INT{$numReg}"); 
		$this->data_ocorrencia_DATETIME = Helper::SESSION("data_ocorrencia_DATETIME{$numReg}"); 
		$this->desliga_monitoramento_BOOLEAN = Helper::SESSION("desliga_monitoramento_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->desenha_circulo_BOOLEAN = Helper::POST("desenha_circulo_BOOLEAN{$numReg}"); 
		$this->desenha_quadrado_BOOLEAN = Helper::POST("desenha_quadrado_BOOLEAN{$numReg}"); 
		$this->acao_clique_BOOLEAN = Helper::POST("acao_clique_BOOLEAN{$numReg}"); 
		$this->coordenada_x_INT = Helper::POST("coordenada_x_INT{$numReg}"); 
		$this->coordenada_y_INT = Helper::POST("coordenada_y_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::POST("mobile_identificador_id_INT{$numReg}"); 
		$this->tipo_operacao_monitora_tela_id_INT = Helper::POST("tipo_operacao_monitora_tela_id_INT{$numReg}"); 
		$this->sequencia_operacao_INT = Helper::POST("sequencia_operacao_INT{$numReg}"); 
		$this->data_ocorrencia_DATETIME = Helper::POST("data_ocorrencia_DATETIME{$numReg}"); 
		$this->desliga_monitoramento_BOOLEAN = Helper::POST("desliga_monitoramento_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->desenha_circulo_BOOLEAN = Helper::GET("desenha_circulo_BOOLEAN{$numReg}"); 
		$this->desenha_quadrado_BOOLEAN = Helper::GET("desenha_quadrado_BOOLEAN{$numReg}"); 
		$this->acao_clique_BOOLEAN = Helper::GET("acao_clique_BOOLEAN{$numReg}"); 
		$this->coordenada_x_INT = Helper::GET("coordenada_x_INT{$numReg}"); 
		$this->coordenada_y_INT = Helper::GET("coordenada_y_INT{$numReg}"); 
		$this->mobile_identificador_id_INT = Helper::GET("mobile_identificador_id_INT{$numReg}"); 
		$this->tipo_operacao_monitora_tela_id_INT = Helper::GET("tipo_operacao_monitora_tela_id_INT{$numReg}"); 
		$this->sequencia_operacao_INT = Helper::GET("sequencia_operacao_INT{$numReg}"); 
		$this->data_ocorrencia_DATETIME = Helper::GET("data_ocorrencia_DATETIME{$numReg}"); 
		$this->desliga_monitoramento_BOOLEAN = Helper::GET("desliga_monitoramento_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["desenha_circulo_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "desenha_circulo_BOOLEAN = $this->desenha_circulo_BOOLEAN, ";

	} 

	if(isset($tipo["desenha_quadrado_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "desenha_quadrado_BOOLEAN = $this->desenha_quadrado_BOOLEAN, ";

	} 

	if(isset($tipo["acao_clique_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "acao_clique_BOOLEAN = $this->acao_clique_BOOLEAN, ";

	} 

	if(isset($tipo["coordenada_x_INT{$numReg}"]) || $tipo == null){

		$upd.= "coordenada_x_INT = $this->coordenada_x_INT, ";

	} 

	if(isset($tipo["coordenada_y_INT{$numReg}"]) || $tipo == null){

		$upd.= "coordenada_y_INT = $this->coordenada_y_INT, ";

	} 

	if(isset($tipo["mobile_identificador_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "mobile_identificador_id_INT = $this->mobile_identificador_id_INT, ";

	} 

	if(isset($tipo["tipo_operacao_monitora_tela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_operacao_monitora_tela_id_INT = $this->tipo_operacao_monitora_tela_id_INT, ";

	} 

	if(isset($tipo["sequencia_operacao_INT{$numReg}"]) || $tipo == null){

		$upd.= "sequencia_operacao_INT = $this->sequencia_operacao_INT, ";

	} 

	if(isset($tipo["data_ocorrencia_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_ocorrencia_DATETIME = $this->data_ocorrencia_DATETIME, ";

	} 

	if(isset($tipo["desliga_monitoramento_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "desliga_monitoramento_BOOLEAN = $this->desliga_monitoramento_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE monitora_tela_web_para_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    