<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tabela_chave_atributo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Tabela_chave_atributo.php
    * TABELA MYSQL:    tabela_chave_atributo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Tabela_chave_atributo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $tabela_chave_id_INT;
	public $obj;
	public $atributo_id_INT;
	public $objTabela_chave;
	public $seq_INT;


    public $nomeEntidade;



    

	public $label_id;
	public $label_tabela_chave_id_INT;
	public $label_atributo_id_INT;
	public $label_seq_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "tabela_chave_atributo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTabela_chave(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tabela_chave($this->getDatabase());
		if($this->tabela_chave_id_INT != null) 
		$this->obj->select($this->tabela_chave_id_INT);
	}
	return $this->obj ;
}
function getFkObjAtributo(){
	if($this->objTabela_chave ==null){
		$this->objTabela_chave = new EXTDAO_Atributo($this->getDatabase());
		if($this->atributo_id_INT != null) 
		$this->objTabela_chave->select($this->atributo_id_INT);
	}
	return $this->objTabela_chave ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTabela_chave($objArgumentos){

		$objArgumentos->nome="tabela_chave_id_INT";
		$objArgumentos->id="tabela_chave_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela_chave()->getComboBox($objArgumentos);

	}

public function getComboBoxAllAtributo($objArgumentos){

		$objArgumentos->nome="atributo_id_INT";
		$objArgumentos->id="atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjAtributo()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_tabela_chave_atributo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tabela_chave_atributo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTabela_chave_id_INT()
    {
    	return $this->tabela_chave_id_INT;
    }
    
    public function getAtributo_id_INT()
    {
    	return $this->atributo_id_INT;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTabela_chave_id_INT($val)
    {
    	$this->tabela_chave_id_INT =  $val;
    }
    
    function setAtributo_id_INT($val)
    {
    	$this->atributo_id_INT =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM tabela_chave_atributo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->tabela_chave_id_INT = $row->tabela_chave_id_INT;
        if(isset($this->objTabela_chave))
			$this->objTabela_chave->select($this->tabela_chave_id_INT);

        $this->atributo_id_INT = $row->atributo_id_INT;
        if(isset($this->objAtributo))
			$this->objAtributo->select($this->atributo_id_INT);

        $this->seq_INT = $row->seq_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM tabela_chave_atributo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO tabela_chave_atributo ( tabela_chave_id_INT , atributo_id_INT , seq_INT ) VALUES ( {$this->tabela_chave_id_INT} , {$this->atributo_id_INT} , {$this->seq_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTabela_chave_id_INT(){ 

		return "tabela_chave_id_INT";

	}

	public function nomeCampoAtributo_id_INT(){ 

		return "atributo_id_INT";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTabela_chave_id_INT($objArguments){

		$objArguments->nome = "tabela_chave_id_INT";
		$objArguments->id = "tabela_chave_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoAtributo_id_INT($objArguments){

		$objArguments->nome = "atributo_id_INT";
		$objArguments->id = "atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->tabela_chave_id_INT == ""){

			$this->tabela_chave_id_INT = "null";

		}

		if($this->atributo_id_INT == ""){

			$this->atributo_id_INT = "null";

		}

		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->tabela_chave_id_INT = null; 
		$this->objTabela_chave= null;
		$this->atributo_id_INT = null; 
		$this->objAtributo= null;
		$this->seq_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("tabela_chave_id_INT", $this->tabela_chave_id_INT); 
		Helper::setSession("atributo_id_INT", $this->atributo_id_INT); 
		Helper::setSession("seq_INT", $this->seq_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("tabela_chave_id_INT");
		Helper::clearSession("atributo_id_INT");
		Helper::clearSession("seq_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->tabela_chave_id_INT = Helper::SESSION("tabela_chave_id_INT{$numReg}"); 
		$this->atributo_id_INT = Helper::SESSION("atributo_id_INT{$numReg}"); 
		$this->seq_INT = Helper::SESSION("seq_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->tabela_chave_id_INT = Helper::POST("tabela_chave_id_INT{$numReg}"); 
		$this->atributo_id_INT = Helper::POST("atributo_id_INT{$numReg}"); 
		$this->seq_INT = Helper::POST("seq_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->tabela_chave_id_INT = Helper::GET("tabela_chave_id_INT{$numReg}"); 
		$this->atributo_id_INT = Helper::GET("atributo_id_INT{$numReg}"); 
		$this->seq_INT = Helper::GET("seq_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["tabela_chave_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_chave_id_INT = $this->tabela_chave_id_INT, ";

	} 

	if(isset($tipo["atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "atributo_id_INT = $this->atributo_id_INT, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == null){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE tabela_chave_atributo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    