<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_versao_processo_estrutura
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_versao_processo_estrutura.php
    * TABELA MYSQL:    projetos_versao_processo_estrutura
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_versao_processo_estrutura extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_versao_banco_banco_id_INT;
	public $obj;
	public $processo_estrutura_id_INT;
	public $objProjetos_versao_banco_banco;
	public $data_inicio_DATETIME;
	public $data_fim_DATETIME;
	public $id_atual_projetos_versao_caminho_INT;
	public $total_ciclo_INT;
	public $ciclo_atual_INT;
	public $get_vetor_id;
	public $total_tempo_gasto_seg_INT;
	public $copia_projetos_versao_banco_banco_id_INT;
	public $objProcesso_estrutura;
	public $utiliza_sistema_projetos_versao_produto_id_INT;
	public $objCopia_projetos_versao_banco_banco;


    public $nomeEntidade;

	public $data_inicio_DATETIME_UNIX;
	public $data_fim_DATETIME_UNIX;


    

	public $label_id;
	public $label_projetos_versao_banco_banco_id_INT;
	public $label_processo_estrutura_id_INT;
	public $label_data_inicio_DATETIME;
	public $label_data_fim_DATETIME;
	public $label_id_atual_projetos_versao_caminho_INT;
	public $label_total_ciclo_INT;
	public $label_ciclo_atual_INT;
	public $label_get_vetor_id;
	public $label_total_tempo_gasto_seg_INT;
	public $label_copia_projetos_versao_banco_banco_id_INT;
	public $label_utiliza_sistema_projetos_versao_produto_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_versao_processo_estrutura";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_versao_banco_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->obj->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjProcesso_estrutura(){
	if($this->objProjetos_versao_banco_banco ==null){
		$this->objProjetos_versao_banco_banco = new EXTDAO_Processo_estrutura($this->getDatabase());
		if($this->processo_estrutura_id_INT != null) 
		$this->objProjetos_versao_banco_banco->select($this->processo_estrutura_id_INT);
	}
	return $this->objProjetos_versao_banco_banco ;
}
function getFkObjCopia_projetos_versao_banco_banco(){
	if($this->objProcesso_estrutura ==null){
		$this->objProcesso_estrutura = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->copia_projetos_versao_banco_banco_id_INT != null) 
		$this->objProcesso_estrutura->select($this->copia_projetos_versao_banco_banco_id_INT);
	}
	return $this->objProcesso_estrutura ;
}
function getFkObjUtiliza_sistema_projetos_versao_produto(){
	if($this->objCopia_projetos_versao_banco_banco ==null){
		$this->objCopia_projetos_versao_banco_banco = new EXTDAO_Sistema_projetos_versao_produto($this->getDatabase());
		if($this->utiliza_sistema_projetos_versao_produto_id_INT != null) 
		$this->objCopia_projetos_versao_banco_banco->select($this->utiliza_sistema_projetos_versao_produto_id_INT);
	}
	return $this->objCopia_projetos_versao_banco_banco ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProcesso_estrutura($objArgumentos){

		$objArgumentos->nome="processo_estrutura_id_INT";
		$objArgumentos->id="processo_estrutura_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProcesso_estrutura()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCopia_projetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="copia_projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="copia_projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCopia_projetos_versao_banco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllUtiliza_sistema_projetos_versao_produto($objArgumentos){

		$objArgumentos->nome="utiliza_sistema_projetos_versao_produto_id_INT";
		$objArgumentos->id="utiliza_sistema_projetos_versao_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUtiliza_sistema_projetos_versao_produto()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_versao_processo_estrutura", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_versao_processo_estrutura", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    public function getProcesso_estrutura_id_INT()
    {
    	return $this->processo_estrutura_id_INT;
    }
    
    function getData_inicio_DATETIME_UNIX()
    {
    	return $this->data_inicio_DATETIME_UNIX;
    }
    
    public function getData_inicio_DATETIME()
    {
    	return $this->data_inicio_DATETIME;
    }
    
    function getData_fim_DATETIME_UNIX()
    {
    	return $this->data_fim_DATETIME_UNIX;
    }
    
    public function getData_fim_DATETIME()
    {
    	return $this->data_fim_DATETIME;
    }
    
    public function getId_atual_projetos_versao_caminho_INT()
    {
    	return $this->id_atual_projetos_versao_caminho_INT;
    }
    
    public function getTotal_ciclo_INT()
    {
    	return $this->total_ciclo_INT;
    }
    
    public function getCiclo_atual_INT()
    {
    	return $this->ciclo_atual_INT;
    }
    
    public function getGet_vetor_id()
    {
    	return $this->get_vetor_id;
    }
    
    public function getTotal_tempo_gasto_seg_INT()
    {
    	return $this->total_tempo_gasto_seg_INT;
    }
    
    public function getCopia_projetos_versao_banco_banco_id_INT()
    {
    	return $this->copia_projetos_versao_banco_banco_id_INT;
    }
    
    public function getUtiliza_sistema_projetos_versao_produto_id_INT()
    {
    	return $this->utiliza_sistema_projetos_versao_produto_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setProcesso_estrutura_id_INT($val)
    {
    	$this->processo_estrutura_id_INT =  $val;
    }
    
    function setData_inicio_DATETIME($val)
    {
    	$this->data_inicio_DATETIME =  $val;
    }
    
    function setData_fim_DATETIME($val)
    {
    	$this->data_fim_DATETIME =  $val;
    }
    
    function setId_atual_projetos_versao_caminho_INT($val)
    {
    	$this->id_atual_projetos_versao_caminho_INT =  $val;
    }
    
    function setTotal_ciclo_INT($val)
    {
    	$this->total_ciclo_INT =  $val;
    }
    
    function setCiclo_atual_INT($val)
    {
    	$this->ciclo_atual_INT =  $val;
    }
    
    function setGet_vetor_id($val)
    {
    	$this->get_vetor_id =  $val;
    }
    
    function setTotal_tempo_gasto_seg_INT($val)
    {
    	$this->total_tempo_gasto_seg_INT =  $val;
    }
    
    function setCopia_projetos_versao_banco_banco_id_INT($val)
    {
    	$this->copia_projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setUtiliza_sistema_projetos_versao_produto_id_INT($val)
    {
    	$this->utiliza_sistema_projetos_versao_produto_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_inicio_DATETIME) AS data_inicio_DATETIME_UNIX, UNIX_TIMESTAMP(data_fim_DATETIME) AS data_fim_DATETIME_UNIX FROM projetos_versao_processo_estrutura WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

        $this->processo_estrutura_id_INT = $row->processo_estrutura_id_INT;
        if(isset($this->objProcesso_estrutura))
			$this->objProcesso_estrutura->select($this->processo_estrutura_id_INT);

        $this->data_inicio_DATETIME = $row->data_inicio_DATETIME;
        $this->data_inicio_DATETIME_UNIX = $row->data_inicio_DATETIME_UNIX;

        $this->data_fim_DATETIME = $row->data_fim_DATETIME;
        $this->data_fim_DATETIME_UNIX = $row->data_fim_DATETIME_UNIX;

        $this->id_atual_projetos_versao_caminho_INT = $row->id_atual_projetos_versao_caminho_INT;
        
        $this->total_ciclo_INT = $row->total_ciclo_INT;
        
        $this->ciclo_atual_INT = $row->ciclo_atual_INT;
        
        $this->get_vetor_id = $row->get_vetor_id;
        
        $this->total_tempo_gasto_seg_INT = $row->total_tempo_gasto_seg_INT;
        
        $this->copia_projetos_versao_banco_banco_id_INT = $row->copia_projetos_versao_banco_banco_id_INT;
        if(isset($this->objCopia_projetos_versao_banco_banco))
			$this->objCopia_projetos_versao_banco_banco->select($this->copia_projetos_versao_banco_banco_id_INT);

        $this->utiliza_sistema_projetos_versao_produto_id_INT = $row->utiliza_sistema_projetos_versao_produto_id_INT;
        if(isset($this->objUtiliza_sistema_projetos_versao_produto))
			$this->objUtiliza_sistema_projetos_versao_produto->select($this->utiliza_sistema_projetos_versao_produto_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_versao_processo_estrutura WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_versao_processo_estrutura ( projetos_versao_banco_banco_id_INT , processo_estrutura_id_INT , data_inicio_DATETIME , data_fim_DATETIME , id_atual_projetos_versao_caminho_INT , total_ciclo_INT , ciclo_atual_INT , get_vetor_id , total_tempo_gasto_seg_INT , copia_projetos_versao_banco_banco_id_INT , utiliza_sistema_projetos_versao_produto_id_INT ) VALUES ( {$this->projetos_versao_banco_banco_id_INT} , {$this->processo_estrutura_id_INT} , {$this->data_inicio_DATETIME} , {$this->data_fim_DATETIME} , {$this->id_atual_projetos_versao_caminho_INT} , {$this->total_ciclo_INT} , {$this->ciclo_atual_INT} , {$this->get_vetor_id} , {$this->total_tempo_gasto_seg_INT} , {$this->copia_projetos_versao_banco_banco_id_INT} , {$this->utiliza_sistema_projetos_versao_produto_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoProcesso_estrutura_id_INT(){ 

		return "processo_estrutura_id_INT";

	}

	public function nomeCampoData_inicio_DATETIME(){ 

		return "data_inicio_DATETIME";

	}

	public function nomeCampoData_fim_DATETIME(){ 

		return "data_fim_DATETIME";

	}

	public function nomeCampoId_atual_projetos_versao_caminho_INT(){ 

		return "id_atual_projetos_versao_caminho_INT";

	}

	public function nomeCampoTotal_ciclo_INT(){ 

		return "total_ciclo_INT";

	}

	public function nomeCampoCiclo_atual_INT(){ 

		return "ciclo_atual_INT";

	}

	public function nomeCampoGet_vetor_id(){ 

		return "get_vetor_id";

	}

	public function nomeCampoTotal_tempo_gasto_seg_INT(){ 

		return "total_tempo_gasto_seg_INT";

	}

	public function nomeCampoCopia_projetos_versao_banco_banco_id_INT(){ 

		return "copia_projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoUtiliza_sistema_projetos_versao_produto_id_INT(){ 

		return "utiliza_sistema_projetos_versao_produto_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProcesso_estrutura_id_INT($objArguments){

		$objArguments->nome = "processo_estrutura_id_INT";
		$objArguments->id = "processo_estrutura_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_inicio_DATETIME($objArguments){

		$objArguments->nome = "data_inicio_DATETIME";
		$objArguments->id = "data_inicio_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_fim_DATETIME($objArguments){

		$objArguments->nome = "data_fim_DATETIME";
		$objArguments->id = "data_fim_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoId_atual_projetos_versao_caminho_INT($objArguments){

		$objArguments->nome = "id_atual_projetos_versao_caminho_INT";
		$objArguments->id = "id_atual_projetos_versao_caminho_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTotal_ciclo_INT($objArguments){

		$objArguments->nome = "total_ciclo_INT";
		$objArguments->id = "total_ciclo_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCiclo_atual_INT($objArguments){

		$objArguments->nome = "ciclo_atual_INT";
		$objArguments->id = "ciclo_atual_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoGet_vetor_id($objArguments){

		$objArguments->nome = "get_vetor_id";
		$objArguments->id = "get_vetor_id";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTotal_tempo_gasto_seg_INT($objArguments){

		$objArguments->nome = "total_tempo_gasto_seg_INT";
		$objArguments->id = "total_tempo_gasto_seg_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCopia_projetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "copia_projetos_versao_banco_banco_id_INT";
		$objArguments->id = "copia_projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUtiliza_sistema_projetos_versao_produto_id_INT($objArguments){

		$objArguments->nome = "utiliza_sistema_projetos_versao_produto_id_INT";
		$objArguments->id = "utiliza_sistema_projetos_versao_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->processo_estrutura_id_INT == ""){

			$this->processo_estrutura_id_INT = "null";

		}

		if($this->id_atual_projetos_versao_caminho_INT == ""){

			$this->id_atual_projetos_versao_caminho_INT = "null";

		}

		if($this->total_ciclo_INT == ""){

			$this->total_ciclo_INT = "null";

		}

		if($this->ciclo_atual_INT == ""){

			$this->ciclo_atual_INT = "null";

		}

			$this->get_vetor_id = $this->formatarDadosParaSQL($this->get_vetor_id);
		if($this->total_tempo_gasto_seg_INT == ""){

			$this->total_tempo_gasto_seg_INT = "null";

		}

		if($this->copia_projetos_versao_banco_banco_id_INT == ""){

			$this->copia_projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->utiliza_sistema_projetos_versao_produto_id_INT == ""){

			$this->utiliza_sistema_projetos_versao_produto_id_INT = "null";

		}



	$this->data_inicio_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicio_DATETIME); 
	$this->data_fim_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_fim_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_inicio_DATETIME = $this->formatarDataTimeParaExibicao($this->data_inicio_DATETIME); 
	$this->data_fim_DATETIME = $this->formatarDataTimeParaExibicao($this->data_fim_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;
		$this->processo_estrutura_id_INT = null; 
		$this->objProcesso_estrutura= null;
		$this->data_inicio_DATETIME = null; 
		$this->data_fim_DATETIME = null; 
		$this->id_atual_projetos_versao_caminho_INT = null; 
		$this->total_ciclo_INT = null; 
		$this->ciclo_atual_INT = null; 
		$this->get_vetor_id = null; 
		$this->total_tempo_gasto_seg_INT = null; 
		$this->copia_projetos_versao_banco_banco_id_INT = null; 
		$this->objCopia_projetos_versao_banco_banco= null;
		$this->utiliza_sistema_projetos_versao_produto_id_INT = null; 
		$this->objUtiliza_sistema_projetos_versao_produto= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 
		Helper::setSession("processo_estrutura_id_INT", $this->processo_estrutura_id_INT); 
		Helper::setSession("data_inicio_DATETIME", $this->data_inicio_DATETIME); 
		Helper::setSession("data_fim_DATETIME", $this->data_fim_DATETIME); 
		Helper::setSession("id_atual_projetos_versao_caminho_INT", $this->id_atual_projetos_versao_caminho_INT); 
		Helper::setSession("total_ciclo_INT", $this->total_ciclo_INT); 
		Helper::setSession("ciclo_atual_INT", $this->ciclo_atual_INT); 
		Helper::setSession("get_vetor_id", $this->get_vetor_id); 
		Helper::setSession("total_tempo_gasto_seg_INT", $this->total_tempo_gasto_seg_INT); 
		Helper::setSession("copia_projetos_versao_banco_banco_id_INT", $this->copia_projetos_versao_banco_banco_id_INT); 
		Helper::setSession("utiliza_sistema_projetos_versao_produto_id_INT", $this->utiliza_sistema_projetos_versao_produto_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");
		Helper::clearSession("processo_estrutura_id_INT");
		Helper::clearSession("data_inicio_DATETIME");
		Helper::clearSession("data_fim_DATETIME");
		Helper::clearSession("id_atual_projetos_versao_caminho_INT");
		Helper::clearSession("total_ciclo_INT");
		Helper::clearSession("ciclo_atual_INT");
		Helper::clearSession("get_vetor_id");
		Helper::clearSession("total_tempo_gasto_seg_INT");
		Helper::clearSession("copia_projetos_versao_banco_banco_id_INT");
		Helper::clearSession("utiliza_sistema_projetos_versao_produto_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->processo_estrutura_id_INT = Helper::SESSION("processo_estrutura_id_INT{$numReg}"); 
		$this->data_inicio_DATETIME = Helper::SESSION("data_inicio_DATETIME{$numReg}"); 
		$this->data_fim_DATETIME = Helper::SESSION("data_fim_DATETIME{$numReg}"); 
		$this->id_atual_projetos_versao_caminho_INT = Helper::SESSION("id_atual_projetos_versao_caminho_INT{$numReg}"); 
		$this->total_ciclo_INT = Helper::SESSION("total_ciclo_INT{$numReg}"); 
		$this->ciclo_atual_INT = Helper::SESSION("ciclo_atual_INT{$numReg}"); 
		$this->get_vetor_id = Helper::SESSION("get_vetor_id{$numReg}"); 
		$this->total_tempo_gasto_seg_INT = Helper::SESSION("total_tempo_gasto_seg_INT{$numReg}"); 
		$this->copia_projetos_versao_banco_banco_id_INT = Helper::SESSION("copia_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->utiliza_sistema_projetos_versao_produto_id_INT = Helper::SESSION("utiliza_sistema_projetos_versao_produto_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->processo_estrutura_id_INT = Helper::POST("processo_estrutura_id_INT{$numReg}"); 
		$this->data_inicio_DATETIME = Helper::POST("data_inicio_DATETIME{$numReg}"); 
		$this->data_fim_DATETIME = Helper::POST("data_fim_DATETIME{$numReg}"); 
		$this->id_atual_projetos_versao_caminho_INT = Helper::POST("id_atual_projetos_versao_caminho_INT{$numReg}"); 
		$this->total_ciclo_INT = Helper::POST("total_ciclo_INT{$numReg}"); 
		$this->ciclo_atual_INT = Helper::POST("ciclo_atual_INT{$numReg}"); 
		$this->get_vetor_id = Helper::POST("get_vetor_id{$numReg}"); 
		$this->total_tempo_gasto_seg_INT = Helper::POST("total_tempo_gasto_seg_INT{$numReg}"); 
		$this->copia_projetos_versao_banco_banco_id_INT = Helper::POST("copia_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->utiliza_sistema_projetos_versao_produto_id_INT = Helper::POST("utiliza_sistema_projetos_versao_produto_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->processo_estrutura_id_INT = Helper::GET("processo_estrutura_id_INT{$numReg}"); 
		$this->data_inicio_DATETIME = Helper::GET("data_inicio_DATETIME{$numReg}"); 
		$this->data_fim_DATETIME = Helper::GET("data_fim_DATETIME{$numReg}"); 
		$this->id_atual_projetos_versao_caminho_INT = Helper::GET("id_atual_projetos_versao_caminho_INT{$numReg}"); 
		$this->total_ciclo_INT = Helper::GET("total_ciclo_INT{$numReg}"); 
		$this->ciclo_atual_INT = Helper::GET("ciclo_atual_INT{$numReg}"); 
		$this->get_vetor_id = Helper::GET("get_vetor_id{$numReg}"); 
		$this->total_tempo_gasto_seg_INT = Helper::GET("total_tempo_gasto_seg_INT{$numReg}"); 
		$this->copia_projetos_versao_banco_banco_id_INT = Helper::GET("copia_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->utiliza_sistema_projetos_versao_produto_id_INT = Helper::GET("utiliza_sistema_projetos_versao_produto_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["processo_estrutura_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "processo_estrutura_id_INT = $this->processo_estrutura_id_INT, ";

	} 

	if(isset($tipo["data_inicio_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_inicio_DATETIME = $this->data_inicio_DATETIME, ";

	} 

	if(isset($tipo["data_fim_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_fim_DATETIME = $this->data_fim_DATETIME, ";

	} 

	if(isset($tipo["id_atual_projetos_versao_caminho_INT{$numReg}"]) || $tipo == null){

		$upd.= "id_atual_projetos_versao_caminho_INT = $this->id_atual_projetos_versao_caminho_INT, ";

	} 

	if(isset($tipo["total_ciclo_INT{$numReg}"]) || $tipo == null){

		$upd.= "total_ciclo_INT = $this->total_ciclo_INT, ";

	} 

	if(isset($tipo["ciclo_atual_INT{$numReg}"]) || $tipo == null){

		$upd.= "ciclo_atual_INT = $this->ciclo_atual_INT, ";

	} 

	if(isset($tipo["get_vetor_id{$numReg}"]) || $tipo == null){

		$upd.= "get_vetor_id = $this->get_vetor_id, ";

	} 

	if(isset($tipo["total_tempo_gasto_seg_INT{$numReg}"]) || $tipo == null){

		$upd.= "total_tempo_gasto_seg_INT = $this->total_tempo_gasto_seg_INT, ";

	} 

	if(isset($tipo["copia_projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "copia_projetos_versao_banco_banco_id_INT = $this->copia_projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["utiliza_sistema_projetos_versao_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "utiliza_sistema_projetos_versao_produto_id_INT = $this->utiliza_sistema_projetos_versao_produto_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_versao_processo_estrutura SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    