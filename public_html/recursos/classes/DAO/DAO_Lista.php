<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Lista
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Lista.php
    * TABELA MYSQL:    lista
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Lista extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $descricao;
	public $tabela_id_INT;
	public $obj;
	public $ultima_alteracao_usuario_id_INT;
	public $objTabela;
	public $ultima_alteracao_DATETIME;
	public $criacao_DATETIME;
	public $criador_usuario_id_INT;
	public $objUltima_alteracao_usuario;
	public $projetos_versao_id_INT;
	public $objCriador_usuario;


    public $nomeEntidade;

	public $ultima_alteracao_DATETIME_UNIX;
	public $criacao_DATETIME_UNIX;


    

	public $label_id;
	public $label_nome;
	public $label_descricao;
	public $label_tabela_id_INT;
	public $label_ultima_alteracao_usuario_id_INT;
	public $label_ultima_alteracao_DATETIME;
	public $label_criacao_DATETIME;
	public $label_criador_usuario_id_INT;
	public $label_projetos_versao_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "lista";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTabela(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tabela($this->getDatabase());
		if($this->tabela_id_INT != null) 
		$this->obj->select($this->tabela_id_INT);
	}
	return $this->obj ;
}
function getFkObjUltima_alteracao_usuario(){
	if($this->objTabela ==null){
		$this->objTabela = new EXTDAO_Usuario($this->getDatabase());
		if($this->ultima_alteracao_usuario_id_INT != null) 
		$this->objTabela->select($this->ultima_alteracao_usuario_id_INT);
	}
	return $this->objTabela ;
}
function getFkObjCriador_usuario(){
	if($this->objUltima_alteracao_usuario ==null){
		$this->objUltima_alteracao_usuario = new EXTDAO_Usuario($this->getDatabase());
		if($this->criador_usuario_id_INT != null) 
		$this->objUltima_alteracao_usuario->select($this->criador_usuario_id_INT);
	}
	return $this->objUltima_alteracao_usuario ;
}
function getFkObjProjetos_versao(){
	if($this->objCriador_usuario ==null){
		$this->objCriador_usuario = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objCriador_usuario->select($this->projetos_versao_id_INT);
	}
	return $this->objCriador_usuario ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTabela($objArgumentos){

		$objArgumentos->nome="tabela_id_INT";
		$objArgumentos->id="tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllUltima_alteracao_usuario($objArgumentos){

		$objArgumentos->nome="ultima_alteracao_usuario_id_INT";
		$objArgumentos->id="ultima_alteracao_usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUltima_alteracao_usuario()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCriador_usuario($objArgumentos){

		$objArgumentos->nome="criador_usuario_id_INT";
		$objArgumentos->id="criador_usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCriador_usuario()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_lista", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_lista", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getDescricao()
    {
    	return $this->descricao;
    }
    
    public function getTabela_id_INT()
    {
    	return $this->tabela_id_INT;
    }
    
    public function getUltima_alteracao_usuario_id_INT()
    {
    	return $this->ultima_alteracao_usuario_id_INT;
    }
    
    function getUltima_alteracao_DATETIME_UNIX()
    {
    	return $this->ultima_alteracao_DATETIME_UNIX;
    }
    
    public function getUltima_alteracao_DATETIME()
    {
    	return $this->ultima_alteracao_DATETIME;
    }
    
    function getCriacao_DATETIME_UNIX()
    {
    	return $this->criacao_DATETIME_UNIX;
    }
    
    public function getCriacao_DATETIME()
    {
    	return $this->criacao_DATETIME;
    }
    
    public function getCriador_usuario_id_INT()
    {
    	return $this->criador_usuario_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setDescricao($val)
    {
    	$this->descricao =  $val;
    }
    
    function setTabela_id_INT($val)
    {
    	$this->tabela_id_INT =  $val;
    }
    
    function setUltima_alteracao_usuario_id_INT($val)
    {
    	$this->ultima_alteracao_usuario_id_INT =  $val;
    }
    
    function setUltima_alteracao_DATETIME($val)
    {
    	$this->ultima_alteracao_DATETIME =  $val;
    }
    
    function setCriacao_DATETIME($val)
    {
    	$this->criacao_DATETIME =  $val;
    }
    
    function setCriador_usuario_id_INT($val)
    {
    	$this->criador_usuario_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(ultima_alteracao_DATETIME) AS ultima_alteracao_DATETIME_UNIX, UNIX_TIMESTAMP(criacao_DATETIME) AS criacao_DATETIME_UNIX FROM lista WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->descricao = $row->descricao;
        
        $this->tabela_id_INT = $row->tabela_id_INT;
        if(isset($this->objTabela))
			$this->objTabela->select($this->tabela_id_INT);

        $this->ultima_alteracao_usuario_id_INT = $row->ultima_alteracao_usuario_id_INT;
        if(isset($this->objUltima_alteracao_usuario))
			$this->objUltima_alteracao_usuario->select($this->ultima_alteracao_usuario_id_INT);

        $this->ultima_alteracao_DATETIME = $row->ultima_alteracao_DATETIME;
        $this->ultima_alteracao_DATETIME_UNIX = $row->ultima_alteracao_DATETIME_UNIX;

        $this->criacao_DATETIME = $row->criacao_DATETIME;
        $this->criacao_DATETIME_UNIX = $row->criacao_DATETIME_UNIX;

        $this->criador_usuario_id_INT = $row->criador_usuario_id_INT;
        if(isset($this->objCriador_usuario))
			$this->objCriador_usuario->select($this->criador_usuario_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM lista WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO lista ( nome , descricao , tabela_id_INT , ultima_alteracao_usuario_id_INT , ultima_alteracao_DATETIME , criacao_DATETIME , criador_usuario_id_INT , projetos_versao_id_INT ) VALUES ( {$this->nome} , {$this->descricao} , {$this->tabela_id_INT} , {$this->ultima_alteracao_usuario_id_INT} , {$this->ultima_alteracao_DATETIME} , {$this->criacao_DATETIME} , {$this->criador_usuario_id_INT} , {$this->projetos_versao_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoDescricao(){ 

		return "descricao";

	}

	public function nomeCampoTabela_id_INT(){ 

		return "tabela_id_INT";

	}

	public function nomeCampoUltima_alteracao_usuario_id_INT(){ 

		return "ultima_alteracao_usuario_id_INT";

	}

	public function nomeCampoUltima_alteracao_DATETIME(){ 

		return "ultima_alteracao_DATETIME";

	}

	public function nomeCampoCriacao_DATETIME(){ 

		return "criacao_DATETIME";

	}

	public function nomeCampoCriador_usuario_id_INT(){ 

		return "criador_usuario_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDescricao($objArguments){

		$objArguments->nome = "descricao";
		$objArguments->id = "descricao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTabela_id_INT($objArguments){

		$objArguments->nome = "tabela_id_INT";
		$objArguments->id = "tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUltima_alteracao_usuario_id_INT($objArguments){

		$objArguments->nome = "ultima_alteracao_usuario_id_INT";
		$objArguments->id = "ultima_alteracao_usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUltima_alteracao_DATETIME($objArguments){

		$objArguments->nome = "ultima_alteracao_DATETIME";
		$objArguments->id = "ultima_alteracao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoCriacao_DATETIME($objArguments){

		$objArguments->nome = "criacao_DATETIME";
		$objArguments->id = "criacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoCriador_usuario_id_INT($objArguments){

		$objArguments->nome = "criador_usuario_id_INT";
		$objArguments->id = "criador_usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->descricao = $this->formatarDadosParaSQL($this->descricao);
		if($this->tabela_id_INT == ""){

			$this->tabela_id_INT = "null";

		}

		if($this->ultima_alteracao_usuario_id_INT == ""){

			$this->ultima_alteracao_usuario_id_INT = "null";

		}

		if($this->criador_usuario_id_INT == ""){

			$this->criador_usuario_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}



	$this->ultima_alteracao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->ultima_alteracao_DATETIME); 
	$this->criacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->criacao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->ultima_alteracao_DATETIME = $this->formatarDataTimeParaExibicao($this->ultima_alteracao_DATETIME); 
	$this->criacao_DATETIME = $this->formatarDataTimeParaExibicao($this->criacao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->descricao = null; 
		$this->tabela_id_INT = null; 
		$this->objTabela= null;
		$this->ultima_alteracao_usuario_id_INT = null; 
		$this->objUltima_alteracao_usuario= null;
		$this->ultima_alteracao_DATETIME = null; 
		$this->criacao_DATETIME = null; 
		$this->criador_usuario_id_INT = null; 
		$this->objCriador_usuario= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("descricao", $this->descricao); 
		Helper::setSession("tabela_id_INT", $this->tabela_id_INT); 
		Helper::setSession("ultima_alteracao_usuario_id_INT", $this->ultima_alteracao_usuario_id_INT); 
		Helper::setSession("ultima_alteracao_DATETIME", $this->ultima_alteracao_DATETIME); 
		Helper::setSession("criacao_DATETIME", $this->criacao_DATETIME); 
		Helper::setSession("criador_usuario_id_INT", $this->criador_usuario_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("descricao");
		Helper::clearSession("tabela_id_INT");
		Helper::clearSession("ultima_alteracao_usuario_id_INT");
		Helper::clearSession("ultima_alteracao_DATETIME");
		Helper::clearSession("criacao_DATETIME");
		Helper::clearSession("criador_usuario_id_INT");
		Helper::clearSession("projetos_versao_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->descricao = Helper::SESSION("descricao{$numReg}"); 
		$this->tabela_id_INT = Helper::SESSION("tabela_id_INT{$numReg}"); 
		$this->ultima_alteracao_usuario_id_INT = Helper::SESSION("ultima_alteracao_usuario_id_INT{$numReg}"); 
		$this->ultima_alteracao_DATETIME = Helper::SESSION("ultima_alteracao_DATETIME{$numReg}"); 
		$this->criacao_DATETIME = Helper::SESSION("criacao_DATETIME{$numReg}"); 
		$this->criador_usuario_id_INT = Helper::SESSION("criador_usuario_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->descricao = Helper::POST("descricao{$numReg}"); 
		$this->tabela_id_INT = Helper::POST("tabela_id_INT{$numReg}"); 
		$this->ultima_alteracao_usuario_id_INT = Helper::POST("ultima_alteracao_usuario_id_INT{$numReg}"); 
		$this->ultima_alteracao_DATETIME = Helper::POST("ultima_alteracao_DATETIME{$numReg}"); 
		$this->criacao_DATETIME = Helper::POST("criacao_DATETIME{$numReg}"); 
		$this->criador_usuario_id_INT = Helper::POST("criador_usuario_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->descricao = Helper::GET("descricao{$numReg}"); 
		$this->tabela_id_INT = Helper::GET("tabela_id_INT{$numReg}"); 
		$this->ultima_alteracao_usuario_id_INT = Helper::GET("ultima_alteracao_usuario_id_INT{$numReg}"); 
		$this->ultima_alteracao_DATETIME = Helper::GET("ultima_alteracao_DATETIME{$numReg}"); 
		$this->criacao_DATETIME = Helper::GET("criacao_DATETIME{$numReg}"); 
		$this->criador_usuario_id_INT = Helper::GET("criador_usuario_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["descricao{$numReg}"]) || $tipo == null){

		$upd.= "descricao = $this->descricao, ";

	} 

	if(isset($tipo["tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_id_INT = $this->tabela_id_INT, ";

	} 

	if(isset($tipo["ultima_alteracao_usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "ultima_alteracao_usuario_id_INT = $this->ultima_alteracao_usuario_id_INT, ";

	} 

	if(isset($tipo["ultima_alteracao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "ultima_alteracao_DATETIME = $this->ultima_alteracao_DATETIME, ";

	} 

	if(isset($tipo["criacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "criacao_DATETIME = $this->criacao_DATETIME, ";

	} 

	if(isset($tipo["criador_usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "criador_usuario_id_INT = $this->criador_usuario_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE lista SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    