<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tabela
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Tabela.php
    * TABELA MYSQL:    tabela
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Tabela extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $nome_exibicao;
	public $banco_id_INT;
	public $obj;
	public $projetos_versao_id_INT;
	public $objBanco;
	public $frequencia_sincronizador_INT;
	public $transmissao_web_para_mobile_BOOLEAN;
	public $transmissao_mobile_para_web_BOOLEAN;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_nome_exibicao;
	public $label_banco_id_INT;
	public $label_projetos_versao_id_INT;
	public $label_frequencia_sincronizador_INT;
	public $label_transmissao_web_para_mobile_BOOLEAN;
	public $label_transmissao_mobile_para_web_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "tabela";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjBanco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Banco($this->getDatabase());
		if($this->banco_id_INT != null) 
		$this->obj->select($this->banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjProjetos_versao(){
	if($this->objBanco ==null){
		$this->objBanco = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->objBanco->select($this->projetos_versao_id_INT);
	}
	return $this->objBanco ;
}


    public function valorCampoLabel(){

    	return $this->getNome();

    }

    

        public function getComboBoxAllBanco($objArgumentos){

		$objArgumentos->nome="banco_id_INT";
		$objArgumentos->id="banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjBanco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_tabela", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tabela", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getNome_exibicao()
    {
    	return $this->nome_exibicao;
    }
    
    public function getBanco_id_INT()
    {
    	return $this->banco_id_INT;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    public function getFrequencia_sincronizador_INT()
    {
    	return $this->frequencia_sincronizador_INT;
    }
    
    public function getTransmissao_web_para_mobile_BOOLEAN()
    {
    	return $this->transmissao_web_para_mobile_BOOLEAN;
    }
    
    public function getTransmissao_mobile_para_web_BOOLEAN()
    {
    	return $this->transmissao_mobile_para_web_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setNome_exibicao($val)
    {
    	$this->nome_exibicao =  $val;
    }
    
    function setBanco_id_INT($val)
    {
    	$this->banco_id_INT =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    
    function setFrequencia_sincronizador_INT($val)
    {
    	$this->frequencia_sincronizador_INT =  $val;
    }
    
    function setTransmissao_web_para_mobile_BOOLEAN($val)
    {
    	$this->transmissao_web_para_mobile_BOOLEAN =  $val;
    }
    
    function setTransmissao_mobile_para_web_BOOLEAN($val)
    {
    	$this->transmissao_mobile_para_web_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM tabela WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->nome_exibicao = $row->nome_exibicao;
        
        $this->banco_id_INT = $row->banco_id_INT;
        if(isset($this->objBanco))
			$this->objBanco->select($this->banco_id_INT);

        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

        $this->frequencia_sincronizador_INT = $row->frequencia_sincronizador_INT;
        
        $this->transmissao_web_para_mobile_BOOLEAN = $row->transmissao_web_para_mobile_BOOLEAN;
        
        $this->transmissao_mobile_para_web_BOOLEAN = $row->transmissao_mobile_para_web_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM tabela WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO tabela ( nome , nome_exibicao , banco_id_INT , projetos_versao_id_INT , frequencia_sincronizador_INT , transmissao_web_para_mobile_BOOLEAN , transmissao_mobile_para_web_BOOLEAN ) VALUES ( {$this->nome} , {$this->nome_exibicao} , {$this->banco_id_INT} , {$this->projetos_versao_id_INT} , {$this->frequencia_sincronizador_INT} , {$this->transmissao_web_para_mobile_BOOLEAN} , {$this->transmissao_mobile_para_web_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoNome_exibicao(){ 

		return "nome_exibicao";

	}

	public function nomeCampoBanco_id_INT(){ 

		return "banco_id_INT";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}

	public function nomeCampoFrequencia_sincronizador_INT(){ 

		return "frequencia_sincronizador_INT";

	}

	public function nomeCampoTransmissao_web_para_mobile_BOOLEAN(){ 

		return "transmissao_web_para_mobile_BOOLEAN";

	}

	public function nomeCampoTransmissao_mobile_para_web_BOOLEAN(){ 

		return "transmissao_mobile_para_web_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoNome_exibicao($objArguments){

		$objArguments->nome = "nome_exibicao";
		$objArguments->id = "nome_exibicao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoBanco_id_INT($objArguments){

		$objArguments->nome = "banco_id_INT";
		$objArguments->id = "banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoFrequencia_sincronizador_INT($objArguments){

		$objArguments->nome = "frequencia_sincronizador_INT";
		$objArguments->id = "frequencia_sincronizador_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTransmissao_web_para_mobile_BOOLEAN($objArguments){

		$objArguments->nome = "transmissao_web_para_mobile_BOOLEAN";
		$objArguments->id = "transmissao_web_para_mobile_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoTransmissao_mobile_para_web_BOOLEAN($objArguments){

		$objArguments->nome = "transmissao_mobile_para_web_BOOLEAN";
		$objArguments->id = "transmissao_mobile_para_web_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->nome_exibicao = $this->formatarDadosParaSQL($this->nome_exibicao);
		if($this->banco_id_INT == ""){

			$this->banco_id_INT = "null";

		}

		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}

		if($this->frequencia_sincronizador_INT == ""){

			$this->frequencia_sincronizador_INT = "null";

		}

		if($this->transmissao_web_para_mobile_BOOLEAN == ""){

			$this->transmissao_web_para_mobile_BOOLEAN = "null";

		}

		if($this->transmissao_mobile_para_web_BOOLEAN == ""){

			$this->transmissao_mobile_para_web_BOOLEAN = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->nome_exibicao = null; 
		$this->banco_id_INT = null; 
		$this->objBanco= null;
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;
		$this->frequencia_sincronizador_INT = null; 
		$this->transmissao_web_para_mobile_BOOLEAN = null; 
		$this->transmissao_mobile_para_web_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("nome_exibicao", $this->nome_exibicao); 
		Helper::setSession("banco_id_INT", $this->banco_id_INT); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 
		Helper::setSession("frequencia_sincronizador_INT", $this->frequencia_sincronizador_INT); 
		Helper::setSession("transmissao_web_para_mobile_BOOLEAN", $this->transmissao_web_para_mobile_BOOLEAN); 
		Helper::setSession("transmissao_mobile_para_web_BOOLEAN", $this->transmissao_mobile_para_web_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("nome_exibicao");
		Helper::clearSession("banco_id_INT");
		Helper::clearSession("projetos_versao_id_INT");
		Helper::clearSession("frequencia_sincronizador_INT");
		Helper::clearSession("transmissao_web_para_mobile_BOOLEAN");
		Helper::clearSession("transmissao_mobile_para_web_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->nome_exibicao = Helper::SESSION("nome_exibicao{$numReg}"); 
		$this->banco_id_INT = Helper::SESSION("banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 
		$this->frequencia_sincronizador_INT = Helper::SESSION("frequencia_sincronizador_INT{$numReg}"); 
		$this->transmissao_web_para_mobile_BOOLEAN = Helper::SESSION("transmissao_web_para_mobile_BOOLEAN{$numReg}"); 
		$this->transmissao_mobile_para_web_BOOLEAN = Helper::SESSION("transmissao_mobile_para_web_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->nome_exibicao = Helper::POST("nome_exibicao{$numReg}"); 
		$this->banco_id_INT = Helper::POST("banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 
		$this->frequencia_sincronizador_INT = Helper::POST("frequencia_sincronizador_INT{$numReg}"); 
		$this->transmissao_web_para_mobile_BOOLEAN = Helper::POST("transmissao_web_para_mobile_BOOLEAN{$numReg}"); 
		$this->transmissao_mobile_para_web_BOOLEAN = Helper::POST("transmissao_mobile_para_web_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->nome_exibicao = Helper::GET("nome_exibicao{$numReg}"); 
		$this->banco_id_INT = Helper::GET("banco_id_INT{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 
		$this->frequencia_sincronizador_INT = Helper::GET("frequencia_sincronizador_INT{$numReg}"); 
		$this->transmissao_web_para_mobile_BOOLEAN = Helper::GET("transmissao_web_para_mobile_BOOLEAN{$numReg}"); 
		$this->transmissao_mobile_para_web_BOOLEAN = Helper::GET("transmissao_mobile_para_web_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["nome_exibicao{$numReg}"]) || $tipo == null){

		$upd.= "nome_exibicao = $this->nome_exibicao, ";

	} 

	if(isset($tipo["banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "banco_id_INT = $this->banco_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

	if(isset($tipo["frequencia_sincronizador_INT{$numReg}"]) || $tipo == null){

		$upd.= "frequencia_sincronizador_INT = $this->frequencia_sincronizador_INT, ";

	} 

	if(isset($tipo["transmissao_web_para_mobile_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "transmissao_web_para_mobile_BOOLEAN = $this->transmissao_web_para_mobile_BOOLEAN, ";

	} 

	if(isset($tipo["transmissao_mobile_para_web_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "transmissao_mobile_para_web_BOOLEAN = $this->transmissao_mobile_para_web_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE tabela SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    