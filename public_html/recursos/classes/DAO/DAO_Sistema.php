<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema.php
    * TABELA MYSQL:    sistema
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $projetos_id_INT;
	public $obj;
	public $manutencao_BOOLEAN;
	public $prototipo_BOOLEAN;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_projetos_id_INT;
	public $label_manutencao_BOOLEAN;
	public $label_prototipo_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos($this->getDatabase());
		if($this->projetos_id_INT != null) 
		$this->obj->select($this->projetos_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos($objArgumentos){

		$objArgumentos->nome="projetos_id_INT";
		$objArgumentos->id="projetos_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getProjetos_id_INT()
    {
    	return $this->projetos_id_INT;
    }
    
    public function getManutencao_BOOLEAN()
    {
    	return $this->manutencao_BOOLEAN;
    }
    
    public function getPrototipo_BOOLEAN()
    {
    	return $this->prototipo_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setProjetos_id_INT($val)
    {
    	$this->projetos_id_INT =  $val;
    }
    
    function setManutencao_BOOLEAN($val)
    {
    	$this->manutencao_BOOLEAN =  $val;
    }
    
    function setPrototipo_BOOLEAN($val)
    {
    	$this->prototipo_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->projetos_id_INT = $row->projetos_id_INT;
        if(isset($this->objProjetos))
			$this->objProjetos->select($this->projetos_id_INT);

        $this->manutencao_BOOLEAN = $row->manutencao_BOOLEAN;
        
        $this->prototipo_BOOLEAN = $row->prototipo_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema ( nome , projetos_id_INT , manutencao_BOOLEAN , prototipo_BOOLEAN ) VALUES ( {$this->nome} , {$this->projetos_id_INT} , {$this->manutencao_BOOLEAN} , {$this->prototipo_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoProjetos_id_INT(){ 

		return "projetos_id_INT";

	}

	public function nomeCampoManutencao_BOOLEAN(){ 

		return "manutencao_BOOLEAN";

	}

	public function nomeCampoPrototipo_BOOLEAN(){ 

		return "prototipo_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoProjetos_id_INT($objArguments){

		$objArguments->nome = "projetos_id_INT";
		$objArguments->id = "projetos_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoManutencao_BOOLEAN($objArguments){

		$objArguments->nome = "manutencao_BOOLEAN";
		$objArguments->id = "manutencao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoPrototipo_BOOLEAN($objArguments){

		$objArguments->nome = "prototipo_BOOLEAN";
		$objArguments->id = "prototipo_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
		if($this->projetos_id_INT == ""){

			$this->projetos_id_INT = "null";

		}

		if($this->manutencao_BOOLEAN == ""){

			$this->manutencao_BOOLEAN = "null";

		}

		if($this->prototipo_BOOLEAN == ""){

			$this->prototipo_BOOLEAN = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->projetos_id_INT = null; 
		$this->objProjetos= null;
		$this->manutencao_BOOLEAN = null; 
		$this->prototipo_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("projetos_id_INT", $this->projetos_id_INT); 
		Helper::setSession("manutencao_BOOLEAN", $this->manutencao_BOOLEAN); 
		Helper::setSession("prototipo_BOOLEAN", $this->prototipo_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("projetos_id_INT");
		Helper::clearSession("manutencao_BOOLEAN");
		Helper::clearSession("prototipo_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->projetos_id_INT = Helper::SESSION("projetos_id_INT{$numReg}"); 
		$this->manutencao_BOOLEAN = Helper::SESSION("manutencao_BOOLEAN{$numReg}"); 
		$this->prototipo_BOOLEAN = Helper::SESSION("prototipo_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->projetos_id_INT = Helper::POST("projetos_id_INT{$numReg}"); 
		$this->manutencao_BOOLEAN = Helper::POST("manutencao_BOOLEAN{$numReg}"); 
		$this->prototipo_BOOLEAN = Helper::POST("prototipo_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->projetos_id_INT = Helper::GET("projetos_id_INT{$numReg}"); 
		$this->manutencao_BOOLEAN = Helper::GET("manutencao_BOOLEAN{$numReg}"); 
		$this->prototipo_BOOLEAN = Helper::GET("prototipo_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["projetos_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_id_INT = $this->projetos_id_INT, ";

	} 

	if(isset($tipo["manutencao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "manutencao_BOOLEAN = $this->manutencao_BOOLEAN, ";

	} 

	if(isset($tipo["prototipo_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "prototipo_BOOLEAN = $this->prototipo_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    