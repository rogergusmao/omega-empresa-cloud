<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tabela_chave_atributo_tabela_chave_atributo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Tabela_chave_atributo_tabela_chave_atributo.php
    * TABELA MYSQL:    tabela_chave_atributo_tabela_chave_atributo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Tabela_chave_atributo_tabela_chave_atributo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $homologacao_tabela_chave_atributo_id_INT;
	public $obj;
	public $producao_tabela_chave_atributo_id_INT;
	public $objHomologacao_tabela_chave_atributo;
	public $tabela_chave_tabela_chave_id_INT;
	public $objProducao_tabela_chave_atributo;
	public $projetos_versao_banco_banco_id_INT;
	public $objTabela_chave_tabela_chave;


    public $nomeEntidade;



    

	public $label_id;
	public $label_homologacao_tabela_chave_atributo_id_INT;
	public $label_producao_tabela_chave_atributo_id_INT;
	public $label_tabela_chave_tabela_chave_id_INT;
	public $label_projetos_versao_banco_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "tabela_chave_atributo_tabela_chave_atributo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjHomologacao_tabela_chave_atributo(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tabela_chave_atributo($this->getDatabase());
		if($this->homologacao_tabela_chave_atributo_id_INT != null) 
		$this->obj->select($this->homologacao_tabela_chave_atributo_id_INT);
	}
	return $this->obj ;
}
function getFkObjProducao_tabela_chave_atributo(){
	if($this->objHomologacao_tabela_chave_atributo ==null){
		$this->objHomologacao_tabela_chave_atributo = new EXTDAO_Tabela_chave_atributo($this->getDatabase());
		if($this->producao_tabela_chave_atributo_id_INT != null) 
		$this->objHomologacao_tabela_chave_atributo->select($this->producao_tabela_chave_atributo_id_INT);
	}
	return $this->objHomologacao_tabela_chave_atributo ;
}
function getFkObjTabela_chave_tabela_chave(){
	if($this->objProducao_tabela_chave_atributo ==null){
		$this->objProducao_tabela_chave_atributo = new EXTDAO_Tabela_chave_tabela_chave($this->getDatabase());
		if($this->tabela_chave_tabela_chave_id_INT != null) 
		$this->objProducao_tabela_chave_atributo->select($this->tabela_chave_tabela_chave_id_INT);
	}
	return $this->objProducao_tabela_chave_atributo ;
}
function getFkObjProjetos_versao_banco_banco(){
	if($this->objTabela_chave_tabela_chave ==null){
		$this->objTabela_chave_tabela_chave = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->objTabela_chave_tabela_chave->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->objTabela_chave_tabela_chave ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllHomologacao_tabela_chave_atributo($objArgumentos){

		$objArgumentos->nome="homologacao_tabela_chave_atributo_id_INT";
		$objArgumentos->id="homologacao_tabela_chave_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjHomologacao_tabela_chave_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProducao_tabela_chave_atributo($objArgumentos){

		$objArgumentos->nome="producao_tabela_chave_atributo_id_INT";
		$objArgumentos->id="producao_tabela_chave_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProducao_tabela_chave_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTabela_chave_tabela_chave($objArgumentos){

		$objArgumentos->nome="tabela_chave_tabela_chave_id_INT";
		$objArgumentos->id="tabela_chave_tabela_chave_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela_chave_tabela_chave()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_tabela_chave_atributo_tabela_chave_atributo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tabela_chave_atributo_tabela_chave_atributo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHomologacao_tabela_chave_atributo_id_INT()
    {
    	return $this->homologacao_tabela_chave_atributo_id_INT;
    }
    
    public function getProducao_tabela_chave_atributo_id_INT()
    {
    	return $this->producao_tabela_chave_atributo_id_INT;
    }
    
    public function getTabela_chave_tabela_chave_id_INT()
    {
    	return $this->tabela_chave_tabela_chave_id_INT;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHomologacao_tabela_chave_atributo_id_INT($val)
    {
    	$this->homologacao_tabela_chave_atributo_id_INT =  $val;
    }
    
    function setProducao_tabela_chave_atributo_id_INT($val)
    {
    	$this->producao_tabela_chave_atributo_id_INT =  $val;
    }
    
    function setTabela_chave_tabela_chave_id_INT($val)
    {
    	$this->tabela_chave_tabela_chave_id_INT =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM tabela_chave_atributo_tabela_chave_atributo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->homologacao_tabela_chave_atributo_id_INT = $row->homologacao_tabela_chave_atributo_id_INT;
        if(isset($this->objHomologacao_tabela_chave_atributo))
			$this->objHomologacao_tabela_chave_atributo->select($this->homologacao_tabela_chave_atributo_id_INT);

        $this->producao_tabela_chave_atributo_id_INT = $row->producao_tabela_chave_atributo_id_INT;
        if(isset($this->objProducao_tabela_chave_atributo))
			$this->objProducao_tabela_chave_atributo->select($this->producao_tabela_chave_atributo_id_INT);

        $this->tabela_chave_tabela_chave_id_INT = $row->tabela_chave_tabela_chave_id_INT;
        if(isset($this->objTabela_chave_tabela_chave))
			$this->objTabela_chave_tabela_chave->select($this->tabela_chave_tabela_chave_id_INT);

        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM tabela_chave_atributo_tabela_chave_atributo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO tabela_chave_atributo_tabela_chave_atributo ( homologacao_tabela_chave_atributo_id_INT , producao_tabela_chave_atributo_id_INT , tabela_chave_tabela_chave_id_INT , projetos_versao_banco_banco_id_INT ) VALUES ( {$this->homologacao_tabela_chave_atributo_id_INT} , {$this->producao_tabela_chave_atributo_id_INT} , {$this->tabela_chave_tabela_chave_id_INT} , {$this->projetos_versao_banco_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHomologacao_tabela_chave_atributo_id_INT(){ 

		return "homologacao_tabela_chave_atributo_id_INT";

	}

	public function nomeCampoProducao_tabela_chave_atributo_id_INT(){ 

		return "producao_tabela_chave_atributo_id_INT";

	}

	public function nomeCampoTabela_chave_tabela_chave_id_INT(){ 

		return "tabela_chave_tabela_chave_id_INT";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHomologacao_tabela_chave_atributo_id_INT($objArguments){

		$objArguments->nome = "homologacao_tabela_chave_atributo_id_INT";
		$objArguments->id = "homologacao_tabela_chave_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProducao_tabela_chave_atributo_id_INT($objArguments){

		$objArguments->nome = "producao_tabela_chave_atributo_id_INT";
		$objArguments->id = "producao_tabela_chave_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTabela_chave_tabela_chave_id_INT($objArguments){

		$objArguments->nome = "tabela_chave_tabela_chave_id_INT";
		$objArguments->id = "tabela_chave_tabela_chave_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->homologacao_tabela_chave_atributo_id_INT == ""){

			$this->homologacao_tabela_chave_atributo_id_INT = "null";

		}

		if($this->producao_tabela_chave_atributo_id_INT == ""){

			$this->producao_tabela_chave_atributo_id_INT = "null";

		}

		if($this->tabela_chave_tabela_chave_id_INT == ""){

			$this->tabela_chave_tabela_chave_id_INT = "null";

		}

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->homologacao_tabela_chave_atributo_id_INT = null; 
		$this->objHomologacao_tabela_chave_atributo= null;
		$this->producao_tabela_chave_atributo_id_INT = null; 
		$this->objProducao_tabela_chave_atributo= null;
		$this->tabela_chave_tabela_chave_id_INT = null; 
		$this->objTabela_chave_tabela_chave= null;
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("homologacao_tabela_chave_atributo_id_INT", $this->homologacao_tabela_chave_atributo_id_INT); 
		Helper::setSession("producao_tabela_chave_atributo_id_INT", $this->producao_tabela_chave_atributo_id_INT); 
		Helper::setSession("tabela_chave_tabela_chave_id_INT", $this->tabela_chave_tabela_chave_id_INT); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("homologacao_tabela_chave_atributo_id_INT");
		Helper::clearSession("producao_tabela_chave_atributo_id_INT");
		Helper::clearSession("tabela_chave_tabela_chave_id_INT");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->homologacao_tabela_chave_atributo_id_INT = Helper::SESSION("homologacao_tabela_chave_atributo_id_INT{$numReg}"); 
		$this->producao_tabela_chave_atributo_id_INT = Helper::SESSION("producao_tabela_chave_atributo_id_INT{$numReg}"); 
		$this->tabela_chave_tabela_chave_id_INT = Helper::SESSION("tabela_chave_tabela_chave_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->homologacao_tabela_chave_atributo_id_INT = Helper::POST("homologacao_tabela_chave_atributo_id_INT{$numReg}"); 
		$this->producao_tabela_chave_atributo_id_INT = Helper::POST("producao_tabela_chave_atributo_id_INT{$numReg}"); 
		$this->tabela_chave_tabela_chave_id_INT = Helper::POST("tabela_chave_tabela_chave_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->homologacao_tabela_chave_atributo_id_INT = Helper::GET("homologacao_tabela_chave_atributo_id_INT{$numReg}"); 
		$this->producao_tabela_chave_atributo_id_INT = Helper::GET("producao_tabela_chave_atributo_id_INT{$numReg}"); 
		$this->tabela_chave_tabela_chave_id_INT = Helper::GET("tabela_chave_tabela_chave_id_INT{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["homologacao_tabela_chave_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "homologacao_tabela_chave_atributo_id_INT = $this->homologacao_tabela_chave_atributo_id_INT, ";

	} 

	if(isset($tipo["producao_tabela_chave_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "producao_tabela_chave_atributo_id_INT = $this->producao_tabela_chave_atributo_id_INT, ";

	} 

	if(isset($tipo["tabela_chave_tabela_chave_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_chave_tabela_chave_id_INT = $this->tabela_chave_tabela_chave_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE tabela_chave_atributo_tabela_chave_atributo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    