<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Processo_estrutura_caminho
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Processo_estrutura_caminho.php
    * TABELA MYSQL:    processo_estrutura_caminho
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Processo_estrutura_caminho extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $tipo_processo_estrutura_id_INT;
	public $obj;
	public $processo_estrutura_id_INT;
	public $objTipo_processo_estrutura;
	public $seq_INT;


    public $nomeEntidade;



    

	public $label_id;
	public $label_tipo_processo_estrutura_id_INT;
	public $label_processo_estrutura_id_INT;
	public $label_seq_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "processo_estrutura_caminho";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjTipo_processo_estrutura(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Tipo_processo_estrutura($this->getDatabase());
		if($this->tipo_processo_estrutura_id_INT != null) 
		$this->obj->select($this->tipo_processo_estrutura_id_INT);
	}
	return $this->obj ;
}
function getFkObjProcesso_estrutura(){
	if($this->objTipo_processo_estrutura ==null){
		$this->objTipo_processo_estrutura = new EXTDAO_Processo_estrutura($this->getDatabase());
		if($this->processo_estrutura_id_INT != null) 
		$this->objTipo_processo_estrutura->select($this->processo_estrutura_id_INT);
	}
	return $this->objTipo_processo_estrutura ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllTipo_processo_estrutura($objArgumentos){

		$objArgumentos->nome="tipo_processo_estrutura_id_INT";
		$objArgumentos->id="tipo_processo_estrutura_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_processo_estrutura()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProcesso_estrutura($objArgumentos){

		$objArgumentos->nome="processo_estrutura_id_INT";
		$objArgumentos->id="processo_estrutura_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProcesso_estrutura()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_processo_estrutura_caminho", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_processo_estrutura_caminho", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getTipo_processo_estrutura_id_INT()
    {
    	return $this->tipo_processo_estrutura_id_INT;
    }
    
    public function getProcesso_estrutura_id_INT()
    {
    	return $this->processo_estrutura_id_INT;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setTipo_processo_estrutura_id_INT($val)
    {
    	$this->tipo_processo_estrutura_id_INT =  $val;
    }
    
    function setProcesso_estrutura_id_INT($val)
    {
    	$this->processo_estrutura_id_INT =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM processo_estrutura_caminho WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->tipo_processo_estrutura_id_INT = $row->tipo_processo_estrutura_id_INT;
        if(isset($this->objTipo_processo_estrutura))
			$this->objTipo_processo_estrutura->select($this->tipo_processo_estrutura_id_INT);

        $this->processo_estrutura_id_INT = $row->processo_estrutura_id_INT;
        if(isset($this->objProcesso_estrutura))
			$this->objProcesso_estrutura->select($this->processo_estrutura_id_INT);

        $this->seq_INT = $row->seq_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM processo_estrutura_caminho WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO processo_estrutura_caminho ( tipo_processo_estrutura_id_INT , processo_estrutura_id_INT , seq_INT ) VALUES ( {$this->tipo_processo_estrutura_id_INT} , {$this->processo_estrutura_id_INT} , {$this->seq_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoTipo_processo_estrutura_id_INT(){ 

		return "tipo_processo_estrutura_id_INT";

	}

	public function nomeCampoProcesso_estrutura_id_INT(){ 

		return "processo_estrutura_id_INT";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoTipo_processo_estrutura_id_INT($objArguments){

		$objArguments->nome = "tipo_processo_estrutura_id_INT";
		$objArguments->id = "tipo_processo_estrutura_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProcesso_estrutura_id_INT($objArguments){

		$objArguments->nome = "processo_estrutura_id_INT";
		$objArguments->id = "processo_estrutura_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->tipo_processo_estrutura_id_INT == ""){

			$this->tipo_processo_estrutura_id_INT = "null";

		}

		if($this->processo_estrutura_id_INT == ""){

			$this->processo_estrutura_id_INT = "null";

		}

		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->tipo_processo_estrutura_id_INT = null; 
		$this->objTipo_processo_estrutura= null;
		$this->processo_estrutura_id_INT = null; 
		$this->objProcesso_estrutura= null;
		$this->seq_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("tipo_processo_estrutura_id_INT", $this->tipo_processo_estrutura_id_INT); 
		Helper::setSession("processo_estrutura_id_INT", $this->processo_estrutura_id_INT); 
		Helper::setSession("seq_INT", $this->seq_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("tipo_processo_estrutura_id_INT");
		Helper::clearSession("processo_estrutura_id_INT");
		Helper::clearSession("seq_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->tipo_processo_estrutura_id_INT = Helper::SESSION("tipo_processo_estrutura_id_INT{$numReg}"); 
		$this->processo_estrutura_id_INT = Helper::SESSION("processo_estrutura_id_INT{$numReg}"); 
		$this->seq_INT = Helper::SESSION("seq_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->tipo_processo_estrutura_id_INT = Helper::POST("tipo_processo_estrutura_id_INT{$numReg}"); 
		$this->processo_estrutura_id_INT = Helper::POST("processo_estrutura_id_INT{$numReg}"); 
		$this->seq_INT = Helper::POST("seq_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->tipo_processo_estrutura_id_INT = Helper::GET("tipo_processo_estrutura_id_INT{$numReg}"); 
		$this->processo_estrutura_id_INT = Helper::GET("processo_estrutura_id_INT{$numReg}"); 
		$this->seq_INT = Helper::GET("seq_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["tipo_processo_estrutura_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_processo_estrutura_id_INT = $this->tipo_processo_estrutura_id_INT, ";

	} 

	if(isset($tipo["processo_estrutura_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "processo_estrutura_id_INT = $this->processo_estrutura_id_INT, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == null){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE processo_estrutura_caminho SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    