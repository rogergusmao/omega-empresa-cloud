<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_download_banco_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Operacao_download_banco_mobile.php
    * TABELA MYSQL:    operacao_download_banco_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_download_banco_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $path_script_sql_banco;
	public $observacao;
	public $operacao_sistema_mobile_id_INT;
	public $obj;
	public $popular_tabela_BOOLEAN;
	public $drop_tabela_BOOLEAN;
	public $destino_banco_id_INT;
	public $objOperacao_sistema_mobile;


    public $nomeEntidade;



    

	public $label_id;
	public $label_path_script_sql_banco;
	public $label_observacao;
	public $label_operacao_sistema_mobile_id_INT;
	public $label_popular_tabela_BOOLEAN;
	public $label_drop_tabela_BOOLEAN;
	public $label_destino_banco_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "operacao_download_banco_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjOperacao_sistema_mobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Operacao_sistema_mobile($this->getDatabase());
		if($this->operacao_sistema_mobile_id_INT != null) 
		$this->obj->select($this->operacao_sistema_mobile_id_INT);
	}
	return $this->obj ;
}
function getFkObjDestino_banco(){
	if($this->objOperacao_sistema_mobile ==null){
		$this->objOperacao_sistema_mobile = new EXTDAO_Banco($this->getDatabase());
		if($this->destino_banco_id_INT != null) 
		$this->objOperacao_sistema_mobile->select($this->destino_banco_id_INT);
	}
	return $this->objOperacao_sistema_mobile ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllOperacao_sistema_mobile($objArgumentos){

		$objArgumentos->nome="operacao_sistema_mobile_id_INT";
		$objArgumentos->id="operacao_sistema_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjOperacao_sistema_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllDestino_banco($objArgumentos){

		$objArgumentos->nome="destino_banco_id_INT";
		$objArgumentos->id="destino_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjDestino_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_operacao_download_banco_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_download_banco_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getPath_script_sql_banco()
    {
    	return $this->path_script_sql_banco;
    }
    
    public function getObservacao()
    {
    	return $this->observacao;
    }
    
    public function getOperacao_sistema_mobile_id_INT()
    {
    	return $this->operacao_sistema_mobile_id_INT;
    }
    
    public function getPopular_tabela_BOOLEAN()
    {
    	return $this->popular_tabela_BOOLEAN;
    }
    
    public function getDrop_tabela_BOOLEAN()
    {
    	return $this->drop_tabela_BOOLEAN;
    }
    
    public function getDestino_banco_id_INT()
    {
    	return $this->destino_banco_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setPath_script_sql_banco($val)
    {
    	$this->path_script_sql_banco =  $val;
    }
    
    function setObservacao($val)
    {
    	$this->observacao =  $val;
    }
    
    function setOperacao_sistema_mobile_id_INT($val)
    {
    	$this->operacao_sistema_mobile_id_INT =  $val;
    }
    
    function setPopular_tabela_BOOLEAN($val)
    {
    	$this->popular_tabela_BOOLEAN =  $val;
    }
    
    function setDrop_tabela_BOOLEAN($val)
    {
    	$this->drop_tabela_BOOLEAN =  $val;
    }
    
    function setDestino_banco_id_INT($val)
    {
    	$this->destino_banco_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM operacao_download_banco_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->path_script_sql_banco = $row->path_script_sql_banco;
        
        $this->observacao = $row->observacao;
        
        $this->operacao_sistema_mobile_id_INT = $row->operacao_sistema_mobile_id_INT;
        if(isset($this->objOperacao_sistema_mobile))
			$this->objOperacao_sistema_mobile->select($this->operacao_sistema_mobile_id_INT);

        $this->popular_tabela_BOOLEAN = $row->popular_tabela_BOOLEAN;
        
        $this->drop_tabela_BOOLEAN = $row->drop_tabela_BOOLEAN;
        
        $this->destino_banco_id_INT = $row->destino_banco_id_INT;
        if(isset($this->objDestino_banco))
			$this->objDestino_banco->select($this->destino_banco_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM operacao_download_banco_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO operacao_download_banco_mobile ( path_script_sql_banco , observacao , operacao_sistema_mobile_id_INT , popular_tabela_BOOLEAN , drop_tabela_BOOLEAN , destino_banco_id_INT ) VALUES ( {$this->path_script_sql_banco} , {$this->observacao} , {$this->operacao_sistema_mobile_id_INT} , {$this->popular_tabela_BOOLEAN} , {$this->drop_tabela_BOOLEAN} , {$this->destino_banco_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoPath_script_sql_banco(){ 

		return "path_script_sql_banco";

	}

	public function nomeCampoObservacao(){ 

		return "observacao";

	}

	public function nomeCampoOperacao_sistema_mobile_id_INT(){ 

		return "operacao_sistema_mobile_id_INT";

	}

	public function nomeCampoPopular_tabela_BOOLEAN(){ 

		return "popular_tabela_BOOLEAN";

	}

	public function nomeCampoDrop_tabela_BOOLEAN(){ 

		return "drop_tabela_BOOLEAN";

	}

	public function nomeCampoDestino_banco_id_INT(){ 

		return "destino_banco_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoPath_script_sql_banco($objArguments){

		$objArguments->nome = "path_script_sql_banco";
		$objArguments->id = "path_script_sql_banco";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoObservacao($objArguments){

		$objArguments->nome = "observacao";
		$objArguments->id = "observacao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoOperacao_sistema_mobile_id_INT($objArguments){

		$objArguments->nome = "operacao_sistema_mobile_id_INT";
		$objArguments->id = "operacao_sistema_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPopular_tabela_BOOLEAN($objArguments){

		$objArguments->nome = "popular_tabela_BOOLEAN";
		$objArguments->id = "popular_tabela_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoDrop_tabela_BOOLEAN($objArguments){

		$objArguments->nome = "drop_tabela_BOOLEAN";
		$objArguments->id = "drop_tabela_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoDestino_banco_id_INT($objArguments){

		$objArguments->nome = "destino_banco_id_INT";
		$objArguments->id = "destino_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->path_script_sql_banco = $this->formatarDadosParaSQL($this->path_script_sql_banco);
			$this->observacao = $this->formatarDadosParaSQL($this->observacao);
		if($this->operacao_sistema_mobile_id_INT == ""){

			$this->operacao_sistema_mobile_id_INT = "null";

		}

		if($this->popular_tabela_BOOLEAN == ""){

			$this->popular_tabela_BOOLEAN = "null";

		}

		if($this->drop_tabela_BOOLEAN == ""){

			$this->drop_tabela_BOOLEAN = "null";

		}

		if($this->destino_banco_id_INT == ""){

			$this->destino_banco_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->path_script_sql_banco = null; 
		$this->observacao = null; 
		$this->operacao_sistema_mobile_id_INT = null; 
		$this->objOperacao_sistema_mobile= null;
		$this->popular_tabela_BOOLEAN = null; 
		$this->drop_tabela_BOOLEAN = null; 
		$this->destino_banco_id_INT = null; 
		$this->objDestino_banco= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("path_script_sql_banco", $this->path_script_sql_banco); 
		Helper::setSession("observacao", $this->observacao); 
		Helper::setSession("operacao_sistema_mobile_id_INT", $this->operacao_sistema_mobile_id_INT); 
		Helper::setSession("popular_tabela_BOOLEAN", $this->popular_tabela_BOOLEAN); 
		Helper::setSession("drop_tabela_BOOLEAN", $this->drop_tabela_BOOLEAN); 
		Helper::setSession("destino_banco_id_INT", $this->destino_banco_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("path_script_sql_banco");
		Helper::clearSession("observacao");
		Helper::clearSession("operacao_sistema_mobile_id_INT");
		Helper::clearSession("popular_tabela_BOOLEAN");
		Helper::clearSession("drop_tabela_BOOLEAN");
		Helper::clearSession("destino_banco_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->path_script_sql_banco = Helper::SESSION("path_script_sql_banco{$numReg}"); 
		$this->observacao = Helper::SESSION("observacao{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::SESSION("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->popular_tabela_BOOLEAN = Helper::SESSION("popular_tabela_BOOLEAN{$numReg}"); 
		$this->drop_tabela_BOOLEAN = Helper::SESSION("drop_tabela_BOOLEAN{$numReg}"); 
		$this->destino_banco_id_INT = Helper::SESSION("destino_banco_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->path_script_sql_banco = Helper::POST("path_script_sql_banco{$numReg}"); 
		$this->observacao = Helper::POST("observacao{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::POST("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->popular_tabela_BOOLEAN = Helper::POST("popular_tabela_BOOLEAN{$numReg}"); 
		$this->drop_tabela_BOOLEAN = Helper::POST("drop_tabela_BOOLEAN{$numReg}"); 
		$this->destino_banco_id_INT = Helper::POST("destino_banco_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->path_script_sql_banco = Helper::GET("path_script_sql_banco{$numReg}"); 
		$this->observacao = Helper::GET("observacao{$numReg}"); 
		$this->operacao_sistema_mobile_id_INT = Helper::GET("operacao_sistema_mobile_id_INT{$numReg}"); 
		$this->popular_tabela_BOOLEAN = Helper::GET("popular_tabela_BOOLEAN{$numReg}"); 
		$this->drop_tabela_BOOLEAN = Helper::GET("drop_tabela_BOOLEAN{$numReg}"); 
		$this->destino_banco_id_INT = Helper::GET("destino_banco_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["path_script_sql_banco{$numReg}"]) || $tipo == null){

		$upd.= "path_script_sql_banco = $this->path_script_sql_banco, ";

	} 

	if(isset($tipo["observacao{$numReg}"]) || $tipo == null){

		$upd.= "observacao = $this->observacao, ";

	} 

	if(isset($tipo["operacao_sistema_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "operacao_sistema_mobile_id_INT = $this->operacao_sistema_mobile_id_INT, ";

	} 

	if(isset($tipo["popular_tabela_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "popular_tabela_BOOLEAN = $this->popular_tabela_BOOLEAN, ";

	} 

	if(isset($tipo["drop_tabela_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "drop_tabela_BOOLEAN = $this->drop_tabela_BOOLEAN, ";

	} 

	if(isset($tipo["destino_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "destino_banco_id_INT = $this->destino_banco_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE operacao_download_banco_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    