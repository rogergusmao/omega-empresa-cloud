<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_projetos_versao_produto
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_projetos_versao_produto.php
    * TABELA MYSQL:    sistema_projetos_versao_produto
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_projetos_versao_produto extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_projetos_versao_id_INT;
	public $obj;
	public $sistema_produto_id_INT;
	public $objSistema_projetos_versao;
	public $prototipo_projetos_versao_banco_banco_id_INT;
	public $objSistema_produto;
	public $servidor_projetos_versao_banco_banco_id_INT;
	public $objPrototipo_projetos_versao_banco_banco;
	public $script_estrutura_banco_hom_para_prod_ARQUIVO;
	public $script_registros_banco_hom_para_prod_ARQUIVO;
	public $xml_estrutura_banco_hom_para_prod_ARQUIVO;
	public $xml_registros_banco_hom_para_prod_ARQUIVO;
	public $script_estrutura_banco_hom_ARQUIVO;
	public $script_estrutura_banco_prod_ARQUIVO;
	public $data_homologacao_DATETIME;
	public $data_producao_DATETIME;


    public $nomeEntidade;

	public $data_homologacao_DATETIME_UNIX;
	public $data_producao_DATETIME_UNIX;


    

	public $label_id;
	public $label_sistema_projetos_versao_id_INT;
	public $label_sistema_produto_id_INT;
	public $label_prototipo_projetos_versao_banco_banco_id_INT;
	public $label_servidor_projetos_versao_banco_banco_id_INT;
	public $label_script_estrutura_banco_hom_para_prod_ARQUIVO;
	public $label_script_registros_banco_hom_para_prod_ARQUIVO;
	public $label_xml_estrutura_banco_hom_para_prod_ARQUIVO;
	public $label_xml_registros_banco_hom_para_prod_ARQUIVO;
	public $label_script_estrutura_banco_hom_ARQUIVO;
	public $label_script_estrutura_banco_prod_ARQUIVO;
	public $label_data_homologacao_DATETIME;
	public $label_data_producao_DATETIME;


	public $diretorio_script_estrutura_banco_hom_para_prod_ARQUIVO;
	public $diretorio_script_registros_banco_hom_para_prod_ARQUIVO;
	public $diretorio_xml_estrutura_banco_hom_para_prod_ARQUIVO;
	public $diretorio_xml_registros_banco_hom_para_prod_ARQUIVO;
	public $diretorio_script_estrutura_banco_hom_ARQUIVO;
	public $diretorio_script_estrutura_banco_prod_ARQUIVO;




    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_projetos_versao_produto";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_projetos_versao(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_projetos_versao($this->getDatabase());
		if($this->sistema_projetos_versao_id_INT != null) 
		$this->obj->select($this->sistema_projetos_versao_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_produto(){
	if($this->objSistema_projetos_versao ==null){
		$this->objSistema_projetos_versao = new EXTDAO_Sistema_produto($this->getDatabase());
		if($this->sistema_produto_id_INT != null) 
		$this->objSistema_projetos_versao->select($this->sistema_produto_id_INT);
	}
	return $this->objSistema_projetos_versao ;
}
function getFkObjPrototipo_projetos_versao_banco_banco(){
	if($this->objSistema_produto ==null){
		$this->objSistema_produto = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->prototipo_projetos_versao_banco_banco_id_INT != null) 
		$this->objSistema_produto->select($this->prototipo_projetos_versao_banco_banco_id_INT);
	}
	return $this->objSistema_produto ;
}
function getFkObjServidor_projetos_versao_banco_banco(){
	if($this->objPrototipo_projetos_versao_banco_banco ==null){
		$this->objPrototipo_projetos_versao_banco_banco = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->servidor_projetos_versao_banco_banco_id_INT != null) 
		$this->objPrototipo_projetos_versao_banco_banco->select($this->servidor_projetos_versao_banco_banco_id_INT);
	}
	return $this->objPrototipo_projetos_versao_banco_banco ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_produto($objArgumentos){

		$objArgumentos->nome="sistema_produto_id_INT";
		$objArgumentos->id="sistema_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_produto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllPrototipo_projetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="prototipo_projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="prototipo_projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjPrototipo_projetos_versao_banco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllServidor_projetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="servidor_projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="servidor_projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjServidor_projetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_projetos_versao_produto", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_projetos_versao_produto", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMO��O DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_script_estrutura_banco_hom_para_prod_ARQUIVO . $this->getScript_estrutura_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_script_registros_banco_hom_para_prod_ARQUIVO . $this->getScript_registros_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_xml_estrutura_banco_hom_para_prod_ARQUIVO . $this->getXml_estrutura_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_xml_registros_banco_hom_para_prod_ARQUIVO . $this->getXml_registros_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Script_estrutura_banco_hom_ARQUIVO
            $pathArquivo = $this->diretorio_script_estrutura_banco_hom_ARQUIVO . $this->getScript_estrutura_banco_hom_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Script_estrutura_banco_prod_ARQUIVO
            $pathArquivo = $this->diretorio_script_estrutura_banco_prod_ARQUIVO . $this->getScript_estrutura_banco_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_script_estrutura_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_script_estrutura_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["script_estrutura_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadScript_registros_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_script_registros_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_script_registros_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["script_registros_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_2." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_xml_estrutura_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_xml_estrutura_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["xml_estrutura_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_3." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadXml_registros_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_xml_registros_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_xml_registros_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["xml_registros_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_4." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadScript_estrutura_banco_hom_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_script_estrutura_banco_hom_ARQUIVO;
            $labelCampo = $this->label_script_estrutura_banco_hom_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["script_estrutura_banco_hom_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_5." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadScript_estrutura_banco_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_script_estrutura_banco_prod_ARQUIVO;
            $labelCampo = $this->label_script_estrutura_banco_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["script_estrutura_banco_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_6." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_projetos_versao_id_INT()
    {
    	return $this->sistema_projetos_versao_id_INT;
    }
    
    public function getSistema_produto_id_INT()
    {
    	return $this->sistema_produto_id_INT;
    }
    
    public function getPrototipo_projetos_versao_banco_banco_id_INT()
    {
    	return $this->prototipo_projetos_versao_banco_banco_id_INT;
    }
    
    public function getServidor_projetos_versao_banco_banco_id_INT()
    {
    	return $this->servidor_projetos_versao_banco_banco_id_INT;
    }
    
    public function getScript_estrutura_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->script_estrutura_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getScript_registros_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->script_registros_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getXml_estrutura_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->xml_estrutura_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getXml_registros_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->xml_registros_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getScript_estrutura_banco_hom_ARQUIVO()
    {
    	return $this->script_estrutura_banco_hom_ARQUIVO;
    }
    
    public function getScript_estrutura_banco_prod_ARQUIVO()
    {
    	return $this->script_estrutura_banco_prod_ARQUIVO;
    }
    
    function getData_homologacao_DATETIME_UNIX()
    {
    	return $this->data_homologacao_DATETIME_UNIX;
    }
    
    public function getData_homologacao_DATETIME()
    {
    	return $this->data_homologacao_DATETIME;
    }
    
    function getData_producao_DATETIME_UNIX()
    {
    	return $this->data_producao_DATETIME_UNIX;
    }
    
    public function getData_producao_DATETIME()
    {
    	return $this->data_producao_DATETIME;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_projetos_versao_id_INT($val)
    {
    	$this->sistema_projetos_versao_id_INT =  $val;
    }
    
    function setSistema_produto_id_INT($val)
    {
    	$this->sistema_produto_id_INT =  $val;
    }
    
    function setPrototipo_projetos_versao_banco_banco_id_INT($val)
    {
    	$this->prototipo_projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setServidor_projetos_versao_banco_banco_id_INT($val)
    {
    	$this->servidor_projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setScript_estrutura_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->script_estrutura_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setScript_registros_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->script_registros_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setXml_estrutura_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->xml_estrutura_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setXml_registros_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->xml_registros_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setScript_estrutura_banco_hom_ARQUIVO($val)
    {
    	$this->script_estrutura_banco_hom_ARQUIVO =  $val;
    }
    
    function setScript_estrutura_banco_prod_ARQUIVO($val)
    {
    	$this->script_estrutura_banco_prod_ARQUIVO =  $val;
    }
    
    function setData_homologacao_DATETIME($val)
    {
    	$this->data_homologacao_DATETIME =  $val;
    }
    
    function setData_producao_DATETIME($val)
    {
    	$this->data_producao_DATETIME =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_homologacao_DATETIME) AS data_homologacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_producao_DATETIME) AS data_producao_DATETIME_UNIX FROM sistema_projetos_versao_produto WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->sistema_projetos_versao_id_INT = $row->sistema_projetos_versao_id_INT;
        if(isset($this->objSistema_projetos_versao))
			$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_id_INT);

        $this->sistema_produto_id_INT = $row->sistema_produto_id_INT;
        if(isset($this->objSistema_produto))
			$this->objSistema_produto->select($this->sistema_produto_id_INT);

        $this->prototipo_projetos_versao_banco_banco_id_INT = $row->prototipo_projetos_versao_banco_banco_id_INT;
        if(isset($this->objPrototipo_projetos_versao_banco_banco))
			$this->objPrototipo_projetos_versao_banco_banco->select($this->prototipo_projetos_versao_banco_banco_id_INT);

        $this->servidor_projetos_versao_banco_banco_id_INT = $row->servidor_projetos_versao_banco_banco_id_INT;
        if(isset($this->objServidor_projetos_versao_banco_banco))
			$this->objServidor_projetos_versao_banco_banco->select($this->servidor_projetos_versao_banco_banco_id_INT);

        $this->script_estrutura_banco_hom_para_prod_ARQUIVO = $row->script_estrutura_banco_hom_para_prod_ARQUIVO;
        
        $this->script_registros_banco_hom_para_prod_ARQUIVO = $row->script_registros_banco_hom_para_prod_ARQUIVO;
        
        $this->xml_estrutura_banco_hom_para_prod_ARQUIVO = $row->xml_estrutura_banco_hom_para_prod_ARQUIVO;
        
        $this->xml_registros_banco_hom_para_prod_ARQUIVO = $row->xml_registros_banco_hom_para_prod_ARQUIVO;
        
        $this->script_estrutura_banco_hom_ARQUIVO = $row->script_estrutura_banco_hom_ARQUIVO;
        
        $this->script_estrutura_banco_prod_ARQUIVO = $row->script_estrutura_banco_prod_ARQUIVO;
        
        $this->data_homologacao_DATETIME = $row->data_homologacao_DATETIME;
        $this->data_homologacao_DATETIME_UNIX = $row->data_homologacao_DATETIME_UNIX;

        $this->data_producao_DATETIME = $row->data_producao_DATETIME;
        $this->data_producao_DATETIME_UNIX = $row->data_producao_DATETIME_UNIX;

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_projetos_versao_produto WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_projetos_versao_produto ( sistema_projetos_versao_id_INT , sistema_produto_id_INT , prototipo_projetos_versao_banco_banco_id_INT , servidor_projetos_versao_banco_banco_id_INT , script_estrutura_banco_hom_para_prod_ARQUIVO , script_registros_banco_hom_para_prod_ARQUIVO , xml_estrutura_banco_hom_para_prod_ARQUIVO , xml_registros_banco_hom_para_prod_ARQUIVO , script_estrutura_banco_hom_ARQUIVO , script_estrutura_banco_prod_ARQUIVO , data_homologacao_DATETIME , data_producao_DATETIME ) VALUES ( {$this->sistema_projetos_versao_id_INT} , {$this->sistema_produto_id_INT} , {$this->prototipo_projetos_versao_banco_banco_id_INT} , {$this->servidor_projetos_versao_banco_banco_id_INT} , {$this->script_estrutura_banco_hom_para_prod_ARQUIVO} , {$this->script_registros_banco_hom_para_prod_ARQUIVO} , {$this->xml_estrutura_banco_hom_para_prod_ARQUIVO} , {$this->xml_registros_banco_hom_para_prod_ARQUIVO} , {$this->script_estrutura_banco_hom_ARQUIVO} , {$this->script_estrutura_banco_prod_ARQUIVO} , {$this->data_homologacao_DATETIME} , {$this->data_producao_DATETIME} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_projetos_versao_id_INT(){ 

		return "sistema_projetos_versao_id_INT";

	}

	public function nomeCampoSistema_produto_id_INT(){ 

		return "sistema_produto_id_INT";

	}

	public function nomeCampoPrototipo_projetos_versao_banco_banco_id_INT(){ 

		return "prototipo_projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoServidor_projetos_versao_banco_banco_id_INT(){ 

		return "servidor_projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoScript_estrutura_banco_hom_para_prod_ARQUIVO(){ 

		return "script_estrutura_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoScript_registros_banco_hom_para_prod_ARQUIVO(){ 

		return "script_registros_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoXml_estrutura_banco_hom_para_prod_ARQUIVO(){ 

		return "xml_estrutura_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoXml_registros_banco_hom_para_prod_ARQUIVO(){ 

		return "xml_registros_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoScript_estrutura_banco_hom_ARQUIVO(){ 

		return "script_estrutura_banco_hom_ARQUIVO";

	}

	public function nomeCampoScript_estrutura_banco_prod_ARQUIVO(){ 

		return "script_estrutura_banco_prod_ARQUIVO";

	}

	public function nomeCampoData_homologacao_DATETIME(){ 

		return "data_homologacao_DATETIME";

	}

	public function nomeCampoData_producao_DATETIME(){ 

		return "data_producao_DATETIME";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_id_INT";
		$objArguments->id = "sistema_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_produto_id_INT($objArguments){

		$objArguments->nome = "sistema_produto_id_INT";
		$objArguments->id = "sistema_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPrototipo_projetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "prototipo_projetos_versao_banco_banco_id_INT";
		$objArguments->id = "prototipo_projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoServidor_projetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "servidor_projetos_versao_banco_banco_id_INT";
		$objArguments->id = "servidor_projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoScript_estrutura_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "script_estrutura_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "script_estrutura_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoScript_registros_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "script_registros_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "script_registros_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoXml_estrutura_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "xml_estrutura_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "xml_estrutura_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoXml_registros_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "xml_registros_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "xml_registros_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoScript_estrutura_banco_hom_ARQUIVO($objArguments){

		$objArguments->nome = "script_estrutura_banco_hom_ARQUIVO";
		$objArguments->id = "script_estrutura_banco_hom_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoScript_estrutura_banco_prod_ARQUIVO($objArguments){

		$objArguments->nome = "script_estrutura_banco_prod_ARQUIVO";
		$objArguments->id = "script_estrutura_banco_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoData_homologacao_DATETIME($objArguments){

		$objArguments->nome = "data_homologacao_DATETIME";
		$objArguments->id = "data_homologacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_producao_DATETIME($objArguments){

		$objArguments->nome = "data_producao_DATETIME";
		$objArguments->id = "data_producao_DATETIME";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_projetos_versao_id_INT == ""){

			$this->sistema_projetos_versao_id_INT = "null";

		}

		if($this->sistema_produto_id_INT == ""){

			$this->sistema_produto_id_INT = "null";

		}

		if($this->prototipo_projetos_versao_banco_banco_id_INT == ""){

			$this->prototipo_projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->servidor_projetos_versao_banco_banco_id_INT == ""){

			$this->servidor_projetos_versao_banco_banco_id_INT = "null";

		}

			$this->script_estrutura_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->script_estrutura_banco_hom_para_prod_ARQUIVO);
			$this->script_registros_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->script_registros_banco_hom_para_prod_ARQUIVO);
			$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->xml_estrutura_banco_hom_para_prod_ARQUIVO);
			$this->xml_registros_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->xml_registros_banco_hom_para_prod_ARQUIVO);
			$this->script_estrutura_banco_hom_ARQUIVO = $this->formatarDadosParaSQL($this->script_estrutura_banco_hom_ARQUIVO);
			$this->script_estrutura_banco_prod_ARQUIVO = $this->formatarDadosParaSQL($this->script_estrutura_banco_prod_ARQUIVO);


	$this->data_homologacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_homologacao_DATETIME); 
	$this->data_producao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_producao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_homologacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_homologacao_DATETIME); 
	$this->data_producao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_producao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->sistema_projetos_versao_id_INT = null; 
		$this->objSistema_projetos_versao= null;
		$this->sistema_produto_id_INT = null; 
		$this->objSistema_produto= null;
		$this->prototipo_projetos_versao_banco_banco_id_INT = null; 
		$this->objPrototipo_projetos_versao_banco_banco= null;
		$this->servidor_projetos_versao_banco_banco_id_INT = null; 
		$this->objServidor_projetos_versao_banco_banco= null;
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = null; 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = null; 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = null; 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = null; 
		$this->script_estrutura_banco_hom_ARQUIVO = null; 
		$this->script_estrutura_banco_prod_ARQUIVO = null; 
		$this->data_homologacao_DATETIME = null; 
		$this->data_producao_DATETIME = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("sistema_projetos_versao_id_INT", $this->sistema_projetos_versao_id_INT); 
		Helper::setSession("sistema_produto_id_INT", $this->sistema_produto_id_INT); 
		Helper::setSession("prototipo_projetos_versao_banco_banco_id_INT", $this->prototipo_projetos_versao_banco_banco_id_INT); 
		Helper::setSession("servidor_projetos_versao_banco_banco_id_INT", $this->servidor_projetos_versao_banco_banco_id_INT); 
		Helper::setSession("script_estrutura_banco_hom_para_prod_ARQUIVO", $this->script_estrutura_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("script_registros_banco_hom_para_prod_ARQUIVO", $this->script_registros_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("xml_estrutura_banco_hom_para_prod_ARQUIVO", $this->xml_estrutura_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("xml_registros_banco_hom_para_prod_ARQUIVO", $this->xml_registros_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("script_estrutura_banco_hom_ARQUIVO", $this->script_estrutura_banco_hom_ARQUIVO); 
		Helper::setSession("script_estrutura_banco_prod_ARQUIVO", $this->script_estrutura_banco_prod_ARQUIVO); 
		Helper::setSession("data_homologacao_DATETIME", $this->data_homologacao_DATETIME); 
		Helper::setSession("data_producao_DATETIME", $this->data_producao_DATETIME); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("sistema_projetos_versao_id_INT");
		Helper::clearSession("sistema_produto_id_INT");
		Helper::clearSession("prototipo_projetos_versao_banco_banco_id_INT");
		Helper::clearSession("servidor_projetos_versao_banco_banco_id_INT");
		Helper::clearSession("script_estrutura_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("script_registros_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("xml_estrutura_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("xml_registros_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("script_estrutura_banco_hom_ARQUIVO");
		Helper::clearSession("script_estrutura_banco_prod_ARQUIVO");
		Helper::clearSession("data_homologacao_DATETIME");
		Helper::clearSession("data_producao_DATETIME");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::SESSION("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->sistema_produto_id_INT = Helper::SESSION("sistema_produto_id_INT{$numReg}"); 
		$this->prototipo_projetos_versao_banco_banco_id_INT = Helper::SESSION("prototipo_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->servidor_projetos_versao_banco_banco_id_INT = Helper::SESSION("servidor_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = Helper::SESSION("script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = Helper::SESSION("script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = Helper::SESSION("xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = Helper::SESSION("xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_estrutura_banco_hom_ARQUIVO = Helper::SESSION("script_estrutura_banco_hom_ARQUIVO{$numReg}"); 
		$this->script_estrutura_banco_prod_ARQUIVO = Helper::SESSION("script_estrutura_banco_prod_ARQUIVO{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::SESSION("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::SESSION("data_producao_DATETIME{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::POST("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->sistema_produto_id_INT = Helper::POST("sistema_produto_id_INT{$numReg}"); 
		$this->prototipo_projetos_versao_banco_banco_id_INT = Helper::POST("prototipo_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->servidor_projetos_versao_banco_banco_id_INT = Helper::POST("servidor_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = Helper::POST("script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = Helper::POST("script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = Helper::POST("xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = Helper::POST("xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_estrutura_banco_hom_ARQUIVO = Helper::POST("script_estrutura_banco_hom_ARQUIVO{$numReg}"); 
		$this->script_estrutura_banco_prod_ARQUIVO = Helper::POST("script_estrutura_banco_prod_ARQUIVO{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::POST("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::POST("data_producao_DATETIME{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::GET("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->sistema_produto_id_INT = Helper::GET("sistema_produto_id_INT{$numReg}"); 
		$this->prototipo_projetos_versao_banco_banco_id_INT = Helper::GET("prototipo_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->servidor_projetos_versao_banco_banco_id_INT = Helper::GET("servidor_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = Helper::GET("script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = Helper::GET("script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = Helper::GET("xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = Helper::GET("xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_estrutura_banco_hom_ARQUIVO = Helper::GET("script_estrutura_banco_hom_ARQUIVO{$numReg}"); 
		$this->script_estrutura_banco_prod_ARQUIVO = Helper::GET("script_estrutura_banco_prod_ARQUIVO{$numReg}"); 
		$this->data_homologacao_DATETIME = Helper::GET("data_homologacao_DATETIME{$numReg}"); 
		$this->data_producao_DATETIME = Helper::GET("data_producao_DATETIME{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_id_INT = $this->sistema_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["sistema_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_produto_id_INT = $this->sistema_produto_id_INT, ";

	} 

	if(isset($tipo["prototipo_projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "prototipo_projetos_versao_banco_banco_id_INT = $this->prototipo_projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["servidor_projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "servidor_projetos_versao_banco_banco_id_INT = $this->servidor_projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "script_estrutura_banco_hom_para_prod_ARQUIVO = $this->script_estrutura_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "script_registros_banco_hom_para_prod_ARQUIVO = $this->script_registros_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "xml_estrutura_banco_hom_para_prod_ARQUIVO = $this->xml_estrutura_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "xml_registros_banco_hom_para_prod_ARQUIVO = $this->xml_registros_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["script_estrutura_banco_hom_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "script_estrutura_banco_hom_ARQUIVO = $this->script_estrutura_banco_hom_ARQUIVO, ";

	} 

	if(isset($tipo["script_estrutura_banco_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "script_estrutura_banco_prod_ARQUIVO = $this->script_estrutura_banco_prod_ARQUIVO, ";

	} 

	if(isset($tipo["data_homologacao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_homologacao_DATETIME = $this->data_homologacao_DATETIME, ";

	} 

	if(isset($tipo["data_producao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_producao_DATETIME = $this->data_producao_DATETIME, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_projetos_versao_produto SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    