<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos.php
    * TABELA MYSQL:    projetos
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $conexaoBanco_INT;
	public $nomeBanco;
	public $diretorioClassesDAO;
	public $diretorioClassesEXTDAO;
	public $diretorioForms;
	public $diretorioFiltros;
	public $diretorioLists;
	public $diretorioAjaxForms;
	public $diretorioAjaxPages;
	public $colunasForms_INT;
	public $chaveComNomeTabela;
	public $underlineNomeClasse;
	public $status_INT;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_conexaoBanco_INT;
	public $label_nomeBanco;
	public $label_diretorioClassesDAO;
	public $label_diretorioClassesEXTDAO;
	public $label_diretorioForms;
	public $label_diretorioFiltros;
	public $label_diretorioLists;
	public $label_diretorioAjaxForms;
	public $label_diretorioAjaxPages;
	public $label_colunasForms_INT;
	public $label_chaveComNomeTabela;
	public $label_underlineNomeClasse;
	public $label_status_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getConexaoBanco_INT()
    {
    	return $this->conexaoBanco_INT;
    }
    
    public function getNomeBanco()
    {
    	return $this->nomeBanco;
    }
    
    public function getDiretorioClassesDAO()
    {
    	return $this->diretorioClassesDAO;
    }
    
    public function getDiretorioClassesEXTDAO()
    {
    	return $this->diretorioClassesEXTDAO;
    }
    
    public function getDiretorioForms()
    {
    	return $this->diretorioForms;
    }
    
    public function getDiretorioFiltros()
    {
    	return $this->diretorioFiltros;
    }
    
    public function getDiretorioLists()
    {
    	return $this->diretorioLists;
    }
    
    public function getDiretorioAjaxForms()
    {
    	return $this->diretorioAjaxForms;
    }
    
    public function getDiretorioAjaxPages()
    {
    	return $this->diretorioAjaxPages;
    }
    
    public function getColunasForms_INT()
    {
    	return $this->colunasForms_INT;
    }
    
    public function getChaveComNomeTabela()
    {
    	return $this->chaveComNomeTabela;
    }
    
    public function getUnderlineNomeClasse()
    {
    	return $this->underlineNomeClasse;
    }
    
    public function getStatus_INT()
    {
    	return $this->status_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setConexaoBanco_INT($val)
    {
    	$this->conexaoBanco_INT =  $val;
    }
    
    function setNomeBanco($val)
    {
    	$this->nomeBanco =  $val;
    }
    
    function setDiretorioClassesDAO($val)
    {
    	$this->diretorioClassesDAO =  $val;
    }
    
    function setDiretorioClassesEXTDAO($val)
    {
    	$this->diretorioClassesEXTDAO =  $val;
    }
    
    function setDiretorioForms($val)
    {
    	$this->diretorioForms =  $val;
    }
    
    function setDiretorioFiltros($val)
    {
    	$this->diretorioFiltros =  $val;
    }
    
    function setDiretorioLists($val)
    {
    	$this->diretorioLists =  $val;
    }
    
    function setDiretorioAjaxForms($val)
    {
    	$this->diretorioAjaxForms =  $val;
    }
    
    function setDiretorioAjaxPages($val)
    {
    	$this->diretorioAjaxPages =  $val;
    }
    
    function setColunasForms_INT($val)
    {
    	$this->colunasForms_INT =  $val;
    }
    
    function setChaveComNomeTabela($val)
    {
    	$this->chaveComNomeTabela =  $val;
    }
    
    function setUnderlineNomeClasse($val)
    {
    	$this->underlineNomeClasse =  $val;
    }
    
    function setStatus_INT($val)
    {
    	$this->status_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM projetos WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->conexaoBanco_INT = $row->conexaoBanco_INT;
        
        $this->nomeBanco = $row->nomeBanco;
        
        $this->diretorioClassesDAO = $row->diretorioClassesDAO;
        
        $this->diretorioClassesEXTDAO = $row->diretorioClassesEXTDAO;
        
        $this->diretorioForms = $row->diretorioForms;
        
        $this->diretorioFiltros = $row->diretorioFiltros;
        
        $this->diretorioLists = $row->diretorioLists;
        
        $this->diretorioAjaxForms = $row->diretorioAjaxForms;
        
        $this->diretorioAjaxPages = $row->diretorioAjaxPages;
        
        $this->colunasForms_INT = $row->colunasForms_INT;
        
        $this->chaveComNomeTabela = $row->chaveComNomeTabela;
        
        $this->underlineNomeClasse = $row->underlineNomeClasse;
        
        $this->status_INT = $row->status_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos ( nome , conexaoBanco_INT , nomeBanco , diretorioClassesDAO , diretorioClassesEXTDAO , diretorioForms , diretorioFiltros , diretorioLists , diretorioAjaxForms , diretorioAjaxPages , colunasForms_INT , chaveComNomeTabela , underlineNomeClasse , status_INT ) VALUES ( {$this->nome} , {$this->conexaoBanco_INT} , {$this->nomeBanco} , {$this->diretorioClassesDAO} , {$this->diretorioClassesEXTDAO} , {$this->diretorioForms} , {$this->diretorioFiltros} , {$this->diretorioLists} , {$this->diretorioAjaxForms} , {$this->diretorioAjaxPages} , {$this->colunasForms_INT} , {$this->chaveComNomeTabela} , {$this->underlineNomeClasse} , {$this->status_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoConexaoBanco_INT(){ 

		return "conexaoBanco_INT";

	}

	public function nomeCampoNomeBanco(){ 

		return "nomeBanco";

	}

	public function nomeCampoDiretorioClassesDAO(){ 

		return "diretorioClassesDAO";

	}

	public function nomeCampoDiretorioClassesEXTDAO(){ 

		return "diretorioClassesEXTDAO";

	}

	public function nomeCampoDiretorioForms(){ 

		return "diretorioForms";

	}

	public function nomeCampoDiretorioFiltros(){ 

		return "diretorioFiltros";

	}

	public function nomeCampoDiretorioLists(){ 

		return "diretorioLists";

	}

	public function nomeCampoDiretorioAjaxForms(){ 

		return "diretorioAjaxForms";

	}

	public function nomeCampoDiretorioAjaxPages(){ 

		return "diretorioAjaxPages";

	}

	public function nomeCampoColunasForms_INT(){ 

		return "colunasForms_INT";

	}

	public function nomeCampoChaveComNomeTabela(){ 

		return "chaveComNomeTabela";

	}

	public function nomeCampoUnderlineNomeClasse(){ 

		return "underlineNomeClasse";

	}

	public function nomeCampoStatus_INT(){ 

		return "status_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoConexaoBanco_INT($objArguments){

		$objArguments->nome = "conexaoBanco_INT";
		$objArguments->id = "conexaoBanco_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoNomeBanco($objArguments){

		$objArguments->nome = "nomeBanco";
		$objArguments->id = "nomeBanco";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioClassesDAO($objArguments){

		$objArguments->nome = "diretorioClassesDAO";
		$objArguments->id = "diretorioClassesDAO";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioClassesEXTDAO($objArguments){

		$objArguments->nome = "diretorioClassesEXTDAO";
		$objArguments->id = "diretorioClassesEXTDAO";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioForms($objArguments){

		$objArguments->nome = "diretorioForms";
		$objArguments->id = "diretorioForms";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioFiltros($objArguments){

		$objArguments->nome = "diretorioFiltros";
		$objArguments->id = "diretorioFiltros";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioLists($objArguments){

		$objArguments->nome = "diretorioLists";
		$objArguments->id = "diretorioLists";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioAjaxForms($objArguments){

		$objArguments->nome = "diretorioAjaxForms";
		$objArguments->id = "diretorioAjaxForms";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDiretorioAjaxPages($objArguments){

		$objArguments->nome = "diretorioAjaxPages";
		$objArguments->id = "diretorioAjaxPages";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoColunasForms_INT($objArguments){

		$objArguments->nome = "colunasForms_INT";
		$objArguments->id = "colunasForms_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoChaveComNomeTabela($objArguments){

		$objArguments->nome = "chaveComNomeTabela";
		$objArguments->id = "chaveComNomeTabela";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUnderlineNomeClasse($objArguments){

		$objArguments->nome = "underlineNomeClasse";
		$objArguments->id = "underlineNomeClasse";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoStatus_INT($objArguments){

		$objArguments->nome = "status_INT";
		$objArguments->id = "status_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
		if($this->conexaoBanco_INT == ""){

			$this->conexaoBanco_INT = "null";

		}

			$this->nomeBanco = $this->formatarDadosParaSQL($this->nomeBanco);
			$this->diretorioClassesDAO = $this->formatarDadosParaSQL($this->diretorioClassesDAO);
			$this->diretorioClassesEXTDAO = $this->formatarDadosParaSQL($this->diretorioClassesEXTDAO);
			$this->diretorioForms = $this->formatarDadosParaSQL($this->diretorioForms);
			$this->diretorioFiltros = $this->formatarDadosParaSQL($this->diretorioFiltros);
			$this->diretorioLists = $this->formatarDadosParaSQL($this->diretorioLists);
			$this->diretorioAjaxForms = $this->formatarDadosParaSQL($this->diretorioAjaxForms);
			$this->diretorioAjaxPages = $this->formatarDadosParaSQL($this->diretorioAjaxPages);
		if($this->colunasForms_INT == ""){

			$this->colunasForms_INT = "null";

		}

			$this->chaveComNomeTabela = $this->formatarDadosParaSQL($this->chaveComNomeTabela);
			$this->underlineNomeClasse = $this->formatarDadosParaSQL($this->underlineNomeClasse);
		if($this->status_INT == ""){

			$this->status_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->conexaoBanco_INT = null; 
		$this->nomeBanco = null; 
		$this->diretorioClassesDAO = null; 
		$this->diretorioClassesEXTDAO = null; 
		$this->diretorioForms = null; 
		$this->diretorioFiltros = null; 
		$this->diretorioLists = null; 
		$this->diretorioAjaxForms = null; 
		$this->diretorioAjaxPages = null; 
		$this->colunasForms_INT = null; 
		$this->chaveComNomeTabela = null; 
		$this->underlineNomeClasse = null; 
		$this->status_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("conexaoBanco_INT", $this->conexaoBanco_INT); 
		Helper::setSession("nomeBanco", $this->nomeBanco); 
		Helper::setSession("diretorioClassesDAO", $this->diretorioClassesDAO); 
		Helper::setSession("diretorioClassesEXTDAO", $this->diretorioClassesEXTDAO); 
		Helper::setSession("diretorioForms", $this->diretorioForms); 
		Helper::setSession("diretorioFiltros", $this->diretorioFiltros); 
		Helper::setSession("diretorioLists", $this->diretorioLists); 
		Helper::setSession("diretorioAjaxForms", $this->diretorioAjaxForms); 
		Helper::setSession("diretorioAjaxPages", $this->diretorioAjaxPages); 
		Helper::setSession("colunasForms_INT", $this->colunasForms_INT); 
		Helper::setSession("chaveComNomeTabela", $this->chaveComNomeTabela); 
		Helper::setSession("underlineNomeClasse", $this->underlineNomeClasse); 
		Helper::setSession("status_INT", $this->status_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("conexaoBanco_INT");
		Helper::clearSession("nomeBanco");
		Helper::clearSession("diretorioClassesDAO");
		Helper::clearSession("diretorioClassesEXTDAO");
		Helper::clearSession("diretorioForms");
		Helper::clearSession("diretorioFiltros");
		Helper::clearSession("diretorioLists");
		Helper::clearSession("diretorioAjaxForms");
		Helper::clearSession("diretorioAjaxPages");
		Helper::clearSession("colunasForms_INT");
		Helper::clearSession("chaveComNomeTabela");
		Helper::clearSession("underlineNomeClasse");
		Helper::clearSession("status_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->conexaoBanco_INT = Helper::SESSION("conexaoBanco_INT{$numReg}"); 
		$this->nomeBanco = Helper::SESSION("nomeBanco{$numReg}"); 
		$this->diretorioClassesDAO = Helper::SESSION("diretorioClassesDAO{$numReg}"); 
		$this->diretorioClassesEXTDAO = Helper::SESSION("diretorioClassesEXTDAO{$numReg}"); 
		$this->diretorioForms = Helper::SESSION("diretorioForms{$numReg}"); 
		$this->diretorioFiltros = Helper::SESSION("diretorioFiltros{$numReg}"); 
		$this->diretorioLists = Helper::SESSION("diretorioLists{$numReg}"); 
		$this->diretorioAjaxForms = Helper::SESSION("diretorioAjaxForms{$numReg}"); 
		$this->diretorioAjaxPages = Helper::SESSION("diretorioAjaxPages{$numReg}"); 
		$this->colunasForms_INT = Helper::SESSION("colunasForms_INT{$numReg}"); 
		$this->chaveComNomeTabela = Helper::SESSION("chaveComNomeTabela{$numReg}"); 
		$this->underlineNomeClasse = Helper::SESSION("underlineNomeClasse{$numReg}"); 
		$this->status_INT = Helper::SESSION("status_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->conexaoBanco_INT = Helper::POST("conexaoBanco_INT{$numReg}"); 
		$this->nomeBanco = Helper::POST("nomeBanco{$numReg}"); 
		$this->diretorioClassesDAO = Helper::POST("diretorioClassesDAO{$numReg}"); 
		$this->diretorioClassesEXTDAO = Helper::POST("diretorioClassesEXTDAO{$numReg}"); 
		$this->diretorioForms = Helper::POST("diretorioForms{$numReg}"); 
		$this->diretorioFiltros = Helper::POST("diretorioFiltros{$numReg}"); 
		$this->diretorioLists = Helper::POST("diretorioLists{$numReg}"); 
		$this->diretorioAjaxForms = Helper::POST("diretorioAjaxForms{$numReg}"); 
		$this->diretorioAjaxPages = Helper::POST("diretorioAjaxPages{$numReg}"); 
		$this->colunasForms_INT = Helper::POST("colunasForms_INT{$numReg}"); 
		$this->chaveComNomeTabela = Helper::POST("chaveComNomeTabela{$numReg}"); 
		$this->underlineNomeClasse = Helper::POST("underlineNomeClasse{$numReg}"); 
		$this->status_INT = Helper::POST("status_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->conexaoBanco_INT = Helper::GET("conexaoBanco_INT{$numReg}"); 
		$this->nomeBanco = Helper::GET("nomeBanco{$numReg}"); 
		$this->diretorioClassesDAO = Helper::GET("diretorioClassesDAO{$numReg}"); 
		$this->diretorioClassesEXTDAO = Helper::GET("diretorioClassesEXTDAO{$numReg}"); 
		$this->diretorioForms = Helper::GET("diretorioForms{$numReg}"); 
		$this->diretorioFiltros = Helper::GET("diretorioFiltros{$numReg}"); 
		$this->diretorioLists = Helper::GET("diretorioLists{$numReg}"); 
		$this->diretorioAjaxForms = Helper::GET("diretorioAjaxForms{$numReg}"); 
		$this->diretorioAjaxPages = Helper::GET("diretorioAjaxPages{$numReg}"); 
		$this->colunasForms_INT = Helper::GET("colunasForms_INT{$numReg}"); 
		$this->chaveComNomeTabela = Helper::GET("chaveComNomeTabela{$numReg}"); 
		$this->underlineNomeClasse = Helper::GET("underlineNomeClasse{$numReg}"); 
		$this->status_INT = Helper::GET("status_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["conexaoBanco_INT{$numReg}"]) || $tipo == null){

		$upd.= "conexaoBanco_INT = $this->conexaoBanco_INT, ";

	} 

	if(isset($tipo["nomeBanco{$numReg}"]) || $tipo == null){

		$upd.= "nomeBanco = $this->nomeBanco, ";

	} 

	if(isset($tipo["diretorioClassesDAO{$numReg}"]) || $tipo == null){

		$upd.= "diretorioClassesDAO = $this->diretorioClassesDAO, ";

	} 

	if(isset($tipo["diretorioClassesEXTDAO{$numReg}"]) || $tipo == null){

		$upd.= "diretorioClassesEXTDAO = $this->diretorioClassesEXTDAO, ";

	} 

	if(isset($tipo["diretorioForms{$numReg}"]) || $tipo == null){

		$upd.= "diretorioForms = $this->diretorioForms, ";

	} 

	if(isset($tipo["diretorioFiltros{$numReg}"]) || $tipo == null){

		$upd.= "diretorioFiltros = $this->diretorioFiltros, ";

	} 

	if(isset($tipo["diretorioLists{$numReg}"]) || $tipo == null){

		$upd.= "diretorioLists = $this->diretorioLists, ";

	} 

	if(isset($tipo["diretorioAjaxForms{$numReg}"]) || $tipo == null){

		$upd.= "diretorioAjaxForms = $this->diretorioAjaxForms, ";

	} 

	if(isset($tipo["diretorioAjaxPages{$numReg}"]) || $tipo == null){

		$upd.= "diretorioAjaxPages = $this->diretorioAjaxPages, ";

	} 

	if(isset($tipo["colunasForms_INT{$numReg}"]) || $tipo == null){

		$upd.= "colunasForms_INT = $this->colunasForms_INT, ";

	} 

	if(isset($tipo["chaveComNomeTabela{$numReg}"]) || $tipo == null){

		$upd.= "chaveComNomeTabela = $this->chaveComNomeTabela, ";

	} 

	if(isset($tipo["underlineNomeClasse{$numReg}"]) || $tipo == null){

		$upd.= "underlineNomeClasse = $this->underlineNomeClasse, ";

	} 

	if(isset($tipo["status_INT{$numReg}"]) || $tipo == null){

		$upd.= "status_INT = $this->status_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    