<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_versao_log_erro_script
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_versao_log_erro_script.php
    * TABELA MYSQL:    projetos_versao_log_erro_script
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_versao_log_erro_script extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $script_comando_banco_id_INT;
	public $obj;
	public $projetos_versao_log_erro_id_INT;
	public $objScript_comando_banco;


    public $nomeEntidade;



    

	public $label_id;
	public $label_script_comando_banco_id_INT;
	public $label_projetos_versao_log_erro_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_versao_log_erro_script";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjScript_comando_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Script_comando_banco($this->getDatabase());
		if($this->script_comando_banco_id_INT != null) 
		$this->obj->select($this->script_comando_banco_id_INT);
	}
	return $this->obj ;
}
function getFkObjProjetos_versao_log_erro(){
	if($this->objScript_comando_banco ==null){
		$this->objScript_comando_banco = new EXTDAO_Projetos_versao_log_erro($this->getDatabase());
		if($this->projetos_versao_log_erro_id_INT != null) 
		$this->objScript_comando_banco->select($this->projetos_versao_log_erro_id_INT);
	}
	return $this->objScript_comando_banco ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllScript_comando_banco($objArgumentos){

		$objArgumentos->nome="script_comando_banco_id_INT";
		$objArgumentos->id="script_comando_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjScript_comando_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_versao_log_erro($objArgumentos){

		$objArgumentos->nome="projetos_versao_log_erro_id_INT";
		$objArgumentos->id="projetos_versao_log_erro_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_log_erro()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_versao_log_erro_script", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_versao_log_erro_script", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getScript_comando_banco_id_INT()
    {
    	return $this->script_comando_banco_id_INT;
    }
    
    public function getProjetos_versao_log_erro_id_INT()
    {
    	return $this->projetos_versao_log_erro_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setScript_comando_banco_id_INT($val)
    {
    	$this->script_comando_banco_id_INT =  $val;
    }
    
    function setProjetos_versao_log_erro_id_INT($val)
    {
    	$this->projetos_versao_log_erro_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM projetos_versao_log_erro_script WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->script_comando_banco_id_INT = $row->script_comando_banco_id_INT;
        if(isset($this->objScript_comando_banco))
			$this->objScript_comando_banco->select($this->script_comando_banco_id_INT);

        $this->projetos_versao_log_erro_id_INT = $row->projetos_versao_log_erro_id_INT;
        if(isset($this->objProjetos_versao_log_erro))
			$this->objProjetos_versao_log_erro->select($this->projetos_versao_log_erro_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_versao_log_erro_script WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_versao_log_erro_script ( script_comando_banco_id_INT , projetos_versao_log_erro_id_INT ) VALUES ( {$this->script_comando_banco_id_INT} , {$this->projetos_versao_log_erro_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoScript_comando_banco_id_INT(){ 

		return "script_comando_banco_id_INT";

	}

	public function nomeCampoProjetos_versao_log_erro_id_INT(){ 

		return "projetos_versao_log_erro_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoScript_comando_banco_id_INT($objArguments){

		$objArguments->nome = "script_comando_banco_id_INT";
		$objArguments->id = "script_comando_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_versao_log_erro_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_log_erro_id_INT";
		$objArguments->id = "projetos_versao_log_erro_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->script_comando_banco_id_INT == ""){

			$this->script_comando_banco_id_INT = "null";

		}

		if($this->projetos_versao_log_erro_id_INT == ""){

			$this->projetos_versao_log_erro_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->script_comando_banco_id_INT = null; 
		$this->objScript_comando_banco= null;
		$this->projetos_versao_log_erro_id_INT = null; 
		$this->objProjetos_versao_log_erro= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("script_comando_banco_id_INT", $this->script_comando_banco_id_INT); 
		Helper::setSession("projetos_versao_log_erro_id_INT", $this->projetos_versao_log_erro_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("script_comando_banco_id_INT");
		Helper::clearSession("projetos_versao_log_erro_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::SESSION("script_comando_banco_id_INT{$numReg}"); 
		$this->projetos_versao_log_erro_id_INT = Helper::SESSION("projetos_versao_log_erro_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::POST("script_comando_banco_id_INT{$numReg}"); 
		$this->projetos_versao_log_erro_id_INT = Helper::POST("projetos_versao_log_erro_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->script_comando_banco_id_INT = Helper::GET("script_comando_banco_id_INT{$numReg}"); 
		$this->projetos_versao_log_erro_id_INT = Helper::GET("projetos_versao_log_erro_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["script_comando_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "script_comando_banco_id_INT = $this->script_comando_banco_id_INT, ";

	} 

	if(isset($tipo["projetos_versao_log_erro_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_log_erro_id_INT = $this->projetos_versao_log_erro_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_versao_log_erro_script SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    