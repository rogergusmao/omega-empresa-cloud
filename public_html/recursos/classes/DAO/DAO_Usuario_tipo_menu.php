<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Usuario_tipo_menu
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Usuario_tipo_menu.php
    * TABELA MYSQL:    usuario_tipo_menu
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Usuario_tipo_menu extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $usuario_tipo_id_INT;
	public $obj;
	public $area_menu;


    public $nomeEntidade;



    

	public $label_id;
	public $label_usuario_tipo_id_INT;
	public $label_area_menu;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "usuario_tipo_menu";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjUsuario_tipo(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Usuario_tipo($this->getDatabase());
		if($this->usuario_tipo_id_INT != null) 
		$this->obj->select($this->usuario_tipo_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllUsuario_tipo($objArgumentos){

		$objArgumentos->nome="usuario_tipo_id_INT";
		$objArgumentos->id="usuario_tipo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario_tipo()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_usuario_tipo_menu", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_usuario_tipo_menu", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getUsuario_tipo_id_INT()
    {
    	return $this->usuario_tipo_id_INT;
    }
    
    public function getArea_menu()
    {
    	return $this->area_menu;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setUsuario_tipo_id_INT($val)
    {
    	$this->usuario_tipo_id_INT =  $val;
    }
    
    function setArea_menu($val)
    {
    	$this->area_menu =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM usuario_tipo_menu WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->usuario_tipo_id_INT = $row->usuario_tipo_id_INT;
        if(isset($this->objUsuario_tipo))
			$this->objUsuario_tipo->select($this->usuario_tipo_id_INT);

        $this->area_menu = $row->area_menu;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM usuario_tipo_menu WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO usuario_tipo_menu ( usuario_tipo_id_INT , area_menu ) VALUES ( {$this->usuario_tipo_id_INT} , {$this->area_menu} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoUsuario_tipo_id_INT(){ 

		return "usuario_tipo_id_INT";

	}

	public function nomeCampoArea_menu(){ 

		return "area_menu";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoUsuario_tipo_id_INT($objArguments){

		$objArguments->nome = "usuario_tipo_id_INT";
		$objArguments->id = "usuario_tipo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoArea_menu($objArguments){

		$objArguments->nome = "area_menu";
		$objArguments->id = "area_menu";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->usuario_tipo_id_INT == ""){

			$this->usuario_tipo_id_INT = "null";

		}

			$this->area_menu = $this->formatarDadosParaSQL($this->area_menu);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->usuario_tipo_id_INT = null; 
		$this->objUsuario_tipo= null;
		$this->area_menu = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("usuario_tipo_id_INT", $this->usuario_tipo_id_INT); 
		Helper::setSession("area_menu", $this->area_menu); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("usuario_tipo_id_INT");
		Helper::clearSession("area_menu");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->usuario_tipo_id_INT = Helper::SESSION("usuario_tipo_id_INT{$numReg}"); 
		$this->area_menu = Helper::SESSION("area_menu{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->usuario_tipo_id_INT = Helper::POST("usuario_tipo_id_INT{$numReg}"); 
		$this->area_menu = Helper::POST("area_menu{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->usuario_tipo_id_INT = Helper::GET("usuario_tipo_id_INT{$numReg}"); 
		$this->area_menu = Helper::GET("area_menu{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["usuario_tipo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_tipo_id_INT = $this->usuario_tipo_id_INT, ";

	} 

	if(isset($tipo["area_menu{$numReg}"]) || $tipo == null){

		$upd.= "area_menu = $this->area_menu, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE usuario_tipo_menu SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    