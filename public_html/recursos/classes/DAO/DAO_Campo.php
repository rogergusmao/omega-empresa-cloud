<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Campo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Campo.php
    * TABELA MYSQL:    campo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Campo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $descricao;
	public $lista_id_INT;
	public $obj;
	public $tipo_campo_id_INT;
	public $objLista;
	public $projetos_vesao_id_INT;
	public $objTipo_campo;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_descricao;
	public $label_lista_id_INT;
	public $label_tipo_campo_id_INT;
	public $label_projetos_vesao_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "campo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjLista(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Lista($this->getDatabase());
		if($this->lista_id_INT != null) 
		$this->obj->select($this->lista_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_campo(){
	if($this->objLista ==null){
		$this->objLista = new EXTDAO_Tipo_campo($this->getDatabase());
		if($this->tipo_campo_id_INT != null) 
		$this->objLista->select($this->tipo_campo_id_INT);
	}
	return $this->objLista ;
}
function getFkObjProjetos_vesao(){
	if($this->objTipo_campo ==null){
		$this->objTipo_campo = new EXTDAO_Projetos($this->getDatabase());
		if($this->projetos_vesao_id_INT != null) 
		$this->objTipo_campo->select($this->projetos_vesao_id_INT);
	}
	return $this->objTipo_campo ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllLista($objArgumentos){

		$objArgumentos->nome="lista_id_INT";
		$objArgumentos->id="lista_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjLista()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_campo($objArgumentos){

		$objArgumentos->nome="tipo_campo_id_INT";
		$objArgumentos->id="tipo_campo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_campo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProjetos_vesao($objArgumentos){

		$objArgumentos->nome="projetos_vesao_id_INT";
		$objArgumentos->id="projetos_vesao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_vesao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_campo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_campo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getDescricao()
    {
    	return $this->descricao;
    }
    
    public function getLista_id_INT()
    {
    	return $this->lista_id_INT;
    }
    
    public function getTipo_campo_id_INT()
    {
    	return $this->tipo_campo_id_INT;
    }
    
    public function getProjetos_vesao_id_INT()
    {
    	return $this->projetos_vesao_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setDescricao($val)
    {
    	$this->descricao =  $val;
    }
    
    function setLista_id_INT($val)
    {
    	$this->lista_id_INT =  $val;
    }
    
    function setTipo_campo_id_INT($val)
    {
    	$this->tipo_campo_id_INT =  $val;
    }
    
    function setProjetos_vesao_id_INT($val)
    {
    	$this->projetos_vesao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM campo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->descricao = $row->descricao;
        
        $this->lista_id_INT = $row->lista_id_INT;
        if(isset($this->objLista))
			$this->objLista->select($this->lista_id_INT);

        $this->tipo_campo_id_INT = $row->tipo_campo_id_INT;
        if(isset($this->objTipo_campo))
			$this->objTipo_campo->select($this->tipo_campo_id_INT);

        $this->projetos_vesao_id_INT = $row->projetos_vesao_id_INT;
        if(isset($this->objProjetos_vesao))
			$this->objProjetos_vesao->select($this->projetos_vesao_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM campo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	

    	$sql = "INSERT INTO campo ( id , nome , descricao , lista_id_INT , tipo_campo_id_INT , projetos_vesao_id_INT ) VALUES ( {$this->id} , {$this->nome} , {$this->descricao} , {$this->lista_id_INT} , {$this->tipo_campo_id_INT} , {$this->projetos_vesao_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoDescricao(){ 

		return "descricao";

	}

	public function nomeCampoLista_id_INT(){ 

		return "lista_id_INT";

	}

	public function nomeCampoTipo_campo_id_INT(){ 

		return "tipo_campo_id_INT";

	}

	public function nomeCampoProjetos_vesao_id_INT(){ 

		return "projetos_vesao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDescricao($objArguments){

		$objArguments->nome = "descricao";
		$objArguments->id = "descricao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoLista_id_INT($objArguments){

		$objArguments->nome = "lista_id_INT";
		$objArguments->id = "lista_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_campo_id_INT($objArguments){

		$objArguments->nome = "tipo_campo_id_INT";
		$objArguments->id = "tipo_campo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProjetos_vesao_id_INT($objArguments){

		$objArguments->nome = "projetos_vesao_id_INT";
		$objArguments->id = "projetos_vesao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
			$this->descricao = $this->formatarDadosParaSQL($this->descricao);
		if($this->lista_id_INT == ""){

			$this->lista_id_INT = "null";

		}

		if($this->tipo_campo_id_INT == ""){

			$this->tipo_campo_id_INT = "null";

		}

		if($this->projetos_vesao_id_INT == ""){

			$this->projetos_vesao_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->descricao = null; 
		$this->lista_id_INT = null; 
		$this->objLista= null;
		$this->tipo_campo_id_INT = null; 
		$this->objTipo_campo= null;
		$this->projetos_vesao_id_INT = null; 
		$this->objProjetos_vesao= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("descricao", $this->descricao); 
		Helper::setSession("lista_id_INT", $this->lista_id_INT); 
		Helper::setSession("tipo_campo_id_INT", $this->tipo_campo_id_INT); 
		Helper::setSession("projetos_vesao_id_INT", $this->projetos_vesao_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("descricao");
		Helper::clearSession("lista_id_INT");
		Helper::clearSession("tipo_campo_id_INT");
		Helper::clearSession("projetos_vesao_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->descricao = Helper::SESSION("descricao{$numReg}"); 
		$this->lista_id_INT = Helper::SESSION("lista_id_INT{$numReg}"); 
		$this->tipo_campo_id_INT = Helper::SESSION("tipo_campo_id_INT{$numReg}"); 
		$this->projetos_vesao_id_INT = Helper::SESSION("projetos_vesao_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->descricao = Helper::POST("descricao{$numReg}"); 
		$this->lista_id_INT = Helper::POST("lista_id_INT{$numReg}"); 
		$this->tipo_campo_id_INT = Helper::POST("tipo_campo_id_INT{$numReg}"); 
		$this->projetos_vesao_id_INT = Helper::POST("projetos_vesao_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->descricao = Helper::GET("descricao{$numReg}"); 
		$this->lista_id_INT = Helper::GET("lista_id_INT{$numReg}"); 
		$this->tipo_campo_id_INT = Helper::GET("tipo_campo_id_INT{$numReg}"); 
		$this->projetos_vesao_id_INT = Helper::GET("projetos_vesao_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["descricao{$numReg}"]) || $tipo == null){

		$upd.= "descricao = $this->descricao, ";

	} 

	if(isset($tipo["lista_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "lista_id_INT = $this->lista_id_INT, ";

	} 

	if(isset($tipo["tipo_campo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_campo_id_INT = $this->tipo_campo_id_INT, ";

	} 

	if(isset($tipo["projetos_vesao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_vesao_id_INT = $this->projetos_vesao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE campo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    