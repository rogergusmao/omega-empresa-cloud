<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_registro_sincronizador_android_para_web
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_registro_sincronizador_android_para_web.php
    * TABELA MYSQL:    sistema_registro_sincronizador_android_para_web
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_registro_sincronizador_android_para_web extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_tabela_id_INT;
	public $obj;
	public $id_tabela_INT;
	public $sistema_tipo_operacao_id_INT;
	public $objSistema_tabela;
	public $usuario_id_INT;
	public $objSistema_tipo_operacao;
	public $corporacao_id_INT;
	public $objUsuario;
	public $imei;
	public $sistema_produto_INT;
	public $sistema_projetos_versao_INT;


    public $nomeEntidade;



    

	public $label_id;
	public $label_sistema_tabela_id_INT;
	public $label_id_tabela_INT;
	public $label_sistema_tipo_operacao_id_INT;
	public $label_usuario_id_INT;
	public $label_corporacao_id_INT;
	public $label_imei;
	public $label_sistema_produto_INT;
	public $label_sistema_projetos_versao_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_registro_sincronizador_android_para_web";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_tabela(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema($this->getDatabase());
		if($this->sistema_tabela_id_INT != null) 
		$this->obj->select($this->sistema_tabela_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_tipo_operacao(){
	if($this->objSistema_tabela ==null){
		$this->objSistema_tabela = new EXTDAO_Sistema($this->getDatabase());
		if($this->sistema_tipo_operacao_id_INT != null) 
		$this->objSistema_tabela->select($this->sistema_tipo_operacao_id_INT);
	}
	return $this->objSistema_tabela ;
}
function getFkObjUsuario(){
	if($this->objSistema_tipo_operacao ==null){
		$this->objSistema_tipo_operacao = new EXTDAO_Usuario($this->getDatabase());
		if($this->usuario_id_INT != null) 
		$this->objSistema_tipo_operacao->select($this->usuario_id_INT);
	}
	return $this->objSistema_tipo_operacao ;
}
function getFkObjCorporacao(){
	if($this->objUsuario ==null){
		$this->objUsuario = new EXTDAO_Corporacao($this->getDatabase());
		if($this->corporacao_id_INT != null) 
		$this->objUsuario->select($this->corporacao_id_INT);
	}
	return $this->objUsuario ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_tabela($objArgumentos){

		$objArgumentos->nome="sistema_tabela_id_INT";
		$objArgumentos->id="sistema_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_tipo_operacao($objArgumentos){

		$objArgumentos->nome="sistema_tipo_operacao_id_INT";
		$objArgumentos->id="sistema_tipo_operacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_tipo_operacao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllUsuario($objArgumentos){

		$objArgumentos->nome="usuario_id_INT";
		$objArgumentos->id="usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCorporacao($objArgumentos){

		$objArgumentos->nome="corporacao_id_INT";
		$objArgumentos->id="corporacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCorporacao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_registro_sincronizador_android_para_web", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_registro_sincronizador_android_para_web", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_tabela_id_INT()
    {
    	return $this->sistema_tabela_id_INT;
    }
    
    public function getId_tabela_INT()
    {
    	return $this->id_tabela_INT;
    }
    
    public function getSistema_tipo_operacao_id_INT()
    {
    	return $this->sistema_tipo_operacao_id_INT;
    }
    
    public function getUsuario_id_INT()
    {
    	return $this->usuario_id_INT;
    }
    
    public function getCorporacao_id_INT()
    {
    	return $this->corporacao_id_INT;
    }
    
    public function getImei()
    {
    	return $this->imei;
    }
    
    public function getSistema_produto_INT()
    {
    	return $this->sistema_produto_INT;
    }
    
    public function getSistema_projetos_versao_INT()
    {
    	return $this->sistema_projetos_versao_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_tabela_id_INT($val)
    {
    	$this->sistema_tabela_id_INT =  $val;
    }
    
    function setId_tabela_INT($val)
    {
    	$this->id_tabela_INT =  $val;
    }
    
    function setSistema_tipo_operacao_id_INT($val)
    {
    	$this->sistema_tipo_operacao_id_INT =  $val;
    }
    
    function setUsuario_id_INT($val)
    {
    	$this->usuario_id_INT =  $val;
    }
    
    function setCorporacao_id_INT($val)
    {
    	$this->corporacao_id_INT =  $val;
    }
    
    function setImei($val)
    {
    	$this->imei =  $val;
    }
    
    function setSistema_produto_INT($val)
    {
    	$this->sistema_produto_INT =  $val;
    }
    
    function setSistema_projetos_versao_INT($val)
    {
    	$this->sistema_projetos_versao_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema_registro_sincronizador_android_para_web WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->sistema_tabela_id_INT = $row->sistema_tabela_id_INT;
        if(isset($this->objSistema_tabela))
			$this->objSistema_tabela->select($this->sistema_tabela_id_INT);

        $this->id_tabela_INT = $row->id_tabela_INT;
        
        $this->sistema_tipo_operacao_id_INT = $row->sistema_tipo_operacao_id_INT;
        if(isset($this->objSistema_tipo_operacao))
			$this->objSistema_tipo_operacao->select($this->sistema_tipo_operacao_id_INT);

        $this->usuario_id_INT = $row->usuario_id_INT;
        if(isset($this->objUsuario))
			$this->objUsuario->select($this->usuario_id_INT);

        $this->corporacao_id_INT = $row->corporacao_id_INT;
        if(isset($this->objCorporacao))
			$this->objCorporacao->select($this->corporacao_id_INT);

        $this->imei = $row->imei;
        
        $this->sistema_produto_INT = $row->sistema_produto_INT;
        
        $this->sistema_projetos_versao_INT = $row->sistema_projetos_versao_INT;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_registro_sincronizador_android_para_web WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	

    	$sql = "INSERT INTO sistema_registro_sincronizador_android_para_web ( id , sistema_tabela_id_INT , id_tabela_INT , sistema_tipo_operacao_id_INT , usuario_id_INT , corporacao_id_INT , imei , sistema_produto_INT , sistema_projetos_versao_INT ) VALUES ( {$this->id} , {$this->sistema_tabela_id_INT} , {$this->id_tabela_INT} , {$this->sistema_tipo_operacao_id_INT} , {$this->usuario_id_INT} , {$this->corporacao_id_INT} , {$this->imei} , {$this->sistema_produto_INT} , {$this->sistema_projetos_versao_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_tabela_id_INT(){ 

		return "sistema_tabela_id_INT";

	}

	public function nomeCampoId_tabela_INT(){ 

		return "id_tabela_INT";

	}

	public function nomeCampoSistema_tipo_operacao_id_INT(){ 

		return "sistema_tipo_operacao_id_INT";

	}

	public function nomeCampoUsuario_id_INT(){ 

		return "usuario_id_INT";

	}

	public function nomeCampoCorporacao_id_INT(){ 

		return "corporacao_id_INT";

	}

	public function nomeCampoImei(){ 

		return "imei";

	}

	public function nomeCampoSistema_produto_INT(){ 

		return "sistema_produto_INT";

	}

	public function nomeCampoSistema_projetos_versao_INT(){ 

		return "sistema_projetos_versao_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_tabela_id_INT($objArguments){

		$objArguments->nome = "sistema_tabela_id_INT";
		$objArguments->id = "sistema_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_tabela_INT($objArguments){

		$objArguments->nome = "id_tabela_INT";
		$objArguments->id = "id_tabela_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_tipo_operacao_id_INT($objArguments){

		$objArguments->nome = "sistema_tipo_operacao_id_INT";
		$objArguments->id = "sistema_tipo_operacao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUsuario_id_INT($objArguments){

		$objArguments->nome = "usuario_id_INT";
		$objArguments->id = "usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorporacao_id_INT($objArguments){

		$objArguments->nome = "corporacao_id_INT";
		$objArguments->id = "corporacao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoImei($objArguments){

		$objArguments->nome = "imei";
		$objArguments->id = "imei";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSistema_produto_INT($objArguments){

		$objArguments->nome = "sistema_produto_INT";
		$objArguments->id = "sistema_produto_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_INT";
		$objArguments->id = "sistema_projetos_versao_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_tabela_id_INT == ""){

			$this->sistema_tabela_id_INT = "null";

		}

		if($this->id_tabela_INT == ""){

			$this->id_tabela_INT = "null";

		}

		if($this->sistema_tipo_operacao_id_INT == ""){

			$this->sistema_tipo_operacao_id_INT = "null";

		}

		if($this->usuario_id_INT == ""){

			$this->usuario_id_INT = "null";

		}

		if($this->corporacao_id_INT == ""){

			$this->corporacao_id_INT = "null";

		}

			$this->imei = $this->formatarDadosParaSQL($this->imei);
		if($this->sistema_produto_INT == ""){

			$this->sistema_produto_INT = "null";

		}

		if($this->sistema_projetos_versao_INT == ""){

			$this->sistema_projetos_versao_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->sistema_tabela_id_INT = null; 
		$this->objSistema_tabela= null;
		$this->id_tabela_INT = null; 
		$this->sistema_tipo_operacao_id_INT = null; 
		$this->objSistema_tipo_operacao= null;
		$this->usuario_id_INT = null; 
		$this->objUsuario= null;
		$this->corporacao_id_INT = null; 
		$this->objCorporacao= null;
		$this->imei = null; 
		$this->sistema_produto_INT = null; 
		$this->sistema_projetos_versao_INT = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("sistema_tabela_id_INT", $this->sistema_tabela_id_INT); 
		Helper::setSession("id_tabela_INT", $this->id_tabela_INT); 
		Helper::setSession("sistema_tipo_operacao_id_INT", $this->sistema_tipo_operacao_id_INT); 
		Helper::setSession("usuario_id_INT", $this->usuario_id_INT); 
		Helper::setSession("corporacao_id_INT", $this->corporacao_id_INT); 
		Helper::setSession("imei", $this->imei); 
		Helper::setSession("sistema_produto_INT", $this->sistema_produto_INT); 
		Helper::setSession("sistema_projetos_versao_INT", $this->sistema_projetos_versao_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("sistema_tabela_id_INT");
		Helper::clearSession("id_tabela_INT");
		Helper::clearSession("sistema_tipo_operacao_id_INT");
		Helper::clearSession("usuario_id_INT");
		Helper::clearSession("corporacao_id_INT");
		Helper::clearSession("imei");
		Helper::clearSession("sistema_produto_INT");
		Helper::clearSession("sistema_projetos_versao_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->sistema_tabela_id_INT = Helper::SESSION("sistema_tabela_id_INT{$numReg}"); 
		$this->id_tabela_INT = Helper::SESSION("id_tabela_INT{$numReg}"); 
		$this->sistema_tipo_operacao_id_INT = Helper::SESSION("sistema_tipo_operacao_id_INT{$numReg}"); 
		$this->usuario_id_INT = Helper::SESSION("usuario_id_INT{$numReg}"); 
		$this->corporacao_id_INT = Helper::SESSION("corporacao_id_INT{$numReg}"); 
		$this->imei = Helper::SESSION("imei{$numReg}"); 
		$this->sistema_produto_INT = Helper::SESSION("sistema_produto_INT{$numReg}"); 
		$this->sistema_projetos_versao_INT = Helper::SESSION("sistema_projetos_versao_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->sistema_tabela_id_INT = Helper::POST("sistema_tabela_id_INT{$numReg}"); 
		$this->id_tabela_INT = Helper::POST("id_tabela_INT{$numReg}"); 
		$this->sistema_tipo_operacao_id_INT = Helper::POST("sistema_tipo_operacao_id_INT{$numReg}"); 
		$this->usuario_id_INT = Helper::POST("usuario_id_INT{$numReg}"); 
		$this->corporacao_id_INT = Helper::POST("corporacao_id_INT{$numReg}"); 
		$this->imei = Helper::POST("imei{$numReg}"); 
		$this->sistema_produto_INT = Helper::POST("sistema_produto_INT{$numReg}"); 
		$this->sistema_projetos_versao_INT = Helper::POST("sistema_projetos_versao_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->sistema_tabela_id_INT = Helper::GET("sistema_tabela_id_INT{$numReg}"); 
		$this->id_tabela_INT = Helper::GET("id_tabela_INT{$numReg}"); 
		$this->sistema_tipo_operacao_id_INT = Helper::GET("sistema_tipo_operacao_id_INT{$numReg}"); 
		$this->usuario_id_INT = Helper::GET("usuario_id_INT{$numReg}"); 
		$this->corporacao_id_INT = Helper::GET("corporacao_id_INT{$numReg}"); 
		$this->imei = Helper::GET("imei{$numReg}"); 
		$this->sistema_produto_INT = Helper::GET("sistema_produto_INT{$numReg}"); 
		$this->sistema_projetos_versao_INT = Helper::GET("sistema_projetos_versao_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_tabela_id_INT = $this->sistema_tabela_id_INT, ";

	} 

	if(isset($tipo["id_tabela_INT{$numReg}"]) || $tipo == null){

		$upd.= "id_tabela_INT = $this->id_tabela_INT, ";

	} 

	if(isset($tipo["sistema_tipo_operacao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_tipo_operacao_id_INT = $this->sistema_tipo_operacao_id_INT, ";

	} 

	if(isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_id_INT = $this->usuario_id_INT, ";

	} 

	if(isset($tipo["corporacao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "corporacao_id_INT = $this->corporacao_id_INT, ";

	} 

	if(isset($tipo["imei{$numReg}"]) || $tipo == null){

		$upd.= "imei = $this->imei, ";

	} 

	if(isset($tipo["sistema_produto_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_produto_INT = $this->sistema_produto_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_INT = $this->sistema_projetos_versao_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_registro_sincronizador_android_para_web SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    