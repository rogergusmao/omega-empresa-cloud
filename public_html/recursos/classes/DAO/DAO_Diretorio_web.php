<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Diretorio_web
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Diretorio_web.php
    * TABELA MYSQL:    diretorio_web
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Diretorio_web extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $projetos_versao_banco_banco_id_INT;
	public $obj;
	public $raiz;
	public $DAO;
	public $EXTDAO;
	public $forms;
	public $filtros;
	public $lists;
	public $ajaxForms;
	public $ajaxPages;
	public $classes;


    public $nomeEntidade;



    

	public $label_id;
	public $label_projetos_versao_banco_banco_id_INT;
	public $label_raiz;
	public $label_DAO;
	public $label_EXTDAO;
	public $label_forms;
	public $label_filtros;
	public $label_lists;
	public $label_ajaxForms;
	public $label_ajaxPages;
	public $label_classes;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "diretorio_web";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_versao_banco_banco(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->projetos_versao_banco_banco_id_INT != null) 
		$this->obj->select($this->projetos_versao_banco_banco_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao_banco_banco()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_diretorio_web", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_diretorio_web", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getProjetos_versao_banco_banco_id_INT()
    {
    	return $this->projetos_versao_banco_banco_id_INT;
    }
    
    public function getRaiz()
    {
    	return $this->raiz;
    }
    
    public function getDAO()
    {
    	return $this->DAO;
    }
    
    public function getEXTDAO()
    {
    	return $this->EXTDAO;
    }
    
    public function getForms()
    {
    	return $this->forms;
    }
    
    public function getFiltros()
    {
    	return $this->filtros;
    }
    
    public function getLists()
    {
    	return $this->lists;
    }
    
    public function getAjaxForms()
    {
    	return $this->ajaxForms;
    }
    
    public function getAjaxPages()
    {
    	return $this->ajaxPages;
    }
    
    public function getClasses()
    {
    	return $this->classes;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setProjetos_versao_banco_banco_id_INT($val)
    {
    	$this->projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setRaiz($val)
    {
    	$this->raiz =  $val;
    }
    
    function setDAO($val)
    {
    	$this->DAO =  $val;
    }
    
    function setEXTDAO($val)
    {
    	$this->EXTDAO =  $val;
    }
    
    function setForms($val)
    {
    	$this->forms =  $val;
    }
    
    function setFiltros($val)
    {
    	$this->filtros =  $val;
    }
    
    function setLists($val)
    {
    	$this->lists =  $val;
    }
    
    function setAjaxForms($val)
    {
    	$this->ajaxForms =  $val;
    }
    
    function setAjaxPages($val)
    {
    	$this->ajaxPages =  $val;
    }
    
    function setClasses($val)
    {
    	$this->classes =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM diretorio_web WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->projetos_versao_banco_banco_id_INT = $row->projetos_versao_banco_banco_id_INT;
        if(isset($this->objProjetos_versao_banco_banco))
			$this->objProjetos_versao_banco_banco->select($this->projetos_versao_banco_banco_id_INT);

        $this->raiz = $row->raiz;
        
        $this->DAO = $row->DAO;
        
        $this->EXTDAO = $row->EXTDAO;
        
        $this->forms = $row->forms;
        
        $this->filtros = $row->filtros;
        
        $this->lists = $row->lists;
        
        $this->ajaxForms = $row->ajaxForms;
        
        $this->ajaxPages = $row->ajaxPages;
        
        $this->classes = $row->classes;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM diretorio_web WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO diretorio_web ( projetos_versao_banco_banco_id_INT , raiz , DAO , EXTDAO , forms , filtros , lists , ajaxForms , ajaxPages , classes ) VALUES ( {$this->projetos_versao_banco_banco_id_INT} , {$this->raiz} , {$this->DAO} , {$this->EXTDAO} , {$this->forms} , {$this->filtros} , {$this->lists} , {$this->ajaxForms} , {$this->ajaxPages} , {$this->classes} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoProjetos_versao_banco_banco_id_INT(){ 

		return "projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoRaiz(){ 

		return "raiz";

	}

	public function nomeCampoDAO(){ 

		return "DAO";

	}

	public function nomeCampoEXTDAO(){ 

		return "EXTDAO";

	}

	public function nomeCampoForms(){ 

		return "forms";

	}

	public function nomeCampoFiltros(){ 

		return "filtros";

	}

	public function nomeCampoLists(){ 

		return "lists";

	}

	public function nomeCampoAjaxForms(){ 

		return "ajaxForms";

	}

	public function nomeCampoAjaxPages(){ 

		return "ajaxPages";

	}

	public function nomeCampoClasses(){ 

		return "classes";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoProjetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_banco_banco_id_INT";
		$objArguments->id = "projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoRaiz($objArguments){

		$objArguments->nome = "raiz";
		$objArguments->id = "raiz";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDAO($objArguments){

		$objArguments->nome = "DAO";
		$objArguments->id = "DAO";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoEXTDAO($objArguments){

		$objArguments->nome = "EXTDAO";
		$objArguments->id = "EXTDAO";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoForms($objArguments){

		$objArguments->nome = "forms";
		$objArguments->id = "forms";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoFiltros($objArguments){

		$objArguments->nome = "filtros";
		$objArguments->id = "filtros";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoLists($objArguments){

		$objArguments->nome = "lists";
		$objArguments->id = "lists";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoAjaxForms($objArguments){

		$objArguments->nome = "ajaxForms";
		$objArguments->id = "ajaxForms";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoAjaxPages($objArguments){

		$objArguments->nome = "ajaxPages";
		$objArguments->id = "ajaxPages";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoClasses($objArguments){

		$objArguments->nome = "classes";
		$objArguments->id = "classes";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->projetos_versao_banco_banco_id_INT == ""){

			$this->projetos_versao_banco_banco_id_INT = "null";

		}

			$this->raiz = $this->formatarDadosParaSQL($this->raiz);
			$this->DAO = $this->formatarDadosParaSQL($this->DAO);
			$this->EXTDAO = $this->formatarDadosParaSQL($this->EXTDAO);
			$this->forms = $this->formatarDadosParaSQL($this->forms);
			$this->filtros = $this->formatarDadosParaSQL($this->filtros);
			$this->lists = $this->formatarDadosParaSQL($this->lists);
			$this->ajaxForms = $this->formatarDadosParaSQL($this->ajaxForms);
			$this->ajaxPages = $this->formatarDadosParaSQL($this->ajaxPages);
			$this->classes = $this->formatarDadosParaSQL($this->classes);




    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->projetos_versao_banco_banco_id_INT = null; 
		$this->objProjetos_versao_banco_banco= null;
		$this->raiz = null; 
		$this->DAO = null; 
		$this->EXTDAO = null; 
		$this->forms = null; 
		$this->filtros = null; 
		$this->lists = null; 
		$this->ajaxForms = null; 
		$this->ajaxPages = null; 
		$this->classes = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("projetos_versao_banco_banco_id_INT", $this->projetos_versao_banco_banco_id_INT); 
		Helper::setSession("raiz", $this->raiz); 
		Helper::setSession("DAO", $this->DAO); 
		Helper::setSession("EXTDAO", $this->EXTDAO); 
		Helper::setSession("forms", $this->forms); 
		Helper::setSession("filtros", $this->filtros); 
		Helper::setSession("lists", $this->lists); 
		Helper::setSession("ajaxForms", $this->ajaxForms); 
		Helper::setSession("ajaxPages", $this->ajaxPages); 
		Helper::setSession("classes", $this->classes); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("projetos_versao_banco_banco_id_INT");
		Helper::clearSession("raiz");
		Helper::clearSession("DAO");
		Helper::clearSession("EXTDAO");
		Helper::clearSession("forms");
		Helper::clearSession("filtros");
		Helper::clearSession("lists");
		Helper::clearSession("ajaxForms");
		Helper::clearSession("ajaxPages");
		Helper::clearSession("classes");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::SESSION("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->raiz = Helper::SESSION("raiz{$numReg}"); 
		$this->DAO = Helper::SESSION("DAO{$numReg}"); 
		$this->EXTDAO = Helper::SESSION("EXTDAO{$numReg}"); 
		$this->forms = Helper::SESSION("forms{$numReg}"); 
		$this->filtros = Helper::SESSION("filtros{$numReg}"); 
		$this->lists = Helper::SESSION("lists{$numReg}"); 
		$this->ajaxForms = Helper::SESSION("ajaxForms{$numReg}"); 
		$this->ajaxPages = Helper::SESSION("ajaxPages{$numReg}"); 
		$this->classes = Helper::SESSION("classes{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::POST("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->raiz = Helper::POST("raiz{$numReg}"); 
		$this->DAO = Helper::POST("DAO{$numReg}"); 
		$this->EXTDAO = Helper::POST("EXTDAO{$numReg}"); 
		$this->forms = Helper::POST("forms{$numReg}"); 
		$this->filtros = Helper::POST("filtros{$numReg}"); 
		$this->lists = Helper::POST("lists{$numReg}"); 
		$this->ajaxForms = Helper::POST("ajaxForms{$numReg}"); 
		$this->ajaxPages = Helper::POST("ajaxPages{$numReg}"); 
		$this->classes = Helper::POST("classes{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->projetos_versao_banco_banco_id_INT = Helper::GET("projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->raiz = Helper::GET("raiz{$numReg}"); 
		$this->DAO = Helper::GET("DAO{$numReg}"); 
		$this->EXTDAO = Helper::GET("EXTDAO{$numReg}"); 
		$this->forms = Helper::GET("forms{$numReg}"); 
		$this->filtros = Helper::GET("filtros{$numReg}"); 
		$this->lists = Helper::GET("lists{$numReg}"); 
		$this->ajaxForms = Helper::GET("ajaxForms{$numReg}"); 
		$this->ajaxPages = Helper::GET("ajaxPages{$numReg}"); 
		$this->classes = Helper::GET("classes{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_banco_banco_id_INT = $this->projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["raiz{$numReg}"]) || $tipo == null){

		$upd.= "raiz = $this->raiz, ";

	} 

	if(isset($tipo["DAO{$numReg}"]) || $tipo == null){

		$upd.= "DAO = $this->DAO, ";

	} 

	if(isset($tipo["EXTDAO{$numReg}"]) || $tipo == null){

		$upd.= "EXTDAO = $this->EXTDAO, ";

	} 

	if(isset($tipo["forms{$numReg}"]) || $tipo == null){

		$upd.= "forms = $this->forms, ";

	} 

	if(isset($tipo["filtros{$numReg}"]) || $tipo == null){

		$upd.= "filtros = $this->filtros, ";

	} 

	if(isset($tipo["lists{$numReg}"]) || $tipo == null){

		$upd.= "lists = $this->lists, ";

	} 

	if(isset($tipo["ajaxForms{$numReg}"]) || $tipo == null){

		$upd.= "ajaxForms = $this->ajaxForms, ";

	} 

	if(isset($tipo["ajaxPages{$numReg}"]) || $tipo == null){

		$upd.= "ajaxPages = $this->ajaxPages, ";

	} 

	if(isset($tipo["classes{$numReg}"]) || $tipo == null){

		$upd.= "classes = $this->classes, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE diretorio_web SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    