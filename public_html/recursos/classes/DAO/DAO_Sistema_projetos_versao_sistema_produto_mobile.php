<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_projetos_versao_sistema_produto_mobile
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_projetos_versao_sistema_produto_mobile.php
    * TABELA MYSQL:    sistema_projetos_versao_sistema_produto_mobile
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_projetos_versao_sistema_produto_mobile extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_produto_mobile_id_INT;
	public $obj;
	public $sistema_projetos_versao_id_INT;
	public $objSistema_produto_mobile;
	public $programa_ARQUIVO;
	public $sistema_projetos_versao_produto_id_INT;
	public $objSistema_projetos_versao;
	public $version_code;
	public $version_name;
	public $pacote_completo_zipado_ARQUIVO;
	public $banco_db_ARQUIVO;
	public $xml_publicacao_ARQUIVO;
	public $resetar_banco_no_fim_da_atualizacao_BOOLEAN;
	public $enviar_dados_locais_do_sincronizador_BOOLEAN;


    public $nomeEntidade;



    

	public $label_id;
	public $label_sistema_produto_mobile_id_INT;
	public $label_sistema_projetos_versao_id_INT;
	public $label_programa_ARQUIVO;
	public $label_sistema_projetos_versao_produto_id_INT;
	public $label_version_code;
	public $label_version_name;
	public $label_pacote_completo_zipado_ARQUIVO;
	public $label_banco_db_ARQUIVO;
	public $label_xml_publicacao_ARQUIVO;
	public $label_resetar_banco_no_fim_da_atualizacao_BOOLEAN;
	public $label_enviar_dados_locais_do_sincronizador_BOOLEAN;


	public $diretorio_programa_ARQUIVO;
	public $diretorio_pacote_completo_zipado_ARQUIVO;
	public $diretorio_banco_db_ARQUIVO;
	public $diretorio_xml_publicacao_ARQUIVO;




    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_projetos_versao_sistema_produto_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_produto_mobile(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_produto_mobile($this->getDatabase());
		if($this->sistema_produto_mobile_id_INT != null) 
		$this->obj->select($this->sistema_produto_mobile_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_projetos_versao(){
	if($this->objSistema_produto_mobile ==null){
		$this->objSistema_produto_mobile = new EXTDAO_Sistema_projetos_versao($this->getDatabase());
		if($this->sistema_projetos_versao_id_INT != null) 
		$this->objSistema_produto_mobile->select($this->sistema_projetos_versao_id_INT);
	}
	return $this->objSistema_produto_mobile ;
}
function getFkObjSistema_projetos_versao_produto(){
	if($this->objSistema_projetos_versao ==null){
		$this->objSistema_projetos_versao = new EXTDAO_Sistema_projetos_versao_produto($this->getDatabase());
		if($this->sistema_projetos_versao_produto_id_INT != null) 
		$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_produto_id_INT);
	}
	return $this->objSistema_projetos_versao ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_produto_mobile($objArgumentos){

		$objArgumentos->nome="sistema_produto_mobile_id_INT";
		$objArgumentos->id="sistema_produto_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_produto_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao_produto($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->id="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao_produto()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Programa_ARQUIVO
            if(Helper::verificarUploadArquivo("programa_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPrograma_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "programa_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Pacote_completo_zipado_ARQUIVO
            if(Helper::verificarUploadArquivo("pacote_completo_zipado_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPacote_completo_zipado_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "pacote_completo_zipado_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Banco_db_ARQUIVO
            if(Helper::verificarUploadArquivo("banco_db_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadBanco_db_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "banco_db_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_publicacao_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_publicacao_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_publicacao_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_publicacao_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Programa_ARQUIVO
            if(Helper::verificarUploadArquivo("programa_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPrograma_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "programa_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Pacote_completo_zipado_ARQUIVO
            if(Helper::verificarUploadArquivo("pacote_completo_zipado_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPacote_completo_zipado_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "pacote_completo_zipado_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Banco_db_ARQUIVO
            if(Helper::verificarUploadArquivo("banco_db_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadBanco_db_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "banco_db_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_publicacao_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_publicacao_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_publicacao_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_publicacao_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Programa_ARQUIVO
            if(Helper::verificarUploadArquivo("programa_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPrograma_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "programa_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Pacote_completo_zipado_ARQUIVO
            if(Helper::verificarUploadArquivo("pacote_completo_zipado_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadPacote_completo_zipado_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "pacote_completo_zipado_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Banco_db_ARQUIVO
            if(Helper::verificarUploadArquivo("banco_db_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadBanco_db_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "banco_db_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_publicacao_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_publicacao_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_publicacao_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_publicacao_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_projetos_versao_sistema_produto_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_projetos_versao_sistema_produto_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMO��O DO ARQUIVO Programa_ARQUIVO
            $pathArquivo = $this->diretorio_programa_ARQUIVO . $this->getPrograma_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Pacote_completo_zipado_ARQUIVO
            $pathArquivo = $this->diretorio_pacote_completo_zipado_ARQUIVO . $this->getPacote_completo_zipado_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Banco_db_ARQUIVO
            $pathArquivo = $this->diretorio_banco_db_ARQUIVO . $this->getBanco_db_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Xml_publicacao_ARQUIVO
            $pathArquivo = $this->diretorio_xml_publicacao_ARQUIVO . $this->getXml_publicacao_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadPrograma_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_programa_ARQUIVO;
            $labelCampo = $this->label_programa_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["programa_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadPacote_completo_zipado_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_pacote_completo_zipado_ARQUIVO;
            $labelCampo = $this->label_pacote_completo_zipado_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["pacote_completo_zipado_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_2." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadBanco_db_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_banco_db_ARQUIVO;
            $labelCampo = $this->label_banco_db_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["banco_db_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_3." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadXml_publicacao_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_xml_publicacao_ARQUIVO;
            $labelCampo = $this->label_xml_publicacao_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["xml_publicacao_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_4." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_produto_mobile_id_INT()
    {
    	return $this->sistema_produto_mobile_id_INT;
    }
    
    public function getSistema_projetos_versao_id_INT()
    {
    	return $this->sistema_projetos_versao_id_INT;
    }
    
    public function getPrograma_ARQUIVO()
    {
    	return $this->programa_ARQUIVO;
    }
    
    public function getSistema_projetos_versao_produto_id_INT()
    {
    	return $this->sistema_projetos_versao_produto_id_INT;
    }
    
    public function getVersion_code()
    {
    	return $this->version_code;
    }
    
    public function getVersion_name()
    {
    	return $this->version_name;
    }
    
    public function getPacote_completo_zipado_ARQUIVO()
    {
    	return $this->pacote_completo_zipado_ARQUIVO;
    }
    
    public function getBanco_db_ARQUIVO()
    {
    	return $this->banco_db_ARQUIVO;
    }
    
    public function getXml_publicacao_ARQUIVO()
    {
    	return $this->xml_publicacao_ARQUIVO;
    }
    
    public function getResetar_banco_no_fim_da_atualizacao_BOOLEAN()
    {
    	return $this->resetar_banco_no_fim_da_atualizacao_BOOLEAN;
    }
    
    public function getEnviar_dados_locais_do_sincronizador_BOOLEAN()
    {
    	return $this->enviar_dados_locais_do_sincronizador_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_produto_mobile_id_INT($val)
    {
    	$this->sistema_produto_mobile_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_id_INT($val)
    {
    	$this->sistema_projetos_versao_id_INT =  $val;
    }
    
    function setPrograma_ARQUIVO($val)
    {
    	$this->programa_ARQUIVO =  $val;
    }
    
    function setSistema_projetos_versao_produto_id_INT($val)
    {
    	$this->sistema_projetos_versao_produto_id_INT =  $val;
    }
    
    function setVersion_code($val)
    {
    	$this->version_code =  $val;
    }
    
    function setVersion_name($val)
    {
    	$this->version_name =  $val;
    }
    
    function setPacote_completo_zipado_ARQUIVO($val)
    {
    	$this->pacote_completo_zipado_ARQUIVO =  $val;
    }
    
    function setBanco_db_ARQUIVO($val)
    {
    	$this->banco_db_ARQUIVO =  $val;
    }
    
    function setXml_publicacao_ARQUIVO($val)
    {
    	$this->xml_publicacao_ARQUIVO =  $val;
    }
    
    function setResetar_banco_no_fim_da_atualizacao_BOOLEAN($val)
    {
    	$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN =  $val;
    }
    
    function setEnviar_dados_locais_do_sincronizador_BOOLEAN($val)
    {
    	$this->enviar_dados_locais_do_sincronizador_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema_projetos_versao_sistema_produto_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->sistema_produto_mobile_id_INT = $row->sistema_produto_mobile_id_INT;
        if(isset($this->objSistema_produto_mobile))
			$this->objSistema_produto_mobile->select($this->sistema_produto_mobile_id_INT);

        $this->sistema_projetos_versao_id_INT = $row->sistema_projetos_versao_id_INT;
        if(isset($this->objSistema_projetos_versao))
			$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_id_INT);

        $this->programa_ARQUIVO = $row->programa_ARQUIVO;
        
        $this->sistema_projetos_versao_produto_id_INT = $row->sistema_projetos_versao_produto_id_INT;
        if(isset($this->objSistema_projetos_versao_produto))
			$this->objSistema_projetos_versao_produto->select($this->sistema_projetos_versao_produto_id_INT);

        $this->version_code = $row->version_code;
        
        $this->version_name = $row->version_name;
        
        $this->pacote_completo_zipado_ARQUIVO = $row->pacote_completo_zipado_ARQUIVO;
        
        $this->banco_db_ARQUIVO = $row->banco_db_ARQUIVO;
        
        $this->xml_publicacao_ARQUIVO = $row->xml_publicacao_ARQUIVO;
        
        $this->resetar_banco_no_fim_da_atualizacao_BOOLEAN = $row->resetar_banco_no_fim_da_atualizacao_BOOLEAN;
        
        $this->enviar_dados_locais_do_sincronizador_BOOLEAN = $row->enviar_dados_locais_do_sincronizador_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_projetos_versao_sistema_produto_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_projetos_versao_sistema_produto_mobile ( sistema_produto_mobile_id_INT , sistema_projetos_versao_id_INT , programa_ARQUIVO , sistema_projetos_versao_produto_id_INT , version_code , version_name , pacote_completo_zipado_ARQUIVO , banco_db_ARQUIVO , xml_publicacao_ARQUIVO , resetar_banco_no_fim_da_atualizacao_BOOLEAN , enviar_dados_locais_do_sincronizador_BOOLEAN ) VALUES ( {$this->sistema_produto_mobile_id_INT} , {$this->sistema_projetos_versao_id_INT} , {$this->programa_ARQUIVO} , {$this->sistema_projetos_versao_produto_id_INT} , {$this->version_code} , {$this->version_name} , {$this->pacote_completo_zipado_ARQUIVO} , {$this->banco_db_ARQUIVO} , {$this->xml_publicacao_ARQUIVO} , {$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN} , {$this->enviar_dados_locais_do_sincronizador_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_produto_mobile_id_INT(){ 

		return "sistema_produto_mobile_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_id_INT(){ 

		return "sistema_projetos_versao_id_INT";

	}

	public function nomeCampoPrograma_ARQUIVO(){ 

		return "programa_ARQUIVO";

	}

	public function nomeCampoSistema_projetos_versao_produto_id_INT(){ 

		return "sistema_projetos_versao_produto_id_INT";

	}

	public function nomeCampoVersion_code(){ 

		return "version_code";

	}

	public function nomeCampoVersion_name(){ 

		return "version_name";

	}

	public function nomeCampoPacote_completo_zipado_ARQUIVO(){ 

		return "pacote_completo_zipado_ARQUIVO";

	}

	public function nomeCampoBanco_db_ARQUIVO(){ 

		return "banco_db_ARQUIVO";

	}

	public function nomeCampoXml_publicacao_ARQUIVO(){ 

		return "xml_publicacao_ARQUIVO";

	}

	public function nomeCampoResetar_banco_no_fim_da_atualizacao_BOOLEAN(){ 

		return "resetar_banco_no_fim_da_atualizacao_BOOLEAN";

	}

	public function nomeCampoEnviar_dados_locais_do_sincronizador_BOOLEAN(){ 

		return "enviar_dados_locais_do_sincronizador_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_produto_mobile_id_INT($objArguments){

		$objArguments->nome = "sistema_produto_mobile_id_INT";
		$objArguments->id = "sistema_produto_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_id_INT";
		$objArguments->id = "sistema_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPrograma_ARQUIVO($objArguments){

		$objArguments->nome = "programa_ARQUIVO";
		$objArguments->id = "programa_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_produto_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_produto_id_INT";
		$objArguments->id = "sistema_projetos_versao_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoVersion_code($objArguments){

		$objArguments->nome = "version_code";
		$objArguments->id = "version_code";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoVersion_name($objArguments){

		$objArguments->nome = "version_name";
		$objArguments->id = "version_name";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoPacote_completo_zipado_ARQUIVO($objArguments){

		$objArguments->nome = "pacote_completo_zipado_ARQUIVO";
		$objArguments->id = "pacote_completo_zipado_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoBanco_db_ARQUIVO($objArguments){

		$objArguments->nome = "banco_db_ARQUIVO";
		$objArguments->id = "banco_db_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoXml_publicacao_ARQUIVO($objArguments){

		$objArguments->nome = "xml_publicacao_ARQUIVO";
		$objArguments->id = "xml_publicacao_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoResetar_banco_no_fim_da_atualizacao_BOOLEAN($objArguments){

		$objArguments->nome = "resetar_banco_no_fim_da_atualizacao_BOOLEAN";
		$objArguments->id = "resetar_banco_no_fim_da_atualizacao_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoEnviar_dados_locais_do_sincronizador_BOOLEAN($objArguments){

		$objArguments->nome = "enviar_dados_locais_do_sincronizador_BOOLEAN";
		$objArguments->id = "enviar_dados_locais_do_sincronizador_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_produto_mobile_id_INT == ""){

			$this->sistema_produto_mobile_id_INT = "null";

		}

		if($this->sistema_projetos_versao_id_INT == ""){

			$this->sistema_projetos_versao_id_INT = "null";

		}

			$this->programa_ARQUIVO = $this->formatarDadosParaSQL($this->programa_ARQUIVO);
		if($this->sistema_projetos_versao_produto_id_INT == ""){

			$this->sistema_projetos_versao_produto_id_INT = "null";

		}

			$this->version_code = $this->formatarDadosParaSQL($this->version_code);
			$this->version_name = $this->formatarDadosParaSQL($this->version_name);
			$this->pacote_completo_zipado_ARQUIVO = $this->formatarDadosParaSQL($this->pacote_completo_zipado_ARQUIVO);
			$this->banco_db_ARQUIVO = $this->formatarDadosParaSQL($this->banco_db_ARQUIVO);
			$this->xml_publicacao_ARQUIVO = $this->formatarDadosParaSQL($this->xml_publicacao_ARQUIVO);
		if($this->resetar_banco_no_fim_da_atualizacao_BOOLEAN == ""){

			$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN = "null";

		}

		if($this->enviar_dados_locais_do_sincronizador_BOOLEAN == ""){

			$this->enviar_dados_locais_do_sincronizador_BOOLEAN = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->sistema_produto_mobile_id_INT = null; 
		$this->objSistema_produto_mobile= null;
		$this->sistema_projetos_versao_id_INT = null; 
		$this->objSistema_projetos_versao= null;
		$this->programa_ARQUIVO = null; 
		$this->sistema_projetos_versao_produto_id_INT = null; 
		$this->objSistema_projetos_versao_produto= null;
		$this->version_code = null; 
		$this->version_name = null; 
		$this->pacote_completo_zipado_ARQUIVO = null; 
		$this->banco_db_ARQUIVO = null; 
		$this->xml_publicacao_ARQUIVO = null; 
		$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN = null; 
		$this->enviar_dados_locais_do_sincronizador_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("sistema_produto_mobile_id_INT", $this->sistema_produto_mobile_id_INT); 
		Helper::setSession("sistema_projetos_versao_id_INT", $this->sistema_projetos_versao_id_INT); 
		Helper::setSession("programa_ARQUIVO", $this->programa_ARQUIVO); 
		Helper::setSession("sistema_projetos_versao_produto_id_INT", $this->sistema_projetos_versao_produto_id_INT); 
		Helper::setSession("version_code", $this->version_code); 
		Helper::setSession("version_name", $this->version_name); 
		Helper::setSession("pacote_completo_zipado_ARQUIVO", $this->pacote_completo_zipado_ARQUIVO); 
		Helper::setSession("banco_db_ARQUIVO", $this->banco_db_ARQUIVO); 
		Helper::setSession("xml_publicacao_ARQUIVO", $this->xml_publicacao_ARQUIVO); 
		Helper::setSession("resetar_banco_no_fim_da_atualizacao_BOOLEAN", $this->resetar_banco_no_fim_da_atualizacao_BOOLEAN); 
		Helper::setSession("enviar_dados_locais_do_sincronizador_BOOLEAN", $this->enviar_dados_locais_do_sincronizador_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("sistema_produto_mobile_id_INT");
		Helper::clearSession("sistema_projetos_versao_id_INT");
		Helper::clearSession("programa_ARQUIVO");
		Helper::clearSession("sistema_projetos_versao_produto_id_INT");
		Helper::clearSession("version_code");
		Helper::clearSession("version_name");
		Helper::clearSession("pacote_completo_zipado_ARQUIVO");
		Helper::clearSession("banco_db_ARQUIVO");
		Helper::clearSession("xml_publicacao_ARQUIVO");
		Helper::clearSession("resetar_banco_no_fim_da_atualizacao_BOOLEAN");
		Helper::clearSession("enviar_dados_locais_do_sincronizador_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->sistema_produto_mobile_id_INT = Helper::SESSION("sistema_produto_mobile_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::SESSION("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->programa_ARQUIVO = Helper::SESSION("programa_ARQUIVO{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::SESSION("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->version_code = Helper::SESSION("version_code{$numReg}"); 
		$this->version_name = Helper::SESSION("version_name{$numReg}"); 
		$this->pacote_completo_zipado_ARQUIVO = Helper::SESSION("pacote_completo_zipado_ARQUIVO{$numReg}"); 
		$this->banco_db_ARQUIVO = Helper::SESSION("banco_db_ARQUIVO{$numReg}"); 
		$this->xml_publicacao_ARQUIVO = Helper::SESSION("xml_publicacao_ARQUIVO{$numReg}"); 
		$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN = Helper::SESSION("resetar_banco_no_fim_da_atualizacao_BOOLEAN{$numReg}"); 
		$this->enviar_dados_locais_do_sincronizador_BOOLEAN = Helper::SESSION("enviar_dados_locais_do_sincronizador_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->sistema_produto_mobile_id_INT = Helper::POST("sistema_produto_mobile_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::POST("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->programa_ARQUIVO = Helper::POST("programa_ARQUIVO{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::POST("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->version_code = Helper::POST("version_code{$numReg}"); 
		$this->version_name = Helper::POST("version_name{$numReg}"); 
		$this->pacote_completo_zipado_ARQUIVO = Helper::POST("pacote_completo_zipado_ARQUIVO{$numReg}"); 
		$this->banco_db_ARQUIVO = Helper::POST("banco_db_ARQUIVO{$numReg}"); 
		$this->xml_publicacao_ARQUIVO = Helper::POST("xml_publicacao_ARQUIVO{$numReg}"); 
		$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN = Helper::POST("resetar_banco_no_fim_da_atualizacao_BOOLEAN{$numReg}"); 
		$this->enviar_dados_locais_do_sincronizador_BOOLEAN = Helper::POST("enviar_dados_locais_do_sincronizador_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->sistema_produto_mobile_id_INT = Helper::GET("sistema_produto_mobile_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::GET("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->programa_ARQUIVO = Helper::GET("programa_ARQUIVO{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::GET("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->version_code = Helper::GET("version_code{$numReg}"); 
		$this->version_name = Helper::GET("version_name{$numReg}"); 
		$this->pacote_completo_zipado_ARQUIVO = Helper::GET("pacote_completo_zipado_ARQUIVO{$numReg}"); 
		$this->banco_db_ARQUIVO = Helper::GET("banco_db_ARQUIVO{$numReg}"); 
		$this->xml_publicacao_ARQUIVO = Helper::GET("xml_publicacao_ARQUIVO{$numReg}"); 
		$this->resetar_banco_no_fim_da_atualizacao_BOOLEAN = Helper::GET("resetar_banco_no_fim_da_atualizacao_BOOLEAN{$numReg}"); 
		$this->enviar_dados_locais_do_sincronizador_BOOLEAN = Helper::GET("enviar_dados_locais_do_sincronizador_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_produto_mobile_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_produto_mobile_id_INT = $this->sistema_produto_mobile_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_id_INT = $this->sistema_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["programa_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "programa_ARQUIVO = $this->programa_ARQUIVO, ";

	} 

	if(isset($tipo["sistema_projetos_versao_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_produto_id_INT = $this->sistema_projetos_versao_produto_id_INT, ";

	} 

	if(isset($tipo["version_code{$numReg}"]) || $tipo == null){

		$upd.= "version_code = $this->version_code, ";

	} 

	if(isset($tipo["version_name{$numReg}"]) || $tipo == null){

		$upd.= "version_name = $this->version_name, ";

	} 

	if(isset($tipo["pacote_completo_zipado_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "pacote_completo_zipado_ARQUIVO = $this->pacote_completo_zipado_ARQUIVO, ";

	} 

	if(isset($tipo["banco_db_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "banco_db_ARQUIVO = $this->banco_db_ARQUIVO, ";

	} 

	if(isset($tipo["xml_publicacao_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "xml_publicacao_ARQUIVO = $this->xml_publicacao_ARQUIVO, ";

	} 

	if(isset($tipo["resetar_banco_no_fim_da_atualizacao_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "resetar_banco_no_fim_da_atualizacao_BOOLEAN = $this->resetar_banco_no_fim_da_atualizacao_BOOLEAN, ";

	} 

	if(isset($tipo["enviar_dados_locais_do_sincronizador_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "enviar_dados_locais_do_sincronizador_BOOLEAN = $this->enviar_dados_locais_do_sincronizador_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_projetos_versao_sistema_produto_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    