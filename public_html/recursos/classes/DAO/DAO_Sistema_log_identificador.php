<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_log_identificador
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_log_identificador.php
    * TABELA MYSQL:    sistema_log_identificador
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_log_identificador extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $identificador_erro;
	public $descricao;
	public $stacktrace;
	public $sistema_projetos_versao_produto_id_INT;
	public $obj;
	public $corrigida_BOOLEAN;
	public $data_correcao_DATETIME;
	public $corrigida_pelo_usuario_id_INT;
	public $objSistema_projetos_versao_produto;
	public $sistema_projetos_versao_id_INT;
	public $objCorrigida_pelo_usuario;
	public $identificador_completo;


    public $nomeEntidade;

	public $data_correcao_DATETIME_UNIX;


    

	public $label_id;
	public $label_identificador_erro;
	public $label_descricao;
	public $label_stacktrace;
	public $label_sistema_projetos_versao_produto_id_INT;
	public $label_corrigida_BOOLEAN;
	public $label_data_correcao_DATETIME;
	public $label_corrigida_pelo_usuario_id_INT;
	public $label_sistema_projetos_versao_id_INT;
	public $label_identificador_completo;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_log_identificador";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_projetos_versao_produto(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_projetos_versao_produto($this->getDatabase());
		if($this->sistema_projetos_versao_produto_id_INT != null) 
		$this->obj->select($this->sistema_projetos_versao_produto_id_INT);
	}
	return $this->obj ;
}
function getFkObjCorrigida_pelo_usuario(){
	if($this->objSistema_projetos_versao_produto ==null){
		$this->objSistema_projetos_versao_produto = new EXTDAO_Usuario($this->getDatabase());
		if($this->corrigida_pelo_usuario_id_INT != null) 
		$this->objSistema_projetos_versao_produto->select($this->corrigida_pelo_usuario_id_INT);
	}
	return $this->objSistema_projetos_versao_produto ;
}
function getFkObjSistema_projetos_versao(){
	if($this->objCorrigida_pelo_usuario ==null){
		$this->objCorrigida_pelo_usuario = new EXTDAO_Sistema_projetos_versao($this->getDatabase());
		if($this->sistema_projetos_versao_id_INT != null) 
		$this->objCorrigida_pelo_usuario->select($this->sistema_projetos_versao_id_INT);
	}
	return $this->objCorrigida_pelo_usuario ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_projetos_versao_produto($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->id="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao_produto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCorrigida_pelo_usuario($objArgumentos){

		$objArgumentos->nome="corrigida_pelo_usuario_id_INT";
		$objArgumentos->id="corrigida_pelo_usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCorrigida_pelo_usuario()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_log_identificador", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_log_identificador", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getIdentificador_erro()
    {
    	return $this->identificador_erro;
    }
    
    public function getDescricao()
    {
    	return $this->descricao;
    }
    
    public function getStacktrace()
    {
    	return $this->stacktrace;
    }
    
    public function getSistema_projetos_versao_produto_id_INT()
    {
    	return $this->sistema_projetos_versao_produto_id_INT;
    }
    
    public function getCorrigida_BOOLEAN()
    {
    	return $this->corrigida_BOOLEAN;
    }
    
    function getData_correcao_DATETIME_UNIX()
    {
    	return $this->data_correcao_DATETIME_UNIX;
    }
    
    public function getData_correcao_DATETIME()
    {
    	return $this->data_correcao_DATETIME;
    }
    
    public function getCorrigida_pelo_usuario_id_INT()
    {
    	return $this->corrigida_pelo_usuario_id_INT;
    }
    
    public function getSistema_projetos_versao_id_INT()
    {
    	return $this->sistema_projetos_versao_id_INT;
    }
    
    public function getIdentificador_completo()
    {
    	return $this->identificador_completo;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setIdentificador_erro($val)
    {
    	$this->identificador_erro =  $val;
    }
    
    function setDescricao($val)
    {
    	$this->descricao =  $val;
    }
    
    function setStacktrace($val)
    {
    	$this->stacktrace =  $val;
    }
    
    function setSistema_projetos_versao_produto_id_INT($val)
    {
    	$this->sistema_projetos_versao_produto_id_INT =  $val;
    }
    
    function setCorrigida_BOOLEAN($val)
    {
    	$this->corrigida_BOOLEAN =  $val;
    }
    
    function setData_correcao_DATETIME($val)
    {
    	$this->data_correcao_DATETIME =  $val;
    }
    
    function setCorrigida_pelo_usuario_id_INT($val)
    {
    	$this->corrigida_pelo_usuario_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_id_INT($val)
    {
    	$this->sistema_projetos_versao_id_INT =  $val;
    }
    
    function setIdentificador_completo($val)
    {
    	$this->identificador_completo =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_correcao_DATETIME) AS data_correcao_DATETIME_UNIX FROM sistema_log_identificador WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->identificador_erro = $row->identificador_erro;
        
        $this->descricao = $row->descricao;
        
        $this->stacktrace = $row->stacktrace;
        
        $this->sistema_projetos_versao_produto_id_INT = $row->sistema_projetos_versao_produto_id_INT;
        if(isset($this->objSistema_projetos_versao_produto))
			$this->objSistema_projetos_versao_produto->select($this->sistema_projetos_versao_produto_id_INT);

        $this->corrigida_BOOLEAN = $row->corrigida_BOOLEAN;
        
        $this->data_correcao_DATETIME = $row->data_correcao_DATETIME;
        $this->data_correcao_DATETIME_UNIX = $row->data_correcao_DATETIME_UNIX;

        $this->corrigida_pelo_usuario_id_INT = $row->corrigida_pelo_usuario_id_INT;
        if(isset($this->objCorrigida_pelo_usuario))
			$this->objCorrigida_pelo_usuario->select($this->corrigida_pelo_usuario_id_INT);

        $this->sistema_projetos_versao_id_INT = $row->sistema_projetos_versao_id_INT;
        if(isset($this->objSistema_projetos_versao))
			$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_id_INT);

        $this->identificador_completo = $row->identificador_completo;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_log_identificador WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_log_identificador ( identificador_erro , descricao , stacktrace , sistema_projetos_versao_produto_id_INT , corrigida_BOOLEAN , data_correcao_DATETIME , corrigida_pelo_usuario_id_INT , sistema_projetos_versao_id_INT , identificador_completo ) VALUES ( {$this->identificador_erro} , {$this->descricao} , {$this->stacktrace} , {$this->sistema_projetos_versao_produto_id_INT} , {$this->corrigida_BOOLEAN} , {$this->data_correcao_DATETIME} , {$this->corrigida_pelo_usuario_id_INT} , {$this->sistema_projetos_versao_id_INT} , {$this->identificador_completo} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoIdentificador_erro(){ 

		return "identificador_erro";

	}

	public function nomeCampoDescricao(){ 

		return "descricao";

	}

	public function nomeCampoStacktrace(){ 

		return "stacktrace";

	}

	public function nomeCampoSistema_projetos_versao_produto_id_INT(){ 

		return "sistema_projetos_versao_produto_id_INT";

	}

	public function nomeCampoCorrigida_BOOLEAN(){ 

		return "corrigida_BOOLEAN";

	}

	public function nomeCampoData_correcao_DATETIME(){ 

		return "data_correcao_DATETIME";

	}

	public function nomeCampoCorrigida_pelo_usuario_id_INT(){ 

		return "corrigida_pelo_usuario_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_id_INT(){ 

		return "sistema_projetos_versao_id_INT";

	}

	public function nomeCampoIdentificador_completo(){ 

		return "identificador_completo";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoIdentificador_erro($objArguments){

		$objArguments->nome = "identificador_erro";
		$objArguments->id = "identificador_erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDescricao($objArguments){

		$objArguments->nome = "descricao";
		$objArguments->id = "descricao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoStacktrace($objArguments){

		$objArguments->nome = "stacktrace";
		$objArguments->id = "stacktrace";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_produto_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_produto_id_INT";
		$objArguments->id = "sistema_projetos_versao_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorrigida_BOOLEAN($objArguments){

		$objArguments->nome = "corrigida_BOOLEAN";
		$objArguments->id = "corrigida_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoData_correcao_DATETIME($objArguments){

		$objArguments->nome = "data_correcao_DATETIME";
		$objArguments->id = "data_correcao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoCorrigida_pelo_usuario_id_INT($objArguments){

		$objArguments->nome = "corrigida_pelo_usuario_id_INT";
		$objArguments->id = "corrigida_pelo_usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_id_INT";
		$objArguments->id = "sistema_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoIdentificador_completo($objArguments){

		$objArguments->nome = "identificador_completo";
		$objArguments->id = "identificador_completo";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->identificador_erro = $this->formatarDadosParaSQL($this->identificador_erro);
			$this->descricao = $this->formatarDadosParaSQL($this->descricao);
			$this->stacktrace = $this->formatarDadosParaSQL($this->stacktrace);
		if($this->sistema_projetos_versao_produto_id_INT == ""){

			$this->sistema_projetos_versao_produto_id_INT = "null";

		}

		if($this->corrigida_BOOLEAN == ""){

			$this->corrigida_BOOLEAN = "null";

		}

		if($this->corrigida_pelo_usuario_id_INT == ""){

			$this->corrigida_pelo_usuario_id_INT = "null";

		}

		if($this->sistema_projetos_versao_id_INT == ""){

			$this->sistema_projetos_versao_id_INT = "null";

		}

			$this->identificador_completo = $this->formatarDadosParaSQL($this->identificador_completo);


	$this->data_correcao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_correcao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_correcao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_correcao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->identificador_erro = null; 
		$this->descricao = null; 
		$this->stacktrace = null; 
		$this->sistema_projetos_versao_produto_id_INT = null; 
		$this->objSistema_projetos_versao_produto= null;
		$this->corrigida_BOOLEAN = null; 
		$this->data_correcao_DATETIME = null; 
		$this->corrigida_pelo_usuario_id_INT = null; 
		$this->objCorrigida_pelo_usuario= null;
		$this->sistema_projetos_versao_id_INT = null; 
		$this->objSistema_projetos_versao= null;
		$this->identificador_completo = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("identificador_erro", $this->identificador_erro); 
		Helper::setSession("descricao", $this->descricao); 
		Helper::setSession("stacktrace", $this->stacktrace); 
		Helper::setSession("sistema_projetos_versao_produto_id_INT", $this->sistema_projetos_versao_produto_id_INT); 
		Helper::setSession("corrigida_BOOLEAN", $this->corrigida_BOOLEAN); 
		Helper::setSession("data_correcao_DATETIME", $this->data_correcao_DATETIME); 
		Helper::setSession("corrigida_pelo_usuario_id_INT", $this->corrigida_pelo_usuario_id_INT); 
		Helper::setSession("sistema_projetos_versao_id_INT", $this->sistema_projetos_versao_id_INT); 
		Helper::setSession("identificador_completo", $this->identificador_completo); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("identificador_erro");
		Helper::clearSession("descricao");
		Helper::clearSession("stacktrace");
		Helper::clearSession("sistema_projetos_versao_produto_id_INT");
		Helper::clearSession("corrigida_BOOLEAN");
		Helper::clearSession("data_correcao_DATETIME");
		Helper::clearSession("corrigida_pelo_usuario_id_INT");
		Helper::clearSession("sistema_projetos_versao_id_INT");
		Helper::clearSession("identificador_completo");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->identificador_erro = Helper::SESSION("identificador_erro{$numReg}"); 
		$this->descricao = Helper::SESSION("descricao{$numReg}"); 
		$this->stacktrace = Helper::SESSION("stacktrace{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::SESSION("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->corrigida_BOOLEAN = Helper::SESSION("corrigida_BOOLEAN{$numReg}"); 
		$this->data_correcao_DATETIME = Helper::SESSION("data_correcao_DATETIME{$numReg}"); 
		$this->corrigida_pelo_usuario_id_INT = Helper::SESSION("corrigida_pelo_usuario_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::SESSION("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->identificador_completo = Helper::SESSION("identificador_completo{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->identificador_erro = Helper::POST("identificador_erro{$numReg}"); 
		$this->descricao = Helper::POST("descricao{$numReg}"); 
		$this->stacktrace = Helper::POST("stacktrace{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::POST("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->corrigida_BOOLEAN = Helper::POST("corrigida_BOOLEAN{$numReg}"); 
		$this->data_correcao_DATETIME = Helper::POST("data_correcao_DATETIME{$numReg}"); 
		$this->corrigida_pelo_usuario_id_INT = Helper::POST("corrigida_pelo_usuario_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::POST("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->identificador_completo = Helper::POST("identificador_completo{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->identificador_erro = Helper::GET("identificador_erro{$numReg}"); 
		$this->descricao = Helper::GET("descricao{$numReg}"); 
		$this->stacktrace = Helper::GET("stacktrace{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::GET("sistema_projetos_versao_produto_id_INT{$numReg}"); 
		$this->corrigida_BOOLEAN = Helper::GET("corrigida_BOOLEAN{$numReg}"); 
		$this->data_correcao_DATETIME = Helper::GET("data_correcao_DATETIME{$numReg}"); 
		$this->corrigida_pelo_usuario_id_INT = Helper::GET("corrigida_pelo_usuario_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::GET("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->identificador_completo = Helper::GET("identificador_completo{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["identificador_erro{$numReg}"]) || $tipo == null){

		$upd.= "identificador_erro = $this->identificador_erro, ";

	} 

	if(isset($tipo["descricao{$numReg}"]) || $tipo == null){

		$upd.= "descricao = $this->descricao, ";

	} 

	if(isset($tipo["stacktrace{$numReg}"]) || $tipo == null){

		$upd.= "stacktrace = $this->stacktrace, ";

	} 

	if(isset($tipo["sistema_projetos_versao_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_produto_id_INT = $this->sistema_projetos_versao_produto_id_INT, ";

	} 

	if(isset($tipo["corrigida_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "corrigida_BOOLEAN = $this->corrigida_BOOLEAN, ";

	} 

	if(isset($tipo["data_correcao_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_correcao_DATETIME = $this->data_correcao_DATETIME, ";

	} 

	if(isset($tipo["corrigida_pelo_usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "corrigida_pelo_usuario_id_INT = $this->corrigida_pelo_usuario_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_id_INT = $this->sistema_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["identificador_completo{$numReg}"]) || $tipo == null){

		$upd.= "identificador_completo = $this->identificador_completo, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_log_identificador SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    