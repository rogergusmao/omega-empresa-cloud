<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Projetos_versao_banco_banco
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Projetos_versao_banco_banco.php
    * TABELA MYSQL:    projetos_versao_banco_banco
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Projetos_versao_banco_banco extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $projetos_versao_id_INT;
	public $obj;
	public $banco_banco_id_INT;
	public $objProjetos_versao;
	public $script_estrutura_banco_hom_para_prod_ARQUIVO;
	public $script_registros_banco_hom_para_prod_ARQUIVO;
	public $xml_estrutura_banco_hom_para_prod_ARQUIVO;
	public $xml_registros_banco_hom_para_prod_ARQUIVO;
	public $copia_do_projetos_versao_banco_banco_id_INT;
	public $objBanco_banco;
	public $tipo_analise_projeto_id_INT;
	public $objCopia_do_projetos_versao_banco_banco;
	public $sinc_do_projetos_versao_banco_banco_id_INT;
	public $objTipo_analise_projeto;
	public $tipo_plataforma_operacional_id_INT;
	public $objSinc_do_projetos_versao_banco_banco;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_projetos_versao_id_INT;
	public $label_banco_banco_id_INT;
	public $label_script_estrutura_banco_hom_para_prod_ARQUIVO;
	public $label_script_registros_banco_hom_para_prod_ARQUIVO;
	public $label_xml_estrutura_banco_hom_para_prod_ARQUIVO;
	public $label_xml_registros_banco_hom_para_prod_ARQUIVO;
	public $label_copia_do_projetos_versao_banco_banco_id_INT;
	public $label_tipo_analise_projeto_id_INT;
	public $label_sinc_do_projetos_versao_banco_banco_id_INT;
	public $label_tipo_plataforma_operacional_id_INT;


	public $diretorio_script_estrutura_banco_hom_para_prod_ARQUIVO;
	public $diretorio_script_registros_banco_hom_para_prod_ARQUIVO;
	public $diretorio_xml_estrutura_banco_hom_para_prod_ARQUIVO;
	public $diretorio_xml_registros_banco_hom_para_prod_ARQUIVO;




    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "projetos_versao_banco_banco";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjProjetos_versao(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Projetos_versao($this->getDatabase());
		if($this->projetos_versao_id_INT != null) 
		$this->obj->select($this->projetos_versao_id_INT);
	}
	return $this->obj ;
}
function getFkObjBanco_banco(){
	if($this->objProjetos_versao ==null){
		$this->objProjetos_versao = new EXTDAO_Banco_banco($this->getDatabase());
		if($this->banco_banco_id_INT != null) 
		$this->objProjetos_versao->select($this->banco_banco_id_INT);
	}
	return $this->objProjetos_versao ;
}
function getFkObjCopia_do_projetos_versao_banco_banco(){
	if($this->objBanco_banco ==null){
		$this->objBanco_banco = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->copia_do_projetos_versao_banco_banco_id_INT != null) 
		$this->objBanco_banco->select($this->copia_do_projetos_versao_banco_banco_id_INT);
	}
	return $this->objBanco_banco ;
}
function getFkObjTipo_analise_projeto(){
	if($this->objCopia_do_projetos_versao_banco_banco ==null){
		$this->objCopia_do_projetos_versao_banco_banco = new EXTDAO_Tipo_analise_projeto($this->getDatabase());
		if($this->tipo_analise_projeto_id_INT != null) 
		$this->objCopia_do_projetos_versao_banco_banco->select($this->tipo_analise_projeto_id_INT);
	}
	return $this->objCopia_do_projetos_versao_banco_banco ;
}
function getFkObjSinc_do_projetos_versao_banco_banco(){
	if($this->objTipo_analise_projeto ==null){
		$this->objTipo_analise_projeto = new EXTDAO_Projetos_versao_banco_banco($this->getDatabase());
		if($this->sinc_do_projetos_versao_banco_banco_id_INT != null) 
		$this->objTipo_analise_projeto->select($this->sinc_do_projetos_versao_banco_banco_id_INT);
	}
	return $this->objTipo_analise_projeto ;
}
function getFkObjTipo_plataforma_operacional(){
	if($this->objSinc_do_projetos_versao_banco_banco ==null){
		$this->objSinc_do_projetos_versao_banco_banco = new EXTDAO_Tipo_plataforma_operacional($this->getDatabase());
		if($this->tipo_plataforma_operacional_id_INT != null) 
		$this->objSinc_do_projetos_versao_banco_banco->select($this->tipo_plataforma_operacional_id_INT);
	}
	return $this->objSinc_do_projetos_versao_banco_banco ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllProjetos_versao($objArgumentos){

		$objArgumentos->nome="projetos_versao_id_INT";
		$objArgumentos->id="projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProjetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllBanco_banco($objArgumentos){

		$objArgumentos->nome="banco_banco_id_INT";
		$objArgumentos->id="banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjBanco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCopia_do_projetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="copia_do_projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="copia_do_projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCopia_do_projetos_versao_banco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_analise_projeto($objArgumentos){

		$objArgumentos->nome="tipo_analise_projeto_id_INT";
		$objArgumentos->id="tipo_analise_projeto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_analise_projeto()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSinc_do_projetos_versao_banco_banco($objArgumentos){

		$objArgumentos->nome="sinc_do_projetos_versao_banco_banco_id_INT";
		$objArgumentos->id="sinc_do_projetos_versao_banco_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSinc_do_projetos_versao_banco_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_plataforma_operacional($objArgumentos){

		$objArgumentos->nome="tipo_plataforma_operacional_id_INT";
		$objArgumentos->id="tipo_plataforma_operacional_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_plataforma_operacional()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("script_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadScript_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "script_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_estrutura_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_estrutura_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            if(Helper::verificarUploadArquivo("xml_registros_banco_hom_para_prod_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadXml_registros_banco_hom_para_prod_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "xml_registros_banco_hom_para_prod_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_projetos_versao_banco_banco", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_projetos_versao_banco_banco", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMO��O DO ARQUIVO Script_estrutura_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_script_estrutura_banco_hom_para_prod_ARQUIVO . $this->getScript_estrutura_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Script_registros_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_script_registros_banco_hom_para_prod_ARQUIVO . $this->getScript_registros_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Xml_estrutura_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_xml_estrutura_banco_hom_para_prod_ARQUIVO . $this->getXml_estrutura_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMO��O DO ARQUIVO Xml_registros_banco_hom_para_prod_ARQUIVO
            $pathArquivo = $this->diretorio_xml_registros_banco_hom_para_prod_ARQUIVO . $this->getXml_registros_banco_hom_para_prod_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadScript_estrutura_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_script_estrutura_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_script_estrutura_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["script_estrutura_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadScript_registros_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_script_registros_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_script_registros_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["script_registros_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_2." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadXml_estrutura_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_xml_estrutura_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_xml_estrutura_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["xml_estrutura_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_3." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadXml_registros_banco_hom_para_prod_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_xml_registros_banco_hom_para_prod_ARQUIVO;
            $labelCampo = $this->label_xml_registros_banco_hom_para_prod_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["xml_registros_banco_hom_para_prod_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_4." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getProjetos_versao_id_INT()
    {
    	return $this->projetos_versao_id_INT;
    }
    
    public function getBanco_banco_id_INT()
    {
    	return $this->banco_banco_id_INT;
    }
    
    public function getScript_estrutura_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->script_estrutura_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getScript_registros_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->script_registros_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getXml_estrutura_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->xml_estrutura_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getXml_registros_banco_hom_para_prod_ARQUIVO()
    {
    	return $this->xml_registros_banco_hom_para_prod_ARQUIVO;
    }
    
    public function getCopia_do_projetos_versao_banco_banco_id_INT()
    {
    	return $this->copia_do_projetos_versao_banco_banco_id_INT;
    }
    
    public function getTipo_analise_projeto_id_INT()
    {
    	return $this->tipo_analise_projeto_id_INT;
    }
    
    public function getSinc_do_projetos_versao_banco_banco_id_INT()
    {
    	return $this->sinc_do_projetos_versao_banco_banco_id_INT;
    }
    
    public function getTipo_plataforma_operacional_id_INT()
    {
    	return $this->tipo_plataforma_operacional_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setProjetos_versao_id_INT($val)
    {
    	$this->projetos_versao_id_INT =  $val;
    }
    
    function setBanco_banco_id_INT($val)
    {
    	$this->banco_banco_id_INT =  $val;
    }
    
    function setScript_estrutura_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->script_estrutura_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setScript_registros_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->script_registros_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setXml_estrutura_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->xml_estrutura_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setXml_registros_banco_hom_para_prod_ARQUIVO($val)
    {
    	$this->xml_registros_banco_hom_para_prod_ARQUIVO =  $val;
    }
    
    function setCopia_do_projetos_versao_banco_banco_id_INT($val)
    {
    	$this->copia_do_projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setTipo_analise_projeto_id_INT($val)
    {
    	$this->tipo_analise_projeto_id_INT =  $val;
    }
    
    function setSinc_do_projetos_versao_banco_banco_id_INT($val)
    {
    	$this->sinc_do_projetos_versao_banco_banco_id_INT =  $val;
    }
    
    function setTipo_plataforma_operacional_id_INT($val)
    {
    	$this->tipo_plataforma_operacional_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM projetos_versao_banco_banco WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
        if(isset($this->objProjetos_versao))
			$this->objProjetos_versao->select($this->projetos_versao_id_INT);

        $this->banco_banco_id_INT = $row->banco_banco_id_INT;
        if(isset($this->objBanco_banco))
			$this->objBanco_banco->select($this->banco_banco_id_INT);

        $this->script_estrutura_banco_hom_para_prod_ARQUIVO = $row->script_estrutura_banco_hom_para_prod_ARQUIVO;
        
        $this->script_registros_banco_hom_para_prod_ARQUIVO = $row->script_registros_banco_hom_para_prod_ARQUIVO;
        
        $this->xml_estrutura_banco_hom_para_prod_ARQUIVO = $row->xml_estrutura_banco_hom_para_prod_ARQUIVO;
        
        $this->xml_registros_banco_hom_para_prod_ARQUIVO = $row->xml_registros_banco_hom_para_prod_ARQUIVO;
        
        $this->copia_do_projetos_versao_banco_banco_id_INT = $row->copia_do_projetos_versao_banco_banco_id_INT;
        if(isset($this->objCopia_do_projetos_versao_banco_banco))
			$this->objCopia_do_projetos_versao_banco_banco->select($this->copia_do_projetos_versao_banco_banco_id_INT);

        $this->tipo_analise_projeto_id_INT = $row->tipo_analise_projeto_id_INT;
        if(isset($this->objTipo_analise_projeto))
			$this->objTipo_analise_projeto->select($this->tipo_analise_projeto_id_INT);

        $this->sinc_do_projetos_versao_banco_banco_id_INT = $row->sinc_do_projetos_versao_banco_banco_id_INT;
        if(isset($this->objSinc_do_projetos_versao_banco_banco))
			$this->objSinc_do_projetos_versao_banco_banco->select($this->sinc_do_projetos_versao_banco_banco_id_INT);

        $this->tipo_plataforma_operacional_id_INT = $row->tipo_plataforma_operacional_id_INT;
        if(isset($this->objTipo_plataforma_operacional))
			$this->objTipo_plataforma_operacional->select($this->tipo_plataforma_operacional_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM projetos_versao_banco_banco WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO projetos_versao_banco_banco ( nome , projetos_versao_id_INT , banco_banco_id_INT , script_estrutura_banco_hom_para_prod_ARQUIVO , script_registros_banco_hom_para_prod_ARQUIVO , xml_estrutura_banco_hom_para_prod_ARQUIVO , xml_registros_banco_hom_para_prod_ARQUIVO , copia_do_projetos_versao_banco_banco_id_INT , tipo_analise_projeto_id_INT , sinc_do_projetos_versao_banco_banco_id_INT , tipo_plataforma_operacional_id_INT ) VALUES ( {$this->nome} , {$this->projetos_versao_id_INT} , {$this->banco_banco_id_INT} , {$this->script_estrutura_banco_hom_para_prod_ARQUIVO} , {$this->script_registros_banco_hom_para_prod_ARQUIVO} , {$this->xml_estrutura_banco_hom_para_prod_ARQUIVO} , {$this->xml_registros_banco_hom_para_prod_ARQUIVO} , {$this->copia_do_projetos_versao_banco_banco_id_INT} , {$this->tipo_analise_projeto_id_INT} , {$this->sinc_do_projetos_versao_banco_banco_id_INT} , {$this->tipo_plataforma_operacional_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoProjetos_versao_id_INT(){ 

		return "projetos_versao_id_INT";

	}

	public function nomeCampoBanco_banco_id_INT(){ 

		return "banco_banco_id_INT";

	}

	public function nomeCampoScript_estrutura_banco_hom_para_prod_ARQUIVO(){ 

		return "script_estrutura_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoScript_registros_banco_hom_para_prod_ARQUIVO(){ 

		return "script_registros_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoXml_estrutura_banco_hom_para_prod_ARQUIVO(){ 

		return "xml_estrutura_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoXml_registros_banco_hom_para_prod_ARQUIVO(){ 

		return "xml_registros_banco_hom_para_prod_ARQUIVO";

	}

	public function nomeCampoCopia_do_projetos_versao_banco_banco_id_INT(){ 

		return "copia_do_projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoTipo_analise_projeto_id_INT(){ 

		return "tipo_analise_projeto_id_INT";

	}

	public function nomeCampoSinc_do_projetos_versao_banco_banco_id_INT(){ 

		return "sinc_do_projetos_versao_banco_banco_id_INT";

	}

	public function nomeCampoTipo_plataforma_operacional_id_INT(){ 

		return "tipo_plataforma_operacional_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoProjetos_versao_id_INT($objArguments){

		$objArguments->nome = "projetos_versao_id_INT";
		$objArguments->id = "projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoBanco_banco_id_INT($objArguments){

		$objArguments->nome = "banco_banco_id_INT";
		$objArguments->id = "banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoScript_estrutura_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "script_estrutura_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "script_estrutura_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoScript_registros_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "script_registros_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "script_registros_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoXml_estrutura_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "xml_estrutura_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "xml_estrutura_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoXml_registros_banco_hom_para_prod_ARQUIVO($objArguments){

		$objArguments->nome = "xml_registros_banco_hom_para_prod_ARQUIVO";
		$objArguments->id = "xml_registros_banco_hom_para_prod_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoCopia_do_projetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "copia_do_projetos_versao_banco_banco_id_INT";
		$objArguments->id = "copia_do_projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_analise_projeto_id_INT($objArguments){

		$objArguments->nome = "tipo_analise_projeto_id_INT";
		$objArguments->id = "tipo_analise_projeto_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSinc_do_projetos_versao_banco_banco_id_INT($objArguments){

		$objArguments->nome = "sinc_do_projetos_versao_banco_banco_id_INT";
		$objArguments->id = "sinc_do_projetos_versao_banco_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_plataforma_operacional_id_INT($objArguments){

		$objArguments->nome = "tipo_plataforma_operacional_id_INT";
		$objArguments->id = "tipo_plataforma_operacional_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->nome = $this->formatarDadosParaSQL($this->nome);
		if($this->projetos_versao_id_INT == ""){

			$this->projetos_versao_id_INT = "null";

		}

		if($this->banco_banco_id_INT == ""){

			$this->banco_banco_id_INT = "null";

		}

			$this->script_estrutura_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->script_estrutura_banco_hom_para_prod_ARQUIVO);
			$this->script_registros_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->script_registros_banco_hom_para_prod_ARQUIVO);
			$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->xml_estrutura_banco_hom_para_prod_ARQUIVO);
			$this->xml_registros_banco_hom_para_prod_ARQUIVO = $this->formatarDadosParaSQL($this->xml_registros_banco_hom_para_prod_ARQUIVO);
		if($this->copia_do_projetos_versao_banco_banco_id_INT == ""){

			$this->copia_do_projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->tipo_analise_projeto_id_INT == ""){

			$this->tipo_analise_projeto_id_INT = "null";

		}

		if($this->sinc_do_projetos_versao_banco_banco_id_INT == ""){

			$this->sinc_do_projetos_versao_banco_banco_id_INT = "null";

		}

		if($this->tipo_plataforma_operacional_id_INT == ""){

			$this->tipo_plataforma_operacional_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->nome = null; 
		$this->projetos_versao_id_INT = null; 
		$this->objProjetos_versao= null;
		$this->banco_banco_id_INT = null; 
		$this->objBanco_banco= null;
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = null; 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = null; 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = null; 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = null; 
		$this->copia_do_projetos_versao_banco_banco_id_INT = null; 
		$this->objCopia_do_projetos_versao_banco_banco= null;
		$this->tipo_analise_projeto_id_INT = null; 
		$this->objTipo_analise_projeto= null;
		$this->sinc_do_projetos_versao_banco_banco_id_INT = null; 
		$this->objSinc_do_projetos_versao_banco_banco= null;
		$this->tipo_plataforma_operacional_id_INT = null; 
		$this->objTipo_plataforma_operacional= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("nome", $this->nome); 
		Helper::setSession("projetos_versao_id_INT", $this->projetos_versao_id_INT); 
		Helper::setSession("banco_banco_id_INT", $this->banco_banco_id_INT); 
		Helper::setSession("script_estrutura_banco_hom_para_prod_ARQUIVO", $this->script_estrutura_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("script_registros_banco_hom_para_prod_ARQUIVO", $this->script_registros_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("xml_estrutura_banco_hom_para_prod_ARQUIVO", $this->xml_estrutura_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("xml_registros_banco_hom_para_prod_ARQUIVO", $this->xml_registros_banco_hom_para_prod_ARQUIVO); 
		Helper::setSession("copia_do_projetos_versao_banco_banco_id_INT", $this->copia_do_projetos_versao_banco_banco_id_INT); 
		Helper::setSession("tipo_analise_projeto_id_INT", $this->tipo_analise_projeto_id_INT); 
		Helper::setSession("sinc_do_projetos_versao_banco_banco_id_INT", $this->sinc_do_projetos_versao_banco_banco_id_INT); 
		Helper::setSession("tipo_plataforma_operacional_id_INT", $this->tipo_plataforma_operacional_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("nome");
		Helper::clearSession("projetos_versao_id_INT");
		Helper::clearSession("banco_banco_id_INT");
		Helper::clearSession("script_estrutura_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("script_registros_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("xml_estrutura_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("xml_registros_banco_hom_para_prod_ARQUIVO");
		Helper::clearSession("copia_do_projetos_versao_banco_banco_id_INT");
		Helper::clearSession("tipo_analise_projeto_id_INT");
		Helper::clearSession("sinc_do_projetos_versao_banco_banco_id_INT");
		Helper::clearSession("tipo_plataforma_operacional_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->nome = Helper::SESSION("nome{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::SESSION("projetos_versao_id_INT{$numReg}"); 
		$this->banco_banco_id_INT = Helper::SESSION("banco_banco_id_INT{$numReg}"); 
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = Helper::SESSION("script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = Helper::SESSION("script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = Helper::SESSION("xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = Helper::SESSION("xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->copia_do_projetos_versao_banco_banco_id_INT = Helper::SESSION("copia_do_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::SESSION("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->sinc_do_projetos_versao_banco_banco_id_INT = Helper::SESSION("sinc_do_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_plataforma_operacional_id_INT = Helper::SESSION("tipo_plataforma_operacional_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->nome = Helper::POST("nome{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::POST("projetos_versao_id_INT{$numReg}"); 
		$this->banco_banco_id_INT = Helper::POST("banco_banco_id_INT{$numReg}"); 
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = Helper::POST("script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = Helper::POST("script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = Helper::POST("xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = Helper::POST("xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->copia_do_projetos_versao_banco_banco_id_INT = Helper::POST("copia_do_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::POST("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->sinc_do_projetos_versao_banco_banco_id_INT = Helper::POST("sinc_do_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_plataforma_operacional_id_INT = Helper::POST("tipo_plataforma_operacional_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->nome = Helper::GET("nome{$numReg}"); 
		$this->projetos_versao_id_INT = Helper::GET("projetos_versao_id_INT{$numReg}"); 
		$this->banco_banco_id_INT = Helper::GET("banco_banco_id_INT{$numReg}"); 
		$this->script_estrutura_banco_hom_para_prod_ARQUIVO = Helper::GET("script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->script_registros_banco_hom_para_prod_ARQUIVO = Helper::GET("script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_estrutura_banco_hom_para_prod_ARQUIVO = Helper::GET("xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->xml_registros_banco_hom_para_prod_ARQUIVO = Helper::GET("xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"); 
		$this->copia_do_projetos_versao_banco_banco_id_INT = Helper::GET("copia_do_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_analise_projeto_id_INT = Helper::GET("tipo_analise_projeto_id_INT{$numReg}"); 
		$this->sinc_do_projetos_versao_banco_banco_id_INT = Helper::GET("sinc_do_projetos_versao_banco_banco_id_INT{$numReg}"); 
		$this->tipo_plataforma_operacional_id_INT = Helper::GET("tipo_plataforma_operacional_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == null){

		$upd.= "nome = $this->nome, ";

	} 

	if(isset($tipo["projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "projetos_versao_id_INT = $this->projetos_versao_id_INT, ";

	} 

	if(isset($tipo["banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "banco_banco_id_INT = $this->banco_banco_id_INT, ";

	} 

	if(isset($tipo["script_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "script_estrutura_banco_hom_para_prod_ARQUIVO = $this->script_estrutura_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["script_registros_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "script_registros_banco_hom_para_prod_ARQUIVO = $this->script_registros_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["xml_estrutura_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "xml_estrutura_banco_hom_para_prod_ARQUIVO = $this->xml_estrutura_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["xml_registros_banco_hom_para_prod_ARQUIVO{$numReg}"]) || $tipo == null){

		$upd.= "xml_registros_banco_hom_para_prod_ARQUIVO = $this->xml_registros_banco_hom_para_prod_ARQUIVO, ";

	} 

	if(isset($tipo["copia_do_projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "copia_do_projetos_versao_banco_banco_id_INT = $this->copia_do_projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["tipo_analise_projeto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_analise_projeto_id_INT = $this->tipo_analise_projeto_id_INT, ";

	} 

	if(isset($tipo["sinc_do_projetos_versao_banco_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sinc_do_projetos_versao_banco_banco_id_INT = $this->sinc_do_projetos_versao_banco_banco_id_INT, ";

	} 

	if(isset($tipo["tipo_plataforma_operacional_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_plataforma_operacional_id_INT = $this->tipo_plataforma_operacional_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE projetos_versao_banco_banco SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    