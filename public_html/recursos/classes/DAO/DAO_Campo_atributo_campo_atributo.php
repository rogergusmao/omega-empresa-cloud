<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Campo_atributo_campo_atributo
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Campo_atributo_campo_atributo.php
    * TABELA MYSQL:    campo_atributo_campo_atributo
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Campo_atributo_campo_atributo extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $homologacao_campo_atributo_id_INT;
	public $obj;
	public $producao_campo_atributo_id_INT;
	public $objHomologacao_campo_atributo;
	public $campo_campo_id_INT;
	public $objProducao_campo_atributo;


    public $nomeEntidade;



    

	public $label_id;
	public $label_homologacao_campo_atributo_id_INT;
	public $label_producao_campo_atributo_id_INT;
	public $label_campo_campo_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "campo_atributo_campo_atributo";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjHomologacao_campo_atributo(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Campo_atributo($this->getDatabase());
		if($this->homologacao_campo_atributo_id_INT != null) 
		$this->obj->select($this->homologacao_campo_atributo_id_INT);
	}
	return $this->obj ;
}
function getFkObjProducao_campo_atributo(){
	if($this->objHomologacao_campo_atributo ==null){
		$this->objHomologacao_campo_atributo = new EXTDAO_Campo_atributo($this->getDatabase());
		if($this->producao_campo_atributo_id_INT != null) 
		$this->objHomologacao_campo_atributo->select($this->producao_campo_atributo_id_INT);
	}
	return $this->objHomologacao_campo_atributo ;
}
function getFkObjCampo_campo(){
	if($this->objProducao_campo_atributo ==null){
		$this->objProducao_campo_atributo = new EXTDAO_Campo_campo($this->getDatabase());
		if($this->campo_campo_id_INT != null) 
		$this->objProducao_campo_atributo->select($this->campo_campo_id_INT);
	}
	return $this->objProducao_campo_atributo ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllHomologacao_campo_atributo($objArgumentos){

		$objArgumentos->nome="homologacao_campo_atributo_id_INT";
		$objArgumentos->id="homologacao_campo_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjHomologacao_campo_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllProducao_campo_atributo($objArgumentos){

		$objArgumentos->nome="producao_campo_atributo_id_INT";
		$objArgumentos->id="producao_campo_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjProducao_campo_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCampo_campo($objArgumentos){

		$objArgumentos->nome="campo_campo_id_INT";
		$objArgumentos->id="campo_campo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjCampo_campo()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_campo_atributo_campo_atributo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_campo_atributo_campo_atributo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getHomologacao_campo_atributo_id_INT()
    {
    	return $this->homologacao_campo_atributo_id_INT;
    }
    
    public function getProducao_campo_atributo_id_INT()
    {
    	return $this->producao_campo_atributo_id_INT;
    }
    
    public function getCampo_campo_id_INT()
    {
    	return $this->campo_campo_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setHomologacao_campo_atributo_id_INT($val)
    {
    	$this->homologacao_campo_atributo_id_INT =  $val;
    }
    
    function setProducao_campo_atributo_id_INT($val)
    {
    	$this->producao_campo_atributo_id_INT =  $val;
    }
    
    function setCampo_campo_id_INT($val)
    {
    	$this->campo_campo_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM campo_atributo_campo_atributo WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->homologacao_campo_atributo_id_INT = $row->homologacao_campo_atributo_id_INT;
        if(isset($this->objHomologacao_campo_atributo))
			$this->objHomologacao_campo_atributo->select($this->homologacao_campo_atributo_id_INT);

        $this->producao_campo_atributo_id_INT = $row->producao_campo_atributo_id_INT;
        if(isset($this->objProducao_campo_atributo))
			$this->objProducao_campo_atributo->select($this->producao_campo_atributo_id_INT);

        $this->campo_campo_id_INT = $row->campo_campo_id_INT;
        if(isset($this->objCampo_campo))
			$this->objCampo_campo->select($this->campo_campo_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM campo_atributo_campo_atributo WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO campo_atributo_campo_atributo ( homologacao_campo_atributo_id_INT , producao_campo_atributo_id_INT , campo_campo_id_INT ) VALUES ( {$this->homologacao_campo_atributo_id_INT} , {$this->producao_campo_atributo_id_INT} , {$this->campo_campo_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoHomologacao_campo_atributo_id_INT(){ 

		return "homologacao_campo_atributo_id_INT";

	}

	public function nomeCampoProducao_campo_atributo_id_INT(){ 

		return "producao_campo_atributo_id_INT";

	}

	public function nomeCampoCampo_campo_id_INT(){ 

		return "campo_campo_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoHomologacao_campo_atributo_id_INT($objArguments){

		$objArguments->nome = "homologacao_campo_atributo_id_INT";
		$objArguments->id = "homologacao_campo_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProducao_campo_atributo_id_INT($objArguments){

		$objArguments->nome = "producao_campo_atributo_id_INT";
		$objArguments->id = "producao_campo_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCampo_campo_id_INT($objArguments){

		$objArguments->nome = "campo_campo_id_INT";
		$objArguments->id = "campo_campo_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->homologacao_campo_atributo_id_INT == ""){

			$this->homologacao_campo_atributo_id_INT = "null";

		}

		if($this->producao_campo_atributo_id_INT == ""){

			$this->producao_campo_atributo_id_INT = "null";

		}

		if($this->campo_campo_id_INT == ""){

			$this->campo_campo_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->homologacao_campo_atributo_id_INT = null; 
		$this->objHomologacao_campo_atributo= null;
		$this->producao_campo_atributo_id_INT = null; 
		$this->objProducao_campo_atributo= null;
		$this->campo_campo_id_INT = null; 
		$this->objCampo_campo= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("homologacao_campo_atributo_id_INT", $this->homologacao_campo_atributo_id_INT); 
		Helper::setSession("producao_campo_atributo_id_INT", $this->producao_campo_atributo_id_INT); 
		Helper::setSession("campo_campo_id_INT", $this->campo_campo_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("homologacao_campo_atributo_id_INT");
		Helper::clearSession("producao_campo_atributo_id_INT");
		Helper::clearSession("campo_campo_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->homologacao_campo_atributo_id_INT = Helper::SESSION("homologacao_campo_atributo_id_INT{$numReg}"); 
		$this->producao_campo_atributo_id_INT = Helper::SESSION("producao_campo_atributo_id_INT{$numReg}"); 
		$this->campo_campo_id_INT = Helper::SESSION("campo_campo_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->homologacao_campo_atributo_id_INT = Helper::POST("homologacao_campo_atributo_id_INT{$numReg}"); 
		$this->producao_campo_atributo_id_INT = Helper::POST("producao_campo_atributo_id_INT{$numReg}"); 
		$this->campo_campo_id_INT = Helper::POST("campo_campo_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->homologacao_campo_atributo_id_INT = Helper::GET("homologacao_campo_atributo_id_INT{$numReg}"); 
		$this->producao_campo_atributo_id_INT = Helper::GET("producao_campo_atributo_id_INT{$numReg}"); 
		$this->campo_campo_id_INT = Helper::GET("campo_campo_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["homologacao_campo_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "homologacao_campo_atributo_id_INT = $this->homologacao_campo_atributo_id_INT, ";

	} 

	if(isset($tipo["producao_campo_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "producao_campo_atributo_id_INT = $this->producao_campo_atributo_id_INT, ";

	} 

	if(isset($tipo["campo_campo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "campo_campo_id_INT = $this->campo_campo_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE campo_atributo_campo_atributo SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    