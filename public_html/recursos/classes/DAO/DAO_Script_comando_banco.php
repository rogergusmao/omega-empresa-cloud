<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Script_comando_banco
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Script_comando_banco.php
    * TABELA MYSQL:    script_comando_banco
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Script_comando_banco extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $consulta;
	public $seq_INT;
	public $script_tabela_tabela_id_INT;
	public $obj;
	public $tipo_comando_banco_id_INT;
	public $objScript_tabela_tabela;
	public $atributo_atributo_id_INT;
	public $objTipo_comando_banco;
	public $tabela_chave_tabela_chave_id_INT;
	public $objAtributo_atributo;


    public $nomeEntidade;



    

	public $label_id;
	public $label_consulta;
	public $label_seq_INT;
	public $label_script_tabela_tabela_id_INT;
	public $label_tipo_comando_banco_id_INT;
	public $label_atributo_atributo_id_INT;
	public $label_tabela_chave_tabela_chave_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "script_comando_banco";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjScript_tabela_tabela(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Script_tabela_tabela($this->getDatabase());
		if($this->script_tabela_tabela_id_INT != null) 
		$this->obj->select($this->script_tabela_tabela_id_INT);
	}
	return $this->obj ;
}
function getFkObjTipo_comando_banco(){
	if($this->objScript_tabela_tabela ==null){
		$this->objScript_tabela_tabela = new EXTDAO_Tipo_comando_banco($this->getDatabase());
		if($this->tipo_comando_banco_id_INT != null) 
		$this->objScript_tabela_tabela->select($this->tipo_comando_banco_id_INT);
	}
	return $this->objScript_tabela_tabela ;
}
function getFkObjAtributo_atributo(){
	if($this->objTipo_comando_banco ==null){
		$this->objTipo_comando_banco = new EXTDAO_Atributo_atributo($this->getDatabase());
		if($this->atributo_atributo_id_INT != null) 
		$this->objTipo_comando_banco->select($this->atributo_atributo_id_INT);
	}
	return $this->objTipo_comando_banco ;
}
function getFkObjTabela_chave_tabela_chave(){
	if($this->objAtributo_atributo ==null){
		$this->objAtributo_atributo = new EXTDAO_Tabela_chave_tabela_chave($this->getDatabase());
		if($this->tabela_chave_tabela_chave_id_INT != null) 
		$this->objAtributo_atributo->select($this->tabela_chave_tabela_chave_id_INT);
	}
	return $this->objAtributo_atributo ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllScript_tabela_tabela($objArgumentos){

		$objArgumentos->nome="script_tabela_tabela_id_INT";
		$objArgumentos->id="script_tabela_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjScript_tabela_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_comando_banco($objArgumentos){

		$objArgumentos->nome="tipo_comando_banco_id_INT";
		$objArgumentos->id="tipo_comando_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTipo_comando_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllAtributo_atributo($objArgumentos){

		$objArgumentos->nome="atributo_atributo_id_INT";
		$objArgumentos->id="atributo_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjAtributo_atributo()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTabela_chave_tabela_chave($objArgumentos){

		$objArgumentos->nome="tabela_chave_tabela_chave_id_INT";
		$objArgumentos->id="tabela_chave_tabela_chave_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjTabela_chave_tabela_chave()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_script_comando_banco", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_script_comando_banco", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getConsulta()
    {
    	return $this->consulta;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    public function getScript_tabela_tabela_id_INT()
    {
    	return $this->script_tabela_tabela_id_INT;
    }
    
    public function getTipo_comando_banco_id_INT()
    {
    	return $this->tipo_comando_banco_id_INT;
    }
    
    public function getAtributo_atributo_id_INT()
    {
    	return $this->atributo_atributo_id_INT;
    }
    
    public function getTabela_chave_tabela_chave_id_INT()
    {
    	return $this->tabela_chave_tabela_chave_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setConsulta($val)
    {
    	$this->consulta =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    
    function setScript_tabela_tabela_id_INT($val)
    {
    	$this->script_tabela_tabela_id_INT =  $val;
    }
    
    function setTipo_comando_banco_id_INT($val)
    {
    	$this->tipo_comando_banco_id_INT =  $val;
    }
    
    function setAtributo_atributo_id_INT($val)
    {
    	$this->atributo_atributo_id_INT =  $val;
    }
    
    function setTabela_chave_tabela_chave_id_INT($val)
    {
    	$this->tabela_chave_tabela_chave_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM script_comando_banco WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->consulta = $row->consulta;
        
        $this->seq_INT = $row->seq_INT;
        
        $this->script_tabela_tabela_id_INT = $row->script_tabela_tabela_id_INT;
        if(isset($this->objScript_tabela_tabela))
			$this->objScript_tabela_tabela->select($this->script_tabela_tabela_id_INT);

        $this->tipo_comando_banco_id_INT = $row->tipo_comando_banco_id_INT;
        if(isset($this->objTipo_comando_banco))
			$this->objTipo_comando_banco->select($this->tipo_comando_banco_id_INT);

        $this->atributo_atributo_id_INT = $row->atributo_atributo_id_INT;
        if(isset($this->objAtributo_atributo))
			$this->objAtributo_atributo->select($this->atributo_atributo_id_INT);

        $this->tabela_chave_tabela_chave_id_INT = $row->tabela_chave_tabela_chave_id_INT;
        if(isset($this->objTabela_chave_tabela_chave))
			$this->objTabela_chave_tabela_chave->select($this->tabela_chave_tabela_chave_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM script_comando_banco WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO script_comando_banco ( consulta , seq_INT , script_tabela_tabela_id_INT , tipo_comando_banco_id_INT , atributo_atributo_id_INT , tabela_chave_tabela_chave_id_INT ) VALUES ( {$this->consulta} , {$this->seq_INT} , {$this->script_tabela_tabela_id_INT} , {$this->tipo_comando_banco_id_INT} , {$this->atributo_atributo_id_INT} , {$this->tabela_chave_tabela_chave_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoConsulta(){ 

		return "consulta";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}

	public function nomeCampoScript_tabela_tabela_id_INT(){ 

		return "script_tabela_tabela_id_INT";

	}

	public function nomeCampoTipo_comando_banco_id_INT(){ 

		return "tipo_comando_banco_id_INT";

	}

	public function nomeCampoAtributo_atributo_id_INT(){ 

		return "atributo_atributo_id_INT";

	}

	public function nomeCampoTabela_chave_tabela_chave_id_INT(){ 

		return "tabela_chave_tabela_chave_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoConsulta($objArguments){

		$objArguments->nome = "consulta";
		$objArguments->id = "consulta";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoScript_tabela_tabela_id_INT($objArguments){

		$objArguments->nome = "script_tabela_tabela_id_INT";
		$objArguments->id = "script_tabela_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_comando_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_comando_banco_id_INT";
		$objArguments->id = "tipo_comando_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoAtributo_atributo_id_INT($objArguments){

		$objArguments->nome = "atributo_atributo_id_INT";
		$objArguments->id = "atributo_atributo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTabela_chave_tabela_chave_id_INT($objArguments){

		$objArguments->nome = "tabela_chave_tabela_chave_id_INT";
		$objArguments->id = "tabela_chave_tabela_chave_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

			$this->consulta = $this->formatarDadosParaSQL($this->consulta);
		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}

		if($this->script_tabela_tabela_id_INT == ""){

			$this->script_tabela_tabela_id_INT = "null";

		}

		if($this->tipo_comando_banco_id_INT == ""){

			$this->tipo_comando_banco_id_INT = "null";

		}

		if($this->atributo_atributo_id_INT == ""){

			$this->atributo_atributo_id_INT = "null";

		}

		if($this->tabela_chave_tabela_chave_id_INT == ""){

			$this->tabela_chave_tabela_chave_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->consulta = null; 
		$this->seq_INT = null; 
		$this->script_tabela_tabela_id_INT = null; 
		$this->objScript_tabela_tabela= null;
		$this->tipo_comando_banco_id_INT = null; 
		$this->objTipo_comando_banco= null;
		$this->atributo_atributo_id_INT = null; 
		$this->objAtributo_atributo= null;
		$this->tabela_chave_tabela_chave_id_INT = null; 
		$this->objTabela_chave_tabela_chave= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("consulta", $this->consulta); 
		Helper::setSession("seq_INT", $this->seq_INT); 
		Helper::setSession("script_tabela_tabela_id_INT", $this->script_tabela_tabela_id_INT); 
		Helper::setSession("tipo_comando_banco_id_INT", $this->tipo_comando_banco_id_INT); 
		Helper::setSession("atributo_atributo_id_INT", $this->atributo_atributo_id_INT); 
		Helper::setSession("tabela_chave_tabela_chave_id_INT", $this->tabela_chave_tabela_chave_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("consulta");
		Helper::clearSession("seq_INT");
		Helper::clearSession("script_tabela_tabela_id_INT");
		Helper::clearSession("tipo_comando_banco_id_INT");
		Helper::clearSession("atributo_atributo_id_INT");
		Helper::clearSession("tabela_chave_tabela_chave_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->consulta = Helper::SESSION("consulta{$numReg}"); 
		$this->seq_INT = Helper::SESSION("seq_INT{$numReg}"); 
		$this->script_tabela_tabela_id_INT = Helper::SESSION("script_tabela_tabela_id_INT{$numReg}"); 
		$this->tipo_comando_banco_id_INT = Helper::SESSION("tipo_comando_banco_id_INT{$numReg}"); 
		$this->atributo_atributo_id_INT = Helper::SESSION("atributo_atributo_id_INT{$numReg}"); 
		$this->tabela_chave_tabela_chave_id_INT = Helper::SESSION("tabela_chave_tabela_chave_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->consulta = Helper::POST("consulta{$numReg}"); 
		$this->seq_INT = Helper::POST("seq_INT{$numReg}"); 
		$this->script_tabela_tabela_id_INT = Helper::POST("script_tabela_tabela_id_INT{$numReg}"); 
		$this->tipo_comando_banco_id_INT = Helper::POST("tipo_comando_banco_id_INT{$numReg}"); 
		$this->atributo_atributo_id_INT = Helper::POST("atributo_atributo_id_INT{$numReg}"); 
		$this->tabela_chave_tabela_chave_id_INT = Helper::POST("tabela_chave_tabela_chave_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->consulta = Helper::GET("consulta{$numReg}"); 
		$this->seq_INT = Helper::GET("seq_INT{$numReg}"); 
		$this->script_tabela_tabela_id_INT = Helper::GET("script_tabela_tabela_id_INT{$numReg}"); 
		$this->tipo_comando_banco_id_INT = Helper::GET("tipo_comando_banco_id_INT{$numReg}"); 
		$this->atributo_atributo_id_INT = Helper::GET("atributo_atributo_id_INT{$numReg}"); 
		$this->tabela_chave_tabela_chave_id_INT = Helper::GET("tabela_chave_tabela_chave_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["consulta{$numReg}"]) || $tipo == null){

		$upd.= "consulta = $this->consulta, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == null){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

	if(isset($tipo["script_tabela_tabela_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "script_tabela_tabela_id_INT = $this->script_tabela_tabela_id_INT, ";

	} 

	if(isset($tipo["tipo_comando_banco_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tipo_comando_banco_id_INT = $this->tipo_comando_banco_id_INT, ";

	} 

	if(isset($tipo["atributo_atributo_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "atributo_atributo_id_INT = $this->atributo_atributo_id_INT, ";

	} 

	if(isset($tipo["tabela_chave_tabela_chave_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "tabela_chave_tabela_chave_id_INT = $this->tabela_chave_tabela_chave_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE script_comando_banco SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    