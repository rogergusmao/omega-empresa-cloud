<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_projetos_versao_sistema_produto_web
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Sistema_projetos_versao_sistema_produto_web.php
    * TABELA MYSQL:    sistema_projetos_versao_sistema_produto_web
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_projetos_versao_sistema_produto_web extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_produto_web_id_INT;
	public $obj;
	public $sistema_projetos_versao_id_INT;
	public $objSistema_produto_web;
	public $sistema_projetos_versao_produto_id_INT;
	public $objSistema_projetos_versao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_sistema_produto_web_id_INT;
	public $label_sistema_projetos_versao_id_INT;
	public $label_sistema_projetos_versao_produto_id_INT;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sistema_projetos_versao_sistema_produto_web";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjSistema_produto_web(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Sistema_produto_web($this->getDatabase());
		if($this->sistema_produto_web_id_INT != null) 
		$this->obj->select($this->sistema_produto_web_id_INT);
	}
	return $this->obj ;
}
function getFkObjSistema_projetos_versao(){
	if($this->objSistema_produto_web ==null){
		$this->objSistema_produto_web = new EXTDAO_Sistema_projetos_versao($this->getDatabase());
		if($this->sistema_projetos_versao_id_INT != null) 
		$this->objSistema_produto_web->select($this->sistema_projetos_versao_id_INT);
	}
	return $this->objSistema_produto_web ;
}
function getFkObjSistema_projetos_versao_produto(){
	if($this->objSistema_projetos_versao ==null){
		$this->objSistema_projetos_versao = new EXTDAO_Sistema_projetos_versao_produto($this->getDatabase());
		if($this->sistema_projetos_versao_produto_id_INT != null) 
		$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_produto_id_INT);
	}
	return $this->objSistema_projetos_versao ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_produto_web($objArgumentos){

		$objArgumentos->nome="sistema_produto_web_id_INT";
		$objArgumentos->id="sistema_produto_web_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_produto_web()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSistema_projetos_versao_produto($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->id="sistema_projetos_versao_produto_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjSistema_projetos_versao_produto()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sistema_projetos_versao_sistema_produto_web", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_projetos_versao_sistema_produto_web", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_produto_web_id_INT()
    {
    	return $this->sistema_produto_web_id_INT;
    }
    
    public function getSistema_projetos_versao_id_INT()
    {
    	return $this->sistema_projetos_versao_id_INT;
    }
    
    public function getSistema_projetos_versao_produto_id_INT()
    {
    	return $this->sistema_projetos_versao_produto_id_INT;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_produto_web_id_INT($val)
    {
    	$this->sistema_produto_web_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_id_INT($val)
    {
    	$this->sistema_projetos_versao_id_INT =  $val;
    }
    
    function setSistema_projetos_versao_produto_id_INT($val)
    {
    	$this->sistema_projetos_versao_produto_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM sistema_projetos_versao_sistema_produto_web WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->sistema_produto_web_id_INT = $row->sistema_produto_web_id_INT;
        if(isset($this->objSistema_produto_web))
			$this->objSistema_produto_web->select($this->sistema_produto_web_id_INT);

        $this->sistema_projetos_versao_id_INT = $row->sistema_projetos_versao_id_INT;
        if(isset($this->objSistema_projetos_versao))
			$this->objSistema_projetos_versao->select($this->sistema_projetos_versao_id_INT);

        $this->sistema_projetos_versao_produto_id_INT = $row->sistema_projetos_versao_produto_id_INT;
        if(isset($this->objSistema_projetos_versao_produto))
			$this->objSistema_projetos_versao_produto->select($this->sistema_projetos_versao_produto_id_INT);

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sistema_projetos_versao_sistema_produto_web WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sistema_projetos_versao_sistema_produto_web ( sistema_produto_web_id_INT , sistema_projetos_versao_id_INT , sistema_projetos_versao_produto_id_INT ) VALUES ( {$this->sistema_produto_web_id_INT} , {$this->sistema_projetos_versao_id_INT} , {$this->sistema_projetos_versao_produto_id_INT} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_produto_web_id_INT(){ 

		return "sistema_produto_web_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_id_INT(){ 

		return "sistema_projetos_versao_id_INT";

	}

	public function nomeCampoSistema_projetos_versao_produto_id_INT(){ 

		return "sistema_projetos_versao_produto_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_produto_web_id_INT($objArguments){

		$objArguments->nome = "sistema_produto_web_id_INT";
		$objArguments->id = "sistema_produto_web_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_id_INT";
		$objArguments->id = "sistema_projetos_versao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSistema_projetos_versao_produto_id_INT($objArguments){

		$objArguments->nome = "sistema_projetos_versao_produto_id_INT";
		$objArguments->id = "sistema_projetos_versao_produto_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_produto_web_id_INT == ""){

			$this->sistema_produto_web_id_INT = "null";

		}

		if($this->sistema_projetos_versao_id_INT == ""){

			$this->sistema_projetos_versao_id_INT = "null";

		}

		if($this->sistema_projetos_versao_produto_id_INT == ""){

			$this->sistema_projetos_versao_produto_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->sistema_produto_web_id_INT = null; 
		$this->objSistema_produto_web= null;
		$this->sistema_projetos_versao_id_INT = null; 
		$this->objSistema_projetos_versao= null;
		$this->sistema_projetos_versao_produto_id_INT = null; 
		$this->objSistema_projetos_versao_produto= null;

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("sistema_produto_web_id_INT", $this->sistema_produto_web_id_INT); 
		Helper::setSession("sistema_projetos_versao_id_INT", $this->sistema_projetos_versao_id_INT); 
		Helper::setSession("sistema_projetos_versao_produto_id_INT", $this->sistema_projetos_versao_produto_id_INT); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("sistema_produto_web_id_INT");
		Helper::clearSession("sistema_projetos_versao_id_INT");
		Helper::clearSession("sistema_projetos_versao_produto_id_INT");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->sistema_produto_web_id_INT = Helper::SESSION("sistema_produto_web_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::SESSION("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::SESSION("sistema_projetos_versao_produto_id_INT{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->sistema_produto_web_id_INT = Helper::POST("sistema_produto_web_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::POST("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::POST("sistema_projetos_versao_produto_id_INT{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->sistema_produto_web_id_INT = Helper::GET("sistema_produto_web_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_id_INT = Helper::GET("sistema_projetos_versao_id_INT{$numReg}"); 
		$this->sistema_projetos_versao_produto_id_INT = Helper::GET("sistema_projetos_versao_produto_id_INT{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_produto_web_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_produto_web_id_INT = $this->sistema_produto_web_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_id_INT = $this->sistema_projetos_versao_id_INT, ";

	} 

	if(isset($tipo["sistema_projetos_versao_produto_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "sistema_projetos_versao_produto_id_INT = $this->sistema_projetos_versao_produto_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sistema_projetos_versao_sistema_produto_web SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    