<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Erro
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Erro.php
    * TABELA MYSQL:    erro
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Erro extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $usuario_id_INT;
	public $obj;
	public $codigo_erro;
	public $mensagem_erro;
	public $url;
	public $arquivo_erro;
	public $linha_erro;
	public $get;
	public $post;
	public $session;
	public $stacktrace;
	public $datahora_DATETIME;
	public $status_BOOLEAN;


    public $nomeEntidade;

	public $datahora_DATETIME_UNIX;


    

	public $label_id;
	public $label_usuario_id_INT;
	public $label_codigo_erro;
	public $label_mensagem_erro;
	public $label_url;
	public $label_arquivo_erro;
	public $label_linha_erro;
	public $label_get;
	public $label_post;
	public $label_session;
	public $label_stacktrace;
	public $label_datahora_DATETIME;
	public $label_status_BOOLEAN;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "erro";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjUsuario(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Usuario($this->getDatabase());
		if($this->usuario_id_INT != null) 
		$this->obj->select($this->usuario_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllUsuario($objArgumentos){

		$objArgumentos->nome="usuario_id_INT";
		$objArgumentos->id="usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_erro", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_erro", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getUsuario_id_INT()
    {
    	return $this->usuario_id_INT;
    }
    
    public function getCodigo_erro()
    {
    	return $this->codigo_erro;
    }
    
    public function getMensagem_erro()
    {
    	return $this->mensagem_erro;
    }
    
    public function getUrl()
    {
    	return $this->url;
    }
    
    public function getArquivo_erro()
    {
    	return $this->arquivo_erro;
    }
    
    public function getLinha_erro()
    {
    	return $this->linha_erro;
    }
    
    public function getGet()
    {
    	return $this->get;
    }
    
    public function getPost()
    {
    	return $this->post;
    }
    
    public function getSession()
    {
    	return $this->session;
    }
    
    public function getStacktrace()
    {
    	return $this->stacktrace;
    }
    
    function getDatahora_DATETIME_UNIX()
    {
    	return $this->datahora_DATETIME_UNIX;
    }
    
    public function getDatahora_DATETIME()
    {
    	return $this->datahora_DATETIME;
    }
    
    public function getStatus_BOOLEAN()
    {
    	return $this->status_BOOLEAN;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setUsuario_id_INT($val)
    {
    	$this->usuario_id_INT =  $val;
    }
    
    function setCodigo_erro($val)
    {
    	$this->codigo_erro =  $val;
    }
    
    function setMensagem_erro($val)
    {
    	$this->mensagem_erro =  $val;
    }
    
    function setUrl($val)
    {
    	$this->url =  $val;
    }
    
    function setArquivo_erro($val)
    {
    	$this->arquivo_erro =  $val;
    }
    
    function setLinha_erro($val)
    {
    	$this->linha_erro =  $val;
    }
    
    function setGet($val)
    {
    	$this->get =  $val;
    }
    
    function setPost($val)
    {
    	$this->post =  $val;
    }
    
    function setSession($val)
    {
    	$this->session =  $val;
    }
    
    function setStacktrace($val)
    {
    	$this->stacktrace =  $val;
    }
    
    function setDatahora_DATETIME($val)
    {
    	$this->datahora_DATETIME =  $val;
    }
    
    function setStatus_BOOLEAN($val)
    {
    	$this->status_BOOLEAN =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(datahora_DATETIME) AS datahora_DATETIME_UNIX FROM erro WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->usuario_id_INT = $row->usuario_id_INT;
        if(isset($this->objUsuario))
			$this->objUsuario->select($this->usuario_id_INT);

        $this->codigo_erro = $row->codigo_erro;
        
        $this->mensagem_erro = $row->mensagem_erro;
        
        $this->url = $row->url;
        
        $this->arquivo_erro = $row->arquivo_erro;
        
        $this->linha_erro = $row->linha_erro;
        
        $this->get = $row->get;
        
        $this->post = $row->post;
        
        $this->session = $row->session;
        
        $this->stacktrace = $row->stacktrace;
        
        $this->datahora_DATETIME = $row->datahora_DATETIME;
        $this->datahora_DATETIME_UNIX = $row->datahora_DATETIME_UNIX;

        $this->status_BOOLEAN = $row->status_BOOLEAN;
        
		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM erro WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO erro ( usuario_id_INT , codigo_erro , mensagem_erro , url , arquivo_erro , linha_erro , get , post , session , stacktrace , datahora_DATETIME , status_BOOLEAN ) VALUES ( {$this->usuario_id_INT} , {$this->codigo_erro} , {$this->mensagem_erro} , {$this->url} , {$this->arquivo_erro} , {$this->linha_erro} , {$this->get} , {$this->post} , {$this->session} , {$this->stacktrace} , {$this->datahora_DATETIME} , {$this->status_BOOLEAN} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoUsuario_id_INT(){ 

		return "usuario_id_INT";

	}

	public function nomeCampoCodigo_erro(){ 

		return "codigo_erro";

	}

	public function nomeCampoMensagem_erro(){ 

		return "mensagem_erro";

	}

	public function nomeCampoUrl(){ 

		return "url";

	}

	public function nomeCampoArquivo_erro(){ 

		return "arquivo_erro";

	}

	public function nomeCampoLinha_erro(){ 

		return "linha_erro";

	}

	public function nomeCampoGet(){ 

		return "get";

	}

	public function nomeCampoPost(){ 

		return "post";

	}

	public function nomeCampoSession(){ 

		return "session";

	}

	public function nomeCampoStacktrace(){ 

		return "stacktrace";

	}

	public function nomeCampoDatahora_DATETIME(){ 

		return "datahora_DATETIME";

	}

	public function nomeCampoStatus_BOOLEAN(){ 

		return "status_BOOLEAN";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoUsuario_id_INT($objArguments){

		$objArguments->nome = "usuario_id_INT";
		$objArguments->id = "usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCodigo_erro($objArguments){

		$objArguments->nome = "codigo_erro";
		$objArguments->id = "codigo_erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoMensagem_erro($objArguments){

		$objArguments->nome = "mensagem_erro";
		$objArguments->id = "mensagem_erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUrl($objArguments){

		$objArguments->nome = "url";
		$objArguments->id = "url";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoArquivo_erro($objArguments){

		$objArguments->nome = "arquivo_erro";
		$objArguments->id = "arquivo_erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoLinha_erro($objArguments){

		$objArguments->nome = "linha_erro";
		$objArguments->id = "linha_erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoGet($objArguments){

		$objArguments->nome = "get";
		$objArguments->id = "get";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoPost($objArguments){

		$objArguments->nome = "post";
		$objArguments->id = "post";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSession($objArguments){

		$objArguments->nome = "session";
		$objArguments->id = "session";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoStacktrace($objArguments){

		$objArguments->nome = "stacktrace";
		$objArguments->id = "stacktrace";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDatahora_DATETIME($objArguments){

		$objArguments->nome = "datahora_DATETIME";
		$objArguments->id = "datahora_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoStatus_BOOLEAN($objArguments){

		$objArguments->nome = "status_BOOLEAN";
		$objArguments->id = "status_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->usuario_id_INT == ""){

			$this->usuario_id_INT = "null";

		}

			$this->codigo_erro = $this->formatarDadosParaSQL($this->codigo_erro);
			$this->mensagem_erro = $this->formatarDadosParaSQL($this->mensagem_erro);
			$this->url = $this->formatarDadosParaSQL($this->url);
			$this->arquivo_erro = $this->formatarDadosParaSQL($this->arquivo_erro);
			$this->linha_erro = $this->formatarDadosParaSQL($this->linha_erro);
			$this->get = $this->formatarDadosParaSQL($this->get);
			$this->post = $this->formatarDadosParaSQL($this->post);
			$this->session = $this->formatarDadosParaSQL($this->session);
			$this->stacktrace = $this->formatarDadosParaSQL($this->stacktrace);
		if($this->status_BOOLEAN == ""){

			$this->status_BOOLEAN = "null";

		}



	$this->datahora_DATETIME = $this->formatarDataTimeParaComandoSQL($this->datahora_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->datahora_DATETIME = $this->formatarDataTimeParaExibicao($this->datahora_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->usuario_id_INT = null; 
		$this->objUsuario= null;
		$this->codigo_erro = null; 
		$this->mensagem_erro = null; 
		$this->url = null; 
		$this->arquivo_erro = null; 
		$this->linha_erro = null; 
		$this->get = null; 
		$this->post = null; 
		$this->session = null; 
		$this->stacktrace = null; 
		$this->datahora_DATETIME = null; 
		$this->status_BOOLEAN = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("usuario_id_INT", $this->usuario_id_INT); 
		Helper::setSession("codigo_erro", $this->codigo_erro); 
		Helper::setSession("mensagem_erro", $this->mensagem_erro); 
		Helper::setSession("url", $this->url); 
		Helper::setSession("arquivo_erro", $this->arquivo_erro); 
		Helper::setSession("linha_erro", $this->linha_erro); 
		Helper::setSession("get", $this->get); 
		Helper::setSession("post", $this->post); 
		Helper::setSession("session", $this->session); 
		Helper::setSession("stacktrace", $this->stacktrace); 
		Helper::setSession("datahora_DATETIME", $this->datahora_DATETIME); 
		Helper::setSession("status_BOOLEAN", $this->status_BOOLEAN); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("usuario_id_INT");
		Helper::clearSession("codigo_erro");
		Helper::clearSession("mensagem_erro");
		Helper::clearSession("url");
		Helper::clearSession("arquivo_erro");
		Helper::clearSession("linha_erro");
		Helper::clearSession("get");
		Helper::clearSession("post");
		Helper::clearSession("session");
		Helper::clearSession("stacktrace");
		Helper::clearSession("datahora_DATETIME");
		Helper::clearSession("status_BOOLEAN");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->usuario_id_INT = Helper::SESSION("usuario_id_INT{$numReg}"); 
		$this->codigo_erro = Helper::SESSION("codigo_erro{$numReg}"); 
		$this->mensagem_erro = Helper::SESSION("mensagem_erro{$numReg}"); 
		$this->url = Helper::SESSION("url{$numReg}"); 
		$this->arquivo_erro = Helper::SESSION("arquivo_erro{$numReg}"); 
		$this->linha_erro = Helper::SESSION("linha_erro{$numReg}"); 
		$this->get = Helper::SESSION("get{$numReg}"); 
		$this->post = Helper::SESSION("post{$numReg}"); 
		$this->session = Helper::SESSION("session{$numReg}"); 
		$this->stacktrace = Helper::SESSION("stacktrace{$numReg}"); 
		$this->datahora_DATETIME = Helper::SESSION("datahora_DATETIME{$numReg}"); 
		$this->status_BOOLEAN = Helper::SESSION("status_BOOLEAN{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->usuario_id_INT = Helper::POST("usuario_id_INT{$numReg}"); 
		$this->codigo_erro = Helper::POST("codigo_erro{$numReg}"); 
		$this->mensagem_erro = Helper::POST("mensagem_erro{$numReg}"); 
		$this->url = Helper::POST("url{$numReg}"); 
		$this->arquivo_erro = Helper::POST("arquivo_erro{$numReg}"); 
		$this->linha_erro = Helper::POST("linha_erro{$numReg}"); 
		$this->get = Helper::POST("get{$numReg}"); 
		$this->post = Helper::POST("post{$numReg}"); 
		$this->session = Helper::POST("session{$numReg}"); 
		$this->stacktrace = Helper::POST("stacktrace{$numReg}"); 
		$this->datahora_DATETIME = Helper::POST("datahora_DATETIME{$numReg}"); 
		$this->status_BOOLEAN = Helper::POST("status_BOOLEAN{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->usuario_id_INT = Helper::GET("usuario_id_INT{$numReg}"); 
		$this->codigo_erro = Helper::GET("codigo_erro{$numReg}"); 
		$this->mensagem_erro = Helper::GET("mensagem_erro{$numReg}"); 
		$this->url = Helper::GET("url{$numReg}"); 
		$this->arquivo_erro = Helper::GET("arquivo_erro{$numReg}"); 
		$this->linha_erro = Helper::GET("linha_erro{$numReg}"); 
		$this->get = Helper::GET("get{$numReg}"); 
		$this->post = Helper::GET("post{$numReg}"); 
		$this->session = Helper::GET("session{$numReg}"); 
		$this->stacktrace = Helper::GET("stacktrace{$numReg}"); 
		$this->datahora_DATETIME = Helper::GET("datahora_DATETIME{$numReg}"); 
		$this->status_BOOLEAN = Helper::GET("status_BOOLEAN{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_id_INT = $this->usuario_id_INT, ";

	} 

	if(isset($tipo["codigo_erro{$numReg}"]) || $tipo == null){

		$upd.= "codigo_erro = $this->codigo_erro, ";

	} 

	if(isset($tipo["mensagem_erro{$numReg}"]) || $tipo == null){

		$upd.= "mensagem_erro = $this->mensagem_erro, ";

	} 

	if(isset($tipo["url{$numReg}"]) || $tipo == null){

		$upd.= "url = $this->url, ";

	} 

	if(isset($tipo["arquivo_erro{$numReg}"]) || $tipo == null){

		$upd.= "arquivo_erro = $this->arquivo_erro, ";

	} 

	if(isset($tipo["linha_erro{$numReg}"]) || $tipo == null){

		$upd.= "linha_erro = $this->linha_erro, ";

	} 

	if(isset($tipo["get{$numReg}"]) || $tipo == null){

		$upd.= "get = $this->get, ";

	} 

	if(isset($tipo["post{$numReg}"]) || $tipo == null){

		$upd.= "post = $this->post, ";

	} 

	if(isset($tipo["session{$numReg}"]) || $tipo == null){

		$upd.= "session = $this->session, ";

	} 

	if(isset($tipo["stacktrace{$numReg}"]) || $tipo == null){

		$upd.= "stacktrace = $this->stacktrace, ";

	} 

	if(isset($tipo["datahora_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "datahora_DATETIME = $this->datahora_DATETIME, ";

	} 

	if(isset($tipo["status_BOOLEAN{$numReg}"]) || $tipo == null){

		$upd.= "status_BOOLEAN = $this->status_BOOLEAN, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE erro SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    