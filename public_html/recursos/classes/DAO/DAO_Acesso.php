<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Acesso
    * DATA DE GERA��O: 13.02.2018
    * ARQUIVO:         DAO_Acesso.php
    * TABELA MYSQL:    acesso
    * BANCO DE DADOS:  biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Acesso extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

	public $id;
	public $usuario_id_INT;
	public $obj;
	public $data_login_DATETIME;
	public $data_logout_DATETIME;


    public $nomeEntidade;

	public $data_login_DATETIME_UNIX;
	public $data_logout_DATETIME_UNIX;


    

	public $label_id;
	public $label_usuario_id_INT;
	public $label_data_login_DATETIME;
	public $label_data_logout_DATETIME;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "acesso";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
function getFkObjUsuario(){
	if($this->obj ==null){
		$this->obj = new EXTDAO_Usuario($this->getDatabase());
		if($this->usuario_id_INT != null) 
		$this->obj->select($this->usuario_id_INT);
	}
	return $this->obj ;
}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllUsuario($objArgumentos){

		$objArgumentos->nome="usuario_id_INT";
		$objArgumentos->id="usuario_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getFkObjUsuario()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_acesso", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_acesso", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getUsuario_id_INT()
    {
    	return $this->usuario_id_INT;
    }
    
    function getData_login_DATETIME_UNIX()
    {
    	return $this->data_login_DATETIME_UNIX;
    }
    
    public function getData_login_DATETIME()
    {
    	return $this->data_login_DATETIME;
    }
    
    function getData_logout_DATETIME_UNIX()
    {
    	return $this->data_logout_DATETIME_UNIX;
    }
    
    public function getData_logout_DATETIME()
    {
    	return $this->data_logout_DATETIME;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setUsuario_id_INT($val)
    {
    	$this->usuario_id_INT =  $val;
    }
    
    function setData_login_DATETIME($val)
    {
    	$this->data_login_DATETIME =  $val;
    }
    
    function setData_logout_DATETIME($val)
    {
    	$this->data_logout_DATETIME =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_login_DATETIME) AS data_login_DATETIME_UNIX, UNIX_TIMESTAMP(data_logout_DATETIME) AS data_logout_DATETIME_UNIX FROM acesso WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro() )
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);
		if($row == null) return false;
    
        $this->id = $row->id;
        
        $this->usuario_id_INT = $row->usuario_id_INT;
        if(isset($this->objUsuario))
			$this->objUsuario->select($this->usuario_id_INT);

        $this->data_login_DATETIME = $row->data_login_DATETIME;
        $this->data_login_DATETIME_UNIX = $row->data_login_DATETIME_UNIX;

        $this->data_logout_DATETIME = $row->data_logout_DATETIME;
        $this->data_logout_DATETIME_UNIX = $row->data_logout_DATETIME_UNIX;

		return null;
    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM acesso WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO acesso ( usuario_id_INT , data_login_DATETIME , data_logout_DATETIME ) VALUES ( {$this->usuario_id_INT} , {$this->data_login_DATETIME} , {$this->data_logout_DATETIME} )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoUsuario_id_INT(){ 

		return "usuario_id_INT";

	}

	public function nomeCampoData_login_DATETIME(){ 

		return "data_login_DATETIME";

	}

	public function nomeCampoData_logout_DATETIME(){ 

		return "data_logout_DATETIME";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoUsuario_id_INT($objArguments){

		$objArguments->nome = "usuario_id_INT";
		$objArguments->id = "usuario_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_login_DATETIME($objArguments){

		$objArguments->nome = "data_login_DATETIME";
		$objArguments->id = "data_login_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_logout_DATETIME($objArguments){

		$objArguments->nome = "data_logout_DATETIME";
		$objArguments->id = "data_logout_DATETIME";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->usuario_id_INT == ""){

			$this->usuario_id_INT = "null";

		}



	$this->data_login_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_login_DATETIME); 
	$this->data_logout_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_logout_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_login_DATETIME = $this->formatarDataTimeParaExibicao($this->data_login_DATETIME); 
	$this->data_logout_DATETIME = $this->formatarDataTimeParaExibicao($this->data_logout_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

	public function clear() {
		$this->id = null; 
		$this->usuario_id_INT = null; 
		$this->objUsuario= null;
		$this->data_login_DATETIME = null; 
		$this->data_logout_DATETIME = null; 

	}
	
    public function createSession(){

		Helper::setSession("id", $this->id); 
		Helper::setSession("usuario_id_INT", $this->usuario_id_INT); 
		Helper::setSession("data_login_DATETIME", $this->data_login_DATETIME); 
		Helper::setSession("data_logout_DATETIME", $this->data_logout_DATETIME); 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		Helper::clearSession("id");
		Helper::clearSession("usuario_id_INT");
		Helper::clearSession("data_login_DATETIME");
		Helper::clearSession("data_logout_DATETIME");


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = Helper::SESSION("id{$numReg}"); 
		$this->usuario_id_INT = Helper::SESSION("usuario_id_INT{$numReg}"); 
		$this->data_login_DATETIME = Helper::SESSION("data_login_DATETIME{$numReg}"); 
		$this->data_logout_DATETIME = Helper::SESSION("data_logout_DATETIME{$numReg}"); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = Helper::POST("id{$numReg}"); 
		$this->usuario_id_INT = Helper::POST("usuario_id_INT{$numReg}"); 
		$this->data_login_DATETIME = Helper::POST("data_login_DATETIME{$numReg}"); 
		$this->data_logout_DATETIME = Helper::POST("data_logout_DATETIME{$numReg}"); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = Helper::GET("id{$numReg}"); 
		$this->usuario_id_INT = Helper::GET("usuario_id_INT{$numReg}"); 
		$this->data_login_DATETIME = Helper::GET("data_login_DATETIME{$numReg}"); 
		$this->data_logout_DATETIME = Helper::GET("data_logout_DATETIME{$numReg}"); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = null, $numReg=1)
    {

	$upd="";
	if(isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == null){

		$upd.= "usuario_id_INT = $this->usuario_id_INT, ";

	} 

	if(isset($tipo["data_login_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_login_DATETIME = $this->data_login_DATETIME, ";

	} 

	if(isset($tipo["data_logout_DATETIME{$numReg}"]) || $tipo == null){

		$upd.= "data_logout_DATETIME = $this->data_logout_DATETIME, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE acesso SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    