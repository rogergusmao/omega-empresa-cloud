<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 12/02/2018
 * Time: 09:48
 */
class BO_Atualiza_bancos
{

    private $objSPVP ;
    private $db ;
    private $objPVBB;
    private $objPVPE;
    private $objPVC ;
    private $objTPE ;
    private $objPVBBTeste ;
    private $objPVTeste ;
    private $objPE ;
    private $dadosPrototipoWeb;
    private $dadosPrototipoAndroid;
    private $dadosPrototipoSincronizadorWeb;
    private $dadosWebParaAndroid;
    private $dadosWebParaSincronizacao;
    private $idProjeto = 34;

    public function factory(){
        return new BO_Atualiza_bancos();
    }

    public function __construct()
    {
        $this->init();
    }
    private function init(){
        if($this->db != null)
            $this->db->close();
        $this->db = new Database();
        $this->objSPVP = new EXTDAO_Sistema_projetos_versao_produto($this->db);
        $this->objPVBB = new EXTDAO_Projetos_versao_banco_banco($this->db);
        $this->objPVPE = new EXTDAO_Projetos_versao_processo_estrutura($this->db);
        $this->objPVC = new EXTDAO_Projetos_versao_caminho($this->db);
        $this->objTPE = new EXTDAO_Tipo_processo_estrutura($this->db);
        $this->objPVBBTeste = new EXTDAO_Projetos_versao_banco_banco($this->db);
        $this->objPVTeste = new EXTDAO_Projetos_versao($this->db);
        if($this->dadosPrototipoWeb==null){
            $this->dadosPrototipoWeb = EXTDAO_Trunk::getTipoAnalisePorDadosVersaoDoTrunk(
                $this->idProjeto,
                EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado,
                EXTDAO_Tipo_analise_projeto::PROTOTIPO,
                EXTDAO_Tipo_banco::$TIPO_BANCO_WEB,
                $this->db);

            $this->dadosPrototipoAndroid = EXTDAO_Trunk::getTipoAnalisePorDadosVersaoDoTrunk(
                $this->idProjeto,
                EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado,
                EXTDAO_Tipo_analise_projeto::PROTOTIPO,
                EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID,
                $this->db);
            $this->dadosPrototipoSincronizadorWeb = EXTDAO_Trunk::getTipoAnalisePorDadosVersaoDoTrunk(
                $this->idProjeto,
                EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado,
                EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web,
                EXTDAO_Tipo_banco::$TIPO_BANCO_WEB,
                $this->db);

            $this->dadosWebParaAndroid     = EXTDAO_Trunk::getTipoAnalisePorDadosVersaoDoTrunk(
                $this->idProjeto,
                EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado,
                EXTDAO_Tipo_analise_projeto::SINCRONIZACAO,
                EXTDAO_Tipo_banco::$TIPO_BANCO_WEB,
                $this->db);

            $idPVBBSinc = EXTDAO_Projetos_versao_banco_banco::getIdPVBBWebParaSincronizadorWebHmg($this->dadosPrototipoWeb[0], $this->db );
            $this->dadosWebParaSincronizacao = array($this->dadosPrototipoWeb[0], $idPVBBSinc);
        }


    }


    public function runGeraArquivoRegistros(){
        //MAPEIA O RELACIONAMENTO DO BANCO DE HOMOLOGACAO COM O DE PRODUCAO DO SINCRONIZADOR WEB
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::GERAR_SCRIPT_SQL_DE_ATUALIZACAO_DOS_REGISTROS_DO_BANCO_DE_PRODUCAO,
                //TODO descomentar depois validar!
                //EXTDAO_Processo_estrutura::EXECUTAR_SCRIPT_DE_ATUALIZACAO_DOS_REGISTROS_DO_BANCO_DE_PRODUCAO
            )
            ,$this->dadosPrototipoWeb[1]
            , "[lj3b25i34i6bg4i56h]"
        );


        $script = $this->geraScript(
            $this->dadosPrototipoWeb[1]
            , "RegistrosDatabaseWebHmgParaPrd"
            , "[jkb3i56h3io5345g]");
        echo "Script gerado: $script";

        return $script;
    }


    public function runDatabaseWebHmgParaPrd(){
        //MAPEIA O RELACIONAMENTO DO BANCO DE HOMOLOGACAO COM O DE PRODUCAO DA WEB
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
                , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO
            )
            ,$this->dadosPrototipoWeb[1]
        );
        //MAPEIA O RELACIONAMENTO DO BANCO DE HOMOLOGACAO COM O DE PRODUCAO DA WEB
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
            , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
            , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
            , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
            )
            ,$this->dadosPrototipoWeb[1]
        );

        $script = $this->geraScript($this->dadosPrototipoWeb[1], "EstruturaSincronizadorWebHmgParaPrdAvulso");
        echo "Script gerado: $script";
    }

    public function runGeraScriptPrototipoWeb(){

        $script = $this->geraScript($this->dadosPrototipoWeb[1], "EstruturaSincronizadorWebHmgParaPrdAvulso");
        echo "Script gerado: $script";

    }

    public function run(){

//        $this->geraClassesAntigas();
//        exit();


        //TODO chamar para todos os id PVBB
        //EXTDAO_Projetos_versao_processo_estrutura::atualizaEstruturaProjetosVersao($idPVBB, $idPV){


        //http://localhost/bibliotecanuvem/BN10003Corporacao/public_html/adm/index.php?
        //page=menu_banco_banco&tipo=pages&
        //&projetos_versao_processo_estrutura=112
        //&id_projetos_versao_banco_banco=30
        //&id_projetos_versao=25

        //MAPEA OS DADOS DA WEB DE HMG
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
            ,EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO
            )
            ,$this->dadosPrototipoWeb[1]
        );

        //MAPEA OS DADOS DA ANDROID DE HMG
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
                ,EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO
            )
            ,$this->dadosPrototipoAndroid[1]
        );

        //MAPEIA RELACIONAMENTO ENTRE WEB E ANDROID
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
            ,EXTDAO_Processo_estrutura::CARREGAR_CONFIGURACOES_SINCRONIZACAO

            )
            ,$this->dadosWebParaAndroid[1]
        );


        //AQUI O PROCEDIMENTO PARA! O USUARIO DEVE CONFIGURAR AS TABELAS DE SINCRONIZACAO
        //http://localhost/bibliotecanuvem/BN10003Corporacao/public_html/adm/index.php?tipo=lists&page=gerencia_sincronizacao_tabela&&id_projetos=&id_projetos_versao_banco_banco=31&id_projetos_versao=26&
        //
        //
    }



    private function procedimentoBancoSincronizadorWeb(){
        $ret=array();

        //MAPEA OS DADOS DA WEB DE HMG
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
                ,EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO

            )
            ,$this->dadosPrototipoSincronizadorWeb[1]
            , "[ve45tk6n934w56]"
        );


        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::MAPEANDO_ESTRUTURA_DOS_BANCOS_HMG_SINC_E_WEB
                ,EXTDAO_Processo_estrutura::CONFIGURANDO_RELACIONAMENTO_ENTRE_WEB_COM_SINCRONIZADOR_WEB
                ,EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
            )
            ,$this->dadosWebParaSincronizacao[1]
            , "[wrmt906]"
        );

        $script = $this->geraScript($this->dadosWebParaSincronizacao[1], "EstruturaWebParaSincronizadorWeb");
        $ret["scriptEstruturaWebParaSincronizadorWeb"]=$script;
        $ret["erro"] = false;
        if($script != null && $script){
            //IGUALANDO O BANCO WEB COM O SINCRONIZADOR WEB
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::ATUALIZA_ESTRUTURA_BANCO_DE_PROD
                    , EXTDAO_Processo_estrutura::CRIA_ATRIBUTOS_CRUD_MOBILE_E_CRUD
                )
                ,$this->dadosWebParaSincronizacao[1]
                , "[3n5694576]"
            );

            //UMA VEZ QUE A ESTRUTURA MUDOU, TEMOS QUE REMAPEAR
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
                    , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO
                )
                ,$this->dadosPrototipoSincronizadorWeb[1]
                , "[34m5n76u4nh57]"
            );
        }

        //MAPEIA O RELACIONAMENTO DO BANCO DE HOMOLOGACAO COM O DE PRODUCAO DO SINCRONIZADOR WEB
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
                , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
                , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
                , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
            )
            ,$this->dadosPrototipoSincronizadorWeb[1]
            , "[mk76j45n7n57]"
        );

        $script = $this->geraScript(
            $this->dadosWebParaSincronizacao[1]
                , "EstruturaSincronizadorWebHmgParaPrd"
                , "[w�kreno4w5n6]");

        $ret["scriptEstruturaSincronizadorWebHmgParaPrd"]=$script;
        if($script != null && $script){
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::ATUALIZA_ESTRUTURA_BANCO_DE_PROD
                )
                ,$this->dadosPrototipoSincronizadorWeb[1]
                , "[e3m564n57n57]"
            );

            //MAPEIA O RELACIONAMENTO DO BANCO DE HOMOLOGACAO COM O DE PRODUCAO DO SINCRONIZADOR WEB
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
                    , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
                    , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
                    , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
                )
                ,$this->dadosPrototipoSincronizadorWeb[1]
                , "[srtetmyo5e675]"
            );

            $script = $this->geraScript(
                    $this->dadosWebParaSincronizacao[1]
                    , "EstruturaSincronizadorWebHmgParaPrd"
                    , "[ok4354etht5eujr67yuh68u]");
            if($script != null && $script){
                echo "ERRO sincronizador_web nao atualizou a estrutura do banco prd corretamente!";
                $ret["erro"] = true;
            }
            //VALIDA SE DE FATO A ALTERACAO DO BANCO FOI SATISFATORIA
        }


        return $ret;
    }
    public function runBancoAndroidWebHmgParaPrd(){
        $ret = array();
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
                , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
                , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
            )
            , $this->dadosPrototipoAndroid[1]
            ,"[87we/r5]"
        );
//        $this->executaProcessosEstrutura(
//            array(
//                EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
//            )
//            ,$this->dadosPrototipoAndroid[1]
//            ,"[wpoojer50q9234u]"
//        );
//
//        //TODO verificar se esse foi o script sqlite ou outro gerado
//        $script = $this->geraScript(
//            $this->dadosWebParaAndroid[1]
//                , "EstruturaAndroidHmgParaPrdSqlite"
//                , "[4�k5m6o45no5n67l5k6]");
//        $ret["scriptEstruturaAndroidHmgParaPrdSqlite"]=$script;

        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::GERAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_DO_BANCO_DE_PRODUCAO
            )
            , $this->dadosPrototipoAndroid[1]
            ,"[�ksemr6tioh456h]"
        );
        $script = $this->geraScript(
            $this->dadosPrototipoAndroid[1]
            , "EstruturaAndroidHmgParaPrd"
            , "[oifjh3945yh39tuh4i]");
        $ret["scriptEstruturaAndroidHmgParaPrd"]=$script;

        if($script != null && $script){
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::EXECUTAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_BANCO_PRD
                )
                ,$this->dadosPrototipoAndroid[1]
                ,"[�w4ije5034u5]"
            );
        }
        return $ret;
    }

    public function run2(){

        $ret = array();
        $ret["erro"] = false;


        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::PREPARAR_ESTRUTURA_PARA_ANALISE
            ,EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM

            )
            ,$this->dadosWebParaAndroid[1]
            , "[afnoawejr]"
        );
        $script = $this->geraScript(
            $this->dadosWebParaAndroid[1]
                , "EstruturaWebParaAndroid"
        , "[lkn495ht945h6tjn]");
        $ret["scriptEstruturaWebParaAndroid"] =$script;
        //Atualizando o banco android hmg

        $this->executaProcessoEstrutura(
          EXTDAO_Processo_estrutura::ATUALIZA_ESTRUTURA_BANCO_DE_PROD
            ,$this->dadosWebParaAndroid[1]
            ,"[3f435t]"
        );

//        se houver altracao estrutura
        if($script != null){
            if($script ){
                //REMAPEA OS DADOS WEB
                $this->executaProcessosEstrutura(
                    array(
                        EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
                    ,EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO

                    )
                    ,$this->dadosPrototipoWeb[1]
                    ,"[54sadf54]"
                );

                //REMAPEA OS DADOS DA ANDROID DE HMG
                $this->executaProcessosEstrutura(
                    array(
                        EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
                    ,EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_HOMOLOGACAO

                    )
                    ,$this->dadosPrototipoAndroid[1]
                    ,"[564aw9er8twaer]"
                );

                //REMAPEIA RELACIONAMENTO ENTRE WEB E ANDROID
                $this->executaProcessosEstrutura(
                    array(
                        EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
                    ,EXTDAO_Processo_estrutura::CARREGAR_CONFIGURACOES_SINCRONIZACAO

                    )
                    ,$this->dadosWebParaAndroid[1]
                    ,"[wkor45h2983u4]"
                );

            }
            else
                throw new Exception("Falha ao gerar o script de atualizacao EstruturaWebParaAndroid");
        }

        $retSW= $this->procedimentoBancoSincronizadorWeb();

        $retAndroid = $this->runBancoAndroidWebHmgParaPrd();

        $this->executaProcessosEstrutura(
            array(
                //EXTDAO_Processo_estrutura::GERAR_ARQUIVOS_CODIGO_FONTE_ANDROID
                 EXTDAO_Processo_estrutura::SALVAR_CONFIGURACAO_SINCRONIZACAO_ATUAL_NOS_BANCOS_DE_HOMOLOGACAO
            )
            ,$this->dadosWebParaAndroid[1]
            ,"[wklmt934w5]"
        );

        //RETORNANDO PARA O BANCO WEB

        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
                , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
                , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
                , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
            )
            ,$this->dadosPrototipoWeb[1]
            ,"[�wl�m5o3w4]"
        );

        $script = $this->geraScript($this->dadosPrototipoWeb[1], "EstruturaWebHmgParaPrd");
        $ret["scriptEstruturaWebHmgParaPrd"]=$script;
        if($script != null && $script){
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::ATUALIZA_ESTRUTURA_BANCO_DE_PROD
                )
                , $this->dadosPrototipoWeb[1]
                ,"[awrlmt50690]"
            );
        }

        //verifica se houve script de alteracao estrutural
        if($this->checkRet($ret)){
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
                    , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
                    , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
                    , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
                )
                ,$this->dadosPrototipoWeb[1]
                ,"[aw�kme4tr98w34u5]"
            );
//
            $script = $this->geraScript(
                $this->dadosPrototipoWeb[1]
                , "verificacao__EstruturaWebHmgParaPrd"
                , "[n345j3vj5b34o534io]");
            if($script != nul && $script){
                echo "ERRO! O segundo script deveria estar vazio.";
                $ret["erro"] = true;
            }

            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO
                    , EXTDAO_Processo_estrutura::MAPEAR_BANCO_DE_PRODUCAO
                    , EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
//                    , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
                )
                ,$this->dadosPrototipoAndroid[1]
                ,"[wkmenrt93i4hj69h45]"
            );
//            $script = $this->geraScript(
//                $this->dadosPrototipoAndroid[1]
//                , "verificacao__EstruturaAndroidHmgParaPrdSqlite"
//                , "[b345jk3i5h34oi5h]");
//
//            if($script != nul && $script){
//                echo "ERRO! O segundo script deveria estar vazio.";
//                $ret["erro"] = true;
//            }

            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::GERAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_DO_BANCO_DE_PRODUCAO
                )
                ,$this->dadosPrototipoAndroid[1]
                ,"[awlmrt64wj56]"
            );
            $script = $this->geraScript(
                $this->dadosPrototipoAndroid[1]
                    , "verificacao__EstruturaAndroidHmgParaPrd"
                    , "[lklnewrtoiw435o6ijh46j]");
            if($script != nul && $script){
                echo "ERRO! O terceiro script deveria estar vazio.";
                $ret["erro"] = true;
            }
        }

        if(!$ret["erro"]
            && !$retSW["erro"]
            && !$retAndroid["erro"]){

            $this->run2SalvarConfiguracoes();
            $this->runGeraArquivoRegistros();
        }
    }

    public function run2SalvarConfiguracoes(){
        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::SALVAR_CONFIGURACAO_SINCRONIZACAO_ATUAL_NOS_BANCOS_DE_HOMOLOGACAO
                , EXTDAO_Processo_estrutura::SALVAR_CONFIGURACAO_SINCRONIZACAO_ATUAL_NOS_BANCOS_DE_PRODUCAO
            )
            ,$this->dadosWebParaAndroid[1]
            ,"[kmweg945n67]"
        );
    }

    public function run2GerarCodigoFonte(){
        //EXECUTARE DEPOISD E RUN

        $this->executaProcessosEstrutura(
            array(
                EXTDAO_Processo_estrutura::PREPARAR_ESTRUTURA_PARA_ANALISE
                , EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
            )
            ,$this->dadosWebParaAndroid[1]
        );
        $script = $this->geraScript(
            $this->dadosWebParaAndroid[1]
            , "__checkWebParaAndroidRun2GerarCodigoFonte");

        if($script != null && $script ){
            echo "O banco android continua apresentando difen�as do banco web para as tabelas em comum";
        }
        else{
            $this->executaProcessosEstrutura(
                array(
                    EXTDAO_Processo_estrutura::GERAR_ARQUIVOS_CODIGO_FONTE_ANDROID
                )
                ,$this->dadosWebParaAndroid[1]
            );
        }

    }

    private function executaProcessosEstrutura(array $idsProcessoEstrutura, $idPVBB, $marcador = ''){
        if(strlen($marcador)){
            echo "executaProcessosEstrutura - Inicio - marcador $marcador @#$%\n";
        }

        foreach ($idsProcessoEstrutura as $idPE ){
            echo "executaProcessosEstrutura $idPE @#$%\n";

            $this->cicloProcessoEstrutura(
                $idPVBB
                , EXTDAO_Projetos_versao_processo_estrutura::getIdRelativoAoProcessoEstrutura(
                $idPVBB
                ,$idPE
                , $this->db)
            );
        }

        if(strlen($marcador)){
            echo "executaProcessosEstrutura - Fim - marcador $marcador @#$%\n";
        }
    }
    private function executaProcessoEstrutura($idProcessoEstrutura, $idPVBB, $marcador = ''){
        if(strlen($marcador ))
            echo "executaProcessoEstrutura - Inicio - marcador: $marcador ";
        echo "executaProcessosEstrutura $idProcessoEstrutura @#$%\n";
        $ret = $this->cicloProcessoEstrutura(
            $idPVBB
            , EXTDAO_Projetos_versao_processo_estrutura::getIdRelativoAoProcessoEstrutura(
            $idPVBB
            ,$idProcessoEstrutura
            , $this->db)
        );
        if(strlen($marcador)){
            echo "executaProcessoEstrutura - Fim - marcador $marcador @#$%\n";
        }
        return $ret;
    }

    private function checkRet($ret){
        foreach ($ret as $k=>$v){
            if($k == "erro")
                continue;
            if($v!=null && $v)
                return true;
        }
        return false;
    }
    private function geraScript($idPVBB, $nomeArquivo, $marcador = ''){
        if(strlen($marcador))
            echo "geraScript - Inicio - marcador $marcador";
        $q = " SELECT scb.consulta
        FROM script_comando_banco scb
            JOIN script_tabela_tabela stt
                ON scb.script_tabela_tabela_id_INT = stt.id
            JOIN script_projetos_versao_banco_banco spvbb
                ON stt.script_projetos_versao_banco_banco_id_INT = spvbb.id
                
         WHERE spvbb.projetos_versao_banco_banco_id_INT=$idPVBB 
        ORDER BY scb.script_tabela_tabela_id_INT, scb.seq_INT";
        $this->db->query($q);
        $rs = Helper::getResultSetToMatriz($this->db->result);
        if(count($rs)==0) return null;
        $path = Helper::acharRaiz()."conteudo/scripts/projetos_versao_banco_banco_$idPVBB";
        Helper::mkdir($path);
        $dh= Helper::getDataHoraNomeArquivo();
        $pathArquivo = "{$path}/{$nomeArquivo}_{$dh}.sql";
        echo "Script $pathArquivo gerado!";

        $token = "";
        for($i = 0 ; $i < count($rs); $i++){
            $token .= $rs[$i][0].";\n";
        }
        $ret1= Helper::criaArquivoComOConteudo($pathArquivo,$token);
        if(strlen($marcador))
            echo "geraScript - Fim - marcador $marcador";
        return $ret1;


    }

    public function cicloProcessoEstrutura(
        $idPVBB,
        $idPVPE
    ){

        for($k = 0 ; $k < 10; $k++){
            try{
                $primeiraVez = true;
                $idPVCaminhoAtual = EXTDAO_Projetos_versao_processo_estrutura::getIdPrimeiraEtapa($idPVPE, $this->db);
                if(!EXTDAO_Projetos_versao_processo_estrutura::reinicializaCicloNaEtapa(
                    $idPVPE, $idPVCaminhoAtual, $this->db))
                    throw new Exception("Nao foi possivel reinical o ciclo $idPVPE");
                do{
                    $this->init();
                    if($this->objPVBB->getId() != $idPVBB)
                        $this->objPVBB->select($idPVBB);

                    $this->objPVPE->select($idPVPE);
                    $this->objPE = $this->objPVPE->getObjProcessoEstrutura($this->db);

                    if($this->objPVC->getId() != $idPVCaminhoAtual){
                        $this->objPVC->clear();
                        $this->objPVC->select($idPVCaminhoAtual);
                    }

                    $idTPE = $this->objPVC->getTipoProcessoEstrutura();
                    if($this->objTPE->getId() != $idTPE)
                        $this->objTPE->select($idTPE);


                    $indiceAtual = 0;
                    $vetorId = array();
                    $outProcessoConcluido = false;

                    while(!$outProcessoConcluido){

                        $outCicloAtual = 0;

                        $idPVBBTeste = null;
                        if($this->objPE->getUtiliza_copia_projetos_versao_BOOLEAN() == "1"){
                            if(!strlen($this->objPVPE->getCopia_projetos_versao_banco_banco_id_INT())){
                                //inicializa a copia a ser utilizada no processo
                                EXTDAO_Projetos_versao::cadastraCopiaProjetosVersao(
                                    $idPVBB, $this->objPVPE, $this->db);
                                if($this->objPVPE->getId() !=  $idPVPE)
                                    $this->objPVPE->select($idPVPE);
                            }
                            $idPVBBTeste = $this->objPVPE->getCopia_projetos_versao_banco_banco_id_INT();
                        } else
                            $idPVBBTeste = $idPVBB;

                        if($this->objPVBBTeste->getId() != $idPVBBTeste){
                            $this->objPVBBTeste->select($idPVBBTeste);
                            $idPVTeste = $this->objPVBBTeste->getProjetos_versao_id_INT();
                            $this->objPVTeste->select($idPVTeste);
                        }

                        echo "\ncicloProcessoEstrutura. TipoProcesso estrutura $idTPE @#$%\n";

                        $proc = BO_Atualiza_bancos::factoryProcessoEstrutura($idTPE);

                        $statusProcesso = $proc->executa_pagina_processo(
                            $this->objPVPE
                            , $this->objPVTeste
                            , $this->objPVBBTeste
                            , $vetorId
                            , $indiceAtual
                            , $idPVCaminhoAtual);

                        if( $statusProcesso->outProcessoConcluido != true){
                            if(isset($statusProcesso->tipo) && $statusProcesso->tipo == "TIPO_ITERACAO"){
                                $vetorId = $statusProcesso->totalIteracao;
                                $indiceAtual = $statusProcesso->indiceAtual;
                                $outProcessoConcluido = false;
                                $outCicloAtual = $indiceAtual;
                            } else {
                                $vetorId = $statusProcesso->vetorId;
                                $indiceAtual = $statusProcesso->indiceAtual;
                                if($indiceAtual >= count($vetorId)){
                                    $outProcessoConcluido = true;
                                }
                                $outCicloAtual = $indiceAtual;
                            }
                        } else $outProcessoConcluido = true;

                        $this->objPVPE->registraCiclo(
                            $outCicloAtual
                            , Helper::vetorToString($vetorId, ",")
                            , null);
                    }
                    if($this->objPVPE->existeProximaEtapa()){
                        //$idNovo
                        $idPVCaminhoAtual = EXTDAO_Projetos_versao_processo_estrutura::proximaEtapa(
                            $this->objPVPE->getId());
                        if(!$idPVCaminhoAtual)
                            break;
                        else continue;
                    }
                    else{
                        $this->objPVC->finalizaEtapa();
                        EXTDAO_Projetos_versao_processo_estrutura::finalizaCiclo($this->objPVPE->getId());
                        break;
                    }
                }while(true);
                break;
            }catch(DatabaseException $db){
                $this->init();
                echo Helper::getDescricaoException($db);

            }

        }

    }

    private function geraClassesAntigas(){
        $q = "SELECT id, nome, pagina FROM tipo_processo_estrutura";
        $db = new Database();
        $db->query($q);
        $rs = Helper::getResultSetToMatriz($db->result);

        $arqs= Helper::getListaArquivoDoDiretorio("C:\wamp64\www\BibliotecaNuvem\BN10003Corporacao\public_html\adm\pages\\");
        $phpIndex = "<?";
        $phpEnd = "?>";
        $endClass=  "}";

        $switch = "";
        $const = "";
        for($i = 0 ; $i < count($arqs); $i++){
            $file = Helper::getNomeDoArquivoEmPathCompleto($arqs[$i]);
            $encontrou = false;
            $strTag = "";
            $id = "";
            for($j = 0 ; $j < count($rs); $j++){
                $pagina = $rs[$j][2];
                if(strpos($pagina, $file) > 0 ){
                    $strTag= $rs[$j][1];

                    $strTag = Helper::somenteLetrasENumeros($strTag);
                    $strTag = str_replace(" ", "_", $strTag );

                    $strTag = Helper::strtoupperlatin1( $strTag );

                    $id = $rs[$j][0];
                    $encontrou = true;
                    break;
                }
            }
            if(!$encontrou) continue;
            $phpSufix = strpos($file, "." );
            $class = substr($file, 0, $phpSufix );
            $class = Helper::ucFirstLatin1($class);

            $switch .= "
                    case EXTDAO_Processo_estrutura::$strTag:
                        return new $class();
                    ";
            $const .= "
                    const $strTag = $id;
                    ";
            $content= file_get_contents($arqs[$i]);
            if(strpos( $content, "executa_pagina_processo") > 0 ){
                $iEnd = strrpos ($content, $endClass);
                $p = strpos( $content, $phpIndex ) + 3;
                if($p >= 0 ){
                    $f = "<?php 
                        class $class {
                            public function factory(){
                                return new $class ();
                            }
                        ";

                    $f .= substr(
                        $content
                        , $p + strlen($phpIndex)
                        , $iEnd  - ($p + strlen($phpIndex)) + 1);


                    $f .= "
                    } 
                    ?>";
                    file_put_contents("c:/out/$class.php", $f);
                }
            }
        }

        $funcaoFactory = " public static function factoryProcessoEstrutura(\$idPE){
            switch (\$idPE){
                {$switch}
                default:
                    throw new Exception(\"Fabrica nao definida para o tipo \$idPE\");
            }
        }";
        file_put_contents("c:/out/factory.php", $funcaoFactory);
        file_put_contents("c:/out/const.php", $const);

        exit();
    }

    public static function factoryProcessoEstrutura($idPE){

        switch ($idPE){

            case EXTDAO_Tipo_processo_estrutura::ATUALIZAR_CHAVES_UNICAS_SISTEMATABELA_DO_BANCO_SINCRONIZADOR_WEB_PROD:
                return new Atualizar_atributos_chave_unica_sincronizador();

            case EXTDAO_Tipo_processo_estrutura::ANEXAR_ARQUIVOS_OBRIGATORIOS_NA_APK:
                return new Processo_anexar_arquivos_obrigatorios_na_apk();

            case EXTDAO_Tipo_processo_estrutura::APAGAR_RELACIONAMENTO_ENTRE_OS_ATRIBUTOS_DO_BANCO_DE_HOMOLOGAO_E_PRODUO_DA_ANLISE:
                return new Processo_apaga_atributo_atributo_projetos_versao_banco_banco();

            case EXTDAO_Tipo_processo_estrutura::APAGAR_ATRIBUTOS_DO_BANCO_DE_HOMOLOGAO:
                return new Processo_apaga_atributo_banco_homologacao();

            case EXTDAO_Tipo_processo_estrutura::APAGAR_ATRIBUTOS_DO_BANCO_DE_PRODUO:
                return new Processo_apaga_atributo_banco_producao();

            case EXTDAO_Tipo_processo_estrutura::APAGA_MAPEAMENTO_ODS_ATRIBUTOS:
                return new Processo_apaga_atributo_projetos_versao_banco_banco();

            case EXTDAO_Tipo_processo_estrutura::APAGA_SCRIPT_ANTIGO_DE_ATUALIZACAO_REGISTRO:
                return new Processo_apaga_script_atualizacao_registro_banco();

            case EXTDAO_Tipo_processo_estrutura::APAGA_SCRIPT_ANTIGO_DE_ATUALIZAO_DE_ESTRUTURA:
                return new Processo_apaga_script_banco();

            case EXTDAO_Tipo_processo_estrutura::APAGAR_TABELAS_DO_BANCO_DE_HOMOLOGAO:
                return new Processo_apaga_tabela_banco_homologacao();

            case EXTDAO_Tipo_processo_estrutura::APAGAR_TABELAS_DO_BANCO_DE_PRODUO:
                return new Processo_apaga_tabela_banco_producao();

            case EXTDAO_Tipo_processo_estrutura::APAGAR_RELACIONAMENTO_ENTRE_AS_TABELAS_DO_BANCO_DE_HOMOLOGAO_E_PRODUO_DA_ANLISE:
                return new Processo_apaga_tabela_projetos_versao_banco_banco();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZAR_CHAVES_UNICAS_SISTEMATABELA_DO_BANCO_ANDROID_PROD:
                return new Processo_atualizar_atributos_chave_unica_banco_android();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZAR_CHAVES_UNICAS_SISTEMATABELA_DO_BANCO_WEB_PROD:
                return new Processo_atualizar_atributos_chave_unica_banco_web();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZAR_CHAVES_UNICAS_SISTEMATABELA_DO_BANCO_SINCRONIZADOR_WEB_PROD:
                return new Processo_atualizar_atributos_chave_unica_sincronizador();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZA_BANCO_SQLITE_UTILIZADO_NA_PRIMEIRA_SINCRONIZAO_DOS_APP_ANDROID:
                return new Processo_atualizar_banco_sqlite_da_primeira_sincronizacao_do_app();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZA_DUMP_SQL_DOS_BANCOS_WEB_E_SINCRONIZADORWEB_PARA_CRIACAO_DE_ASSINATURA:
                return new Processo_atualizar_dump_sql_dos_bancos_web_e_sincronizador_web_prod();

            case EXTDAO_Tipo_processo_estrutura::EXECUTAR_SCRIPT_DE_ATUALIZAO_ESTRUTURA_DO_BANCO_DE_PRODUO:
                return new Processo_atualiza_banco_hom_para_prod();

            case EXTDAO_Tipo_processo_estrutura::CARREGA_RELACIONAMENTO_ENTRE_AS_CHAVES_DAS_TABELAS_DE_HOMOLOGAO_E_PRODUO:
                return new Processo_atualiza_chave_das_tabelas();

            case EXTDAO_Tipo_processo_estrutura::EXECUTAR_SCRIPT_DE_ATUALIZAO_DOS_REGISTROS_DO_BANCO_DE_PRODU:
                return new Processo_atualiza_registros_banco_hom_para_prod();

            case EXTDAO_Tipo_processo_estrutura::CARREGAR_ESTRUTURA_SINCRONIZAO__PREENCHE_OS_DADOS:
                return new Processo_carregar_estrutura_sincronizacao();

            case EXTDAO_Tipo_processo_estrutura::CARREGAESTRUTURAATRIBUTO:
                return new Processo_carrega_estrutura_atributo();

            case EXTDAO_Tipo_processo_estrutura::MAPEAR_RELACIONAMENTO_ENTRE_OS_ATRIBUTOS_ATRIBUTOATRIBUTO:
                return new Processo_carrega_estrutura_atributo_atributo();

            case EXTDAO_Tipo_processo_estrutura::CARREGAR_ATRIBUTOS_DO_BANCO_DE_HOMOLOGAO:
                return new Processo_carrega_estrutura_atributo_homologacao();

            case EXTDAO_Tipo_processo_estrutura::CARREGAR_ATRIBUTOS_DO_BANCO_DE_PRODUO:
                return new Processo_carrega_estrutura_atributo_producao();

            case EXTDAO_Tipo_processo_estrutura::CARREGAESTRUTURATABELA:
                return new Processo_carrega_estrutura_tabela();

            case EXTDAO_Tipo_processo_estrutura::CARREGAR_TABELAS_DO_BANCO_DE_HOMOLOGAO:
                return new Processo_carrega_estrutura_tabela_homologacao();

            case EXTDAO_Tipo_processo_estrutura::CARREGAR_TABELAS_DO_BANCO_DE_PRODUO:
                return new Processo_carrega_estrutura_tabela_producao();

            case EXTDAO_Tipo_processo_estrutura::MAPEAR_RELACIONAMENTO_ENTRE_AS_TABELAS_TABELATABELA:
                return new Processo_carrega_estrutura_tabela_tabela();

            case EXTDAO_Tipo_processo_estrutura::CARREGA_LOG_IDENTIFICADOR:
                return new Processo_carrega_log_erro();

            case EXTDAO_Tipo_processo_estrutura::CARREGA_TIPO_OPERACAO_DE_ATUALIZAO_DO_ATRIBUTO:
                return new Processo_carrega_tipo_operacao_atualizacao_atributo();

            case EXTDAO_Tipo_processo_estrutura::CARREGA_TIPO_OPERACAO_DE_ATUALIZAO_DA_TABELA:
                return new Processo_carrega_tipo_operacao_atualizacao_tabela();

            case EXTDAO_Tipo_processo_estrutura::CRIA_OS_ATRIBUTOS_DE_CONTROLE_NAS_TABELAS_DO_SINCRONIZADORWEB:
                return new Processo_cria_atributos_controle_tabelas_sincronizador_web();

            case EXTDAO_Tipo_processo_estrutura::DELETA_CHAVES_EXTRANGEIRAS_DUPLICADAS_HOMOLOGACAO:
                return new Processo_deleta_chaves_extrangeiras_duplicadas_homologacao();

            case EXTDAO_Tipo_processo_estrutura::DELETA_CHAVES_EXTRANGEIRAS_DUPLICADAS_PRODUCAO:
                return new Processo_deleta_chaves_extrangeiras_duplicadas_producao();

            case EXTDAO_Tipo_processo_estrutura::FINALIZA_ATUALIZAO_DE_VERSO_DO_SISTEMA_MOBILE:
                return new Processo_finaliza_atualizacao_de_versao_do_sistema_mobile();

            case EXTDAO_Tipo_processo_estrutura::FINALIZA_COMPARAO_BANCO_DE_PRODUO_E_HOMOLOGA:
                return new Processo_finaliza_comparacao_banco_hom_prod();

            case EXTDAO_Tipo_processo_estrutura::GERAR_DATABASEJAVA_NO_CODIGO_FONTE_DO_APP_ANDROID:
                return new Processo_gerar_arquivo_database_no_codigo_fonte_android();

            case EXTDAO_Tipo_processo_estrutura::GERAR_DATABASEPHP_NO_CDIGO_FONTE_WEB_E_NO_CDIGO_FONTE_DO_SINCRONIZADORWEB:
                return new Processo_gerar_arquivo_database_no_codigo_fonte_web();

            case EXTDAO_Tipo_processo_estrutura::GERAR_DAOEXTDAO_DAS_TABELAS_DO_APP_ANDROID:
                return new Processo_gerar_dao_extdao_das_tabelas_android();

            case EXTDAO_Tipo_processo_estrutura::GERAR_DAOEXTDAO_DAS_TABELAS_DO_SISTEMA_WEB:
                return new Processo_gerar_dao_extdao_das_tabelas_do_sistema_web();

            case EXTDAO_Tipo_processo_estrutura::PROCESSO_GERAR_SCRIPTS_INSERT_NO_BANCO_WEB_PARA_ANDROID:
                return new Processo_gerar_scripts_insert_do_banco_web_para_android();

            case EXTDAO_Tipo_processo_estrutura::GERA_SCRIPT_DE_ATUALIZAO_DE_REGISTROS_DO_BANCO_DE_PRODUO:
                return new Processo_gera_script_atualizacao_registro_banco();

            case EXTDAO_Tipo_processo_estrutura::PROCESSO_GERA_SCRIPT_DE_ATUALIZAO_DO_BANCO_DE_PRODUO:
                return new Processo_gera_script_banco();

            case EXTDAO_Tipo_processo_estrutura::IGNORAR_FKS_DO_BANCO_WEB:
                return new Processo_ignorar_fks_do_banco_sincronizador_web_hmg();

            case EXTDAO_Tipo_processo_estrutura::IGNORAR_TABELAS_QUE_NO_SO_SINCRONIZADAS:
                return new Processo_ignorar_tabelas_que_nao_sao_sincronizadas();

            case EXTDAO_Tipo_processo_estrutura::IGNORAR_AS_TABELAS_DO_BANCO_WEB_MOBILE_QUE_NO_FAZEM_PARTE_DA_SINCRONIZAO:
                return new Processo_ignorar_tabelas_so_do_banco_mobile();

            case EXTDAO_Tipo_processo_estrutura::IGNORA_ATRIBUTOS_S_DO_BANCO_SINCRONIZADOR_WEB_HMG:
                return new Processo_ignora_atributos_so_do_banco_sincronizador_web_hmg();

            case EXTDAO_Tipo_processo_estrutura::IGNORA_TABELAS_S_DO_BANCO_SINCRONIZADOR_WEB_HMG:
                return new Processo_ignora_tabelas_so_do_banco_sincronizador_web_hmg();

            case EXTDAO_Tipo_processo_estrutura::INICIALIZA_ATUALIZAO_DE_VERSO_DO_SISTEMA_MOBILE:
                return new Processo_inicializa_atualizacao_de_versao_do_sistema_mobile();

            case EXTDAO_Tipo_processo_estrutura::INICIALIZA_COMPARAO_BANCO_DE_PRODUO_E_HOMOLOGA:
                return new Processo_inicializa_comparacao_banco_hom_prod();

            case EXTDAO_Tipo_processo_estrutura::INICIALIZAR_EXECUO_DO_SCRIPT_DE_ATUALIZAO_DA_ESTRUTURA_DO_MOBILE:
                return new Processo_inicializa_execucao_do_script_de_atualizacao_da_estrutura_do_mobile();

            case EXTDAO_Tipo_processo_estrutura::INICIALIZA_EXECUO_DO_SCRIPT_ATUALIZAO_REGISTRO_DO_BANCO_DE_PRODUO:
                return new Processo_inicializa_execucao_script_atualizacao_registro_banco();

            case EXTDAO_Tipo_processo_estrutura::INICIALIZA_EXECUO_DO_SCRIPT_ATUALIZAO_DA_ESTRUTURA_DO_BANCO_DE_PRODUO_0:
                return new Processo_inicializa_execucao_script_banco();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMAATRIBUTO_DO_BANCO_SINCRONIZADOR_WEB_HMG_:
                return new Processo_popular_sistema_atributo_do_banco_sincronizador_web();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMAATRIBUTO_DO_BANCO_SINCRONIZADOR_WEB_PROD:
                return new Processo_popular_sistema_atributo_do_banco_sincronizador_web_prod();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMAATRIBUTO_DO_BANCO_WEB_HMG_:
                return new Processo_popular_sistema_atributo_do_banco_web();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMAATRIBUTO_DO_BANCO_WEB_PRD:
                return new Processo_popular_sistema_atributo_do_banco_web_prod();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMATABELA_DO_BANCO_SINCRONIZADOR_WEB_HMG:
                return new Processo_popular_sistema_tabela_do_banco_sincronizador_web();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMATABELA_DO_BANCO_SINCRONIZADOR_WEB_PROD:
                return new Processo_popular_sistema_tabela_do_banco_sincronizador_web_prod();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMATABELA_DO_BANCO_WEB_HMG:
                return new Processo_popular_sistema_tabela_do_banco_web();

            case EXTDAO_Tipo_processo_estrutura::POPULAR_SISTEMATABELA_DO_BANCO_WEB_PRD:
                return new Processo_popular_sistema_tabela_do_banco_web_prod();

            case EXTDAO_Tipo_processo_estrutura::PUBLICA_VERSO_DO_SISTEMA_MOBILE:
                return new Processo_publica_versao_do_sistema_mobile();

            case EXTDAO_Tipo_processo_estrutura::SALVAR_ARQUIVOS_SCRIPT_SQL:
                return new Processo_salvar_arquivos_script_sql();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZA_OS_DADOS_DOS_REGISTROS_ATRIBUTO_DO_BANCO_DE_HOMOLOGAO_PARA_PRODUO:
                return new Processo_simula_atualizacao_atributo_atributo_hmg_para_prd();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZA_OS_DADOS_DOS_REGISTROS_TABELACHAVE_DO_BANCO_DE_HOMOLOGAO_PARA_PRODUO:
                return new Processo_simula_atualizacao_chave_chave_hmg_para_prd();

            case EXTDAO_Tipo_processo_estrutura::ATUALIZA_OS_DADOS_DOS_REGISTROS_TABELA_DO_BANCO_DE_HOMOLOGAO_PARA_PRODUO:
                return new Processo_simula_atualizacao_tabela_tabela_hmg_para_prd();

            case EXTDAO_Tipo_processo_estrutura::USURIO_INFORMA_DADOS_OBRIGATRIOS_PARA_PUBLICAO_DA_APK:
                return new Processo_usuario_informa_dados_para_publicacao_mobile();

            case EXTDAO_Tipo_processo_estrutura::USURIO_INFORMA_OS_DADOS_DO_APLICATIVO:
                return new Processo_usuario_informa_os_dados_do_aplicativo();

            case EXTDAO_Tipo_processo_estrutura::STATUS_OPERAO_SISTEMA_MOBILE:
                return new Status_operacao_sistema_mobile();

            default:
                throw new Exception("Fabrica nao definida para o tipo $idPE");
        }
    }
}