<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_SIHOP
 *
 * @author home
 */
class BO_SIHOP
{
    

    public static function removerDumpDosBancosModelosDoSistema($idSistema)
    {

        try {

            //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=consultaHospedagensDoUsuario&id_sistema=1&email=nave%40n.com.br
            $url = DOMINIO_DE_ACESSO_SERVICOS_SIHOP . "removerDumpDosBancosModelosDoSistema";
            $objSistema = new EXTDAO_Sistema($idSistema);
            $objSistema->select($idSistema);
            $idSistemaBibliotecaNuvem = $objSistema->getId_sistema_sihop_INT();
            $json = Helper::chamadaCurlJson($url, array("id_sistema_biblioteca_nuvem"), array($idSistemaBibliotecaNuvem));
            return $json;

        } catch (Exception $ex) {

            return new Mensagem(null, null, $ex);

        }

    }

}
