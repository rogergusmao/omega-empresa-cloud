<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Controlador_mobile{
    //put your code here
    public function conectaTelefone(
            $imei, 
            $identificador, 
            $idSPVSPMobile, 
            $idUsuario, 
            $idCorporacao,
            $marca,
            $modelo,
            $cpu,
            $nomeCorporacao){
        if(strlen($imei) 
            && strlen($identificador) ){
            $db = new Database();
            Registry::add($db);
            //Cadastrando Telefone
            $boM = new BO_Mobile();
            $vRet = $boM->cadastraSeNecessario(
                    $imei, 
                    $identificador,
                    $marca,
                    $modelo,
                    $cpu,
                    $db);
                
            if($vRet->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                return $vRet;
            
            $vRetSimples = new Mensagem_token();
            $vRetSimples = $vRet;
            $idMobile = $vRetSimples->mValor; 
            
            $boMPV = new BO_Mobile_identificador();
            $vRet = $boMPV->cadastraSeNecessario($idMobile, $idSPVSPMobile);
            
            if($vRet->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                return $vRet;
        
            $vRetSimples = new Mensagem_token();
            $vRetSimples = $vRet;
            $idMobileIdentificador = $vRetSimples->mValor; 
            
            $boMC = new BO_Mobile_conectado();
            $vRet = $boMC->cadastraSeNecessario(
                $idMobileIdentificador, 
                $idUsuario, 
                $idCorporacao, 
                $nomeCorporacao);
            
            $vProtocolo = Protocolo_conecta_telefone::constroi(array($idMobile , $vRet->mValor, $idMobileIdentificador, $idUsuario, $idCorporacao));
            return new Mensagem_protocolo(
                $vProtocolo,
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, 
                null );
        } else
            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                HelperMensagem::GERAL_PARAMETRO_INVALIDO("conectaTelefone", array($imei, $identificador, $idSPVSPMobile, $idCorporacao, $idUsuario))
            );
    }
    
    public function consulta($imei){
        if(strlen($imei)){
            $id = EXTDAO_Mobile::consultaTelefone($imei);
            if(strlen($id))
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $id);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
    
    
}
