<?php 
                        class Processo_carrega_estrutura_tabela_producao {
                            public function factory(){
                                return new Processo_carrega_estrutura_tabela_producao ();
                            }
                        


function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $trunk = Trunk::factory();
    $idPVBB = $trunk->getPVBBDoBancoDeDadosDeProducao($objPVBB->getId());
    $idPVTabela = $trunk->getPVDoBancoDeDadosDeProducao($objPV->getId());
    
    $objPVBBTabela = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBTabela->select($idPVBB);

    $idBB = $objPVBBTabela->getBanco_banco_id_INT();
    $objBB = new EXTDAO_Banco_banco();
    $objBB->select($idBB);

    //Se a analise atual for do tipo ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB
    //que possui os bancos
    //HMG -> Banco Web Hmg do Prototipo web
    //PRD -> Banco Sincronizador Web Hmg do Tipo Analise Atualiza Banco MySql
    //
    //Temos nesse caso a seguinte situa��o: todas as tabelas do banco Sincronizador Web 
    //que se relacionam com o banco de homologa��o possuem o prefixo "__"
    switch ($objPV->getTipo_analise_projeto_id_INT()) {
        case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
            
            //setando o prefixo das tabelas do Banco Sincronizador Web referentes as tabelas do Banco Web
            $objBB->setPrefixoNomeTabelaProducao("__");
            break;
        default:
            break;
    } 
    
    //carregando o estado dos atributos
    if ($vetorId == null || empty($vetorId)) {
        $vetorId = array(0, 1 );
        $indiceAtual = 0;
    }
    $vMensagem = "";
    for ($i = 0; $i < 1; $i++) {
        if ($indiceAtual < count($vetorId)) {

            //carregando a estrutura da tabela
            switch ($indiceAtual) {
              case 0:

    //TODO responsavel por popular as tabela 'tabela' relativo ao banco de homologacao do respectivo tipo e projeto
                    //nesse caso � realizada a copia da estrutura do banco de produ��o para de homologa��o
                    $objBB->populaTabelaDoProjetoVersaoDoBancoDeProducao($idPVTabela);
                    $vMensagem = "Carregando tabelas";
                    break;
              
                case 1:
                    //Retira as tabelas inexistentes no banco e que estao cadastradas 
                    //na tabela 'Tabela' da BibliotecaNuvem
                    //=> toda tupla tabela_tabela tb eh retirada
                    $objBB->retiraTabelasInexistentes($idPVTabela, false, true);
                    $vMensagem = "Retirando tabelas inexistente.";
                    break;

                default:
                    $vMensagem = "Estado n�o mapeado";
                    break;
            }
        }
        if ($indiceAtual >= count($vetorId)) {

            break;
        } else {
            $indiceAtual += 1;
        }
    }

    Helper::imprimirMensagem($vMensagem);
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>