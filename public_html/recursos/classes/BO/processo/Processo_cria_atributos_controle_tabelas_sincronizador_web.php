<?php 
                        class Processo_cria_atributos_controle_tabelas_sincronizador_web {
                            public function factory(){
                                return new Processo_cria_atributos_controle_tabelas_sincronizador_web ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    //banco sincronizador web =(equivalente a) banco de produ��o
    
//    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $db = new Database ();
    $databaseSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoProducao($objPVBB->getId(), $db);
    $idBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($objPVBB->getId(), $db);
    $idsTabelasSoDoSincronizadorWeb = EXTDAO_Tabela::getTabelasComPrefixo($idBancoSincronizadorWeb, $objPVBB->getProjetos_versao_id_INT(), "__", $db);
    if(count($idsTabelasSoDoSincronizadorWeb)){
        $objAA = new EXTDAO_Atributo_atributo();
        for($i = 0 ; $i < count($idsTabelasSoDoSincronizadorWeb); $i++){
            
            $idTabelaEspelho = $idsTabelasSoDoSincronizadorWeb[$i][0];
            $tabelaEspelho = $idsTabelasSoDoSincronizadorWeb[$i][1];
            $idAtributoCrudMobile = EXTDAO_Atributo::getIdAtributo("crud_mobile_id_INT", $idTabelaEspelho);
            if(!strlen($idAtributoCrudMobile)){
                try {
                    $databaseSincronizadorWeb->query("SELECT crud_mobile_id_INT FROM $tabelaEspelho LIMIT 0,1 ");    
                } catch (Exception $exc) {
                    $databaseSincronizadorWeb->queryMensagem("ALTER TABLE $tabelaEspelho
  ADD crud_mobile_id_INT int(11)");
                }
                $aleatorio = rand(0, 10000000);
                    
                $databaseSincronizadorWeb->queryMensagem("ALTER TABLE $tabelaEspelho 
                    ADD  CONSTRAINT fk_crud_mobile_$aleatorio 
                        FOREIGN KEY(crud_mobile_id_INT)
REFERENCES crud_mobile (id) ON UPDATE CASCADE ON DELETE CASCADE ");                
            }
        }
    }
    
    Helper::imprimirMensagem("Os atributos foram criados com sucesso.");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
                    } 
                    ?>