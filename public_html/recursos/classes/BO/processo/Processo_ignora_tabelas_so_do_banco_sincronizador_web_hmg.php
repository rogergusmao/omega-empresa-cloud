<?php 
                        class Processo_ignora_tabelas_so_do_banco_sincronizador_web_hmg {
                            public function factory(){
                                return new Processo_ignora_tabelas_so_do_banco_sincronizador_web_hmg ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    //banco sincronizador web =(equivalente a) banco de produ��o
    
    
    $idBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($objPVBB->getId());
    
    //ignorando as tabelas de controle do banco de dados do ''sincronizador web'  
    $idsTabelasSoDoSincronizadorWeb = EXTDAO_Tabela::getTabelasSemPrefixo($idBancoSincronizadorWeb, $objPVBB->getProjetos_versao_id_INT(), "__", $objPVBB->database);
    if(count($idsTabelasSoDoSincronizadorWeb)){
        $objTT = new EXTDAO_Tabela_tabela();
        for($i = 0 ; $i < count($idsTabelasSoDoSincronizadorWeb); $i++){
            $idSoDoSincronizadorWeb = $idsTabelasSoDoSincronizadorWeb[$i];
            $idTabelaTabela = EXTDAO_Tabela_tabela::getIdTabelaTabelaDaTabelaProducao($objPVBB->getId(), $idSoDoSincronizadorWeb);
            
            if(strlen($idTabelaTabela)){
                
                $objTT->select($idTabelaTabela);
                $objTT->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE);
                $objTT->formatarParaSQL();
                $objTT->update($idTabelaTabela);
            }
        }
    }
    
    $db = new Database();
    $q = "UPDATE tabela_tabela"
        . " SET tipo_operacao_atualizacao_banco_id_INT = ".EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE
        . " WHERE producao_tabela_id_INT IS NOT NULL "
        . "         AND homologacao_tabela_id_INT IS NOT NULL "
        . "         AND projetos_versao_banco_banco_id_INT = ".$objPVBB->getId();
    
    $db->query($q);
    
    
    
    Helper::imprimirMensagem("Os relacionamentos entre tabelas do banco mobile que n�o ser�o sincronizadas foram ignorados da an�lise.");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
                    } 
                    ?>