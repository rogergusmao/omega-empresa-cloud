<?php 
                        class Processo_popular_sistema_atributo_do_banco_web_prod {
                            public function factory(){
                                return new Processo_popular_sistema_atributo_do_banco_web_prod ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB,
    $totalIteracao, $indiceAtual, $idPVC){

   
    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: sistema_projetos_versao_produto
    * DATA DE GERAÇÃO:    17.06.2013
    * ARQUIVO:            sistema_projetos_versao_produto.php
    * TABELA MYSQL:       sistema_projetos_versao_produto
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    //O projeto_versao_banco_banco é banco web -> banco_android
    
    
    //gerarBancoWeb("1", $objPVBB->getId());
//        print_r($indiceAtual);
//    exit();
    //Atualiza em sistema_tabela@banco_web os dados contidos em tabela@biblioteca_nuvem
            //relativa a sincronizacao
    $indiceAtual = !is_numeric($indiceAtual) ? 0 : $indiceAtual;
    $db = new Database();
    //$objPVBB->getFkObjBanco_banco()->atualizaSistemaTabelaDoBancoDeProducao($objPVBB->getProjetos_versao_id_INT());
    $rs = EXTDAO_Projetos_versao_banco_banco::getIdPVePVBBPrototipoAndroidDoProjetosVersao(
        $objPVBB->getProjetos_versao_id_INT(), 
        $db);
//    print_r($rs);
    $idPVAndroid = $rs[0];
    $idPVBBAndroid = $rs[1];
    
    //$objPVBB->select($idPVBBAndroid);
    $objBancoAndroid = EXTDAO_Projetos_versao_banco_banco::getObjBancoHomologacao($idPVBBAndroid, $db);

    $rs = EXTDAO_Projetos_versao_banco_banco::getIdPVePVBBPrototipoWebDoProjetosVersao(
        $objPVBB->getProjetos_versao_id_INT(), 
        $db);
//    print_r($rs);
    
    $idPVWeb = $rs[0];
    $idPVBBWeb = $rs[1];
//    echo "$idPVWeb::$idPVBBAndroid";
//    exit();
    $objBancoWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBWeb, $db);
    
    
    
    //Atualizando o sistema_tabela do banco sincronizador_web
    $idPVBBSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getVersaoSinc(
        $idPVBBWeb, 
        EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB, 
        $db);
    $objBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBSincronizadorWeb);

    $LIMITE_TABELAS_POR_ITERACAO = 20;
    
    
    
    $objProcesso = new Atualiza_sistema_atributo_do_banco_web(
        $objBancoWeb, 
        $objBancoAndroid->getId(), 
        $idPVWeb,
        $idPVAndroid,
        $db,
        $indiceAtual,
        $LIMITE_TABELAS_POR_ITERACAO);
    $tot = $objProcesso->executa();
    
    $varGET= Param_Get::getIdentificadorProjetosVersao();
    
    $totalIteracao = $objProcesso->getTotalIteracoes();
    $status = new stdClass();
    $status->totalIteracao = $totalIteracao;
    $status->tipo = "TIPO_ITERACAO";
    if($tot < $LIMITE_TABELAS_POR_ITERACAO || $totalIteracao < $LIMITE_TABELAS_POR_ITERACAO){

        $status->outProcessoConcluido = true;
        
        Helper::imprimirMensagem("O sistema_atributo do banco web foi populado com sucesso.");
    } else {
        $status->indiceAtual = $indiceAtual+ $tot;
        
        $status->outProcessoConcluido = false;
    }

    return $status;
}
                    } 
                    ?>