<?php 
                        class Processo_apaga_atributo_banco_producao {
                            public function factory(){
                                return new Processo_apaga_atributo_banco_producao ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    //Apaga os atributos do banco de homologacao do projetos_Versao_banco_banco
    //repassado na entrada
    $trunk = Trunk::factory();
    $idPVBB = $trunk->getPVBBDoBancoDeDadosDeProducao($objPVBB->getId());

    $objPVBBAtributo = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBAtributo->select($idPVBB);

    if(empty($vetorId)){

        
        
       $vetorId = EXTDAO_Atributo::getIdsAtributosDoProjetosVersao(
            $objPVBBAtributo->getProjetos_versao_id_INT(),
            $objPVBBAtributo->getBanco_banco_id_INT());
       
       $indiceAtual = 0;
    }


    $objA = new EXTDAO_Atributo();
    for($i = 0 ; $i < 200; $i++){
        
       if($indiceAtual < count($vetorId)){
           $idA = $vetorId[$indiceAtual];
           if(strlen($idA))
            $objA->delete($idA);
       }
       if($indiceAtual >= count($vetorId)){
           break;
       } else{
           $indiceAtual += 1;
       }
    }



    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." atributos apagados ate o momento.");
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>