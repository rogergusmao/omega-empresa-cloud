<?php 
                        class Processo_carrega_estrutura_atributo_atributo {
                            public function factory(){
                                return new Processo_carrega_estrutura_atributo_atributo ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $totalTabelaPorCiclo = 15;

    $idBB = $objPVBB->getBanco_banco_id_INT();
    $objBancoBanco = new EXTDAO_Banco_banco();
    $objBancoBanco->select($idBB);
    
    if(empty($vetorId)){
        //carregando a estrutura de atributos das tabelas
        //$objPV = new EXTDAO_Projetos_versao();
        switch ($objPV->getTipo_analise_projeto_id_INT()) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                $objBancoBanco->setPrefixoNomeTabelaProducao("__");
                break;
            default:
                break;
        } 

        
        $vetorId = EXTDAO_Tabela_tabela::getVetorIdTabelaTabela($objPVBB->getId(), $idBB);
        $indiceAtual = 0;
    }
    $db = new Database();
    $trunk = Trunk::factory();
    //$pvBancos = $trunk->getProjetosVersaoDoBancoDeDados($objPV->getId(), $db);
    $idPVAtributoHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($objPV->getId());
    
    $idPVAtributoProducao = $trunk->getPVDoBancoDeDadosDeProducao($objPV->getId());
//    $idPVAtributoProducao = $pvBancos['idPVTabelaProducao'];
//    $idPVAtributoHomologacao = $pvBancos['idPVTabelaHomologacao'];
  
    for($i = 0 ; $i < $totalTabelaPorCiclo; $i++){
        $objTabelaTabela = new EXTDAO_Tabela_tabela();
        $db = new Database();
        if($indiceAtual < count($vetorId)){
            $idTT = $vetorId[$indiceAtual];
            if(strlen($idTT)){
                $objTabelaTabela->select($idTT);
                $objBancoBanco->populaAtributoAtributoDoProjetoVersao(
                    $objPVBB->getId(), $objTabelaTabela,
                    $idPVAtributoHomologacao, $idPVAtributoProducao, $db);
                
            }
        }
        if($indiceAtual >= count($vetorId)){

            break;
        } else{
            $indiceAtual += 1;
        }
    }


    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." relacionamento(s) entre tabelas mapeada(s) at� o momento.");
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>