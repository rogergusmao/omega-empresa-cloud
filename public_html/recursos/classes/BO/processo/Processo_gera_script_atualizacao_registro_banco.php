<?php 
                        class Processo_gera_script_atualizacao_registro_banco {
                            public function factory(){
                                return new Processo_gera_script_atualizacao_registro_banco ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    
    if(empty($vetorId)){

       $vVetor = $objPVBB->getVetorTabelaTabela();
       $vetorId = $vVetor;
       $indiceAtual = 0;
    }

    
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB, 
        EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DE_REGISTROS_DE_HMG_PARA_PRD);
    if($indiceAtual < count($vetorId)){
        for($i = 0 ; $i < 20; $i++){

            if($indiceAtual < count($vetorId)){
                $vTabelaTabela = $vetorId[$indiceAtual];
                if(strlen($vTabelaTabela)){
                    $objGSBB->geraScriptAtualizaTuplas($vTabelaTabela);
                }
            }

            if($indiceAtual >= count($vetorId)){

                break;
            } else{
                $indiceAtual += 1;
            }
        }
    }

    if($indiceAtual >= count($vetorId)){
        $arqXML = $objGSBB->arquivarXML();

        $arqScript = $objGSBB->arquivarScriptSQL();
        //$objPVBB = new EXTDAO_Projetos_versao_banco_banco();
        $objPVBB->setScript_registros_banco_hom_para_prod_ARQUIVO($arqScript);
        //$objPVBB->setScript_sql_homologacao_para_producao_ARQUIVO($arqScript);
        $objPVBB->formatarParaSQL();
        $objPVBB->update($objPVBB->getId());
    } 

    Helper::imprimirMensagem($indiceAtual." relacionamento(s) entre tabelas mapeados at� o momento.");
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>