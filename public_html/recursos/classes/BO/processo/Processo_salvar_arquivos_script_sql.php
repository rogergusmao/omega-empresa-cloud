<?php 
                        class Processo_salvar_arquivos_script_sql {
                            public function factory(){
                                return new Processo_salvar_arquivos_script_sql ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB, 
        EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD);
   
    $arqXML = $objGSBB->arquivarXML();
    $arqScript = $objGSBB->arquivarScriptSQL();

    $objPVBB->setScript_estrutura_banco_hom_para_prod_ARQUIVO($arqScript);
    $objPVBB->setXml_estrutura_banco_hom_para_prod_ARQUIVO($arqXML);

    $objPVBB->formatarParaSQL();
    $objPVBB->update($objPVBB->getId());

    $pathXML = $objGSBB->pathDiretorio.$arqXML;
    $pathScript = $objGSBB->pathDiretorio.$arqScript;
    Helper::imprimirMensagem("Arquivos salvos.</br>XML: $pathXML</br>Script: $pathScript</br>");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
                    } 
                    ?>