<?php 
                        class Processo_apaga_tabela_banco_homologacao {
                            public function factory(){
                                return new Processo_apaga_tabela_banco_homologacao ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    //carregando o estado das tabelas
    $trunk = Trunk::factory();
    $idPVBB = $trunk->getPVBBDoBancoDeDadosDeHomologacao($objPVBB->getId());
    $db = new Database();
    $objPVBBTabela = new EXTDAO_Projetos_versao_banco_banco($db);
    $objPVBBTabela->select($idPVBB);
    if(empty($vetorId)){

        $objBancoHomologacao = EXTDAO_Projetos_versao_banco_banco::getObjBancoHomologacao($idPVBB, $db);
        
       $vetorId = EXTDAO_Tabela::getListaTabela(
           $objPVBBTabela->getProjetos_versao_id_INT(),
           $objBancoHomologacao->getId(),
           $db);
       
       $indiceAtual = 0;
    }


    $objA = new EXTDAO_Tabela();
    for($i = 0 ; $i < 200; $i++){
        
       if($indiceAtual < count($vetorId)){
           $idA = $vetorId[$indiceAtual];
           if(strlen($idA))
            $objA->delete($idA);
       }
       if($indiceAtual >= count($vetorId)){
           break;
       } else{
           $indiceAtual += 1;
       }
    }



    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." tabelas apagados ate o momento.");
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>