<?php 
                        class Processo_carrega_estrutura_atributo_producao {
                            public function factory(){
                                return new Processo_carrega_estrutura_atributo_producao ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $totalTabelaPorCiclo = 15;

    $trunk = Trunk::factory();
    $idPVBB = $trunk->getPVBBDoBancoDeDadosDeProducao($objPVBB->getId());
    $idPVTabela = $trunk->getPVDoBancoDeDadosDeProducao($objPV->getId());
    
    $objPVBBTabela = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBTabela->select($idPVBB);

    $idBB = $objPVBBTabela->getBanco_banco_id_INT();
    $objBancoBanco = new EXTDAO_Banco_banco();
    $objBancoBanco->select($idBB);
    
    if(empty($vetorId)){
        
        //Se a analise atual for do tipo ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB
        //que possui os bancos
        //HMG -> Banco Web Hmg do Prototipo web
        //PRD -> Banco Sincronizador Web Hmg do Tipo Analise Atualiza Banco MySql
        //
        //Temos nesse caso a seguinte situa��o: todas as tabelas do banco Sincronizador Web 
        //que se relacionam com o banco de homologa��o possuem o prefixo "__"
        switch ($objPV->getTipo_analise_projeto_id_INT()) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                
                $objBancoBanco->setPrefixoNomeTabelaProducao("__");
                break;
            default:
                break;
        } 
        $vetorId = EXTDAO_Tabela::getListaTabela(
            $idPVTabela,
            $objBancoBanco->getProducao_banco_id_INT());
        
        $indiceAtual = 0;
    }
    
    
    for($i = 0 ; $i < $totalTabelaPorCiclo; $i++){
        $objTabela= new EXTDAO_Tabela();
        $db = new Database();
        if($indiceAtual < count($vetorId)){
            $idT = $vetorId[$indiceAtual];
            if(is_numeric($idT)){
                $objTabela->select($idT);
                $objBancoBanco->inicializaEstruturaDeAtributoDaTabela($objPVBBTabela->getId(), $objTabela, false, $db);    
            }
            
            
        }
        if($indiceAtual >= count($vetorId)){
            break;
        } else{
            $indiceAtual += 1;
        }
    }


    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." tabelas cujos atributos foram mapeado(s) at� o momento.");
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>