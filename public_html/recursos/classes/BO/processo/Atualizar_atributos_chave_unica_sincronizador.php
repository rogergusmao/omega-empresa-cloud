<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 13/02/2018
 * Time: 14:28
 */
class Atualizar_atributos_chave_unica_sincronizador
{
    public function factory(){
        return new Atualizar_atributos_chave_unica_sincronizador ();
    }

    function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
//    $trunk = new Trunk();
        $db = new Database();
        $trunk = Trunk::factory();
        $idPVBBSincronizacao = $trunk->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
        $idPVBBWeb = $trunk->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
        $idPVWeb = $trunk->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::PROTOTIPO);


        EXTDAO_Projetos_versao_banco_banco::procedimentoAtualizaSistemaTabelaDoBancoProducao($idPVBBWeb, $idPVWeb, $db, $idPVBBSincronizacao);

        $status = new stdClass();
        $status->indiceAtual = $indiceAtual;
        $status->vetorId = $vetorId;
        $status->outProcessoConcluido = true;
        return $status;
//        $db = new Database();
//
//        $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($objPVBB->getId(), $db);
//        $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($objPVBB->getId(), $db);
//
//        $idPVBBSinc = EXTDAO_Projetos_versao_banco_banco::getIdPVBBWebParaSincronizadorWebHmg(
//            $objPVBB->getProjetos_versao_id_INT()
//            , $db );
//
//        $dbSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBSinc , $db);
//
//        $database->query("SHOW TABLES");
//        $tabelas = Helper::getResultSetToArrayDeUmCampo($database->result);
//
//        $objTabela = new EXTDAO_Tabela($db);
//        for($i = 0 ;$i < count($tabelas); $i++){
//
//            $table = $tabelas[$i];
//            $idTabela = EXTDAO_Tabela::existeTabela(
//                $table, $idBanco, $objPV->getId(), $db);
//            if(!strlen($idTabela)){
//                Helper::imprimirMensagem ("[$table] Tabela inexistente no banco de dados, atualize os dados do projeto no Biblioteca Nuvem", MENSAGEM_ERRO);
//            }
//            $objTabela->clear();
//            $objTabela->select($idTabela);
//
//            //$vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($objTabela->getId(), $db);
//            $idTabelaChave = EXTDAO_Tabela_chave::getIdDaChaveUnicaDaTabela($idTabela, $db);
//
//            if(strlen($idTabelaChave))
//                $vetorAtributoChaveUnica = EXTDAO_Tabela_chave_atributo::getNomesAtributosChaveUnica($idTabelaChave, $db);
//
//            $strChaveUnica = "";
//            if(count($vetorAtributoChaveUnica) > 0){
//
//                //public void setUniqueKey(String pVetorAttributeName[])
//
//                for($j = 0 ; $j < count($vetorAtributoChaveUnica); $j++){
//
//                    if($j > 0)$strChaveUnica .= ", ";
//                    $strChaveUnica .= $vetorAtributoChaveUnica[$j];
//                }
//            }
//
//            if(strlen($strChaveUnica)){
//                $q = "UPDATE sistema_tabela SET atributos_chave_unica = '$strChaveUnica' WHERE nome LIKE '$table'";
//                $dbSincronizadorWeb->query($q);
//                echo $q;
//                Helper::imprimirMensagem("[$table] Chaves atualizadas com sucesso", MENSAGEM_OK);
//            } else{
//                Helper::imprimirMensagem("[$table] A tabela n�o possui chaves unicas, isso � um problema para a solucao de duplicaidades durante a sincronizacao.",
//                    MENSAGEM_WARNING);
//            }
//        }
//        $status=new stdClass();
//        $status->indiceAtual = null;
//        $status->vetorId = null;
//        $status->outProcessoConcluido = true;
//        return $status;



    }
}