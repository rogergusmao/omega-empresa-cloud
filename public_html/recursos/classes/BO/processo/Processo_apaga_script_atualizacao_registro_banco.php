<?php 
                        class Processo_apaga_script_atualizacao_registro_banco {
                            public function factory(){
                                return new Processo_apaga_script_atualizacao_registro_banco ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){

    $objGSBB = new Gerador_script_banco_banco($objPVBB, EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DE_REGISTROS_DE_HMG_PARA_PRD);
    if(empty($vetorId)){
       $vetorId = EXTDAO_Script_tabela_tabela::getListaId($objGSBB->objScriptProjetosVersaoBancoBanco->getId());
       $indiceAtual = 0;
    }


    $objSTT = new EXTDAO_Script_tabela_tabela();
    for($i = 0 ; $i < 80; $i++){

       if($indiceAtual < count($vetorId)){
           $idSTT = $vetorId[$indiceAtual];
           if(strlen($idSTT))
            $objSTT->delete($idSTT);
       }

       if($indiceAtual >= count($vetorId)){
           break;
       } else{
           $indiceAtual += 1;
       }

    }



    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." comandos apagados ate o momento.");
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
                    } 
                    ?>