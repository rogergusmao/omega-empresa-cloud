<?php 
                        class Processo_carregar_estrutura_sincronizacao {
                            public function factory(){
                                return new Processo_carregar_estrutura_sincronizacao ();
                            }
                        
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    //carregando os registro da tabela "sistema_tabela" do banco de produção e copiando
    //no banco de homologação
    $db = new Database();
    $idPVBBAndroid = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoAndroidDoProjetosVersao($objPV->getId(), $db);
    $idPVBBWeb = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoWebDoProjetosVersao($objPV->getId(), $db);
    $idBancoAndroidHomologacao = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBAndroid, $db);
    $idBancoWebHomologacao = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBWeb, $db);
    
    
    $objBancoAndroidHmg = new EXTDAO_Banco();
    $objBancoWebHmg = new EXTDAO_Banco();
    
    $objBancoAndroidHmg->select($idBancoAndroidHomologacao);
    $objBancoWebHmg->select($idBancoWebHomologacao);
    
    $objPVBBAndroid = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBAndroid->select($idPVBBAndroid);
    $objPVBBWeb = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBWeb->select($idPVBBWeb);
    
    $objBancoBanco = new EXTDAO_Banco_banco();
    
    $objBancoBanco->atualizaTabelaComDadosDoSistemaTabela(
        $objPVBBWeb->getProjetos_versao_id_INT(), 
        $objPVBBAndroid->getProjetos_versao_id_INT(), 
        $objBancoWebHmg, 
        $objBancoAndroidHmg,
        true);
    
    $idBancoAndroidProducao= EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($idPVBBAndroid, $db);
    $idBancoWebProducao= EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($idPVBBWeb, $db);
    
    $objBancoAndroidPrd = new EXTDAO_Banco();
    $objBancoWebPrd = new EXTDAO_Banco();
    
    $objBancoAndroidPrd->select($idBancoAndroidProducao);
    $objBancoWebPrd->select($idBancoWebProducao);
    
    $objBancoBanco->atualizaTabelaComDadosDoSistemaTabela(
        $objPVBBWeb->getProjetos_versao_id_INT(), 
        $objPVBBAndroid->getProjetos_versao_id_INT(), 
        $objBancoWebPrd, 
        $objBancoAndroidPrd,
        false);
    
    Helper::imprimirMensagem("Carregar estrutura de sincronização.");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
                    } 
                    ?>