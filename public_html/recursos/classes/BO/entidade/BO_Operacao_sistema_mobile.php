<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Operacao_sistema_mobile extends Interface_BO_operacao_sistema_mobile{
    public function __construct($pIdOperacaoSistemaMobile = null) {
        parent::__construct($pIdOperacaoSistemaMobile);
    }
    
    public static function consultaOperacaoAguardando($pIdMobileIdentificador){
        
        $rs = EXTDAO_Operacao_sistema_mobile::consultaOperacaoSistemaPorEstado(
                $pIdMobileIdentificador, 
                EXTDAO_Estado_operacao_sistema_mobile::AGUARDANDO);
        
        $rss= Helper::getResultSetToMatriz($rs);
//        print_r($rss);
        for($i=0; $registro = $rss[$i]; $i++){
            $idOSM = $registro[0];
            $vVetorProtocolo = array();
        
//        foreach ($vVetorObj as $vObj) {
            $protocoloOSM = new Protocolo_operacao_sistema_mobile($idOSM);
            $protocoloOSMFilho = null;
            $validade = true;
            switch ($registro[1]) {
                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_DOWNLOAD_BANCO_MOBILE:
                    
                    $idOSMFilho = EXTDAO_Operacao_sistema_mobile::getIdOperacaoSistemaMobileFilho($idOSM, "operacao_download_banco_mobile");
                    $protocoloOSMFilho = new Protocolo_operacao_download_banco_mobile($idOSMFilho);
                    break;
                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_CRUD_ALEATORIO:
                    
                    $idOSMFilho = EXTDAO_Operacao_sistema_mobile::getIdOperacaoSistemaMobileFilho($idOSM, "operacao_crud_aleatorio");
                    $protocoloOSMFilho = new Protocolo_operacao_crud_aleatorio($idOSMFilho);
                    break; 
                
                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_EXECUTA_SCRIPT:
                    
                    $idOSMFilho = EXTDAO_Operacao_sistema_mobile::getIdOperacaoSistemaMobileFilho($idOSM, "operacao_executa_script");
                    $protocoloOSMFilho = new Protocolo_executa_script($idOSMFilho);
                    break; 
                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_SINCRONIZA:
                    
                    $idOSMFilho = null;
                    $protocoloOSMFilho = null;
                    break; 
                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_MONITORAR_TELA:
                    $objOSM = new EXTDAO_Operacao_sistema_mobile();
                    $objOSM->select($idOSM);
                    $totSecDataAbertura = $objOSM->getData_abertura_DATETIME_UNIX();
                    $now = new DateTime('now');
                    $dif= ($now->getTimestamp() - TEMPO_DE_ESPERA_PARA_INICIO_DO_MONITORAMENTO_DE_TELA);
                    if($totSecDataAbertura - $dif < 0){
                    //ja passou da hora permitida para aceitar a opera��o    
                        $objOSM->setEstado_operacao_sistema_mobile_id_INT(EXTDAO_Estado_operacao_sistema_mobile::CANCELADA);
                        $objOSM->setData_conclusao_DATETIME(Helper::getDiaEHoraAtualSQL());
                        $objOSM->formatarParaSQL();
                        $objOSM->update($idOSM);
                    }
                    
                    $idOSMFilho = null;
                    $protocoloOSMFilho = null;
                    $protocoloOSM = null;
                    break; 
                default:
                    break;
            }
            if($protocoloOSM != null){
            $protocoloOSM->objOperacao = $protocoloOSMFilho ;
            $vVetorProtocolo[count($vVetorProtocolo)] = $protocoloOSM;    
            }
            
//            A atualizacao da operacao nao pode ser realizada
//            $vObjEXTDAO = new EXTDAO_Operacao_sistema_mobile();
//            $vObjEXTDAO->select($idOSM);
//            $vObjEXTDAO->setData_processamento_DATETIME(Helper::getDiaEHoraAtualSQL());
//            $vObjEXTDAO->setEstado_operacao_sistema_mobile_id_INT(EXTDAO_Estado_operacao_sistema_mobile::PROCESSANDO);
//            $vObjEXTDAO->formatarParaSQL();
//            $vObjEXTDAO->update($idOSM);
        }
        if(count($vVetorProtocolo) > 0 )
            return new Mensagem_vetor_protocolo(null, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $vVetorProtocolo);    
        else
            return new Mensagem_vetor_protocolo(null, PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        
        
    }
    
     public static function atualizaEstadoOperacaoSistemaMobile( $idOSM, $idEstado){
        
        $vObjEXTDAO = new EXTDAO_Operacao_sistema_mobile();
        $vObjEXTDAO->select($idOSM);
        switch ($idEstado) {
            case EXTDAO_Estado_operacao_sistema_mobile::PROCESSANDO:
                $vObjEXTDAO->setData_processamento_DATETIME(Helper::getDiaEHoraAtualSQL());
                break;
            case EXTDAO_Estado_operacao_sistema_mobile::ERRO_EXECUCAO:
                $vObjEXTDAO->setData_conclusao_DATETIME(Helper::getDiaEHoraAtualSQL());
                break;
            case EXTDAO_Estado_operacao_sistema_mobile::CONCLUIDA:
                $vObjEXTDAO->setData_conclusao_DATETIME(Helper::getDiaEHoraAtualSQL());
                break;
            case EXTDAO_Estado_operacao_sistema_mobile::CANCELADA:
                $vObjEXTDAO->setData_conclusao_DATETIME(Helper::getDiaEHoraAtualSQL());
                break;
            
            default:
                break;
        }
        
        $vObjEXTDAO->setEstado_operacao_sistema_mobile_id_INT($idEstado);
        $vObjEXTDAO->formatarParaSQL();
        $vObjEXTDAO->update($idOSM);
        
        return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }

    protected function procedimentoExecuta() {
        
    }

    public function consultaOperacaoSistemaMobile() {
        
    }
    
    
    
    
}

