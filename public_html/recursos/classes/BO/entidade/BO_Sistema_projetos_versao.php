<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Sistema_projetos_versao{
    
    
    public function consultaUltimaVersaoDoSistema($idSistema){
        if(strlen($idSistema)){
            $id = EXTDAO_Sistema_projetos_versao::consultaUltimaVersaoDoSistema($idSistema);
            if(strlen($id))
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $id);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
    
}

