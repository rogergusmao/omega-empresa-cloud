<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Sistema_log_erro_produto{

    public static function getPathLogArquivo()
    {
        $pathRaiz = Helper::acharRaiz();
        $strPath = "{$pathRaiz}conteudo/arquivos/log_android/" ;
        if (!is_dir($strPath))
            mkdir($strPath, 0777, true);
        return $strPath;
    }

    public static function logArquivoErroProdutoMobile($idMI){
        try {
            if(isset($_FILES["logfile"])){
                $pathDestino = self::getPathLogArquivo();

                $nomeArquivo = "MI_{$idMI}_". Helper::getDiaNomeArquivo().".log";
                $objUpload = new Upload();
                $objUpload->arrPermitido = "";
                $objUpload->tamanhoMax = "";
                $objUpload->file = $_FILES["logfile"];
                $objUpload->nome = $nomeArquivo;
                $objUpload->uploadPath = $pathDestino;

                $success = $objUpload->uploadFile();
                if (!$success) {

                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        "[ewfvefrvg]Falha durante o download do arquivo: ".print_r($_FILES["logfile"],true));
                }
            }
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "A sincroniza��o mobile teve o estado atualizado.");

        }catch (Exception $exc) {
            return new Mensagem(null,null,$exc);
        }
    }

     public static function logErroProdutoMobile( $strJson){
        
        
        $strJson = str_replace("\\", "", $strJson);
        $json = json_decode($strJson);
        
        $mensagem = new Mensagem_vetor_protocolo(new Protocolo_log_erro_produto());
        $mensagem->inicializa($json);
        
        if($json->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
            if(count($mensagem->mVetorObj)){
                for($i = 0 ; $i < count($mensagem->mVetorObj); $i++){
                    $protocolo = $mensagem->mVetorObj[$i];
                    $protocolo->persiste();
                }
            }
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        } else{
            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                "Objeto JSON repassado no parametro ".Param_Get::ID_OBJ_JSON." � invalido.");
        }
        
    }
    
    
     public static function logErroProdutoScriptMobile( $strJson){
        $json = json_decode($strJson);
        $mensagem = new Mensagem_vetor_protocolo(new Protocolo_log_erro_produto());
        $mensagem->inicializa($json);
        if($json->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
            if(count($mensagem->mVetorObj)){
                for($i = 0 ; $i < count($mensagem->mVetorObj); $i++){
                    $protocolo = $mensagem->mVetorObj[$i];
                    $protocolo->persiste();
                }
            }
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        } else{
            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Objeto JSON repassado no parametro ".Param_Get::ID_OBJ_JSON." � invalido.");
        }
        
    }
    
}

