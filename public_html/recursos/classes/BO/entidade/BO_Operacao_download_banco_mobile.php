<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Operacao_download_banco_mobile extends Interface_BO_operacao_sistema_mobile{
    
    public $idODBM; 
    public $nomeTabela;
    public $idSistemaTipoDownloadArquivo;
    
    public function __construct($pIdOperacaoSistemaMobile,
                        $pNomeTabela, 
                        $pIdSistemaTipoDownloadArquivo){
        
        parent::__construct($pIdOperacaoSistemaMobile);
        
        $this->idODBM = EXTDAO_Operacao_download_banco_mobile::getIdOperacaoSistemaMobile($pIdOperacaoSistemaMobile); 
        $this->nomeTabela = $pNomeTabela;
        $this->idSistemaTipoDownloadArquivo = $pIdSistemaTipoDownloadArquivo;
    }

    protected function procedimentoExecuta(){

         $vRet= BO_Sistema_tipo_download_arquivo::transfereArquivoZipadoMobileParaWeb(
                        $this->idOperacaoSistemaMobile, 
                        $this->nomeTabela, 
                        $this->idSistemaTipoDownloadArquivo);
        
        switch ($vRet->mCodRetorno) {
            
            case PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO:
                
                $obj = new EXTDAO_Operacao_download_banco_mobile();
                $obj->select($this->idODBM);
                
                if(!empty($vRet->mVetor)){
                    foreach ($vRet->mVetor as $pathArquivo) {
                        if (strpos($pathArquivo,'.db') !== false) {
                            $arq = Helper::getNomeDoArquivoEmPathCompleto($pathArquivo);
                            
                            $obj->setPath_script_sql_banco($arq);
                            $obj->formatarParaSQL();
                            
                            $obj->update($this->idODBM);
                            $obj->select($this->idODBM);
                            break;
                        }
                    }
                    
                }
                
                $idDestinoBanco = $obj->getDestino_banco_id_INT();
                if(strlen($idDestinoBanco)){
                    
                    $objBanco = new EXTDAO_Banco();
                    $objBanco->select($idDestinoBanco);
//                    $database = new Database();
                    $database = $objBanco->getObjDatabase();
                    $pathDir = Helper::acharRaiz(). EXTDAO_Operacao_download_banco_mobile::getPathDiretorio($this->idOperacaoSistemaMobile);
                    $pathArquivo = $pathDir.$obj->getPath_script_sql_banco();
                    $database->executeScript($pathArquivo);
                }
                $vIdEstadoAtual = EXTDAO_Estado_operacao_sistema_mobile::CONCLUIDA;
                
                break;

            default:
                
                $vIdEstadoAtual = EXTDAO_Estado_operacao_sistema_mobile::ERRO_EXECUCAO;
                break;
        }
        
        if($vRet->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
            
            $vRet = new Mensagem_protocolo(
                        Protocolo_transfere_script_banco_android_para_web::constroi(
                            $vRet->mObj, 
                            $vIdEstadoAtual
                        ),        
                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, 
                        null
                    );
            return $vRet;
        }
        else return $vRet;
        
    }

}

