<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Mobile_conectado{
    
    
    //put your code here
    public function cadastra($idMobileContectado, $idUsuario, $idCorporacao, $db = null){
        if(strlen($idMobileContectado)){
            $objMC = new EXTDAO_Mobile_conectado($db);
            $objMC->setMobile_identificador_id_INT($idMobileContectado);
            
            $objMC->setData_login_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objMC->setData_atualizacao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objMC->setUsuario_INT($idUsuario);
            $objMC->setCorporacao_INT($idCorporacao);
            $objMC->formatarParaSQL();
            $objMC->insert();
            $objMC->selectUltimoRegistroInserido();
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $objMC->getId());
        }
        return new Mensagem_token(
            PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, 
            HelperMensagem::GERAL_FALHA_CADASTRO(HelperMensagem::ENTIDADE_MOBILE_CONECTADO, array($idMobileContectado))
        );
    }
    
    public function consulta($idMobileIdentificador, $idUsuario, $idCorporacao, $db = null){
        if(strlen($idMobileIdentificador)){
            $id = EXTDAO_Mobile_conectado::consulta($idMobileIdentificador, $idUsuario, $idCorporacao, $db);
            if(strlen($id))
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $id);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_FALHA_CONSULTA(HelperMensagem::ENTIDADE_MOBILE_CONECTADO, array($idMobileIdentificador)));
    }
    
    public function cadastraSeNecessario($idMobileIdentificador, $idUsuario, $idCorporacao, $corporacao, $db = null){
        if($db == null)
        $db = new Database();
        $boMC = new BO_Mobile_conectado();
        $vRet = $boMC->consulta($idMobileIdentificador, $idUsuario, $idCorporacao, $db);
        if($vRet->mCodRetorno == PROTOCOLO_SISTEMA::RESULTADO_VAZIO ){
            $vRet = $boMC->cadastra($idMobileIdentificador, $idUsuario, $idCorporacao, $db);

            if($vRet->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                return $vRet;
        }

        if(!empty($idCorporacao) && !EXTDAO_Corporacao::existeIdCorporacao($idCorporacao, $db)){
            $objC = new EXTDAO_Corporacao($db);
            $objC->setId($idCorporacao);
            $objC->setNome($corporacao);
            $objC->formatarParaSQL();
            $objC->insert();
        }
        return $vRet;

    }
    
    public static function isMobileConectado($idMobile){

        $consulta = "SELECT data_atualizacao_DATETIME
                    FROM mobile_conectado mc JOIN mobile_identificador mi 
                        ON mc.mobile_identificador_id_INT = mi.id
                    WHERE mi.mobile_id_INT = {$idMobile} 
                    ORDER BY data_atualizacao_DATETIME DESC
                    LIMIT 0,1";

        $db = new Database();
        $db->query($consulta);
        $data = $db->getPrimeiraTuplaDoResultSet("id");
        if(strlen($data)){
            $totSegAtualizacao = strtotime($data) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE;
            $totSegAtual = strtotime(Helper::getDiaEHoraAtualSQL());
            if($totSegAtualizacao < $totSegAtual)
                return true;
            else return false;
        } else return false;
    }

    
    public function atualizaConexao($idMobileConectado){

        if(strlen($idMobileConectado)){

            $objMobileConectado = new EXTDAO_Mobile_conectado();    
            $objMobileConectado->select($idMobileConectado);

            if(!strlen($objMobileConectado->getData_logout_DATETIME())){

                $objMobileConectado->setData_atualizacao_DATETIME(Helper::getDiaEHoraAtualSQL());
                $objMobileConectado->formatarParaSQL();
                $objMobileConectado->update($idMobileConectado);
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, null);

            }

            else return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::PROTOCOLO_SISTEMA_MOBILE_DESCONECTADO);

        }

        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);

    }
    
    public function desconectaTelefone($idMobileConectado){
        if(strlen($idMobileConectado)){
            $objMobileConectado = new EXTDAO_Mobile_conectado();    
            $objMobileConectado->select($idMobileConectado);
            if(!strlen($objMobileConectado->getData_logout_DATETIME())){
                $objMobileConectado->setData_logout_DATETIME(Helper::getDiaEHoraAtualSQL());
                $objMobileConectado->formatarParaSQL();
                $objMobileConectado->update($idMobileConectado);
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, null);
            }
            else return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::MOBILE_JA_ESTA_DESCONECTADO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
}
