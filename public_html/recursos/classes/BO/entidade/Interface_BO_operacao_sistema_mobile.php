<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
abstract class Interface_BO_operacao_sistema_mobile {
    public $idOperacaoSistemaMobile;
    
    public function __construct($idOperacaoSistemaMobile){
        $this->idOperacaoSistemaMobile = $idOperacaoSistemaMobile;
    }
    
    //public abstract function consultaOperacaoSistemaMobile();
    protected abstract function procedimentoExecuta();
    
    public function executa(){
        
        $vObj = new EXTDAO_Operacao_sistema_mobile();
        $vObj->select($this->idOperacaoSistemaMobile);

        $vRet = $this->procedimentoExecuta();
        
        if($vRet->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
            
            
            if($vRet->mObj != null && strlen($vRet->mObj->idEstado))
                $vObj->setEstado_operacao_sistema_mobile_id_INT($vRet->mObj->idEstado);
            $vObj->setData_conclusao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $vObj->formatarParaSQL();
            
            $vObj->update($this->idOperacaoSistemaMobile);
            return $vRet;
        }
        else return $vRet;
    }
    
    
}   

