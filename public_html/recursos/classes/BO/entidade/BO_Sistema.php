<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Sistema{
   
    public static function removerDumpDosBancosModelosDoSistema($idSistemaBibliotecaNuvem){
        try{
            $idSistema = EXTDAO_Sistema::getIdSistemaDoIdSistemaBibliotecaNuvem($idSistemaBibliotecaNuvem);
            if(!strlen($idSistema)){
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, 
                    "O sistema da biblioteca nuvem '$idSistemaBibliotecaNuvem' n�o foi mapeado no sistema de controle de hospedagem.");
            }
            $obj = new EXTDAO_Sistema($idSistemaBibliotecaNuvem);

            $msgRet = $obj->removerDumpDosBancosModelosDoSistema();
            return $msgRet;
        } catch (Exception $ex) {
            return new Mensagem(null,null,$ex);
        }
    }
    
        public static function getArvoreBancoOmegaEmpresa($idSistema){
            try {
                set_time_limit(0);
                $db = new Database();
                $objSistema = new EXTDAO_Sistema($db);
                $ret = $objSistema->select($idSistema);
                if($ret != null ){
                    if(!$ret)return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Sistema n�o cadastrado: $idSistema");
                    else if(is_object ($ret) && $ret->erro()) return $ret;
                }
                    
                
    //                tca.projetos_versao_id_INT, 
    //                tca.projetos_versao_banco_banco_id_INT,
                $dadosVersao = EXTDAO_Trunk::getTipoAnalisePorDadosVersaoDoTrunk(
                    $objSistema->getProjetos_id_INT(), 
                    EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado,
                    EXTDAO_Tipo_analise_projeto::PROTOTIPO,
                    EXTDAO_Tipo_banco::$TIPO_BANCO_WEB,
                    $db);
                
                HelperLog::verbose("[1] - getTipoAnalisePorDadosVersaoDoTrunk: ".print_r($dadosVersao, true));
                
                if(empty($dadosVersao))
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO, 
                        "Nenhuma dado encontrado no trunk para o sistema: $idSistema");
                
                $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($dadosVersao[1], $db);

                $idsTabelas = EXTDAO_Tabela::getHierarquiaTabelaBanco($dadosVersao[0], $idBanco);
                HelperLog::verbose("[2] - getTipoAnalisePorDadosVersaoDoTrunk: ".print_r($idsTabelas, true));
                
                if(empty($idsTabelas))
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Nenhuma tabela enontradao para o sistema: $idSistema");
                $dados = EXTDAO_Tabela::getDadosDasTabelas($idsTabelas, $db);
                HelperLog::verbose("[3] - getTipoAnalisePorDadosVersaoDoTrunk: ".print_r($dados, true));
                return new Mensagem_vetor_protocolo(null, null, null, $dados);
            } catch (Exception $exc) {
                return new Mensagem(null,null,$exc);
            }

        }
}

