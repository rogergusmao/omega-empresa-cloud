<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Sistema_projetos_versao_sistema_produto_mobile
 *
 * @author rogerfsg
 */
class BO_Sistema_projetos_versao_sistema_produto_mobile {
    //put your code here
    public static function donwloadApkAndroid($id){
        $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
        $obj->select($id);
        
        return BO_Sistema_tipo_download_arquivo::transfereArquivoWebParaMobile(
                $obj->getSistema_projetos_versao_produto_id_INT(),
                "sistema_projetos_versao_produto", 
                GerenciadorArquivo::ID_DEFAULT,
                $obj->getPacote_completo_zipado_ARQUIVO());
        
    }
    
    
    public function getListaVersao($idSistemaProdutoMobile, $idSistemaProjetosVersaoSistemaProdutoMobile){
        if(strlen($idSistemaProdutoMobile)){
            $vetorId = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getListaVersao(
                    $idSistemaProdutoMobile,
                    $idSistemaProjetosVersaoSistemaProdutoMobile);
            
            if(count($vetorId) > 0)
                return new Mensagem_vetor_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $vetorId);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "N�o existem vers�es a serem atualizadas");
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
    
    public function getUltimaVersao(
        $idSistemaProdutoMobile
            , $idSistemaProjetosVersaoSistemaProdutoMobile
            , $db = null){
        if(strlen($idSistemaProdutoMobile)){
            $vetorId = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getIdDaUltimaVersao(
                $idSistemaProdutoMobile
                ,   $idSistemaProjetosVersaoSistemaProdutoMobile
                , $db);
            
            if(count($vetorId) > 0)
                return new Mensagem_vetor_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $vetorId);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
}

