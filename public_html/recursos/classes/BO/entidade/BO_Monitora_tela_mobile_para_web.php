<?php
class BO_Monitora_tela_mobile_para_web {
    
    static public $PRINT_SCREEN_MOBILE_PARA_WEB = 1;
    public $idOperacaoSistemaMobile;
    public static function falhaPrintScreen(
            $idOperacaoSistemaMobile, 
            $idMobileIdentificador, 
            $seq,
            $programaAberto,
            $tecladoAtivo,
            $motivoErro) {
        
        
        try {
            
            $ext = new EXTDAO_Monitora_tela_mobile_para_web();
            $ext->setOperacao_sistema_mobile_id_INT($idOperacaoSistemaMobile);
            $ext->setTipo_operacao_monitora_tela_id_INT(EXTDAO_Tipo_operacao_monitora_tela::PRINT_SCREEN_MOBILE_PARA_WEB);
            $ext->setData_ocorrencia_DATETIME(Helper::getDiaEHoraAtualSQL());
            $ext->setMobile_identificador_id_INT($idMobileIdentificador);
            $ext->setSequencia_operacao_INT($seq);
            $ext->setPrograma_aberto_BOOLEAN($programaAberto);
            $ext->setTeclado_ativo_BOOLEAN($tecladoAtivo);
            $ext->setOcorreu_erro_BOOLEAN("1");
            $ext->setMotivo_erro($motivoErro);
            $ext->formatarParaSQL();
            $ext->insert();
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
         } catch (Exception $exc) {

            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, Helper::getMensagemExcecao($exc) ) ;
        }

        
    }
    public static function printScreenMobileParaWeb(
            $idOperacaoSistemaMobile, 
            $idMobileIdentificador, 
            $seq,
            $programaAberto,
            $tecladoAtivo,
            $pIdNaTabela, 
            $pNomeTabela, 
            $pIdSistemaTipoDownloadArquivo) {
        try {
            $ret = BO_Sistema_tipo_download_arquivo::tranfereArquivoMobileParaWeb($pIdNaTabela, $pNomeTabela, $pIdSistemaTipoDownloadArquivo);
            if($ret->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
    //            $mensagemProtocolo = new Mensagem_protocolo();
                $mensagemProtocolo = $ret;
    //            $objProtocoloArquivo = new Protocolo_arquivo();
                $objProtocoloArquivo = $mensagemProtocolo->mObj;
                $objProtocoloArquivo->nomeArquivo;
                $objProtocoloArquivo->pathArquivo;
                $ext = new EXTDAO_Monitora_tela_mobile_para_web();
                $ext->setOperacao_sistema_mobile_id_INT($idOperacaoSistemaMobile);
                $ext->setPrintscreen_ARQUIVO($objProtocoloArquivo->nomeArquivo);
                $ext->setTipo_operacao_monitora_tela_id_INT(EXTDAO_Tipo_operacao_monitora_tela::PRINT_SCREEN_MOBILE_PARA_WEB);
                $ext->setData_ocorrencia_DATETIME(Helper::getDiaEHoraAtualSQL());
                $ext->setMobile_identificador_id_INT($idMobileIdentificador);
                $ext->setSequencia_operacao_INT($seq);
                $ext->setPrograma_aberto_BOOLEAN($programaAberto);
                $ext->setTeclado_ativo_BOOLEAN($tecladoAtivo);
                $ext->formatarParaSQL();
                $ext->insert();

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            } else return $ret;

        } catch (Exception $exc) {

            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, Helper::getMensagemExcecao($exc) ) ;
        }
    }

}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
