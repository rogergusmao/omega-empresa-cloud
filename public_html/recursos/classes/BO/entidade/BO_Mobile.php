<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Mobile{
    //put your code here
    public function cadastra(
                        $imei, 
                        $identificador, 
                        $marca,
                        $modelo,
                        $cpu){
        if(strlen($imei)){
            $objM = new EXTDAO_Mobile();    
            $objM->setImei($imei);
            if(strlen($identificador))
                $objM->setIdentificador($identificador);
            
            if(strlen($marca))
                $objM->setMarca($marca);
            if(strlen($cpu))
                $objM->setCpu($cpu);
            if(strlen($modelo))
                $objM->setModelo($modelo);
                    
            $objM->formatarParaSQL();
            $objM->insert();
            $objM->selectUltimoRegistroInserido();
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $objM->getId());
        }
        return new Mensagem_token(
            PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, 
            HelperMensagem::GERAL_FALHA_CADASTRO(HelperMensagem::ENTIDADE_MOBILE, array($imei, $identificador))
        );
    }
    
    public function consulta($imei, $db = null){
        if(strlen($imei)){
            $id = EXTDAO_Mobile::consulta($imei, $db);
            if(strlen($id))
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $id);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
    
    public function cadastraSeNecessario(
            $imei, 
            $identificador,
            $marca,
            $modelo,
            $cpu,
            $db = null){
        $vRet = null;
        
        $boM = new BO_Mobile();
        $vRet = $boM->consulta($imei, $db);
        
        if($vRet->mCodRetorno == PROTOCOLO_SISTEMA::RESULTADO_VAZIO ){
            $vRet = $boM->cadastra(
                        $imei, 
                        $identificador, 
                        $marca,
                        $modelo,
                        $cpu);
            if($vRet->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                return $vRet;
        }

        return $vRet;
    }
}

?>
