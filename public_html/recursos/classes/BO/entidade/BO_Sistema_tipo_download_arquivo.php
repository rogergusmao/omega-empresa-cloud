<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Sistema_tipo_download_arquivo
 *
 * @author rogerfsg
 */
class BO_Sistema_tipo_download_arquivo {
    
    //put your code here
    //Exemplo:
    //$pIdNaTabela = 1 => id do usuario
    //$pNomeTabela = usuario
    //$pIdSistemaTipoDownloadArquivo = xml
    //$pNomeArquivo = script.xml
    public static function transfereArquivoWebParaMobile(
            $pIdNaTabela,
            $pNomeTabela,
            $pIdSistemaTipoDownloadArquivo,
            $pNomeArquivo){
        
        $path = GerenciadorArquivo::getPathDoTipo(
                $pIdNaTabela,
                $pNomeTabela,
                $pIdSistemaTipoDownloadArquivo);
        $raiz = Helper::acharRaiz();
        $pathArquivo = $raiz.$path.$pNomeArquivo;
        if(!is_file($pathArquivo))
            return new Mensagem_token(PROTOCOLO_SISTEMA::ARQUIVO_INEXISTENTE, HelperMensagem::GERAL_ARQUIVO_INEXISTENTE($pathArquivo));
        else{
            $objDownload = new Download($pathArquivo);
            $fp = $objDownload->df_download();
            if ($fp)
                print $fp;
            else
                return new Mensagem_token(PROTOCOLO_SISTEMA::ARQUIVO_INEXISTENTE, HelperMensagem::GERAL_ARQUIVO_INEXISTENTE($pathArquivo));
        }
        
    }
    
    public static function transfereDiretorioZipadoWebParaMobile(
            $pIdNaTabela,
            $pNomeTabela,
            $pIdSistemaTipoDownloadArquivo){
        
        $path = GerenciadorArquivo::getPathDoTipo(
                $pIdNaTabela,
                $pNomeTabela,
                $pIdSistemaTipoDownloadArquivo);
        $raiz = Helper::acharRaiz();
        $pathArquivo = $raiz.$path;
        
        
        $pathArquivo .= EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO;
        
        if(!is_file($pathArquivo))
            return new Mensagem_token(PROTOCOLO_SISTEMA::ARQUIVO_INEXISTENTE, HelperMensagem::GERAL_ARQUIVO_INEXISTENTE($pathArquivo));
        else{
            $objDownload = new Download($pathArquivo);
            $fp = $objDownload->df_download();
            if ($fp){
                print $fp;
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, NOME_APK_ANDROID);
            }
            else
                return new Mensagem_token(PROTOCOLO_SISTEMA::ARQUIVO_INEXISTENTE, HelperMensagem::GERAL_ARQUIVO_INEXISTENTE($pathArquivo));
        }
    }
    
    public static function tranfereArquivoMobileParaWeb(
                $pIdNaTabela,
                $pNomeTabela,
                $pIdSistemaTipoDownloadArquivo){
        try {
       
            $path = GerenciadorArquivo::getPathDoTipo(
                    $pIdNaTabela,
                    $pNomeTabela,
                    $pIdSistemaTipoDownloadArquivo);

            $raiz = Helper::acharRaiz();
            $pathDiretorio = $raiz.$path;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["logfile"];
            $objUpload->nome = $objUpload->file["name"];
            $objUpload->uploadPath = $pathDiretorio;

            $success = $objUpload->uploadFile();

            if (!$success) {
                return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, HelperMensagem::SISTEMA_TIPO_DOWNLOAD_FALHA_UPLOAD_ARQUIVO);
            } else {

                return new Mensagem_protocolo(
                        Protocolo_arquivo::constroi(array($objUpload->nome, $pathDiretorio),    
                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, 
                        null));
            }
        } catch (Exception $exc) {

            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, Helper::getMensagemExcecao($exc) ) ;
        }
    }
    public static function transfereArquivoZipadoMobileParaWeb(
                $pIdNaTabela,
                $pNomeTabela,
                $pIdSistemaTipoDownloadArquivo){

        $path = GerenciadorArquivo::getPathDoTipo(
                $pIdNaTabela,
                $pNomeTabela,
                $pIdSistemaTipoDownloadArquivo);
        
        $raiz = Helper::acharRaiz();
        
        $pathDiretorio = $raiz.$path;
        
        if (Helper::verificarUploadArquivo("logfile")) {
             if(! is_dir($pathDiretorio))
                mkdir($pathDiretorio, 0777, true);
             
            $nomeArquivo = $_FILES["logfile"]["name"];
        
             if(is_file($pathDiretorio.$nomeArquivo))
                 unlink ($pathDiretorio.$nomeArquivo);
             
            $vRet = BO_Sistema_tipo_download_arquivo::tranfereArquivoMobileParaWeb(
                    $pIdNaTabela,
                    $pNomeTabela,
                    $pIdSistemaTipoDownloadArquivo);  
            $vProtocoloArquivo = null;
            if($vRet->codRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
                
                $vProtocoloArquivo = $vRet->mObj;
            }
            
            $pathArquivoZip = $pathDiretorio.$nomeArquivo;
            
            $handlerZip = zip_open($pathArquivoZip);

            $strErro = "";
            $arquivos = array();
            while ($arquivoNoZip = zip_read($handlerZip)) {
                if (zip_entry_open($handlerZip, $arquivoNoZip)) {

                    $nomeArquivoNoZip = zip_entry_name($arquivoNoZip);

                    $diretorioArquivoNovo = $pathDiretorio.$nomeArquivoNoZip;
                    $arquivos[count($arquivos)] = $diretorioArquivoNovo;
                    if ($handlerArquivo = fopen($diretorioArquivoNovo, 'w')) {
                        fwrite($handlerArquivo, zip_entry_read($arquivoNoZip, zip_entry_filesize($arquivoNoZip)));
                        fclose($handlerArquivo);
                    }
                }
            }
            

            if (strlen($strErro)) {
            
                return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, $strErro);
            }
            elseif(is_file($pathArquivoZip)){
                
                unlink($pathArquivoZip);
                return new Mensagem_vetor_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $arquivos);
                
            } else {
                
                return  new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, HelperMensagem::SISTEMA_TIPO_DOWNLOAD_ARQUIVO_ARQUIVO_NAO_DELETADO);
            }
        } else {
            
            return new Mensagem_token(PROTOCOLO_SISTEMA::ARQUIVO_INEXISTENTE, HelperMensagem::SISTEMA_TIPO_DOWNLOAD_ARQUIVO_ARQUIVO_INVALIDO);
        }
            
            
    }
//            $nomeArquivo = "";
//            $raiz = Helper::acharRaiz();
//            
//            $pathDiretorio = $raiz.GerenciadorArquivo::getPathDoTipo(Helper::POST("tipo"),  Helper::POST("corporacao"), Helper::POST("usuario"));
//            
//            if(! is_dir($pathDiretorio))
//                mkdir($pathDiretorio, 0777, true);
//            
//            if (Helper::verificarUploadArquivo("logfile")) {
//                if (is_array($arquivo = DatabaseSincronizador::__uploadArquivo_zip_ARQUIVO("", $urlErro, $pathDiretorio))) {
//                    $nomeArquivo = $arquivo[0];
//                }
//            } else {
//                echo "NOTOK: O arquivo é invalido";
//                return;
//            }
//            
//            
//            $pathArquivoZip = $pathDiretorio.$nomeArquivo;
//            echo $pathArquivoZip;
//            echo "DIRETORIO ZIP: ".$pathDiretorio.";";
//            
//            $handlerZip = zip_open($pathArquivoZip);
//            
//            $strErro = "";
//            while ($arquivoNoZip = zip_read($handlerZip)) {
//                if (zip_entry_open($handlerZip, $arquivoNoZip)) {
//
//                    $nomeArquivoNoZip = zip_entry_name($arquivoNoZip);
//
//                    $nomeArquivo = $nomeArquivoNoZip;
//                    $diretorioImagens = $pathDiretorio.$nomeArquivo;
//
//                    if ($handlerArquivo = fopen($diretorioImagens, 'w')) {
//                        fwrite($handlerArquivo, zip_entry_read($arquivoNoZip, zip_entry_filesize($arquivoNoZip)));
//                        fclose($handlerArquivo);
//                    }
//                }
//            }
//
//            if (!strlen($strErro)) {
//                echo "TRUE";
//            } else {
//                echo "NOTOK: {$strErro}";
//            }
//            if(is_file($pathArquivoZip))
//                unlink($pathArquivoZip);
//        } else {
//            echo "NOTOK: os parametros imei e logfile s�o obrigat�rios";
//        }
//    }
}

