<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Vazia
 *
 * @author rogerfsg
 */
class BO_Mobile_identificador{
    //put your code here
    public function cadastra($idMobile, $idSistemaProjetosVersaoSistemaProdutoMobile){
        if(strlen($idMobile) ){
            $db = Registry::get('Database');
            $objMPV = new EXTDAO_Mobile_identificador($db);
            if(!empty($idSistemaProjetosVersaoSistemaProdutoMobile))
            $objMPV->setSistema_projetos_versao_sistema_produto_mobile_id_INT($idSistemaProjetosVersaoSistemaProdutoMobile);
            $objMPV->setMobile_id_INT($idMobile);
            $objMPV->setData_cadastro_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objMPV->formatarParaSQL();
            $objMPV->insert();
            
            $objMPV->selectUltimoRegistroInserido();
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $objMPV->getId());
        }
        return new Mensagem_token(
            PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, 
            HelperMensagem::GERAL_FALHA_CADASTRO(HelperMensagem::ENTIDADE_MOBILE_PROJETOS_VERSAO, array($idMobile, $idSistemaProjetosVersaoSistemaProdutoMobile))
        );
    }
    
    public function cadastraSeNecessario($idMobile, $sistemaProjetosVersaoSistemaProdutoMobile){
       
        
        $vRet = $this->consulta($idMobile, $sistemaProjetosVersaoSistemaProdutoMobile);
        if($vRet->mCodRetorno == PROTOCOLO_SISTEMA::RESULTADO_VAZIO ){
            $vRet = $this->cadastra($idMobile, $sistemaProjetosVersaoSistemaProdutoMobile);
            if($vRet->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                return $vRet;
        }
        return $vRet;
    }
    
    public function consulta($idMobileConectado,$sistemaProjetosVersaoSistemaProdutoMobile){
        if(strlen($idMobileConectado) 
                && strlen($sistemaProjetosVersaoSistemaProdutoMobile)){
            
            $id = EXTDAO_Mobile_identificador::consulta($idMobileConectado, $sistemaProjetosVersaoSistemaProdutoMobile);
            
            if(strlen($id))
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $id);
            else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_FALHA_CONSULTA(HelperMensagem::ENTIDADE_MOBILE_PROJETOS_VERSAO, array($idMobileConectado, $idUsuario, $idCorporacao, $idProjetosVersao)));
    }
    
    public function modoSuporte($idMobileIdentificador){
        $protocolo = new Protocolo_suporte();
        
        $obj = new EXTDAO_Mobile_identificador();
        $obj->select($idMobileIdentificador);
        $obj->setMobile_resposta_suporte_DATETIME(Helper::getDiaEHoraAtualSQL());
        $obj->formatarParaSQL();
        $obj->update($idMobileIdentificador);
        $protocolo->isSuporteRequisitado = $obj->isSuporteAtivo();
        if($protocolo->isSuporteRequisitado ){
            $dataReq = $obj->getRequisicao_modo_suporte_DATETIME();
            if(strlen($dataReq))
                $protocolo->totSegundosTempoReqSuporte = strtotime($dataReq);
            else $protocolo->totSegundosTempoReqSuporte = null;
            $protocolo->usuarioRequisicao = Seguranca::getId();
            
            return new Mensagem_protocolo(
                $protocolo, 
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, 
                null);
        } else return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, HelperMensagem::SUPORTE_INATIVO );
    }
    
    
    public function desconectaTelefone($idMobileConectado){
        if(strlen($idMobileConectado)){
            $objMobileConectado = new EXTDAO_Mobile_conectado();    
            $objMobileConectado->select($idMobileConectado);
            if(!strlen($objMobileConectado->getData_logout_DATETIME())){
                $objMobileConectado->setData_logout_DATETIME(Helper::getDiaEHoraAtualSQL());
                $objMobileConectado->formatarParaSQL();
                $objMobileConectado->update($idMobileConectado);
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, null);
            }
            else return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::MOBILE_JA_ESTA_DESCONECTADO);
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
    
    public function verificaReinstalacaoDeVersao($idMI){
        

        if(strlen($idMI)){
            $objMI = new EXTDAO_Mobile_identificador();
            $objMI->select($idMI);
            if($objMI->getAtualizacao_versao_ativa_BOOLEAN() == "0" 
                    && !strlen($objMI->getReinstalar_a_versao_id_INT()))
                return new Mensagem_vetor_token(
                        PROTOCOLO_SISTEMA::ATUALIZACAO_DE_VERSAO_BLOQUEADA, 
                        "A atualiza��o de vers�o est� bloqueada para seu celular.");
            else if(strlen($objMI->getReinstalar_a_versao_id_INT())){
                $versaoASerInstalada = $objMI->getReinstalar_a_versao_id_INT();
                $objMI->setReinstalar_a_versao_id_INT(null);
                $objMI->formatarParaSQL();
                $objMI->update($objMI->getId());
                return new Mensagem_vetor_token(
                        PROTOCOLO_SISTEMA::REINSTALAR_VERSAO, 
                        "O suporte enviou uma requisi��o para atualizar a vers�o do seu aplicativo.", 
                        $versaoASerInstalada);
            } else{
                return new Mensagem_vetor_token(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
            
        }
        return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, HelperMensagem::GERAL_ID_VAZIO);
    }
}

