<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Monitora_tela_web_para_mobile
 *
 * @author home
 */
class BO_Monitora_tela_web_para_mobile {
    //put your code here
    public static function consultaListaProtocoloDaOSM($idOSM){

        $listaMTWPM = EXTDAO_Monitora_tela_web_para_mobile::consultaListaIdDaOSM($idOSM);
        $mensagemRet = null;
        if(count($listaMTWPM)){
            $vetor = array();    
            $objMTWM = new EXTDAO_Monitora_tela_web_para_mobile();
            
            for($i = 0 ; $i < count($listaMTWPM); $i++){
                $idMTMPW = $listaMTWPM[$i];
                $protocolo = new Protocolo_monitora_tela_mobile_para_web($idMTMPW);
                $vetor[count($vetor)] = $protocolo;
                $objMTWM->delete($idMTMPW);
            }
            $mensagemRet = new Mensagem_vetor_protocolo(null, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $vetor);
        } else {
            $mensagemRet = new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        return $mensagemRet;
    }
}
