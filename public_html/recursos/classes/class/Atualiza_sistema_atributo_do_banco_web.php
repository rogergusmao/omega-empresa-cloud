<?php

class Atualiza_sistema_atributo_do_banco_web{
     public $objBancoWebTgt,
            $idBancoAndroidSrc,    
            $idPVPrototipoWeb,
            $idPVPrototipoAndroid,
            $dbSrc,
            $indiceInicio,
            $limite;
     
    public function __construct( 
        $objBancoWebTgt, 
        $idBancoAndroidSrc, 
        $idPVPrototipoWeb,
        $idPVPrototipoAndroid,
        $db ,
        $indiceInicio,
        $limite){
        
        $this->limite = $limite;
        $this->objBancoWebTgt = $objBancoWebTgt;
        $this->idBancoAndroidSrc = $idBancoAndroidSrc;
        $this->idPVPrototipoAndroid = $idPVPrototipoAndroid;
        $this->idPVPrototipoWeb = $idPVPrototipoWeb;
        
        if($db == null)
            $this->dbSrc = new Database();
        else 
            $this->dbSrc = $db;
        
        $this->indiceInicio = $indiceInicio;
            
    }
    
    
    public function  getTotalIteracoes(){
        $idBancoWebTgt = $this->objBancoWebTgt->getId();
        
        $this->dbSrc->query("SELECT COUNT(t.id)
                    FROM tabela t 
                    WHERE t.projetos_versao_id_INT = " . $this->idPVPrototipoWeb." 
                        AND t.banco_id_INT = $idBancoWebTgt");
        
        $totalTabelaWeb = $this->dbSrc->getPrimeiraTuplaDoResultSet(0);
        $totalTabelaWeb = !is_numeric($totalTabelaWeb) ? 0 : $totalTabelaWeb;
        
        $this->dbSrc->query("SELECT COUNT(t.id)
                    FROM tabela t 
                    WHERE t.projetos_versao_id_INT = " . $this->idPVPrototipoAndroid." 
                        AND t.banco_id_INT = {$this->idBancoAndroidSrc}");
        $totalTabelaAndroid = $this->dbSrc->getPrimeiraTuplaDoResultSet(0);
        $totalTabelaAndroid = !is_numeric($totalTabelaAndroid) ? 0 : $totalTabelaAndroid;
        return $totalTabelaWeb + $totalTabelaAndroid;
    }


    public function executa() {
                
        $cont = 0;
        if($this->dbSrc == null)
            $this->dbSrc = new Database();
        $idBancoWeb = $this->objBancoWebTgt->getId();
        $dbWebTgt = $this->objBancoWebTgt->getObjDatabase();
        
        $qTabelasWeb = "SELECT t.nome, 
                        t.transmissao_web_para_mobile_BOOLEAN, 
                        t.transmissao_mobile_para_web_BOOLEAN , 
                        t.frequencia_sincronizador_INT,
                        t.id
                    FROM tabela t
                    WHERE t.projetos_versao_id_INT = " . $this->idPVPrototipoWeb." 
                        AND t.banco_id_INT = $idBancoWeb";
        
//        echo $q;
//        exit();
        $this->dbSrc->query($qTabelasWeb);
        $registrosTabelaDoPrototipoWeb = Helper::getResultSetToMatriz($this->dbSrc->result);
        
        $qTabelasAndroid = "SELECT t.nome, 
                        t.transmissao_web_para_mobile_BOOLEAN, 
                        t.transmissao_mobile_para_web_BOOLEAN , 
                        t.frequencia_sincronizador_INT,
                        t.id
                    FROM tabela t 
                    WHERE t.projetos_versao_id_INT = " . $this->idPVPrototipoAndroid." 
                        AND t.banco_id_INT = {$this->idBancoAndroidSrc}";
        
        $this->dbSrc->query($qTabelasAndroid);
        
        $registrosTabelaDoPrototipoAndroid = Helper::getResultSetToMatriz(
            $this->dbSrc->result, 0 , 1);
        
        $db = new Database();
        
        if($this->indiceInicio < count($registrosTabelaDoPrototipoWeb)){
//            print_r($registrosTabelaDoPrototipoWeb);
//            print_r("************************************************************<br/><br/>");
//            echo "indice inicial: {$this->indiceInicio}<br/>";
                
             for (
                $i = $this->indiceInicio; 
                $i < count($registrosTabelaDoPrototipoWeb)
                    && $cont < $this->limite; 
                $i++, $this->indiceInicio++, $cont ++) {


                
                //se transmissao_web_para_mobile_BOOLEAN == 1
                //
                //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
                //da 'tabela' deve ser atualizada
    //                $idSistemaTabelaWeb = array_search($registrosTabelaDoPrototipoWeb[$i][0], $tabelasDoSistemaTabelaWeb);
                $tabelaPrototipoWeb = $registrosTabelaDoPrototipoWeb[$i][0];
                $idTabelaPrototipoWeb = $registrosTabelaDoPrototipoWeb[$i][4];
                Helper::imprimirMensagem("Indice: $i - {$tabelaPrototipoWeb}[$idTabelaPrototipoWeb] ");
                
                $qTabelasAndroid = "SELECT id
                    FROM sistema_tabela
                    WHERE nome = '$tabelaPrototipoWeb'";
                
                $dbWebTgt->query($qTabelasAndroid);
                $idSistemaTabelaWeb = $dbWebTgt->getPrimeiraTuplaDoResultSet(0);
                
                
                
                $ret =  null;
                if(!strlen($idSistemaTabelaWeb)){
                    Helper::imprimirMensagem("Sistema tabela n�o mapeado no banco web: $tabelaPrototipoWeb", MENSAGEM_ERRO);
                    exit();
                }
                $qSA = "SELECT id, nome
                              FROM sistema_atributo
                              WHERE sistema_tabela_id_INT = $idSistemaTabelaWeb";
                $dbWebTgt->query($qSA);

                $atributosSistemaAtributoWeb =  Helper::getResultSetToMatriz($dbWebTgt->result);
                
                $atributosSistemaAtributoWebPorFlagEncontrada = array();
                
                $identificadoresAtributoPrototipoWeb = EXTDAO_Tabela::getVetorIdentificadorAtributo(
                    $idTabelaPrototipoWeb, $this->dbSrc);
                
                for($j = 0 ; $j < count($identificadoresAtributoPrototipoWeb); $j++){
                    $idAtributo = $identificadoresAtributoPrototipoWeb[$j][0];
                    $nomeAtributo = $identificadoresAtributoPrototipoWeb[$j][1];
                    $objAtributoPrototipoWebOuAndroid = new EXTDAO_Atributo($db);
                    $objAtributoPrototipoWebOuAndroid->select($idAtributo);

                    $ret = EXTDAO_Sistema_atributo::atualizaOuInsereAtributo(
                        $dbWebTgt, $objAtributoPrototipoWebOuAndroid, $idSistemaTabelaWeb);

                    if($ret["operacao"] == "edicao" || $ret["operacao"] == "sem_modificacao"){
                        $atributosSistemaAtributoWebPorFlagEncontrada[$nomeAtributo] = true;
                        $idSistemaAtributo = $ret["idSistemaAtributo"];
                        $dbWebTgt->query("UPDATE sistema_atributo "
                            . " SET excluido_BOOLEAN = '0' "
                            . " WHERE id='$idSistemaAtributo' ");
                    }
                }
                //REMOVER OS ATRIBUTOS ANTIGOS
                //Percorre os registros da tabela 'sistema_tabela' 
                //retira as tabelas que n�o foram encontradas
                for($u = 0 ; $u < count($atributosSistemaAtributoWeb); $u++){
                   $id = $atributosSistemaAtributoWeb[$u][0];
                   $nome = $atributosSistemaAtributoWeb[$u][1];
                    //se a tabela contida no sistema_tabela 
                    //nao foi encontrada na atual estrutura da base web
                    if($atributosSistemaAtributoWebPorFlagEncontrada[$nome]!= true ){
                         
                        $dbWebTgt->query("UPDATE sistema_atributo "
                            . " SET excluido_BOOLEAN = '1' "
                            . " WHERE id='$id' ");
                    }
                }
            }
        }
       
        
        for (
            $i = $this->indiceInicio - count($registrosTabelaDoPrototipoWeb); 
            $i < count($registrosTabelaDoPrototipoAndroid)
                && $cont < $this->limite; 
            $i++, $this->indiceInicio++, $cont ++) {

            //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
            //da 'tabela' deve ser atualizada
            // $idSistemaTabelaWeb = array_search($registrosTabelaDoPrototipoWeb[$i][0], $tabelasDoSistemaTabelaWeb);
            $tabelaPrototipoAndroid = $registrosTabelaDoPrototipoAndroid[$i][0];
            $idTabelaPrototipoAndroid= $registrosTabelaDoPrototipoAndroid[$i][4];
            $dbWebTgt->query("SELECT id
                          FROM sistema_tabela
                          WHERE nome = '$tabelaPrototipoAndroid'");
           
            $idSistemaTabelaWeb = $dbWebTgt->getPrimeiraTuplaDoResultSet(0);
            
             
            if(!strlen($idSistemaTabelaWeb)){
                Helper::imprimirMensagem("Sistema tabela n�o mapeado no banco android em sistema_tabela@web: $tabelaPrototipoAndroid", MENSAGEM_ERRO);
                exit();
            }
            
            
            $ret =  null;
            
            $dbWebTgt->query("SELECT id, nome
                          FROM sistema_atributo
                          WHERE sistema_tabela_id_INT = $idSistemaTabelaWeb");

            $atributosSistemaAtributoWeb =  Helper::getResultSetToMatriz($dbWebTgt->result);    

            $atributosSistemaAtributoWebPorFlagEncontrada = array();
//            if(!strlen($idSistemaTabelaWeb)){
//                    echo $q;
//                    exit;
//                }
            
            $identificadoresAtributoPrototipoWeb = EXTDAO_Tabela::getVetorIdentificadorAtributo(
                                                        $idTabelaPrototipoAndroid, $this->dbSrc);
            
//            print_r($idSistemaTabelaWeb);
//            exit();
            for($j = 0 ; $j < count($identificadoresAtributoPrototipoWeb); $j++){
                $idAtributo = $identificadoresAtributoPrototipoWeb[$j][0];
                $nomeAtributo = $identificadoresAtributoPrototipoWeb[$j][1];
                $objAtributoPrototipoWebOuAndroid = new EXTDAO_Atributo($db);
                $objAtributoPrototipoWebOuAndroid->select($idAtributo);
                
                $ret = EXTDAO_Sistema_atributo::atualizaOuInsereAtributo(
                    $dbWebTgt, $objAtributoPrototipoWebOuAndroid, $idSistemaTabelaWeb);
                
                if($ret["operacao"] == "edicao" || $ret["operacao"] == "sem_modificacao"){
                    $atributosSistemaAtributoWebPorFlagEncontrada[$nomeAtributo] = true;
                    $idSistemaAtributo = $atributosSistemaAtributoWebPorFlagEncontrada["idSistemaAtributo"];
                    $dbWebTgt->query(
                        "UPDATE sistema_atributo "
                        . " SET excluido_BOOLEAN = '0'"
                        . " WHERE id='$idSistemaAtributo'");
                }
            }
//            print_r($atributosSistemaAtributoWeb);
//            exit();
            //Percorre os registros da tabela 'sistema_tabela' 
            //retira as tabelas que n�o foram encontradas
            for($u = 0 ; $u < count($atributosSistemaAtributoWeb); $u++){
               $id = $atributosSistemaAtributoWeb[$u][0];
               $nome = $atributosSistemaAtributoWeb[$u][1];
               //se a tabela contida no sistema_tabela 
               //nao foi encontrada na atual estrutura da base web
               if($atributosSistemaAtributoWebPorFlagEncontrada[$nome]!= true ){
          
                    $dbWebTgt->query(
                        "UPDATE sistema_atributo "
                        . " SET excluido_BOOLEAN = '1'"
                        . " WHERE id='$id'");
                }
            }
        }
        return $cont;
    }
}
