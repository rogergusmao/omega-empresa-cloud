<?php

class Database_Populator_Helper {

    public $blackList = null;
    
    public function __construct(){

        $this->blackList = array(
            "usuario", 
            "categoria_permissao",
            "permissao_categoria_permissao",
            "usuario_categoria_permissao",
            "usuario_corporacao",
            "usuario_servico",
            "tipo_ponto",
            "usuario_tipo_corporacao",
            "pessoa_usuario",
            "usuario_tipo",
            "operadora"
            );


    }

    public function getTableListWeb(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $idCorporacao = $POSTData->idCorporacao;
        
        $objConexaoHelper = new Database_Connection_Helper();
        $connParams = $objConexaoHelper->getCorporacaoMySQLConnectionParameters($idCorporacao);
        
        if($connParams == null)
            throw new Exception("Falha ao recuperar a conex�o do banco da corpora��o: $idCorporacao");
        //cria conexao para o banco do produdo destinado ao cliente em questao
        $objBanco = new Database($connParams->database, $connParams->host, $connParams->port, $connParams->user, $connParams->password);

        $arrRetorno = array();
        $arrTables = $this->getAndroidWebTables($objBanco);

        $arrTabelasDefault = array(
            new DataGenerationTableInfo("acesso", 10),
            new DataGenerationTableInfo("cliente", 20),
            new DataGenerationTableInfo("bairro", 10),
            new DataGenerationTableInfo("cidade", 5),
            new DataGenerationTableInfo("produto", 20),
            new DataGenerationTableInfo("rede", 2)
        );
        if($arrTables  != null)
        foreach($arrTables as $tableName){
            if(in_array($tableName, $this->blackList))
                continue;
                
            $recordCount = null;
            for($i=0; $i < count($arrTabelasDefault); $i++) {

                if($arrTabelasDefault[$i]->tableName == $tableName){

                    $recordCount = $arrTabelasDefault[$i]->recordCount;
                    break;

                }

            }

            $objTable = new DataGenerationTableInfo($tableName, $recordCount, false);
            $arrRetorno[] = $objTable;

        }

        echo json_encode($arrRetorno);

    }

    public function getAndroidWebTables(Database $objBanco){

        $arrRetorno = array();
        
        $q = "SELECT
                  st.nome
              FROM
                  sistema_tabela st
              WHERE
                 (
                    st.transmissao_web_para_mobile_BOOLEAN = '1'
                    AND st.transmissao_mobile_para_web_BOOLEAN = '1'
                 )
              AND (st.tabela_sistema_BOOLEAN IS NULL OR st.tabela_sistema_BOOLEAN != '1')
              AND st.excluida_BOOLEAN = '0' ";
        
           
        $objBanco->query($q);
        
        while($dadosTabelas = $objBanco->fetchArray()){

            $arrRetorno[] = $dadosTabelas[0];

        }

        return $arrRetorno;

    }
    
    public function getMySQLTableList(Database $objBanco){

        $arrRetorno = array();
        $objBanco->query("SHOW TABLES");
        while($dadosTabelas = $objBanco->fetchArray()){

            $arrRetorno[] = $dadosTabelas[0];

        }

        return $arrRetorno;

    }

    public function generateWebTestData(){

        set_time_limit(30 * 60);
        $POSTData = json_decode(file_get_contents('php://input'));
        $arrTables = $POSTData->tableList;
        $idCorporacao = $POSTData->corporacaoId;
        $parameters = $POSTData->parameters;

        $objPost = new stdClass();
        $objPost->tableList = $arrTables;
        $objPost->operations = $parameters->operations;
        $objPost->corporacaoId = $idCorporacao;
        $objPost->isToGenerateJson = $parameters->runOperationsOnMobile;
        $objPost->jsonOperations = $parameters->operationsOnMobile;
        $objPost->desativarOperacoesChaveEstrangeira = $parameters->desativarOperacoesChaveEstrangeira;

        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getDominioDaCorporacaoId&corporacao_id={$idCorporacao}";
        $strJsonDominioProduto = Helper::chamadaCurl($url);
        $objJsonDominioProduto = json_decode($strJsonDominioProduto);
        $dominioProduto = $objJsonDominioProduto->mValor;

        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getNomeDaCorporacao&corporacao_id={$idCorporacao}";
        $strJsonNomeCorporacao = Helper::chamadaCurl($url);
        $objJsonNomeCorporacao = json_decode($strJsonNomeCorporacao);
        $nomeCorporacao = $objJsonNomeCorporacao->mValor;

        $urlRequisicao = "{$dominioProduto}/adm/webservice.php?class=BO_Database_Populator&action=insertData&corporacao={$nomeCorporacao}";
        $objPost->urlDoProduto = $dominioProduto;
//      echo $urlRequisicao;
//      exit();
//      http://127.0.0.1/PontoEletronico/10001/public_html//adm/webservice.php?class=BO_Database_Populator&action=insertData&corporacao=X2222
        $strJson = Helper::postCurl($urlRequisicao, json_encode($objPost));

        echo $strJson;

    }

    public static function factory(){

        return new Database_Populator_Helper();

    }

}


class DataGenerationTableInfo {

    public function __construct($tableName, $recordCount, $isSelected = true){

        $this->tableName = $tableName;
        $this->recordCount = $recordCount;
        $this->isSelected = $isSelected;

    }

    public $tableName;
    public $recordCount;
    public $isSelected;

}