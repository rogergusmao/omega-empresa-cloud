<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
class Gerador_script_android_atributo extends Gerador_script_atributo{
    
    public function __construct(){
        parent::__construct();
        
    }
    
    public function constroiScriptCreate($pObjAtributoHomologacao){
        $objScript = new Container_atributo();
        $vScriptInsert = "";
        //LastName 
        if($pObjAtributoHomologacao->getNome() ){
            $vScriptInsert .= "{$pObjAtributoHomologacao->getNome()} ";
        }
        //varchar
        if(strlen($pObjAtributoHomologacao->getTipo_sql()) ){
            $vScriptInsert .= $pObjAtributoHomologacao->getTipo_sql();
            //float(255, 0)
            if(strlen($pObjAtributoHomologacao->getTamanho_INT()) && strlen($pObjAtributoHomologacao->getDecimal_INT()))
                $vScriptInsert .= "({$pObjAtributoHomologacao->getTamanho_INT()}, {$pObjAtributoHomologacao->getDecimal_INT()})";
            //varchar(255)
            else if(strlen($pObjAtributoHomologacao->getTamanho_INT()) ){
                if($pObjAtributoHomologacao->tipoSQLReal())
                    $vScriptInsert .= "({$pObjAtributoHomologacao->getTamanho_INT()}, 0) ";
                else
                    $vScriptInsert .= "({$pObjAtributoHomologacao->getTamanho_INT()}) ";
                
            }
                    
        }
        //x varchar(255) NOT NULL
        if(strlen($pObjAtributoHomologacao->getNot_null_BOOLEAN()) 
                && $pObjAtributoHomologacao->getNot_null_BOOLEAN() == "1"){
            $vScriptInsert .= " NOT NULL ";
        }
        //x varchar(255) NOT NULL DEFAULT 'X'
        if(strlen($pObjAtributoHomologacao->getValor_default())){
            $vScriptInsert .= " DEFAULT '{$pObjAtributoHomologacao->getValor_default()}' ";
        }
        
        $objScript->mScriptInsert = $vScriptInsert;
        
        
        if($pObjAtributoHomologacao->getPrimary_key_BOOLEAN()  == "1"){
            $objScript->mIsPrimaryKey = true;
        }
        
        $vObjTabela = new EXTDAO_Tabela();
        $vObjTabela->select( $pObjAtributoHomologacao->getTabela_id_INT());
        if(strlen($pObjAtributoHomologacao->getFk_tabela_id_INT())){
            
//            $vNomeFK = Helper::getNomeAleatorio(substr($vObjTabela->getNome(), 0, 5))."_KEY";
            $objScript->mNomeKey = $pObjAtributoHomologacao->getFk_nome();
//            $objScript->mScriptKey = "CREATE INDEX {$objScript->mNomeKey}
//                    ON ".$vObjTabela->getNome()." ({$pObjAtributoHomologacao->getNome()} ASC) ";
            
            $vObjTabelaFK = new EXTDAO_Tabela();
            $vObjTabelaFK->select($pObjAtributoHomologacao->getFk_tabela_id_INT());
            $vObjAtributoFK = new EXTDAO_Atributo();
            $vObjAtributoFK->select($pObjAtributoHomologacao->getFk_atributo_id_INT());
            $objScript->mIdAtributoFkHmg = $pObjAtributoHomologacao->getId();
            $objScript->mNomeFK = $objScript->mNomeKey;
            $vConsultaFK = "";
            $vConsultaFK .= "CONSTRAINT {$objScript->mNomeFK} FOREIGN KEY ({$pObjAtributoHomologacao->getNome()})"; 
            $vConsultaFK .= " REFERENCES {$vObjTabelaFK->getNome()} ({$vObjAtributoFK->getNome()})";
            $vConsultaFK .= " ON DELETE {$pObjAtributoHomologacao->getDelete_tipo_fk()} ON UPDATE {$pObjAtributoHomologacao->getUpdate_tipo_fk()}";
            //ANTES
//            $vConsultaFK = "";
//            $vConsultaFK .= "FOREIGN KEY ({$pObjAtributoHomologacao->getNome()})"; 
//            $vConsultaFK .= " REFERENCES {$vObjTabelaFK->getNome()} ({$vObjAtributoFK->getNome()})";
//            $vConsultaFK .= " ON DELETE {$pObjAtributoHomologacao->getDelete_tipo_fk()} ON UPDATE {$pObjAtributoHomologacao->getUpdate_tipo_fk()}";
            
            $objScript->mScriptFK = $vConsultaFK;
            
        }
        if($pObjAtributoHomologacao->getUnique_BOOLEAN()){
            $objScript->mIsUnique = true;
        }
        
        return $objScript;
    }
    
    public function constroiScriptDelete( $pObjTabelaHomologacao, $pObjAtributoProducao){
       return null;
    }
    
    public function constroiScriptEdit($pObjAtributoHomologacao, $pObjAtributoProducao){
       return null;
    }
    
    protected function constroiScriptInsert( $pObjAtributoHomologacao){
        $vTabela = new EXTDAO_Tabela();
        $vTabela->select($pObjAtributoHomologacao->getTabela_id_INT());
        
        $vConsultaAlterarTipo ="";
        $vConsultaAlterarTipo .= "\nALTER TABLE {$vTabela->getNome()} ";
        $vConsultaAlterarTipo .= " ADD COLUMN {$pObjAtributoHomologacao->getNome()} ";
        if($pObjAtributoHomologacao->getTipo_sql() )
           $vConsultaAlterarTipo .= " {$pObjAtributoHomologacao->getTipo_sql()}";

       if(strlen($pObjAtributoHomologacao->getTamanho_INT()) && strlen($pObjAtributoHomologacao->getDecimal_INT()))
           $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}, {$pObjAtributoHomologacao->getDecimal_INT()}) ";
       else if(strlen($pObjAtributoHomologacao->getTamanho_INT()))
           $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}) ";

       if($pObjAtributoHomologacao->getNot_null_BOOLEAN())
           $vConsultaAlterarTipo .= " NOT NULL ";
       
        if(strlen($pObjAtributoHomologacao->getValor_default()))
            $vConsultaAlterarTipo .= " DEFAULT '{$pObjAtributoHomologacao->getValor_default()}' ";
            
       $this->insereScriptComandoBanco($vConsultaAlterarTipo, EXTDAO_Tipo_comando_banco::ADD_COLUMN);
        return new Script_insere_atributo($vConsultaAlterarTipo);
    }
    
        
}

?>
