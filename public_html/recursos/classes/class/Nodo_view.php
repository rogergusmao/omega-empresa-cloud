<?php

class Nodo_view {// Classe : início
   
    public $id;
    public $label;
    public $tipoPagina;
    public $pagina;
    public $iconeAberto;
    public $iconeAbertoSemFilho;
    public $iconeFechado;
    public $tipo;
    public $idParamGet;
    public $getComplemento;
    public $isDiretorio;
    public $labelTab;
    public $abrirNovaJanela;
    public function __construct(
            $tipo,
            $isDiretorio,
            $id, 
            $label, 
            $idParamGet = null,
            $getComplemento = null,
            $tipoPagina = null, 
            $pagina = null, 
            $labelTab = null,
            $iconeAberto = null, 
            $iconeAbertoSemFilho= null, 
            $iconeFechado= null,
            $abrirNovaJanela = null) {
        $this->abrirNovaJanela = !strlen($abrirNovaJanela) ? false : $abrirNovaJanela;
        $this->isDiretorio = $isDiretorio;
        $this->tipo = $tipo;
        $this->id = $id;
        $this->label = $label;
        
        $this->getComplemento = $getComplemento;
        $this->tipoPagina = $tipoPagina;
        $this->pagina = $pagina;
        $this->labelTab = $labelTab;
        $this->idParamGet = $idParamGet;
        $this->iconeAberto = $iconeAberto;
        $this->iconeAbertoSemFilho = $iconeAbertoSemFilho;
        $this->iconeFechado = $iconeFechado;
    }
    public function abrirNovaJanela(){
        return $this->abrirNovaJanela;
    }
    public function getVetorObjPagina(){
        if(is_array($this->pagina)){
            $ret = array();
            for($i = 0 ; $i < count($this->pagina); $i++){
                $ret[$i]->pagina = $this->pagina[$i];
                $ret[$i]->labelTabEncode = urlencode( $this->labelTab[$i]);
                $ret[$i]->tipoPagina = $this->tipoPagina[$i];
                $ret[$i]->abrirNovaJanela = $this->abrirNovaJanela[$i] ;
            }
            return $ret;
        } else{
            $obj = new stdClass();
            $obj->pagina = $this->pagina;
            $obj->labelTabEncode = urlencode($this->labelTab);
            $obj->tipoPagina = $this->tipoPagina;
            $obj->abrirNovaJanela = $this->abrirNovaJanela;
            return array($obj);
        }
    }
    
    public function isDiretorio(){
        return $this->isDiretorio;
    }
    public function getId(){
        return $this->id;
    }
    public function getTipoPagina(){
        return $this->tipoPagina;
    }
    public function getPagina(){
        return $this->pagina;
    }
    
    public function getLabel(){
        return $this->label;
    }
    
    public function getXML(){
        
        $xml = "";
        $strChild="";
        $nodoEncoded = urlencode($this->getId());
        
        if($this->isDiretorio())
            $strChild = "child='1'";
        
        $xml .= "<item {$strChild} id='" . $nodoEncoded . "' text='" . $this->getLabel() . "'";
        if(strlen($this->iconeAberto))
            $xml .= " im0=\"{$this->iconeAberto}\" ";
        if(strlen($this->iconeAbertoSemFilho))
            $xml .= " im1=\"{$this->iconeAbertoSemFilho}\" ";
        if(strlen($this->iconeFechado))
            $xml .= " im2=\"{$this->iconeFechado}\" ";
        $xml .= ">";
        $xml .= "<userdata name='ud_block'>ud_data
                </userdata>
           </item>";
        return $xml;
    }
    
    
    public function factory($id, $label){
        
        return new Nodo_view(
            $this->tipo,
            $this->isDiretorio,
            $id, 
            $label, 
            $this->idParamGet,
            $this->getComplemento,
            $this->tipoPagina, 
            $this->pagina, 
            $this->labelTab, 
            $this->iconeAberto, 
            $this->iconeAbertoSemFilho, 
            $this->iconeFechado,
            $this->abrirNovaJanela);
        
        
    }
    public function getParamGet($aux, $varGETPai){
        $ret = "";
        
        if(strlen($this->idParamGet) 
                && $aux != null 
                && count($aux) > 0){
            
            if(strlen($varGETPai))
                $ret = $this->idParamGet ."=".$aux[0];
            else 
                $ret = $this->idParamGet."=".$aux[0];
            
        } 
        if(strlen($varGETPai)){
            if(strlen($ret))
                $ret .= "&".$varGETPai;
            else $ret .= $varGETPai;
        }
        if(strlen($this->getComplemento)){
            if(strlen($ret))
                $ret .= "&".$this->getComplemento;
            else $ret .= $this->getComplemento;
        }
        
        return $ret;
    }
    
    public static function formataGETIdNodo($strId){
        $pos = strpos($strId, '&');
        
        $idNodoAux = $strId;
        $varGET = -1;
        if ($pos) {
            $idNodoAux = substr($strId, 0, $pos);
            $varGET = substr($strId, $pos + 1);
        }else {
            $varGET = null;
        }
        return array($idNodoAux, $varGET);
    }
    
    public function getVetorNodo($varGETPai){
        // A funcao eh chamada no evento 'expande arvore da tag'
        //Ex.: Quando clica no banco, aparece a lista de tabelas como filha
        $vetorAuxId = array();
        $vetorObj = array();
        $GET = Helper::constroiGET($varGETPai, "&");
        $objNotFactory = null;
        switch ($this->tipo) {
            case Arvore_view::PROJETOS:
                $vetorAuxId = EXTDAO_Projetos::getListaIdentificadorNo();
                break;
            case Arvore_view::BANCOS:
                $vetorAuxId = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNo($GET[Param_Get::ID_PROJETOS_VERSAO]);
                break;
            case Arvore_view::ANALISE_BANCOS:
                $vetorAuxId = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoAnaliseBancos();
                break;
            case Arvore_view::TABELA_TABELA_HOMOLOGACAO:
                $vetorAuxId = EXTDAO_Tabela_tabela::getListaIdentificadorNoHomologacao($GET[Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO]);
                break;
            case Arvore_view::TABELA_TABELA_PRODUCAO:
                $vetorAuxId = EXTDAO_Tabela_tabela::getListaIdentificadorNoProducao($GET[Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO]);
                break;
            case Arvore_view::PROJETOS_VERSAO:
                $vetorAuxId = EXTDAO_Projetos_versao::getListaIdentificadorNo($GET[Param_Get::ID_PROJETOS]);
                break;
            case Arvore_view::SISTEMA:
                $vetorAuxId = EXTDAO_Sistema::getListaIdentificadorNo($GET[Param_Get::ID_PROJETOS]);
                break;
            
            case Arvore_view::SISTEMA_PRODUTO:
                $vetorAuxId = EXTDAO_Sistema_produto::getListaIdentificadorNo($GET[Param_Get::ID_SISTEMA]);
                break;
            case Arvore_view::SISTEMA_PRODUTO_WEB:
                $vetorAuxId = EXTDAO_Sistema_produto_web::getListaIdentificadorNo($GET[Param_Get::ID_SISTEMA]);
                break;
            case Arvore_view::SISTEMA_PRODUTO_MOBILE:
                $vetorAuxId = EXTDAO_Sistema_produto_mobile::getListaIdentificadorNo($GET[Param_Get::ID_SISTEMA]);
                break;
            
            case Arvore_view::CONEXOES:
                $vetorAuxId = EXTDAO_Conexoes::getListaIdentificadorNo();
                break;
            case Arvore_view::BANCO_BANCO_HOMOLOGACAO:
                
                $vetorAuxId = EXTDAO_Banco_banco::getListaIdentificadorNoBancoHomologacao($GET[Param_Get::ID_CONEXOES]);
                break;
            case Arvore_view::BANCO_BANCO_PRODUCAO:
                $vetorAuxId = EXTDAO_Banco_banco::getListaIdentificadorNoBancoProducao($GET[Param_Get::ID_CONEXOES]);
                break;
            case Arvore_view::BANCO:
                $vetorAuxId = EXTDAO_Banco::getListaIdentificadorNo($GET[Param_Get::ID_CONEXOES]);
                break;
            
            case Arvore_view::MOBILE_IDENTIFICADOR_PROJETOS_VERSAO:
                $vetorAuxId = EXTDAO_Mobile_identificador::getListaIdentificadorNo($GET[Param_Get::ID_MOBILE_IDENTIFICADOR]);
                break;

            
            case Arvore_view::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                
                $vetorAuxId = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoBancoSincronizadorWeb($GET[Param_Get::ID_PROJETOS_VERSAO]);
                break;
            case Arvore_view::ATUALIZA_SINCRONIZADOR_WEB_PROD:
                
                $vetorAuxId = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoAtualizaSincronizadorWebProd($GET[Param_Get::ID_PROJETOS_VERSAO]);
                break;
            case Arvore_view::DESENVOLVIMENTO_APP:
                
                $vetorAuxId = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoDesenvolvimentoApp($GET[Param_Get::ID_PROJETOS_VERSAO]);
                break;
                
            case Arvore_view::SINCRONIZACAO_PROJETOS_VERSAO:
                $vetorAuxId = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoSincronizacao($GET[Param_Get::ID_PROJETOS_VERSAO]);
                break;
            
            case Arvore_view::MOBILE_IDENTIFICADOR:
                $vetorAuxId = EXTDAO_Mobile_identificador::getListaIdentificadorNoProdutos($GET[Param_Get::ID_MOBILE]);
                break;
            
            case Arvore_view::MOBILE:
                $vetorAuxId = EXTDAO_Mobile::getListaIdentificadorNo();
                break;
            
            case Arvore_view::ESTATICO :
                
                $varGET = $this->getParamGet(null, $varGETPai);
                $id = null;
                //adiciona a variavel GET para passar para os filhos
                if($varGET != null && strlen($varGET) > 0)
                    $id = $this->id. "&".$varGET;
                else $id = $this->id;
                
                $objNotFactory = $this->factory($id, $this->label);
                break;
            default:
                throw new Exception("Nodo não definido");
                return;
            default:
                break;
        }
        if($objNotFactory == null){
            if($vetorAuxId != null)
            foreach ($vetorAuxId as $aux) {
                $varGET = $this->getParamGet($aux, $varGETPai);
                $id = null;
                //adiciona a variavel GET para passar para os filhos
                if($varGET != null && strlen($varGET) > 0)
                    $id = $this->id. "&".$varGET;
                else $id = $this->id;

                $vetorObj[count($vetorObj)] = $this->factory($id, $aux[1]);
            }
        } else if($objNotFactory != null){
            $vetorObj[0] = $objNotFactory;
        }
        return $vetorObj;
    }

    
}

// Class : fim
?>
