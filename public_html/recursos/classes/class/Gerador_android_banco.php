<?php
class Gerador_android_banco{
    public function __construct(){
        
    }
    public function gerarBANCO( 
        $sobrescrever, 
        $idPVBBSincronizacao, 
        $idPVBBPrototipoAndroid,
        $objBanco = null){
    if($objBanco == null)
    $objBanco = new Database();

    $arrExcessoesRelacionamento = array();
    
    if($idPVBBSincronizacao == null)
    $idPVBBSincronizacao = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    
    $varGET= Param_Get::getIdentificadorProjetosVersao();
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    //$objPVBB->select($idPVBBSincronizacao);
    $objPVBB->select($idPVBBPrototipoAndroid);
    $idPV= $objPVBB->getProjetos_versao_id_INT();
    $objPV = new EXTDAO_Projetos_versao();
    $objPV->select($idPV);
    $idProjeto = $objPV->getProjetos_id_INT();
    
//    $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoProducao($idPVBBSincronizacao, $objBanco);
//    $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($idPVBBSincronizacao, $objBanco);
    $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBPrototipoAndroid, $objBanco);
    $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBPrototipoAndroid, $objBanco);
    
    $objBanco = new EXTDAO_Banco();
    $objBanco->select($idBanco);
    $nomeClasseBanco = "PontoEletronico";
    $nomeClasseBanco = "Database".$nomeClasseBanco;
    
    $objDiretorio = new EXTDAO_Diretorio_android();
    $objDiretorio->select(EXTDAO_Diretorio_android::getIdDiretorio($idPVBBPrototipoAndroid));
    
    $raiz = $objDiretorio->getRaizSrc();
    $raizDatabase = $objDiretorio->getRaiz_biblioteca_database();
    
    $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raiz));
    $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
    $packageDatabase = str_replace("/", ".", $raizDatabase);
    
    $diretorioDAO = $objDiretorio->getDAO();
    $diretorioEXTDAO = $objDiretorio->getEXTDAO();
    $packageEXTDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorioEXTDAO));
    $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorioDAO));
    
    $objTabela = new EXTDAO_Tabela();
    
    
    $vetorIdTabelaBanco = EXTDAO_Tabela::getHierarquiaTabelaBanco($idPV, $idBanco);
    
    $objTabelaBanco = new EXTDAO_Tabela();
    $strImportEXTDAO = "";
    $strVetorTabelaBanco = "";
    
    for($k = 0 ; $k < count($vetorIdTabelaBanco); $k++){
        $idTabelaBanco = $vetorIdTabelaBanco[$k];
        
        $objTabelaBanco->select($idTabelaBanco);
        $classEXTDAO = "EXTDAO".$objTabelaBanco->getNomeClasseAndroid();
        if($objTabelaBanco->getNome() == "permissao"){
            $z = 0;
        }
        $strImportEXTDAO .= "\timport $packageEXTDAO.$classEXTDAO;\n";
        
        if(strlen($strVetorTabelaBanco)){
            $strVetorTabelaBanco .= ", \n";
        } 
        $strVetorTabelaBanco .= "\t\tnew ".$classEXTDAO."(null)";
        
    }
    $strVetorTabelaBanco = "private static Table __vectorTable[] = new Table[] { \n$strVetorTabelaBanco};";
    $dir = dirname(__FILE__);

    $filename = Helper::getPathComBarra($raiz) .Helper::getPathComBarra($diretorioEXTDAO) . $nomeClasseBanco . ".java";



    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

    // open file in insert mode
    $file = fopen($filename, "w+");
    $filedate = date("d.m.Y");
    

    $c = "";

    $c = "

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  $nomeClasseBanco
    * DATA DE GERAÇÃO: $filedate
    * ARQUIVO:         $nomeClasseBanco.java
    * BANCO DE DADOS:  $database->database
    * -------------------------------------------------------
    *
    */
    
    package $packageEXTDAO;

    import android.content.Context;
    import android.os.Environment;
    
    $strImportEXTDAO
    
    import $packageDatabase.Database;
    import $packageDatabase.Table;
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    public class $nomeClasseBanco extends Database {
	
";
    $strImport = "";
   
    $strConstrutor = "";
   
    
   
    $versaoBancoAndroid = 1;
    $nomeArquivoBancoAndroid = "contatoempresanuvem.db";
    
    
    
    $strConstrutor.= "
    // *************************
    // CONSTRUTOR
    // *************************
    public static final String DATABASE_PATH = Environment.getDataDirectory()+ \"/$nomeArquivoBancoAndroid\";
    public static final String DATABASE_NAME =  \"$nomeArquivoBancoAndroid\";
    public static final int DATABASE_VERSION = $versaoBancoAndroid;    

    public $nomeClasseBanco(Context context){
            super(
                    context,
                    DATABASE_NAME, 
                    DATABASE_VERSION, 
                    DATABASE_PATH);
            //populate();


    }

    public $nomeClasseBanco(Context context, boolean pOpen){
            super(
                    context,
                    DATABASE_NAME, 
                    DATABASE_VERSION, 
                    DATABASE_PATH,
                    pOpen);
            //populate();


    }
    ";
    
    $strFabrica.= "
    // *************************
    // FACTORY
    // *************************
    public Database factory(){
        return new $nomeClasseBanco(this.getContext());

    }
    ";
    
    
    $strFunctionGetVetorTable = "
        @Override
	public Table[] getVetorTable() {
		// TODO Auto-generated method stub
		return __vectorTable;
	}";
    $c .= "
    $strImport
        
    

    
    // *************************
    // DECLARAÇÃO DE TODAS AS TABELAS DO BANCO DE DADOS
    // *************************
    ";
    $c.= $strVetorTabelaBanco;
    $c.= $strConstrutor;
    $c.= $strFabrica;
    $c.= $strFunctionGetVetorTable;
    $c.= "

    } // classe: fim
    

    ";
    fwrite($file, $c);
    fclose($file);

	
	$strGerouExt = "|<p class=\"mensagem_retorno\">
		&bull;&nbsp;&nbsp;Classe <b>$classEXTDAO</b> gerada com sucesso no arquivo <b>$classEXTDAO.php</b>.
        
		</p>"
            . "<p class=\"mensagem_retorno\">"
            . "        &nbsp;Path: $filename"
            . "</p>"
            
            ;

    print $strGerouExt;


    }

}
}
?>
