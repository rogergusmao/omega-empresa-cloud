<?php
class Gerador_android_dao {

    public function __construct(){
        
        
    }
    public function gerarDAO(
        $vetorTabelaBancoOrdenada, $table, $class,
        $key, $label, $ext, $sobrescrever, $idPVBBSincronizacao = null,
        $db = null){
        
        set_time_limit(40 * 60);    
        if($db ==null)
        $db = new Database();
        $arrExcessoesRelacionamento = array();

        if($idPVBBSincronizacao == null)
            $idPVBBSincronizacao = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

        $varGET= Param_Get::getIdentificadorProjetosVersao();
        
        $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
        $objPVBB->select($idPVBBSincronizacao);
        
        
    
        $idPV= $objPVBB->getProjetos_versao_id_INT();
        $idPVBBPrototipoAndroid= EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoAndroidDoProjetosVersao($idPV);
        
        $objPV = new EXTDAO_Projetos_versao();
        $objPV->select($idPV);
        $idProjeto = $objPV->getProjetos_id_INT();
        $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBSincronizacao);
        $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBSincronizacao);

        $objDiretorio = new EXTDAO_Diretorio_android();
        $idDiretorio = EXTDAO_Diretorio_android::getIdDiretorio($idPVBBPrototipoAndroid, $db);
        
        if(!strlen($idDiretorio)){
            Helper::imprimirMensagem("Não existe diretório android vinculado ao projetos_versao_banco_banco = $idPVBBSincronizacao",MENSAGEM_ERRO);
                exit();
        }
        $objDiretorio->select($idDiretorio);
        

        $raizSrc = $objDiretorio->getRaizSrc();
        $raizDatabase = $objDiretorio->getRaiz_biblioteca_database();

        $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raizSrc));
        $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
        $packageDatabase = str_replace("/", ".", $raizDatabase);

        $diretorio = $objDiretorio->getDAO();
        $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorio));

        $objTabela = new EXTDAO_Tabela();
        if(is_numeric($table)){
            $idTabela = $table;
            $objTabela->select($table);
            $table = $objTabela->getNome();
        } else {
            $idTabela = EXTDAO_Tabela::existeTabela($table, $idBanco, $idPV);    
            if(!strlen($idTabela)){

                Helper::imprimirMensagem ("Tabela $table. idBanco: $idBanco. Banco ".$database->getDBName().". Projetos Versão: $idPV.  não encontrada.", MENSAGEM_ERRO);
                exit();
            }
            $objTabela->select($idTabela);
        }
        
        $class = "DAO".$objTabela->getNomeClasseAndroid();
        $dir = dirname(__FILE__);

        $filename = Helper::getPathComBarra($raizSrc) .Helper::getPathComBarra($diretorio) . $class . ".java";

        $sql = "SHOW TABLES LIKE '$table';";

        $database->query($sql);

        if($database->rows() < 1){

            return;

        }

        $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

        // if file exists, then delete it
        if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
        {
            unlink($filename);
        }

        if(!file_exists($filename)){



        // open file in insert mode
        $file = fopen($filename, "w+");
        $filedate = date("d.m.Y");

        $c = "";

        $c = "

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  $class
        * DATA DE GERAÇÃO: $filedate
        * ARQUIVO:         $class.java
        * TABELA MYSQL:    $table
        * BANCO DE DADOS:  $database->database
        * -------------------------------------------------------
        *
        */

        package $packageDAO;

        import $packageDatabase.Database;
        import $packageDatabase.Table;
        import $packageDatabase.Attribute;
        import $packageDatabase.Attribute.SQLLITE_TYPE;
        import $packageDatabase.Attribute.TYPE_RELATION_FK;
        import $packageDatabase.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    ";
        $import = array();
        $import[count($import)] = "android.content.Context";
        $strDeclaracaoAtributo = "";
        $strDeclaracaoEnum = "";
        $strConstrutor = "";

        $vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($objTabela->getId(), $db);
        
        $identificadoresChavesNaoUnica = EXTDAO_Tabela_chave::getIdentificadoresDasChavesNaoUnica($idTabela, $db);
        $retChaveUnica = EXTDAO_Tabela_chave::getIdentificadorDaChaveUnicaDaTabela($idTabela, $db);
        $idChaveUnica = $retChaveUnica[0];
        $nomeChaveUnica = $retChaveUnica[1];
        $objAtributo = new EXTDAO_Atributo();
        if(strlen($idChaveUnica))
            $vetorAtributoChaveUnica = EXTDAO_Tabela_chave_atributo::getListaNomeOrdenadoAtributoDaChave($idChaveUnica);
       
//        if($table == "sistema_atributo"){
//            print_r($vetorAtributoChaveUnica);
//            exit();
//        }
        $strChaveUnica = "";
        if(count($vetorAtributoChaveUnica) > 0){
            //public void setUniqueKey(String pVetorAttributeName[])
            $strChaveUnica = "setUniqueKey( \"$nomeChaveUnica\", new String[]{ ";
            $vetorAtributoChaveUnica = Helper::arrayToUpper($vetorAtributoChaveUnica);
            $strChaveUnica .= Helper::arrayToString($vetorAtributoChaveUnica, ", ");
            $strChaveUnica .= "});";
        }
        //EXTDAO_Tabela::get
        for($i = 0 ; $i < count($vetorIdAtributo); $i++){
            $idAtributo = $vetorIdAtributo[$i];
            
            $objAtributo->select($idAtributo);
            $nomeAtributo = $objAtributo->getNome();
            $strDeclaracaoAtributo .= "\t\tpublic static final String ".  strtoupper($nomeAtributo)." = \"$nomeAtributo\";\n";
            if(strlen($strDeclaracaoEnum))
                $strDeclaracaoEnum .= ",\n ";
            $strDeclaracaoEnum .= "\t\t\t".$nomeAtributo;
        }

        $nomeTabelaConst = strtoupper($objTabela->getNome());
        $nomeEnum = "ATRIBUTOS_$nomeTabelaConst";
        $strDeclaracaoEnum = "\t\tpublic static enum $nomeEnum{\n\t\t{$strDeclaracaoEnum}};\n";


        $strFactoryProtocoloJson = "

    public ProtocoloEntidade<$nomeEnum> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<$nomeEnum>(
                jsonObj, 
                factory(), 
                $nomeEnum.class);

    }
        ";



        $strConstrutor.= "
        // *************************
        // CONSTRUTOR
        // *************************
        public $class(Database database){
            super(NAME, database);
            $strChaveUnica
        ";    

        $strConstrutor .= "";
        for($i = 0 ; $i < count($vetorIdAtributo); $i++){
            $idAtributo = $vetorIdAtributo[$i];
            $objAtributo = new EXTDAO_Atributo();
            $objAtributo->select($idAtributo);
            $nomeAtributo = $objAtributo->getNome();
            $variavelAtributo = strtoupper($nomeAtributo);
            $tipoSQLite = $objAtributo->getTipoSQLiteDAOAndroid();
            $isNull = $objAtributo->getNot_null_BOOLEAN() == "1" ? "false" : "true";
            $default = strlen($objAtributo->getValor_default()) ? '"'.$objAtributo->getValor_default().'"' : "null";
            $isPrimaryKey = $objAtributo->getPrimary_key_BOOLEAN() == "1" ? "true" : "false";
            $tamanho = strlen($objAtributo->getTamanho_INT()) ? $objAtributo->getTamanho_INT() : "0";
            $autoIncrement = $objAtributo->getAuto_increment_BOOLEAN() == "1" ? "true" : "false";
            // se for chave extrangeira
            $idFKAtributo = $objAtributo->getFk_atributo_id_INT();
            if(strlen($idFKAtributo)){
                
                $nomeFk = $objAtributo->getFk_nome();
                $objFKAtributo = $objAtributo->getObjFKAtributo();
                $nomeAtributoFK = $objFKAtributo->getNome();
                $varAtributoFK = strtoupper($nomeAtributoFK);
                $objFKTabela = $objAtributo->getObjFKTabela();
                $classeFK = $objFKTabela->getNomeClasseAndroid();
                $daoTabelaFK = "DAO".$classeFK;
                $import[count($import)] = "$packageDAO.$daoTabelaFK";

                $onDelete = $objAtributo->getDeleteTypeRelationFKAndroid();
                $onUpdate = $objAtributo->getUpdateTypeRelationFKAndroid();
                $strConstrutor .= "
            super.addAttribute(
                new Attribute(
                    $variavelAtributo, 
                    $daoTabelaFK.NAME,
                    $daoTabelaFK.$varAtributoFK,
                    \"$nomeAtributo\",
                    true,
                    $tipoSQLite,
                    $isNull,
                    $default,
                    $isPrimaryKey,
                    null,
                    $tamanho,
                    $onUpdate,
                    $onDelete,
                    \"$nomeFk\"
                )
            );\n";
            } else{
                if(Helper::endsWith($nomeAtributo, "_normalizado") ){
                    $nomeAtributoOriginal = str_replace("_normalizado", "", $nomeAtributo);
                    $varAtributoOriginal = strtoupper($nomeAtributoOriginal);
                    
                    
                    
                  $strConstrutor .= "
            super.addAttribute(getNewAtributoNormalizado($varAtributoOriginal));\n";  
                } else{
                 $strConstrutor .= "
            super.addAttribute(
                new Attribute(
                    $variavelAtributo, 
                    \"$nomeAtributo\",
                    true,
                    $tipoSQLite,
                    $isNull,
                    $default,
                    $isPrimaryKey,
                    null,
                    $tamanho,
                    $autoIncrement
                )
            );\n";   
                }

            }
        }    
        if(count($identificadoresChavesNaoUnica)){
            for($i = 0; $i < count($identificadoresChavesNaoUnica ); $i++){
                $nomeChave = $identificadoresChavesNaoUnica [$i][1];
                $atributosChave = EXTDAO_Tabela_chave_atributo::getListaNomeOrdenadoAtributoDaChave(
                                    $identificadoresChavesNaoUnica [$i][0], 
                                    $db);
                $atributosChave = Helper::arrayToUpper($atributosChave);
                $str = Helper::arrayToString($atributosChave, ",");
                if(strlen($str)){
                    $strConstrutor .= "\n\t\t\taddKey(\"$nomeChave\", new String[] { {$str} });";
                }
            }
        }
        $strConstrutor .= "
        }
        ";



        $objTabelasPai = $this->getTabelasPai($vetorTabelaBancoOrdenada, $idPV, $idBanco,$packageDatabase, $objTabela->getId(), array());
        if(count($objTabelasPai["import"]))
            $import = $import + $objTabelasPai["import"];
        $strTabelaPai = "\n\n\t\tpublic static String TABELAS_RELACIONADAS[] = {{$objTabelasPai["str"]}};";

        $abstractFunctions = "  
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        ";
        $strImport = "";
        for($i = 0; $i < count($import); $i++){
            $strImport .= "\t\timport {$import[$i]};\n";
        }

        $c .= "

        $strImport

        public abstract class $class extends Table
        {

        public static final String NAME = \"$table\";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        ";
        $c.= $strDeclaracaoEnum;

        $c.= $strDeclaracaoAtributo;
        $c.= $strTabelaPai;
        $c.= $strConstrutor;

        $c.= $abstractFunctions;
        $c.= $strFactoryProtocoloJson;
        $c.= "

        } // classe: fim


        ";
        fwrite($file, $c);
        fclose($file);


            $strGerouExt = "<p class=\"mensagem_retorno\">
                    &bull;&nbsp;&nbsp;Classe <b>$class</b> gerada com sucesso no arquivo <b>$filename</b>.

                    </p>"
                . "<p class=\"mensagem_retorno\">"
                . "        &nbsp;Path: $filename"
                . "</p>"  ;

        print $strGerouExt;


        }

    }

    public function getTabelasPai($vetorTabelaBancoOrdenada, $idPV, $idBanco, $packageDatabase, $idTabelaRaiz, $vetorProcurado){
        $strTabelaPai = "";
        $vetorProcurado[count($vetorProcurado)] = $idTabelaRaiz;

        $vetorIdTabelaPai = EXTDAO_Tabela::getListaIdTabelaPaiAteRaiz($idTabelaRaiz, $idPV, $idBanco);
        $import = array();
        for($i = 0 ; $i < count($vetorTabelaBancoOrdenada); $i++){

            if(!in_array($vetorTabelaBancoOrdenada[$i], $vetorIdTabelaPai)) continue;
            $objTabelaFk = new EXTDAO_Tabela();
            $objTabelaFk->select($vetorTabelaBancoOrdenada[$i]);
            if(strlen($strTabelaPai))
                $strTabelaPai.= ", ";
            $daoPai = "DAO".$objTabelaFk->getNomeClasseAndroid();
            $strTabelaPai.= $daoPai.".NAME";

            $import[count($import)] = "$packageDatabase.DAO.$daoPai";


        }
        return array ("str" => $strTabelaPai, "vetor" => $vetorProcurado, "import" => $import);
    }

    function getTabelasPaiBackup($packageDatabase, $idTabelaRaiz, $vetorProcurado){
        $strTabelaPai = "";
        $vetorProcurado[count($vetorProcurado)] = $idTabelaRaiz;
        $vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($idTabelaRaiz);
        $import = array();
        for($i = 0 ; $i < count($vetorIdAtributo); $i++){
            $idAtributo = $vetorIdAtributo[$i];
            $objAtributo = new EXTDAO_Atributo();
            $objAtributo->select($idAtributo);
            $idTabelaFk = $objAtributo->getFk_tabela_id_INT();


            if(strlen($idTabelaFk)){

                if(!in_array($idTabelaFk, $vetorProcurado)){
                    $objTabelaFk = new EXTDAO_Tabela();
                    $objTabelaFk->select($idTabelaFk);
                    if(strlen($strTabelaPai))
                        $strTabelaPai.= ", ";
                    $daoPai = "DAO".$objTabelaFk->getNomeClasseAndroid();
                    $strTabelaPai.= $daoPai.".NAME";

                    $import[count($import)] = "$packageDatabase.DAO.$daoPai";
                    $obj = $this->getTabelasPai($packageDatabase, $idTabelaFk, $vetorProcurado);
                    if(strlen($obj["str"])){
                        $strTabelaPai = $obj["str"].", ".$strTabelaPai;
                    }

                    if(count($obj["vetor"])){
                        $vetorProcurado = $obj["vetor"];

                        $import =  $obj["import"] + $import;
                    }
                }
            }
        }
        return array ("str" => $strTabelaPai, "vetor" => $vetorProcurado, "import" => $import);
    }
}
?>
