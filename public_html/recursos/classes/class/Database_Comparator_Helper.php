<?php

class Database_Comparator_Helper {

    public $comparisonDemoSQLite = "";
    public function __construct(){
        //TODO colocar o path correto do arquivo de dowload da OS aqui
        $this->comparisonDemoSQLite = Helper::acharRaiz() . "/conteudo/bancos/database.359752031375473.db";

    }

    public static $QUERY_GRID_CORPORACAO = "SELECT DISTINCT mc3.corporacao_INT AS corporacao_id,
                                              mc3.data_atualizacao_DATETIME AS data_atualizacao,
                                              m.id AS mobile_id,
                                              mi.id AS mobile_identificador_id,
                                              m.identificador AS mobile_descricao,
                                              m.modelo AS mobile_modelo,
                                              m.marca AS mobile_marca,
                                              m.cpu AS mobile_cpu,
                                              spvspm.id AS sistema_projeto_versao_sistema_produto_mobile_id,
                                              spvp.id AS sistema_projeto_versao_produto_id,
                                              spv.id AS sistema_projeto_versao_id,
                                              pv.nome AS projeto_versao_nome,
                                              sp.nome AS projeto_nome,
                                              c.nome AS corporacao_nome
                                       FROM mobile_conectado mc3
                                            JOIN
                                                (SELECT MAX(mc2.data_atualizacao_DATETIME) AS data_atualizacao, mc2.mobile_identificador_id_INT
                                                    AS mobile_identificador FROM mobile_conectado AS mc2
                                                    WHERE mc2.corporacao_INT IS NOT NULL
                                                    GROUP BY mc2.mobile_identificador_id_INT) AS j1
                                                ON j1.data_atualizacao=mc3.data_atualizacao_DATETIME
                                                AND j1.mobile_identificador=mc3.mobile_identificador_id_INT
                                            JOIN mobile_identificador AS mi
                                                ON mi.id=mc3.mobile_identificador_id_INT
																						JOIN mobile AS m	
																								ON m.id=mi.mobile_id_INT
                                            JOIN sistema_projetos_versao_sistema_produto_mobile AS spvspm
                                                ON spvspm.id=mi.sistema_projetos_versao_sistema_produto_mobile_id_INT
                                            JOIN sistema_projetos_versao AS spv
                                                ON spv.id=spvspm.sistema_projetos_versao_id_INT
                                            JOIN projetos_versao pv
                                                ON pv.id=spv.projetos_versao_id_INT
                                            JOIN sistema_projetos_versao_produto AS spvp
                                                ON spvp.id=spvspm.sistema_projetos_versao_produto_id_INT
                                            JOIN sistema_produto AS sp
                                                ON sp.id=spvp.sistema_produto_id_INT
                                            LEFT JOIN corporacao c 
                                                ON c.id  = mc3.corporacao_INT
                                        ORDER BY mc3.corporacao_INT, m.id, mi.id";

    public function getCorporacaoGridData(){

        $objBancoMysql = new Database();
        $objBancoMysql->query(self::$QUERY_GRID_CORPORACAO);

        //resultado total do grid
        $objResultado = array();

        $objCorporacaoAtual = null;
        $objMobile = null;
        $objMobileIdentificador = null;

        $corporacaoAnterior = null;
        $mobileAnterior = null;
        $mobileIdentificadorAnterior = null;
        while($dados = $objBancoMysql->fetchArray(MYSQL_ASSOC)){

            $corporacaoAtual = $dados["corporacao_id"];
            $dataAtualizacao = $dados["data_atualizacao"];
            $mobileAtual = $dados["mobile_id"];
            $mobileIdentificadorAtual = $dados["mobile_identificador_id"];
            $mobileDescricao = $dados["mobile_descricao"];
            $mobileModelo = $dados["mobile_modelo"];
            $mobileMarca = $dados["mobile_marca"];
            $mobileCpu = $dados["mobile_cpu"];
            $sistemaProjetoVersaoSistemaProdutoMobileId = $dados["sistema_projeto_versao_sistema_produto_mobile_id"];
            $sistemaProjetoVersaoProdutoId = $dados["sistema_projeto_versao_produto_id"];
            $sistemaProjetoVersaoId = $dados["sistema_projeto_versao_id"];
            $projetoVersaoNome = $dados["projeto_versao_nome"];
            $projetoNome = $dados["projeto_nome"];
            $nomeCorporacao = $dados["corporacao_nome"];
            
            if($mobileIdentificadorAnterior !== $mobileIdentificadorAtual){

                if($mobileIdentificadorAnterior !== null){

                    $objMobile->arrMobileIdentificador[] = $objMobileIdentificador;

                }

                $objMobileIdentificador = new CorporacaoGridMobileIdentificadorData();
                $objMobileIdentificador->idMobileIdentificador = $mobileIdentificadorAtual;
                $objMobileIdentificador->projectName = utf8_encode($projetoNome);
                $objMobileIdentificador->projectVersionName = utf8_encode($projetoVersaoNome);
                $objMobileIdentificador->sistemaProjetoVersaoId = $sistemaProjetoVersaoId;
                $objMobileIdentificador->sistemaProjetoVersaoProdutoId = $sistemaProjetoVersaoProdutoId;
                $objMobileIdentificador->sistemaProjetoVersaoSistemaProdutoMobileId = $sistemaProjetoVersaoSistemaProdutoMobileId;

                $objMobileIdentificador->connectionStatus = Helper::getUnixTimestamp($dataAtualizacao) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE > time();
                
                
//                $t0 = Helper::getUnixTimestamp($dataAtualizacao) ;
//                echo "alskdf::";
//                print_r($t0);
//                $t1 = Helper::getUnixTimestamp($dataAtualizacao) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE;
//                $t2 = time();
//                echo "<br\>$dataAtualizacao::$t1 < $t2";
                //$objMobileIdentificador->connectionStatus = print_r(time(), true);
                //$objMobileIdentificador->connectionStatus = $dataAtualizacao

            }

            if($mobileAnterior !== $mobileAtual){

                if($mobileAnterior !== null){

                    $objCorporacaoAtual->arrMobiles[] = $objMobile;

                }

                $objMobile = new CorporacaoGridMobileData();
                $objMobile->idMobile = $mobileAtual;
                $objMobile->nomeMobile = utf8_encode($mobileDescricao);
                $objMobile->marcaMobile = utf8_encode($mobileMarca);
                $objMobile->modeloMobile = utf8_encode($mobileModelo);
                $objMobile->cpuMobile = utf8_encode($mobileCpu);
                $objMobile->arrMobileIdentificador = array();

            }

            if($corporacaoAtual !== $corporacaoAnterior){

                if($corporacaoAnterior !== null){

                    $objResultado[] = $objCorporacaoAtual;

                }

                $objCorporacaoAtual = new CorporacaoGridData();
                $objCorporacaoAtual->idCorporacao = $corporacaoAtual;
                $objCorporacaoAtual->arrMobiles = array();
                $objCorporacaoAtual->nomeCorporacao = $nomeCorporacao;
            }

            $corporacaoAnterior = $corporacaoAtual;
            $mobileAnterior = $mobileAtual;
            $mobileIdentificadorAnterior = $mobileIdentificadorAtual;

        }

        if($objBancoMysql->rows() > 0) {

            //adicionando os elementos da ultima linha do result set
            $objMobile->arrMobileIdentificador[] = $objMobileIdentificador;
            $objCorporacaoAtual->arrMobiles[] = $objMobile;
            $objResultado[] = $objCorporacaoAtual;

       }

        echo json_encode($objResultado);

    }

    public function startComparisonProcess(){

        $POSTData = json_decode(file_get_contents('php://input'));

        $corporacaoId = $POSTData->corporacaoId;
        $mobileId = $POSTData->mobileId;
        $mobileIdentificadorId = $POSTData->mobileIdentificadorId;

        if(is_numeric($corporacaoId) && is_numeric($mobileId) && is_numeric($mobileIdentificadorId)){

            $comparisonId = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($mobileIdentificadorId, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_COMPARACAO_BANCO_SQLITE_MOBILE);

            $objRetorno = new stdClass();
            $objRetorno->comparisonId = $comparisonId;

            echo json_encode($objRetorno);

        }

    }


    public function getNumberOfOperationsBefore($comparisonId){

        $objBanco = new Database();
        $objBanco->query("SELECT COUNT(osm.id)
                          FROM operacao_sistema_mobile AS osm
                          WHERE data_abertura_DATETIME < (SELECT osm2.data_abertura_DATETIME FROM operacao_sistema_mobile AS osm2 WHERE osm2.id={$comparisonId})
                          AND osm.data_conclusao_DATETIME IS NULL
                          AND osm.mobile_identificador_id_INT=(SELECT osm2.mobile_identificador_id_INT FROM operacao_sistema_mobile AS osm2 WHERE osm2.id={$comparisonId})");

        return $objBanco->getPrimeiraTuplaDoResultSet(0);

    }

    public function cancelComparisonProcess(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $comparisonId = $POSTData->comparisonId;
        $objRetorno = new stdClass();

        if(is_numeric($comparisonId)){

            $statusCancelado = EXTDAO_Estado_operacao_sistema_mobile::CANCELADA;
            $objBanco = new Database();
            $objBanco->query("UPDATE operacao_sistema_mobile
                              SET estado_operacao_sistema_mobile_id_INT={$statusCancelado}, data_conclusao_DATETIME=NOW(), data_processamento_DATETIME=NOW()
                              WHERE id={$comparisonId}");

            $objRetorno->status = ComparisonStatus::PROCESS_CANCELED;
            $objRetorno->message = utf8_encode("Opera��o de compara��o cancelada pelo usu�rio.");

        }
        else{

            $objRetorno->status = ComparisonStatus::PROCESS_FINISHED_WITH_ERROR;
            $objRetorno->message = utf8_encode("Falha ao cancelar opera��o de compara��o.");

        }

        echo json_encode($objRetorno);

    }

    public function getComparisonProgress(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $comparisonId = $POSTData->comparisonId;
        $objRetorno = new stdClass();

        $objOperacaoSistemaMobile = new EXTDAO_Operacao_sistema_mobile();
        $objOperacaoSistemaMobile->select($comparisonId);
        $statusOperacao = $objOperacaoSistemaMobile->getEstado_operacao_sistema_mobile_id_INT();

        if($statusOperacao == EXTDAO_Estado_operacao_sistema_mobile::CANCELADA){

            $objRetorno->status = ComparisonStatus::PROCESS_CANCELED;
            $objRetorno->message = utf8_encode("Opera��o cancelada.");

        }
        elseif($statusOperacao == EXTDAO_Estado_operacao_sistema_mobile::ERRO_EXECUCAO){

            $objRetorno->status = ComparisonStatus::PROCESS_FINISHED_WITH_ERROR;
            $objRetorno->message = utf8_encode("Opera��o cancelada.");

        }
        elseif($objOperacaoSistemaMobile->getData_conclusao_DATETIME()){

            $objRetorno->status = ComparisonStatus::FILE_UPLOADED_TO_SERVER;
            $objRetorno->message = utf8_encode("Upload do arquivo SQLite conclu�do.");

        }
        elseif($objOperacaoSistemaMobile->getData_processamento_DATETIME()){

            $objRetorno->status = ComparisonStatus::PROCESS_RECEIVED_BY_DEVICE;
            $objRetorno->message = utf8_encode("Pedido de upload do arquivo SQLite recebido pelo dispositivo.");

        }
        else {

            $numeroProcessosAnteriores = $this->getNumberOfOperationsBefore($comparisonId);
            $objRetorno->status = ComparisonStatus::WAITING_DEVICE_TO_START_PROCESS;

            $objRetorno->message = utf8_encode($numeroProcessosAnteriores > 0 ?
                                                "Pedido ainda n�o processado pelo dispositivo m�vel. Existe(m) {$numeroProcessosAnteriores} anteriores na fila." :
                                                "Pedido ainda n�o processado pelo dispositivo m�vel. Seu pedido � o primeiro da fila.");

        }

        echo json_encode($objRetorno);

    }

    public function getTableList(){

        $POSTData = json_decode(file_get_contents('php://input'));

        $comparisonId = $POSTData->comparisonId;
        $mysqlParameters = $this->getComparisonMySQLConnectionParameters($comparisonId);
        if($mysqlParameters == null){
            echo json_encode(array());
            return;
        }
        $objDatabase = new Database_Comparator_MySQL($mysqlParameters->database, $mysqlParameters->host, $mysqlParameters->port, $mysqlParameters->user, $mysqlParameters->password);

        $db = new Database();
        $identificador = EXTDAO_Operacao_sistema_mobile::getIdentificadorDoPrototipoDaVersaoDoMobile($comparisonId, $db);

        $idPV = $identificador[0][0];
        $idBancoHMG = $identificador[0][1];
        $arrDefaultTables = EXTDAO_Tabela::getListaNomeTabelaQueSincronizam($idPV, $idBancoHMG, $db);
        
        $tabelasDiferentesEmAlgunsAspectos = array("usuario", "usuario_tipo", "permissao", "servico");

        $arrRetorno = array();
        $tempTables = $objDatabase->getTablesArray();
        if($tempTables  != null)
        foreach($tempTables  as $table){

            $obj = new stdClass();
            $obj->name = $table;
            $obj->isSelected = in_array($table, $arrDefaultTables) && ! in_array($table, $tabelasDiferentesEmAlgunsAspectos);

            $arrRetorno[] = $obj;

        }

        echo json_encode($arrRetorno);

    }

    public function getComparisonSQLitePath($comparisonId){
        //$path = this->comparisonDemoSQLite;
         $path = GerenciadorArquivo::getPathDoTipo(
                $comparisonId,
                "operacao_sistema_mobile",
                GerenciadorArquivo::ID_SCRIPT);
         
         $objAux = new EXTDAO_Operacao_download_banco_mobile();
         
         $idOSDA = EXTDAO_Operacao_download_banco_mobile::getIdOperacaoSistemaMobile($comparisonId);
         $objAux->select($idOSDA);
         $nomeArquivo = $objAux->getPath_script_sql_banco();
         
        $objAux->setOperacao_sistema_mobile_id_INT($comparisonId);
        $objAux->setPath_script_sql_banco(Helper::getNomeAleatorio("sqlite", ".db"));
        $objAux->formatarParaSQL();
        $objAux->insert();
         //echo $path.$nomeArquivo;
         //exit();
        return Helper::acharRaiz(). $path.$nomeArquivo;

    }

    public function getSQLiteFiles(){

        $fileList = Helper::getTodosOsArquivosDoDiretorio("");
        echo json_encode($fileList);

    }

    public function getSQLiteTables($sqliteFilePath){

        if(is_file($sqliteFilePath)){

            $objDatabase = new Database_Comparator_SQLite($sqliteFilePath);
            $arrTables = $objDatabase->getTablesArray();

            return $arrTables;

        }
        else{

            return null;

        }

    }

    public function showPriorComparisons(){

        $POSTData = json_decode(file_get_contents('php://input'));

        $corporacaoId = $POSTData->corporacaoId;
        $mobileIdentificadorId = $POSTData->mobileIdentificadorId;

        $objBanco = new Database();
        $objBanco->query("SELECT id, data_conclusao_DATETIME
                          FROM operacao_sistema_mobile
                          WHERE mobile_identificador_id_INT={$mobileIdentificadorId}
                          AND data_conclusao_DATETIME IS NOT NULL");

        $arrRetorno = array();
        while($dados = $objBanco->fetchArray()){

            $comparison = new PriorComparison();
            $comparison->comparisonId = $dados[0];
            $comparison->datetime = Helper::formatarDataTimeParaExibicao($dados[1]);
            $arrRetorno[] = $comparison;

        }

        echo json_encode($arrRetorno);

    }

    public function getFilteredResults(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $arrTables = $POSTData->selectedTables;
        $corporacaoId= $POSTData->corporacaoId;
        
        $comparisonId = $POSTData->comparisonId;
        $comparedDatabaseFileName = $this->getComparisonSQLitePath($comparisonId);
        
        $mysqlParameters = $this->getComparisonMySQLConnectionParameters($comparisonId);

        $objMainDatabase = new Database_Comparator_MySQL($mysqlParameters->database, $mysqlParameters->host, $mysqlParameters->port, $mysqlParameters->user, $mysqlParameters->password);
        
//        echo $comparedDatabaseFileName;
//        exit();
        $arrSecondaryDatabases = array(new Database_Comparator_SQLite($comparedDatabaseFileName, true));

        $objComparator = new Database_Comparator(
            $objMainDatabase, $arrSecondaryDatabases, $arrTables, $corporacaoId);

        for($i=0; $i < count($arrSecondaryDatabases); $i++){

            $comparisonResults[] = $objComparator->compareDatabases($i);

        }

        echo json_encode($comparisonResults);

    }

    public function getCorporacaoDoMobileIdentificador($mobileIdentificador){

        $objBanco = new Database();
        $queryGrid = self::$QUERY_GRID_CORPORACAO;
        $objBanco->query("SELECT qd.corporacao_id FROM ({$queryGrid}) AS qd WHERE qd.mobile_identificador_id={$mobileIdentificador}");

        if($objBanco->rows() > 0){

            return $objBanco->getPrimeiraTuplaDoResultSet(0);

        }

    }

    public function getComparisonMySQLConnectionParameters($comparisonId){

        $objOperacao = new EXTDAO_Operacao_sistema_mobile();
        $objOperacao->select($comparisonId);
        $idCorporacao = $this->getCorporacaoDoMobileIdentificador($objOperacao->getMobile_identificador_id_INT());

        $objConexaoHelper = new Database_Connection_Helper();
        $objReceivedParameters = $objConexaoHelper->getCorporacaoMySQLConnectionParameters($idCorporacao);

        return $objReceivedParameters;

    }

    /*
     *
     * M�TODOS DA COMPARA��O MOBILE
     *
     */

    public function startDataGenerationMobileJsonProcess(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $mobileIdentificador = $POSTData->mobileIdentificadorId;
        $urlJson = $POSTData->urlJson;

        if(is_numeric($mobileIdentificador)){

            $operationId = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($mobileIdentificador, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_CRUD_MOBILE_BASEADO_WEB);

            $objCrudAleatorio = new EXTDAO_Operacao_crud_mobile_baseado_web();
            $objCrudAleatorio->setUrl_json_operacoes_web($urlJson);
            $objCrudAleatorio->setSincronizar_BOOLEAN("1");
            $objCrudAleatorio->setOperacao_sistema_mobile_id_INT($operationId);
            $objCrudAleatorio->formatarParaSQL();
            $objCrudAleatorio->insert();

            $objRetorno = new stdClass();
            $objRetorno->operationId = $operationId;

            echo json_encode($objRetorno);

        }

    }

    public function startDataGenerationMobileProcess(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $mobileIdentificador = $POSTData->mobileIdentificadorId;
        $numberOfOperations = $POSTData->numberOfOperations;
        $percentageOfUpdates = $POSTData->percentageOfUpdates;
        $percentageOfDeletes = $POSTData->percentageOfDeletes;

        if(is_numeric($mobileIdentificador)){

            $operationId = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($mobileIdentificador, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_CRUD_ALEATORIO);

            $objCrudAleatorio = new EXTDAO_Operacao_crud_aleatorio();
            $objCrudAleatorio->setPorcentagem_edicao_FLOAT($percentageOfUpdates);
            $objCrudAleatorio->setPorcentagem_remocao_FLOAT($percentageOfDeletes);
            $objCrudAleatorio->setTotal_insercao_INT($numberOfOperations);
            $objCrudAleatorio->setSincronizar_BOOLEAN("1");
            $objCrudAleatorio->setOperacao_sistema_mobile_id_INT($operationId);
            $objCrudAleatorio->formatarParaSQL();
            $objCrudAleatorio->insert();

            $objRetorno = new stdClass();
            $objRetorno->operationId = $operationId;

            echo json_encode($objRetorno);

        }

    }
    
    
    public function startDataGenerationMultipleMobileProcess(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $idsMobileIdentificador = $POSTData->idsMobileIdentificador;
        
        $numberOfOperations = $POSTData->numberOfOperations;
        $percentageOfUpdates = $POSTData->percentageOfUpdates;
        $percentageOfDeletes = $POSTData->percentageOfDeletes;

        if(count($idsMobileIdentificador)){
            $ids = array();
            for($i = 0 ; $i < count($idsMobileIdentificador); $i++){
                $idMobileIdentificador = $idsMobileIdentificador[$i];
                $operationId = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($idMobileIdentificador, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_CRUD_ALEATORIO);

                $objCrudAleatorio = new EXTDAO_Operacao_crud_aleatorio();
                $objCrudAleatorio->setPorcentagem_edicao_FLOAT($percentageOfUpdates);
                $objCrudAleatorio->setPorcentagem_remocao_FLOAT($percentageOfDeletes);
                $objCrudAleatorio->setTotal_insercao_INT($numberOfOperations);
                $objCrudAleatorio->setSincronizar_BOOLEAN("1");
                $objCrudAleatorio->setOperacao_sistema_mobile_id_INT($operationId);
                $objCrudAleatorio->formatarParaSQL();
                $objCrudAleatorio->insert();
                $ids[count($ids)] = $operationId;
            }
            $objRetorno = new stdClass();
            $objRetorno->operationIds = $ids;
            echo json_encode($objRetorno);

        }

    }

    public function stopDataGenerationMobileProcess(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $operationId = $POSTData->operationId;

        if(is_numeric($operationId)){

            EXTDAO_Operacao_sistema_mobile::atualizaEstado($operationId, EXTDAO_Estado_operacao_sistema_mobile::CANCELADA);
            echo json_encode(true);

        }
        else{

            echo json_encode(false);

        }

    }

    public function getDataGenerationMobileProgress(){

        $POSTData = json_decode(file_get_contents('php://input'));
        $operationId = $POSTData->operationId;
        $objRetorno = new stdClass();

        $objOperacaoSistemaMobile = new EXTDAO_Operacao_sistema_mobile();
        $objOperacaoSistemaMobile->select($operationId);
        $statusOperacao = $objOperacaoSistemaMobile->getEstado_operacao_sistema_mobile_id_INT();

        if($statusOperacao == EXTDAO_Estado_operacao_sistema_mobile::CANCELADA){

            $objRetorno->status = ComparisonStatus::PROCESS_CANCELED;
            $objRetorno->message = utf8_encode("Opera��o {$operationId} cancelada.");

        }
        elseif($statusOperacao == EXTDAO_Estado_operacao_sistema_mobile::ERRO_EXECUCAO){

            $objRetorno->status = ComparisonStatus::PROCESS_FINISHED_WITH_ERROR;
            $objRetorno->message = utf8_encode("Opera��o {$operationId} cancelada.");

        }
        elseif($objOperacaoSistemaMobile->getData_conclusao_DATETIME()){

            $objRetorno->status = ComparisonStatus::FILE_UPLOADED_TO_SERVER;
            $objRetorno->message = utf8_encode("Gera��o de dados {$operationId} conclu�da.");

        }
        elseif($objOperacaoSistemaMobile->getData_processamento_DATETIME()){

            $objRetorno->status = ComparisonStatus::PROCESS_RECEIVED_BY_DEVICE;
            $objRetorno->message = utf8_encode("Pedido de gera��o de dados {$operationId} recebida pelo dispositivo m�vel.");

        }
        else {

            $numeroProcessosAnteriores = $this->getNumberOfOperationsBefore($operationId);
            $objRetorno->status = ComparisonStatus::WAITING_DEVICE_TO_START_PROCESS;

            $objRetorno->message = utf8_encode($numeroProcessosAnteriores > 0 ?
                "Pedido {$operationId} ainda n�o processado. Existe(m) {$numeroProcessosAnteriores} anteriores na fila." :
                "Pedido {$operationId} ainda n�o processado. Seu pedido � o primeiro da fila.");

        }

        echo json_encode($objRetorno);

    }

    public static function factory(){

        return new Database_Comparator_Helper();

    }

}

class PriorComparison {

    public $comparisonId;
    public $datetime;

}

class CorporacaoGridData {

    public $idCorporacao;
    public $nomeCorporacao;
    public $arrMobiles;

}

class CorporacaoGridMobileData {

    public $arrMobileIdentificador;
    public $idMobile;
    public $nomeMobile;
    public $marcaMobile;
    public $modeloMobile;
    public $cpuMobile;

}

class CorporacaoGridMobileIdentificadorData {

    public $idMobileIdentificador;
    public $connectionStatus;
    public $sistemaProjetoVersaoSistemaProdutoMobileId;
    public $sistemaProjetoVersaoProdutoId;
    public $sistemaProjetoVersaoId;
    public $projectName;
    public $projectVersionName;
    public $supportStatus;
    public $nomeCorporacao;
}

class ComparisonStatus {

    const WAITING_DEVICE_TO_START_PROCESS = 1;
    const PROCESS_RECEIVED_BY_DEVICE = 2;
    const FILE_UPLOADED_TO_SERVER = 3;
    const PROCESS_CANCELED = 4;
    const PROCESS_FINISHED_WITH_ERROR = 5;

}



?>