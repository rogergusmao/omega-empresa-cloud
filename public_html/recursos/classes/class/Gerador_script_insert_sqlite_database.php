<?php
class Gerador_script_insert_sqlite_database{
    public function __construct(){
        
    }
    
      public function gerarScriptEstrutura(
        $vetorTabelaBancoOrdenada, $sobrescrever, $idPVBBPrototipoWeb = null){
        
        set_time_limit(40 * 60);    
        
        $dbBibliotecaNuvem = new Database();
        if($idPVBBPrototipoWeb == null)
            $idPVBBPrototipoWeb = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
         
        $ret = EXTDAO_Projetos_versao_banco_banco::getIdProjetosEProjetosVersao($idPVBBPrototipoWeb, $dbBibliotecaNuvem);
        
        $idProjeto = $ret[0];
        $idPV = $ret[1];

        $arrExcessoesRelacionamento = array();


        $varGET= Param_Get::getIdentificadorProjetosVersao();
        
        
        $dbHomologacao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBPrototipoWeb, $dbBibliotecaNuvem);
        
        $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBPrototipoWeb, $dbBibliotecaNuvem);

        
        $idDiretorio = EXTDAO_Diretorio_web::getIdDiretorio($idPVBBPrototipoWeb, $dbBibliotecaNuvem);
        if(!strlen($idDiretorio)){
            Helper::imprimirMensagem("Não encontrou o ditorio web mapeado para projetos_versao_banco_banco = $idPVBBPrototipoAndroid", MENSAGEM_ERRO);
            exit();
        }
        $objDiretorio = new EXTDAO_Diretorio_web();
        $objDiretorio->select($idDiretorio);

        $objPadronizadorDatabase = new Padronizador_Database($dbHomologacao);
        
        $raizSrc = $objDiretorio->getRaiz();
        $raizDatabase = $objDiretorio->getClasses();

        $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raizSrc));
        $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
        $packageDatabase = str_replace("/", ".", $raizDatabase);

        $diretorio = $objDiretorio->getDAO();
        $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorio));
        
        $dir = dirname(__FILE__);

        $filename = Helper::getPathComBarra($raizSrc) 
            .Helper::getPathComBarra($diretorio)
            . "script_ordem_valores_".$dbHomologacao->getDBName().".sql";
        
        if(file_exists($filename))
            unlink($filename);

        echo "<p class=\"mensagem_retorno\">
                &bull;Gerando arquivo <b>$filename</b>.
                </p>";

        // open file in insert mode
        $file = fopen($filename, "w+");
        $filedate = date("d.m.Y");

        for($i = 0 ; $i < count($vetorTabelaBancoOrdenada); $i++){
            fwrite($file, $vetorTabelaBancoOrdenada[$i]."\n");
            if($i % 50 == 0)
                fflush($file);
        }
        fflush($file);
        fclose($file);


        $strGerouExt = "<p class=\"mensagem_retorno\">
                &bull;Script gerado com sucesso no arquivo <b>$filename</b>.
                </p>";

        print $strGerouExt;


        return true;
    }
    public function gerarScript(
        $vetorTabelaBancoOrdenada, $table, $key, 
        $label, $sobrescrever, $idPVBBPrototipoWeb = null){
        
        set_time_limit(40 * 60);    
        
        $dbBibliotecaNuvem = new Database();
         if($idPVBBPrototipoWeb == null)
            $idPVBBPrototipoWeb = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

        $ret = EXTDAO_Projetos_versao_banco_banco::getIdProjetosEProjetosVersao($idPVBBPrototipoWeb, $dbBibliotecaNuvem);
        
        $idProjeto = $ret[0];
        $idPV = $ret[1];

        $arrExcessoesRelacionamento = array();


        $varGET= Param_Get::getIdentificadorProjetosVersao();
        
        
        $dbHomologacao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBPrototipoWeb, $dbBibliotecaNuvem);
        
        $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBPrototipoWeb, $dbBibliotecaNuvem);

        
        $idDiretorio = EXTDAO_Diretorio_web::getIdDiretorio($idPVBBPrototipoWeb, $dbBibliotecaNuvem);
        if(!strlen($idDiretorio)){
            Helper::imprimirMensagem("Não encontrou o ditorio web mapeado para projetos_versao_banco_banco = $idPVBBPrototipoAndroid", MENSAGEM_ERRO);
            exit();
        }
        $objDiretorio = new EXTDAO_Diretorio_web();
        $objDiretorio->select($idDiretorio);

        $objPadronizadorDatabase = new Padronizador_Database($dbHomologacao);
        
        $raizSrc = $objDiretorio->getRaiz();
        $raizDatabase = $objDiretorio->getClasses();

        $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raizSrc));
        $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
        $packageDatabase = str_replace("/", ".", $raizDatabase);

        $diretorio = $objDiretorio->getDAO();
        $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorio));


        $objTabela = new EXTDAO_Tabela();
        if(is_numeric($table)){
            $idTabela = $table;
            $objTabela->select($table);
            $table = $objTabela->getNome();
        } else {
            $idTabela = EXTDAO_Tabela::existeTabela($table, $idBanco, $idPV);    
            if(!strlen($idTabela)){

                Helper::imprimirMensagem ("Tabela $table. idBanco: $idBanco. Banco ".$dbHomologacao->getDBName().". Projetos Versão: $idPV.  não encontrada.", MENSAGEM_ERRO);
                exit();
            }
            $objTabela->select($idTabela);
        }

        $objTabela->select($idTabela);
        
        $dir = dirname(__FILE__);

        $filename = Helper::getPathComBarra($raizSrc) 
            .Helper::getPathComBarra($diretorio)
            . "script_valores_".$dbHomologacao->getDBName()."_$table.sql";

        echo "<p class=\"mensagem_retorno\">
                    &bull;Gerando arquivo <b>$filename</b>. No banco: {$dbHomologacao->getDBName()}
                    </p>";

        
        $sql = "SHOW TABLES LIKE '$table';";

        $dbHomologacao->query($sql);

        if($dbHomologacao->rows() < 1){
            return;
        }
        
        $dbHomologacao->query($sql);

        if($dbHomologacao->rows() < 1){
            return;
        }

        $dadosAtributos = EXTDAO_Tabela::getDadosAtributosFk(
            $objTabela->getId(),
            $dbBibliotecaNuvem);
        if(empty($dadosAtributos)){
            if(file_exists($filename))
                unlink ($filename);
            return null;
        }
        
        $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

        // if file exists, then delete it
        if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
        {
            unlink($filename);
        }
//        echo $filename;
//        exit();
        if(!file_exists($filename)){

            
        // open file in insert mode
        $file = fopen($filename, "w+");
        $filedate = date("d.m.Y");

        
//        print_r($dadosAtributos);
        
        $strAttr = "";
        $indicesFks = array();
        $possuiCorporacao = false;
        for($i = 0 ; $i < count($dadosAtributos); $i++){
            if($i > 0){
                $strAttr .= ",";
            }
            if($dadosAtributos[$i][2] == "corporacao")
                $possuiCorporacao = true;
            $strAttr .= $dadosAtributos[$i][1];
            if($dadosAtributos[$i] != $dadosAtributos[$i][2]){
                $indicesFks[count($indicesFks)] = $i;    
            }
        }
        if(!$possuiCorporacao){
            fclose($file);
            unlink ($filename);
            return null;
        }
        fwrite ($file, json_encode($dadosAtributos)."\n");
        fflush($file);
        $q = "SELECT $strAttr FROM $table  ";
        
        $dbHomologacao->query($q);
        
        if (mysqli_num_rows($dbHomologacao->result) > 0) {
            mysqli_data_seek($dbHomologacao->result, 0);
        }
        
        for ($i = 0; $registro = mysqli_fetch_array($dbHomologacao->result, MYSQL_NUM); $i++) {
            
            fwrite($file, json_encode($registro)."\n");
            if($i % 50 == 0)
                fflush($file);
        }
        if($i == 0){
            fclose($file);
            unlink($filename);
        } else {
            fflush($file);    
            fclose($file);
        }

        $strGerouExt = "<p class=\"mensagem_retorno\">
                &bull;Script gerado com sucesso no arquivo <b>$filename</b>.
                </p>";

        print $strGerouExt;
        return true;

        }


    }
}
?>
