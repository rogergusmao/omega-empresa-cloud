<?php

class Menu extends InterfaceMenu{

    public function getArrayDoConteudoDoMenuCompleto(){

        $arrMenu = array(

            "Mobile Identificador" => array(
                "config" => new MenuConfig("Mobile Identificador", MENUCONFIG_MENU_ABA),
                "Gerenciar Mobile Identificador" => new MenuItem("lists_mobile_identificador"),
                "Cadastro Mobile Identificador" => new MenuItem("forms_mobile_identificador")
            ),

            "Tabelas" => array(
                "config" => new MenuConfig("Tabelas", MENUCONFIG_MENU_ABA),
                "Gerenciar Sincronizacao Tabela" => new MenuItem("lists_gerencia_sincronizacao_tabela"),
                "Gerenciar Equival�ncia de Tabelas" => new MenuItem("pages_seleciona_banco_banco")
            ),
            "Atributos" => array(

                "config" => new MenuConfig("Atributos", MENUCONFIG_MENU_ABA),
                "Gerenciar Atributos" => new MenuItem("lists_atributo"),
                "Gerenciar Equival�ncia de Atributos" => new MenuItem("lists_gerencia_atributo_atributo")
            ),
            "Monitorar Telefones" => array(

                "config" => new MenuConfig("Atributos", MENUCONFIG_MENU_ABA),
                "Hist�rico de Conex�o dos Telefones" => new MenuItem("lists_mobile_conectado"),
                "Telefones Cadastrados" => new MenuItem("lists_mobile_identificador"),
                "Opera��o de Sistema do Telefone" => array(
                    "config" => new MenuConfig("Opera��o de Sistema", MENUCONFIG_MENU_ABA),
                    "Cadastro" => new MenuItem("forms_operacao_sistema_mobile"),
                    "Gerenciar" => new MenuItem("lists_operacao_sistema_mobile")
                ),

            ),
            "Produtos" => array(
                "config" => new MenuConfig("Atributos", MENUCONFIG_MENU_ABA),
                "Cadastrar Sistema" => new MenuItem("forms_sistema"),
                "Gerenciar Sistemas" => new MenuItem("lists_sistema"),
                "Cadastrar Vers�o de Projeto" => new MenuItem("forms_sistema_projetos_versao"),
                "Gerenciar Vers�es de Projeto" => new MenuItem("lists_sistema_projetos_versao"),
                "Cadastrar Diret�rio Web" => new MenuItem("forms_diretorio_web"),
                "Gerenciar Diret�rio Web" => new MenuItem("lists_diretorio_web"),
                "Cadastrar Diret�rio Android" => new MenuItem("forms_diretorio_android"),
                "Gerenciar Diret�rio Android" => new MenuItem("lists_diretorio_android"),
                "Cadastrar Produto" => new MenuItem("forms_sistema_produto"),
                "Gerenciar Produtos" => new MenuItem("lists_sistema_produto"),
                "Cadastrar Produto Web" => new MenuItem("forms_sistema_produto_web"),
                "Gerenciar Produtos Web" => new MenuItem("lists_sistema_produto_web"),
                "Cadastrar Produto Mobile" => new MenuItem("forms_sistema_produto_mobile"),
                "Gerenciar Produtos Mobile" => new MenuItem("lists_sistema_produto_mobile"),
                "Cadastrar Nova Vers�o do Produto Web" => new MenuItem("forms_sistema_projetos_versao_sistema_produto_web"),
                "Gerenciar Nova Vers�o do Produto Web" => new MenuItem("lists_sistema_projetos_versao_sistema_produto_web"),
                "Cadastrar Nova Vers�o do Produto Mobile" => new MenuItem("forms_sistema_projetos_versao_sistema_produto_mobile"),
                "Gerenciar Nova Vers�o do Produto Mobile" => new MenuItem("lists_sistema_projetos_versao_sistema_produto_mobile"),
            ),
            "Sincronizador" => array(
                "config" => new MenuConfig("Atributos", MENUCONFIG_MENU_ABA),
                "Projetos"=> array(
                    "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                    "Cadastrar Projeto" => new MenuItem("forms_projetos"),
                    "Gerenciar Projeto" => new MenuItem("lists_projetos")
                ),
                "Vers�o do Projeto"=> array(
                    "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                    "Cadastrar Vers�o do Projeto" => new MenuItem("forms_projetos_versao"),
                    "Gerenciar Vers�o do Projeto" => new MenuItem("lists_projetos_versao")
                ),
                "Bancos da Vers�o do Projeto"=> array(
                    "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                    "Cadastrar Bancos dos Prot�tipos" => new MenuItem("forms_projetos_versao_banco_banco"),
                    "Gerenciar Bancos dos Prot�tipos" => new MenuItem("lists_projetos_versao_banco_banco")
                ),
                "Bancos" => array(
                    "config" => new MenuConfig("Bancos", MENUCONFIG_MENU_ABA),
                    "Cadastrar Banco" => new MenuItem("forms_banco"),
                    "Gerenciar Banco" => new MenuItem("lists_banco"),
                ),
                "Conex�es"=> array(
                    "config" => new MenuConfig("Conex�es", MENUCONFIG_MENU_ABA),
                    "Cadastrar Conex�es" => new MenuItem("forms_conexoes"),
                    "Gerenciar Conex�es" => new MenuItem("lists_conexoes"),
                )
            ),
            "Processos" => array(
                "config" => new MenuConfig("Atributos", MENUCONFIG_MENU_ABA),
                "Configuracao"=> array(
                    "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                    "Ciclos de Processos" => array(
                        "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                        "Cadastrar Ciclo" => new MenuItem("forms_processo_estrutura"),
                        "Gerenciar Ciclos" => new MenuItem("lists_processo_estrutura")
                    ),
                    "Tipos de Etapa dos Ciclos de Processo" => array(
                        "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                        "Cadastrar Tipo de Etapa" => new MenuItem("forms_tipo_processo_estrutura"),
                        "Gerenciar Tipos de Etapas" => new MenuItem("lists_tipo_processo_estrutura")
                    ),
                ),
                "Processos dos Projetos"=> array(
                    "config" => new MenuConfig("Projetos", MENUCONFIG_MENU_ABA),
                    "Cadastrar Processo" => new MenuItem("forms_projetos_versao_processo_estrutura"),
                    "Gerenciar Processos" => new MenuItem("lists_projetos_versao_processo_estrutura")
                ),
            ),
        );

        $menuSistema = array(

            "Sistema" => array(

                "config" => new MenuConfig("Sistema", MENUCONFIG_MENU_ABA),

                "Backup do Banco de Dados"  => array(

                    "config" => new MenuConfig("Backup do Banco de Dados", MENUCONFIG_LISTA_VERTICAL),
                    "Restaurar um Backup do Banco de Dados" => new MenuItem("pages_restaurar_backup_banco_dados", "restauracao_backup.png"),
                    "Fazer backup do Banco de Dados" => new MenuItem("pages_fazer_backup_banco_dados", "backup.png"),
                    "Gerenciar Rotinas de Backup Autom�tico" => new MenuItem("lists_backup_automatico", "gerenciar_backups.png")

                ),

            ),
            "Palavras" => array(

                "config" => new MenuConfig("Usu�rios", MENUCONFIG_LISTA_VERTICAL),
                "Cadastrar Palavra"	=> new MenuItem("forms_palavra", "cadastrar_usuario.png"),
                "Gerenciar Palavras"	=> new MenuItem("lists_palavra", "usuarios.png"),
                "Cadastrar Verbo" => new MenuItem("forms_verbo", "operacoes_usuarios.png"),
                "Gerenciar Verbo" => new MenuItem("lists_verbo", "operacoes_usuarios.png"),

            ),
            "Usu�rios" => array(

                "config" => new MenuConfig("Usu�rios", MENUCONFIG_MENU_ABA),

                "Usu�rios" => array(

                    "config" => new MenuConfig("Usu�rios", MENUCONFIG_LISTA_VERTICAL),
                    "Cadastrar Usu�rio do Sistema"	=> new MenuItem("forms_usuario", "cadastrar_usuario.png"),
                    "Gerenciar Usu�rios do Sistema"	=> new MenuItem("lists_usuario", "usuarios.png"),
                    "Visualizar Opera��es de Usu�rios no sistema" => new MenuItem("lists_operacao_sistema", "operacoes_usuarios.png"),

                ),

                "Classes de Usu�rios" => array(

                    "config" => new MenuConfig("Classes de Usu�rios", MENUCONFIG_LISTA_VERTICAL),
                    "Cadastrar Classe de Usu�rio"	=> new MenuItem("forms_usuario_tipo", "classe_usuarios.png"),
                    "Gerenciar Classes de Usu�rio"	=> new MenuItem("lists_usuario_tipo", "grupos_de_usuarios.png")

                ),

                "Sugest�es" => array(

                    "config" => new MenuConfig("Sugest�es", MENUCONFIG_LISTA_HORIZONTAL),
                    "Cadastrar Sugest�o"	=> new MenuItem("forms_sugestao_software", "cadastrar_sugestao.png"),
                    "Visualizar Sugest�es"	=> new MenuItem("lists_sugestao_software", "sugestao.png")

                ),

            )

        );

        $arrMenu = array_merge($arrMenu, $menuSistema);

        $menuSincronizacao = array(

            "Sincroniza��o" => array(

                "config" => new MenuConfig("Sincroniza��o", MENUCONFIG_MENU_ABA),

                "Sincroniza��o" => array(

                    "config" => new MenuConfig("Sincroniza��o", MENUCONFIG_LISTA_HORIZONTAL),

                    "Sincroniza��o " => array(

                        "config" => new MenuConfig("Sincroniza��o", MENUCONFIG_LISTA_VERTICAL),
                        "Importar Folha de Pagamento" => new MenuItem("forms_importar_dados_csv#tipo_import=folha_pagamentos", "faixas_salariais.png"),
                        "Importar Cargos" => new MenuItem("forms_importar_dados_csv#tipo_import=cargo", "cargos.png"),
                        "Importar Niveis/Estrutura Org." => new MenuItem("forms_importar_dados_csv#tipo_import=estrutura_organizacional", "nome_niveis_estrutura.png"),

                    )

                )

            )

        );

        $arrMenu = array_merge($arrMenu, $menuSincronizacao);

        return $arrMenu;

    }

}
