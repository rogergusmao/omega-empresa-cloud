<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XMLScript
 *
 * @author rogerfsg
 */
class XMLScript {
    //put your code here
    
    public function formataXMLProjetosVersaoBancoBancoScript($pObjProjetosVersaoBancoBancoScript){

        $vConsulta = "<?xml version=\"1.0\"?>
            <projetos_versao_banco_banco_script>
                    <id>{$pObjProjetosVersaoBancoBancoScript->getId()}</id>
                    <consulta>{$pObjProjetosVersaoBancoBancoScript->getConsulta()}</consulta>
                    <is_atualizacao_BOOLEAN>{$pObjProjetosVersaoBancoBancoScript->getIs_atualizacao_BOOLEAN()}</is_atualizacao_BOOLEAN>
                    <tipo_operacao_atualizacao_id_INT>{$pObjProjetosVersaoBancoBancoScript->getTipo_operacao_atualizacao_id_INT()}</tipo_operacao_atualizacao_id_INT>
                    <atributo_atributo_id_INT>{$pObjProjetosVersaoBancoBancoScript->getAtributo_atributo_id_INT()}</atributo_atributo_id_INT>
                    <tabela_tabela_id_INT>{$pObjProjetosVersaoBancoBancoScript->getTabela_tabela_id_INT()}</tabela_tabela_id_INT>
                    <projetos_versao_banco_banco_id_INT>{$pObjProjetosVersaoBancoBancoScript->getProjetos_versao_banco_banco_id_INT()}</projetos_versao_banco_banco_id_INT>
            </projetos_versao_banco_banco_script>
";
         return $vConsulta;
    }
    
}

?>
