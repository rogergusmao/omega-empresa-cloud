<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Script_insert_atributo
 *
 * @author rogerfsg
 */
class Script_vetor_obj extends Interface_script{
    //put your code here
    
    public $mVetor = array();
    public function __construct($pVetor) {
        if(count($pVetor)){
        $this->mVetor = $pVetor;    
        }
        
    }
    public function setVetorObjScriptUpdateAtributo($pVetor){
        if(count($pVetor))
            $this->mVetor = $pVetor;
    }
    
    protected function getVetorAtributo(){
        $vetor = array();
        
        if(!empty($this->mVetor)){
            $vConsultaTotal = "";
            foreach ($this->mVetor as $vObj) {
               $vConsulta = $vObj->getQuery();
                if(strlen($vConsulta))
                    $vConsultaTotal.= $vConsulta;
            }
            if(strlen($vConsultaTotal))$vetor[count($vetor)] = $vConsultaTotal;
        }
        return $vetor;
        
    }
}

?>
