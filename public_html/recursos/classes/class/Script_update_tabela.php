<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Script_insert_atributo
 *
 * @author rogerfsg
 */
class Script_update_tabela extends Interface_script{
    //put your code here
    public $mScriptAlteraNome = "";
    public $mVetorScriptDeletarChaveUnica = array();
    public $mVetorScriptInserirChaveUnica= array();
    
    
    public function adicionaScriptInserirChaveUnica($pScript){
        if(strlen($pScript))
            $this->mVetorScriptInserirChaveUnica[count($this->mVetorScriptInserirChaveUnica)] = $pScript;
    }
    
    public function adicionaScriptDeletarChaveUnica($pScript){
        if(strlen($pScript))
            $this->mVetorScriptDeletarChaveUnica[count($this->mVetorScriptDeletarChaveUnica)] = $pScript;
    }
    
    
    protected function getVetorAtributo(){
        $vetor = array($this->mScriptAlteraNome, 
            $this->mVetorScriptDeletarChaveUnica,
            $this->mVetorScriptInserirChaveUnica);
        
        return $vetor;
        
    }
}

?>
