<?php
class Seguranca extends InterfaceSegurancaUsuario
{   
     //lista de paginas que nao precisam de autenticacao
    public static $paginasExcecao = array("lembrar_senha", "actions", "omega_empresa_web");
    //actions que nao precisam de autenticacao
    public static $actionsExcecao = array("lembrarSenha", "login", "efetuarLogout",
        "loginSistemaOmega",
        "transfereScriptBancoAndroidParaWeb",
        "isServidorOnline",
        "consultaOperacaoSistemaMobile",
        "downloadApkAndroid",
        "consultaListaIdSistemaProjetosVersaoSistemaProdutoMobile",
        "consultaUltimoIdSistemaProjetosVersaoSistemaProdutoMobile",
        "downloadBancoAndroid",
        "conectaTelefone",
        "registraUsuarioMensagem",
        "consultaUltimoProjetosVersaoDoSistema",
        "registraLogErroMobile",
        "registraLogErroAtualizacaoBancoMobile",
        "consultaOperacaoDeSistemaMobile",
        "atualizaConexao",
        "desconectaTelefone",
        "modoSuporte",
        "atualizaEstadoOperacaoSistemaMobile",
        "logErroProdutoMobile",
        "falhaPrintScreenMobileParaWeb",
        "transfereArquivoMobileParaWeb",
        "transferePrintScreenMobileParaWeb",
        "consultaMonitoraTelaWebParaMobile",
        "criaOperacaoSistemaMobileDownloadBancoMobileAvulso",
        "criaOperacaoSistemaMobileDownloadBancoSQLiteMobileAvulso",
        "getArvoreBancoOmegaEmpresa");
    
    public function __construct()
    {
        $this->paginaExcecao = Seguranca::$paginasExcecao;
    }

    
    public function __actionLogin($dados, $senha, $db = null)
    {
        try {
        
            if($db == null)$db = new Database();

            HelperLog::verbose("validarLogin::verificaSenha");
            $nextAction = Helper::POST("next_action");

            $mensagem = $this->login($dados, $senha, $db, true);

            if ($mensagem->ok()) {
                if (Helper::POST("next_action") == "fecharEAtualizar") {

                    Helper::imprimirCabecalhoParaFormatarAction();
                    Helper::imprimirMensagem("Login realizado com sucesso, aguarde redirecionamento...", MENSAGEM_OK);
                    Helper::imprimirComandoJavascript("window.opener.location.reload(true);");
                    Helper::imprimirComandoJavascriptComTimer("window.close();", 3);
                    exit();
                } else {
                    return array("index.php?msgSucesso=Login realizado com sucesso");
                }
            } else {
                if (Helper::POST("next_action") == "fecharEAtualizar") {
                    $complementoGET = "next_action=fecharEAtualizar&";
                }
                return array("login.php?{$complementoGET}msgErro=Não foi possível efetuar o login com os dados de acesso informados.");
            }
        } catch (Exception $exc) {
            return array("login.php?msgErro=".  urlencode($exc->getMessage()));
        }
    }
    
    
    public function __actionLogout($mensagemUsuario = null)
    {
        
        $this->logout();
        return array("login.php?msgSucesso={$mensagemUsuario}");
    }
    
    public function factory()
    {
        return new Seguranca();
    }
    
    public static function isAutenticado(){
        $obj = Registry::get('Seguranca');
        
        return $obj->__isAutenticado();
    }
    
    public function isPaginaRestrita($pagina){
        return !in_array($pagina, Seguranca::$paginasExcecao);
    }
    public function isAcaoRestrita($classe, $acao)
    {
        
        if(!in_array($acao, Seguranca::$actionsExcecao)) {
            return true;
        }
        
        return false;
    }

    public function getAcoesLiberadasParaUsoAutenticado(){
        return null;
    }
}
