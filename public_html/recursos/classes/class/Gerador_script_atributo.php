<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
abstract class Gerador_script_atributo {
    //put your code here
    protected $objTabelaTabela;
    protected $objAtributoAtributo;
    protected $idScriptTabelaTabela;
    protected $objTabelaHomologacao;
    protected $prefixoTabela;
    protected $db;
    public function __construct($db = null){
        if($db == null)
            $this->db = $db;
    }
    
    public function setPrefixoTabela($prefixo){
        $this->prefixoTabela = $prefixo;
    }
    public function inicializa($pObjTabelaTabela, 
            $pObjAtributoAtributo, 
            $pIdScriptTabelaTabela,
            $pIdSPVBB){
        $this->idSPVBB = $pIdSPVBB;
        $this->objAtributoAtributo = $pObjAtributoAtributo;
        $this->idPVBB = $this->objAtributoAtributo->getProjetos_versao_banco_banco_id_INT();
//        $this->objTabelaTabela = new EXTDAO_Tabela_tabela();
        $this->objTabelaTabela = $pObjTabelaTabela;
        $this->idScriptTabelaTabela = $pIdScriptTabelaTabela;
    
        if($this->objTabelaTabela != null)
        $this->objTabelaHomologacao = $this->objTabelaTabela->getObjTabelaHomologacao();
    }
    public function getScriptAtributo(){
//        $this->pObjAtributoAtributo = new EXTDAO_Atributo_atributo();
        $vIdAtributoHomologacao = null;
        $vIdAtributoProducao = null;
        
        $vTipoAtualizacao = $this->objAtributoAtributo->getTipo_operacao_atualizacao_banco_id_INT($this->idPVBB);
        
        $pObjAtributoProducao = null;
        if(strlen($this->objAtributoAtributo->getProducao_atributo_id_INT())){
            $pObjAtributoProducao = new EXTDAO_Atributo();
            $pObjAtributoProducao->select($this->objAtributoAtributo->getProducao_atributo_id_INT());
            $vIdAtributoProducao = $this->objAtributoAtributo->getProducao_atributo_id_INT();
            
        }
        
        $pObjAtributoHomologacao = null;
        if(strlen($this->objAtributoAtributo->getHomologacao_atributo_id_INT())){
            if(
                     $this->objAtributoAtributo->getHomologacao_atributo_id_INT() == "4241"
                    ){
                $i = 0;
                $i += 1; 
            }
            $pObjAtributoHomologacao = new EXTDAO_Atributo();
            $pObjAtributoHomologacao->select($this->objAtributoAtributo->getHomologacao_atributo_id_INT());
            $vIdAtributoHomologacao = $this->objAtributoAtributo->getHomologacao_atributo_id_INT();
            
        }
        
        $vErro = false;
        if($vIdAtributoHomologacao == null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
            Helper::imprimirMensagem ("A atributo de homologa��o � nula e o tipo de opera��o de atualiza��o do banco est� como EDI��O. Id: {$this->objAtributoAtributo->getId()}", MENSAGEM_ERRO);
            $vErro = true;
        }
        else if($vIdAtributoProducao != null && $vIdAtributoHomologacao == null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){

            Helper::imprimirMensagem ("A atributo de produ��o � nula e o tipo de opera��o de atualiza��o do banco est� como INSER��O. Id: {$this->objAtributoAtributo->getId()}", MENSAGEM_ERRO);
            $vErro = true;
        }
        else if($vIdAtributoProducao == null && $vIdAtributoHomologacao != null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
            Helper::imprimirMensagem ("A atributo de homologa��o � nula e o tipo de opera��o de atualiza��o do banco est� como REMO��O. Id: {$this->objAtributoAtributo->getId()}", MENSAGEM_ERRO);
            $vErro = true;
        }
        else if($vIdAtributoProducao == null && $vIdAtributoHomologacao == null){
            Helper::imprimirMensagem ("N�o existem atributos no relacionamento: {$this->objAtributoAtributo->getId()}.", MENSAGEM_ERRO);
            $vErro = true;
        }
        if($vErro) 
            exit();

        if($vTipoAtualizacao == null) return null;
        else{
            $vObjScript = null;
            switch ($vTipoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
                    $vObjScript =  $this->constroiScriptDelete($this->objTabelaHomologacao, $pObjAtributoProducao);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                    $vObjScript =  $this->constroiScriptEdit($pObjAtributoHomologacao, $pObjAtributoProducao);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                    $vObjScript =  $this->constroiScriptInsert($pObjAtributoHomologacao);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION:
                    return null;
                default:
                    return null;
            }
            return $vObjScript;
        }
    }
    
    protected abstract function constroiScriptCreate($pObjAtributoHomologacao);
    
    protected abstract function constroiScriptDelete( $pObjTabelaHomologacao, $pObjAtributoProducao);    
    
    protected abstract function constroiScriptEdit($pObjAtributoHomologacao, $pObjAtributoProducao);    
    
    protected abstract function constroiScriptInsert( $pObjAtributoHomologacao);
    
    protected function insereScriptComandoBanco($consulta, $tipoComandoBanco){
        if(!strlen($consulta)){
            $i = 0;
            return;
        }
        $obj = new EXTDAO_Script_comando_banco();
        
        $obj->setConsulta($consulta);
        $vSeqINT = EXTDAO_Script_projetos_versao_banco_banco::getProximoSeqScriptComandoBanco($this->idSPVBB);
        $obj->setSeq_INT($vSeqINT);
        $obj->setScript_tabela_tabela_id_INT($this->idScriptTabelaTabela);
        $obj->setAtributo_atributo_id_INT($this->objAtributoAtributo->getId());
        $obj->setTipo_comando_banco_id_INT($tipoComandoBanco);
        $obj->setTabela_chave_tabela_chave_id_INT(null);
        
        $obj->formatarParaSQL();
        $obj->insert();
        $obj->selectUltimoRegistroInserido();
        return $obj;
    }    
}

?>
