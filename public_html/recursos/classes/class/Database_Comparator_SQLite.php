<?php

class Database_Comparator_SQLite {// Classe : in�cio

    private $filename;
    private $readonly;
    public $query;
    public $result;
    public $array;
    public $object;
    public $rows;
    public $lastInsertedId;
    public $now;
    public $indiceLink;
    public $errMessage;
    public static $arrLinks = array();
    public static $arrFilenames = array();
    public static $arrAutocommit = array();
    public $arrTables = array();

    public function __construct( $filename = false, $readonly = false ) {// M�todo : in�cio - Construtor

        if ($filename) {
            $this->filename = $filename;
        }

        if($readonly){
            $this->readonly = $readonly;
        }

        $this->lastInsertedId = "";
        $this->rows() = 0;
        $this->now = date("Y-m-d H:i:s");

        $this->indiceLink = -1;
        $this->openLink();

    }

    public function getTablesArray($forceUpdate=false) {

        if(count($this->arrTables) == 0 || $forceUpdate) {
            
            $this->query("SELECT name FROM sqlite_master WHERE type='table'");

            while ($dados = $this->fetchArray()) {

                $this->arrTables[] = $dados[0];

            }

        }

        return $this->arrTables;

    }

    public function getFieldsArray($tableName){

        $arrRetorno = array();
        $this->query("PRAGMA table_info('{$tableName}')");

        while($dados = $this->fetchObject()){

            $objRetorno = new stdClass();
            $objRetorno->Field = $dados->name;
            $objRetorno->Type =

            $arrRetorno[] = $dados;

        }

        return $arrRetorno;

    }

// M�todo : Fim - Construtor

    public function iniciarTransacao() {

        mysqli_query(self::$arrLinks[$this->indiceLink], "BEGIN TRANSACTION;");

        self::$arrAutocommit[$this->indiceLink] = false;
    }

    public function commitTransacao() {

        if (self::$arrAutocommit[$this->indiceLink] === false) {

            mysqli_query(self::$arrLinks[$this->indiceLink], "COMMIT TRANSACTION;");
        }
    }

    public function rollbackTransacao() {

        if (self::$arrAutocommit[$this->indiceLink] === false) {

            mysqli_query(self::$arrLinks[$this->indiceLink], "ROLLBACK TRANSACTION;");
        }
    }

    public function getFilename() {

        return $this->filename;
    }

    public function __destruct() {

    }

    private function openLink() {// M�todo : in�cio

        if (!in_array($this->filename, self::$arrFilenames)) {

            $indiceLink = count(self::$arrLinks);
            $this->indiceLink = $indiceLink;

            self::$arrFilenames[$this->indiceLink] = $this->DBName;
            self::$arrAutocommit[$this->indiceLink] = true;

            try{

                $dbHandler = new PDO("sqlite:{$this->filename}");
                
                self::$arrLinks[$this->indiceLink] = $dbHandler;

            }
            catch(PDOException $ex){

                echo "
        			<div class='divErro'>
        				<table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'>
        					<tr>
        						<td class='imgErro' valign='top'>&nbsp;</td>
        						<td class='conteudosDiv'>
        							<b>CONEX�O COM O BANCO DE DADOS SQLITE N�O PODE SER ESTABELECIDA</b>
        							<br />Descri��o: " . $ex->getMessage() . "
           						</td>
        					</tr>
        				</table>
        			</div>";

                exit();

            }

        }
        else {

            $this->indiceLink = array_search($this->filename, self::$arrFilenames);

        }

    }

    public function getNumberOfRows(){

        $query = "SELECT COUNT(*) FROM ({$this->query})";
        return self::$arrLinks[$this->indiceLink]->query($query)->fetchColumn(0);

    }

// M�todo : fim

    public function imprimirQuery($formatacao = false) {

        if ($formatacao) {

            Helper::imprimirMensagem($this->query);

        }
        else {

            Helper::imprimirMensagemNaoFormatada($this->query . "<br /><br />");

        }
    }

    public function query($query, $forcarParadaOnError=true) {// M�todo : in�cio

        if ($this->indiceLink == -1) {

            $this->openLink();

        }

        $errorMessage = "";
        $this->query = $query;

        try{

            $this->result = self::$arrLinks[$this->indiceLink]->query($query);
            
        }
        catch(PDOException $ex){
            
            $this->result = false;
            echo Helper::imprimirMensagem("Excecao capturada: [". $ex->getMessage()."]. TRACE [ ".$ex->getTraceAsString()."]");
            
        }

        if ($this->result === false) {
            $pdo = self::$arrLinks[$this->indiceLink];
            
            $errorMessage = "Code: {$pdo->errorCode()} . Info: ".print_r($pdo->errorInfo(), true).".";
            if (substr(strtolower($query), 0, 6) == "insert") {

                if ($errorMessage) {

                    $this->imprimirQuery(true);
                    Helper::imprimirMensagem("O Registro n�o p�de ser inserido devido ao seguinte erro: {$errorMessage}", MENSAGEM_WARNING);
                    $this->rollbackTransacao();
                    exit();
                }

            }
            elseif (substr(strtolower($query), 0, 6) == "delete") {

                if ($errorMessage) {

                    Helper::imprimirMensagem("O Registro n�o p�de ser removido devido ao seguinte erro: {$errorMessage}", MENSAGEM_WARNING);
                    Helper::imprimirMensagem(mysqli_error(self::$arrLinks[$this->indiceLink]));
                    $this->rollbackTransacao();
                    exit();
                }

            }

            $this->rollbackTransacao();
            $this->errMessage = $errorMessage ? $errorMessage : "Query error.";
            $this->rows() = 0;

            if($forcarParadaOnError) {

                Helper::imprimirMensagem("<b>ERRO NA EXECU��O DO COMANDO SQL</b>

                                     <b>QUERY: </b> " . nl2br($this->query) . "
                                     <b>ERRO: </b> " . $errorMessage . "
                                     ", MENSAGEM_ERRO);

                exit();

            }

        }
        else {

            if (substr(strtolower(str_replace("(", "", $query)), 0, 6) == "select" || substr(strtolower($query), 0, 4) == "show") {

                $this->rows() = $this->getNumberOfRows();
                return $this->result;

            }
            elseif (substr(strtolower($query), 0, 6) == "insert") {

                $con = self::$arrLinks[$this->indiceLink]->lastInsertId();

                if (!$con) {

                    $this->rollbackTransacao();

                } else {

                    $this->lastInsertedId = $con;
                    return $this->result;
                }

            }
            else {

                return $this->result;

            }
        }
    }

// M�todo : fim

    public function fetchArray($constante=PDO::FETCH_BOTH) {//M�todo : in�cio

        $this->array = $this->result->fetch($constante);
        return $this->array;

    }

//M�todo : fim

    public function fetchObject() {//M�todo : in�cio

        $this->object = $this->result->fetch(PDO::FETCH_OBJ);
        return $this->object;

    }

//M�todo : fim

    public function getPrimeiraTuplaDoResultSet($campo) {//M�todo : in�cio

        $firstLine = $this->result->fetch(PDO::FETCH_BOTH);
        return $firstLine[$campo];

    }

//M�todo : fim

    public function getResultSet() {

        return $this->result;
    }

    public function resultSet($linha, $coluna) {

        return self::sqlite_result($this->result, $linha, $coluna);
    }

    public static function sqlite_result($resultSet, $linha, $coluna) {

        $arrLinha = $resultSet->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_ABS, $linha);
        return $arrLinha[$coluna];

    }

}

// Class : fim
?>