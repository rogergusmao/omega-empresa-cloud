<?php
class Gerador_android_extdao{
    public function __construct(){
        
    }
    public function gerarEXTDAO(
        $table, 
        $classEXTDAO, 
        $key, 
        $label, 
        $ext, 
        $sobrescrever,
        $idPVBB = null,
        $objBanco  = null){
        if($objBanco==null)
        $objBanco = new Database();

        $arrExcessoesRelacionamento = array();

        if($idPVBB == null)
        $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

        $varGET= Param_Get::getIdentificadorProjetosVersao();
        $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
        $objPVBB->select($idPVBB);
        $idPV= $objPVBB->getProjetos_versao_id_INT();
        $objPV = new EXTDAO_Projetos_versao();
        $objPV->select($idPV);
        $idProjeto = $objPV->getProjetos_id_INT();
        $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBB);
        $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBB, $objBanco);
        
        $idPVBBPrototipoAndroid = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoAndroidDoProjetosVersao($idPV, $objBanco);
        $idDiretorio = EXTDAO_Diretorio_android::getIdDiretorio($idPVBBPrototipoAndroid, $objBanco);
        if(!strlen($idDiretorio)){
            Helper::imprimirMensagem("Não encontrou o ditorio web mapeado para projetos_versao_banco_banco = $idPVBBPrototipoAndroid", MENSAGEM_ERRO);
            exit();
        }
        $objDiretorio = new EXTDAO_Diretorio_android();
        $objDiretorio->select($idDiretorio);

        $raiz = $objDiretorio->getRaizSrc();
        $raizDatabase = $objDiretorio->getRaiz_biblioteca_database();

        $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raiz));
        $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
        $packageDatabase = str_replace("/", ".", $raizDatabase);

        $diretorioDAO = $objDiretorio->getDAO();
        $diretorioEXTDAO = $objDiretorio->getEXTDAO();
        $packageEXTDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorioEXTDAO));
        $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorioDAO));

        $objTabela = new EXTDAO_Tabela();
        if(is_numeric($table)){
            $idTabela = $table;
            $objTabela->select($table);
            $table = $objTabela->getNome();
        } else {
            $idTabela = EXTDAO_Tabela::existeTabela($table, $idBanco, $idPV);    
            if(!strlen($idTabela)){

                Helper::imprimirMensagem ("Tabela $table. idBanco: $idBanco. Banco ".$database->getDBName().". Projetos Versão: $idPV.  não encontrada.", MENSAGEM_ERRO);
                exit();
            }
            $objTabela->select($idTabela);
        }

        if(!strlen($idTabela)){
            Helper::imprimirMensagem ("Estrutura android não foi inicializada.", MENSAGEM_ERRO);
            exit();
        }
        $objTabela->select($idTabela);
        $classEXTDAO = "EXTDAO".$objTabela->getNomeClasseAndroid();
        $classDAO = "DAO".$objTabela->getNomeClasseAndroid();
        $dir = dirname(__FILE__);

        $filename = Helper::getPathComBarra($raiz) .Helper::getPathComBarra($diretorioEXTDAO) . $classEXTDAO . ".java";

        $sql = "SHOW TABLES LIKE '$table';";

        $database->query($sql);

        if($database->rows() < 1){

            return;

        }

        $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

        // if file exists, then delete it
        if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
        {
            unlink($filename);
        }

        if(!file_exists($filename)){

        // open file in insert mode
        $file = fopen($filename, "w+");
        $filedate = date("d.m.Y");

        $c = "";

        $c = "

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  $classEXTDAO
        * DATA DE GERAÇÃO: $filedate
        * ARQUIVO:         $classEXTDAO.java
        * TABELA MYSQL:    $table
        * BANCO DE DADOS:  $database->database
        * -------------------------------------------------------
        *
        */

        package $packageEXTDAO;

        import $packageDAO.$classDAO;
        import $packageDatabase.Attribute;
        import $packageDatabase.Attribute.SQLLITE_TYPE;
        import $packageDatabase.Attribute.TYPE_RELATION_FK;
        import $packageDatabase.Database;
        import $packageDatabase.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    ";
        $strImport = "";
        $strDeclaracaoAtributo = "";
        $strConstrutor = "";
        $vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($objTabela->getId());
        for($i = 0 ; $i < count($vetorIdAtributo); $i++){
            $idAtributo = $vetorIdAtributo[$i];
            $objAtributo = new EXTDAO_Atributo();
            $objAtributo->select($idAtributo);
            $nomeAtributo = $objAtributo->getNome();
            $strDeclaracaoAtributo .= "\t\tpublic static final String ".  strtoupper($nomeAtributo)." = \"$nomeAtributo\";\n";
        }

        $objTabela->getListaIdAtributoChaveUnica();

        $strConstrutor.= "
        // *************************
        // CONSTRUTOR
        // *************************
        public $classEXTDAO(Database database){
            super(database);

        }
        ";

        $strFabrica.= "
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new $classEXTDAO(this.getDatabase());

        }
        ";

        $c .= "
        $strImport

        public class $classEXTDAO extends $classDAO
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        ";

        $c.= $strConstrutor;
        $c.= $strFabrica;
        $c.= "

        } // classe: fim


        ";
        fwrite($file, $c);
        fclose($file);


            $strGerouExt = "<p class=\"mensagem_retorno\">
                    &bull;&nbsp;&nbsp;Classe <b>$classEXTDAO</b> gerada com sucesso no arquivo <b>$filename.php</b>.

                    </p>"
                . "<p class=\"mensagem_retorno\">"
                . "        &nbsp;Path: $filename"
                . "</p>"  ;

        print $strGerouExt;


        }

    }
}
?>
