<?php
class Gerador_web_extdao{
    public function __construct(){
        
    }
public function gerarEXTDAOWeb(
    $table, 
    $classEXTDAO, 
    $key, 
    $label, 
    $ext, 
    $sobrescrever,
    $idPVBBPrototipoWeb,
    $objBanco= null){
    if($objBanco == null)
    $objBanco = new Database();

    $arrExcessoesRelacionamento = array();

    if($idPVBBPrototipoWeb == null)
    $idPVBBPrototipoWeb = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    
    $varGET= Param_Get::getIdentificadorProjetosVersao();
    
    $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao(
        $idPVBBPrototipoWeb, $objBanco);
    $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao(
        $idPVBBPrototipoWeb, $objBanco);
    
    $idDiretorio = EXTDAO_Diretorio_web::getIdDiretorio($idPVBBPrototipoWeb, $objBanco);
    if(!strlen($idDiretorio)){
        Helper::imprimirMensagem("Não encontrou o ditorio web mapeado para projetos_versao_banco_banco = $idPVBBPrototipoWeb", MENSAGEM_ERRO);
        exit();
    }
    $objDiretorio = new EXTDAO_Diretorio_web($objBanco);
    $objDiretorio->select($idDiretorio);
    
    $raiz = $objDiretorio->getRaiz();
    $raizDatabase = $objDiretorio->getClasses();
    
    $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raiz));
    $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
    $packageDatabase = str_replace("/", ".", $raizDatabase);
    
    $diretorioDAO = $objDiretorio->getDAO();
    $diretorioEXTDAO = $objDiretorio->getEXTDAO();
    $packageEXTDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorioEXTDAO));
    $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorioDAO));

    $idPV= EXTDAO_Projetos_versao_banco_banco::getIdProjetosVersao($idPVBBPrototipoWeb, $objBanco);

    $objTabela = new EXTDAO_Tabela($objBanco);
    if(is_numeric($table)){
        $idTabela = $table;
        $objTabela->select($table);
        $table = $objTabela->getNome();
    } else {

        $idTabela = EXTDAO_Tabela::existeTabela(
                $table
                , $idBanco
                , $idPV);
        if(!strlen($idTabela)){

            Helper::imprimirMensagem ("Tabela $table. idBanco: $idBanco. Banco ".$database->getDBName().". Projetos Versão: $idPV.  não encontrada.", MENSAGEM_ERRO);
            exit();
        }
        $objTabela->select($idTabela);
    }
    $objTabela->select($idTabela);
    $classEXTDAO = "EXTDAO_".$objTabela->getNomeClasseWeb();
    $classDAO = "DAO_".$objTabela->getNomeClasseWeb();
    
    $dir = dirname(__FILE__);

    $filename = Helper::getPathComBarra($raiz) .Helper::getPathComBarra($diretorioEXTDAO) . $classEXTDAO . ".php";
    
    $sql = "SHOW TABLES LIKE '$table';";

    $database->query($sql);

    if($database->rows() < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $file = fopen($filename, "w+");

        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;

        $cadastrado[0] = "cadastrado";
        $cadastrado[1] = "cadastrada";
        $cadastrado[2] = "cadastrados";
        $cadastrado[3] = "cadastradas";

        $modificado[0] = "modificado";
        $modificado[1] = "modificada";
        $modificado[2] = "modificados";
        $modificado[3] = "modificadas";

        $excluido[0] = "excluído";
        $excluido[1] = "excluída";
        $excluido[2] = "excluídos";
        $excluido[3] = "excluídas";

        $foi[0] = "foi";
        $foi[1] = "foi";
        $foi[2] = "foram";
        $foi[3] = "foram";

        $artigo[0] = "O";
        $artigo[1] = "A";
        $artigo[2] = "Os";
        $artigo[3] = "As";

        $genero   = $_POST["genero_entidade"]; //F ou M
        $nomeSing = $_POST["entidade_singular"];
        $nomePlu  = $_POST["entidade_plural"];
        $numeroCadastros = $_POST["numeroCadastros"];

        $index = 0;

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $mensagemCadastroSucesso = $artigo[$index] . " " . $nomeMsg . " " . $foi[$index] . " " . $cadastrado[$index] . " com sucesso.";

        $mensagemEdicaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $modificado[$index] . " com sucesso.";
        
        while($row = mysqli_fetch_row($result)){

            $col = $row[0];
            $colUpper = ucfirst($col);
            $nomeColunaFormatado = ucwords($_POST["nomexcampo_" . $col]);

            $ocorrencias = array(" De ", " Da ", " Do ", " Das ", " Dos ", " Em ", " No ", " Na ", " Nos ", " Nas ");
            $substituicoes = array(" de ", " da ", " do ", " das ", " dos ", " em ", " no ", " na ", " nos ", " nas ");

            $nomeColunaFormatado = str_replace($ocorrencias, $substituicoes, $nomeColunaFormatado);
         
            $instLabel .= "\t\t\t\$this->label_{$col} = \"{$nomeColunaFormatado}\";\n";
            
            if(substr_count($col, "_ARQUIVO") > 0 || substr_count($col, "_IMAGEM") > 0){

               $instDiretorio .= "\t\t\t\$this->diretorio_{$col} = \"\";        //caminho a partir de da raiz, com '/' no final\n";
  
               if(substr_count($col, "_IMAGEM") > 0){

                    $instDimensoesImagem .= "\t\t\t\$this->dimensoes_{$col} = array(\"\", \"\");    //largura, altura (em pixels)\n";
                
               }
               
            }

        }
        
        if(strlen($instDiretorio)){
            
            $instDiretorio = "
              
        public function setDiretorios(){

            $instDiretorio

        }
            
        ";
            
            $strChamadas .= "\t\t\t\$this->setDiretorios();\n";
            
        }
        
        if(strlen($instDimensoesImagem)){
            
            $instDimensoesImagem = "
              
        public function setDimensoesImagens(){

            $instDimensoesImagem

        }
        ";
            
            $strChamadas .= "\t\t\t\$this->setDimensoesImagens();\n";
            
        }

        $conteudo = "<? //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     $ext
    * NOME DA CLASSE DAO: $class
    * DATA DE GERAÇÃO:    $filedate
    * ARQUIVO:            $ext.php
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class {$ext} extends {$class}
    {

        public function __construct(\$configDAO){

            parent::__construct(\$configDAO);

            \t\$this->nomeClasse = \"{$ext}\";


            \$this->setLabels();



$strChamadas


        }

        public function setLabels(){

$instLabel

        }

$instDiretorio

$instDimensoesImagem

        public function factory(){

            return new $ext();

        }

	}

    ";

        fwrite($file, $conteudo);
        fclose($file);
		
        $strGerouExt = "<p class=\"mensagem_retorno\">
                        &bull;&nbsp;&nbsp;Classe <b>{$ext}</b> gerada com sucesso no arquivo <b>$filename</b>.
                        </p>";

    print $strGerouExt;

    }

}
}

?>
