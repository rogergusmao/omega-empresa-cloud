<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trunk
 *
 * @author home
 */
class Trunk {
     
    public $idsProjetosVersaoPorTipoAnaliseProjeto;
    public $idProjetos = ID_PROJETOS;
    public $idTipoConjuntoAnalise = EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado;
    
    private static $singleton = null;
    public static function factory(){
        if(Trunk::$singleton == null ) Trunk::$singleton = new Trunk();
        return Trunk::$singleton;
    }
    //put your code here
    private function __construct(){
        $db = new Database();
        $this->idsProjetosVersaoPorTipoAnaliseProjeto = EXTDAO_Trunk::getProjetosVersaoDoTrunk(
            $this->idProjetos, $this->idTipoConjuntoAnalise, $db);
    
        
    }
    
    public function getProjetosVersaoBancoBanco($pIdTipoAnaliseProjeto, $pIdTipoBanco){
//        print_r($this->idsProjetosVersaoPorTipoAnaliseProjeto);
//        exit();
        for($i = 0 ; $i < count($this->idsProjetosVersaoPorTipoAnaliseProjeto); $i++){
            $idTipoAnaliseProjeto = $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i]['tipo_analise_projeto_id_INT'];
            $idTipoBanco = $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i]['tipo_banco_id_INT'];
            if($idTipoAnaliseProjeto == $pIdTipoAnaliseProjeto 
                && $idTipoBanco == $pIdTipoBanco){
                
                return $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i]['projetos_versao_banco_banco_id_INT'];
            }
        }
        return null;
    }
    
    public function getTipoAnaliseDoProjetosVersaoBancoBanco($idPVBB){
        for($i = 0 ; $i < count($this->idsProjetosVersaoPorTipoAnaliseProjeto); $i++){
            $id = $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][2];
            if($idPVBB == $id)
                return $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][0];
        }
        return null;
    }
    
    public function getTipoAnaliseDoProjetosVersao($idPV){
        
        for($i = 0 ; $i < count($this->idsProjetosVersaoPorTipoAnaliseProjeto); $i++){
            $id = $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][1];
            if($idPV == $id)
                return $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][0];
        }
        return null;
    }
    
    public function getProjetosVersaoDaAnaliseDeProjeto($pIdTipoAnaliseProjeto){
        for($i = 0 ; $i < count($this->idsProjetosVersaoPorTipoAnaliseProjeto); $i++){
            $idTipoAnaliseProjeto = $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][0];
            if($idTipoAnaliseProjeto == $pIdTipoAnaliseProjeto)
                return $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][1];
        }
        return null;
    }
    //Temos varios projetos_versao para um dado banco
    //porem so um dos projetos analise que contem o banco de dados
    public function getPVDoBancoDeDadosDeProducao($idPV){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        $idPVPrototipoProducao = $idPV;
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersao($idPV);
        switch ($idTipoAnaliseProjeto) {
            
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na analise de 'atualiza��o banco de dados mysql do mysql'
                $idPVPrototipoProducao = $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web);
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                $idPVPrototipoProducao = $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::PROTOTIPO);
                break;
            default:
                break;
        }
        return $idPVPrototipoProducao;
    }
    
     //Temos varios projetos_versao para um dado banco
    //porem so um dos projetos analise que contem o banco de dados
    public function getPVBBDoBancoDeDadosDeProducaoRelacionadoAoProjetosVersao($idPV, $tipoBanco){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersao($idPV);
        switch ($idTipoAnaliseProjeto) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na analise de 'atualiza��o banco de dados mysql do mysql'
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
            
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID);
            default:
                break;
        }
        return $this->getProjetosVersaoBancoBanco($idTipoAnaliseProjeto, $tipoBanco);
    }
    
    //Temos varios projetos_versao para um dado banco
    //porem so um dos projetos analise que contem o banco de dados
    public function getPVDoBancoDeDadosDeProducaoRelacionadoAoPVBB($idPVBB){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersaoBancoBanco($idPVBB);
        switch ($idTipoAnaliseProjeto) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na analise de 'atualiza��o banco de dados mysql do mysql'
                return $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web);
                
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
            
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                return $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::PROTOTIPO);
            default:
                break;
        }
        return $this->getProjetosVersaoDaAnaliseDeProjeto($idTipoAnaliseProjeto);
    }
    
    
     //Temos varios projetos_versao para um dado banco
    //porem so um dos projetos analise que contem o banco de dados
    public function getPVBBDoBancoDeDadosDeProducao($idPVBB){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersaoBancoBanco($idPVBB);
        
        switch ($idTipoAnaliseProjeto) {
            
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na analise de 'atualiza��o banco de dados mysql do mysql'
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
                
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
//                echo EXTDAO_Tipo_analise_projeto::PROTOTIPO;
//                echo "::";
//                echo EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID;
//                exit();
                $idPVBBAndroid = $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID);
//                echo $idPVBBAndroid;
//                exit();
                return $idPVBBAndroid;
            default:
                break;
        }
        return $idPVBB;
    }
    //Temos varios projetos_versao para um dado banco
    //porem so um dos projetos analise que contem o banco de dados
    public function getPVDoBancoDeDadosDeHomologacao($idPV){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersao($idPV);
        
        switch ($idTipoAnaliseProjeto) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na 'analise de atualiza��o do mysql'
                //� o banco sincronizador_web de homologa��o
                return $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web);
            
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                return $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::PROTOTIPO);
            

            default:
                break;
        }

        return $idPV;
    }
    
    public function getPVBBDoBancoDeDadosDeHomologacaoRelacionadoAoProjetosVersao($idPV, $tipoBanco){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersao($idPV);
        switch ($idTipoAnaliseProjeto) {
            
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na 'analise de atualiza��o do mysql'
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
                
                
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
                //� o banco android de homologa��o
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
                
            default:
                break;
        }
        return $this->getProjetosVersaoBancoBanco($idTipoAnaliseProjeto, $tipoBanco);
    }
    public function getPVDoBancoDeDadosDeHomologacaoRelacionadoAoPVBB($idPVBB){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersaoBancoBanco($idPVBB);
        switch ($idTipoAnaliseProjeto) {
            
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na 'analise de atualiza��o do mysql'
                return $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::PROTOTIPO);
                
                
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
                //� o banco android de homologa��o
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                return $this->getProjetosVersaoDaAnaliseDeProjeto(EXTDAO_Tipo_analise_projeto::PROTOTIPO);
                
                
            default:
                break;
        }
        return $this->getProjetosVersaoDaAnaliseDeProjeto($idTipoAnaliseProjeto);
    }
    
    
    public function getPVBBDoBancoDeDadosDeHomologacao($idPVBB){
        //ATENCAO apenas algumas analises de projeto possuem os registros tabelas de fato
        $idTipoAnaliseProjeto = $this->getTipoAnaliseDoProjetosVersaoBancoBanco($idPVBB);
        switch ($idTipoAnaliseProjeto) {
            
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Sincronizador Web Hmg utilizado na 'analise de atualiza��o do mysql'
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
                
                
            case EXTDAO_Tipo_analise_projeto::SINCRONIZACAO:
                //� o banco android de homologa��o
                //HMG -> Banco Web Hmg do Prototipo Web
                //PRD -> Banco Android Web Hmg do Prototipo Web
                return $this->getProjetosVersaoBancoBanco(EXTDAO_Tipo_analise_projeto::PROTOTIPO, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
                
            default:
                break;
        }
        return $idPVBB;
    }
    
    
    
}
