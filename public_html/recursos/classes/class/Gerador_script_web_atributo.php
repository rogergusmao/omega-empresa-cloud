<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
class Gerador_script_web_atributo extends Gerador_script_atributo{
    
    public function __construct($db = null){
        parent::__construct($db);
        
    }
    
    public function constroiScriptCreate($pObjAtributoHomologacao){
        $objScript = new Container_atributo();
        $vScriptInsert = "";
        //LastName 
        if($pObjAtributoHomologacao->getNome() ){
            $vScriptInsert .= "{$pObjAtributoHomologacao->getNome()} ";
        }
        //varchar
        if(strlen($pObjAtributoHomologacao->getTipo_sql()) ){
            $vScriptInsert .= $pObjAtributoHomologacao->getTipo_sql();
            //float(255, 0)
            if(strlen($pObjAtributoHomologacao->getTamanho_INT()) && strlen($pObjAtributoHomologacao->getDecimal_INT()))
                $vScriptInsert .= "({$pObjAtributoHomologacao->getTamanho_INT()}, {$pObjAtributoHomologacao->getDecimal_INT()})";
            //varchar(255)
            else if(strlen($pObjAtributoHomologacao->getTamanho_INT()) ){
                if($pObjAtributoHomologacao->getTipo_sql() != "double"
                    || ($pObjAtributoHomologacao->getTipo_sql() == "double" 
                        && $pObjAtributoHomologacao->getTamanho_INT() > 0)
                ){
                    if($pObjAtributoHomologacao->tipoSQLReal())
                        $vScriptInsert .= "({$pObjAtributoHomologacao->getTamanho_INT()}, 0) ";
                    else
                        $vScriptInsert .= "({$pObjAtributoHomologacao->getTamanho_INT()}) ";
                }
            }
                    
        }
        //x varchar(255) NOT NULL
        if(strlen($pObjAtributoHomologacao->getNot_null_BOOLEAN()) 
                && $pObjAtributoHomologacao->getNot_null_BOOLEAN() == "1"){
            $vScriptInsert .= " NOT NULL ";
        }
        


        if(strlen($pObjAtributoHomologacao->getAuto_increment_BOOLEAN()) 
                && $pObjAtributoHomologacao->getAuto_increment_BOOLEAN() == "1"){
            $vScriptInsert .= " AUTO_INCREMENT ";
        } else{
        //x varchar(255) NOT NULL DEFAULT 'X'
            if(strlen($pObjAtributoHomologacao->getValor_default())){
                $vScriptInsert .= " DEFAULT '{$pObjAtributoHomologacao->getValor_default()}' ";
            } 
        }
        
        $objScript->mScriptInsert = $vScriptInsert;
        
        
        if($pObjAtributoHomologacao->getPrimary_key_BOOLEAN()  == "1"){
            $objScript->mIsPrimaryKey = true;
        }



        $vObjTabela = new EXTDAO_Tabela($this->db);
        $vObjTabela->select( $pObjAtributoHomologacao->getTabela_id_INT());
        if(strlen($pObjAtributoHomologacao->getFk_tabela_id_INT())){
            
            $objScript->mIdTabelaChaveHmg = EXTDAO_Tabela_chave::getIdDaChaveCriadaJuntoAFkDoAtributo(
                $vObjTabela->getId(), $pObjAtributoHomologacao->getId(), $this->db);
            
            if(!strlen($pObjAtributoHomologacao->getFk_nome())){

                $atributoHmg = $pObjAtributoHomologacao->getNome();
                Helper::imprimirMensagem("[asdfcaw]A chave fk relacionada ao atributo  ({$vObjTabela->getId()}) {$vObjTabela->getNome()}::({$pObjAtributoHomologacao->getId()}) $atributoHmg - nao foi encontrada",
                    MENSAGEM_ERRO);
                exit();
            }

//            $objScript->mNomeKey = Helper::getNomeAleatorio(substr($vObjTabela->getNome(), 0, 5))."_KEY";
            $objScript->mNomeKey =$pObjAtributoHomologacao->getFk_nome();
//            $objScript->mScriptKey = "KEY `{$objScript->mNomeKey}` (`{$pObjAtributoHomologacao->getNome()}`) ";
            
            $vObjTabelaFK = new EXTDAO_Tabela();
            $vObjTabelaFK->select($pObjAtributoHomologacao->getFk_tabela_id_INT());
            $vObjAtributoFK = new EXTDAO_Atributo();
            $vObjAtributoFK->select($pObjAtributoHomologacao->getFk_atributo_id_INT());
            $objScript->mIdAtributoFkHmg = $pObjAtributoHomologacao->getId();
            $objScript->mNomeFK = $objScript->mNomeKey;
            $vConsultaFK = "";
            $vConsultaFK .= "CONSTRAINT `{$objScript->mNomeFK}` FOREIGN KEY (`{$pObjAtributoHomologacao->getNome()}`)"; 
            $vConsultaFK .= " REFERENCES `{$vObjTabelaFK->getNome()}` (`{$vObjAtributoFK->getNome()}`)";
            $vConsultaFK .= " ON DELETE {$pObjAtributoHomologacao->getDelete_tipo_fk()} ON UPDATE {$pObjAtributoHomologacao->getUpdate_tipo_fk()}";
            
            $objScript->mScriptFK = $vConsultaFK;
            
            
        }
        
        
        if($pObjAtributoHomologacao->getUnique_BOOLEAN()){
            $objScript->mIsUnique = true;
        }
        
        return $objScript;
    }
    
    public function constroiScriptDelete( $pObjTabelaHomologacao, $pObjAtributoProducao){
        //caso n�o exista tabela de homologacao, signifca que a tabela de producao foi removida, 
        //logo todos os atributos ja foram removidos
        if($pObjTabelaHomologacao == null) return "";
        //tenta remover chaves existentes do atributo
        //=> tomar cuidade, ex.: retira a tabela pais, porem o estado relativo a esse havia sido removido, e nao ser� mais
        //tenta remover 
        
        
        
        $consulta = "\nALTER TABLE `{$this->prefixoTabela}{$pObjTabelaHomologacao->getNome()}`\n";
        $consulta .= "DROP COLUMN `{$pObjAtributoProducao->getNome()}`";
        $this->insereScriptComandoBanco($consulta, EXTDAO_Tipo_comando_banco::DROP_COLUMN);
        return new Script_deleta_atributo($consulta);
    }
    
    public function constroiScriptEdit($pObjAtributoHomologacao, $pObjAtributoProducao){
//        if($pObjAtributoProducao->getId() == "4374"){
//            $i = 0;
//            $i += 1;
//        }
        //TODO retirar inicializacao dos objetos de entrada
//        $pObjAtributoProducao = new EXTDAO_Atributo();
//        $pObjAtributoHomologacao = new EXTDAO_Atributo();
        $vTabela = new EXTDAO_Tabela();
        $vTabela->select($pObjAtributoHomologacao->getTabela_id_INT());
        $vObj = new Script_update_atributo();
        $vConsultaRenomear = "";
//        ALTER TABLE supplier
//        RENAME COLUMN supplier_name to sname;
        if($pObjAtributoProducao->getNome() != $pObjAtributoHomologacao->getNome()){
            $vConsultaAlterarTipo = "";
            
            
            $vConsultaAlterarTipo .= " {$pObjAtributoHomologacao->getTipo_sql()}";
                
            if(strlen($pObjAtributoHomologacao->getTamanho_INT()) && strlen($pObjAtributoHomologacao->getDecimal_INT()))
                $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}, {$pObjAtributoHomologacao->getDecimal_INT()}) ";
            else if(strlen($pObjAtributoHomologacao->getTamanho_INT())){
                if($pObjAtributoHomologacao->getTipo_sql() != "double"
                    || ($pObjAtributoHomologacao->getTipo_sql() == "double" 
                        && $pObjAtributoHomologacao->getTamanho_INT() > 0)
                ){
                    if($pObjAtributoHomologacao->tipoSQLReal())
                        $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}, 0) ";
                    else
                        $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}) ";
                }
            }
                
                
            if(strlen($pObjAtributoHomologacao->getNot_null_BOOLEAN()))
                $vConsultaAlterarTipo .= " NOT NULL ";
            
            
            if(strlen($pObjAtributoHomologacao->getValor_default()))
                $vConsultaAlterarTipo .= " DEFAULT '{$pObjAtributoHomologacao->getValor_default()}' ";
            
            $vConsultaRenomear = "";
            $vConsultaRenomear .= "\nALTER TABLE {$this->prefixoTabela}{$vTabela->getNome()}\n";
            $vConsultaRenomear .= "CHANGE COLUMN {$pObjAtributoProducao->getNome()} {$pObjAtributoHomologacao->getNome()} {$vConsultaAlterarTipo}";
            $vObj->alterarNome = $vConsultaRenomear;
            $this->insereScriptComandoBanco( $vConsultaRenomear, EXTDAO_Tipo_comando_banco::RENAME_ATTRIBUTE);

        }
        
//        ALTER TABLE supplier
//        MODIFY  supplier_name varchar2(50);
        $vConsultaAlterarTipo = "";
        
        if($pObjAtributoHomologacao->getTipo_sql() != $pObjAtributoProducao->getTipo_sql()
                || $pObjAtributoHomologacao->getNot_null_BOOLEAN() != $pObjAtributoProducao->getNot_null_BOOLEAN()
                || $pObjAtributoHomologacao->getValor_default() != $pObjAtributoProducao->getValor_default()
                || $pObjAtributoHomologacao->getTamanho_INT() != $pObjAtributoProducao->getTamanho_INT() 
                || $pObjAtributoHomologacao->getDecimal_INT() != $pObjAtributoProducao->getDecimal_INT()
                || $pObjAtributoHomologacao->getAuto_increment_BOOLEAN() != $pObjAtributoProducao->getAuto_increment_BOOLEAN()){
            $vConsultaAlterarTipo = "";
            
            $vConsultaAlterarTipo .= "\nALTER TABLE {$this->prefixoTabela}{$vTabela->getNome()} ";
            $vConsultaAlterarTipo .= " MODIFY COLUMN {$pObjAtributoProducao->getNome()} ";
//            echo "entrou: {$pObjAtributoHomologacao->getId()}{$pObjAtributoHomologacao->getValor_default()}";
//            exit();
            
            $vConsultaAlterarTipo .= " {$pObjAtributoHomologacao->getTipo_sql()}";

            if(strlen($pObjAtributoHomologacao->getTamanho_INT()) && strlen($pObjAtributoHomologacao->getDecimal_INT())){
                $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}, {$pObjAtributoHomologacao->getDecimal_INT()}) ";
            }
            else if(strlen($pObjAtributoHomologacao->getTamanho_INT() ) ){
                if($pObjAtributoHomologacao->getTipo_sql() != "double"
                    || ($pObjAtributoHomologacao->getTipo_sql() == "double" 
                        && $pObjAtributoHomologacao->getTamanho_INT() > 0)
                ){
                    if($pObjAtributoHomologacao->tipoSQLReal() ){
                        $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}, 0) ";
                    }
                    else{ 
                        $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}) ";
                    }
                }
            }

            if($pObjAtributoHomologacao->getNot_null_BOOLEAN() == "1")
                $vConsultaAlterarTipo .= " NOT NULL ";
            
//            ALTER TABLE `sexo`
//MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST
            if(strlen($pObjAtributoHomologacao->getAuto_increment_BOOLEAN()) 
                    && $pObjAtributoHomologacao->getAuto_increment_BOOLEAN() == "1"){
                $vConsultaAlterarTipo .= " AUTO_INCREMENT ";
            }
            else{
                if(strlen($pObjAtributoHomologacao->getValor_default()))
                    $vConsultaAlterarTipo .= " DEFAULT '{$pObjAtributoHomologacao->getValor_default()}' ";
                
            }
            
            $vObj->alterarTipo = $vConsultaAlterarTipo;
            $this->insereScriptComandoBanco($vConsultaAlterarTipo, EXTDAO_Tipo_comando_banco::MODIFY_COLUMN);
        }
        
        return $vObj;
    }
    
    protected function constroiScriptInsert( $pObjAtributoHomologacao){
        $vTabela = new EXTDAO_Tabela();
        $vTabela->select($pObjAtributoHomologacao->getTabela_id_INT());
        
        $vConsultaAlterarTipo ="";
        $vConsultaAlterarTipo .= "\nALTER TABLE {$this->prefixoTabela}{$vTabela->getNome()} ";
        $vConsultaAlterarTipo .= " ADD COLUMN {$pObjAtributoHomologacao->getNome()} ";
        if($pObjAtributoHomologacao->getTipo_sql() )
           $vConsultaAlterarTipo .= " {$pObjAtributoHomologacao->getTipo_sql()}";

       if(strlen($pObjAtributoHomologacao->getTamanho_INT()) && strlen($pObjAtributoHomologacao->getDecimal_INT()))
           $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}, {$pObjAtributoHomologacao->getDecimal_INT()}) ";
       else if(strlen($pObjAtributoHomologacao->getTamanho_INT()))
           $vConsultaAlterarTipo .= "({$pObjAtributoHomologacao->getTamanho_INT()}) ";

       if($pObjAtributoHomologacao->getNot_null_BOOLEAN() == "1")
           $vConsultaAlterarTipo .= " NOT NULL ";
       
       if(strlen($pObjAtributoHomologacao->getAuto_increment_BOOLEAN()) 
                    && $pObjAtributoHomologacao->getAuto_increment_BOOLEAN() == "1"){
        $vConsultaAlterarTipo .= " AUTO_INCREMENT ";
       }
       else{
        if(strlen($pObjAtributoHomologacao->getValor_default()))
            $vConsultaAlterarTipo .= " DEFAULT '{$pObjAtributoHomologacao->getValor_default()}' ";        
       }
        
       $this->insereScriptComandoBanco($vConsultaAlterarTipo, EXTDAO_Tipo_comando_banco::ADD_COLUMN);
        return new Script_insere_atributo($vConsultaAlterarTipo);
    }
    
        
}

?>
