<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
class Gerador_script_registros {
    public $objBB;
    public $objTabelaTabela;
    public $objTabelaHomologacao;
    public $objTabelaProducao;
    
    
    
    public function __construct(
        $pIdBancoBanco,
        $pObjTabelaTabela,
        $vIdScriptProjetosVersaoBancoBanco,
        $contador,
        $db = null){
        
        $this->objBB = new EXTDAO_Banco_banco($db);
        $this->objBB->select($pIdBancoBanco);
        $this->objTabelaTabela = $pObjTabelaTabela;
        $this->idPVBB = $this->objTabelaTabela->getProjetos_versao_banco_banco_id_INT();
        $this->objTabelaHomologacao = $this->objTabelaTabela->getObjTabelaHomologacao();
        $this->objTabelaProducao = $this->objTabelaTabela->getObjTabelaProducao();
        
        $this->vIdScriptProjetosVersaoBancoBanco = $vIdScriptProjetosVersaoBancoBanco;
        $this->objScriptTabelaTabela = $this->insereScriptTabelaTabela();
        $this->contador = $contador;
        
        
    }
    
    
    private function insereScriptComandoBanco($consulta, $tipoComandoBanco, $db = null){
        if(!strlen($consulta)){
            $i = 0;
            return;
        }
        $obj = new EXTDAO_Script_comando_banco($db);
        $obj->setScript_tabela_tabela_id_INT($this->objScriptTabelaTabela->getId());
        $obj->setConsulta($consulta);
        $vSeqINT = EXTDAO_Script_projetos_versao_banco_banco::getProximoSeqScriptComandoBanco($this->vIdScriptProjetosVersaoBancoBanco);
        $obj->setSeq_INT($vSeqINT);

        $obj->setAtributo_atributo_id_INT(null);
        $obj->setTipo_comando_banco_id_INT($tipoComandoBanco);
        $obj->setTabela_chave_tabela_chave_id_INT(null);
        $obj->formatarParaSQL();
        
        $obj->insert();
        $obj->selectUltimoRegistroInserido();
        return $obj;
    }
    private function insereScriptTabelaTabela(){
        $vObjPVBB = new EXTDAO_Script_tabela_tabela();
        
        $vObjPVBB->setScript_projetos_versao_banco_banco_id_INT($this->vIdScriptProjetosVersaoBancoBanco);
        $vObjPVBB->setTabela_tabela_id_INT($this->objTabelaTabela->getId());
        $vObjPVBB->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_ATUALIZACAO_REGISTRO_HOM_PARA_PROD);
        $vObjPVBB->formatarParaSQL();
        $vObjPVBB->insert();
        $vObjPVBB->selectUltimoRegistroInserido();
        return $vObjPVBB;
    }
    
    public function getScriptRegistros($db = null){
        if($db ==null)$db =new Database();
        $remover = $this->objTabelaTabela->getRemover_tuplas_na_homologacao_BOOLEAN();
        $editar = $this->objTabelaTabela->getEditar_tuplas_na_homologacao_BOOLEAN();
        $inserir = $this->objTabelaTabela->getInserir_tuplas_na_homologacao_BOOLEAN();
//        if($editar == "1")
            $this->getScriptRegistrosEditar($db);
        
//        if($inserir == "1")
            $this->getScriptRegistrosInserir($db);
        
//        if($remover == "1")
            $this->getScriptRegistrosRemover($db);
    }
        
    
    private function getScriptRegistrosEditar($db = null){
        try{
            if($this->objTabelaHomologacao == null || $this->objTabelaProducao == null){
                Helper::imprimirMensagem("Relacionamento entre as tabelas est� desatualizado.", MENSAGEM_ERRO);
                return;
            }else{
                $chavesDiferentes = EXTDAO_Tabela_tabela::chavePrimariaDiferente(
                    $this->idPVBB, $this->objTabelaTabela->getId(), $db);

                if($chavesDiferentes){
                    Helper::imprimirMensagem("Chave primaria da tabela de produ��o � diferente da tabela de homologa��o.\n\tPRODU��O:\t{$this->objTabelaProducao->getNome()}. HOMOLOGA��O:\t{$this->objTabelaHomologacao->getNome()} .", MENSAGEM_ERRO);
                    return;
                }else{
                    $vNomesAttrHom = EXTDAO_Tabela::getNomeAtributosDaChavePrimaria(
                        $this->objTabelaTabela->getId(), $this->objTabelaHomologacao->getId(), $db);
                    $vNomesAttrProd = EXTDAO_Tabela::getNomeAtributosDaChavePrimaria(
                        $this->objTabelaTabela->getId(), $this->objTabelaProducao->getId(), $db);

                    $scriptSelectGenericoProd = $this->concatenaString($vNomesAttrProd, ", ");
                    $scriptSelectGenericoHom = $this->concatenaString($vNomesAttrHom, ", ");

                    $scriptWhereGenericoHom = $this->concatenaConsultaGenericaComNome(
                        $vNomesAttrHom, " AND ", "?#");

                    $nomeAtributosSemSerDaChavePrimariaHom = EXTDAO_Tabela::getNomeAtributosSemSerDaChavePrimaria(
                        $this->objTabelaTabela->getId(), $this->objTabelaHomologacao->getId(), $db);

                    $nomeAtributosSemSerDaChavePrimariaProd = EXTDAO_Tabela::getNomeAtributosSemSerDaChavePrimaria(
                        $this->objTabelaTabela->getId(), $this->objTabelaProducao->getId(), $db);

                    $attrsPrdOrdenados = array();
                    $scriptSelectSemSerChavePrimariaGenericoProd= "";
                    foreach ($nomeAtributosSemSerDaChavePrimariaHom as $attrHmg){
                        $encontrou = false;
                        foreach ($nomeAtributosSemSerDaChavePrimariaProd as $attrPrd){
                            if($attrHmg == $attrPrd){
                                $attrsPrdOrdenados[count($attrsPrdOrdenados)] =$attrPrd;
                                $encontrou =true;
                                break;
                            }
                        }
                        if(!$encontrou ){
                            $attrsPrdOrdenados[count($attrsPrdOrdenados)] =null;
                        } else {
                            if(strlen($scriptSelectSemSerChavePrimariaGenericoProd)) $scriptSelectSemSerChavePrimariaGenericoProd .= ", ";
                            $scriptSelectSemSerChavePrimariaGenericoProd .= $attrHmg;
                        }
                    }

                    $scriptUpdateGenericoHom = $this->concatenaConsultaGenericaComNome($nomeAtributosSemSerDaChavePrimariaHom, ", ", "?$");
                    //$scriptSelectSemSerChavePrimariaGenericoProd = $this->concatenaString($nomeAtributosSemSerDaChavePrimariaProd, ", ");
                    $scriptSelectSemSerChavePrimariaGenericoHmg = $this->concatenaString($nomeAtributosSemSerDaChavePrimariaHom, ", ");

                    $vUpdateHomGenerica = "UPDATE {$this->objTabelaHomologacao->getNome()} 
                    SET {$scriptUpdateGenericoHom}
                    WHERE {$scriptWhereGenericoHom} ";

                    $selectGenericoHom = "SELECT {$scriptSelectGenericoHom}, $scriptSelectSemSerChavePrimariaGenericoHmg
                              FROM {$this->objTabelaHomologacao->getNome()}
                              WHERE $scriptWhereGenericoHom
                              LIMIT 0,1";
                    //seleciona todos os registros da tabela de producao
                    $dbProd = $this->objBB->getObjBancoProducao();
                    $dbProd->query("SELECT {$scriptSelectGenericoProd}, {$scriptSelectSemSerChavePrimariaGenericoProd}
                    FROM {$this->objTabelaProducao->getNome()}");
                    $dbHom = $this->objBB->getObjBancoHomologacao();
                    if (mysqli_num_rows($dbProd->result) > 0) {
                        mysqli_data_seek($dbProd->result, 0);
                    }

                    for ($i = 0; $registro = mysqli_fetch_array($dbProd->result); $i++) {
                        $vConsultaEspecificaSelectHom = $this->formataConsultaGenerica(
                            "?#"
                                , $selectGenericoHom
                                , $registro
                                , 0
                                , count($vNomesAttrHom)
                                ,0
                                , $db);
                        $dbHom->query($vConsultaEspecificaSelectHom);

                        $obj = Helper::getPrimeiroObjeto($dbHom->result);
                        $vId = ($obj != null ) ? $obj["id"] : null;
                        //se o registro nao existir, sera inserido pela rotina de montagem de consulta INSERT
                        if($vId==null) continue;

                        $realizarUpdate = false;
                        for ($j = 0 ; $j < count($nomeAtributosSemSerDaChavePrimariaHom ); $j++){
                            $attrHmg = $nomeAtributosSemSerDaChavePrimariaHom [$j];
                            if($attrsPrdOrdenados[$j] != null ){
                                if($registro[$attrHmg] != $obj[$attrHmg]){
                                    $realizarUpdate = true;
                                    break;
                                }
                            }
                        }

                        //$vId = Helper::getResultSetToPrimeiroResultado($dbHom->result);
                        //se o registro j� existir na base de homologa��o
                        //montamos o script de update
                        if($realizarUpdate){
                            $vConsultaEspecificaUpdateHom = $this->formataConsultaGenerica(
                                "?#",
                                $vUpdateHomGenerica,
                                $registro,
                                0,
                                count($vNomesAttrHom),
                                0 ,
                                $db
                            );
                            $vConsultaEspecificaUpdateHom = $this->formataConsultaGenerica(
                                "?$",
                                $vConsultaEspecificaUpdateHom,
                                $registro,
                                count($vNomesAttrHom),
                                count($nomeAtributosSemSerDaChavePrimariaHom) + count($vNomesAttrHom),
                                0,
                                $db
                            );

                            $this->insereScriptComandoBanco(
                                $vConsultaEspecificaUpdateHom,
                                EXTDAO_Tipo_comando_banco::UPDATE_REGISTER,
                                $db);
                        }


                    }
                }
            }
        }catch(Exception $ex){
            Helper::imprimirMensagem(Helper::getDescricaoException($ex));
        }

    }    
    
    private function formataConsultaGenerica(
        $prefixoGenerico,
        $vConsulta,
        $pVetorValor,
        $indiceInicialIncluso,
        $indiceFinalExcluso,
        $iParametro = 0,
        $db
    ){

        $pEnd='$';
        for($i = $indiceInicialIncluso; $i < $indiceFinalExcluso; $i++, $iParametro++){
            $vValor = $pVetorValor[$i];
           if(!strlen($vValor))
               $vValor = "null";
           else $vValor = "'".$db->realEscapeString($vValor)."'";

           $vConsulta = str_replace($prefixoGenerico.$iParametro.$pEnd, $vValor, $vConsulta);

       }
       return $vConsulta;
    }
    
    private function getScriptRegistrosInserir($db){
            try{
        if($this->objTabelaHomologacao == null || $this->objTabelaProducao == null){
                Helper::imprimirMensagem("Relacionamento entre as tabelas est� desatualizado.", MENSAGEM_ERRO);
                return;
        }else{
            $chavesDiferentes = EXTDAO_Tabela_tabela::chavePrimariaDiferente($this->idPVBB, $this->objTabelaTabela->getId());

            if($chavesDiferentes){
                Helper::imprimirMensagem("Chave primaria da tabela de produ��o � diferente da tabela de homologa��o.\n\tPRODU��O:\t{$this->objTabelaProducao->getNome()}. HOMOLOGA��O:\t{$this->objTabelaHomologacao->getNome()} .", MENSAGEM_ERRO);
                return;
            }else{

                $vVetorAtributoHom= EXTDAO_Tabela::getNomeAtributosOrdenados(
                    $this->idPVBB, $this->objTabelaTabela->getId(), $this->objTabelaHomologacao->getId(), $db);

                $scriptNomeInsertIntoHom = $this->concatenaString($vVetorAtributoHom, ", ");
                $scriptValuesHom = $this->concatenaConsultaGenericaSemNome(count($vVetorAtributoHom), ", ", "?#");
                $vInserirHomGenerica = "INSERT INTO {$this->objTabelaHomologacao->getNome()} 
                    ($scriptNomeInsertIntoHom) 
                    VALUE ({$scriptValuesHom})";
                    
                $vVetorAtributoChavePrimariaHom = EXTDAO_Tabela::getNomeAtributosDaChavePrimaria($this->objTabelaTabela->getId(), $this->objTabelaHomologacao->getId());
                $scriptValoresChavePrimariaHom = $this->concatenaConsultaGenericaComNome($vVetorAtributoChavePrimariaHom, ", ", "?#");
                $scriptSelectChavePrimariaHom = $this->concatenaString($vVetorAtributoChavePrimariaHom, ", ");
                
                $selectGenericoHom = "SELECT {$scriptSelectChavePrimariaHom}
                                  FROM {$this->objTabelaHomologacao->getNome()}
                                  WHERE $scriptValoresChavePrimariaHom
                                  LIMIT 0,1";
                    
                $vVetorAtributoProd= EXTDAO_Tabela::getNomeAtributosOrdenados(
                    $this->idPVBB, $this->objTabelaTabela->getId(), $this->objTabelaProducao->getId(), $db);
                $scriptSelectProd = $this->concatenaString($vVetorAtributoProd, ", ");
                //seleciona todos os registros da tabela de producao
                $dbProd = $this->objBB->getObjBancoProducao();
                $dbProd->query("SELECT {$scriptSelectProd}
                    FROM {$this->objTabelaProducao->getNome()}");
                $dbHom = $this->objBB->getObjBancoHomologacao();
                if (mysqli_num_rows($dbProd->result) > 0) {
                    mysqli_data_seek($dbProd->result, 0);
                }

                for ($i = 0; $registro = mysqli_fetch_array($dbProd->result); $i++) {

                    
                    $vConsultaEspecificaSelectHom = $this->formataConsultaGenerica(
                            "?#",
                            $selectGenericoHom, 
                            $registro, 
                            0, 
                            count($vVetorAtributoChavePrimariaHom),
                        0 ,
                        $db);
                    
                    $dbHom->query($vConsultaEspecificaSelectHom);
                    $vId = Helper::getResultSetToPrimeiroResultado($dbHom->result);
                    if(!strlen($vId)){
                        $vConsultaEspecificaInsertHom = $this->formataConsultaGenerica(
                            "?#"
                                , $vInserirHomGenerica
                                , $registro
                                , 0
                                , count($vVetorAtributoHom)
                            , 0
                                , $db);
                        //$dbHom->query($vConsultaEspecificaInsertHom);    
                        $this->insereScriptComandoBanco($vConsultaEspecificaInsertHom, EXTDAO_Tipo_comando_banco::INSERT_INTO_REGISTER);
                    }
                }
            }
        }
            }catch(Exception $ex){
                Helper::imprimirMensagem(Helper::getDescricaoException($ex));
            }

    }    
    
    private function getScriptRegistrosRemover($db = null){
        try{
        if($this->objTabelaHomologacao == null || $this->objTabelaProducao == null){
                Helper::imprimirMensagem("Relacionamento entre as tabelas n�o � existente, talvez esteja desatualizado.", MENSAGEM_ERRO);
                return;
        }else{
            $chavesDiferentes = EXTDAO_Tabela_tabela::chavePrimariaDiferente(
                $this->idPVBB, $this->objTabelaTabela->getId(), $db);

            if($chavesDiferentes){
                Helper::imprimirMensagem("Chave primaria da tabela de produ��o � diferente da tabela de homologa��o.\n\tPRODU��O:\t{$this->objTabelaProducao->getNome()}. HOMOLOGA��O:\t{$this->objTabelaHomologacao->getNome()} .", MENSAGEM_ERRO);
                return;
            }else{

                $vVetorAtributoChavePrimariaHom= EXTDAO_Tabela::getNomeAtributosDaChavePrimaria(
                    $this->objTabelaTabela->getId(), $this->objTabelaHomologacao->getId(), $db);
                $scriptValoresChavePrimariaHom = $this->concatenaConsultaGenericaComNome($vVetorAtributoChavePrimariaHom, ", ", "?#");
                $scriptSelectChavePrimariaHom = $this->concatenaString($vVetorAtributoChavePrimariaHom, ", ");
                
                
                $deleteGenericoHom = "DELETE FROM {$this->objTabelaHomologacao->getNome()} WHERE $scriptValoresChavePrimariaHom ";
                $selectGenericoHom = "SELECT {$scriptSelectChavePrimariaHom}
                                      FROM {$this->objTabelaHomologacao->getNome()}
                                      WHERE $scriptValoresChavePrimariaHom
                                      LIMIT 0,1";

                $vVetorAtributoChavePrimariaProd= EXTDAO_Tabela::getNomeAtributosDaChavePrimaria(
                    $this->objTabelaTabela->getId(), $this->objTabelaProducao->getId(), $db);
                $selectGenericoProd = $this->concatenaString($vVetorAtributoChavePrimariaProd, ", ");
                //seleciona todos os registros da tabela de producao
                $dbProd = $this->objBB->getObjBancoProducao();
                $dbProd->query("SELECT {$selectGenericoProd}
                    FROM {$this->objTabelaProducao->getNome()}");
                    
                $dbHom = $this->objBB->getObjBancoHomologacao();
                if (mysqli_num_rows($dbProd->result) > 0) {
                    mysqli_data_seek($dbProd->result, 0);
                }

                for ($i = 0; $registro = mysqli_fetch_array($dbProd->result); $i++) {
                    
                    $vConsultaEspecificaSelectHom = $this->formataConsultaGenerica(
                            "?#",
                            $selectGenericoHom, 
                            $registro, 
                            0, 
                            count($vVetorAtributoChavePrimariaHom),
                            0,
                            $db);
                    
                    $dbHom->query($vConsultaEspecificaSelectHom);
                    $vId = Helper::getResultSetToPrimeiroResultado($dbHom->result);
                    if(!strlen($vId)){
                        $vConsultaEspecificaDeleteHom = $this->formataConsultaGenerica(
                                "?#",
                                $deleteGenericoHom, 
                                $registro, 
                                0, 
                                count($vVetorAtributoChavePrimariaHom),
                            0 ,
                            $db);
                        
                        //$dbHom->query($vConsultaEspecificaDeleteHom);
                        $this->insereScriptComandoBanco(
                            $vConsultaEspecificaDeleteHom,
                                EXTDAO_Tipo_comando_banco::DELETE_REGISTER,
                                $db);
                    }
                }
            }
        }
        }catch(Exception $ex){
            Helper::imprimirMensagem(Helper::getDescricaoException($ex));
        }

    }    

    //concatenaString($vNomesAttrHom, "AND", "?#" )
    private static function concatenaConsultaGenericaComNome($pVetor, $pDelimitador, $pSufixo){
        $pEnd = '$';
        $token = "";
        for($i = 0; $i < count($pVetor); $i++) {
            if(strlen($token)){
                $token .= " {$pDelimitador} ";
            }
            $token .= $pVetor[$i]." = {$pSufixo}{$i}{$pEnd} ";
        }
        return $token;
    }
    
    //concatenaString(12, "AND", "?#" )
    private static function concatenaConsultaGenericaSemNome($pQuantidade, $pDelimitador, $pSufixo, $pEnd = '$'){
        $token = "";
        for($i = 0; $i < $pQuantidade; $i++) {
            if(strlen($token)){
                $token .= " {$pDelimitador} ";
            }
            $token .= "{$pSufixo}{$i}{$pEnd} ";
        }
        return $token;
    }
    
    //concatenaString($vNomesAttrHom, ",")
    private static function concatenaString($pVetor, $pDelimitador){
        $token = "";
        for($i = 0; $i < count($pVetor); $i++) {

            if(strlen($token))
                $token .= $pDelimitador;
            
            $token .= $pVetor[$i];
        }
        return $token;
    }

}

?>
