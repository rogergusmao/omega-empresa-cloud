<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Script_insert_atributo
 *
 * @author rogerfsg
 */
abstract class Interface_script {
    
    
    public function getQuery(){
        $vVetor = $this->getVetorAtributo();
        $vQuery = "";
        if(count($vVetor)){
            $vQuery .= $this->formataQuery($vVetor);
        } 
        return $vQuery;
    }
    
    protected abstract function getVetorAtributo();
    
    protected static function formataQuery($pVetor){
        $vRet = "";
        if(!empty($pVetor)){
            foreach ($pVetor as $value) {
                if(is_array($value)){
                    foreach ($value as $item) {
                        $vRet .= "{$item}\n";
                    }
                }
                else if(strlen($value)){
                    $vRet .= "{$value}\n";
                }
            }
        }
        return $vRet;
    }
}

?>
