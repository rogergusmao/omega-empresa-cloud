<?php
class Gerador_web_banco{
    public function __construct() {
    
    }
    public function gerarBancoWeb( $sobrescrever, $idPVBBSincronizador, $idPVBBPrototipoWeb, $objBanco = null){
    
    $objBanco = new Database();

    $arrExcessoesRelacionamento = array();
    

    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    
    $varGET= Param_Get::getIdentificadorProjetosVersao();
    
    
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBBSincronizador);
    $objDiretorio = new EXTDAO_Diretorio_web();
    // O $idPVBB atual é do sincronizador web
    if(!$objPVBB->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::SINCRONIZACAO){
        Helper::imprimirMensagem("Esse processo está mapeado apenas para projetos do tipo SINCRONIZACAO");
        exit();
    }
    
    $objPVBBPrototipoWeb = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBPrototipoWeb->select($idPVBBPrototipoWeb);
    if($idPVBBPrototipoWeb != $idPVBB){
        $objPVBB->select($idPVBBPrototipoWeb);    
    }
    
//    $retPVBBSincronizador = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoBancoSincronizadorWeb($objPVBBPrototipoWeb->getProjetos_versao_id_INT());
//    $idPVBBSincronizador = $retPVBBSincronizador[0]['id'];
    
        $idPVBBPrototipoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getIdPVBBSincronizadorWebHmgParaProd($objPVBB->getProjetos_versao_id_INT(), $objBanco);
        
    
    $idDiretorioSincronizador = EXTDAO_Diretorio_web::getIdDiretorio($idPVBBPrototipoSincronizadorWeb, $objBanco);
    if(!strlen($idDiretorioSincronizador)){
        Helper::imprimirMensagem("O diretório_web do sincronizador_Web não está mapeado para o projetos_versao_banco_banco = $idPVBBSincronizador", MENSAGEM_ERRO);
        exit();
    } else {
        $objDiretorioSincronizador = new EXTDAO_Diretorio_web();
        $objDiretorioSincronizador->select($idDiretorioSincronizador);
        $diretorioSincronizadorWebDAO = $objDiretorioSincronizador->getDAO();
        $diretorioSincronizadorWebEXTDAO = $objDiretorioSincronizador->getEXTDAO();
        $raizSincronizadorWeb = $objDiretorioSincronizador->getRaiz();
    }
    
    $idPV= $objPVBB->getProjetos_versao_id_INT();
    $objPV = new EXTDAO_Projetos_versao();
    $objPV->select($idPV);
    $idProjeto = $objPV->getProjetos_id_INT();
    $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBPrototipoWeb);
    $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBPrototipoWeb);
    $objBanco = new EXTDAO_Banco();
    $objBanco->select($idBanco);
    $nomeClasseBanco = "Database_ponto_eletronico";
    
    
    
    $idDiretorio = EXTDAO_Diretorio_web::getIdDiretorio($idPVBBPrototipoWeb);
    if(strlen($idDiretorio)){
        $objDiretorio->select($idDiretorio);
    
        $diretorioDAO = $objDiretorio->getDAO();
        $diretorioEXTDAO = $objDiretorio->getEXTDAO();
        $raiz = $objDiretorio->getRaiz();
    } else {
        Helper::imprimirMensagem("O diretório_web não está mapeado para o projetos_versao_banco_banco = $idPVBBPrototipoWeb", MENSAGEM_ERRO);
        exit();
    }
    
    
    //TODO retirar linhas abaixo - TESTE
//    $diretorioDAO = "recursos\classes\DAO";
//    $diretorioEXTDAO = "recursos\classes\EXTDAO";
//    
//    
//    $raiz = "C:\wamp\www\PontoEletronico\10001\public_html";
        
        
    $objTabela = new EXTDAO_Tabela();
    $vetorIdTabelaBanco = EXTDAO_Tabela::getHierarquiaTabelaBanco($idPV, $idBanco);
    
    $objTabelaBanco = new EXTDAO_Tabela();
    $strVetorTabelaBanco = "";
    
    for($k = 0 ; $k < count($vetorIdTabelaBanco); $k++){
        $idTabelaBanco = $vetorIdTabelaBanco[$k];
        
        $objTabelaBanco->select($idTabelaBanco);
        $classEXTDAO = $objTabelaBanco->getNome();
        if($objTabelaBanco->getNome() == "permissao"){
            $z = 0;
        }
        if(strlen($strVetorTabelaBanco)){
            $strVetorTabelaBanco .= ", \n";
        } 
        $strVetorTabelaBanco .= "\t \"".$classEXTDAO."\"";
        
    }
    $strVetorTabelaBanco = "public static \$tabelas = array ( \n$strVetorTabelaBanco);";
    $dir = dirname(__FILE__);
   
    $filenameSincronizador = Helper::getPathComBarra($raizSincronizadorWeb) .Helper::getPathComBarra($diretorioSincronizadorWebEXTDAO) . $nomeClasseBanco . ".php";
    
    
    $filename = Helper::getPathComBarra($raiz) .Helper::getPathComBarra($diretorioEXTDAO) . $nomeClasseBanco . ".php";
    
    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

    // open file in insert mode
    $file = fopen($filename, "w+");
    
    $filedate = date("d.m.Y");
    

    $c = "";

    $c = "<?php
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  $nomeClasseBanco
    * DATA DE GERAÇÃO: $filedate
    * ARQUIVO:         $nomeClasseBanco.php
    * BANCO DE DADOS:  $database->database
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    class $nomeClasseBanco {
	
";
   
   
    $strConstrutor = "";
    
    $strConstrutor.= "
    // *************************
    // CONSTRUTOR
    // *************************
    
    public function __construct(){
            

    }

    ";
    
    $strFabrica.= "
    // *************************
    // FACTORY
    // *************************
    public function factory(){
        return new $nomeClasseBanco();

    }
    ";
    
    $chaves = "";
    for($k = 0 ; $k < count($vetorIdTabelaBanco); $k++){
        $idTabela = $vetorIdTabelaBanco[$k];
        $objTabelaBanco->select($idTabela);
        $objTabelaBanco->getListaObjAtributoChaveExtrangeira();
        
        $nomeTabela = $objTabelaBanco->getNome();
        $idTabelaChave = EXTDAO_Tabela_chave::getIdDaChaveUnicaDaTabela($idTabela);
        $vetorAtributoChaveUnica =null;
        if(strlen($idTabelaChave))
            $vetorAtributoChaveUnica = EXTDAO_Tabela_chave_atributo::getListaIdOrdenadoAtributoDaChave($idTabelaChave);
        
        
        $strChaveUnica = "";
        if(count($vetorAtributoChaveUnica) ){

            //public void setUniqueKey(String pVetorAttributeName[])
            $objAtributo = new EXTDAO_Atributo();
            for($j = 0 ; $j < count($vetorAtributoChaveUnica); $j++){
                $objAtributo->select($vetorAtributoChaveUnica[$j]);
                $nomeAtributo = $objAtributo->getNome();

                if($j > 0)$strChaveUnica .= ", ";
                
                $strChaveUnica .= "'$nomeAtributo'";
            }
        }
        
        if(strlen($strChaveUnica)){
            $strChaveUnica = "\t\t\tDatabase_ponto_eletronico::\$chavesUnicas['$nomeTabela'] = array($strChaveUnica);";
        } else{
            $strChaveUnica = "\t\t\tDatabase_ponto_eletronico::\$chavesUnicas['$nomeTabela'] = null;";
        }
        if(strlen($chaves)) $chaves .= "\n";
        $chaves .= $strChaveUnica;
    }
    $strFunctionGetChaveUnica = "
        public static \$chavesUnicas = null;

        public static function getChaveUnica(\$tabela){
            if(Database_ponto_eletronico::\$chavesUnicas == null){
                Database_ponto_eletronico::\$chavesUnicas = array();
                $chaves
            }
            
            return Database_ponto_eletronico::\$chavesUnicas[\$tabela];
        }
        
        ";
    
    $strFunctionGetVetorTable = "
        
	public function getVetorTable() {
		// TODO Auto-generated method stub
		return Database_ponto_eletronico::\$tabelas;
	}";
    $c .= "
    // *************************
    // DECLARAÇÃO DE TODAS AS TABELAS DO BANCO DE DADOS
    // *************************
    ";
    $c.= $strVetorTabelaBanco;
    $c.= $strFunctionGetChaveUnica;
    
    $c.= $strConstrutor;
    $c.= $strFabrica;
    $c.= $strFunctionGetVetorTable;
    $c.= "

    } // classe: fim
    

    ?>";
    fwrite($file, $c);
    fclose($file);
    
    
    Helper::imprimirMensagem(
                "Arquivo do banco Database.php gerado com sucesso para o protótipo web $filename...\n",
                MENSAGEM_OK);
    if(file_exists($filenameSincronizador))
        unlink($filenameSincronizador);
    $filename = str_replace('/', '\\', $filename);
    $filenameSincronizador = str_replace('/', '\\', $filenameSincronizador);
    if (!copy($filename, $filenameSincronizador)) {
            Helper::imprimirMensagem(
                "Falha ao criar a cópia do banco Database.php no projeto do sincronizador web $filename TO $filenameSincronizador...\n",
                MENSAGEM_ERRO);
    } else {
        Helper::imprimirMensagem(
                "Arquivo do banco Database.php gerado com sucesso para o sincronizador web $filenameSincronizador...\n",
                MENSAGEM_OK);
    }
	
    

    }

}
}
?>
