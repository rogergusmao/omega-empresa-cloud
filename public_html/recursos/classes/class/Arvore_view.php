<?php

class Arvore_view {// Classe : início

    public $arvore;
    const ESTATICO = 100;
    const PROJETOS = 1;
    const PROJETOS_VERSAO = 2;
    const BANCOS = 3;
    
    
    const PROCESSO = 5;
    const ANDAMENTO = 6;
    const SINCRONISMO = 7;
    const ESTRUTURA = 8;
    const TABELA_TABELA_HOMOLOGACAO = 9;
    const TABELA_TABELA_PRODUCAO = 10;
    
    const CONEXOES = 11;
    const BANCO_BANCO_HOMOLOGACAO = 12;
    const BANCO_BANCO_PRODUCAO = 13;
    const BANCO = 14;
    
    const OPERACAO_SISTEMA = 16;
    const MOBILE_IDENTIFICADOR = 17;
    const MOBILE = 18;
    const MOBILE_IDENTIFICADOR_PROJETOS_VERSAO = 19;
    const SINCRONIZACAO_PROJETOS_VERSAO = 20;
    
    const ATUALIZA_SINCRONIZADOR_WEB_PROD = 27;
    const ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB =  28;
    const DESENVOLVIMENTO_APP =  29;
    const ANALISE_BANCOS = 21;
    
    const SISTEMA = 22;
    const SISTEMA_PRODUTO = 23;
    
    const SISTEMA_PRODUTO_WEB = 24;
    const SISTEMA_PRODUTO_MOBILE = 25;
    const SISTEMA_PRODUTO_MOBILE_VERSAO_ATUAL=26;
    public function __construct() {
        $idProjetos =null;
        $idProjetoVersaoAtual =null;
        if(strlen(Helper::GET(Param_Get::ID_PROJETOS))){
            $idProjetos = Helper::GET(Param_Get::ID_PROJETOS);
            $idProjetoVersaoAtual = EXTDAO_Projetos_versao::getIdVersaoProjeto($idProjetos);
        }
        $this->arvore = array(
        "estatico_versao_atual" => array(
            "configuracao_versao_atual" => null,
            "estatico_andamento_projetos_versao" => null,
            "estatico_sincronizacao_processo" => array(
                "s_estatico_andamento" => null,
                
                "s_estatico_bancos" => array( 
                    "s_bancos" => array(
                        "estatico_sincronizacao" =>null,
                        "sp_andamento" => null,
                        "sp_processo" => null,
                        "sp_banco_web" =>array(
                            "spbw_tabela_tabela_homologacao" =>null
                        ),
                        "sp_banco_mobile" =>array(
                            "spbm_tabela_tabela_homologacao" =>null
                        )
                    )
                )
            ),
            "estatico_banco_sincronizador_web_processo" => array(
                "s_estatico_andamento_1" => null,
                
                "s_estatico_bancos_1" => array( 
                    "s_bancos_atualizando_banco_sincronizador_web" => array(
                        
                        "sp_andamento" => null,
                        "sp_processo" => null,
                        "sp_banco_web" =>array(
                            "spbw_tabela_tabela_homologacao" =>null
                        ),
                        "sp_banco_mobile" =>array(
                            "spbm_tabela_tabela_homologacao" =>null
                        )
                    )
                )
            ),
            
            "estatico_atualiza_banco_sincronizador_web_prod" => array(
                "s_estatico_andamento_2" => null,
                
                "s_estatico_bancos_2" => array( 
                    "s_bancos_atualiza_sincronizador_web_prod" => array(
                        
                        "sp_andamento" => null,
                        "sp_processo" => null,
                        "sp_banco_web" =>array(
                            "spbw_tabela_tabela_homologacao" =>null
                        ),
                        "sp_banco_mobile" =>array(
                            "spbm_tabela_tabela_homologacao" =>null
                        )
                    )
                )
            ),
           
            "estatico_bancos" => array( 
                "bancos"=>array(
                    "estatico_processo" =>null,
                    "estatico_andamento" =>null,
                    "estatico_gerador_codigo_projetos_versao_banco_banco" => null,
                    "estatico_banco_producao" =>array(
                        "tabela_tabela_homologacao" =>null
                    ),
                    "estatico_banco_homologacao" =>array(
                        "tabela_tabela_producao" =>null
                    )
                )
            )
        ),
        "estatico_conexoes" =>array(
            "conexoes" => array(
                
                "estatico_associacao" => array(
                    "estatico_banco_banco_producao" => array(
                        "banco_banco_producao" => null
                    ),
                    "estatico_banco_banco_homologacao" => array(
                        "banco_banco_homologacao" => null
                    )
                ),
                
                "estatico_banco" => array(
                    "banco" => null
                )
            )
        ),
        "estatico_suporte" =>array(
            "estatico_sistema_produto_log_erro" => null,
            "estatico_operacao_sistema" => null,
            "estatico_mobile" => array(
                
                "mobile" => array(
                    "estatico_omega_debug" =>array(
                        "estatico_instalar_apk" => null,
                        "estatico_sqlite" => null,
                        "monitorar_tela" => null,
                    ),
                    "mobile_identificador" => array(
                        
                        "mobile_identificador_versao" => array(
                            "estatico_sincroniza" => null,
                            "estatico_crud_aleatorio" => null,
                            "estatico_sqlite" => null,
                            "estatico_simular_atualizacao_versao" => null,
                            "estatico_download_banco" => null,
                            "estatico_upload_banco" => null,
                            "estatico_monitorar_tela" => null,
                            "comparar_estrutura_banco_com_original" => null,
                            "ponte_banco_sql" => null,
                            "reset" => null,
                            "log_cat" => null,
                        ),
                    )
                )
            )
        ),  
            "estatico_analise_bancos" => array( 
                
                   "analise_bancos"=>array(
                       "estatico_processo" =>null,
                       "estatico_andamento" =>null,
                       "estatico_transferencia_registros" => null,
                       "estatico_banco_producao" =>array(
                           "tabela_tabela_homologacao" =>null
                       ),
                       "estatico_banco_homologacao" =>array(
                           "tabela_tabela_producao" =>null
                       )
                   )
               
            ),
            "sistema"=>array( 
                "sistemas" => array(
                    "s_mobile" => array(
                        "sp_mobile" => array(
                            "spm_versao_atual" => array(
                                "spmva_configuracoes" => null,
                                "spmva_atualizar_versao" => array(
                                    "spmvaav_andamento" => null,
                                    "spmvaav_processo" => null
                                ),
                                "spmva_proc_gerar_db_sqlite" => array(
                                    "spmva_andamento" => null,
                                    "spmva_processo" => null,
                                    "spmva_banco_web" =>array(
                                        "spmvabw_tabela_tabela_homologacao" => null
                                    ),
                                    "spmva_banco_mobile" =>array(
                                        "spmvabm_tabela_tabela_homologacao" => null
                                    )
                                ),
                                

                           ),
                           "spm_central_telefonica" => null,
                        )
                    ),
                    "s_atualizar_versao" => array(
                        "sav_andamento" => null,
                        "sav_processo" => null
                    )
                 )
            )
     
       );
        
        $this->vetorNodo = array(
            "spmvaav_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "spmvaav_processo",
                    "Processo",
                    null,
                    null,
                    "pages",
                    "menu_banco_banco",
                        null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png"
                ),
            "spmvaav_andamento" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "spmvaav_andamento", //id
                    "Andamento", //label
                    null,
                    null,    
                    "lists",
                    "gerencia_projetos_versao_processo_estrutura",
                        null,
                    "icone_andamento.gif",
                    "icone_andamento.gif",
                    "icone_andamento.gif"
                ),
           
            
            "spmva_atualizar_versao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spmva_atualizar_versao", //id
                    "Atualizar Versão", //label,
                    null,
                    null,
                    "forms",
                    "gerenciar_sistema_projetos_versao", //url
                    null,
                    "icone_atualizar_versao.gif",
                    "icone_atualizar_versao.gif",
                    "icone_atualizar_versao.gif"
                ),
            
            "sav_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "sav_processo",
                    "Processo",
                    null,
                    null,
                    "pages",
                    "menu_banco_banco",
                        null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png"
                ),
            "sav_andamento" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "sav_andamento", //id
                    "Andamento", //label
                    null,
                    null,    
                    "lists",
                    "gerencia_projetos_versao_processo_estrutura",
                        null,
                    "icone_andamento.gif",
                    "icone_andamento.gif",
                    "icone_andamento.gif"
                ),
            "s_atualizar_versao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_atualizar_versao", //id
                    "Ger. Atualização de Versão do Sist.", //label,
                    null,
                    null,
                    "pages",
                    "andamento_atualizacao_de_versao_do_sistema", //url
                    null,
                    "icone_versao_antiga.png",
                    "icone_versao_antiga.png",
                    "icone_versao_antiga.png"
                ),
            "spm_central_telefonica" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spm_central_telefonica", //id
                    "Central Telefonica", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "network.png",
                    "network.png",
                    "network.png"
                ),
             "spmva_banco_web" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spmva_banco_web", //id
                    "Banco Web", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "spmvabm_tabela_tabela_homologacao" => 
                new Nodo_view(
                    Arvore_view::TABELA_TABELA_HOMOLOGACAO,
                    false,
                    "spmvabm_tabela_tabela_homologacao", //id
                    "Tabelas", //label
                    Param_Get::ID1,
                    null,
                    "forms",
                    "compara_tabela_hom_tabela_prod",
                        null,
                    "icone_tabela.png",
                    "icone_tabela.png",
                    "icone_tabela.png"
                ),
            "spmvabw_tabela_tabela_homologacao" => 
                new Nodo_view(
                    Arvore_view::TABELA_TABELA_HOMOLOGACAO,
                    false,
                    "spmvabw_tabela_tabela_homologacao", //id
                    "Tabelas", //label
                    Param_Get::ID1,
                    null,
                    "forms",
                    "compara_tabela_hom_tabela_prod",
                        null,
                    "icone_tabela.png",
                    "icone_tabela.png",
                    "icone_tabela.png"
                ),
            "spmva_banco_mobile" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spmva_banco_mobile", //id
                    "Banco Mobile", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "spmva_banco_web" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spmva_banco_web", //id
                    "Banco Web", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "spmva_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "spmva_processo",
                    "Processo",
                    null,
                    null,
                    "pages",
                    "menu_banco_banco",
                        null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png"
                ),
            "spmva_andamento" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "spmva_andamento", //id
                    "Andamento", //label
                    null,
                    null,    
                    "lists",
                    "gerencia_projetos_versao_processo_estrutura",
                        null,
                    "icone_andamento.gif",
                    "icone_andamento.gif",
                    "icone_andamento.gif"
                ),
            "sistema" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "sistema", //id
                    "Sistemas do projeto", //label
                    Param_Get::ID_SISTEMA,
                    Param_Get::ID_PROJETOS."=".$idProjetos,
                    "forms",
                    "gerenciar_sistema", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png",
                    true
                ),
            "sistemas" => 
                new Nodo_view(
                    Arvore_view::SISTEMA,
                    true,
                    "sistemas", //id
                    "Sistemas", //label
                    Param_Get::ID_SISTEMA,
                    null,
                    "forms",
                    "gerenciar_sistema", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
				
        "s_mobile" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_mobile", //id
                    "Aplicativos Mobile do Sistema", //label
                    null,
                    null,
                    "lists",
                    "gerencia_sistema_produto_mobile", //url
                    null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),		
        "s_produto" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_produto", //id
                    "Produtos", //label
                    null,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
	"sp_web" => 
                new Nodo_view(
                    Arvore_view::SISTEMA_PRODUTO_WEB,
                    true,
                    "sp_web", //id
                    "Produto Web", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
					
        "sp_mobile" => 
                new Nodo_view(
                    Arvore_view::SISTEMA_PRODUTO_MOBILE,
                    true,
                    "sp_mobile", //id
                    "Produto Mobile", //label
                    Param_Get::ID_SISTEMA_PRODUTO,
                    null,
                    "forms",
                    "gerenciar_sistema_produto_mobile", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
				
        "spm_versao_atual" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spm_versao_atual", //id
                    "Versão Atual Utilizada No Produto", //label,
                    null,
                    null,
                    "forms",
                    "gerenciar_sistema_projetos_versao_sistema_produto_mobile", //url
                    null,
                    "icone_versao_antiga.png",
                    "icone_versao_antiga.png",
                    "icone_versao_antiga.png"
                ),
				
	"spmva_configuracoes" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "spmva_configuracoes", //id
                    "Configurações", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                     "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png"
                ),
				
	"spmva_proc_gerar_db_sqlite" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "spmva_proc_gerar_db_sqlite", //id
                    "Gerar Script SQLite", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            "estatico_transferencia_registros" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_transferencia_registros", //id
                    "Transferencia de Registros", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "lists" ,
                    "gerencia_atualizacao_registros",
                    null,
                    "icone_sinc.png",
                    "icone_sinc.png",
                    "icone_sinc.png",
                    true
                ),
            "estatico_analise_bancos" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_analise_bancos", //id
                    "Análise dos Bancos de Dados", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            "analise_bancos" => 
                new Nodo_view(
                    Arvore_view::ANALISE_BANCOS,
                    true,
                    "analise_bancos", //id
                    "analise_bancos", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            "s_bancos" => 
                new Nodo_view(
                    Arvore_view::SINCRONIZACAO_PROJETOS_VERSAO,
                    true,
                    "s_bancos", //id
                    "bancos", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            
            "s_bancos_atualizando_banco_sincronizador_web" => 
                new Nodo_view(
                    Arvore_view::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB,
                    true,
                    "s_bancos_atualizando_banco_sincronizador_web", //id
                    "bancos", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            "s_bancos_atualiza_sincronizador_web_prod" => 
                new Nodo_view(
                    Arvore_view::ATUALIZA_SINCRONIZADOR_WEB_PROD,
                    true,
                    "s_bancos_atualiza_sincronizador_web_prod", //id
                    "bancos", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
           
            "sp_banco_mobile" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "sp_banco_mobile", //id
                    "Banco Mobile", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            
             "sp_banco_web" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "sp_banco_web", //id
                    "Banco Web", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
              
            "sp_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "sp_processo",
                    "Processo",
                    null,
                    null,
                    "pages",
                    "menu_banco_banco",
                        null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png"
                ),     
        
            "sp_andamento" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "sp_andamento", //id
                    "Andamento", //label
                    null,
                    null,    
                    "lists",
                    "gerencia_projetos_versao_processo_estrutura",
                        null,
                    "icone_andamento.gif",
                    "icone_andamento.gif",
                    "icone_andamento.gif"
                ),
            "s_estatico_bancos_2" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_estatico_bancos_2", //id
                    "Banco Sincronizador Web Hmg com Prod", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
             "s_estatico_bancos_3" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_estatico_bancos_3", //id
                    "Bancos de homologação do app", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
             "s_estatico_bancos_1" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_estatico_bancos_1", //id
                    "Banco Web Hmg com Sincronizador Web Hmg", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
            "s_estatico_bancos" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "s_estatico_bancos", //id
                    "Protótipos Mobile", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
             "s_estatico_andamento_1" => 
	new Nodo_view(
		Arvore_view::ESTATICO,
		false,
		"s_estatico_andamento_1", //id
		"Andamento", //label
		Param_Get::ID_PROJETOS_VERSAO,
		Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
		"pages",
		"andamento_sincronizacao_projetos_versao",
		null,
		"icone_andamento.gif",
		"icone_andamento.gif",
		"icone_andamento.gif"
	),
             "s_estatico_andamento_2" => 
	new Nodo_view(
		Arvore_view::ESTATICO,
		false,
		"s_estatico_andamento_2", //id
		"Andamento", //label
		Param_Get::ID_PROJETOS_VERSAO,
		Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
		"pages",
		"andamento_sincronizacao_projetos_versao",
		null,
		"icone_andamento.gif",
		"icone_andamento.gif",
		"icone_andamento.gif"
	),
            "s_estatico_andamento_3" => 
	new Nodo_view(
		Arvore_view::ESTATICO,
		false,
		"s_estatico_andamento_3", //id
		"Andamento", //label
		Param_Get::ID_PROJETOS_VERSAO,
		Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
		"pages",
		"andamento_sincronizacao_projetos_versao",
		null,
		"icone_andamento.gif",
		"icone_andamento.gif",
		"icone_andamento.gif"
	),
             
    "s_estatico_andamento" => 
	new Nodo_view(
		Arvore_view::ESTATICO,
		false,
		"s_estatico_andamento", //id
		"Andamento", //label
		Param_Get::ID_PROJETOS_VERSAO,
		Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
		"pages",
		"andamento_sincronizacao_projetos_versao",
		null,
		"icone_andamento.gif",
		"icone_andamento.gif",
		"icone_andamento.gif"
	),
    "estatico_sincronizacao_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_sincronizacao_processo", //id
                    "Tabelas Sincronização Web/Mobile", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "forms" ,
                    "detalhe_projetos_versao",
                    null,
                    "icone_sinc.png",
                    "icone_sinc.png",
                    "icone_sinc.png",
                    false
                ),
            
            "estatico_banco_sincronizador_web_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_banco_sincronizador_web_processo", //id
                    "Prótotipo Sincronizador Web", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "forms" ,
                    "detalhe_projetos_versao",
                    null,
                    "icone_sinc.png",
                    "icone_sinc.png",
                    "icone_sinc.png",
                    false
                ),
            "estatico_atualiza_banco_sincronizador_web_prod" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_atualiza_banco_sincronizador_web_prod", //id
                    "Atualiza Banco de Dados Sincronizador Web Produção", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "forms" ,
                    "detalhe_projetos_versao",
                    null,
                    "icone_sinc.png",
                    "icone_sinc.png",
                    "icone_sinc.png",
                    false
                ),
                "estatico_desenvolvimento_app" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_desenvolvimento_app", //id
                    "Criando tabelas durante o desenvolvimento do app", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "forms" ,
                    "detalhe_projetos_versao",
                    null,
                    "icone_sinc.png",
                    "icone_sinc.png",
                    "icone_sinc.png",
                    false
                ),

            "log_cat" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "log_cat", //id
                    "Logcat", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_log3.png",
                    "icone_log3.png",
                    "icone_log3.png"
                ),
            "monitorar_tela" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "monitorar_tela", //id
                    "Monitorar Tela", //label
                    null,
                    null,
                    null,
                    null,
                    null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
            "estatico_omega_debug" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_omega_debug", //id
                    "Omega Debug", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "ponte.png",
                    "ponte.png",
                    "ponte.png"
                ),
            "reset" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "reset", //id
                    "Resetar Banco de Dados", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "reset.png",
                    "reset.png",
                    "reset.png"
                ),
            "ponte_banco_sql" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "ponte_banco_sql", //id
                    "Ponte do Banco Sqlite com SQL", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "ponte.png",
                    "ponte.png",
                    "ponte.png"
                ),
            "mobile_identificador_versao" => 
                new Nodo_view(
                    Arvore_view::MOBILE_IDENTIFICADOR_PROJETOS_VERSAO,
                    true,
                    "mobile_identificador_versao", //id
                    "Versão da APK", //label
                    Param_Get::ID_MOBILE_CONECTADO,
                    null,
                    "pages",
                    "gerencia_mobile_identificador",
                    null,
                    "icone_produto_aberto.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png",
                    false
                ),
            "comparar_estrutura_banco_com_original" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "comparar_estrutura_banco_com_original", //id
                    "Comparar Estrutura do Banco Com Orig.", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "nodo1.png",
                    "nodo1.png",
                    "nodo1.png"
                ),
            "estatico_monitorar_tela"=> 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_monitorar_tela", //id
                    "Monitorar Tela", //label
                    null,
                    null,
                    "forms",
                    "gerencia_monitorar_tela",
                    "Download do Banco de Dados",
                    "download.png",
                    "download.png",
                    "download.png",
                    true
                ),
            "estatico_download_banco" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_download_banco", //id
                    "Download do Banco de Dados", //label
                    null,
                    null,
                    "forms",
                    "gerencia_operacao_download_banco_mobile",
                    "Download do Banco de Dados",
                    "download.png",
                    "download.png",
                    "download.png",
                    true
                ),
            
            "estatico_crud_aleatorio" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_crud_aleatorio", //id
                    "Processo CRUD aleatório", //label
                    null,
                    null,
                    "forms",
                    "gerencia_operacao_crud_aleatorio",
                    "Download do Banco de Dados",
                    "download.png",
                    "download.png",
                    "download.png",
                    true
                ),
            "estatico_sincroniza" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_sincroniza", //id
                    "Sincroniza Dados do Telefone", //label
                    null,
                    null,
                    "forms",
                    "gerencia_operacao_sistema_mobile_filho&".Param_Get::ID_TIPO_OPERACAO_SISTEMA."=".EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_SINCRONIZA,
                    "Download do Banco de Dados",
                    "download.png",
                    "download.png",
                    "download.png",
                    true
                ),
            "estatico_upload_banco" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_upload_banco", //id
                    "Upload do Banco de Dados do telefone", //label
                    null,
                    null,
                    null,
                    null,
                    null,
                    "upload.png",
                    "upload.png",
                    "upload.png"
                ),
            "estatico_simular_atualizacao_versao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_simular_atualizacao_versao", //id
                    "Simular Atualização de Versão", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "download.png",
                    "download.png",
                    "download.png"
                ),
            "estatico_instalar_apk" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_instalar_apk", //id
                    "Instalar Versão do Produto", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "download.png",
                    "download.png",
                    "download.png"
                ),
            "estatico_sqlite" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_sqlite", //id
                    "Console Sqlite", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "console.png",
                    "console.png",
                    "console.png"
                ),
            "estatico_operacao_sistema" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_operacao_sistema", //id
                    "Operações de Sistema", //label
                    null,
                    null,
                    "lists",
                    "gerencia_operacao_sistema_mobile",
                    null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png",
                    true
                ),
            
           
            "estatico_sistema_produto_log_erro" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_sistema_produto_log_erro", //id
                    "Log Erro", //label
                    null,
                    null,
                    "pages",
                    "carregar_log_erro",
                    "Log",
                    "icone_log2.png",
                    "icone_log2.png",
                    "icone_log2.png",
                        true
                ),
            "mobile_identificador" => 
                new Nodo_view(
                    Arvore_view::MOBILE_IDENTIFICADOR,
                    true,
                    "mobile_identificador", //id
                    "APK do Telefone", //label
                    Param_Get::ID_MOBILE_IDENTIFICADOR,
                    null,
                    null,
                        null,
                        null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
            "mobile" => 
                new Nodo_view(
                    Arvore_view::MOBILE,
                    true,
                    "mobile", //id
                    "Telefone", //label
                    Param_Get::ID_MOBILE,
                    null,
                    null,
                    null,
                        null,
                    "icone_telefone.png",
                    "icone_telefone.png",
                    "icone_telefone.png"
                ),
//            "estatico_mobile" => 
//                new Nodo_view(
//                    Arvore_view::ESTATICO,
//                    true,
//                    "estatico_mobile", //id
//                    "Telefones", //label
//                    null,
//                    null,
//                    array("pages", "forms", "lists"),
//                    array("central_telefonica", "gerencia_mobile", "gerencia_mobile"),
//                    array("Informações", "Cadastrar Telefone", "Gerenciar Telefones"),
//                    "wifi.png",
//                    "wifi.png",
//                    "wifi.png",
//                    true
//                ),
            "estatico_mobile" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_mobile", //id
                    "Telefones", //label
                    null,
                    null,
                    "lists",
                    "gerencia_mobile",
                    "Telefones",
                    "wifi.png",
                    "wifi.png",
                    "wifi.png",
                        true
                ),
            "estatico_suporte" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_suporte", //id
                    "Central Telefonica", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "network.png",
                    "network.png",
                    "network.png"
                ),
            "banco" => 
                new Nodo_view(
                    Arvore_view::BANCO,
                    true,
                    "banco", //id
                    "Bancos", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_fechado.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "estatico_banco" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_banco", //id
                    "Bancos", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_fechado.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "estatico_associacao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_associacao", //id
                    "Relação", //label
                    null,
                    null,
                    null,
                        null,
                    null,
                    "nodo1.png",
                    "nodo1.png",
                    "nodo1.png"
                ),
            "banco_banco_producao" => 
                new Nodo_view(
                    Arvore_view::BANCO_BANCO_PRODUCAO,
                    true,
                    "banco_banco_producao", //id
                    "Bancos de Produção", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_fechado.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "banco_banco_homologacao" => 
                new Nodo_view(
                    Arvore_view::BANCO_BANCO_HOMOLOGACAO,
                    true,
                    "banco_banco_homologacao", //id
                    "Bancos de Homologação", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_fechado.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "estatico_banco_banco_homologacao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_banco_banco_homologacao", //id
                    "Bancos de Homologação", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_fechado.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "estatico_banco_banco_producao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_banco_banco_producao", //id
                    "Bancos de Produção", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_fechado.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "conexoes" => 
                new Nodo_view(
                    Arvore_view::CONEXOES,
                    true,
                    "conexoes", //id
                    "Conexões", //label
                    Param_Get::ID_CONEXOES,
                    null,
                    null,
                    null,
                        null,
                    "icone_conexao_fechado.png",
                    "icone_conexao_fechado.png",
                    "icone_conexao_aberto.png"
                ),
            "estatico_conexoes" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_conexoes", //id
                    "Conexões", //label
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_servidor.png",
                    "icone_servidor.png",
                    "icone_servidor.png"
                ),
            "estatico_sincronizacao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_sincronizacao",
                    "Tabelas Sincronização",
                    null,
                    null,
                    "lists",
                    "gerencia_sincronizacao_tabela",
                    null,
                    "icone_tabela.png",
                    "icone_tabela.png",
                    "icone_tabela.png",
                        true
                ),
            "estatico_andamento_projetos_versao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_andamento_projetos_versao", //id
                    "Andamento", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "pages",
                    "andamento_projetos_versao",
                    null,
                    "icone_andamento.gif",
                    "icone_andamento.gif",
                    "icone_andamento.gif"
                ),
            "configuracao_versao_atual" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "configuracao_versao_atual", //id
                    "Configurações", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "forms" ,
                    "gerencia_projetos_versao",
                    null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png",
                    true
                ),
            "estatico_versao_atual" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_versao_atual", //id
                    "Projeto", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    Param_Get::ID_PROJETOS_VERSAO."=".$idProjetoVersaoAtual,
                    "forms" ,
                    "detalhe_projetos_versao",
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png",
                    false
                ),
            "estatico_projetos_versao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_projetos_versao", //id
                    "projetos_versao", //label
                    null,
                    Param_Get::ID_PROJETOS."=".$idProjetos,
                    "page" ,
                    "seleciona_projetos_versao",
                        null
                ),
            
            "tabela_tabela_homologacao" => 
                new Nodo_view(
                    Arvore_view::TABELA_TABELA_HOMOLOGACAO,
                    false,
                    "tabela_tabela_homologacao", //id
                    "Tabelas", //label
                    Param_Get::ID1,
                    null,
                    "forms",
                    "compara_tabela_hom_tabela_prod",
                        null,
                    "icone_tabela.png",
                    "icone_tabela.png",
                    "icone_tabela.png"
                ),
            "tabela_tabela_producao" => 
                new Nodo_view(
                    Arvore_view::TABELA_TABELA_PRODUCAO,
                    false,
                    "tabela_tabela_producao", //id
                    "Tabelas", //label
                    Param_Get::ID1,
                    null,
                    "forms",
                    "compara_tabela_hom_tabela_prod",
                        null,
                    "icone_tabela.png",
                    "icone_tabela.png",
                    "icone_tabela.png"
                ),
            "estatico_banco_homologacao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_banco_homologacao", //id
                    "Banco de Homologação", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            "estatico_banco_producao" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_banco_producao", //id
                    "Banco de Produção", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    null,
                    null,
                        null,
                    "icone_banco_aberto.png",
                    "icone_banco_aberto.png",
                    "icone_banco_fechado.png"
                ),
            
            "estatico_andamento" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_andamento", //id
                    "Andamento", //label
                    null,
                    null,    
                    "lists",
                    "gerencia_projetos_versao_processo_estrutura",
                        null,
                    "icone_andamento.gif",
                    "icone_andamento.gif",
                    "icone_andamento.gif"
                ),
             "estatico_gerador_codigo_projetos_versao_banco_banco" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_gerador_codigo_projetos_versao_banco_banco", //id
                    "Gerador de Código", //label
                    null,
                    null,
                    "pages",
                    "gerador_codigo_projetos_versao_banco_banco",
                    "Gerador de Código",
                    "icone_log2.png",
                    "icone_log2.png",
                    "icone_log2.png",
                        true
                ),
            "estatico_processo" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    false,
                    "estatico_processo",
                    "Processo",
                    null,
                    null,
                    "pages",
                    "menu_banco_banco",
                        null,
                    "icone_processo.png",
                    "icone_processo.png",
                    "icone_processo.png"
                ),          
            "projetos" => 
                new Nodo_view(
                    Arvore_view::PROJETOS,
                    true,
                    "projetos", //id
                    "projetos", //label
                    Param_Get::ID_PROJETOS
                    
                ),
            
            "projetos_versao" => 
                new Nodo_view(
                    Arvore_view::PROJETOS_VERSAO,
                    true,
                    "projetos_versao", //id
                    "projetos_versao", //label
                    Param_Get::ID_PROJETOS_VERSAO,
                    null,
                    "page" ,
                    "seleciona_projetos_versao",
                        null
                ),
            "estatico_bancos" => 
                new Nodo_view(
                    Arvore_view::ESTATICO,
                    true,
                    "estatico_bancos", //id
                    "Protótipos", //label,
                    null,
                    null,
                    null,
                    null,
                        null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            "bancos" => 
                new Nodo_view(
                    Arvore_view::BANCOS,
                    true,
                    "bancos", //id
                    "bancos", //label
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    null,
                    "lists",
                    "gerencia_tabela_tabela", //url
                    null,
                    "icone_produto_fechado.png",
                    "icone_produto_aberto.png",
                    "icone_produto_fechado.png"
                ),
            );
    }
    
    
    
    public static function persisteNovaArvore(){
        $idArvore = Helper::getNomeAleatorio("arvore_");
        $i = 0 ;
        while(isset($_SESSION[$idArvore])){
            $idArvore = $this->geraIdSessionArvore();    
            $i += 1;
            if($i > 10)
                throw new Exception("Impossivel gerar o identificador da arvore");
            
        }
        $_SESSION[$idArvore] = array();
        return $idArvore;
    }
    
    public static function persisteXML($idArvore, $idNodo, $xml){
        $vetor  = $_SESSION[$idArvore];
        if(is_array($vetor)){
            $vetor[$idNodo] = $xml;    
        }
    }
    
    public function onSelecaoItem($id, $object){
        
    }
    public static function factory(){
        return new Arvore_view();
    }
    public function onXLE($id){
        $i = 0;
        $i += 1;
        
        $k = 0;
        return true;
    }
    
    public function onClick($id){
        $objIdNodo = Nodo_view::formataGETIdNodo($id);
        $idNodo = $objIdNodo[0];
        $varGET = $objIdNodo[1];
        $objNodo = $this->getNodo($idNodo);
        
//        $urlCompleta = $objNodo->getUrl()."&".$varGET;
        //header("location: $url&$varGET");
        $vetor = $objNodo->getVetorObjPagina();
        if(count($vetor)){
            $ret = new stdClass();
            $ret->objPaginas = $vetor;
            $ret->get = $varGET;
            
            $retJsonEnc = json_encode($ret);
            return $retJsonEnc;
        } else return "";
        
    }
    
    public function getListaNodoRaiz(){
        $ret = array();
        if($this->arvore!=null)
        foreach ($this->arvore as $chave => $valor) {
            
             $no = $this->vetorNodo[$chave];
             $vetorFilhos = $no->getVetorNodo("");
             if($vetorFilhos!=null)
             foreach ($vetorFilhos as $noFilho  ) {
                $ret[count($ret)] = $noFilho ;
             }
             
        }
        return $ret;
    }
    
    public function getFilhos($id, $varGET){
        $vetor = array();
        $ret = $this->buscaRecursiva($id, $this->arvore);
        if(is_array($ret)){
            foreach ($ret as $nomeTag => $value) {
             
                $no = $this->getNodo($nomeTag);
                if($no == null)
                    throw new Exception("Nodo não encontrado na arvore: ".$nomeTag);
                $vetorNodoInterno = $no->getVetorNodo($varGET);
                if($vetorNodoInterno!=null)
                foreach ($vetorNodoInterno as $nodoInterno) {
                    $vetor[count($vetor)] = $nodoInterno;    
                }
                
            }
        }
        return $vetor;
    }
    
    public function getNodo($id){
        return $this->vetorNodo[$id];
    }
    
    
    //retorna o valor
    public function buscaRecursiva($chaveProcurada, $vetor){
        if($vetor!=null)
        foreach ($vetor as $chave => $valor) {
            if($chave == $chaveProcurada){
               return $valor;
            }
           if(is_array($valor)) {
               $valorBusca = $this->buscaRecursiva($chaveProcurada, $valor);
               if($valorBusca != null)
                   return $valorBusca;
               
           }
        }
        return null;
    }

}

// Class : fim
?>
