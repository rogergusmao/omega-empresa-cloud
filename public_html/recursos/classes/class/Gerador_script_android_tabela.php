<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
class Gerador_script_android_tabela extends Gerador_script_tabela {

    public function __construct($idSPVBB, $pObjTabelaTabela) {
        parent::__construct($idSPVBB, $pObjTabelaTabela, new Gerador_script_android_atributo());
    }

    private function getScriptDelete($nome) {
        $vConsulta = "\nDROP TABLE {$nome};\n";
        $this->insereScriptComandoBanco($vConsulta, EXTDAO_Tipo_comando_banco::DROP_TABLE);
        return new Script_token( $vConsulta);
    }

    private function getScriptCreate($pObjTabelaHomologacao,$nomeTabela = null) {
        if ($pObjTabelaHomologacao == null) {

            Helper::imprimirMensagem("Tabela de homologa��o nao encontrada para gerar o script UPDATE.");

            return;
        }


        $vCabecalhoAtributos = "";
        $vChaves = "";
        $vChaveUnica = "";
        $vChavesExtrangeiras = "";
        $vChavePrimaria = "";
        $vetor = array();
//        $vObjAtributoAtributo = new EXTDAO_Atributo_atributo();
//        $vIdTabelaHomologacao = $pObjTabelaHomologacao->getId();
//        
        $scriptsChave = array();
        $vVetorIdAtributoHomologacao = EXTDAO_Tabela::getVetorIdAtributo($pObjTabelaHomologacao->getId());
        if($vVetorIdAtributoHomologacao != null)
        foreach ($vVetorIdAtributoHomologacao as $vIdAtributo) {

            $vObjAtributo = new EXTDAO_Atributo();
            $msg = $vObjAtributo->select($vIdAtributo);
            if( $msg != null && $msg->erro())
                throw new Exception (print_r($msg, true));
            $vGerador = new Gerador_script_android_atributo();
            $objScript = $vGerador->constroiScriptCreate($vObjAtributo);
            $scriptsChave[count($scriptsChave)] = $objScript;    
            if (strlen($objScript->mScriptKey))
                $vChaves .= (strlen($vChaves)) ? ",\n\t{$objScript->mScriptKey}" : "{$objScript->mScriptKey}";

            if (strlen($objScript->mScriptFK))
                $vChavesExtrangeiras .= (strlen($vChavesExtrangeiras)) ? ",\n\t{$objScript->mScriptFK}" : "\t{$objScript->mScriptFK}";

            if (strlen($objScript->mScriptInsert))
                $vCabecalhoAtributos .= (strlen($vCabecalhoAtributos)) ? ",\n\t{$objScript->mScriptInsert}" : "\t{$objScript->mScriptInsert}";

            if (strlen($objScript->mIsUnique))
                $vChaveUnica .= (strlen($vChaveUnica)) ? ", `{$vObjAtributo->getNome()}`" : "\t`{$vObjAtributo->getNome()}`";

            if (strlen($objScript->mIsPrimaryKey))
                $vChavePrimaria .= (strlen($vChavePrimaria)) ? ", {$vObjAtributo->getNome()}" : "{$vObjAtributo->getNome()}";
        }
        if($nomeTabela == null){
            $nomeTabela = $pObjTabelaHomologacao->getNome();
        } 
        $vConsulta = "\nCREATE TABLE {$nomeTabela} (\n$vCabecalhoAtributos";
        $nomeChave = "pk_".Helper::getNomeAleatorio($pObjTabelaHomologacao->getNome());
        
//        $nomeChave = $pObjTabelaHomologacao->get
        if (strlen($vChavePrimaria))
            $vConsulta .= ",\n\tCONSTRAINT ".$nomeChave." PRIMARY KEY ($vChavePrimaria)";


        if (strlen($vChavesExtrangeiras))
            $vConsulta .= ",\n{$vChavesExtrangeiras}";
        

        $vConsulta .= "\n);\n";

        $objSCB = $this->insereScriptComandoBanco($vConsulta, EXTDAO_Tipo_comando_banco::CREATE_TABLE);
        $vetor[count($vetor)] = $vConsulta;
        $idSCB = $objSCB->getId();
        $objSCBC = new EXTDAO_Script_comando_banco_cache();
        
        for($i = 0 ; $i < count($scriptsChave); $i++){
            $script = $scriptsChave[$i];
            if(strlen($script->mNomeKey) && strlen($script->mIdTabelaChaveHmg)){
                $objSCBC->setId(null);
                $objSCBC->setTabela_chave_id_INT($script->mIdTabelaChaveHmg);
                $objSCBC->setAtributo_id_INT(null);
                $objSCBC->setNome_chave($script->mNomeKey);
                $objSCBC->setScript_comando_banco_id_INT($idSCB);
                $objSCBC->formatarParaSQL();
                $objSCBC->insert();
            }
            
            if(strlen($script->mNomeFK) && strlen($script->mIdAtributoFkHmg)){
                $objSCBC->setId(null);
                $objSCBC->setAtributo_id_INT($script->mIdAtributoFkHmg);
                $objSCBC->setTabela_chave_id_INT(null);
                $objSCBC->setNome_chave($script->mNomeFK);
                $objSCBC->setScript_comando_banco_id_INT($idSCB);
                $objSCBC->formatarParaSQL();
                $objSCBC->insert();
            }
        }
        $idsChaveUnica = EXTDAO_Tabela_chave::getIdsChaveUnicaDaTabela($pObjTabelaHomologacao->getId());
        if(count($idsChaveUnica) > 1){
            $strIdsChaves= print_r($idsChaveUnica, true);
            Helper::imprimirMensagem(
                "O banco sqlite s� suporta uma chave �nica por tabela: "
                . "({$pObjTabelaHomologacao->getId()}) {$pObjTabelaHomologacao->getNome()}. Chaves: ($strIdsChaves)",
                MENSAGEM_ERRO);
            exit();
        } else if(count($idsChaveUnica) == 1){
            $consultaChaveUnica = $this->criaChave(
                $objSCBC, $idsChaveUnica[0], $pObjTabelaHomologacao);
            $vetor[count($vetor)] = $consultaChaveUnica;
        }
        
        $idsChaveNaoUnica = EXTDAO_Tabela_chave::getIdsDasChavesNaoUnica(
            $pObjTabelaHomologacao->getId(), $this->db);
        
        if(count($idsChaveNaoUnica)){
//            print_r($idsChaveNaoUnica);
//            exit();
            for($i = 0 ; $i < count($idsChaveNaoUnica); $i++){
                $idChaveNaoUnica = $idsChaveNaoUnica[$i];
                $consultaChaveNaoUnica = $this->criaChave(
                    $objSCBC, 
                    $idChaveNaoUnica, 
                    $pObjTabelaHomologacao,
                    $nomeTabela);
                if(strlen($consultaChaveNaoUnica)){
                    $vetor[count($vetor)] = $consultaChaveNaoUnica;
                }
            }
        }
        return new Script_vetor($vetor);
    }
    
    
    private function criaChave(
        $objSCBC, 
        $vIdChaveHom, 
        $pObjTabelaHomologacao,
        $nomeTabela = null){
        
        $vObjChave = new EXTDAO_Tabela_chave();
        $vObjChave->select($vIdChaveHom);
        $vNomeChave = $vObjChave->getNome();
        $chaveUnica = $vObjChave->getNon_unique_BOOLEAN() == "0" ? true : false;

        $vAtributos = EXTDAO_Tabela_chave_atributo::getListaNomeOrdenadoAtributoDaChave($vIdChaveHom);
        $vStrListaAtributo = "";
        if($vAtributos != null)
        foreach ($vAtributos as $vNomeAtributo) {
            $vStrListaAtributo .= strlen($vStrListaAtributo) ? ", {$vNomeAtributo}" : $vNomeAtributo;
        }
        $vConsultaInsereChaveUnicaDaHom = "";

        if($nomeTabela == null)
            $nomeTabela = $this->prefixoTabela.$pObjTabelaHomologacao->getNome();
        if ($chaveUnica) {
//          ALTER TABLE `operadora`
//          ADD INDEX `aonsf` (`nome`) USING BTREE
//            $vNomeChave = Helper::getNomeAleatorio("unique_" . substr($pObjTabelaHomologacao->getNome(), 0, 4));
            $vConsultaInsereChaveUnicaDaHom = "\n\tCREATE UNIQUE INDEX ".$vNomeChave
                ." ON ".$nomeTabela." ({$vStrListaAtributo}) ";
            $objSCB = $this->insereScriptComandoBanco(
                $vConsultaInsereChaveUnicaDaHom, 
                EXTDAO_Tipo_comando_banco::CREATE_UNIQUE_KEY, 
                $vIdChaveHom,
                $vNomeChave);
        } else {
//            $vNomeChave = Helper::getNomeAleatorio("key_" . substr($pObjTabelaHomologacao->getNome(), 0, 4));
            $vConsultaInsereChaveUnicaDaHom = "\n\tCREATE INDEX ".$vNomeChave
                ." ON ".$nomeTabela." ({$vStrListaAtributo}) ";
            $objSCB = $this->insereScriptComandoBanco(
                $vConsultaInsereChaveUnicaDaHom, 
                EXTDAO_Tipo_comando_banco::CREATE_KEY, 
                $vIdChaveHom,
                $vNomeChave);
        }

        

        //TODO comentar linha abaixo
        //$objSCB = new EXTDAO_Script_comando_banco();
        $idSCB = $objSCB->getId();
        $objSCBC->setId(null);
        $objSCBC->setScript_comando_banco_id_INT($idSCB);
        $objSCBC->setTabela_chave_id_INT($vIdChaveHom);
        $objSCBC->setAtributo_id_INT(null);
        $objSCBC->setNome_chave($vNomeChave);
        $objSCBC->formatarParaSQL();
        $objSCBC->insert();
        
        return $vConsultaInsereChaveUnicaDaHom;
    }

    public function getScriptCopiaRegistros($nomeTabelaTempProd, $pObjTabelaHomologacao, $pObjTabelaProducao){
        $vetorAAProd = EXTDAO_Atributo_atributo::getListIdAtributoAtributoDaTabelaDeProducao(
            $this->idPVBB, $pObjTabelaProducao->getId(), $this->db);
        $selectProd = "";
        $selectHmg = "";
        $objAA = new EXTDAO_Atributo_atributo();
        if($vetorAAProd != null)
        foreach ($vetorAAProd as $idAA) {
            
            $objAA->select($idAA);
            
            if(strlen($objAA->getHomologacao_atributo_id_INT())){
                $this->tempAtributo->select($objAA->getHomologacao_atributo_id_INT());
                if(strlen($selectHmg)){
                    $selectHmg .= ", ".$this->tempAtributo->getNome();
                } else {
                    $selectHmg .= $this->tempAtributo->getNome();
                }    
            }
            
            if(strlen($objAA->getProducao_atributo_id_INT())){
                $this->tempAtributo->select($objAA->getProducao_atributo_id_INT());
                if(strlen($selectProd)){
                    $selectProd .= ", ".$this->tempAtributo->getNome();
                }
                else{
                    $selectProd .= $this->tempAtributo->getNome();
                }    
            }
        }
        
        
        $consulta = "INSERT INTO ".$nomeTabelaTempProd." ($selectHmg) SELECT $selectProd FROM ".$pObjTabelaProducao->getNome();
        
        $this->insereScriptComandoBanco($consulta, EXTDAO_Tipo_comando_banco::INSERT_INTO_REGISTER);
        return new Script_token($consulta);
    }
    
    private function getScriptRenomeaTabela($pObjTabelaHomologacao, $pObjTabelaProducao, $nomeTabelaProducao = null) {
        if ($pObjTabelaHomologacao == null || $pObjTabelaProducao == null) {
            if ($pObjTabelaHomologacao == null)
                Helper::imprimirMensagem("Tabela de homologa��o nao encontrada para gerar o script UPDATE.",
                    MENSAGEM_ERRO);
            if ($pObjTabelaProducao == null)
                Helper::imprimirMensagem("Tabela de produ��o nao encontrada para gerar o script UPDATE.",
                    MENSAGEM_ERRO);
            exit();
        }
        $vObjRetorno = new Script_update_tabela();
        if($nomeTabelaProducao == null )
            $nomeTabelaProducao = $pObjTabelaProducao->getNome();
        if ($nomeTabelaProducao != $pObjTabelaHomologacao->getNome()) {
            $vObjRetorno->mScriptAlteraNome .= "\nALTER TABLE " 
                . $nomeTabelaProducao 
                . " RENAME TO " . $pObjTabelaHomologacao->getNome() ;
            
            $this->insereScriptComandoBanco($vObjRetorno->mScriptAlteraNome, EXTDAO_Tipo_comando_banco::RENAME_TABLE);
        }

        return $vObjRetorno;
    }

    protected function constroiScriptCreate($pObjTabelaHomologacao) {
        //constroiScriptInsert
        $vObj = $this->getScriptCreate($pObjTabelaHomologacao);
        
        if ($vObj != null)
            $consulta .= $vObj->getQuery();
        return $consulta;
    }

    protected function constroiScriptDelete($pObjTabelaProducao) {
        
        $obj = $this->getScriptDelete($pObjTabelaProducao->getNome());
        if($obj != null)
            return $obj->getQuery();
        else return null;
    }

    protected function constroiScriptEdit($pObjTabelaHomologacao, $pObjTabelaProducao) {
        $consulta  = "";
        
        if(EXTDAO_Tabela_tabela::existeChaveExtrangeiraASerDeletada($pObjTabelaHomologacao, $pObjTabelaProducao, $this->idPVBB, $this->db)
            
            //http://stackoverflow.com/questions/1884787/how-do-i-drop-a-constraint-from-a-sqlite-3-6-21-table
            //SQLite does not support the alter table drop constraint command. You will need to create a new table without a constraint, transfer the data, then delete the old table.
                || EXTDAO_Tabela_chave_tabela_chave::existeTabelaChaveRemovida($this->idPVBB, $pObjTabelaProducao)
//http://stackoverflow.com/questions/4007014/alter-column-in-sqlite            
//            There's no ALTER COLUMN in sqlite.
//
//I believe your only option is to:
//
//Rename the table to a temporary name
//Create a new table without the NOT NULL constraint
//Copy the content of the old table to the new one
//Remove the old table
                || EXTDAO_Atributo_atributo::existeAtributoAtributoComOTipoOperacao(
                        $this->idPVBB, 
                        array(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT,
                            EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE))){

//CREATE TABLE child2 ( 
//    id INTEGER PRIMARY KEY, 
//    parent_id INTEGER,
//    description TEXT
//);
//INSERT INTO child2 (id, parent_id, description)
//   SELECT id, parent_id, description FROM CHILD;
//DROP TABLE child;
//ALTER TABLE child2 RENAME TO child;
            $nomeTabelaAleatorio = Helper::getNomeAleatorio("temp_");
            $vObj = $this->getScriptCreate($pObjTabelaHomologacao, $nomeTabelaAleatorio);
            if($vObj != null)
                $consulta .= $vObj->getQuery();
            
            $vObj = $this->getScriptCopiaRegistros($nomeTabelaAleatorio, $pObjTabelaHomologacao, $pObjTabelaProducao);
            if ($vObj != null)
                $consulta .= $vObj->getQuery();
            
            $vObj = $this->getScriptDelete($pObjTabelaProducao->getNome());
            if ($consulta != null)
                $consulta .= $vObj->getQuery();
            
            $vObj = $this->getScriptRenomeaTabela($pObjTabelaHomologacao, $pObjTabelaProducao, $nomeTabelaAleatorio);
            if ($vObj != null)
                $consulta .= $vObj->getQuery();
        } else if( EXTDAO_Atributo_atributo::existeAtributoAtributoComOTipoOperacao(
                        $this->idPVBB, 
                        EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT)){
             

            $vVetorObjAtributo = $this->getScriptAtributos($pObjTabelaHomologacao, $pObjTabelaProducao);
            if($vVetorObjAtributo != null)
            foreach ($vVetorObjAtributo as $vObjAtributo) {
                switch ($vObjAtributo->idTipoOperacaoAtualizacao) {
                    case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                        $consulta .=$vObjAtributo->getQuery();
                    default:
                        break;
                }
            }
        }

        return $consulta;
    }

    protected function constroiScriptNoModification($pObjTabelaHomologacao, $pObjTabelaProducao) {
        //constroiScriptNoModification
        $consulta = "";
        if( EXTDAO_Atributo_atributo::existeAtributoAtributoComOTipoOperacao(
                        $this->idPVBB, 
                        array(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE,
                            EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT))){
            $nomeTabelaAleatorio = Helper::getNomeAleatorio("temp_");
            $vObj= $this->getScriptCreate($pObjTabelaHomologacao, $nomeTabelaAleatorio);
            if($vObj != null)
                $consulta .= $vObj->getQuery();
            
            
            $vObj = $this->getScriptCopiaRegistros($nomeTabelaAleatorio, $pObjTabelaHomologacao, $pObjTabelaProducao);
            if ($vObj != null)
                $consulta .= $vObj->getQuery();
 
            $vObj = $this->getScriptDelete($pObjTabelaProducao->getNome());
            if ($consulta != null)
                $consulta .= $vObj->getQuery();
            
            $vObj = $this->getScriptRenomeaTabela($pObjTabelaHomologacao, $pObjTabelaProducao, $nomeTabelaAleatorio);
            if ($vObj != null)
                $consulta .= $vObj->getQuery();
            
            
        }
        else if( EXTDAO_Atributo_atributo::existeAtributoAtributoComOTipoOperacao(
                        $this->idPVBB, 
                        array(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT))){
             

            $vVetorObjAtributo = $this->getScriptAtributos($pObjTabelaHomologacao, $pObjTabelaProducao);
            if($vVetorObjAtributo != null)
            foreach ($vVetorObjAtributo as $vObjAtributo) {
                switch ($vObjAtributo->idTipoOperacaoAtualizacao) {
                    case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                        $consulta .=$vObjAtributo->getQuery();
                    default:
                        break;
                }
            }
        }

        return $consulta;
    }

    protected function constroiScriptIgnoreCase($pObjTabelaHomologacao, $pObjTabelaProducao) {
       return $this->constroiScriptNoModification($pObjTabelaHomologacao, $pObjTabelaProducao);
    }

}

?>
