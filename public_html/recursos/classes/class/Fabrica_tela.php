<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GeradorTabela
 *
 * @author home
 */
class Fabrica_tela {

    public static $tiposFicticios = array(
        array("data_e_hora", "Data e hora Ex.: 03/02/1988 20:48"),
        array("data", "Data. Ex.: 03/02/1988"),
        array("hora", "Hora. Ex.: 20:48 "),
        array("preco", "Pre�o. Ex.: R$ 27,99 "),
        array("inteiro", "N�mero Inteiro. Ex.: 1, 2, 3"),
        array("descricao", "Descri��o (Uma linha)"),
        array("telefone", "Telefone"),
        array("texto", "Texto (M�ltiplas linhas)"),
        array("flutuante", "Real, Flutuante. Ex.: 1.2, 1.323"),
        array("consulta",  "Consultar outro registro. Ex.: Venda->Item da Venda")
    );
    
    
        
    
    
    public static function getStrOptionsTipoFicticio(){
        $str = "<option value=\"\">Selecione</option>";
        for($i = 0 ; $i < count(Fabrica_tela::$tiposFicticios); $i++){
            $str .= "<option value=\"".Fabrica_tela::$tiposFicticios[$i][0]."\">".Fabrica_tela::$tiposFicticios[$i][1]."</option>";    
        }
        return $str;
    }
    
    public $idsProjetosVersaoPorTipoAnaliseProjeto;
    public $idProjetos = ID_PROJETOS;
    public $idTipoConjuntoAnalise = EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado;
    public $db ;
    //put your code here
    public function __construct(){
        $this->db = new Database();
        $this->idProjetos = ID_PROJETOS;
        $this->idsProjetosVersaoPorTipoAnaliseProjeto = EXTDAO_Trunk::getProjetosVersaoDoTrunk(
            $this->idProjetos, $this->idTipoConjuntoAnalise, $this->db);
    }
    
    public function getProjetosVersao($pIdTipoAnaliseProjeto){
        for($i = 0 ; $i < count($this->idsProjetosVersaoPorTipoAnaliseProjeto); $i++){
            $idTipoAnaliseProjeto = $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][0];
            if($idTipoAnaliseProjeto == $pIdTipoAnaliseProjeto)
                return $this->idsProjetosVersaoPorTipoAnaliseProjeto[$i][1];
        }
        return null;
    }
    
}
