<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
abstract class Gerador_script_tabela {
    //put your code here
    public $objTabelaTabela;
    public $objScriptTabelaTabela;
    public $idSPVBB;
    public $geradorScriptAtributo;
    public $idPVBB;
    public $db;
    public $objSCB ;
    public $tempAtributo ;
    public $pObjTabelaProducao ;
    public $pObjTabelaHomologacao;
    public $vObjAtributoAtributo ;
    public $vObjPVBB ;
    protected function __construct($idSPVBB, $pObjTabelaTabela, $geradorScriptAtributo, $db = null){
        if($db == null) $db = new Database();
        $this->db = $db;

        $this->objTabelaTabela = $pObjTabelaTabela;
        $this->idSPVBB = $idSPVBB;
        $this->vObjPVBB = new EXTDAO_Script_tabela_tabela($this->db);

        if($pObjTabelaTabela != null)
            $this->objScriptTabelaTabela = $this->insereScriptTabelaTabela();

        $this->geradorScriptAtributo = $geradorScriptAtributo;
        if($pObjTabelaTabela != null)
            $this->idPVBB = $pObjTabelaTabela->getProjetos_versao_banco_banco_id_INT();

        $this->objSCB = new EXTDAO_Script_comando_banco($this->db);
        $this->tempAtributo = new EXTDAO_Atributo($this->db);
        $this->pObjTabelaProducao = new EXTDAO_Tabela($this->db);
        $this->pObjTabelaHomologacao = new EXTDAO_Tabela($this->db);
        $this->vObjAtributoAtributo = new EXTDAO_Atributo_atributo($this->db);


    }
    
    public $prefixoTabela = "";
    public function setPrefixoTabela($prefixo){
        $this->prefixoTabela = $prefixo;
        $this->geradorScriptAtributo->setPrefixoTabela($prefixo);
    }
    
    public function getScriptTabela(){
        $objTabela = null;

        $this->pObjTabelaProducao->clear();
        if(strlen($this->objTabelaTabela->getProducao_tabela_id_INT())){
            $this->pObjTabelaProducao->select($this->objTabelaTabela->getProducao_tabela_id_INT());
            $objTabela = $this->pObjTabelaProducao;
        } else {
            $objTabela = null;
        }
        
        $objTabelaHomologacao = null;
        $this->pObjTabelaHomologacao->clear();
        if(strlen($this->objTabelaTabela->getHomologacao_tabela_id_INT())){
            $this->pObjTabelaHomologacao->select($this->objTabelaTabela->getHomologacao_tabela_id_INT());
            $objTabelaHomologacao  = $this->pObjTabelaHomologacao;
        } else
            $objTabelaHomologacao =null;
        
        $vTipoAtualizacao = $this->objTabelaTabela->getTipo_operacao_atualizacao_banco_id_INT();
        if($vTipoAtualizacao == null) return null;
        else{
            $consulta = "";
            switch ($vTipoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
                    $consulta .= $this->constroiScriptDelete($objTabela);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                     $consulta .= $this->constroiScriptEdit($objTabelaHomologacao, $objTabela);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                    $consulta .= $this->constroiScriptCreate($objTabelaHomologacao);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION:
                    $consulta .= $this->constroiScriptNoModification($objTabelaHomologacao, $objTabela);
                    break;
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE:
                    $consulta .= $this->constroiScriptIgnoreCase($objTabelaHomologacao, $objTabela);
                    break;
                
                default:
                    return null;
            }
            
            return strlen($consulta) ? $consulta : null;
        }
    }
    protected abstract function constroiScriptDelete($pObjTabelaHomologacao);
    
    protected abstract function constroiScriptCreate($pObjTabelaProducao);
    
    protected abstract function constroiScriptEdit($pObjTabelaHomologacao, $pObjTabelaProducao);
    
    protected abstract function constroiScriptNoModification($pObjTabelaHomologacao, $pObjTabelaProducao);
    
    protected abstract function constroiScriptIgnoreCase($pObjTabelaHomologacao, $pObjTabelaProducao);
    
    protected function getScriptAtributos($pObjTabelaHomologacao, $pObjTabelaProducao){

        $this->vObjAtributoAtributo->clear();

        $vVetorObj = array();
        if($pObjTabelaHomologacao != null){
            $vVetorIdAtributoAtributoHomologacao = EXTDAO_Atributo_atributo::getListIdAtributoAtributoDaTabelaDeHomologacao(
                $this->idPVBB
                , $pObjTabelaHomologacao->getId()
                , $this->db);
            if($vVetorIdAtributoAtributoHomologacao != null)
            foreach ($vVetorIdAtributoAtributoHomologacao as $vIdAtributoAtributo) {
                $this->vObjAtributoAtributo->clear();
                $this->vObjAtributoAtributo->select($vIdAtributoAtributo);
                $this->geradorScriptAtributo->inicializa(
                        $this->objTabelaTabela, 
                        $this->vObjAtributoAtributo,
                        $this->objScriptTabelaTabela->getId(),
                        $this->idSPVBB);
                $vObjScriptUpdateAtributo = $this->geradorScriptAtributo->getScriptAtributo();
                if($vObjScriptUpdateAtributo != null)
                    $vVetorObj[count($vVetorObj)] = $vObjScriptUpdateAtributo;
            }
        }
        
        if($pObjTabelaProducao != null){
            $vVetorIdAtributoAtributoApenasProducao= EXTDAO_Atributo_atributo::getListIdAtributoAtributoApenasDaTabelaDeProducao(
                    $this->idPVBB
                    , $pObjTabelaProducao->getId()
                    , $this->db);
            if($vVetorIdAtributoAtributoApenasProducao != null)
            foreach ($vVetorIdAtributoAtributoApenasProducao as $vIdAtributoAtributo) {
                $this->vObjAtributoAtributo->clear();
                $this->vObjAtributoAtributo->select($vIdAtributoAtributo);
                $this->geradorScriptAtributo->inicializa(
                        $this->objTabelaTabela, 
                        $this->vObjAtributoAtributo,
                        $this->objScriptTabelaTabela->getId(),
                        $this->idSPVBB
                 );
                $vObjScriptUpdateAtributo = $this->geradorScriptAtributo->getScriptAtributo();
                if($vObjScriptUpdateAtributo != null)
                    $vVetorObj[count($vVetorObj)] = $vObjScriptUpdateAtributo;
            }
        }
        return $vVetorObj;
    }
    
    protected function insereScriptComandoBanco(
        $consulta
        , $tipoComandoBanco
        , $idTabelaChaveTabelaChave = null){

        if(!strlen($consulta)){
            $i = 0;
            return;
        }
        $this->objSCB->clear();
        $this->objSCB->setScript_tabela_tabela_id_INT($this->objScriptTabelaTabela->getId());
        $this->objSCB->setConsulta($consulta);
        $vSeqINT = EXTDAO_Script_projetos_versao_banco_banco::getProximoSeqScriptComandoBanco($this->idSPVBB);
        $this->objSCB->setSeq_INT($vSeqINT);
        $this->objSCB->setScript_tabela_tabela_id_INT($this->objScriptTabelaTabela->getId());
        $this->objSCB->setAtributo_atributo_id_INT(null);
        $this->objSCB->setTipo_comando_banco_id_INT($tipoComandoBanco);

        $this->objSCB->setTabela_chave_tabela_chave_id_INT($idTabelaChaveTabelaChave);
        
        $this->objSCB->formatarParaSQL();
        $this->objSCB->insert();
        $this->objSCB->selectUltimoRegistroInserido();
        return $this->objSCB;
    }
    
    protected function insereScriptTabelaTabela(){
        $this->vObjPVBB->clear();
        $this->vObjPVBB->setScript_projetos_versao_banco_banco_id_INT($this->idSPVBB);
        $this->vObjPVBB->setTabela_tabela_id_INT($this->objTabelaTabela->getId());
        $this->vObjPVBB->setTipo_operacao_atualizacao_banco_id_INT($this->objTabelaTabela->getTipo_operacao_atualizacao_banco_id_INT());
        $this->vObjPVBB->formatarParaSQL();
        $this->vObjPVBB->insert();
        $this->vObjPVBB->selectUltimoRegistroInserido();
        return $this->vObjPVBB;
    }
      
}
