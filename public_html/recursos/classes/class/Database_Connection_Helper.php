<?php


class Database_Connection_Helper {

    public function __construct(){


    }

    public function getCorporacaoMySQLConnectionParameters($idCorporacao){

        $objParameters = new MySQLConnectionParameters();

        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getConfiguracoesDeBancoDaCorporacao&corporacao_id={$idCorporacao}";
//        echo $url;
//        exit();
        $json = Helper::chamadaCurlJson($url);
//        echo $strJson;
//        exit();

        if(Interface_mensagem::checkOk(( $json)))
            return null;

        $obj = $json->mValor;

        if(!isset($obj->database) || empty($obj->database)){
            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                "Falha ao carregar a conex�o do banco de dados da corporacao: $idCorporacao.
                 Retorno json: ".json_encode($json));
        }
        $objParameters->host = $obj->host;
        $objParameters->port = $obj->porta;
        $objParameters->database = $obj->database;
        $objParameters->user = $obj->usuario;
        $objParameters->password = $obj->senha;
//        print_r($strJson);
//        exit();
        return $objParameters;

    }

}


class MySQLConnectionParameters {

    public $host;
    public $port;
    public $database;
    public $user;
    public $password;

}