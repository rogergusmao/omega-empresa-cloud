<?php

class Database_Comparator_MySQL {// Classe : in�cio

    private $host;
    private $DBName;
    private $usuario;
    private $senha;
    public $query;
    public $result;
    public $array;
    public $object;
    public $rows;
    public $lastInsertedId;
    public $now;
    public $indiceLink;
    public $errCode;
    public $errMessage;
    public static $arrLinks = array();
    public static $arrDBNames = array();
    public static $arrAutocommit = array();
    public $arrTables = array();

    public function executeScript($pathArquivoSQL){

        $comando = "mysql -u{$this->usuario} -p{$this->senha} "
        . "-h {$this->host} -D {$this->DBName} < ";
        $comando = $comando . ' ' . $pathArquivoSQL;
        echo "Comando: $comando</br>";
        
        $retorno = shell_exec($comando);
        echo "RETORNO: $retorno </br>";
        return $retorno;

    }
    
    public function __construct(
            $databaseName = false, 
            $host = false, 
            $porta = false, 
            $usuario= false,
            $senha = false) {// M�todo : in�cio - Construtor

        if ($databaseName) {

            $this->DBName = $databaseName;

        }
        else {

            $this->DBName = NOME_BANCO_DE_DADOS_PRINCIPAL;

            if (defined('NOME_BANCO_DE_DADOS_SUBSTITUIR_PRINCIPAL')) {

                $this->DBName = NOME_BANCO_DE_DADOS_SUBSTITUIR_PRINCIPAL;
            }

        }
        
        if(!$host)
            $this->host = BANCO_DE_DADOS_HOST;
        else
            $this->host = $host;
        
        if(!$porta)
            $this->porta = BANCO_DE_DADOS_PORTA;
        else
            $this->porta = $porta;
        
        if(!$usuario)
            $this->usuario = BANCO_DE_DADOS_USUARIO;
        else
            $this->usuario = $usuario;
        
        if(!$senha)
            $this->senha = BANCO_DE_DADOS_SENHA;
        else
            $this->senha= $senha;
//        print_r($this);

        $this->lastInsertedId = "";
        $this->rows() = 0;
        $this->now = date("Y-m-d H:i:s");

        $this->indiceLink = -1;

        $this->openLink();
    }

// M�todo : Fim - Construtor

    public function getTablesArray($forceUpdate=false) {

        if(count($this->arrTables) == 0 || $forceUpdate) {

            $this->query("SHOW TABLES");

            while ($dados = $this->fetchArray()) {

                $this->arrTables[] = $dados[0];

            }

        }

        return $this->arrTables;

    }

    public function getPrimaryKeyFieldsArray($tableName){

        $arrRetorno = array();
        $this->query("SELECT `COLUMN_NAME`
                       FROM `information_schema`.`COLUMNS`
                       WHERE (`TABLE_SCHEMA` = '{$this->DBName}')
                       AND (`TABLE_NAME` = '{$tableName}')
                       AND (`COLUMN_KEY` = 'PRI');");

        while($dados = $this->fetchArray(MYSQL_NUM)){

            $arrRetorno[] = $dados[0];

        }

        return $arrRetorno;

    }

    public function getFieldsArray($tableName){

        $arrRetorno = array();
        $this->query("SHOW COLUMNS FROM {$tableName}");

        while($dados = $this->fetchArray(MYSQL_NUM)){

            $arrRetorno[] = $dados[0];

        }

        return $arrRetorno;

    }

    public function iniciarTransacao() {

        mysqli_autocommit(self::$arrLinks[$this->indiceLink], false);
        mysqli_query(self::$arrLinks[$this->indiceLink], "START TRANSACTION;");

        self::$arrAutocommit[$this->indiceLink] = false;
    }

    public function commitTransacao() {

        if (self::$arrAutocommit[$this->indiceLink] === false) {

            mysqli_query(self::$arrLinks[$this->indiceLink], "COMMIT;");
        }
    }

    public function rollbackTransacao() {

        if (self::$arrAutocommit[$this->indiceLink] === false) {

            mysqli_query(self::$arrLinks[$this->indiceLink], "ROLLBACK;");
        }
    }

    public function getUser() {

        return $this->usuario;
    }

    public function getSenha() {

        return $this->senha;
    }

    public function getHost() {

        return $this->host;
    }

    public function getDBName() {

        return $this->DBName;
    }
    
    public function getDescricao(){
        return $this->getHost()."::".$this->getDBName();
    }

    public function __destruct() {
        
    }

    private function openLink() {// M�todo : in�cio

        if (!in_array($this->DBName, self::$arrDBNames)) {

            $indiceLink = count(self::$arrLinks);
            $this->indiceLink = $indiceLink;

            self::$arrDBNames[$this->indiceLink] = $this->DBName;
            self::$arrAutocommit[$this->indiceLink] = true;
             
            self::$arrLinks[$this->indiceLink] = @mysqli_connect($this->host, $this->usuario, $this->senha, $this->DBName, $this->porta);

            if (mysqli_connect_errno()) {


            }

            $this->selectDB();

        } else {

            $this->indiceLink = array_search($this->DBName, self::$arrDBNames);

            if (self::$arrLinks[$this->indiceLink] && mysqli_errno(self::$arrLinks[$this->indiceLink])) {

                //se tem 2 links com o mesmo db, eh pq deu erro, se tiver mais
                //eh pq deu erro dentro do erro, ocasionando um loop infinito
                //entao para

                if (count(array_keys(self::$arrDBNames, $this->DBName)) > 2) {

                    exit();
                    
                }

                $this->indiceLink = count(self::$arrLinks);

                self::$arrLinks[$this->indiceLink] = @mysqli_connect($this->host, $this->usuario, $this->senha, $this->DBName);

                if (mysqli_connect_errno()) {


                }

                self::$arrDBNames[$this->indiceLink] = $this->DBName;

                $this->selectDB();
            }
        }
    }
// M�todo : fim

    private function selectDB() {// M�todo : in�cio
        if ($this->indiceLink == -1) {

            $this->openLink();
        }

        if (!mysqli_select_db(self::$arrLinks[$this->indiceLink], self::$arrDBNames[$this->indiceLink])) {

        }
    }

// M�todo : fim

    public function imprimirQuery($formatacao = false) {

        if ($formatacao) {

            Helper::imprimirMensagem($this->query);
        } else {

            Helper::imprimirMensagemNaoFormatada($this->query . "<br /><br />");
        }
    }

    
    public function getListaAtributoDaTabela($p_strTableName) {
        if (strlen($p_strTableName)) {

            $this->query("SHOW COLUMNS FROM " . $p_strTableName . ";");
            $v_listAttrReturn = array();
            $v_index = 0;
            return Helper::getResultSetToArrayDeUmCampo($this->result);
        }
    }

    public function getListaDosNomesDasTabelas() {


        // Pega todo pedido no status requisitado, seje relativo a um produto individual, ou a um combo, ou promocao.
        $this->Query("SHOW TABLES;");
        $v_listaNomeTabela = Helper::getResultSetToArrayDeUmCampo($this->result);
        $v_arrayRetorno = array();
        $v_index = 0;
        if($v_listaNomeTabela  != null)
        foreach ($v_listaNomeTabela as $v_strTabela) {
            $v_arrayRetorno[$v_index] = trim(strtolower($v_strTabela));
            $v_index += 1;
        }
           
        return $v_arrayRetorno;
    }

    public function query($query) {// M�todo : in�cio

        if ($this->indiceLink == -1) {

            $this->openLink();
            $this->selectDB();
        }

        $this->query = $query;
        $this->result = mysqli_query(self::$arrLinks[$this->indiceLink], $this->query);

        if (!$this->result) {

            if (substr(strtolower($query), 0, 6) == "insert") {

                $codigosDeErro = array(1062);

                if (in_array(mysqli_errno(self::$arrLinks[$this->indiceLink]), $codigosDeErro)) {

                    $this->imprimirQuery(true);
                    $erro= "O Registro n�o p�de ser inserido pois j� existe um registro com a mesma chave �nica";
                    $this->rollbackTransacao();
                    throw new Exception(mysqli_error($erro));
                }
            } elseif (substr(strtolower($query), 0, 6) == "delete") {

                $codigosDeErro = array(1216, 1217, 1451, 1452);
                $codigoDesteErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);

                if (in_array($codigoDesteErro, $codigosDeErro)) {

                    $erro= "O Registro n�o p�de ser removido devido � depend�ncias com outras entidades no banco de dados (Erro {$codigoDesteErro})";
                    $erro .= mysqli_error(self::$arrLinks[$this->indiceLink]);
                    
                    $this->rollbackTransacao();
                    throw new Exception(mysqli_error($erro));
                }
            }

            $erro = self::$arrLinks[$this->indiceLink];
            $this->rollbackTransacao();
            throw new Exception("<b>ERRO NA EXECU��O DO COMANDO SQL</b>

                                     <b>QUERY: </b> ". nl2br($this->query) . "
                                     <b>N� DO ERRO: </b> " . mysqli_errno(self::$arrLinks[$this->indiceLink]) . "
                                     <b>ERRO: </b> " . mysqli_error(self::$arrLinks[$this->indiceLink]). "
                                     ");
            
        } else {

            if (substr(strtolower(str_replace("(", "", $query)), 0, 6) == "select" || substr(strtolower($query), 0, 4) == "show") {

                $this->rows() = mysqli_num_rows($this->result);
                return $this->result;
            } elseif (substr(strtolower($query), 0, 6) == "insert") {

                $sql = "SELECT last_insert_id()";
                $con = mysqli_query(self::$arrLinks[$this->indiceLink], $sql);
                if (!$con) {

                    $this->rollbackTransacao();
                } else {

                    $rs = mysqli_fetch_object($con);
                    $this->lastInsertedId = $rs->Id;
                    return $this->result;
                }
            } else {

                return $this->result;
            }
        }
    }
    
    
    
    public function query($query, $forcarParadaOnError=true) {// M�todo : in�cio

        $this->errCode = false;
        $this->errMessage = "";

        if ($this->indiceLink == -1) {

            $this->openLink();
            $this->selectDB();
        }

        $this->query = $query;
        $this->result = mysqli_query(self::$arrLinks[$this->indiceLink], $this->query);

        if (!$this->result) {

            if (substr(strtolower($query), 0, 6) == "insert") {

                $codigosDeErro = array(1062);

                if (in_array(mysqli_errno(self::$arrLinks[$this->indiceLink]), $codigosDeErro)) {

                    $this->imprimirQuery(true);
                    Helper::imprimirMensagem("O Registro n�o p�de ser inserido pois j� existe um registro com a mesma chave �nica", MENSAGEM_WARNING);
                    $this->rollbackTransacao();
                    exit();

                }

            }
            elseif (substr(strtolower($query), 0, 6) == "delete") {

                $codigosDeErro = array(1216, 1217, 1451, 1452);
                $codigoDesteErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);

                if (in_array($codigoDesteErro, $codigosDeErro)) {

                    Helper::imprimirMensagem("O Registro n�o p�de ser removido devido � depend�ncias com outras entidades no banco de dados (Erro {$codigoDesteErro})", MENSAGEM_WARNING);
                    Helper::imprimirMensagem(mysqli_error(self::$arrLinks[$this->indiceLink]));
                    $this->rollbackTransacao();
                    exit();

                }

            }
            else{

                $this->errCode = mysqli_errno(self::$arrLinks[$this->indiceLink]);
                $this->errMessage  = urlencode(mysqli_error(self::$arrLinks[$this->indiceLink]));

                $this->rollbackTransacao();

                if($forcarParadaOnError) {

                    Helper::imprimirMensagem("<b>ERRO NA EXECU��O DO COMANDO SQL</b>

                                     <b>QUERY: </b> " . nl2br($this->query) . "
                                     <b>N� DO ERRO: </b> " . mysqli_errno(self::$arrLinks[$this->indiceLink]) . "
                                     <b>ERRO: </b> " . mysqli_error(self::$arrLinks[$this->indiceLink]) . "
                                     ", MENSAGEM_ERRO);

                    exit();

                }

            }

        }
        else {

            if (substr(strtolower(str_replace("(", "", $query)), 0, 6) == "select" || substr(strtolower($query), 0, 4) == "show") {

                $this->rows() = mysqli_num_rows($this->result);
                return $this->result;

            } elseif (substr(strtolower($query), 0, 6) == "insert") {

                $sql = "SELECT last_insert_id()";
                $con = mysqli_query(self::$arrLinks[$this->indiceLink], $sql);
                if (!$con) {

                    $this->rollbackTransacao();
                } else {

                    $rs = mysqli_fetch_object($con);
                    $this->lastInsertedId = $rs->Id;
                    return $this->result;
                }
            } else {

                return $this->result;
            }
        }
    }

// M�todo : fim

    public function voltarPonteiroDoResultSet() {

        return mysqli_data_seek($this->result, 0);
    }

    public function fetchArray($constante=MYSQL_BOTH) {//M�todo : in�cio
        $this->array = mysqli_fetch_array($this->result, $constante);

        return $this->array;
    }

//M�todo : fim

    public function fetchObject() {//M�todo : in�cio
        $this->object = mysqli_fetch_object($this->result);

        return $this->object;
    }

//M�todo : fim

    public function getPrimeiraTuplaDoResultSet($campo) {//M�todo : in�cio
        return self::mysqli_result($this->result, 0, $campo);
    }

//M�todo : fim

    public function getResultSet() {

        return $this->result;
    }

    public function resultSet($linha, $coluna) {

        return Database::mysqli_result($this->result, $linha, $coluna);
    }

    public static function mysqli_result($resultSet, $linha, $coluna) {

        if (mysqli_data_seek($resultSet, $linha)) {

            $linha = $resultSet->fetch_array(MYSQLI_BOTH);

            return $linha[$coluna];
        }
    }

}

// Class : fim
?>