<?php
class Gerador_web_dao{
    public function __construct(){
        
    }
    public function gerarDAOWeb(
        $vetorTabelaBancoOrdenada, $table, $class, $key, 
        $label, $ext, $sobrescrever, $idPVBBPrototipoWeb = null){
        
        set_time_limit(40 * 60);    
        
        $objBanco = new Database();
         if($idPVBBPrototipoWeb == null)
            $idPVBBPrototipoWeb = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
//         echo $idPVBBPrototipoWeb;
//         exit();
         
        $ret = EXTDAO_Projetos_versao_banco_banco::getIdProjetosEProjetosVersao(
            $idPVBBPrototipoWeb, $objBanco);
        
        $idProjeto = $ret[0];
        $idPV = $ret[1];
//print_r($idPV);
//        exit();
        $arrExcessoesRelacionamento = array();


        $varGET= Param_Get::getIdentificadorProjetosVersao();
        
        
        $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBBPrototipoWeb, $objBanco);
        
        $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBBPrototipoWeb, $objBanco);

        
        $idDiretorio = EXTDAO_Diretorio_web::getIdDiretorio($idPVBBPrototipoWeb, $objBanco);
        if(!strlen($idDiretorio)){
            Helper::imprimirMensagem("Não encontrou o ditorio web mapeado para projetos_versao_banco_banco = $idPVBBPrototipoAndroid", MENSAGEM_ERRO);
            exit();
        }
        $objDiretorio = new EXTDAO_Diretorio_web();
        $objDiretorio->select($idDiretorio);

        $objPadronizadorDatabase = new Padronizador_Database($database);
        
        $raizSrc = $objDiretorio->getRaiz();
        $raizDatabase = $objDiretorio->getClasses();

        $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raizSrc));
        $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
        $packageDatabase = str_replace("/", ".", $raizDatabase);

        $diretorio = $objDiretorio->getDAO();
        $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorio));


        $objTabela = new EXTDAO_Tabela();
        if(is_numeric($table)){
            $idTabela = $table;
            $objTabela->select($table);
            $table = $objTabela->getNome();
        } else {
            $idTabela = EXTDAO_Tabela::existeTabela($table, $idBanco, $idPV);    
            if(!strlen($idTabela)){

                Helper::imprimirMensagem ("Tabela $table. idBanco: $idBanco. Banco ".$database->getDBName().". Projetos Versão: $idPV.  não encontrada.", MENSAGEM_ERRO);
                exit();
            }
            $objTabela->select($idTabela);
        }

        $objTabela->select($idTabela);
        $class = "DAO_".$objTabela->getNomeClasseWeb();
        $dir = dirname(__FILE__);

        $filename = Helper::getPathComBarra($raizSrc) .Helper::getPathComBarra($diretorio) . $class . ".php";

        $sql = "SHOW TABLES LIKE '$table';";

        $database->query($sql);

        if($database->rows() < 1){

            return;

        }
        


        $database->query($sql);

        if($database->rows() < 1){

            return;

        }

        $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

        // if file exists, then delete it
        if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
        {
            unlink($filename);
        }
//        echo $filename;
//        exit();
        if(!file_exists($filename)){

        // open file in insert mode
        $file = fopen($filename, "w+");
        $filedate = date("d.m.Y");

        $c = "";

        $c = "<?

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  $class
        * DATA DE GERAÇÃO: $filedate
        * ARQUIVO:         $class.php
        * TABELA MYSQL:    $table
        * BANCO DE DADOS:  {$database->getDBName()}
        * -------------------------------------------------------
        *
        */

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************

        class $class extends Generic_DAO
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************

            public \$idSistemaTabela = null;
    ";

        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;
        $labelUpper = ucfirst($label);
        $keyUpper = ucfirst($key);

        $sqlAutoIncremento = "SHOW COLUMNS FROM $table WHERE `Field` = '$key' AND `Extra` = 'auto_increment';";
        $database->query($sqlAutoIncremento);
        $result2 = $database->result;

        $autoInc = mysqli_num_rows($result2);

        if($autoInc == 1)
            $autoIncremento = "TRUE";
        else
            $autoIncremento = "FALSE";

        $numeroArquivos = 0;

        $tabelasRelacionamento = $_POST["tabelasrelacionamento"];
        if($tabelasRelacionamento != null)
        foreach($tabelasRelacionamento as $tabelaRelacionamento){

            $databaseAux = new Database();
            $databaseAux->query("SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT={$idProjeto} AND tabela_nome='{$tabelaRelacionamento}'");

            while ($row = mysqli_fetch_array($databaseAux->result)){

                $arrCamposComplemento = unserialize(html_entity_decode($row[0]));

            }

            $nomeEntidadePlural = $arrCamposComplemento["entidade_plural"];
            $nomeTabela = $arrCamposComplemento["tablename"];
            $nomeTabelaUC = ucfirst($nomeTabela);
            $classeEXT = $arrCamposComplemento["extname"];

            $tabelaBase = $table;
            $tabelaBaseUC = ucfirst($tabelaBase);

            $campoChave = $arrCamposComplemento["keyname"];
            $campoChaveUC = ucfirst($campoChave);

            //action add
            $conteudoAjaxRelacionamento .= "

                    //bloco de {$nomeEntidadePlural}
                    \$obj{$nomeTabelaUC} = new {$classeEXT}();
                    \$obj{$nomeTabelaUC}->set{$tabelaBaseUC}_id_INT(\$this->get{$campoChaveUC}());

                    Ajax::persistirEntidadesPresentesNosBlocosEmLista(\$obj{$nomeTabelaUC}, \"{$nomeTabela}_{$campoChave}\", true, array(), true);

            ";
        }

        $cadastrado[0] = "cadastrado";
        $cadastrado[1] = "cadastrada";
        $cadastrado[2] = "cadastrados";
        $cadastrado[3] = "cadastradas";

        $modificado[0] = "modificado";
        $modificado[1] = "modificada";
        $modificado[2] = "modificados";
        $modificado[3] = "modificadas";

        $excluido[0] = "excluído";
        $excluido[1] = "excluída";
        $excluido[2] = "excluídos";
        $excluido[3] = "excluídas";

        $foi[0] = "foi";
        $foi[1] = "foi";
        $foi[2] = "foram";
        $foi[3] = "foram";

        $artigo[0] = "O";
        $artigo[1] = "A";
        $artigo[2] = "Os";
        $artigo[3] = "As";

        $genero   = $_POST["genero_entidade"]; //F ou M
        $nomeSing = $_POST["entidade_singular"];
        $nomePlu  = $_POST["entidade_plural"];
        $numeroCadastros = $_POST["numeroCadastros"];

        $index = 0;

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $mensagemCadastroSucesso = $artigo[$index] . " " . $nomeMsg . " " . $foi[$index] . " " . $cadastrado[$index] . " com sucesso.";

        $mensagemEdicaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $modificado[$index] . " com sucesso.";

        while ($row = mysqli_fetch_row($result))
        {

                $col = $row[0];
                $colUpper = ucfirst($col);
                $nomeColunaFormatado = ucwords($_POST["nomexcampo_" . $col]);

                $ocorrencias = array(" De ", " Da ", " Do ", " Das ", " Dos ", " Em ", " No ", " Na ", " Nos ", " Nas ");
                $substituicoes = array(" de ", " da ", " do ", " das ", " dos ", " em ", " no ", " na ", " nos ", " nas ");

                $nomeColunaFormatado = str_replace($ocorrencias, $substituicoes, $nomeColunaFormatado);

                $instLabel .= "\t\t\t\$this->label_{$col} = \"{$nomeColunaFormatado}\";\n";

                if(substr_count($col, "_ARQUIVO") > 0 || substr_count($col, "_IMAGEM") > 0){

                   $numeroArquivos++;

                   $strUploadFile .= "
            
            public function __upload$colUpper(\$numRegistro=1, \$urlErro){

                \$dirUpload  = \$this->diretorio_{$col};
                \$labelCampo = \$this->label_{$col};

                \$objUpload = new Upload();

                \$objUpload->arrPermitido = \"\";
                \$objUpload->tamanhoMax = \"\";
                \$objUpload->file = \$_FILES[\"$col{\$numRegistro}\"];
                \$objUpload->nome = \$this->getId() . \"_$numeroArquivos.\" . Helper::getExtensaoDoArquivo(\$objUpload->file[\"name\"]);
                \$objUpload->uploadPath = \$dirUpload;\n";

                if(substr_count($col, "_IMAGEM") > 0){

                    $strUploadFile .= "\t\t\t\$success = \$objUpload->uploadImagem(\$this->dimensoes_{$col}[0], \$this->dimensoes_{$col}[1]);";

                }
                else{

                    $strUploadFile .= "\t\t\t\$success = \$objUpload->uploadFile();";

                }

                $strUploadFile .= "

                if(!\$success){

                    \$_SESSION[\"erro\"] = true;
                    \$this->createSession();
                    return array(\"location: \$urlErro&msgErro=Erro no upload do arquivo \$labelCampo:\" . \$objUpload->erro);
                    exit();

                }
                else{

                    return array(\$objUpload->nome, \$dirUpload);

                }

            }
               ";

                $strActionAdd .= "
                //UPLOAD DO ARQUIVO $colUpper
                if(Helper::verificarUploadArquivo(\"$col{\$i}\")){

                    if(is_array(\$arquivo = \$this->__upload$colUpper(\$i, \$urlErro))){

                        \$this->updateCampo(\$this->get$keyUpper(), \"$col\", \$arquivo[0]);

                    }

                }

                    ";

                $strActionEdit .= "
                //UPLOAD DO ARQUIVO $colUpper
                if(Helper::verificarUploadArquivo(\"$col{\$i}\")){

                    if(is_array(\$arquivo = \$this->__upload$colUpper(\$i, \$urlErro))){

                        \$this->updateCampo(\$this->get$keyUpper(), \"$col\", \$arquivo[0]);

                    }

                }

                    ";

                $strActionRemove .= "

                //REMOÇÃO DO ARQUIVO $colUpper
                \$pathArquivo = \$this->diretorio_$col . \$this->get$colUpper();

                if(file_exists(\$pathArquivo)){

                    unlink(\$pathArquivo);

                }

                    ";

                }

            $camposAjax = $_POST["camposajaxmestres"];

                    if(is_array($camposAjax)){

                        foreach($camposAjax as $campoAjax){

                            $campoAjaxUpper = ucfirst($campoAjax);

                            $listaCmdsAjax .= "\$this->set{$campoAjaxUpper}(Helper::POST(\"{$campoAjax}1\"));\n";

                        }

                    }

            $col=$row[0];
            $colUpper = ucfirst($col);

            $attrLabel .= "\tpublic \$label_{$col};\n";

            if(substr_count($col, "_ARQUIVO") > 0 || substr_count($col, "_IMAGEM") > 0){

                $attrDiretorio .= "\tpublic \$diretorio_$col;\n";

                if(substr_count($col, "_IMAGEM") > 0){

                    $attrDimensoesImagem .= "\tpublic \$dimensoes_{$col};\n";

                }

            }
            elseif(substr_count($col, "_HTML") > 0){


            }

            if(true)
            {

                if(substr_count($col, "_DATE") > 0){

                    $timestamp3 .= "\tpublic $" . $col . "_UNIX;\n";

                }

                $c.= "\tpublic $$col;\n";

                if(substr_count($col, "_id_") > 0 && !in_array($col, $arrExcessoesRelacionamento)){
                    $tabela = $objPadronizadorDatabase->getMorePossibleTableOfForeignKeyByNameOfField($col);
                    $nomeAtributo = substr($col, 0, strpos($col, "_id_"));
                    
                    $objTabela = "obj" . ucfirst($nomeAtributo);
                    $objTabelaUpper = ucfirst($objTabela);
                    $nomeMetodo = "All" . ucfirst($nomeAtributo);
                    $colUpper = ucfirst($col);
                    $functionGetIdFK = "get" . $colUpper . "()";
                    $functionGetObjFK = "getFk{$colUpper}()";
                    $renderGetFk =  "
            public function $functionGetObjFK {
                if(\$this->$objTabela == null){
                    
                    \$this->$objTabela = new EXTDAO_" . ucfirst($tabela) . "(\$this->getConfiguracaoDAO());
                }
                \$idFK = \$this->$functionGetIdFK;
                if(!strlen(\$idFK)){
                    \$this->{$objTabela}->clear();
                }
                else if(\$this->{$objTabela}->getId() != \$idFK){
                    \$this->{$objTabela}->select(\$idFK);
                }
                return \$this->$objTabela;
            }\n";
                    
                    $renderSelect .= $renderGetFk."\n\n";
                    
                    $renderSelect .= "public function getComboBox{$nomeMetodo}(\$objArgumentos){\n\n";
                    $renderSelect .= "\t\t\$objArgumentos->nome=\"$col\";\n";
                    $renderSelect .= "\t\t\$objArgumentos->id=\"$col\";\n";

                    if(substr_count($col, "_id_value") > 0){

                        $renderSelect .= "\t\t\$objArgumentos->valueReplaceId=true;\n\n";

                    }
                    else{

                        $renderSelect .= "\t\t\$objArgumentos->valueReplaceId=false;\n\n";

                    }
                    $renderSelect .= "\t\t\$this->{$objTabela} = \$this->{$functionGetObjFK};\n";
                    $renderSelect .= "\t\treturn \$this->{$objTabela}->getComboBox(\$objArgumentos);\n\n";
                    $renderSelect .= "\t}\n\n";

                    $c.= "\tpublic \$$objTabela;\n";

                }

            } // endif

        } // endwhile

        //$cdb = "$" . "database";
        $cdb2 = "database";

        $c.="

        

        public \$nomeEntidade;

    $timestamp3

        ";

        $cthis = "$" . "this->";
        $thisdb = $cthis . $cdb2 . " = " . "new Database();";

        $c.= "

    $attrLabel

    $attrDiretorio

    $attrDimensoesImagem

        // **********************
        // MÉTODO CONSTRUTOR
        // **********************

        public function __construct(\$configDAO = null)
        {

        \tparent::__construct(\$configDAO);

        \t\$this->nomeEntidade = \"$nomeSing\";
        \t\$this->nomeTabela = \"$table\";
        \t\$this->campoId = \"$key\";
        \t\$this->campoLabel = \"$label\";\n
            
        \t\$this->idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(\$this->nomeTabela, \$this->database);
        }

        public function valorCampoLabel(){

            return \$this->get$labelUpper();

        }

        ";

        $c.="

            {$renderSelect}

             public function __actionAdd(){

                \$mensagemSucesso = \"{$mensagemCadastroSucesso}\";

                \$numeroRegistros = Helper::POST(\"numeroRegs\");

                \$urlSuccess = Helper::getUrlAction(Helper::POST(\"next_action\"), Helper::POST(\"$key\"));
                \$urlErro = Helper::getUrlAction(Helper::POST(\"origin_action\"), Helper::POST(\"$key\"));

                for(\$i=1; \$i <= \$numeroRegistros; \$i++){

                    \$this->setByPost(\$i);
                    \$this->formatarParaSQL();

                    \$this->insert(true);
                    \$this->selectUltimoRegistroInserido();

                    {$strActionAdd}
                    {$conteudoAjaxRelacionamento}

                }

                return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\");

            }

            public function __actionAddAjax(){

                \$mensagemSucesso = \"$mensagemCadastroSucesso\";

                \$numeroRegistros = Helper::POST(\"numero_registros_ajax\");

                \$urlSuccess = Helper::getUrlAction(Helper::POST(\"next_action\"), Helper::POST(\"$key\"));
                \$urlErro = Helper::getUrlAction(Helper::POST(\"origin_action\"), Helper::POST(\"$key\"));

                for(\$i=1; \$i <= \$numeroRegistros; \$i++){

                    \$this->setByPost(\$i);

                    $listaCmdsAjax

                    \$this->formatarParaSQL();

                    \$this->insert(true);
                    \$this->selectUltimoRegistroInserido();

                    $strActionAdd

                    }

                return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\");

            }

            public function __actionEdit(){

                \$mensagemSucesso = \"{$mensagemEdicaoSucesso}\";
                \$numeroRegistros = Helper::POST(\"numeroRegs\");

                \$urlSuccess = Helper::getUrlAction(Helper::POST(\"next_action\"), Helper::POST(\"{$key}\"));
                \$urlErro = Helper::getUrlAction(Helper::POST(\"origin_action\"), Helper::POST(\"{$key}\"));

                for(\$i=1; \$i <= \$numeroRegistros; \$i++){

                    \$this->setByPost(\$i);
                    \$this->formatarParaSQL();

                    \$this->update(\$this->get{$keyUpper}(), \$_POST, \$i, true);

                    \$this->select(\$this->get{$keyUpper}());

                    {$strActionEdit}
                    {$conteudoAjaxRelacionamento}

                }

                return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\");

            }

            public function __actionRemove(){

                \$mensagemSucesso = \"$mensagemExclusaoSucesso\";

                \$urlSuccess = Helper::getUrlAction(\"list_$table\", Helper::GET(\"$key\"));
                \$urlErro = Helper::getUrlAction(\"list_$table\", Helper::GET(\"$key\"));

                \$registroRemover = Helper::GET(\"$key\");

                \$this->delete(\"\$registroRemover\", true);

                $strActionRemove

                return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\", \$registroRemover);

            }

            $strUploadFile


        // **********************
        // MÉTODOS GETTER's
        // **********************

        ";
        // GETTER
        $database->query($sql);
        $result = $database->result;
        while ($row = mysqli_fetch_row($result))
        {

        $col=$row[0];
        $colUpper = ucfirst($col);

            if(substr_count($row[0], "_DATE") > 0){

                $timestamp4 = "get" . $colUpper . "_UNIX()";
                $timestamp5 = "$" . "this->" . $col . "_UNIX";

        $c.="
        function $timestamp4
        {
        \treturn $timestamp5;
        }
        ";

            }

        $mname = "get" . $colUpper . "()";
        $mthis = "$" . "this->" . $col;
        $c.="
        public function $mname
        {
        \treturn $mthis;
        }
        ";
        }


        $c.="
        // **********************
        // MÉTODOS SETTER's
        // **********************

        ";
        // SETTER
        $database->query($sql);
        $result = $database->result;

        while ($row = mysqli_fetch_row($result))
        {

            $col=$row[0];
            $colUpper = ucfirst($col);

            if(Helper::endsWith($col, "_normalizado")){
                    $varNormalizada =  str_replace("_normalizado", "", $col);
                    $varUpperNormalizada = ucfirst($varNormalizada);

                    $val = "$" . "var";
                    $mname = "set" . $colUpper . "()";
                    $mValorNormalizada = "\$var = Helper::retiraAcento(\$this->get$varUpperNormalizada())";
                    $mthis = "$" . "this->" . $col . " = ";
                    $c.="
                    function $mname
                    {
                    \t$mValorNormalizada;
                    \t$mthis $val;
                    }
                    ";
            } else{
                    $val = "$" . "val";
                    $mname = "set" . $colUpper . "($" . "val)";
                    $mthis = "$" . "this->" . $col . " = ";
                    $c.="
                    function $mname
                    {
                    \t$mthis $val;
                    }
                    ";
            }





        if(substr_count($row[0], "_DATE") > 0){

        $timestamp .=  "UNIX_TIMESTAMP($col) AS $col" . "_UNIX, ";

        }

        }

        if(strlen($timestamp) > 0)
            $timestamp = ", " . substr($timestamp, 0, strlen($timestamp) -2);
        else
            $timestamp = "";

        $sql = "\$sql = ";
        $id = "\$id";
        $thisdb = "\$this->database";
        $thisdbquery = "\$msg = \$this->database->queryMensagem(\$sql)";
        
        $result = "\$result = ";
        $row = "\$row";
        $result1 = "\$result";
        $res = "\$result = \$this->database->result;";

        $strSetParameterCorporacao = "";
        $strParametroEntradaCorporacao = "";
        $strParametroSaidaCorporacao ="";
        $strWhereSelect = "";
        
        if($objPadronizadorDatabase->isCorporacaoExistenteNaTabela($table)){
            $strParametroEntradaCorporacao = ", \$idCorporacao = null";
            $strParametroSaidaCorporacao = ", \$idCorporacao";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }";
            $strWhereSelect .= " AND corporacao_id_INT = \".\$idCorporacao.\"";
        }
        
        
        $c.="
        
        // **********************
        // SELECT
        // **********************

        function select($id $strParametroEntradaCorporacao)
        {
        \$this->id = '$id';
        \t$strSetParameterCorporacao
        \t$sql \"SELECT * $timestamp FROM $table WHERE $key = $id $strWhereSelect;\";
        \t$thisdbquery;
        \t$res
            
        \tif(\$msg != null && \$msg->erro() ){ 
            \t\t\$this->clear();
            \t\treturn \$msg;
        \t}
        
        \t\$row = \$this->database->fetchObject($result1);
        \tif(\$row == null) return false;
        ";
        $strFunctionEmpty = "";
        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;
        $strBodyEmptyFunction = "";
        while ($row = mysqli_fetch_row($result))
        {

            $col=$row[0];

            $cthis = "$" . "this->" . $col . " = $" . "row->" . $col;

            $strBodyEmptyFunction .= "\t\t\$this->{$col}  = null;\n";

            
            $c.="
            $cthis;
            ";

            if(substr_count($row[0], "_DATE") > 0){

                $timestamp2 = "\$this->" . $col . "_UNIX = $" . "row->" . $col . "_UNIX;";

                $c.="$timestamp2\n";

            }
            else if(substr_count($row[0], "_id_")){

                if(substr_count($row[0], "_id_value")){
                    $c.="\$this->get{$colUpper}();\n";
                }
                else{
                    $nomeAtributo = substr($col, 0, strrpos($col, "_id_"));

                    $strBodyEmptyFunction .= "\t\t\$obj" . ucfirst($nomeAtributo)."= null;\n";
                }

            }

        }

        $c.="
            return \$msg;
        }
        ";
        
        $c .=  "
        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $strBodyEmptyFunction
        }
        ";
        
        $queryWhereDelete = "";
        $strInsertTuplaSincronizador = "";
        $strSetParameterCorporacao = "";
        $strParametroEntradaCorporacao = "";

        if($objPadronizadorDatabase->isCorporacaoExistenteNaTabela($table)){
            $strParametroEntradaCorporacao = ", \$idCorporacao = null, \$idUsuarioOperacao = null";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if(\$idUsuarioOperacao == null){
                \$idUsuarioOperacao = Seguranca::getId();
            }";
            $queryWhereDelete = " AND corporacao_id_INT = \$idCorporacao \" ";
            $strInsertTuplaSincronizador = "
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                \$this->idSistemaTabela,
                                \$idUsuarioOperacao,
                                \$idCorporacao,
                                '0',
                                DatabaseSincronizador::\$INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                \$id,
                                \$this->database);";
        }else{
            
             $strParametroEntradaCorporacao = ", \$idCorporacao = null, \$idUsuarioOperacao = null";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if(\$idUsuarioOperacao == null){
                \$idUsuarioOperacao = Seguranca::getId();
            }";
            $queryWhereDelete = " \" ";
            $strInsertTuplaSincronizador = "
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                \$this->idSistemaTabela,
                                \$idUsuarioOperacao,
                                \$idCorporacao,
                                '0',
                                DatabaseSincronizador::\$INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                \$id,
                                \$this->database);";
        }



        $zeile1 = "\$sql  = \"DELETE FROM $table WHERE $key = $id $queryWhereDelete";
        $zeile2 = "\$msg = \$this->database->queryMensagem(\$sql);";
        $c.="

        // **********************
        // DELETE
        // **********************

        public function delete($id, \$sincronizar = false $strParametroEntradaCorporacao)
        {
        $strSetParameterCorporacao
        $zeile1;
        $zeile2
            if(\$msg == null
                    && \$sincronizar
                    && strlen(\$this->idSistemaTabela)
                    && strlen(\$id)){
                $strInsertTuplaSincronizador
            }
            return \$msg;
        ";

        $c.="
        }
        ";

        $zeile1 = "$" . "this->$key = \"\"";
        
        $zeile5= ")";
        $strAtributos = "";
        $strValores = "";
        
        
        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;

        while ($row = mysqli_fetch_row($result))
        {
            $col=$row[0];

            if($col!=$key || ($col==$key && $autoIncremento== "FALSE"))
            {
                $strAtributos.= "$col,";
                if($col == "corporacao_id_INT"){
                    $strValores .= "\$idCorporacao,";
                }
                elseif(strpos($col, "dataCadastro") !== false || strpos($col, "dataEdicao") !== false ){

                    $strValores.= "NOW(),";

                }
                elseif(strpos($col, "_BOOLEAN") !== false || strpos($col, "_FLOAT") !== false || strpos($col, "_INT") !== false || strpos($col, "_TIME") !== false || strpos($col, "_DATETIME") !== false || strpos($col, "_DATE") !== false){

                    $strValores.= "\$this->$col" . ",";

                }
                else{

                    $strValores.= "\$this->$col" . ",";

                }
            }
        }

        if($autoIncremento == "TRUE")
            $limparChave = $zeile1 . "; //limpar chave com autoincremento";
        else
            $limparChave = "";

        $strAtributos = substr($strAtributos, 0, -1);
        $strValores = substr($strValores, 0, -1);
        $sql = "\$sql =";
        $zeile7 = "\$msg = \$this->database->queryMensagem(\$sql);";
        
        $zeile8 = "\$row";
        $zeile9 = "\$result";
        //$zeile10 = "$" . "this->$key = " . "mysqli_insert_id($" . "this->database->link);";
        $strParametroEntradaCorporacao = "";
        $strSetParameterCorporacao = "";
        $objPadronizadorDatabase = new Padronizador_Database($database);
        $strInsertTuplaSincronizador = "";
        if($objPadronizadorDatabase->isCorporacaoExistenteNaTabela($table)){
            $strParametroEntradaCorporacao = ", \$idCorporacao = null, \$idUsuarioOperacao = null";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if(\$idUsuarioOperacao == null){
                \$idUsuarioOperacao = Seguranca::getId();
            }";

            $strInsertTuplaSincronizador = "
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                \$this->idSistemaTabela,
                                \$idUsuarioOperacao,
                                \$idCorporacao,
                                '0',
                                DatabaseSincronizador::\$INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                \$this->id,
                                \$this->database);";
        }else{
            $where .= "\"";
             $strParametroEntradaCorporacao = ", \$idCorporacao = null, \$idUsuarioOperacao = null";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if(\$idUsuarioOperacao == null){
                \$idUsuarioOperacao = Seguranca::getId();
            }";

            $strInsertTuplaSincronizador = "
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                \$this->idSistemaTabela,
                                \$idUsuarioOperacao,
                                \$idCorporacao,
                                '0',
                                DatabaseSincronizador::\$INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                \$this->id,
                                \$this->database);";
        }
        
//         if(!is_numeric($this->id)){
//                $this->id = "null";
//                if($opcoes == null) $opcoes = $this->opcoesDAO;        
//                if($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false){
//                    $this->id = EXTDAO_Sistema_sequencia::gerarId($this->nomeTabela);
//                }
//            }
//            
        $inicializaOpcoes = "if(\$opcoes == null) \$opcoes = \$this->opcoesDAO;";
        
        
        
        $strParametroOpcoes = ", \$opcoes = null";
//        if($autoIncremento== "FALSE"){
        
        
        
        $sqlInsert= "\"INSERT INTO $table (id, $strAtributos ) VALUES (\$this->id, $strValores)\"";
        $c.="
        // **********************
        // INSERT
        // **********************

        public function insert(\$sincronizar = false $strParametroEntradaCorporacao $strParametroOpcoes)
        {
            $strSetParameterCorporacao
            
            $inicializaOpcoes        
                
            if(!is_numeric(\$this->id)){\n
                if(\$opcoes == null || \$opcoes[Database::OPCAO_GERAR_ID] != false){\n
                    \$this->id = EXTDAO_Sistema_sequencia::gerarId(\$this->nomeTabela);\n
                    \$msg = \$this->database->queryMensagem(self::getSQLInsert(\$idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    if(\$msg != null && \$msg->erro())
                        return \$msg;
                    else if(\$msg == null){
                        \$msg = new Mensagem_token();
                        \$msg->mValor = \$this->id;
                    }
                }else {
                    \$this->id = null;\n
                    \$msg = \$this->database->queryMensagem(self::getSQLInsert(\$idCorporacao));
                    if(\$msg != null && \$msg->erro())
                        return \$msg;
                    else if(\$msg == null){
                        \$this->id = \$this->database->getLastInsertId();
                        \$msg = new Mensagem_token();
                        \$msg->mValor = \$this->id;
                    }
                }
            } else {
                \$msg = \$this->database->queryMensagem(self::getSQLInsert(\$idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if(\$msg != null && \$msg->erro())
                        return \$msg;
                else if(\$msg == null){
                   \$msg = new Mensagem_token();
                   \$msg->mValor = \$this->id;
                }
            }
            if(\$msg == null || \$msg->ok()){
                
                if(\$sincronizar
                    && strlen(\$this->idSistemaTabela)
                    && strlen(\$this->id)){
                    $strInsertTuplaSincronizador 
                \t}
            } else \$this->id = null;
            return \$msg;
        }
        public function getSQLInsert(\$idCorporacao){
            return $sqlInsert;
        }

        ";



        //funcao para formatar parametros com valores para gravar no banco.
        $sql = "SHOW COLUMNS FROM $table;";
        
        $database->query($sql);
        $result = $database->result;
        while ($row = mysqli_fetch_row($result))
        {
            $col=$row[0];
            
            $colUpper = ucfirst($col);

            $acumuladorGetNome .= "\tpublic function nomeCampo$colUpper(){ \n\n";
            $acumuladorGetNome .= "\t\treturn \"$col\";\n\n";
            $acumuladorGetNome .= "\t}\n\n";

            if($col!=$key)
            {
                $strAtributos.= "$col" . ",";

                $acumuladorFunCampo .= "\tpublic function imprimirCampo$colUpper(\$objArguments){\n\n";
                $acumuladorFunCampo .= "\t\t\$objArguments->nome = \"$col\";\n";
                $acumuladorFunCampo .= "\t\t\$objArguments->id = \"$col\";\n\n";

                if(Helper::endsWith($col, "_normalizado")){
                        $varNormalizada = str_replace("_normalizado", "", $col);
                        $varUpperNormalizada = ucfirst($varNormalizada);
                        $mValorNormalizada = "\$var = Helper::retiraAcentoParaComandoSQL(\$this->get$varUpperNormalizada());";

                        $acumulador .= "\t\t$mValorNormalizada\n";
                        $acumulador .= "\t\t\$this->{$col} = \$this->formatarDadosParaSQL(\$var);\n";

                        $acumuladorFunCampo .= "\t\treturn \$this->campoTexto(\$objArguments);\n\n";

                } elseif($col == "corporacao_id_INT"){
                    $acumulador .= "\t\t\$vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();\n";
                    $acumulador .= "\t\t\$this->{$col} = \$this->formatarDadosParaSQL(\$vIdCorporacao);\n";

                    $acumuladorFunCampo .= "\t\treturn \$this->campoInteiro(\$objArguments);\n\n";
                }
                elseif(strpos($col, "_DATETIME") !== false || $col == "dataCadastro" || $col == "dataEdicao"){

                    $acumulador.= "\t\$this->$col = \$this->formatarDataTimeParaComandoSQL(\$this->$col); \n";
                    $acumulador2.= "\t\$this->$col = \$this->formatarDataTimeParaExibicao(\$this->$col); \n";

                    $acumuladorFunCampo .= "\t\treturn \$this->campoDataTime(\$objArguments);\n\n";


                }
                elseif(strpos($col, "_TIME") !== false){

                    $acumulador.= "\t\$this->$col = \$this->formatarHoraParaComandoSQL(\$this->$col); \n";
                    $acumulador2.= "\t\$this->$col = \$this->formatarHoraParaExibicao(\$this->$col); \n";

                    $acumuladorFunCampo .= "\t\treturn \$this->campoHora(\$objArguments);\n\n";

                }
                elseif(strpos($col, "_HTML") !== false){

                    $acumulador .= "\$this->{$col} = htmlentities(\$this->{$col}, ENT_QUOTES);\n";
                    $acumulador2 .= "\t\$this->{$col} = html_entity_decode(\$this->{$col}); \n";

                }
                elseif(strpos($col, "_DATE") !== false){

                    $acumulador.= "\t\$this->$col = \$this->formatarDataParaComandoSQL(\$this->$col); \n";
                    $acumulador2.= "\t\$this->$col = \$this->formatarDataParaExibicao(\$this->$col); \n";

                    $acumuladorFunCampo .= "\t\treturn \$this->campoData(\$objArguments);\n\n";

                }
                elseif(strpos($col, "_FLOAT") !== false){

//                        $preOperacao .= "\tif(\$this->get$col() == \"\"){
//
//        \t\t\$this->$col = \"null\";
//
//        \t}
//        \n";

                        $acumulador.= "\t\$this->$col = \$this->formatarFloatParaComandoSQL(\$this->$col); \n";
                        $acumulador2.= "\t\$this->$col = \$this->formatarFloatParaExibicao(\$this->$col); \n";

                        $acumuladorFunCampo .= "\t\treturn \$this->campoMoeda(\$objArguments);\n\n";

                }
                elseif(strpos($col, "_INT") !== false){

//                    $preOperacao .= "\t\tif(\$this->$col == \"\"){
//
//        \t\t\t\$this->$col = \"null\";
//
//        \t\t}
//        \n";

                    $acumuladorFunCampo .= "\t\treturn \$this->campoInteiro(\$objArguments);\n\n";
                    $acumulador.= "\t\$this->$col = \$this->formatarIntegerParaComandoSQL(\$this->$col); \n";
                    

                }
                elseif(strpos($col, "_BOOLEAN") !== false){

//                    $preOperacao .= "\t\tif(\$this->$col == \"\"){
//
//        \t\t\t\$this->$col = \"null\";
//
//        \t\t}
//        \n";

                    $acumuladorFunCampo .= "\t\treturn \$this->campoBoolean(\$objArguments);\n\n";
                    $acumulador.= "\t\$this->$col = \$this->formatarBooleanParaComandoSQL(\$this->$col); \n";

                }else {
                    if(strpos($col, "_IMAGEM") !== false){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoImagem(\$objArguments);\n\n";

                    }
                    elseif(strpos($col, "_ARQUIVO") !== false){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoArquivo(\$objArguments);\n\n";

                    }
                    elseif(strpos(Helper::strtolowerlatin1($col), "cpf") !== false){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoCPF(\$objArguments);\n\n";

                    }
                    elseif(strpos(Helper::strtolowerlatin1($col), "cnpj") !== false){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoCNPJ(\$objArguments);\n\n";

                    }
                    elseif(strpos(Helper::strtolowerlatin1($col), "cep") !== false){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoCep(\$objArguments);\n\n";

                    }
                    elseif(strpos(Helper::strtolowerlatin1($col), "email") !== false){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoEmail(\$objArguments);\n\n";

                    }
                    elseif(strpos(Helper::strtolowerlatin1($col), "telefone") !== false || strpos(Helper::strtolowerlatin1($col), "celular") !== false
                            || substr(Helper::strtolowerlatin1($col), strlen($col)-3, strlen($col)) == "tel"){

                        $acumuladorFunCampo .= "\t\treturn \$this->campoTelefone(\$objArguments);\n\n";

                    }
                    else{
                        $acumuladorFunCampo .= "\t\treturn \$this->campoTexto(\$objArguments);\n\n";
                    }
                    $acumulador .= "\t\t\$this->{$col} = \$this->formatarDadosParaSQL(\$this->{$col});\n";
                }
                

                $acumuladorFunCampo .= "\t}\n\n";

        }

    }


        $c .= "

        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

    $acumuladorGetNome


        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

    $acumuladorFunCampo


        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL(){

    $preOperacao

    $acumulador

        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
        //****************************************************************************

        public function formatarParaExibicao(){

    $acumulador2

        }

        ";




        //SETS POR POST, GET E SESSION

        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;
        while ($row = mysqli_fetch_row($result))
        {
            if(Helper::endsWith($row[0], "_normalizado")){
                    $varNormalizada =  str_replace("_normalizado", "", $row[0]);
                    $varUpperNormalizada = ucfirst($varNormalizada);
                    $mValorNormalizada = "\$var = Helper::retiraAcento(\$this->get$varUpperNormalizada())";

                    $camposPost .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::retiraAcento(\$this->$varNormalizada)); \n";
                    $camposGet .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::retiraAcento(\$this->$varNormalizada)); \n";
                    $camposSession .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::retiraAcento(\$this->$varNormalizada)); \n";
            }
            else if($row[0] == "corporacao_id_INT"){
                    $camposPost .= "\t\t\$this->" . $row[0] . " = Seguranca::getIdDaCorporacaoLogada(); \n";
                    $camposGet .= "\t\t\$this->" . $row[0] . " = Seguranca::getIdDaCorporacaoLogada(); \n";
                    $camposSession .= "\t\t\$this->" . $row[0] . " = Seguranca::getIdDaCorporacaoLogada(); \n";
            } else if($row[0] == "email"){
                $camposPost .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::strtolowerlatin1(\$_POST[\"" . $row[0] . "{\$numReg}\"]" . ")); \n";

                $camposGet .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::strtolowerlatin1(\$_GET[\"" . $row[0] . "{\$numReg}\"]" . ")); \n";

                $camposSession .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::strtolowerlatin1(\$_SESSION[\"" . $row[0] . "{\$numReg}\"]" . ")); \n";

                $camposCriarSession .= "\t\t\$_SESSION[\"" . $row[0] . "\"] = Helper::strtolowerlatin1(\$this->" . $row[0] . "); \n";

                $camposLimparSession .= "\t\tunset(\$_SESSION[\"" . $row[0] . "\"]);\n";
            } else if(Helper::endsWith ($row[0], "INT") || 
                    Helper::endsWith ($row[0], "_DATE") ||
                    Helper::endsWith ($row[0], "_DATETIME") ||
                    Helper::endsWith ($row[0], "_TIME")){
                $camposPost .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(\$_POST[\"" . $row[0] . "{\$numReg}\"]" . "); \n";

                $camposGet .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(\$_GET[\"" . $row[0] . "{\$numReg}\"]" . "); \n";

                $camposSession .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(\$_SESSION[\"" . $row[0] . "{\$numReg}\"]" . "); \n";

                $camposCriarSession .= "\t\t\$_SESSION[\"" . $row[0] . "\"] = \$this->" . $row[0] . "; \n";

                $camposLimparSession .= "\t\tunset(\$_SESSION[\"" . $row[0] . "\"]);\n";
            }
            else {
                $camposPost .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::strtoupperlatin1(\$_POST[\"" . $row[0] . "{\$numReg}\"]" . ")); \n";

                $camposGet .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::strtoupperlatin1(\$_GET[\"" . $row[0] . "{\$numReg}\"]" . ")); \n";

                $camposSession .= "\t\t\$this->" . $row[0] . " = \$this->formatarDados(Helper::strtoupperlatin1(\$_SESSION[\"" . $row[0] . "{\$numReg}\"]" . ")); \n";

                $camposCriarSession .= "\t\t\$_SESSION[\"" . $row[0] . "\"] = Helper::strtoupperlatin1(\$this->" . $row[0] . "); \n";

                $camposLimparSession .= "\t\tunset(\$_SESSION[\"" . $row[0] . "\"]);\n";
            }
        }



        $c .="
        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession(){

    $camposCriarSession

        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession(){

    $camposLimparSession

        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession(\$numReg){

    $camposSession

        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost(\$numReg){

    $camposPost

        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet(\$numReg){

    $camposGet

        }
        ";


        // UPDATE ----------------------------------------

        $zeile1 = "$" . "this->$key = \"\"";
        $zeile2 = "UPDATE $table SET";
        $zeile5= ")";
        $strAtributos = "";
        $strValores = "";
        $zeile6 = "VALUES (";

        $upd = "\t\t\$upd=\"\";";

        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;
        while ($row = mysqli_fetch_row($result))
        {

            $col=$row[0];

            if($col!=$key)
            {
                $strAtributos.= "$col" . ",";
                $strValores.= "$" . "this->$col" . ",";

                if(strpos($col, "dataEdicao") !== false ){

                    $condicao .= "\t\t\$upd.= " . "\"" . "$col = NOW(), \";\n\n";

                }

                $condicao .= "\tif(\$this->$col != null || \$tipo == null){\n\n";

                if($col == "corporacao_id_INT"){
                    $condicao .= "\t\t\$upd.= " . "\"" . "$col = \$idCorporacao, \";\n\n";
                }
                else if(strpos($col, "_FLOAT") !== false || strpos($col, "_INT") !== false || strpos($col, "_BOOLEAN") !== false || strpos($col, "_TIME") !== false ||strpos($col, "_DATE") !== false || strpos($col, "_DATETIME") !== false){

                    $condicao .= "\t\t\$upd.= \"$col = \$this->$col, \";\n\n";

                }
                else{

                    $condicao .= "\t\t\$upd.= \"$col = \$this->$col, \";\n\n";

                }

                $condicao .= "\t} \n\n";

            }

        }

        $strAtributos = substr($strAtributos, 0, -1);
        $strValores = substr($strValores, 0, -1);
        $condicao .= "\t\t\$upd = substr(\$upd, 0, -2);";
        $sql = "\$sql = \"";
        
        $zeile10 = "\$this->$key = \$row->$key";
        $id = "\$id";
        $where = "WHERE {$key} = \$id";

        $strParametroEntradaCorporacao = "";
        $strSetParameterCorporacao = "";
        $objPadronizadorDatabase = new Padronizador_Database($database);
        $strInsertTuplaSincronizador = "";
            
        if($objPadronizadorDatabase->isCorporacaoExistenteNaTabela($table)){
            $strParametroEntradaCorporacao = ", \$idCorporacao = null, \$idUsuarioOperacao = null";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if(\$idUsuarioOperacao == null){
                \$idUsuarioOperacao = Seguranca::getId();
            }";
            $where .= " AND corporacao_id_INT = \".\$idCorporacao";
            
           
            
            $strInsertTuplaSincronizador = "
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                \$this->idSistemaTabela,
                                \$idUsuarioOperacao,
                                \$idCorporacao,
                                '0',
                                DatabaseSincronizador::\$INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                \$id,
                                \$this->database);";
        }else{
            $where .= "\"";
             $strParametroEntradaCorporacao = ", \$idCorporacao = null, \$idUsuarioOperacao = null";
            $strSetParameterCorporacao = "
            if(\$idCorporacao == null){
                \$idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if(\$idUsuarioOperacao == null){
                \$idUsuarioOperacao = Seguranca::getId();
            }";
            
            $strInsertTuplaSincronizador = "
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                \$this->idSistemaTabela,
                                \$idUsuarioOperacao,
                                \$idCorporacao,
                                '0',
                                DatabaseSincronizador::\$INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                \$id,
                                \$this->database);";
        }

        $c.="
        // **********************
        // UPDATE
        // **********************

        public function update(\$id, \$tipo = null, \$numReg=1, \$sincronizar = false $strParametroEntradaCorporacao)
        {
            \$upd = \"\";
    $strSetParameterCorporacao
    $condicao

        \t$sql $zeile2 \$upd $where ;

        \t$zeile7;

            if(\$msg == null
                && \$sincronizar
                && strlen(\$this->idSistemaTabela)
                && strlen(\$id)){
                $strInsertTuplaSincronizador
                
            }
            return \$msg;
        ";


        $c.="
        }
        ";

        $c.= "

        } // classe: fim

        ?>";
        fwrite($file, $c);
        fclose($file);


            $strGerouExt = "<p class=\"mensagem_retorno\">
                    &bull;&nbsp;&nbsp;Classe <b>$class</b> gerada com sucesso no arquivo <b>$filename</b>.
                    </p>";

        print $strGerouExt;


        }


    }
}
?>
