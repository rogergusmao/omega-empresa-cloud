<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
class Gerador_script_banco_banco {
    //put your code here
    public $objScriptProjetosVersaoBancoBanco;
    public $objProjetosVersaoBancoBanco;
    public $idPVBB;
    public $pathDiretorio;
    public $pathArquivo;
    
    public $idTipoBanco = null;
    
    public $objBancoBanco;
    public $objBancoHomologacao = -1;
    public $objBancoProducao = -1;
    
    public $dbHomologacao = -1;
    public $dbProducao= -1;
    
    const ARQUIVO_XML_ESTRUTURA = 3;
    const ARQUIVO_SCRIPT_ESTRUTURA = 4;
    const ARQUIVO_SCRIPT_INSERT = 5;
    
    public function __construct($objPVBB, $idTipoScriptBanco = null) {
        
        $this->objProjetosVersaoBancoBanco = $objPVBB;
        $vId = EXTDAO_Script_projetos_versao_banco_banco::getIdScript($objPVBB->getId(), $idTipoScriptBanco);
        if($vId == null){
            $this->objScriptProjetosVersaoBancoBanco = new EXTDAO_Script_projetos_versao_banco_banco();
            $this->objScriptProjetosVersaoBancoBanco->setProjetos_versao_banco_banco_id_INT($objPVBB->getId());
            $this->objScriptProjetosVersaoBancoBanco->setTipo_script_banco_id_INT($idTipoScriptBanco);
            if($idTipoScriptBanco != null 
                && $idTipoScriptBanco == EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_WEB_DO_ANDROID){
                $this->objScriptProjetosVersaoBancoBanco->setIs_atualizacao_BOOLEAN("1");
                $this->idTipoBanco = EXTDAO_Tipo_banco::$TIPO_BANCO_WEB;
            }
            else if($idTipoScriptBanco != null 
                && $idTipoScriptBanco == EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD){
                $this->objScriptProjetosVersaoBancoBanco->setIs_atualizacao_BOOLEAN("1");
            } else {
                $this->objScriptProjetosVersaoBancoBanco->setIs_atualizacao_BOOLEAN("0");
            }
            $this->objScriptProjetosVersaoBancoBanco->setData_criacao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $this->objScriptProjetosVersaoBancoBanco->formatarParaSQL();
            $this->objScriptProjetosVersaoBancoBanco->insert();
            $this->objScriptProjetosVersaoBancoBanco->selectUltimoRegistroInserido();
        } else{
            $this->objScriptProjetosVersaoBancoBanco = new EXTDAO_Script_projetos_versao_banco_banco();
            $this->objScriptProjetosVersaoBancoBanco->select($vId);
            $this->objScriptProjetosVersaoBancoBanco->setData_atualizacao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $this->objScriptProjetosVersaoBancoBanco->formatarParaSQL();
            $this->objScriptProjetosVersaoBancoBanco->update($vId);
        }
        $this->idPVBB = $this->objProjetosVersaoBancoBanco->getId();
        
        $this->inicializaAmbiente($objPVBB);
    }
    public function getDatabaseHomologacao(){
        if(is_numeric($this->dbHomologacao )){
            $this->dbHomologacao = $this->objBancoBanco->getObjBancoHomologacao();    
        }
        return $this->dbHomologacao;
        
            
    }
    public function getDatabaseProducao(){
        if(is_numeric( $this->dbProducao )){
           $this->dbProducao = $this->objBancoBanco->getObjBancoProducao();    
        }
        return $this->dbProducao;
    }
    public function getObjBancoHomologacao(){
        if(is_numeric($this->objBancoHomologacao )) {
            $this->objBancoHomologacao = $this->objBancoBanco->getEXTDAOBancoHomologacao();
        }
        return $this->objBancoHomologacao;
    }
    public function getObjBancoProducao(){
        if(is_numeric($this->objBancoProducao)) {
            $this->objBancoProducao= $this->objBancoBanco->getEXTDAOBancoProducao();
        }
        return $this->objBancoProducao;
    }
    private function inicializaAmbiente($objPVBB){
        $pIdBancoBanco = $this->objProjetosVersaoBancoBanco->getBanco_banco_id_INT();
        
        $this->objBancoBanco = new EXTDAO_Banco_banco();
        $this->objBancoBanco->select($pIdBancoBanco);
        
        if($this->idTipoBanco == null)
            $this->idTipoBanco = $this->objBancoBanco->getTipo_banco_id_INT();
        
        $this->pathDiretorio = $objPVBB->getDiretorioDefault();
     }
    
     public static function getPathArquivo($idPVBB){
        $pathDiretorio = GerenciadorArquivo::getPathDoTipo(
                $idPVBB,
                "projetos_versao_banco_banco",
                GerenciadorArquivo::ID_SCRIPT
                ); 
        
        return $pathDiretorio;
     }
     
     
    public static function getNomeArquivo($tipo){
         $vToken = "";
         switch ($tipo) {
            
            
            case ARQUIVO_XML_ESTRUTURA:
                $vToken = "xml_estrutura";
                $vExtensao = ".xml";
                break;
            case ARQUIVO_SCRIPT_ESTRUTURA :
                $vToken = "script_estrutura";
                $vExtensao = ".sql";
                break;
            case ARQUIVO_SCRIPT_INSERT :
                $vToken = "script_estrutura";
                $vExtensao = ".sql";
                 break;

             default:
                 return null;
         }
         $vToken = "hom_para_prod_{$vToken}{$vExtensao}";
         return $vToken;
     }
     
    public function geraScriptAtualizaTuplas($vTT){
        $db = new Database();
        $vObjTT = new EXTDAO_Tabela_tabela($db);
        $vObjTT->select($vTT);


        $vIdTabelaHomologacao = $vObjTT->getHomologacao_tabela_id_INT();
        $vIdTabelaProducao = $vObjTT->getProducao_tabela_id_INT();
        $contador = new Contador();
        $vErro = false;
        if($vIdTabelaProducao == null && $vIdTabelaHomologacao == null){
            Helper::imprimirMensagem ("N�o existem tabelas no relacionamento: {$vObjTT->getId()}.", MENSAGEM_ERRO);
            $vErro = true;
        }
        if($vErro) 
            exit();

        switch ($this->objBancoBanco->getTipo_banco_id_INT()) {
            case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                $vGeradorTabela = new Gerador_script_registros(
                        $this->objProjetosVersaoBancoBanco->getBanco_banco_id_INT(),
                        $vObjTT,
                        $this->getIdScriptProjetosVersaoBancoBanco(), 
                        $contador,
                        $db);
                $vGeradorTabela->getScriptRegistros($db);
                
                break;
            case EXTDAO_Tipo_banco::$TIPO_BANCO_WEB:
                $vGeradorTabela = new Gerador_script_registros(
                        $this->objProjetosVersaoBancoBanco->getBanco_banco_id_INT(),
                        $vObjTT,
                        $this->getIdScriptProjetosVersaoBancoBanco(), 
                        $contador,
                        $db);
                $vGeradorTabela->getScriptRegistros($db);
                
                break;
            default:
                break;
        }

    }
    
    
    
    public function geraScriptAtualizaTuplasSalvaArquivo(){
        $vStrScript = "";
        $vNomeArquivo = Gerador_script_banco_banco::getNomeArquivo(ARQUIVO_SCRIPT_INSERT);
        $vPathArquivo = $this->pathDiretorio.$vNomeArquivo;
        $contador = new Contador();
        $vArquivo = fopen($vPathArquivo, "w+");
        $vVetorTabelaTabela = $this->objProjetosVersaoBancoBanco->getVetorTabelaTabelaDaHomologacao();
        if($vVetorTabelaTabela != null)
        foreach ($vVetorTabelaTabela as $vTT) {
            $vObjTT = new EXTDAO_Tabela_tabela();
            $vObjTT->select($vTT);
           
            
            $vIdTabelaHomologacao = $vObjTT->getHomologacao_tabela_id_INT();
            $vIdTabelaProducao = $vObjTT->getProducao_tabela_id_INT();
            
            $vErro = false;
            if($vIdTabelaProducao == null && $vIdTabelaHomologacao == null){
                Helper::imprimirMensagem ("N�o existem tabelas no relacionamento: {$vObjTT->getId()}.", MENSAGEM_ERRO);
                $vErro = true;
            }
            if($vErro) 
                exit();

            switch ($this->idTipoBanco) {
                case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                    $vGeradorTabela = new Gerador_script_registros(
                            $this->objProjetosVersaoBancoBanco->getBanco_banco_id_INT(),
                            $vObjTT,
                            $this->getIdScriptProjetosVersaoBancoBanco(), 
                            $contador
                            );
                    $vStrScript = $vGeradorTabela->getScriptRegistros();
                    fwrite($vArquivo, $vStrScript);
                    break;
                case EXTDAO_Tipo_banco::$TIPO_BANCO_WEB:
                    $vGeradorTabela = new Gerador_script_registros(
                            $this->objProjetosVersaoBancoBanco->getBanco_banco_id_INT(),
                            $vObjTT,
                            $this->getIdScriptProjetosVersaoBancoBanco(), 
                            $contador
                            );
                    $vStrScript = $vGeradorTabela->getScriptRegistros();
                    fwrite($vArquivo, $vStrScript);
                    break;
                default:
                    break;
            }
            
            fflush($vArquivo);
        }
        
        fclose($vArquivo);
        
        EXTDAO_Script_projetos_versao_banco_banco::criaArquivoEstruturaXML($this->objScriptProjetosVersaoBancoBanco->getId(), $vPathArquivo.".xml");
        Helper::imprimirMensagem("Arquivo gerado com sucesso no caminho: ".$vPathArquivo);
        
    }
    
    public function getListaIdScriptComandoBancoEstrutura($db = null){
       return EXTDAO_Script_projetos_versao_banco_banco::getListaIdScriptComandoBanco(
           $this->objScriptProjetosVersaoBancoBanco->getId(), 
           array(
               EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE, 
               EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT, 
               EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT, 
               EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION,
               EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE),
           $db
       );
    }
    
    public function getListaIdScriptComandoBancoRegistros(){
       return EXTDAO_Script_projetos_versao_banco_banco::getListaIdScriptComandoBanco($this->objScriptProjetosVersaoBancoBanco->getId(), array(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE, EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT, EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT, EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_ATUALIZACAO_REGISTRO_HOM_PARA_PROD));
    }
    
    public function conectaBancoProducao(){
        $this->dbBancoProducao = EXTDAO_Script_projetos_versao_banco_banco::getObjDatabaseDoBancoProducao($this->objScriptProjetosVersaoBancoBanco->getId());
        $this->dbBancoProducao->query("SET FOREIGN_KEY_CHECKS=0");
    }
        
    public function executaComandoNoBancoDeProducao($idSCB, $vConsulta){
        //lembre de conectar o banco antes de executar um comando

        $vErro = false;

        try {
            
            $this->dbBancoProducao->query($vConsulta);
        } catch (Exception $exc) {
            Helper::imprimirMensagem($exc->getMessage(), MENSAGEM_ERRO);
            $objPVLE = new EXTDAO_Projetos_versao_log_erro();
            $objPVLE->setClasse("Gerador_script_banco_banco");
            $objPVLE->setFuncao("executaComandoNoBancoDeProducao");
            $objPVLE->setDescricao_HTML($exc->getMessage());
            $objPVLE->setUsuario_id_INT(Seguranca::getId());
            $objPVLE->setProjetos_tipo_log_erro_id_INT(EXTDAO_Projetos_tipo_log_erro::ATUALIZA_BANCO_DE_DADOS_PRODUCAO);
            $objPVLE->setProjetos_versao_banco_banco_id_INT($this->objProjetosVersaoBancoBanco->getId());
            $objPVLE->setData_ocorrida_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objPVLE->formatarParaSQL();
            $objPVLE->insert();
            $objPVLE->selectUltimoRegistroInserido();
            
            $obj = new EXTDAO_Projetos_versao_log_erro_script();
            $obj->setScript_comando_banco_id_INT($idSCB);
            $obj->setProjetos_versao_log_erro_id_INT($objPVLE->getId());
            $obj->formatarParaSQL();
            $obj->insert();
            
            $vErro = true;
        }
        return !$vErro;
    }
    
    public function atualizaBancoHomologacao(){
        EXTDAO_Script_projetos_versao_banco_banco::atualizaBancoHomologacao($this->objScriptProjetosVersaoBancoBanco->getId());
    }
  
    public function getIdScriptProjetosVersaoBancoBanco(){
        return $this->objScriptProjetosVersaoBancoBanco->getId();
    }
    
    public function apagaScript(){
        $vVetorIdTabelaTabela = EXTDAO_Script_tabela_tabela::getListaId($this->objScriptProjetosVersaoBancoBanco->getId());
            if($vVetorIdTabelaTabela != null){
                $vObjTT = new EXTDAO_Script_tabela_tabela();
                
                foreach ($vVetorIdTabelaTabela as $vIdTabelaTabela) 
                    $vObjTT->delete($vIdTabelaTabela);
            }
    }
    
    
    public function geraScriptAtualizaEstruturaTabela(
        $vTT, $prefixoNomeTabela = "", $tipoBanco = null){
        
        $objTT = new EXTDAO_Tabela_tabela();
        $objTT->select($vTT);

        $vTipoAtualizacao = $objTT->getTipo_operacao_atualizacao_banco_id_INT();
        $vIdTabelaHomologacao = $objTT->getHomologacao_tabela_id_INT();
        $vIdTabelaProducao = $objTT->getProducao_tabela_id_INT();

        $vErro = false;
        if($vIdTabelaHomologacao == null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
            Helper::imprimirMensagem ("A tabela de homologa��o � nula e o tipo de opera��o de atualiza��o do banco est� como EDI��O. Id: {$pObjTabelaTabela->getId()}", MENSAGEM_ERRO);
            $vErro = true;
        }
        else if($vIdTabelaProducao != null && $vIdTabelaHomologacao == null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){

            Helper::imprimirMensagem ("A tabela de produ��o � nula e o tipo de opera��o de atualiza��o do banco est� como INSER��O. Id: {$pObjTabelaTabela->getId()}", MENSAGEM_ERRO);
            $vErro = true;
        }
        else if($vIdTabelaProducao == null && $vIdTabelaHomologacao != null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
            Helper::imprimirMensagem ("A tabela de homologa��o � nula e o tipo de opera��o de atualiza��o do banco est� como REMO��O. Id: {$pObjTabelaTabela->getId()}", MENSAGEM_ERRO);
            $vErro = true;
        }
        else if($vIdTabelaProducao == null && $vIdTabelaHomologacao == null){
            Helper::imprimirMensagem ("N�o existem tabelas no relacionamento: {$pObjTabelaTabela->getId()}.", MENSAGEM_ERRO);
            $vErro = true;
        }
        if($vErro) 
            exit();
        if($tipoBanco == null)
            $tipoBanco = $this->objBancoBanco->getTipo_banco_id_INT();
        switch ($tipoBanco) {
            case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                $vGeradorTabela = new Gerador_script_android_tabela(
                        $this->getIdScriptProjetosVersaoBancoBanco(), 
                        $objTT);
                $vGeradorTabela->setPrefixoTabela($prefixoNomeTabela);
                $vGeradorTabela->getScriptTabela();
                
                break;
            case EXTDAO_Tipo_banco::$TIPO_BANCO_WEB:
                $vGeradorTabela = new Gerador_script_web_tabela(
                        $this->getIdScriptProjetosVersaoBancoBanco(), 
                        $objTT
                );
                $vGeradorTabela->setPrefixoTabela($prefixoNomeTabela);
                $vGeradorTabela->getScriptTabela();
                
                break;
            default:
                break;
        }
    }
    
    public function geraScriptAtualizaEstruturaHomParaProd($tipoBanco = null){
        $this->apagaScript();
        $vPathArquivo = $this->pathDiretorio.Gerador_script_banco_banco::getNomeArquivo(ARQUIVO_SCRIPT_ESTRUTURA);
        $vArquivo = fopen($vPathArquivo, "w+");
        
        $vVetorTabelaTabela = $this->objScriptProjetosVersaoBancoBanco->getVetorTabelaTabela();
        if($vVetorTabelaTabela != null)
        foreach ($vVetorTabelaTabela as $vTT) {
            $vObjTT = new EXTDAO_Tabela_tabela();
            $vObjTT->select($vTT);
           
            $vTipoAtualizacao = $vObjTT->getTipo_operacao_atualizacao_banco_id_INT();
            $vIdTabelaHomologacao = $vObjTT->getHomologacao_tabela_id_INT();
            $vIdTabelaProducao = $vObjTT->getProducao_tabela_id_INT();
            
            $vErro = false;
            if($vIdTabelaHomologacao == null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
                Helper::imprimirMensagem ("A tabela de homologa��o � nula e o tipo de opera��o de atualiza��o do banco est� como EDI��O. Id: {$pObjTabelaTabela->getId()}", MENSAGEM_ERRO);
                $vErro = true;
            }
            else if($vIdTabelaProducao != null && $vIdTabelaHomologacao == null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){

                Helper::imprimirMensagem ("A tabela de produ��o � nula e o tipo de opera��o de atualiza��o do banco est� como INSER��O. Id: {$pObjTabelaTabela->getId()}", MENSAGEM_ERRO);
                $vErro = true;
            }
            else if($vIdTabelaProducao == null && $vIdTabelaHomologacao != null && $vTipoAtualizacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
                Helper::imprimirMensagem ("A tabela de homologa��o � nula e o tipo de opera��o de atualiza��o do banco est� como REMO��O. Id: {$pObjTabelaTabela->getId()}", MENSAGEM_ERRO);
                $vErro = true;
            }
            else if($vIdTabelaProducao == null && $vIdTabelaHomologacao == null){
                Helper::imprimirMensagem ("N�o existem tabelas no relacionamento: {$pObjTabelaTabela->getId()}.", MENSAGEM_ERRO);
                $vErro = true;
            }
            if($vErro) 
                exit();
            if($tipoBanco == null)
                $tipoBanco = $this->objBancoBanco->getTipo_banco_id_INT();
            switch ($tipoBanco) {
                case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                    $vGeradorTabela = new Gerador_script_android_tabela();
                    $vStrScript = $vGeradorTabela->getScriptTabela($vObjTT);
                    fwrite($vArquivo, $vStrScript);
                    break;
                case EXTDAO_Tipo_banco::$TIPO_BANCO_WEB:
                    $vGeradorTabela = new Gerador_script_web_tabela(
                            $this->getIdScriptProjetosVersaoBancoBanco(), 
                            $vObjTT
                    );
                    $vStrScript = $vGeradorTabela->getScriptTabela();
                    fwrite($vArquivo, $vStrScript);
                    break;
                default:
                    break;
            }
            
            fflush($vArquivo);
        }
        
        fclose($vArquivo);
    }
    
    public function arquivarXML(){
        $arquivoXML = Gerador_script_banco_banco::getNomeArquivo(ARQUIVO_XML_ESTRUTURA);
       $vPathArquivo = $this->pathDiretorio.$arquivoXML;
        EXTDAO_Script_projetos_versao_banco_banco::criaArquivoEstruturaXML(
                $this->objScriptProjetosVersaoBancoBanco->getId(), 
                $vPathArquivo);
        return $arquivoXML;
        
    }
    
    public function arquivarScriptSQL(){
        $arquivoXML = Gerador_script_banco_banco::getNomeArquivo(ARQUIVO_SCRIPT_ESTRUTURA);
        $pathArquivo =$this->pathDiretorio.$arquivoXML;
        
        EXTDAO_Script_projetos_versao_banco_banco::criaArquivoScriptSQL(
                $this->objScriptProjetosVersaoBancoBanco->getId(), 
                $pathArquivo
        );
        
        return $arquivoXML;
        
    }

    
    
    public function removerForeignKeyDuplicada(
        $dbProdOuHmg, 
        $objBancoProdOuHmg,
        $tabelaProdOuHmg,
        $isProd,
        $prefixoNomeTabela = "", 
        $tipoBanco = null){
        
        if($tipoBanco == null)
            $tipoBanco = $this->objBancoBanco->getTipo_banco_id_INT();
        
        switch ($tipoBanco) {
            case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                Helper::imprimirMensagem("Situacao nao programada asdrwe");
                exit();
                break;
            case EXTDAO_Tipo_banco::$TIPO_BANCO_WEB:
                $geradorTabela = new Gerador_script_web_tabela(
                        $this->getIdScriptProjetosVersaoBancoBanco(), 
                        null
                );
                $geradorTabela->setPrefixoTabela($prefixoNomeTabela);
               
                if($tabelaProdOuHmg != null){
                    $chaves = $objBancoProdOuHmg->getChavesExtrangeirasDuplcadas(
                        $tabelaProdOuHmg, $dbProdOuHmg);
                    
                    if(is_array($chaves)){
                        for($i = 0 ; $i < count($chaves) - 1; $i++){
                            $drop = $geradorTabela->getScriptDropForeignKey(
                                $tabelaProdOuHmg, $chaves[$i]);
//                            echo $drop."</br>";
                            $dbProdOuHmg->Query($drop);
                        }
                    }
                }
                break;
            default:
                break;
        }
        
        
    }
    
}

?>

