<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Script_insert_atributo
 *
 * @author rogerfsg
 */
class Script_update_atributo extends Interface_script_atributo{
    //put your code here
    public $alterarNome = null;
    public $alterarTipo = null;
    
    public function __construct($pAlterarNome = null, $pAlterarTipo= null){
        parent::__construct(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT);
        $this->alterarNome = $pAlterarNome;
        $this->alterarTipo = $pAlterarTipo;
    }
    
    protected function getVetorAtributo() {
        return array($this->alterarNome,$this->alterarTipo );
        
    }
    
}

?>
