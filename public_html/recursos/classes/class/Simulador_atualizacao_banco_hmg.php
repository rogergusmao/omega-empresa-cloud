<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Simulador_atualizacao_banco_hmg
 *
 * @author home
 */
class Simulador_atualizacao_banco_prd {
    //put your code here
    
    public $objPVBB;
    public $objTT;
    public $objTabela ;
    public $objBancoHmg;
    public $objBancoPrd;
    public $idBancoPrd;
    
    public function __construct($objPVBB) {
        //TODO retirar linha abaixo
        $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
        $db = new Database();
        $this->objBancoHmg = EXTDAO_Projetos_versao_banco_banco::getObjBancoHomologacao($objPVBB->getId(), $db);
        $this->objBancoPrd = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($objPVBB->getId(), $db);
        $this->idBancoPrd = $this->objBancoPrd->getId();
        $this->objTT = new EXTDAO_Tabela_tabela();
        $this->objTabela = new EXTDAO_Tabela();
    }
    
    public function atualizaTabelaChave(){
        //TODO continuar
        //EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveInserida($pObjTabelaHomologacao);
        
        //EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveRemovida($pObjTabelaHomologacao);
    }
    
    
    public function atualizaTabelaTabela($idTT){
        $this->objTT->select($idTT);
        switch ($this->objTT->getTipo_operacao_atualizacao_banco_id_INT()){
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                $this->objTabela->select($this->objTT->getHomologacao_tabela_id_INT());
                $this->objTabela->setId(null);
                $this->objTabela->setBanco_id_INT($this->idBancoPrd);
                $this->objTabela->formatarParaSQL();
                $this->objTabela->insert();
                $idTabela =  $this->objTabela->getIdDoUltimoRegistroInserido();
                $this->objTT->setProducao_tabela_id_INT($idTabela);
                $this->objTT->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION);
                break;
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:    
                $this->objTabela->select($this->objTT->getHomologacao_tabela_id_INT());
                $this->objTabela->setId($this->objTT->getProducao_tabela_id_INT());
                $this->objTabela->setBanco_id_INT($this->idBancoPrd);
                $this->objTabela->formatarParaSQL();
                $this->objTabela->update($this->objTT->getProducao_tabela_id_INT());
                $idTabela =  $this->objTT->getProducao_tabela_id_INT();
                $this->objTT->setProducao_tabela_id_INT($idTabela);
                $this->objTT->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION);
                break;
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:    
                $idTabela =  $this->objTT->getProducao_tabela_id_INT();
                $this->objTT->delete($this->objTT->getId());
                $this->objTabela->delete($idTabela);
                break;
        }
    }
}
