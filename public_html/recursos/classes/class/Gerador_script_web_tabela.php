<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador_script_android
 *
 * @author rogerfsg
 */
class Gerador_script_web_tabela extends Gerador_script_tabela {

    private $objAtributo;
    private $objChave;
    private $objSCBC;

    public function __construct($idSPVBB, $pObjTabelaTabela, $db = null) {
        parent::__construct($idSPVBB, $pObjTabelaTabela, new Gerador_script_web_atributo(), $db);
        $this->objAtributo = new EXTDAO_Atributo($this->db);
        $this->objChave = new EXTDAO_Tabela_chave($this->db);
        $this->objSCBC = new EXTDAO_Script_comando_banco_cache($this->db);

    }

    private function getScriptDelete($pObjTabelaProducao) {
        if ($pObjTabelaProducao != null) {
            $vConsulta = "\nDROP TABLE {$pObjTabelaProducao->getNome()};\n";
            $this->insereScriptComandoBanco($vConsulta, EXTDAO_Tipo_comando_banco::DROP_TABLE);
            return $vConsulta;
        }
        else
            echo Helper::imprimirMensagem("Tabela nao encontrada para gerar o script DELETE.");
    }

    private function getScriptCreate($pObjTabelaHomologacao) {
        if ($pObjTabelaHomologacao == null) {

            Helper::imprimirMensagem("Tabela de homologação nao encontrada para gerar o script UPDATE.");

            return;
        }


        $vCabecalhoAtributos = "";
        $vChaves = "";
        $vChaveUnica = "";
        $vChavesExtrangeiras = "";
        $vChavePrimaria = "";

        $vVetorIdAtributoHomologacao = EXTDAO_Tabela::getVetorIdAtributo(
            $pObjTabelaHomologacao->getId(), $this->db);
        $scriptsChave = array();
        if($vVetorIdAtributoHomologacao != null){

            foreach ($vVetorIdAtributoHomologacao as $vIdAtributo) {
                $this->objAtributo->clear();
                $this->objAtributo->select($vIdAtributo);

                $objScript = $this->geradorScriptAtributo->constroiScriptCreate($this->objAtributo);

                $scriptsChave[count($scriptsChave)] = $objScript;

                if (strlen($objScript->mScriptKey))
                    $vChaves .= (strlen($vChaves)) ? ",\n\t{$objScript->mScriptKey}" : "{$objScript->mScriptKey}";

                if (strlen($objScript->mScriptFK))
                    $vChavesExtrangeiras .= (strlen($vChavesExtrangeiras)) ? ",\n\t{$objScript->mScriptFK}" : "\t{$objScript->mScriptFK}";

                if (strlen($objScript->mScriptInsert))
                    $vCabecalhoAtributos .= (strlen($vCabecalhoAtributos)) ? ",\n\t{$objScript->mScriptInsert}" : "\t{$objScript->mScriptInsert}";

                if (strlen($objScript->mIsUnique))
                    $vChaveUnica .= (strlen($vChaveUnica)) ? ", `{$this->objAtributo->getNome()}`" : "\t`{$this->objAtributo->getNome()}`";

                if (strlen($objScript->mIsPrimaryKey))
                    $vChavePrimaria .= (strlen($vChavePrimaria)) ? ", {$this->objAtributo->getNome()}" : "{$this->objAtributo->getNome()}";
            }
        }
        $rs = EXTDAO_Tabela_chave_atributo::getDadosChavesNaoUnica(
            $pObjTabelaHomologacao->getId(), $this->db);
        if($rs != null){
            for ($i = 0 ; $i < count($rs); $i++){
                if(strlen($vChaves))
                    $vChaves .= ", ";
                $vChaves  .= "KEY `{$rs[$i][0]}` (`{$rs[$i][1]}`)";
            }
        }

        $vConsulta = "\nCREATE TABLE {$this->prefixoTabela}{$pObjTabelaHomologacao->getNome()} (\n$vCabecalhoAtributos";
        if (strlen($vChavePrimaria))
            $vConsulta .= ",\n\tPRIMARY KEY($vChavePrimaria)";

        if (strlen($vChaves))
            $vConsulta .= ",\n\t {$vChaves}";

        if (strlen($vChavesExtrangeiras))
            $vConsulta .= ",\n{$vChavesExtrangeiras}";
        //if (strlen($vChaveUnica))
            //$vConsulta .= ",\n\tUNIQUE KEY `{$pObjTabelaHomologacao->getNome()}` ({$vChaveUnica}) USING BTREE";

        $vConsulta .= "\n) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;\n";

        $objSCB = $this->insereScriptComandoBanco($vConsulta, EXTDAO_Tipo_comando_banco::CREATE_TABLE);
        $vetor = array();
        $vetor[count($vetor)] = $vConsulta;
            
        $idSCB = $objSCB->getId();

        for($i = 0 ; $i < count($scriptsChave); $i++){
            $script = $scriptsChave[$i];
            $this->objSCBC->clear();
            if(strlen($script->mNomeKey) && strlen($script->mIdTabelaChaveHmg)){

                $this->objSCBC->setId(null);
                $this->objSCBC->setTabela_chave_id_INT($script->mIdTabelaChaveHmg);
                $this->objSCBC->setAtributo_id_INT(null);
                $this->objSCBC->setNome_chave($script->mNomeKey);
                $this->objSCBC->setScript_comando_banco_id_INT($idSCB);
                $this->objSCBC->formatarParaSQL();
                $this->objSCBC->insert();
            }
            
            if(strlen($script->mNomeFK) && strlen($script->mIdAtributoFkHmg)){
                $this->objSCBC->setId(null);
                $this->objSCBC->setAtributo_id_INT($script->mIdAtributoFkHmg);
                $this->objSCBC->setTabela_chave_id_INT(null);
                $this->objSCBC->setNome_chave($script->mNomeFK);
                $this->objSCBC->setScript_comando_banco_id_INT($idSCB);
                $this->objSCBC->formatarParaSQL();
                $this->objSCBC->insert();
            }
        }
        if($pObjTabelaHomologacao->getId() == 23849){
            print_r($pObjTabelaHomologacao);
            exit();
        }
            
        $idsChaveUnica = EXTDAO_Tabela_chave::getIdsChaveUnicaDaTabela($pObjTabelaHomologacao->getId(), $this->db);
        if(count($idsChaveUnica)){

            for($i = 0; $i < count($idsChaveUnica); $i++){
                $consulta = $this->criaChave($idsChaveUnica[$i], $pObjTabelaHomologacao);
                $vetor[count($vetor)] = $consulta;
            }
        }
        
        return new Script_vetor($vetor);
    }

    private function getScriptDeletaTodaChaveUnica($pObjTabelaProducao) {

        $vObjRetorno = new Script_update_tabela();
        $vVetorChaveUnicaProd = $pObjTabelaProducao->getListaChaveUnica();
//          ALTER TABLE Persons
//          DROP INDEX uc_PersonID
        if($vVetorChaveUnicaProd != null)
        foreach ($vVetorChaveUnicaProd as $vNomeChaveUnica) {
            $vConsultaDeletaChaveUnicaDaProd = "";
            $vConsultaDeletaChaveUnicaDaProd .= "\nALTER TABLE {$this->prefixoTabela}{$pObjTabelaProducao->getNome()}";
            $vConsultaDeletaChaveUnicaDaProd .= "\n\tDROP KEY {$vNomeChaveUnica}\n";
            $vObjRetorno->adicionaScriptDeletarChaveUnica($vConsultaDeletaChaveUnicaDaProd);
            $this->insereScriptComandoBanco(
                $vConsultaDeletaChaveUnicaDaProd
                , EXTDAO_Tipo_comando_banco::DROP_UNIQUE_KEY);
            
        }

        return $vObjRetorno;
    }

    private function getScriptChaveExtrangeira(
        $pNomeTabela, $pAtributo, $pTabelaFK, 
        $pAtributoFK, $pOnUpdate, $pOnDelete,
        $pFkNome) {

//ALTER TABLE `teste` 
//ADD FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`)

        $vConsulta = "";
        $vConsulta .= "\nALTER TABLE {$this->prefixoTabela}{$pNomeTabela}";
        $vConsulta .= "\n\tADD CONSTRAINT $pFkNome FOREIGN KEY (`{$pAtributo}`)\n";
        $vConsulta .= "\tREFERENCES `$pTabelaFK` (`$pAtributoFK`)";
        if (strlen($pOnUpdate))
            $vConsulta .= "\tON UPDATE {$pOnUpdate}";
        if (strlen($pOnDelete))
            $vConsulta .= "\tON DELETE {$pOnDelete}";
        return $vConsulta;
    }

    public function getScriptDeletaChavesSeNecessario($pObjTabelaHomologacao, $pObjTabelaProducao) {

        if ($pObjTabelaProducao == null) {
            Helper::imprimirMensagem("Tabela de produção nao encontrada para gerar o script UPDATE.");
            return null;
        } elseif ($pObjTabelaHomologacao == null && $pObjTabelaProducao == null) {
            Helper::imprimirMensagem("Tabela de homologação nao encontrada para gerar o script UPDATE.");
            return null;
        }

        $vetor = array();
        $vChaves = EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveRemovida($this->idPVBB, $pObjTabelaProducao, $this->db);
        $objChave = new EXTDAO_Tabela_chave();
        if($vChaves != null)
        foreach ($vChaves as $vChaveProdParaDeletar) {
            
            $objChave->select($vChaveProdParaDeletar);
            $vConsultaDeletaChaveUnicaDaProd = "";
            $vConsultaDeletaChaveUnicaDaProd .= "\nALTER TABLE {$this->prefixoTabela}{$pObjTabelaProducao->getNome()}";
            $vConsultaDeletaChaveUnicaDaProd .= "\n\tDROP KEY {$objChave->getNome()}\n";
            $vetor[count($vetor)] = $vConsultaDeletaChaveUnicaDaProd;
            $this->insereScriptComandoBanco($vConsultaDeletaChaveUnicaDaProd, EXTDAO_Tipo_comando_banco::DROP_UNIQUE_KEY);
        }
        return new Script_vetor($vetor);
    }

    public function getScriptCriaChaves($pObjTabelaHomologacao) {
        if ($pObjTabelaHomologacao == null) {
            Helper::imprimirMensagem("Tabela de homologação nao encontrada para gerar o script UPDATE.");
            return;
        }
        $vetorRet = array();
        $vConsultaInsereChaveUnicaDaHom = "";
        $vIdChavesTabelaHomParaInserir = EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveInserida($this->idPVBB, $pObjTabelaHomologacao, $this->db);

        if($vIdChavesTabelaHomParaInserir != null)
        foreach ($vIdChavesTabelaHomParaInserir as $vIdChaveHom) {
            $vConsultaInsereChaveUnicaDaHom = $this->criaChave(
                $vIdChaveHom, 
                $pObjTabelaHomologacao);
            
            $vetorRet[count($vetorRet)] = $vConsultaInsereChaveUnicaDaHom;
        }
        return new Script_vetor($vetorRet);
    }
    
    private function criaChave(
        $vIdChaveHom, 
        $pObjTabelaHomologacao){
        $this->objChave->clear();

        $this->objChave->select($vIdChaveHom);

        $chaveUnica = $this->objChave->getNon_unique_BOOLEAN() == "1" ? true : false;

        $vAtributos = EXTDAO_Tabela_chave_atributo::getListaNomeOrdenadoAtributoDaChave($vIdChaveHom, $this->db);
        $vStrListaAtributo = "";
        if($vAtributos != null)
        foreach ($vAtributos as $vNomeAtributo) {
            $vStrListaAtributo .= strlen($vStrListaAtributo) ? ", {$vNomeAtributo}" : $vNomeAtributo;
        }
        $vConsultaInsereChaveUnicaDaHom = "";

        $vConsultaInsereChaveUnicaDaHom .= "\nALTER TABLE {$this->prefixoTabela}{$pObjTabelaHomologacao->getNome()}";
        if ($chaveUnica) {
//          ALTER TABLE `operadora`
//          ADD INDEX `aonsf` (`nome`) USING BTREE
//            $vNomeChave = Helper::getNomeAleatorio("key_" . substr($pObjTabelaHomologacao->getNome(), 0, 4));
            $vNomeChave = $this->objChave->getNome();
            $vConsultaInsereChaveUnicaDaHom .= " ADD INDEX `{$vNomeChave}` ({$vStrListaAtributo}) USING " . $this->objChave->getIndex_type();
        } else {
            //      ALTER TABLE Persons
            //      ADD CONSTRAINT uc_PersonID UNIQUE (P_Id,LastName)

//            $vNomeChave = Helper::getNomeAleatorio("unique_" . substr($pObjTabelaHomologacao->getNome(), 0, 4));
            $vNomeChave = $this->objChave->getNome();
            $vConsultaInsereChaveUnicaDaHom .= " ADD CONSTRAINT {$vNomeChave} UNIQUE ($vStrListaAtributo)\n";
        }

        $objSCB = $this->insereScriptComandoBanco(
            $vConsultaInsereChaveUnicaDaHom, 
            EXTDAO_Tipo_comando_banco::CREATE_UNIQUE_KEY, 
            $vIdChaveHom,
            $vNomeChave);

        //TODO comentar linha abaixo
        //$objSCB = new EXTDAO_Script_comando_banco();
        $idSCB = $objSCB->getId();
        $this->objSCBC->clear();
        $this->objSCBC->setId(null);
        $this->objSCBC->setScript_comando_banco_id_INT($idSCB);
        $this->objSCBC->setTabela_chave_id_INT($vIdChaveHom);
        $this->objSCBC->setAtributo_id_INT(null);
        $this->objSCBC->setNome_chave($vNomeChave);
        $this->objSCBC->formatarParaSQL();
        $this->objSCBC->insert();
        return $vConsultaInsereChaveUnicaDaHom;
    }

    private function getScriptRenomeaTabela($pObjTabelaHomologacao, $pObjTabelaProducao) {
        if ($pObjTabelaHomologacao == null || $pObjTabelaProducao == null) {
            if ($pObjTabelaHomologacao == null)
                Helper::imprimirMensagem("Tabela de homologação nao encontrada para gerar o script UPDATE.");
            if ($pObjTabelaProducao == null)
                Helper::imprimirMensagem("Tabela de produção nao encontrada para gerar o script UPDATE.");
            return;
        }
        $vObjRetorno = new Script_update_tabela();

        if ($pObjTabelaProducao->getNome() != $pObjTabelaHomologacao->getNome()) {
            $vObjRetorno->mScriptAlteraNome .= "\nALTER TABLE `{$this->prefixoTabela}" . $pObjTabelaProducao->getNome() . "`
             RENAME `{$this->prefixoTabela}" . $pObjTabelaHomologacao->getNome() . "`";
            $this->insereScriptComandoBanco($vObjRetorno->mScriptAlteraNome, EXTDAO_Tipo_comando_banco::RENAME_TABLE);
        }

        return $vObjRetorno;
    }

    public function getScriptDropForeignKey($pNomeTabela, $pNomeChave) {
//        ALTER TABLE `bairro` DROP FOREIGN KEY `fk_770de6800697cfcb`
        return "\nALTER TABLE `{$this->prefixoTabela}$pNomeTabela` DROP FOREIGN KEY `$pNomeChave`";
    }

    private function getScriptDeletaChaveExtrangeira($pObjTabelaHomologacao, $pObjTabelaProducao) {

        $rs = EXTDAO_Tabela_tabela::getResultSetChaveExtrangeiraDeletada($pObjTabelaHomologacao, $pObjTabelaProducao, $this->idPVBB, $this->db);

        $arrayRetorno = array();

        for ($i = 0; $vObj = mysqli_fetch_array($rs, MYSQL_NUM); $i++) {
            $vScriptFK = $this->getScriptDropForeignKey($vObj[0], $vObj[1]);
            $arrayRetorno[count($arrayRetorno)] = $vScriptFK;
            $this->insereScriptComandoBanco($vScriptFK, EXTDAO_Tipo_comando_banco::DROP_FOREIGN_KEY);
        }
        return new Script_vetor($arrayRetorno);
    }

    private function getScriptCriaChaveExtrangeira($pObjTabelaHomologacao, $pObjTabelaProducao) {

        $rs = EXTDAO_Tabela_tabela::getResultSetChaveExtrangeiraCriada($pObjTabelaHomologacao, $pObjTabelaProducao, $this->idPVBB, $this->db);

        $arrayRetorno = array();

        for ($i = 0; $vObj = mysqli_fetch_array($rs, MYSQL_NUM); $i++) {
            $vConsulta = $this->getScriptChaveExtrangeira(
                $vObj[0], $vObj[1], $vObj[2], 
                $vObj[3], $vObj[7], $vObj[9],
                $vObj[12]);
            if (strlen($vConsulta)) {
                $arrayRetorno[count($arrayRetorno)] = $vConsulta;
                $this->insereScriptComandoBanco($vConsulta, EXTDAO_Tipo_comando_banco::CREATE_FOREIGN_KEY);
            }
        }
        return new Script_vetor($arrayRetorno);
    }

    protected function constroiScriptCreate($pObjTabelaHomologacao) {
        //constroiScriptInsert
        $consulta="";
        $vObj = $this->getScriptCreate($pObjTabelaHomologacao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();
        return $consulta;
    }

    protected function constroiScriptDelete($pObjTabelaProducao) {
        $vObj = $this->getScriptDeletaTodaChaveUnica($pObjTabelaProducao);
        $consulta="";
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $vObj = $this->getScriptDeletaChaveExtrangeira(null, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $consulta .= $this->getScriptDelete($pObjTabelaProducao);
        return $consulta;
    }

    protected function constroiScriptEdit($pObjTabelaHomologacao, $pObjTabelaProducao) {
        $consulta="";
        $vObj = $this->getScriptDeletaChaveExtrangeira($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $vObj = $this->getScriptDeletaChavesSeNecessario($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $vObj = $this->getScriptRenomeaTabela($pObjTabelaHomologacao, $pObjTabelaProducao);
        $consulta .= $vObj->getQuery();

        $vVetorObjAtributo = $this->getScriptAtributos($pObjTabelaHomologacao, $pObjTabelaProducao);
        if($vVetorObjAtributo != null)
        foreach ($vVetorObjAtributo as $dadoAttr) {
            switch ($dadoAttr->idTipoOperacaoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
                    $consulta .=$dadoAttr->getQuery();
                    break;
                default:
                    break;
            }
        }
if($vVetorObjAtributo != null)
        foreach ($vVetorObjAtributo as $dadoAttr) {
            switch ($dadoAttr->idTipoOperacaoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION:
                    $consulta .=$dadoAttr->getQuery();
                    break;

                default:
                    break;
            }
        }

        $vObj = $this->getScriptCriaChaves($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $vObj = $this->getScriptCriaChaveExtrangeira($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();
        return $consulta;
    }

    protected function constroiScriptNoModification($pObjTabelaHomologacao, $pObjTabelaProducao) {
        //constroiScriptNoModification
$consulta = "";
        $vObj = $this->getScriptDeletaChaveExtrangeira($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $vObj = $this->getScriptDeletaChavesSeNecessario($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();
        
        $vObj = $this->getScriptRenomeaTabela($pObjTabelaHomologacao, $pObjTabelaProducao);
        $consulta .= $vObj->getQuery();

        $vVetorObjAtributo = $this->getScriptAtributos($pObjTabelaHomologacao, $pObjTabelaProducao);
        if($vVetorObjAtributo != null)
        foreach ($vVetorObjAtributo as $dadoAttr) {
            switch ($dadoAttr->idTipoOperacaoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
                    $consulta .=$dadoAttr->getQuery();
                    break;
                default:
                    break;
            }
        }
if($vVetorObjAtributo != null)
        foreach ($vVetorObjAtributo as $dadoAttr) {
            switch ($dadoAttr->idTipoOperacaoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION:
                    $consulta .=$dadoAttr->getQuery();
                    break;

                default:
                    break;
            }
        }
        $vObj = $this->getScriptCriaChaves($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        $vObj = $this->getScriptCriaChaveExtrangeira($pObjTabelaHomologacao, $pObjTabelaProducao);
        if ($vObj != null)
            $consulta .= $vObj->getQuery();

        return $consulta;
    }
    
    
    
    protected function constroiScriptIgnoreCase($pObjTabelaHomologacao, $pObjTabelaProducao) {
        //constroiScriptNoModification
$consulta = "";
        $vVetorObjAtributo = $this->getScriptAtributos($pObjTabelaHomologacao, $pObjTabelaProducao);
        if($vVetorObjAtributo != null)
        foreach ($vVetorObjAtributo as $dadoAttr) {
            switch ($dadoAttr->idTipoOperacaoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
                    $consulta .=$dadoAttr->getQuery();
                    break;
                default:
                    break;
            }
        }
if($vVetorObjAtributo != null)
        foreach ($vVetorObjAtributo as $dadoAttr) {
            switch ($dadoAttr->idTipoOperacaoAtualizacao) {
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION:
                    $consulta .=$dadoAttr->getQuery();
                    break;

                default:
                    break;
            }
        }
        
        return $consulta;
    }

}
