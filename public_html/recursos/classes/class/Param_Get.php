<?php

//CONSTANTES RELATIVAS AO PROJETO ESPECIFICO
class Param_Get{
    const ID_PROJETOS_VERSAO_PROCESSO_ESTRUTURA = "id_projetos_versao_processo_estrutura";
    const ID_PROJETOS_VERSAO_CAMINHO = "id_projetos_versao_caminho";
    const ID_TABELA_RAIZ = "id_tabela_raiz";
    const VETOR_ID = "vetor_id";
    const NUMERICO = "numerico";
    const PAGINA = "pagina";
    const PROXIMA_PAGINA = "proxima_pagina";
    const INDICE_ATUAL = "indice_atual";
    const ID_PROCESSO_ATUAL = "id_processo_atual";
    const BANCO_BANCO = "banco_banco";
    const TABELA_TABELA = "tabela_tabela";
    const ID1 = "id1";
    const DELETAR_ARQUIVOS = "deletar_arquivos";
    const TIPO_BANCO = "tipo_banco";
    const ID_PROJETOS_VERSAO_BANCO_BANCO = "id_projetos_versao_banco_banco";
    const ID_PROJETOS_VERSAO = "id_projetos_versao";
    const ID_PROJETOS = "id_projetos";
    const ID_SISTEMA = "id_sistema";
    const ID_CONEXOES = "id_conexoes";
    const ID_SISTEMA_PRODUTO_MOBILE = "id_sistema_produto_mobile";
    const ID_SISTEMA_PRODUTO = "id_sistema_produto";
    const GET_URL_ENCODE = "get_url_encode";
    const ID_SISTEMA_PRODUTO_LOG_ERRO = "id_sistema_produto_log_erro";

    const ID_SISTEMA_TIPO_DOWNLOAD_ARQUIVO = "id_sistema_tipo_download_arquivo";
    const NOME_TABELA = "NOME_TABELA";
    const NOME_ARQUIVO = "NOME_ARQUIVO";
    const ID_OPERACAO_SISTEMA = "id_operacao_sistema";
    const ID_OPERACAO_DOWNLOAD_BANCO_MOBILE = "id_operacao_download_banco_mobile";
    const ID_OPERACAO_SISTEMA_MOBILE = "id_operacao_sistema_mobile";
    const ID_MOBILE_CONECTADO = "id_mobile_conectado";
    const ID_ESTADO_OPERACAO_SISTEMA_MOBILE = "id_estado_operacao_sistema_mobile";
    const ID_MOBILE_IDENTIFICADOR = "id_mobile_identificador";
    const ID_TIPO_OPERACAO_SISTEMA = "id_tipo_operacao_sistema";
    const ID_MOBILE = "id_mobile";
    const ID_OBJ_JSON = "id_obj_json";
    const ID_PRODUCAO_TABELA_ID_INT = "producao_tabela_id_INT";
    const ID_ARVORE_VIEW = "id_arvore_view";
    const ID_OPERACAO_DOWNLOAD_MOBILE = "id_operacao_download_mobile";
    const  PROCESSO_INICIADO = "processo_iniciado";
    
    const ID_CAMINHO_CICLO_REINICIADO = "id_caminho_ciclo_reinicializado";
    const PROJETOS_VERSAO_PROCESSO_ESTRUTURA = "projetos_versao_processo_estrutura";
    const PROJETOS_VERSAO_CAMINHO = "projetos_versao_caminho";
    
    
    
    const ID_BANCO_BANCO = "id_banco_banco";
    public static function getIdentificadorProjetosVersao($pIdP = null, $pIdPV = null, $pIdPVBB = null){
        $idP = ($pIdP == null ) ? Helper::POSTGET(Param_Get::ID_PROJETOS) : $idP;
        $idPVBB = ($pIdPVBB==null) ? Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO) : $pIdPVBB;
        $idPV = ($pIdPV==null) ? Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO) : $pIdPV;
        

        return Helper::formataGET(array(
            Param_Get::ID_PROJETOS,
                    Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    Param_Get::ID_PROJETOS_VERSAO),
                    array($idP, $idPVBB, $idPV)
                );
    }
    
    public static function getIdentificadorPostProjetosVersao($pIdP = null, $pIdPV = null, $pIdPVBB = null){
        $idP = ($pIdP == null ) ? Helper::POSTGET(Param_Get::ID_PROJETOS) : $idP;
        $idPVBB = ($pIdPVBB==null) ? Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO) : $pIdPVBB;
        $idPV = ($pIdPV==null) ? Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO) : $pIdPV;
        
        ?>
            
        <input type="hidden" name="<?php echo Param_Get::ID_PROJETOS;?>" id="<?php echo Param_Get::ID_PROJETOS;?>" value="<?php echo $idP;?>">
        <input type="hidden" name="<?php echo Param_Get::ID_PROJETOS_VERSAO;?>" id="<?php echo Param_Get::ID_PROJETOS_VERSAO;?>" value="<?php echo $idPV;?>">
        <input type="hidden" name="<?php echo Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO;?>" id="<?php echo Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO;?>" value="<?php echo $idPVBB;?>">


        <?php

    }
    
     public static function getIdentificadorPostProcessoEstrutura($pIdPVPE = null, $pIdPVC = null){
        
        $idPVPE = ($pIdPVPE==null) ? Helper::POST(Param_Get::ID_PROJETOS_VERSAO_PROCESSO_ESTRUTURA): $pIdPVPE;
        $idPVC = ($pIdPVC==null) ? Helper::POST(Param_Get::ID_PROJETOS_VERSAO_CAMINHO):$pIdPVC;
            
        ?>
            
        <input type="hidden" name="<?php echo Param_Get::ID_PROJETOS_VERSAO_PROCESSO_ESTRUTURA;?>" id="<?php echo Param_Get::ID_PROJETOS_VERSAO_PROCESSO_ESTRUTURA;?>" value="<?php echo $idPVPE;?>">
        <input type="hidden" name="<?php echo Param_Get::ID_PROJETOS_VERSAO_CAMINHO;?>" id="<?php echo Param_Get::ID_PROJETOS_VERSAO_CAMINHO;?>" value="<?php echo $idPVC;?>">
        
        
        
        <?php

    }
    
}
