<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Campo
    * NOME DA CLASSE DAO: DAO_Campo
    * DATA DE GERAÇÃO:    07.02.2015
    * ARQUIVO:            EXTDAO_Campo.php
    * TABELA MYSQL:       campo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Campo extends DAO_Campo
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Campo";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_descricao = "Descrição";
			$this->label_lista_id_INT = "Lista";
			$this->label_tipo_campo_id_INT = "Tipo do Campo";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Campo();

        }

	}

    
