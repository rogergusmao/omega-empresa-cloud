<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_operacao_sistema_mobile
    * NOME DA CLASSE DAO: DAO_Tipo_operacao_sistema_mobile
    * DATA DE GERAÇÃO:    29.03.2013
    * ARQUIVO:            EXTDAO_Tipo_operacao_sistema_mobile.php
    * TABELA MYSQL:       tipo_operacao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_operacao_sistema_mobile 
    {
//        const TIPO = 1;
//        const AGUARDANDO = 1;
//        const PROCESSANDO = 1;
//        const CONCLUIDA = 3;
//        const ERRO_EXECUCAO = 4;

//1	transferir arquivo banco sqlite do servidor para o mobile
        const OPERACAO_DOWNLOAD_BANCO_MOBILE = 1;
        const OPERACAO_EXECUTA_SCRIPT = 2;
        const OPERACAO_CRUD_ALEATORIO = 3;
        const OPERACAO_SINCRONIZA = 4;
        const OPERACAO_REINSTALA_VERSAO = 5;
        const OPERACAO_MONITORAR_TELA = 6;
        const OPERACAO_DOWNLOAD_BANCO_SQLITE = 7;
        const OPERACAO_COMPARACAO_BANCO_SQLITE_MOBILE = 8;
        const OPERACAO_CRUD_MOBILE_BASEADO_WEB = 9;
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_operacao_sistema_mobile";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_operacao_sistema_mobile();

        }

	}

    
