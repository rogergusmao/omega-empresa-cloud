<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Sistema_projetos_versao
 * NOME DA CLASSE DAO: DAO_Sistema_projetos_versao
 * DATA DE GERAÃ‡ÃƒO:    15.03.2013
 * ARQUIVO:            EXTDAO_Sistema_projetos_versao.php
 * TABELA MYSQL:       sistema_projetos_versao
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡ÃƒO DA CLASSE
// **********************

class EXTDAO_Sistema_projetos_versao extends DAO_Sistema_projetos_versao {

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Sistema_projetos_versao";

    }

    public function setLabels() {

        $this->label_id = "Id";
        $this->label_projetos_versao_id_INT = "VersÃ£o do Projeto";
        $this->label_sistema_id_INT = "Sistema";

        $this->label_data_publicacao_DATETIME = "Data de PublicaÃ§Ã£o";
    }

    
    
        public function valorCampoLabel(){
            
            return $this->getId() ."@". $this->getFkObjProjetos_versao()->getNome();
            

        }
    
    
    
    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Sistema_projetos_versao();
    }

    public static function getIdSistema($idSistema, $db = null) {
        $consulta = "SELECT sistema_id_INT
                        FROM sistema_projetos_versao
                        WHERE projetos_versao_id_INT = $idSistema
                        LIMIT 0,1";
        if($db == null)$db = new Database();
        $db->query($consulta);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public static function consultaUltimaVersaoDoSistema($idSistema) {
        $consulta = "SELECT id
                        FROM sistema_projetos_versao
                        WHERE sistema_id_INT = {$idSistema}
                            ORDER BY data_producao_DATETIME DESC
                        LIMIT 0,1";
        $db = new Database();
        $db->query($consulta);
        return $db->getPrimeiraTuplaDoResultSet("id");
    }

}

