<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Usuario_menu
    * NOME DA CLASSE DAO: DAO_Usuario_menu
    * DATA DE GERAÇÃO:    24.08.2010
    * ARQUIVO:            EXTDAO_Usuario_menu.php
    * TABELA MYSQL:       usuario_menu
    * BANCO DE DADOS:     DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    
    class EXTDAO_Usuario_menu extends DAO_Usuario_menu
    {

        public function __construct($configDAO = null){
        
            parent::__construct($configDAO);
        
            	$this->nomeClasse = "EXTDAO_Usuario_menu";

        }
        
        public function setLabels(){
        
			$this->label_id = "Id";
			$this->label_usuario_id_INT = "Usuário";
			$this->label_area_menu = "Área do Menu";

        
        }
        
        public function setDiretorios(){
                
        
        
        }
        
        public function setDimensoesImagens(){
                
     
        
        }
        
        public static function factory(){
                
            return new EXTDAO_Usuario_menu();        
        
        }
        
	}
    
    
