<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Operacao_crud_mobile_baseado_web
    * NOME DA CLASSE DAO: DAO_Operacao_crud_mobile_baseado_web
    * DATA DE GERAÇÃO:    14.07.2013
    * ARQUIVO:            EXTDAO_Operacao_crud_mobile_baseado_web.php
    * TABELA MYSQL:       operacao_crud_mobile_baseado_web
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Operacao_crud_mobile_baseado_web extends DAO_Operacao_crud_mobile_baseado_web
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Operacao_crud_mobile_baseado_web";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_operacao_sistema_mobile_id_INT = "Operação de Sistema do Telefone";
			$this->label_url_json_operacoes_web = "URL do JSON das operações Web";

        }
        
        

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Operacao_crud_mobile_baseado_web();

        }

        
        
	 public function __actionAdd(){

            $idMI = Helper::POST(Param_Get::ID_MOBILE_IDENTIFICADOR);
            $idOSM = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($idMI, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_CRUD_MOBILE_BASEADO_WEB);
            
            $this->setByPost(1);
            $this->setOperacao_sistema_mobile_id_INT($idOSM);
            
            $this->formatarParaSQL();

            $this->insert();
            $this->selectUltimoRegistroInserido();

            return array("location: popup.php?tipo=pages&page=status_operacao_sistema_mobile&".Param_Get::ID_OPERACAO_SISTEMA_MOBILE."=".$idOSM);

        }

        
        
	}

    
