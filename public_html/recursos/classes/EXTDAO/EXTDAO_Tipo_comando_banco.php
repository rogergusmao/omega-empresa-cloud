<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_comando_banco
    * NOME DA CLASSE DAO: DAO_Tipo_comando_banco
    * DATA DE GERAÇÃO:    04.04.2013
    * ARQUIVO:            EXTDAO_Tipo_comando_banco.php
    * TABELA MYSQL:       tipo_comando_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_comando_banco extends DAO_Tipo_comando_banco
    {
        const DROP_TABLE = 1;
        const DROP_COLUMN = 2;
        const DROP_INDEX = 3;
        const DROP_FOREIGN_KEY = 4;
        const MODIFY_COLUMN = 5;
        const RENAME_TABLE = 6;
        const CREATE_KEY = 7;
        const CREATE_TABLE = 8;
        const ADD_COLUMN = 9;
        const CREATE_FOREIGN_KEY = 10;
        const DROP_UNIQUE_KEY = 11;
        const CREATE_UNIQUE_KEY = 12;
        
        const INSERT_INTO_REGISTER = 13;
        const DELETE_REGISTER = 14;
        const UPDATE_REGISTER = 15;
        
        const RENAME_ATTRIBUTE = 16;

        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_comando_banco";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_comando_banco();

        }

	}

    
