<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_operacao_atualizacao_banco
    * NOME DA CLASSE DAO: DAO_Tipo_operacao_atualizacao_banco
    * DATA DE GERAÇÃO:    08.01.2013
    * ARQUIVO:            EXTDAO_Tipo_operacao_atualizacao_banco.php
    * TABELA MYSQL:       tipo_operacao_atualizacao_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_operacao_atualizacao_banco extends DAO_Tipo_operacao_atualizacao_banco
    {

        public static $TIPO_INSERT = 2;
        public static $TIPO_EDIT = 3;
        public static $TIPO_DELETE = 1;
        public static $TIPO_NO_MODIFICATION = 4;
        public static $TIPO_ATUALIZACAO_REGISTRO_HOM_PARA_PROD = 5;
        public static $TIPO_IGNORE = 6;
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_operacao_atualizacao_banco";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_operacao_atualizacao_banco();

        }
public static function getClassLegend($pTipo){
            $classTr = "";
            if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){
                $classTr = "legend_filtro_insert";
            }else if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
                $classTr = "legend_filtro_edit";
            }else if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
                $classTr = "legend_filtro_delete";
            }else if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION){
                $classTr = "legend_filtro_no_modification";
            }else{
                $classTr = "legend_filtro";
            }
            return $classTr;
        }
        
        public static function getClassTr($pTipo, $i){
            $classTr = "";
            if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){
                $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
            }else if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
                $classTr = ($i%2)?"tr_list_conteudo_edit_impar":"tr_list_conteudo_edit_par";
            }else if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
                $classTr = ($i%2)?"tr_list_conteudo_delete_impar":"tr_list_conteudo_delete_par";
            }else if($pTipo == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION){
                $classTr = ($i%2)?"tr_list_conteudo_no_modification_impar":"tr_list_conteudo_no_modification_par";
            }else{
                $classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
            }
            return $classTr;
        }
        
        
        
	}

    
