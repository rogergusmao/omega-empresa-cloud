<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_log_identificador
    * NOME DA CLASSE DAO: DAO_Sistema_log_identificador
    * DATA DE GERAÇÃO:    25.05.2013
    * ARQUIVO:            EXTDAO_Sistema_log_identificador.php
    * TABELA MYSQL:       sistema_log_identificador
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_log_identificador extends DAO_Sistema_log_identificador
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_log_identificador";

          
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_identificador_erro = "Identificador do Erro";
			$this->label_descricao = "Descrição";
			$this->label_corrigida_BOOLEAN = "Foi Corrigida";
			$this->label_data_correcao_DATETIME = "Data de Correção";
			$this->label_data_ultima_visualizacao_DATETIME = "Data da Ultima Visualização";
			$this->label_corrigida_pelo_usuario_id_INT = "Usuário Que Corrigiu";
			$this->label_sistema_projetos_versao_id_INT = "Versão do Projeto do Sistema";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_log_identificador();

        }
        public static function atualizaEstadoParaVisualizada($idSLI){
            $data = Helper::getDiaEHoraAtualSQL();
            $q = "SELECT sple.id
                FROM sistema_produto_log_erro sple JOIN
                    sistema_log_identificador sli
                        ON sple.sistema_log_identificador_id_INT = sli.id
                WHERE sli.id = '$idSLI' 
                        AND sple.data_visualizacao_DATETIME IS NULL";
            $db = new Database();
            $db->query($q);
            $vetor = Helper::getResultSetToArrayDeUmCampo($db->result);
            $obj = new EXTDAO_Sistema_produto_log_erro();
            for($i = 0 ; $i < count($vetor); $i++){
                $id = $vetor[$i];
                if(strlen($id)){
                    $obj->select($id);
                    $obj->setData_visualizacao_DATETIME($data);
                    $obj->formatarParaSQL();
                    $obj->update($id);
                }
            }
        }
        
        public static function atualizaEstadoParaCorrigida($idSLI){
            $data = Helper::getDiaEHoraAtualSQL();
            $obj = new EXTDAO_Sistema_log_identificador();
            $obj->select($idSLI);
            $obj->setData_correcao_DATETIME($data);
            $obj->setCorrigida_pelo_usuario_id_INT(Seguranca::getId());
            $obj->formatarParaSQL();
            $obj->update($idSLI);
            
        }
        
        const ESTADO_NAO_VISUALIZADA = 0;
        //VERMELHA
        const ESTADO_NAO_CORRIGIDA = 1;
        //VERDE
        const ESTADO_CORRIGIDA = 2;
        //AMARELA
        const ESTADO_CORRIGIDA_MAS_OCORREU_ERRO_NOVAMENTE = 3;
        
        
        public function getEstado($totSegundosDataVisualizacao){
            if(!strlen($totSegundosDataVisualizacao) || $totSegundosDataVisualizacao == 0){
                return EXTDAO_Sistema_log_identificador::ESTADO_NAO_VISUALIZADA;
            }
            else if(!strlen($this->getData_correcao_DATETIME())){
                return EXTDAO_Sistema_log_identificador::ESTADO_NAO_CORRIGIDA;
            }else{
                $totDataCorrecao = $this->getData_correcao_DATETIME_UNIX();
                if($totDataCorrecao > $totSegundosDataVisualizacao){
                    return EXTDAO_Sistema_log_identificador::ESTADO_CORRIGIDA;
                }else{
                    return EXTDAO_Sistema_log_identificador::ESTADO_CORRIGIDA_MAS_OCORREU_ERRO_NOVAMENTE;
                }
            }
        }
        
        
        public static function backupCarregaEstrutura(){
            $q = "SELECT sple.id
FROM sistema_produto_log_erro sple 
WHERE sple.sistema_log_identificador_id_INT IS NULL";
            $db = new Database();
            $db->query($q);
            if(mysqli_num_rows($db->result) > 0){

                mysqli_data_seek($db->result, 0);

            }

            $objSPLE = new EXTDAO_Sistema_produto_log_erro();
            for($i=0; $registro = mysqli_fetch_array($db->result, MYSQL_NUM); $i++){
                
                $id = $registro[0];
                $objSPLE= new EXTDAO_Sistema_produto_log_erro();
                $objSPLE->select($id);
                
                $erro = "";
                $idSLI = EXTDAO_Sistema_log_identificador::consultaSistemaLogIdentificador($id);
                //se nao existir identificador do erro
                if(!strlen($idSLI)){
                    $obj = new EXTDAO_Sistema_log_identificador();        
                    if(strlen($objSPLE->getIdentificador_erro()))
                        $obj->setIdentificador_erro($objSPLE->getIdentificador_erro());
                    if(strlen($objSPLE->getDescricao()))
                        $obj->setDescricao($objSPLE->getDescricao());
                    
                    if(strlen($objSPLE->getStacktrace()))
                        $obj->setStacktrace($objSPLE->getStacktrace());
                    
                    $obj->setData_correcao_DATETIME(null);
                    $obj->setCorrigida_BOOLEAN("0");
                    $obj->setCorrigida_pelo_usuario_id_INT(null);
                    $obj->setSistema_projetos_versao_id_INT($objSPLE->getSistema_projetos_versao_id_INT());
                    
                    $obj->setSistema_projetos_versao_produto_id_INT($objSPLE->getSistema_projetos_versao_produto_id_INT());
                    
                    $obj->formatarParaSQL();
                    $obj->insert();
                    $obj->selectUltimoRegistroInserido();
                    $idSLI = $obj->getId();
                }
                $idSPLE = $registro[0];
                
                $objSPLE->setSistema_log_identificador_id_INT($idSLI);
                $objSPLE->formatarParaSQL();
                $objSPLE->update($idSPLE);
            }
        }
        
        public static function getRSSPLENaoMapeado($total){
            $q = "SELECT id_sple, id_sli, cod
FROM (
    ( 
        SELECT sple.id id_sple, sli.id id_sli, 1 cod
        FROM sistema_produto_log_erro sple 
                LEFT JOIN sistema_log_identificador sli ON (
                                        sli.descricao IS NULL 
                                        AND sple.descricao IS NULL 
                                        AND sli.stacktrace = sple.stacktrace)
        WHERE sple.sistema_log_identificador_id_INT IS NULL  
    )
    UNION (
        SELECT sple.id id_sple, sli.id id_sli, 2 cod
        FROM sistema_produto_log_erro sple 
            LEFT JOIN sistema_log_identificador sli ON (
                sli.descricao IS NOT NULL 
                AND sple.descricao IS NOT NULL 	
                AND sli.identificador_completo = CONCAT(sple.sistema_projetos_versao_produto_id_INT, sple.identificador_erro, sple.descricao)
        )
        WHERE sple.sistema_log_identificador_id_INT IS NULL  
    )
) X
LIMIT 0, $total";
            $db = new Database();
            $db->query($q);
            return $db->result;
        }
        
        public static function getTotalRSSPLENaoMapeado(){
            $q = "SELECT COUNT(id_sple)
FROM (
    ( 
        SELECT sple.id id_sple, sli.id id_sli, 1 cod
        FROM sistema_produto_log_erro sple 
                LEFT JOIN sistema_log_identificador sli ON (
                                        sli.descricao IS NULL 
                                        AND sple.descricao IS NULL 
                                        AND sli.stacktrace = sple.stacktrace)
        WHERE sple.sistema_log_identificador_id_INT IS NULL  
    )
    UNION (
        SELECT sple.id id_sple, sli.id id_sli, 2 cod
        FROM sistema_produto_log_erro sple 
            LEFT JOIN sistema_log_identificador sli ON (
                sli.descricao IS NOT NULL 
                AND sple.descricao IS NOT NULL 	
                AND sli.identificador_completo = CONCAT(sple.sistema_projetos_versao_produto_id_INT, sple.identificador_erro, sple.descricao)
        )
        WHERE sple.sistema_log_identificador_id_INT IS NULL  
    )
) X";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        public static function carregaEstrutura($total, $rs = null){
            if($rs == null)
                $rs = EXTDAO_Sistema_log_identificador::getRSSPLENaoMapeado($total);
            
            if(mysqli_num_rows($rs) > 0){

                mysqli_data_seek($rs, 0);

            }
            
            $objSPLE = new EXTDAO_Sistema_produto_log_erro();
            $obj = new EXTDAO_Sistema_log_identificador();        
            for($i=0; $registro = mysqli_fetch_array($rs, MYSQL_NUM); $i++){
                
                $idSPLEAux = $registro[0];
                $idSLI = $registro[1];
                $codAux = $registro[2];
                
                $objSPLE->select($idSPLEAux);
                
                $erro = "";
                if(!strlen($idSLI))
                    $idSLI = $objSPLE->consultaSistemaLogIdentificador();
                    
                //se nao existir identificador do erro
                if(! strlen($idSLI)){
                    
                    $obj->setId(null);
                    if(strlen($objSPLE->getIdentificador_erro()))
                        $obj->setIdentificador_erro($objSPLE->getIdentificador_erro());
                    if(strlen($objSPLE->getDescricao()))
                        $obj->setDescricao($objSPLE->getDescricao());
                    
                    if(strlen($objSPLE->getStacktrace()))
                        $obj->setStacktrace($objSPLE->getStacktrace());
                    
                    $obj->setData_correcao_DATETIME(null);
                    $obj->setCorrigida_BOOLEAN("0");
                    $obj->setCorrigida_pelo_usuario_id_INT(null);
                    $obj->setSistema_projetos_versao_id_INT($objSPLE->getSistema_projetos_versao_id_INT());
                    
                    $obj->setSistema_projetos_versao_produto_id_INT($objSPLE->getSistema_projetos_versao_produto_id_INT());
                    $identificadorCompleto = $objSPLE->getSistema_projetos_versao_produto_id_INT().$objSPLE->getIdentificador_erro().$objSPLE->getDescricao();
                    $obj->setIdentificador_completo($identificadorCompleto);
                    
                    $obj->formatarParaSQL();
                    $obj->insert();
                    $obj->selectUltimoRegistroInserido();
                    $idSLI = $obj->getId();
                }
                $objSPLE->setSistema_log_identificador_id_INT($idSLI);
                $objSPLE->formatarParaSQL();
                $objSPLE->update($idSPLEAux);
            }
            return $i;
        }
        
        
        
        
        
        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");
            
            $nextAction = Helper::POST("next_action");
            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            $id = Helper::POST("id1");
            $this->select($id);
            $corrigida = $this->getCorrigida_BOOLEAN() ? true : false;
            if($corrigida){
                $this->setCorrigida_BOOLEAN("1");
                $this->setData_correcao_DATETIME(Helper::getDiaEHoraAtualSQL());
                $this->setCorrigida_pelo_usuario_id_INT(Seguranca::getId());
            }
            $this->formatarParaSQL();

            $this->update($this->getId());

            $this->select($this->getId());

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }
        
        public static function processoCarregaLogErro(){
            

$varGET = Helper::formataGET(
          array(
            Param_get::PAGINA,
            Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
            Param_get::ID_PROJETOS_VERSAO,
            Param_get::PROXIMA_PAGINA
          ), array(
      Helper::encodeURLGet( "pages/processo_carrega_log_erro.php"),
            Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO),
            Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO),
            Helper::encodeURLGet('index.php?tipo=lists&page=gerencia_sistema_log_identificador')
          )
    );
$url = "index.php?tipo=pages&page=executa_processo_pagina&$varGET";
Helper::mudarLocation($url);
        }
        
	}

    
