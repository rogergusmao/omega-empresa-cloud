<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Backup_automatico_controle
    * NOME DA CLASSE DAO: DAO_Backup_automatico_controle
    * DATA DE GERAÇÃO:    25.08.2010
    * ARQUIVO:            EXTDAO_Backup_automatico_controle.php
    * TABELA MYSQL:       backup_automatico_controle
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    
    class EXTDAO_Backup_automatico_controle extends DAO_Backup_automatico_controle
    {

        public function __construct($configDAO = null){
        
            parent::__construct($configDAO);
        
            	$this->nomeClasse = "EXTDAO_Backup_automatico_controle";
            	$this->database = new Database(NOME_BANCO_DE_DADOS_BACKUP);

            
                    
        }
        
        public function setLabels(){
        
			$this->label_id = "";
			$this->label_hora_base_TIME = "";
			$this->label_hora_real_TIME = "";
			$this->label_data_DATE = "";

        
        }
        
        public function setDiretorios(){
                
        
        
        }
        
        public function setDimensoesImagens(){
                
     
        
        }
        
        public function __actionAdd(){
            
            $mensagemSucesso = "O controle de backup automático foi cadastrado com sucesso.";
        
            $numeroRegistros = Helper::POST("numeroRegs");
            
            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));
    
            for($i=1; $i <= $numeroRegistros; $i++){
            
                $this->setByPost($i);

                $this->formatarParaSQL();
                
                $this->insert();
                $this->selectUltimoRegistroInserido();
                
            
        	}
        
            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
                        
        }
        
        public static function factory(){
                
            return new EXTDAO_Backup_automatico_controle();        
        
        }
        
	}
    
    
