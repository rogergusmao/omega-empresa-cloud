<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Monitora_tela_web_para_mobile
    * NOME DA CLASSE DAO: DAO_Monitora_tela_web_para_mobile
    * DATA DE GERAÇÃO:    25.10.2013
    * ARQUIVO:            EXTDAO_Monitora_tela_web_para_mobile.php
    * TABELA MYSQL:       monitora_tela_web_para_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Monitora_tela_web_para_mobile extends DAO_Monitora_tela_web_para_mobile
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Monitora_tela_web_para_mobile";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_operacao_sistema_mobile_id_INT = "Operação de Sistema Mobile Relacionada";
			$this->label_desenha_circulo_BOOLEAN = "Desenha Círculo";
			$this->label_desenha_quadrado_BOOLEAN = "Desenha Quadrado";
			$this->label_acao_clique_BOOLEAN = "Ação de Clique?";
			$this->label_coordenada_x_INT = "Coordenada X";
			$this->label_coordenada_y_INT = "Coordenada Y";
			$this->label_mobile_identificador_id_INT = "Identificador do Telefone";
			$this->label_tipo_operacao_monitora_tela_id_INT = "Tipo de Operação de Monitoramento de Tela";
			$this->label_sequencia_operacao_INT = "Sequência da Operação";
			$this->label_data_ocorrencia_DATETIME = "Data de Ocorrência";
			
                        $this->label_raio_circulo_INT = "Raio do Circulo";
                        $this->label_largura_quadrado_INT = "Largura do retângulo";
                        $this->label_altura_quadrado_INT = "Altura dp retângulo";
                        $this->label_cor_INT = "cpr";
                        $this->label_mensagem = "mensagem";

        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Monitora_tela_web_para_mobile();

        }
        
        public static function consultaListaIdDaOSM($idOSM){
             $sql = "SELECT id "
                            . " FROM monitora_tela_web_para_mobile "
                            . " WHERE operacao_sistema_mobile_id_INT = $idOSM "
                            . " ORDER BY id DESC ";
                    $db = new Database();
                    $db->query($sql);
                    return Helper::getResultSetToArrayDeUmCampo($db->result);
                    
        }

	}

    
