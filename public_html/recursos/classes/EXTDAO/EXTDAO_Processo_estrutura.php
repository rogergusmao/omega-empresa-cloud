<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Processo_estrutura
    * NOME DA CLASSE DAO: DAO_Processo_estrutura
    * DATA DE GERAÇÃO:    22.04.2013
    * ARQUIVO:            EXTDAO_Processo_estrutura.php
    * TABELA MYSQL:       processo_estrutura
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Processo_estrutura extends DAO_Processo_estrutura
    {
        const COMPARAR_ESTRUTURA_BANCO_DE_PROD_COM_HOM= 1;
        const CARREGA_ESTRUTURA_BANCO_DE_HOM_PARA_PROD = 2;
        const GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM= 3;
        const ATUALIZA_ESTRUTURA_BANCO_DE_PROD = 4;
        const COMPARAR_ESTRUTURA_SINCRONIZACAO_BANCO_WEB_ANDROID= 9;
        const CARREGAR_CONFIGURACOES_SINCRONIZACAO = 5;

        const EXECUTAR_SCRIPT_DE_ATUALIZACAO_DOS_REGISTROS_DO_BANCO_DE_PRODUCAO= 11;

        const MAPEANDO_ESTRUTURA_DOS_BANCOS_HMG_SINC_E_WEB= 19;
        const CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO= 22;

        const CONFIGURANDO_RELACIONAMENTO_ENTRE_WEB_COM_SINCRONIZADOR_WEB= 20;
        const APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO = 23;
        const APAGAR_DADOS_DO_BANCO_DE_PRODUCAO = 25;
        const GERAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_DO_BANCO_DE_PRODUCAO = 28;
        const EXECUTAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_BANCO_PRD = 29;
        const GERAR_SCRIPT_SQL_DE_ATUALIZACAO_DOS_REGISTROS_DO_BANCO_DE_PRODUCAO = 10;

        const CRIA_ATRIBUTOS_CRUD_MOBILE_E_CRUD = 21;
        const MAPEAR_BANCO_DE_HOMOLOGACAO = 24;
        const GERAR_ARQUIVOS_CODIGO_FONTE_ANDROID = 17;

        const PREPARAR_ESTRUTURA_PARA_ANALISE= 8;

        const MAPEAR_BANCO_DE_PRODUCAO = 26;
        const SALVAR_CONFIGURACAO_SINCRONIZACAO_ATUAL_NOS_BANCOS_DE_HOMOLOGACAO = 7;
        const SALVAR_CONFIGURACAO_SINCRONIZACAO_ATUAL_NOS_BANCOS_DE_PRODUCAO = 30;



        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Processo_estrutura";

          

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
                        $this->label_utiliza_copia_projetos_versao_BOOLEAN = "Utiliza Cópia da versão do projeto para análise";
                        $this->label_fluxo_independente_BOOLEAN = "Fluxo tem que rodar por completo";
                        


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Processo_estrutura();

        }

        
        public static function getListaId($pIdProcessoEstrutura){
            $consulta = "SELECT DISTINCT id
                FROM processo_estrutura_caminho
                WHERE processo_estrutura_id_INT = $pIdProcessoEstrutura";
            $db = new Database();
            $db->query($consulta);
            return Helper::getResultSetToArrayDeUmCampo($db->query);
        }
        
        
        public static function getListaIdProcessoEstrutura($idPVBB){
            
            $q2 = "SELECT pec.id
                FROM processo_estrutura pe
                    JOIN processo_estrutura_caminho pec
                        ON pe.id = pec.processo_estrutura_id_INT
                WHERE pe.tipo_banco_id_INT = ".EXTDAO_Projetos_versao_banco_banco::getTipoBanco($idPVBB)."
                   ORDER BY pec.id";
        
            $db = new Database();
            
            $db->query($q2);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
           
        }
        
        public static function getListaIdProcessoEstruturaPeloTipoBanco($idTipoBanco){
            
            $q2 = "SELECT pec.id
                FROM processo_estrutura pe
                    JOIN processo_estrutura_caminho pec
                        ON pe.id = pec.processo_estrutura_id_INT
                WHERE pe.tipo_banco_id_INT = ".$idTipoBanco."
                   ORDER BY pec.id";
        
            $db = new Database();
            
            $db->query($q2);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
           
        }
        
        
       
        
}

    
