<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Usuario_tipo_privilegio
    * NOME DA CLASSE DAO: DAO_Usuario_tipo_privilegio
    * DATA DE GERAÇÃO:    13.07.2010
    * ARQUIVO:            EXTDAO_Usuario_tipo_privilegio.php
    * TABELA MYSQL:       usuario_tipo_privilegio
    * BANCO DE DADOS:     DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    
    class EXTDAO_Usuario_tipo_privilegio extends DAO_Usuario_tipo_privilegio
    {

        public function __construct($configDAO = null){
        
            parent::__construct($configDAO);
        
            	$this->nomeClasse = "EXTDAO_Usuario_tipo_privilegio";

          
                    
        }
        
        public function setLabels(){
        
			$this->label_id = "Id";
			$this->label_usuario_tipo_id_INT = "Tipo de Usuário";
			$this->label_identificador_funcionalidade = "Identificador da Funcionalidade";

        
        }
        
        public function setDiretorios(){
                
        
        
        }
        
        public function setDimensoesImagens(){
                
     
        
        }
        
        public static function factory(){
                
            return new EXTDAO_Usuario_tipo_privilegio();        
        
        }
        
	}
    
    
