<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_projetos_versao_produto
    * NOME DA CLASSE DAO: DAO_Sistema_projetos_versao_produto
    * DATA DE GERAÃ‡ÃƒO:    26.05.2013
    * ARQUIVO:            EXTDAO_Sistema_projetos_versao_produto.php
    * TABELA MYSQL:       sistema_projetos_versao_produto
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Sistema_projetos_versao_produto extends DAO_Sistema_projetos_versao_produto
    {

        const PREFIXO_SCRIPT_HOM_PROD = "script_hom_prod_";
        const PREFIXO_XML_HOM_PROD = "xml_hom_prod_";
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_projetos_versao_produto";


        }

        public function setLabels(){

            $this->label_id = "Id";
            $this->label_sistema_projetos_versao_id_INT = "VersÃ£o do Projeto do Sistema";
            $this->label_sistema_produto_id_INT = "Produto";
            $this->label_prototipo_projetos_versao_banco_banco_id_INT = "CÃ³pia do prÃ³totipo";
            $this->label_servidor_projetos_versao_banco_banco_id_INT = "Bancos";
            $this->label_script_estrutura_banco_hom_para_prod_ARQUIVO = "Script de atualizaÃ§Ã£o da estrutura do banco de produÃ§Ã£o com homologaÃ§Ã£o";
            $this->label_script_registros_banco_hom_para_prod_ARQUIVO = "Script de atualizaÃ§Ã£o de registros do banco de produÃ§Ã£o com homologaÃ§Ã£o";
            $this->label_script_estrutura_banco_hom = "Script do banco de homologaÃ§Ã£o";
            $this->label_script_estrutura_banco_prod = "Script do banco de produÃ§Ã£o";

        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        

        public function getComboBoxAllSistema_projetos_versao($objArgumentos){

		$objArgumentos->nome="sistema_projetos_versao_id_INT";
		$objArgumentos->id="sistema_projetos_versao_id_INT";
		$objArgumentos->valueReplaceId=false;
                $objArgumentos->query = "SELECT spv.id, CONCAT(spv.id,'@',pv.nome)
                    FROM sistema_projetos_versao spv JOIN
                            projetos_versao pv ON spv.projetos_versao_id_INT = pv.id
                         ORDER BY pv.nome ASC";
		return $this->getFkObjSistema_projetos_versao()->getComboBox($objArgumentos);

	}



        
        public static function factory(){

            return new EXTDAO_Sistema_projetos_versao_produto();

        }
        
        public function valorCampoLabel(){
            
            return $this->getFkObjSistema_produto()->getId()."@".$this->getFkObjSistema_produto->getNome();
            

        }
    
        
        public static function getNomeXmlHomProd($id){
            return EXTDAO_Sistema_projetos_versao_produto::PREFIXO_XML_HOM_PROD.$id;
        }
        public static function getNomeScriptHomProd($id){
            return EXTDAO_Sistema_projetos_versao_produto::PREFIXO_SCRIPT_HOM_PROD.$id;
        }
        
         public function getListaPrototipoPVBBAntecessor(){
            $idSPVP = $this->getId();
            $idPVBB = $this->getPrototipo_projetos_versao_banco_banco_id_INT();
            $q = "SELECT spvp.id
FROM sistema_projetos_versao_sistema_produto_mobile spvspm
	JOIN sistema_projetos_versao_produto spvp ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id
WHERE spvp.prototipo_projetos_versao_banco_banco_id_INT = $idPVBB
	AND spvp.id < $idSPVP
	AND spvp.id > (
		SELECT MAX(spvp2.id)
		FROM sistema_projetos_versao_sistema_produto_mobile spvspm2
			JOIN sistema_projetos_versao_produto spvp2 ON spvspm2.sistema_projetos_versao_produto_id_INT = spvp2.id
		WHERE spvp2.prototipo_projetos_versao_banco_banco_id_INT = $idPVBB
			AND spvp2.id < $idSPVP
			AND spvspm2.resetar_banco_no_fim_da_atualizacao_BOOLEAN = '1'
		ORDER BY spvp2.id DESC
	)
	AND spvspm.resetar_banco_no_fim_da_atualizacao_BOOLEAN != '1'
ORDER BY spvp.id DESC";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
         public static function getIdSPVPDaUltimaVersaoDoProduto($idSP){
        
        
            $q = "SELECT MAX(spvp.id)
FROM sistema_projetos_versao_produto spvp
WHERE spvp.sistema_produto_id_INT = $idSP
LIMIT 0,1";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public static function executaProcessoUsuarioInformaOsDadosDoAplicativo(){
            $obj = new EXTDAO_Sistema_projetos_versao_produto();
            $obj->select(Helper::POST(Param_Get::ID1));
            $obj->__actionEdit();
            
            $url = EXTDAO_Projetos_versao_processo_estrutura::actionUrlExecutaProcesso(true);
            
            return array("location: $url");
        }

	}

    
