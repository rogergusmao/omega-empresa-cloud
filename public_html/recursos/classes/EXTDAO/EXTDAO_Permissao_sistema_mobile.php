<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Permissao_sistema_mobile
    * NOME DA CLASSE DAO: DAO_Permissao_sistema_mobile
    * DATA DE GERAÇÃO:    19.10.2013
    * ARQUIVO:            EXTDAO_Permissao_sistema_mobile.php
    * TABELA MYSQL:       permissao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Permissao_sistema_mobile extends DAO_Permissao_sistema_mobile
    {
        public static $SUFIXOS = array("_peq", "_med", "grd");
        public static $DIMENSOES = array(32, 64 ,128, 256);
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Permissao_sistema_mobile";

           

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_tag = "Tag";
			$this->label_pai_permissao_id_INT = "Permissão Pai";
			$this->label_tipo_permissao_id_INT = "Tipo de Permissão";
			$this->label_icone_32_ARQUIVO = "ícone 32 Px";
			$this->label_icone_64_ARQUIVO = "ícone 64 Px";
			$this->label_icone_128_ARQUIVO = "ícone 128 Px";
			$this->label_icone_256_ARQUIVO = "ícone 256 Px";
			$this->label_projetos_versao_banco_banco_id_INT = "Versão do Protótipo";
			$this->label_excluido_BOOLEAN = "";
			$this->label_excluido_DATETIME = "";


        }


              
        public function setDiretorios(){

            			$this->diretorio_icone_32_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final
			$this->diretorio_icone_64_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final
			$this->diretorio_icone_128_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final
			$this->diretorio_icone_256_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final


        }
            
        



        public static function factory(){

            return new EXTDAO_Permissao_sistema_mobile();

        }
        
            
    
        public static function getIdPermissao($idPVBB, $tag){
            $q = "SELECT id
                FROM permissao_sistema_mobile
                WHERE tag = '$tag' AND projetos_versao_banco_banco_id_INT = $idPVBB 
                LIMIT 0,1";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
	}

    
