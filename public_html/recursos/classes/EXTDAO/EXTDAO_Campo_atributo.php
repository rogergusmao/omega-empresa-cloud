<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Campo_atributo
    * NOME DA CLASSE DAO: DAO_Campo_atributo
    * DATA DE GERAÇÃO:    07.02.2015
    * ARQUIVO:            EXTDAO_Campo_atributo.php
    * TABELA MYSQL:       campo_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Campo_atributo extends DAO_Campo_atributo
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Campo_atributo";

         

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_campo_id_INT = "Campo";
			$this->label_atributo_id_INT = "Atributo";
			$this->label_seq_INT = "Sequëncia";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Campo_atributo();

        }

	}

    
