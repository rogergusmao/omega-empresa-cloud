<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Corporacao
    * NOME DA CLASSE DAO: DAO_Corporacao
    * DATA DE GERAÇÃO:    02.03.2017
    * ARQUIVO:            EXTDAO_Corporacao.php5
    * TABELA MYSQL:       corporacao
    * BANCO DE DADOS:     biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Corporacao extends DAO_Corporacao
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Corporacao";

         

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Corporacao();

        }


        public static function existeIdCorporacao($id, $db = null)
        {
            if(empty($id)) return false;
            $q = "SELECT 1"
                . " FROM corporacao "
                . " WHERE id = $id ";
            if ($db == null)
                $db = new Database();
            $db->query($q);
            return $db->rows() > 0 ;
        }

	}

    
