<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Diretorio_android
    * NOME DA CLASSE DAO: DAO_Diretorio_android
    * DATA DE GERAÇÃO:    30.05.2013
    * ARQUIVO:            EXTDAO_Diretorio_android.php
    * TABELA MYSQL:       diretorio_android
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Diretorio_android extends DAO_Diretorio_android
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Diretorio_android";

           
        }
        public function getPathResources(){
            $raiz = $objDA->getRaiz();
            $raizRes = $raiz."res/";    
            return $raizRes;
        }
        public function getPathDrawable(){
            $raizDrawableMDPI = $this->getPathResources()."drawable";
            return $raizDrawableMDPI;
        }
        public function getPathDrawableLdpi(){
            $raizDrawableMDPI = $this->getPathResources()."drawable-mdpi";
            return $raizDrawableMDPI;
        }
        
        public function getPathAppConfiguration()
            {
                $raizSrc = $this->getRaizSrc();
                $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raizSrc));
                return $packageRaiz."/appconfiguration/";
            }
        public function getPathDrawableMdpi(){
            $raizDrawableMDPI = $this->getPathResources()."drawable-mdpi";
            return $raizDrawableMDPI;
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_projetos_versao_banco_banco_id_INT = "Protótipo Android";
			$this->label_DAO = "DAO";
			$this->label_EXTDAO = "EXTDAO";
			$this->label_forms = "Formulários";
			$this->label_filtros = "Filtros";
			$this->label_lists = "Listas";
			$this->label_adapter = "Adapter";
			$this->label_itemList = "Item List";
			$this->label_detail = "Detalhes";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Diretorio_android();

        }

        public static function getIdDiretorio($idPVBB, $db = null){
            
            if($db == null)
            $db = new Database();
            $q = "SELECT id FROM diretorio_android WHERE projetos_versao_banco_banco_id_INT = $idPVBB LIMIT 0,1";
            
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
            
        }
    
        public function getRaizSrc(){
            return $this->getRaiz()."src/";
        }
        
        public function getRaizAssets(){
            return $this->getRaiz()."assets/";
        }
        
	}

    
