<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_script_banco
    * NOME DA CLASSE DAO: DAO_Tipo_script_banco
    * DATA DE GERAÇÃO:    07.01.2016
    * ARQUIVO:            EXTDAO_Tipo_script_banco.php
    * TABELA MYSQL:       tipo_script_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_script_banco extends DAO_Tipo_script_banco
    {
        const SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD = 1;
        const SCRIPT_DE_ATUALIZACAO_DO_BANCO_WEB_DO_ANDROID = 2;
        const SCRIPT_DE_ATUALIZACAO_DE_REGISTROS_DE_HMG_PARA_PRD = 3;
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_script_banco";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Tipo_script_banco();

        }

	}

    
