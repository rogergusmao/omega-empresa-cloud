<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Banco
 * NOME DA CLASSE DAO: DAO_Banco
 * DATA DE GERAÃ‡ÃƒO:    08.01.2013
 * ARQUIVO:            EXTDAO_Banco.php
 * TABELA MYSQL:       banco
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡ÃƒO DA CLASSE
// **********************

class EXTDAO_Banco extends DAO_Banco {

    public function __construct($configDAO = null){

        parent::__construct($configDAO);

    }

    
    public function valorCampoLabel(){
        $valor =  $this->getNome();
        if(strlen($this->getConexoes_id_INT()))
            $valor .= "@".$this->getFkObjConexoes()->valorCampoLabel();
        
        return $valor;
    }

    
    public function setLabels() {

        $this->label_id = "Id";
        $this->label_nome = "Nome";
        $this->label_tipo_banco_id_INT = "Tipo de Banco";
        $this->label_projeto_id_INT = "Projeto";
        $this->label_conexoes_id_INT = "ConexÃµes";
        $this->label_banco_homologacao_id_INT = "Banco de HomologaÃ§Ã£o";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Banco();
    }

    public function getObjDatabase() {
        
        $vIdConexao = $this->getConexoes_id_INT();
        $objConexao = new EXTDAO_Conexoes();
        $objConexao->select($vIdConexao);
        
        return new Database(
                        $this->getNome(),
                        $objConexao->getHostnameBanco(),
                        $objConexao->getPortaBanco(),
                        $objConexao->getUsuarioBanco(),
                        $objConexao->getSenhaBanco()
        );
    }

    public function getNomeClasse(){
        $str = $this->getNome();
        $str = str_replace("_", " ", $str);
        $str = ucwords($str);
        $str = trim($str);
        return $str;
    }
    
    public function getIdBancoDaVersaoDoProjeto($pIdProjeto, $pIdTipoWeb, $pIsHomologacao) {


        $database = new Database();
        $vQuery = "SELECT b.id ";
        $vQuery .= "FROM banco b JOIN banco_banco bb ON ";
        if ($pIsHomologacao)
            $vQuery .= " b.id = bb.homologacao_banco_id_INT ";
        else
            $vQuery .= " b.id = bb.producao_banco_id_INT ";

        $vQuery .= " JOIN projetos p ON bb.projetos_id_INT = p.id 
                          WHERE p.id = {$pIdProjeto}";


        $vQuery .= " AND pb.tipo_banco_id_INT = " . $pIdTipoWeb . " ";

        $vQuery .= " LIMIT 0, 1";


        $database->query($vQuery);
        $vVetor = Helper::getResultSetToArrayDeUmCampo($database->result);
        if (count($vVetor) > 0)
            return $vVetor[0];
        else
            return null;
    }

    public function getVetorObjAtributoDaTabela($pTabela, $dbExterno = null) {
        
        if ($pTabela == null || strlen($pTabela) == 0)
            return;
        if($dbExterno == null)
            $vDatabaseBanco = $this->getObjDatabase();
        else
            $vDatabaseBanco = $dbExterno ;
        
        if ($vDatabaseBanco != null) {
            $vDatabaseBanco->query("SHOW COLUMNS FROM {$pTabela}");
            return Helper::getResultSetToMatriz($vDatabaseBanco->result);
        }
        return null;
    }

    public function showColumnsDaTabela($pTabela, $vDatabaseBanco = null) {
        
        if ($pTabela == null || strlen($pTabela) == 0)
            return;
        if($vDatabaseBanco == null)
        $vDatabaseBanco = $this->getObjDatabase();
        if ($vDatabaseBanco != null) {
            $vDatabaseBanco->query("SHOW COLUMNS FROM {$pTabela}");
            $vVetorRetorno = array();
            $res = Helper::getResultSetToMatriz($vDatabaseBanco->result);
            if($res != null)
            foreach ($res as $objAtributo) {
                $vVetorRetorno[count($vVetorRetorno)] = $objAtributo["Field"];
            }
            return $vVetorRetorno;
        }
        return null;
    }

    public function getListaChaveUnicaDaTabelaDoBanco($pTabela, $objBanco = null) {
        if($objBanco == null)
        $objBanco = $this->getObjDatabase();
        if ($objBanco != null) {

            $vQuery = "SHOW KEYS FROM `{$pTabela}` WHERE Non_unique = 0 AND Key_name != 'PRIMARY'";
            $objBanco->Query($vQuery);
            $vVetorChave = Helper::getResultSetToMatriz($objBanco->result);

            $vVetorRetorno = array();
            if($vVetorChave != null){
                foreach ($vVetorChave as $vInformation) {

                    //$vInformation['CONSTRAINT_NAME'];
                    //$vInformation['TABLE_NAME'];
                    //$vInformation['REFERENCED_TABLE_NAME'];
                    //$vInformation['UPDATE_RULE'];
                    //$vInformation['DELETE_RULE'];
                    
                    foreach ($vVetorChave as $vChave) {
                        //$vChave['Table'];

                        $vVetorRetorno[count($vVetorRetorno)] = array(
                            "nome_chave" => $vChave['Key_name'],
                            "tabela" => $vInformation['Table'],
                            "atributo" => $vInformation['Column_name']);
                    }
                }

                return $vVetorRetorno;
            }
        }
        return null;
    }

//        | Table   
//        | Non_unique 
//        | Key_name             
//        | Seq_in_index 
//        | Column_name           
//        | Collation 
//        | Cardinality 
//        | Sub_part 
//        | Packed 
//        | Null 
//        | Index_type 
//        | Comment 
//        | Index_comment 

    public function getChavesDaTabelaDoBanco($pTabela, $objBanco = null) {
        if($objBanco == null)
        $objBanco = $this->getObjDatabase();
        if ($objBanco != null) {
            if ($pTabela == "bairro") {
                $h = 0;
                $y = 0;
            }
            $vQuery = "SHOW INDEX FROM $pTabela WHERE key_name != 'PRIMARY'";
            $objBanco->Query($vQuery);
            return Helper::getResultSetToMatriz($objBanco->result);
        }
        return null;
    }

    public function getChavesExtrangeirasDuplcadas($pTabela, $db = null){
        if($db == null)
            $db = $this->getObjDatabase();

        $q = "SELECT I.constante
FROM 
(
	SELECT rc.CONSTRAINT_NAME constante, kc.COLUMN_NAME
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
			JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kc 
					ON rc.CONSTRAINT_NAME = kc.CONSTRAINT_NAME
	WHERE rc.UNIQUE_CONSTRAINT_SCHEMA='".$db->realEscapeString($this->getNome())."'
			 AND rc.TABLE_NAME='".$db->realEscapeString($pTabela)."'
			 AND kc.TABLE_SCHEMA='".$db->realEscapeString($this->getNome())."'
			 AND kc.TABLE_NAME = '".$db->realEscapeString($pTabela)."'
			 AND REFERENCED_TABLE_SCHEMA IS NOT NULL
	
) I

 JOIN

(
	SELECT COUNT(rc.CONSTRAINT_NAME) total, kc.COLUMN_NAME
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
			JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kc 
					ON rc.CONSTRAINT_NAME = kc.CONSTRAINT_NAME
	WHERE rc.UNIQUE_CONSTRAINT_SCHEMA='".$db->realEscapeString($this->getNome())."'
			 AND rc.TABLE_NAME='".$db->realEscapeString($pTabela)."'
			 AND kc.TABLE_SCHEMA='".$db->realEscapeString($this->getNome())."'
			 AND kc.TABLE_NAME = '".$db->realEscapeString($pTabela)."'
			 AND REFERENCED_TABLE_SCHEMA IS NOT NULL
	GROUP BY kc.COLUMN_NAME
	HAVING total > 1 
) J

ON I.COLUMN_NAME = J.COLUMN_NAME ";
        $db->query($q);
        
        $chaves = Helper::getResultSetToArrayDeUmCampo($db->result);
//        echo "TABELA: $pTabela";
//        if($pTabela == "cidade"){
//            print_r($chaves );
//                    exit();
//        }
        return $chaves;
    }
    
    public function getListaChaveExtrangeiraDaTabelaDoBanco($pTabela, $db = null) {
        if($db == null)
        $db = $this->getObjDatabase();
        if ($db != null) {
            $query = "SELECT rc.CONSTRAINT_NAME, rc.TABLE_NAME, kc.COLUMN_NAME,  rc.REFERENCED_TABLE_NAME , kc.REFERENCED_COLUMN_NAME, rc.UPDATE_RULE, rc.DELETE_RULE
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kc ON rc.CONSTRAINT_NAME = kc.CONSTRAINT_NAME
WHERE rc.UNIQUE_CONSTRAINT_SCHEMA='{$this->getNome()}' "
                . " AND rc.TABLE_NAME='$pTabela'"
                . " AND kc.TABLE_SCHEMA='{$this->getNome()}' "
                . " AND kc.TABLE_NAME = '$pTabela'"
                . " AND REFERENCED_TABLE_SCHEMA IS NOT NULL";
            
            $db->Query($query);
            
            $vVetorInformationSchema = Helper::getResultSetToMatriz($db->result);
            
//            echo "INFORMATION:$query<br/>";
//            print_r($vVetorInformationSchema);
            
            $query = "SHOW KEYS FROM $pTabela WHERE Non_unique = 1";
            $db->Query($query);
            $vVetorChave = Helper::getResultSetToMatriz($db->result);


            $vVetorNomeChave = array();
            $vVetorRetorno = array();
            if($vVetorChave != null)
            foreach ($vVetorChave as $vChave)
                $vVetorNomeChave[count($vVetorChave)] = $vChave['Key_name'];
            if($vVetorInformationSchema != null)
            foreach ($vVetorInformationSchema as $vInformation) {

                //$vInformation['CONSTRAINT_NAME'];
                //$vInformation['TABLE_NAME'];
                //$vInformation['REFERENCED_TABLE_NAME'];
                //$vInformation['UPDATE_RULE'];
                //$vInformation['DELETE_RULE'];

//                foreach ($vVetorChave as $vChave) {
//                    //$vChave['Table'];
//                    if ($vChave['Column_name'] == $vInformation['COLUMN_NAME']) {
                        $vVetorRetorno[count($vVetorRetorno)] = array(
                            "nome_chave" => $vInformation['CONSTRAINT_NAME'],
                            "tabela" => $vInformation['TABLE_NAME'],
                            "atributo" => $vInformation['COLUMN_NAME'],
                            "tabela_referencia" => $vInformation['REFERENCED_TABLE_NAME'],
                            "atributo_referencia" => $vInformation['REFERENCED_COLUMN_NAME'],
                            "on_update" => $vInformation['UPDATE_RULE'],
                            "on_delete" => $vInformation['DELETE_RULE']);
//                    }
//                }
            }

            return $vVetorRetorno;
        }
        return null;
    }

    public function getListaChaveExtrangeiraDoBanco() {
        $objBanco = $this->getObjDatabase();
        if ($objBanco != null) {
            $vQuery = "SELECT  
                    KCU1.CONSTRAINT_NAME AS FK_CONSTRAINT_NAME 
                   ,KCU1.TABLE_NAME AS FK_TABLE_NAME 
                   ,KCU1.COLUMN_NAME AS FK_COLUMN_NAME 
                   ,KCU1.ORDINAL_POSITION AS FK_ORDINAL_POSITION 
                   ,KCU2.CONSTRAINT_NAME AS REFERENCED_CONSTRAINT_NAME 
                   ,KCU2.TABLE_NAME AS REFERENCED_TABLE_NAME 
                   ,KCU2.COLUMN_NAME AS REFERENCED_COLUMN_NAME 
                   ,KCU2.ORDINAL_POSITION AS REFERENCED_ORDINAL_POSITION 
                FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

                LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
                    ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
                    AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
                    AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

                LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU2 
                    ON KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG  
                    AND KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA 
                    AND KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME 
                    AND KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION ";
            $objBanco->Query($vQuery);
            return Helper::getResultSetToMatriz($objBanco->result);
        }
        return null;
    }

    public function isExistenteChaveExtrangeiraDoCampoNaTabela($p_strNomeCampo, $p_strNomeTabela) {
        $objBanco = $this->getObjDatabase();
        if ($objBanco != null) {
            if (strlen($p_strNomeCampo) > 0 && strlen($p_strNomeTabela) > 0) {

                // Pega todo pedido no status requisitado, seje relativo a um produto individual, ou a um combo, ou promocao.
                $objBanco->Query("SHOW KEYS " .
                        " FROM " . $p_strNomeTabela .
                        " WHERE Column_name='" . $p_strNomeCampo . "' AND
                            Non_unique='1';");
                $v_strDefault = $objBanco->getPrimeiraTuplaDoResultSet(0);

                if (strlen($v_strDefault) > 0) {
                    return true;
                }
                else
                    return false;
            }
        }
    }

    public function showTables() {

        $vDatabaseBanco = $this->getObjDatabase();
        if ($vDatabaseBanco != null) {
            $vDatabaseBanco->query("SHOW TABLES");
            return Helper::getResultSetToArrayDeUmCampo($vDatabaseBanco->result);
        }
        return null;
    }

    //TODO - responsavel por inserir as tuplas das tabelas tabela
    public function insereTabelas() {
        
    }

    
    public static function getListaIdentificadorNo($idConexoes){
        $q = "SELECT b.id, b.nome
                FROM banco b
                WHERE b.conexoes_id_INT = {$idConexoes}
                ORDER BY b.nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    
    
}

