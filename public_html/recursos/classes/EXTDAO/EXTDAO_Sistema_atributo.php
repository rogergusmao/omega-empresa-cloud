<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_atributo
    * NOME DA CLASSE DAO: DAO_Sistema_atributo
    * DATA DE GERAÇÃO:    19.07.2014
    * ARQUIVO:            EXTDAO_Sistema_atributo.php
    * TABELA MYSQL:       sistema_atributo
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_atributo extends DAO_Sistema_atributo
    {

      public function __construct($configDAO = null){

            parent::__construct($configDAO);
    $this->nomeClasse = "EXTDAO_Sistema_atributo";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_sistema_tabela_id_INT = "Sistema Tabela";
			$this->label_tipo_sql = "Tipo Sql";
			$this->label_tamanho_INT = "Tamanho";
			$this->label_decimal_INT = "Decimal";
			$this->label_not_null_BOOLEAN = "Not Null";
			$this->label_primary_key_BOOLEAN = "Chave Primária?";
			$this->label_auto_increment_BOOLEAN = "Auto Incremento";
			$this->label_valor_default = "Valor Default";
			$this->label_fk_sistema_tabela_id_INT = "Tabela Fk";
			$this->label_atributo_fk = "Atributo Fk";
			$this->label_fk_nome = "Nome";
			$this->label_update_tipo_fk = "Update Tipo Fk";
			$this->label_delete_tipo_fk = "Delete Tipo Fk";
			$this->label_label = "Label";
			$this->label_unique_BOOLEAN = "única?";
			$this->label_unique_nome = "Nome Chave única";
			$this->label_seq_INT = "Sequência";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Sistema_atributo();

        }

    public static function atualizaOuInsereAtributo(
        $databaseWebOuSincronizadorWebTgt, 
        $objAtributoPrototipoWebOuAndroidSrc, 
        $idSistemaTabelaWebOuSincronizadorWebTgt){

        $nomeAtributo = $objAtributoPrototipoWebOuAndroidSrc->getNome();
        $qWebOuSincronizadorWebTgt = "SELECT id "
            . " FROM sistema_atributo "
            . " WHERE nome LIKE '".$nomeAtributo."' "
            . " AND sistema_tabela_id_INT = $idSistemaTabelaWebOuSincronizadorWebTgt ";
        $databaseWebOuSincronizadorWebTgt->query($qWebOuSincronizadorWebTgt);
        
        $idSistemaAtributo = $databaseWebOuSincronizadorWebTgt->getPrimeiraTuplaDoResultSet(0);
//        if($objAtributoPrototipoWebOuAndroidSrc->getId() == 266){
//            echo "$qWebOuSincronizadorWebTgt. ID SISTEMA ATRIBUTO: $idSistemaAtributo";
//            exit();
//        }
        $objSistemaAtributo = new EXTDAO_Sistema_atributo( $databaseWebOuSincronizadorWebTgt);
        
        
        
        if(strlen($idSistemaAtributo)){

            $objSistemaAtributo->select($idSistemaAtributo);
        }
        $aconteceuAlteracao = false;
        if($objSistemaAtributo->getSistema_tabela_id_INT() != $idSistemaTabelaWebOuSincronizadorWebTgt){
            $objSistemaAtributo->setSistema_tabela_id_INT($idSistemaTabelaWebOuSincronizadorWebTgt);    
            $aconteceuAlteracao = true;
        }

        if( strcmp($objSistemaAtributo->getNome(), $nomeAtributo) != 0){
            $objSistemaAtributo->setNome($nomeAtributo);
            $aconteceuAlteracao = true;
        }
        if($objSistemaAtributo->getNot_null_BOOLEAN() != $objAtributoPrototipoWebOuAndroidSrc->getNot_null_BOOLEAN()){
            $objSistemaAtributo->setNot_null_BOOLEAN($objAtributoPrototipoWebOuAndroidSrc->getNot_null_BOOLEAN());
            $aconteceuAlteracao = true;
        }

        if($objSistemaAtributo->getValor_default() != $objAtributoPrototipoWebOuAndroidSrc->getValor_default()){
            $objSistemaAtributo->setValor_default($objAtributoPrototipoWebOuAndroidSrc->getValor_default());    
            $aconteceuAlteracao = true;
        }
        if($objSistemaAtributo->getPrimary_key_BOOLEAN() != $objAtributoPrototipoWebOuAndroidSrc->getPrimary_key_BOOLEAN()){
            $objSistemaAtributo->setPrimary_key_BOOLEAN($objAtributoPrototipoWebOuAndroidSrc->getPrimary_key_BOOLEAN());    
            $aconteceuAlteracao = true;
        }
        if($objSistemaAtributo->getTamanho_INT() != $objAtributoPrototipoWebOuAndroidSrc->getTamanho_INT()){
            $objSistemaAtributo->setTamanho_INT($objAtributoPrototipoWebOuAndroidSrc->getTamanho_INT());    
            $aconteceuAlteracao = true;
        }
        if($objSistemaAtributo->getAuto_increment_BOOLEAN() != $objAtributoPrototipoWebOuAndroidSrc->getAuto_increment_BOOLEAN()){
            $objSistemaAtributo->setAuto_increment_BOOLEAN($objAtributoPrototipoWebOuAndroidSrc->getAuto_increment_BOOLEAN());    
            $aconteceuAlteracao = true;
        }
        if($objSistemaAtributo->getLabel() != $objAtributoPrototipoWebOuAndroidSrc->getLabel()){
            $objSistemaAtributo->setLabel($objAtributoPrototipoWebOuAndroidSrc->getLabel());    
            $aconteceuAlteracao = true;
        }
        if($objSistemaAtributo->getUnique_BOOLEAN() != $objAtributoPrototipoWebOuAndroidSrc->getUnique_BOOLEAN()){
            $objSistemaAtributo->setUnique_BOOLEAN($objAtributoPrototipoWebOuAndroidSrc->getUnique_BOOLEAN());    
            $aconteceuAlteracao = true;
        }
        if( $objSistemaAtributo->getUnique_nome() != $objAtributoPrototipoWebOuAndroidSrc->getUnique_nome()){
            $objSistemaAtributo->setUnique_nome($objAtributoPrototipoWebOuAndroidSrc->getUnique_nome());
            $aconteceuAlteracao = true;
        }
        // se for chave extrangeira
        $idFKAtributo = $objAtributoPrototipoWebOuAndroidSrc->getFk_atributo_id_INT();
        if(strlen($idFKAtributo)){
            $objFKAtributo = $objAtributoPrototipoWebOuAndroidSrc->getObjFKAtributo();
            $nomeAtributoFK = $objFKAtributo->getNome();
            $varAtributoFK = strtoupper($nomeAtributoFK);

            $objFKTabela = $objAtributoPrototipoWebOuAndroidSrc->getObjFKTabela();
            $tabelaFK = $objFKTabela->getNome();
            $databaseWebOuSincronizadorWebTgt->query("SELECT id FROM sistema_tabela WHERE nome LIKE '".$tabelaFK."'");
            $idSistemaTabelaFk = $databaseWebOuSincronizadorWebTgt->getPrimeiraTuplaDoResultSet(0);

            if($objSistemaAtributo->getFk_nome() != $objAtributoPrototipoWebOuAndroidSrc->getFk_nome()){
                $objSistemaAtributo->setFk_nome($objAtributoPrototipoWebOuAndroidSrc->getFk_nome());    
                $aconteceuAlteracao = true;
            }

            if($objSistemaAtributo->getAtributo_fk() != $nomeAtributoFK){
                $objSistemaAtributo->setAtributo_fk($nomeAtributoFK);
                $aconteceuAlteracao = true;
            }

            if($objSistemaAtributo->getFk_sistema_tabela_id_INT() != $idSistemaTabelaFk){
                $objSistemaAtributo->setFk_sistema_tabela_id_INT($idSistemaTabelaFk);    
                $aconteceuAlteracao = true;
            }
            if($objSistemaAtributo->getDelete_tipo_fk() != $objAtributoPrototipoWebOuAndroidSrc->getDelete_tipo_fk()){
                $objSistemaAtributo->setDelete_tipo_fk($objAtributoPrototipoWebOuAndroidSrc->getDelete_tipo_fk());    
                $aconteceuAlteracao = true;
            }
            if($objSistemaAtributo->getUpdate_tipo_fk() != $objAtributoPrototipoWebOuAndroidSrc->getUpdate_tipo_fk()){
                $objSistemaAtributo->setUpdate_tipo_fk($objAtributoPrototipoWebOuAndroidSrc->getUpdate_tipo_fk());
                $aconteceuAlteracao = true;
            }
        } else{
            //se antes existia fk
            if(strlen($objSistemaAtributo->getFk_nome())){
                $objSistemaAtributo->setFk_nome(null);
                $objSistemaAtributo->setAtributo_fk(null);
                $objSistemaAtributo->setFk_sistema_tabela_id_INT(null);
                $objSistemaAtributo->setDelete_tipo_fk(null);
                $objSistemaAtributo->setUpdate_tipo_fk(null);
                $aconteceuAlteracao = true;
            }
        }
        

        if(strlen($idSistemaAtributo)){
            $ret = array();
            
            $ret["idSistemaAtributo"] = $idSistemaAtributo;
            if($aconteceuAlteracao){
                $objSistemaAtributo->setExcluido_BOOLEAN(0);
                $objSistemaAtributo->formatarParaSQL();
                $objSistemaAtributo->update($idSistemaAtributo);
                Helper::imprimirMensagem("O registro $nomeAtributo alterado", MENSAGEM_OK);
                $ret["operacao"] = "edicao";
                return $ret;    
            } else {
                Helper::imprimirMensagem("O registro $nomeAtributo não sofreu nenhuma modificação", MENSAGEM_WARNING);
                $ret["operacao"] = "sem_modificacao";
                $databaseWebOuSincronizadorWebTgt->query("UPDATE sistema_atributo SET excluido_BOOLEAN = '0' WHERE id = $idSistemaAtributo");

                return $ret;    
            }
        } else{
            $objSistemaAtributo->setExcluido_BOOLEAN(0);
            $objSistemaAtributo->formatarParaSQL();
            $objSistemaAtributo->insert();
            $ret = array();
            $ret["operacao"] = "insercao";
            return $ret;
        }
    }
        
    
    public static function atualizaSistemaAtributoDoBancoSincronizadorWeb(
        $objBancoSrc, 
        $objBancoTgt, 
        $db = null) {
//        $objBBSincronizadorWeb = new EXTDAO_Banco_banco();
//        $objBBSincronizadorWeb->select( $idBancoBancoSincronizadorWeb);
//        $databaseSW = $objBBSincronizadorWeb->getObjDatabase();
//        
        if($db == null)
            $db = new Database();
//            $vObjBancoHomologacao->select($vIdBancoHomologacao);
        $dbSrc = $objBancoSrc->getObjDatabase();
        $dbTgt = $objBancoTgt->getObjDatabase();
        $dbTgt->query("SELECT id FROM sistema_atributo");
        $idsSistemaAtributoTgt = Helper::getResultSetToArrayDeUmCampo($dbTgt->result);
        $idsDoBancoSrcQueJaExistiamNoTgt = array();
        $dbSrc->query("SELECT
id,
nome,
sistema_tabela_id_INT,
tipo_sql,
tamanho_INT,
decimal_INT,
not_null_BOOLEAN,
primary_key_BOOLEAN,
auto_increment_BOOLEAN,
valor_default,
fk_sistema_tabela_id_INT,
atributo_fk,
fk_nome,
update_tipo_fk,
delete_tipo_fk,
label,
unique_BOOLEAN,
unique_nome,
seq_INT,
excluido_BOOLEAN
                FROM sistema_atributo
");
        
        
        if(mysqli_num_rows($dbSrc->result) > 0){
            mysqli_data_seek($dbSrc->result, 0);
        }
        Helper::imprimirMensagem("Banco src: {$dbSrc->getDBName()}. Banco tgt: {$dbTgt->getDBName()}");
        for($i=0; $registroSrc = mysqli_fetch_array($dbSrc->result, MYSQL_NUM); $i++){
            Helper::imprimirMensagem("Mapeando atributo $registroSrc[1] do banco src: {$dbSrc->getDBName()}. ".print_r($registroSrc, true));
//            if($registroSrc[1] == "rede_id_INT"){
//                echo "entrou 36546568: ";
//                print_r($registroSrc);
//                exit();
//            }
            $idSTSQL = Helper::formatarIntegerParaComandoSQL($registroSrc[10]);

            $reg1 = Helper::formatarStringParaComandoSQL($registroSrc[1]);
            $reg2 = Helper::formatarIntegerParaComandoSQL($registroSrc[2]);
            $reg3 = Helper::formatarStringParaComandoSQL($registroSrc[3]);
            $reg4 = Helper::formatarIntegerParaComandoSQL($registroSrc[4]);
            $reg5 = Helper::formatarIntegerParaComandoSQL($registroSrc[5]);
            $reg6 = Helper::formatarBooleanParaComandoSQL($registroSrc[6]);
            $reg7 = Helper::formatarBooleanParaComandoSQL($registroSrc[7]);
            $reg8 = Helper::formatarBooleanParaComandoSQL($registroSrc[8]);
            $reg9 = Helper::formatarStringParaComandoSQL($registroSrc[9]);
            $reg11 = Helper::formatarStringParaComandoSQL($registroSrc[11]);
            $reg12 = Helper::formatarStringParaComandoSQL($registroSrc[12]);
            $reg13 = Helper::formatarStringParaComandoSQL($registroSrc[13]);
            $reg14 = Helper::formatarStringParaComandoSQL($registroSrc[14]);
            $reg15 = Helper::formatarStringParaComandoSQL($registroSrc[15]);
            $reg16 = Helper::formatarBooleanParaComandoSQL($registroSrc[16]);
            $reg17 = Helper::formatarStringParaComandoSQL($registroSrc[17]);
            $reg18 = Helper::formatarIntegerParaComandoSQL($registroSrc[18]);
            $reg19 = Helper::formatarBooleanParaComandoSQL($registroSrc[19]);

            if(in_array($registroSrc[0], $idsSistemaAtributoTgt)){
                Helper::imprimirMensagem("UPDATE");
                $idsDoBancoSrcQueJaExistiamNoTgt[count($idsDoBancoSrcQueJaExistiamNoTgt)] = $registroSrc[0];
                
                 $qUpdate = "UPDATE sistema_atributo
SET nome = $reg1,
sistema_tabela_id_INT= $reg2,
tipo_sql= $reg3,
tamanho_INT= $reg4,
decimal_INT= $reg5,
not_null_BOOLEAN= $reg6,
primary_key_BOOLEAN= $reg7,
auto_increment_BOOLEAN= $reg8,
valor_default= $reg9,
fk_sistema_tabela_id_INT= $idSTSQL,
atributo_fk= $reg11,
fk_nome= $reg12,
update_tipo_fk= $reg13,
delete_tipo_fk= $reg14,
label= $reg15,
unique_BOOLEAN= $reg16,
unique_nome= $reg17,
seq_INT= $reg18,
excluido_BOOLEAN= $reg19
WHERE id = '{$registroSrc[0]}'";
                $dbTgt->query($qUpdate);
//                Helper::imprimirMensagem($qUpdate);
            } else {
                Helper::imprimirMensagem("INSERT");
           
            $qInsert = "INSERT sistema_atributo (   
id,
nome,
sistema_tabela_id_INT,
tipo_sql,
tamanho_INT,
decimal_INT,
not_null_BOOLEAN,
primary_key_BOOLEAN,
auto_increment_BOOLEAN,
valor_default,
fk_sistema_tabela_id_INT,
atributo_fk,
fk_nome,
update_tipo_fk,
delete_tipo_fk,
label,
unique_BOOLEAN,
unique_nome,
seq_INT,
excluido_BOOLEAN)
                      VALUES(
                           '{$registroSrc[0]}',
                           $reg1, 
                           $reg2, 
                           $reg3, 
                           $reg4,
                           $reg5, 
                           $reg6,
                           $reg7,
                           $reg8,
                            $reg9,
                            $idSTSQL,
                            $reg11,
                            $reg12,
                            $reg13,
                            $reg14,
                            $reg15,
                            $reg16,
                            $reg17,
                            $reg18,
                            $reg19
                       )";    
                $dbTgt->query($qInsert);
            }
        }
        for($i = 0 ; $i < count($idsSistemaAtributoTgt); $i++){
            if(!in_array($idsSistemaAtributoTgt[$i], $idsDoBancoSrcQueJaExistiamNoTgt)){
                $dbTgt->query("UPDATE sistema_atributo "
                    . " SET excluido_BOOLEAN = '1' "
                    . " WHERE id = '{$idsSistemaAtributoTgt[$i]}'");
            }
        }
//        exit();
    }
    
    public static function atualizaSistemaAtributoDoBanco(
        $objBancoWeb, 
        $objBancoAndroid, 
        $objBancoSincronizadorWeb, 
        $idPVPrototipoAndroid,
        $idPVPrototipoWeb,
        $db = null) {

        if($db == null)
            $db = new Database();
        $idBancoWeb = $objBancoWeb->getId();
        $idBancoAndroid = $objBancoAndroid->getId();
        
        $dbWeb = $objBancoWeb->getObjDatabase();
//        $dbSincronizadorWeb = $objBancoSincronizadorWeb->getObjDatabase();
        
        $db->query("SELECT t.nome, 
                        t.transmissao_web_para_mobile_BOOLEAN, 
                        t.transmissao_mobile_para_web_BOOLEAN , 
                        t.frequencia_sincronizador_INT,
                        t.id
                    FROM tabela t 
                    WHERE t.projetos_versao_id_INT = " . $idPVPrototipoWeb." 
                        AND t.banco_id_INT = $idBancoWeb");
        $registrosTabelaDoPrototipoWeb = Helper::getResultSetToMatriz($db->result);
        
        $db->query("SELECT t.nome, 
                        t.transmissao_web_para_mobile_BOOLEAN, 
                        t.transmissao_mobile_para_web_BOOLEAN , 
                        t.frequencia_sincronizador_INT,
                        t.id
                    FROM tabela t
                    WHERE t.projetos_versao_id_INT = " . $idPVPrototipoAndroid." 
                        AND t.banco_id_INT = $idBancoAndroid");
        $registrosTabelaDoPrototipoAndroid = Helper::getResultSetToMatriz($db->result, 0 , 1);

        $objAtributoPrototipoWebOuAndroid = new EXTDAO_Atributo();
        for ($i = 0; $i < count($registrosTabelaDoPrototipoWeb); $i++) {
            //se transmissao_web_para_mobile_BOOLEAN == 1
            //
            //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
            //da 'tabela' deve ser atualizada
//                $idSistemaTabelaWeb = array_search($registrosTabelaDoPrototipoWeb[$i][0], $tabelasDoSistemaTabelaWeb);
            $tabelaPrototipoWeb = $registrosTabelaDoPrototipoWeb[$i][0];
            $idTabelaPrototipoWeb = $registrosTabelaDoPrototipoWeb[$i][4];
            
            $dbWeb->query("SELECT id
                          FROM sistema_tabela
                          WHERE nome = '$tabelaPrototipoWeb'");
            
            $idSistemaTabelaWeb = $dbWeb->getPrimeiraTuplaDoResultSet(0);
            $ret =  null;
            if(!strlen($idSistemaTabelaWeb)){
                Helper::imprimirMensagem("Sistema tabela não mapeado no banco web: $tabelaPrototipoWeb", MENSAGEM_ERRO);
                exit();
            }
            $dbWeb->query("SELECT id, nome
                          FROM sistema_atributo
                          WHERE sistema_tabela_id_INT = $idSistemaTabelaWeb");
            
            $atributosSistemaAtributoWeb =  Helper::getResultSetToMatriz($dbWeb->result);
            
            $atributosSistemaAtributoWebPorFlagEncontrada = array();
            $identificadoresAtributoPrototipoWeb = EXTDAO_Tabela::getVetorIdentificadorAtributo($idTabelaPrototipoWeb, $db);
//            print_r($identificadoresAtributoPrototipoWeb);
//            exit();
            
            for($j = 0 ; $j < count($identificadoresAtributoPrototipoWeb); $j++){
                $idAtributo = $identificadoresAtributoPrototipoWeb[$j][0];
                $nomeAtributo = $identificadoresAtributoPrototipoWeb[$j][1];

                $objAtributoPrototipoWebOuAndroid->select($idAtributo);
                
                $ret = EXTDAO_Sistema_atributo::atualizaOuInsereAtributo(
                    $dbWeb, $objAtributoPrototipoWebOuAndroid, $idSistemaTabelaWeb);
                
                if($ret["operacao"] == "edicao" || $ret["operacao"] == "sem_modificacao"){
                    $idSistemaAtributo = $ret["idSistemaAtributo"];
                    $atributosSistemaAtributoWebPorFlagEncontrada[$nomeAtributo] = true;
                    $dbWeb->query("UPDATE sistema_atributo
                                SET excluida_BOOLEAN = 0
                                WHERE id = $idSistemaAtributo ");
                }
                
            }
            //REMOVER OS ATRIBUTOS ANTIGOS
            //Percorre os registros da tabela 'sistema_tabela' 
            //retira as tabelas que não foram encontradas
            for($i = 0 ; $i < count($atributosSistemaAtributoWeb); $i++){
               $id = $atributosSistemaAtributoWeb[$i][0];
               $nome = $atributosSistemaAtributoWeb[$i][1];
                //se a tabela contida no sistema_tabela 
                //nao foi encontrada na atual estrutura da base web
                if($atributosSistemaAtributoWebPorFlagEncontrada[$nome]!= true ){
                     
                    $dbWeb->query("UPDATE sistema_atributo "
                        . " SET excluido_BOOLEAN = '1' "
                        . " WHERE id='$id' ");
                }
            }
        }
        
         for ($i = 0; $i < count($registrosTabelaDoPrototipoAndroid); $i++) {
            //se transmissao_web_para_mobile_BOOLEAN == 1
            //
            //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
            //da 'tabela' deve ser atualizada
//                $idSistemaTabelaWeb = array_search($registrosTabelaDoPrototipoWeb[$i][0], $tabelasDoSistemaTabelaWeb);
            $idTabelaPrototipoAndroid = $registrosTabelaDoPrototipoAndroid[$i][4];
            $nomeTabelaPrototipoAndroid = $registrosTabelaDoPrototipoAndroid[$i][0];
            
            $dbWeb->query("SELECT id
                          FROM sistema_tabela
                          WHERE nome = '$nomeTabelaPrototipoAndroid'");
            $idSistemaTabelaWeb = $dbWeb->getPrimeiraTuplaDoResultSet(0);
            $dbWeb->query("SELECT id, nome
                          FROM sistema_atributo
                          WHERE sistema_tabela_id_INT = $idSistemaTabelaWeb");
            
            $atributosSistemaAtributoWeb =  Helper::getResultSetToMatriz($dbWeb->result);
            

            $atributosSistemaAtributoWebPorFlagEncontrada = array();
           
            $identificadoresAtributoPrototipoWeb = EXTDAO_Tabela::getVetorIdentificadorAtributo($idTabelaPrototipoAndroid, $db);
            
            for($j = 0 ; $j < $identificadoresAtributoPrototipoWeb; $j++){
                $idAtributo = $identificadoresAtributoPrototipoWeb[$j][0];
                $nomeAtributo = $identificadoresAtributoPrototipoWeb[$j][1];

                $objAtributoPrototipoWebOuAndroid->select($idAtributo);
                $ret = EXTDAO_Sistema_atributo::atualizaOuInsereAtributo(
                    $dbWeb, $objAtributoPrototipoWebOuAndroid, $idSistemaTabelaWeb);
                
                if($ret["operacao"] == "edicao" || $ret["operacao"] == "sem_modificacao"){
                    $atributosSistemaAtributoWebPorFlagEncontrada[$nomeAtributo] = true;
                    $idSistemaAtributo = $ret["idSistemaAtributo"];
                    $dbWeb->query("UPDATE sistema_atributo SET excluido_BOOLEAN = '0' "
                        . " WHERE id=$idSistemaAtributo ");
                }
                
            }
            
            //Percorre os registros da tabela 'sistema_tabela' 
            //retira as tabelas que não foram encontradas
            for($i = 0 ; $i < count($atributosSistemaAtributoWeb); $i++){
               $id = $atributosSistemaAtributoWeb[$i][0];
               $nome = $atributosSistemaAtributoWeb[$i][0];
                //se a tabela contida no sistema_tabela 
                //nao foi encontrada na atual estrutura da base web
                if($atributosSistemaAtributoWebPorFlagEncontrada[$nome] != true ){
                     
                    $dbWeb->query("UPDATE sistema_atributo "
                        . " SET excluido_BOOLEAN = '1' "
                        . " WHERE nome='$nome' "
                        . "     AND sistema_tabela_id_INT = $idSistemaTabelaWeb ");
                }
            }

            
            //REMOVER OS ATRIBUTOS ANTIGOS
        }

    }
    
    
}

    
