<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Script_comando_banco_cache
    * NOME DA CLASSE DAO: DAO_Script_comando_banco_cache
    * DATA DE GERAÇÃO:    14.02.2015
    * ARQUIVO:            EXTDAO_Script_comando_banco_cache.php
    * TABELA MYSQL:       script_comando_banco_cache
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Script_comando_banco_cache extends DAO_Script_comando_banco_cache
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Script_comando_banco_cache";

           

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_script_comando_banco_id_INT = "Script do Comando";
			$this->label_tabela_chave_id_INT = "Chave da Tabela";
			$this->label_tabela_chave_tabela_chave_id_INT = "Relacionamento Entre A Chave da Tabela de Homologação E Produção";
			$this->label_nome_chave = "Nome da Chave";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Script_comando_banco_cache();

        }
        
        public static function getNomeChavePeloIdAtributo(
            $idAtributo, 
            $db = null){
//script_comando_banco_id_INT
//tabela_chave_id_INT
//tabela_chave_tabela_chave_id_INT
//nome_chave
            if($db == null) $db = new Database();
            $q = "SELECT nome_chave "
                . " FROM script_comando_banco_cache "
                . " WHERE atributo_id_INT = $idAtributo ";
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        

        public static function getNomeChavePeloIdTabelaChave(
            $idTabelaChave, 
            $db = null){
//script_comando_banco_id_INT
//tabela_chave_id_INT
//tabela_chave_tabela_chave_id_INT
//nome_chave
            if($db == null) $db = new Database();
            $q = "SELECT nome_chave "
                . " FROM script_comando_banco_cache"
                . " WHERE tabela_chave_id_INT = $idTabelaChave ";
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
	}

    
