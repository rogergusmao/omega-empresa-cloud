<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Script_comando_banco
    * NOME DA CLASSE DAO: DAO_Script_comando_banco
    * DATA DE GERAÃ‡ÃƒO:    04.04.2013
    * ARQUIVO:            EXTDAO_Script_comando_banco.php
    * TABELA MYSQL:       script_comando_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Script_comando_banco extends DAO_Script_comando_banco
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Script_comando_banco";

         

        }
        public function clear(){
            unset($this->id);
            unset($this->consulta);
            unset($this->seq_INT);
            unset($this->script_tabela_tabela_id_INT);

            unset($this->tipo_comando_banco_id_INT);

            unset($this->atributo_atributo_id_INT);

            unset($this->tabela_chave_tabela_chave_id_INT);

        }
//        function select($id)
//        {
//
//            $sql =  "SELECT *  FROM script_comando_banco WHERE id = $id;";
//            $this->database->query($sql);
//            $result = $this->database->result;
//            $row = $this->database->fetchObject($result);
//
//
//            $this->id = $row->id;
//
//            $this->consulta = $row->consulta;
//
//            $this->seq_INT = $row->seq_INT;
//
//            $this->script_tabela_tabela_id_INT = $row->script_tabela_tabela_id_INT;
//
//                $this->getFkObjScript_tabela_tabela()->select($this->script_tabela_tabela_id_INT);
//
//            $this->tipo_comando_banco_id_INT = $row->tipo_comando_banco_id_INT;
//
//                            $this->getFkObjTipo_comando_banco()->select($this->tipo_comando_banco_id_INT);
//
//            $this->atributo_atributo_id_INT = $row->atributo_atributo_id_INT;
//
//                            $this->getFkObjAtributo_atributo()->select($this->atributo_atributo_id_INT);
//
//            $this->tabela_chave_tabela_chave_id_INT = $row->tabela_chave_tabela_chave_id_INT;
//
//                            $this->getFkObjTabela_chave_tabela_chave()->select($this->tabela_chave_tabela_chave_id_INT);
//
//
//        }
//        public function formatarParaSQL(){
//                if(strlen($this->getConsulta())){
//                    $this->consulta = Helper::formatarCampoConsultaSQLParaSQL($this->getConsulta());
//                }
//		if($this->seq_INT == ""){
//
//			$this->seq_INT = "null";
//
//		}
//
//		if($this->script_tabela_tabela_id_INT == ""){
//
//			$this->script_tabela_tabela_id_INT = "null";
//
//		}
//
//		if($this->tipo_comando_banco_id_INT == ""){
//
//			$this->tipo_comando_banco_id_INT = "null";
//
//		}
//
//		if($this->atributo_atributo_id_INT == ""){
//
//			$this->atributo_atributo_id_INT = "null";
//
//		}
//
//                if($this->tabela_chave_tabela_chave_id_INT == ""){
//
//			$this->tabela_chave_tabela_chave_id_INT = "null";
//
//		}
//
//
//
//    }
    

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_consulta = "Consulta";
			$this->label_seq_INT = "SequÃªncia";
			$this->label_script_comando_id_INT = "Script do Comando";
                        $this->label_script_tabela_tabela_id_INT ="Relacionado as tabelas de homologaÃ§Ã£o e produÃ§Ã£o";
			$this->label_tipo_comando_banco_id_INT = "Tipo de comando";
			$this->label_atributo_atributo_id_INT =  "Relacionado aos atributos de homologaÃ§Ã£o e produÃ§Ã£o";

        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Script_comando_banco();

        }
        
        
        function setConsulta($val)
        {
            if(strlen($val)){
                $i = 0 ;
                for(; $i < strlen($val); $i++){
                    if( substr($val, $i, 1) != " " && substr($val, $i, 1) != "\t" && substr($val, $i, 1) != "\n") break;
                }
                if($i > 0 ){
                    $val = substr ($val, $i);
                }
                $this->consulta = $val;
            } else{
                $this->consulta =  null;
            }
            
        }
        
	}

    
