<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Script_projetos_versao_banco_banco
    * NOME DA CLASSE DAO: DAO_Script_projetos_versao_banco_banco
    * DATA DE GERAÇÃO:    05.04.2013
    * ARQUIVO:            EXTDAO_Script_projetos_versao_banco_banco.php
    * TABELA MYSQL:       script_projetos_versao_banco_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Script_projetos_versao_banco_banco extends DAO_Script_projetos_versao_banco_banco
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Script_projetos_versao_banco_banco";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_is_atualizacao_BOOLEAN = "Atualização do Banco de Homologação Para Produção";
			$this->label_projetos_versao_id_INT = "Versão do Projeto";
			$this->label_banco_banco_id_INT = "Relacionamento do Banco do Projeto";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Script_projetos_versao_banco_banco();

        }
        
        public static function getIdScript($pIdProjetosVersaoBancoBanco, $pIdTipoScriptBanco){
            $vConsulta = "SELECT id
                FROM script_projetos_versao_banco_banco
                WHERE projetos_versao_banco_banco_id_INT = {$pIdProjetosVersaoBancoBanco}
                AND tipo_script_banco_id_INT = $pIdTipoScriptBanco";
            $db = new Database();
            $db->query($vConsulta);
            $vId = Helper::getResultSetToPrimeiroResultado($db->result);
            return strlen($vId ) == 0 ? null : $vId;
        }
        
        public static function getObjDatabaseDoBancoProducao($pIdScriptProjetosVersaoBancoBanco){
            $db = new Database();
            $vConsulta = "SELECT bb.producao_banco_id_INT
                FROM script_projetos_versao_banco_banco spvbb JOIN projetos_versao_banco_banco pvbb 
                        ON spvbb.projetos_versao_banco_banco_id_INT = pvbb.id
                    JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
                WHERE bb.producao_banco_id_INT IS NOT NULL 
                    AND spvbb.id = {$pIdScriptProjetosVersaoBancoBanco}";
            $db->query($vConsulta);
            $vIdBanco = Helper::getResultSetToPrimeiroResultado($db->result);
            $obj = new EXTDAO_Banco();
            $obj->select($vIdBanco);
            return $obj->getObjDatabase();
        }
        
        public static function getListaIdScriptComandoBanco(
            $pIdScriptProjetosVersaoBancoBanco, 
            $vetorTipoOperacaoAtualizacaoBanco,
            $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT 
                    scb.id
                FROM script_comando_banco scb JOIN script_tabela_tabela stt 
                    ON scb.script_tabela_tabela_id_INT = stt.id
                WHERE stt.script_projetos_versao_banco_banco_id_INT = {$pIdScriptProjetosVersaoBancoBanco}
                    AND stt.tipo_operacao_atualizacao_banco_id_INT IN (".Helper::arrayToString($vetorTipoOperacaoAtualizacaoBanco, ",").")
                ORDER BY scb.script_tabela_tabela_id_INT, scb.seq_INT";
            
            $db->query($q);
            $resultSet = $db->result;
            return Helper::getResultSetToArrayDeUmCampo($resultSet);
            
        }
        
        
        
        public static function atualizaBancoHomologacao($pIdScriptProjetosVersaoBancoBanco){
            $db = new Database();
            
            $db->query("SELECT 
                    scb.consulta,
                    scb.script_tabela_tabela_id_INT, 
                    scb.seq_INT
                FROM script_comando_banco scb JOIN script_tabela_tabela stt 
                    ON scb.script_tabela_tabela_id_INT = stt.id
                WHERE stt.script_projetos_versao_banco_banco_id_INT = {$pIdScriptProjetosVersaoBancoBanco}
                ORDER BY scb.script_tabela_tabela_id_INT, scb.seq_INT");
             $resultSet = $db->result;
            if(mysqli_num_rows($resultSet) > 0){
                mysqli_data_seek($resultSet, 0);
            }
            $dbEstrutura= EXTDAO_Script_projetos_versao_banco_banco::getObjDatabaseDoBancoProducao($pIdScriptProjetosVersaoBancoBanco);
            $dbEstrutura->query("SET FOREIGN_KEY_CHECKS=0");
            $vErro = false;
            for($i=0; $regs = mysqli_fetch_array($db->result, MYSQL_BOTH); $i++){
               try {
                   $vConsulta = $regs[0];
                   $dbEstrutura->query($vConsulta);
               } catch (Exception $exc) {
                   Helper::imprimirMensagem($exc->getMessage(), MENSAGEM_ERRO);
                   $vErro = true;
               }
            }
            if(!$vErro)
                Helper::imprimirMensagem("Estrutura atualizada com sucesso.");
        }
        
        
        public static function criaArquivoEstruturaXML($pIdScriptProjetosVersaoBancoBanco, $pathArquivo){
            $vArquivo = fopen($pathArquivo, "w+");
            fwrite($vArquivo, "<?xml version=\"1.0\"?>");
            $db = new Database();
            $db->query("SELECT id,
                    is_atualizacao_BOOLEAN,
                    projetos_versao_banco_banco_id_INT,
                    data_criacao_DATETIME,
                    data_atualizacao_DATETIME 
                FROM script_projetos_versao_banco_banco
                WHERE id = {$pIdScriptProjetosVersaoBancoBanco}
                LIMIT 0,1");
            
            $vVetor = Helper::getResultSetToMatriz($db->result);
            if(count($vVetor) == 0 ) return;
            $vObjSPVBB = $vVetor[0];
                    
            $vXML = "
                <raiz>
                    <script_projetos_versao_banco_banco>
                        <id>".$vObjSPVBB[0]."</id>
                        <is_atualizacao_BOOLEAN>".$vObjSPVBB[1]."</is_atualizacao_BOOLEAN>
                        <projetos_versao_banco_banco_id_INT>".$vObjSPVBB[2]."</projetos_versao_banco_banco_id_INT>
                        <data_criacao_DATETIME>".$vObjSPVBB[3]."</data_criacao_DATETIME>
                        <data_atualizacao_DATETIME>".$vObjSPVBB[4]."</data_atualizacao_DATETIME>
                    </script_projetos_versao_banco_banco>
        ";
            fwrite($vArquivo, $vXML);
            fflush($vArquivo);
            
            $db->query("SELECT scb.id, 
                    scb.consulta, 
                    scb.seq_INT, 
                    scb.script_tabela_tabela_id_INT, 
                    scb.tipo_comando_banco_id_INT, 
                    scb.atributo_atributo_id_INT
                FROM script_comando_banco scb JOIN script_tabela_tabela stt 
                            ON scb.script_tabela_tabela_id_INT = stt.id
                     JOIN tipo_comando_banco tcb ON tcb.id = scb.tipo_comando_banco_id_INT
                WHERE stt.script_projetos_versao_banco_banco_id_INT = {$pIdScriptProjetosVersaoBancoBanco}
                    AND tcb.modifica_estrutura_BOOLEAN = 0
                ORDER BY scb.script_tabela_tabela_id_INT, scb.seq_INT");
            
            $resultSet = $db->result;
            if(mysqli_num_rows($resultSet) > 0){
                mysqli_data_seek($resultSet, 0);
            }
            
            for($i=0; $regs = mysqli_fetch_array($resultSet, MYSQL_BOTH); $i++){
                //<?xml version=\"1.0\"
                $vAtributoAtributo = $regs[4];
                if(!strlen($vAtributoAtributo)) 
                    $vAtributoAtributo = "null";
                
                $vXML = "
                    <script_comando_banco>
                        <id>".$regs[0]."</id>
                        <consulta>".$regs[1]."</consulta>
                        <seq_INT>".$regs[2]."</seq_INT>
                        <script_tabela_tabela_id_INT>".$regs[3]."</script_tabela_tabela_id_INT>
                        <tipo_comando_banco_id_INT>".$regs[4]."</tipo_comando_banco_id_INT>
                        <atributo_atributo_id_INT>{$vAtributoAtributo}</atributo_atributo_id_INT>
                    </script_comando_banco>
        ";
                 if(strlen($vXML)){
                     fwrite($vArquivo, $vXML);
                     fflush($vArquivo);
                 }
            }
            fwrite($vArquivo, "</raiz>");
            fflush($vArquivo);
            fclose($vArquivo);
            return;

            
        }
        
        
        
        public static function criaArquivoScriptSQL($pIdScriptProjetosVersaoBancoBanco, $pathArquivo){
            $vArquivo = fopen($pathArquivo, "w+");
            
            $db = new Database();
            $db->query("SELECT id,
                    is_atualizacao_BOOLEAN,
                    projetos_versao_banco_banco_id_INT,
                    data_criacao_DATETIME,
                    data_atualizacao_DATETIME 
                FROM script_projetos_versao_banco_banco
                WHERE id = {$pIdScriptProjetosVersaoBancoBanco}
                LIMIT 0,1");
            
            $vVetor = Helper::getResultSetToMatriz($db->result);
            if(count($vVetor) == 0 ) return;
            
            
            $db->query("SELECT scb.id, 
                    scb.consulta, 
                    scb.seq_INT, 
                    scb.script_tabela_tabela_id_INT, 
                    scb.tipo_comando_banco_id_INT, 
                    scb.atributo_atributo_id_INT
                FROM script_comando_banco scb JOIN script_tabela_tabela stt 
                            ON scb.script_tabela_tabela_id_INT = stt.id
                     JOIN tipo_comando_banco tcb ON tcb.id = scb.tipo_comando_banco_id_INT
                WHERE stt.script_projetos_versao_banco_banco_id_INT = {$pIdScriptProjetosVersaoBancoBanco}
                    AND tcb.modifica_estrutura_BOOLEAN = 0
                ORDER BY scb.script_tabela_tabela_id_INT, scb.seq_INT");
            
            $resultSet = $db->result;
            if(mysqli_num_rows($resultSet) > 0){
                mysqli_data_seek($resultSet, 0);
            }
            
            for($i=0; $regs = mysqli_fetch_array($resultSet, MYSQL_BOTH); $i++){
                
                
                $vXML = $regs[1]."
                    ";
                 if(strlen($vXML)){
                     fwrite($vArquivo, $vXML);
                     fflush($vArquivo);
                 }
            }
            fclose($vArquivo);
            return;

            
        }

        public static function getProximoSeqScriptComandoBanco($scriptScriptProjetosVersaoBancoBanco){
            $vConsulta = "SELECT MAX(scb.seq_INT)
                FROM script_tabela_tabela stt JOIN
                    script_comando_banco scb ON
                    scb.script_tabela_tabela_id_INT = stt.id
                WHERE stt.script_projetos_versao_banco_banco_id_INT ={$scriptScriptProjetosVersaoBancoBanco}";
            $db = new Database();
            $db->query($vConsulta);
            $id = $db->getPrimeiraTuplaDoResultSet(0);
            if($id == null || !strlen($id))
                $id = 1;
            else $id += 1;
            
            return "{$id}";
        }
        

	}

    
