<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Bairro
    * NOME DA CLASSE DAO: DAO_Bairro
    * DATA DE GERAÇÃO:    06.08.2011
    * ARQUIVO:            EXTDAO_Bairro.php
    * TABELA MYSQL:       bairro
    * BANCO DE DADOS:     websaude
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Bairro extends DAO_Bairro
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Bairro";

         


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_cidade_id_INT = "Cidade";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Bairro();

        }

	}

    
