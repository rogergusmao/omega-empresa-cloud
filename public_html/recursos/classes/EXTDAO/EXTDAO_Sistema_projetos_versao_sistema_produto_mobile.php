<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_projetos_versao_sistema_produto_mobile
    * NOME DA CLASSE DAO: DAO_Sistema_projetos_versao_sistema_produto_mobile
    * DATA DE GERAÃ‡ÃƒO:    20.03.2013
    * ARQUIVO:            EXTDAO_Sistema_projetos_versao_sistema_produto_mobile.php
    * TABELA MYSQL:       sistema_projetos_versao_sistema_produto_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Sistema_projetos_versao_sistema_produto_mobile extends DAO_Sistema_projetos_versao_sistema_produto_mobile
    {
        const ARQUIVO_XML_PUBLICACAO_ANDROID = "publicacao_android.xml";
        const ARQUIVO_XML_INSTRUCOES_ATUALIZACAO = "instrucoes_atualizacao.xml";
        const ARQUIVO_PACOTE_COMPLETO = "pacote_completo.zip";
        CONST ARQUIVO_XML_SISTEMA_TABELA = "sistema_tabela.xml";
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_projetos_versao_sistema_produto_mobile";

        }
        
        
        public function setLabels(){

			$this->label_id = "Id";
			$this->label_sistema_produto_mobile_id_INT = "Sistema do Produto Mobile";
			$this->label_sistema_projetos_versao_id_INT = "VersÃ£o do Projeto do Sistema";
			$this->label_programa_ARQUIVO = "Arquivo do Programa";
			$this->label_sistema_projetos_versao_produto_id_INT = "VersÃ£o do Produto";
                        $this->label_version_code = "Version Code";
                        $this->label_version_name = "Version Name";
                        $this->label_banco_db_ARQUIVO = "Banco de Dados SQLite3 BinÃ¡rio";
                        $this->label_resetar_banco_no_fim_da_atualizacao_BOOLEAN = "Resetar o banco de dados no caso de atualizaÃ§Ã£o da apk?";
                        $this->label_enviar_dados_locais_do_sincronizador_BOOLEAN = "Tentar sincronizar os dados armazenados no telefone na versÃ£o antiga?";
                        
                        $this->label_pacote_completo_zipado_ARQUIVO = "Pacote Completo Zipado";
                        

        }

           
        public function setDiretorios(){

            

        }

        
        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();

        }
        
        public function getXMLPublicacaoMobile(){
        $xml = "<?xml version=\"1.0\"?>";
        $xml .= "<parametro_global>";
        $xml .= "<programa_arquivo>";
        $xml .= $this->getPrograma_ARQUIVO();
        $xml .= "</programa_arquivo>";
        
        $xml .= "<sistema_projetos_versao_sistema_produto_mobile>";
        $xml .= $this->getId();
        $xml .= "</sistema_projetos_versao_sistema_produto_mobile>";
        
        $xml .= "<sistema_produto_mobile>";
        $xml .= $this->getSistema_produto_mobile_id_INT();
        $xml .= "</sistema_produto_mobile>";
        
        $xml .= "<sistema_projetos_versao>";
        $xml .= $this->getSistema_projetos_versao_id_INT();
        $xml .= "</sistema_projetos_versao>";
        
        $xml .= "<sistema_projetos_versao_produto>";
        $xml .= $this->getSistema_projetos_versao_produto_id_INT();
        $xml .= "</sistema_projetos_versao_produto>";
        
        $xml .= "<sistema>";
        $xml .= $this->getFkObjSistema_projetos_versao()->getSistema_id_INT();
        $xml .= "</sistema>";
        
        $xml .= "<data_homologacao_sistema_projetos_versao>";
        $xml .= $this->getFkObjSistema_projetos_versao()->getData_homologacao_DATETIME();
        $xml .= "</data_homologacao_sistema_projetos_versao>";
        
        
        $xml .= "<data_producao_sistema_projetos_versao>";
        $xml .= $this->getFkObjSistema_projetos_versao()->getData_producao_DATETIME();
        $xml .= "</data_producao_sistema_projetos_versao>";
        
        $xml .= "<data_homologacao_sistema_projetos_versao_produto>";
        $xml .= $this->getFkObjSistema_projetos_versao_produto()->getData_homologacao_DATETIME();
        $xml .= "</data_homologacao_sistema_projetos_versao_produto>";
        
        $xml .= "<data_producao_sistema_projetos_versao_produto>";
        $xml .= $this->getFkObjSistema_projetos_versao_produto()->getData_producao_DATETIME();
        $xml .= "</data_producao_sistema_projetos_versao_produto>";
        
        $xml .= "<sistema_produto>";
        $xml .= $this->getFkObjSistema_produto_mobile()->getSistema_produto_id_INT();
        $xml .= "</sistema_produto>";
        
        
        $xml .= "<sistema_tipo_mobile>";
        $xml .= $this->getFkObjSistema_produto_mobile()->getSistema_tipo_mobile_id_INT();
        $xml .= "</sistema_tipo_mobile>";
        
        $objProjetosVersao = new EXTDAO_Projetos_versao();
        $objProjetosVersao->select($this->getFkObjSistema_projetos_versao()->getProjetos_versao_id_INT());
        
        $xml .= "<projetos>";
        $xml .= $objProjetosVersao->getProjetos_id_INT();
        $xml .= "</projetos>";
        
        $xml .= "</parametro_global>";
        return $xml;
        }
        public static function copiaArquivosDoProjetosVersaoBancoBanco($id, $dirAssets){
            $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
            $obj->select($id);
            $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
            $objSPVP->select($obj->getSistema_projetos_versao_produto_id_INT());
            $idPVBB = $objSPVP->getPrototipo_projetos_versao_banco_banco_id_INT();
            $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBB->select($idPVBB);
            
            $srcDir = $objPVBB->getDiretorioDefault();
            
            $destDir = $objSPVP->getDiretorioDefault();
            
            $srcScriptEstrutura = $objPVBB->getScript_estrutura_banco_hom_para_prod_ARQUIVO();
            $srcXMLEstrutura = $objPVBB->getXml_estrutura_banco_hom_para_prod_ARQUIVO();
            
            $destXMLEstrutura = $srcXMLEstrutura;
            $destScriptEstrutura = $srcScriptEstrutura;
            
            
            if(is_file($srcDir.$srcScriptEstrutura)){
                copy($srcDir.$srcScriptEstrutura, $destDir.$destScriptEstrutura);
                copy($srcDir.$srcScriptEstrutura, $dirAssets.$destScriptEstrutura);
                $objSPVP->setScript_estrutura_banco_hom_para_prod_ARQUIVO($destScriptEstrutura);
            }
            if(is_file($srcDir.$srcXMLEstrutura)){
                copy($srcDir.$srcXMLEstrutura, $destDir.$destXMLEstrutura);
                copy($srcDir.$srcXMLEstrutura, $dirAssets.$destXMLEstrutura);
                $objSPVP->setXml_estrutura_banco_hom_para_prod_ARQUIVO($destXMLEstrutura);    
            }
            
            
            $srcScriptRegistros = $objPVBB->getScript_registros_banco_hom_para_prod_ARQUIVO();
            $srcXMLRegistros = $objPVBB->getXml_registros_banco_hom_para_prod_ARQUIVO();
            
            $destXMLRegistros = $srcXMLRegistros;
            $destScriptRegistros = $srcScriptRegistros;
            
            if(is_file($srcDir.$srcScriptRegistros)){
                copy($srcDir.$srcScriptRegistros, $destDir.$destScriptRegistros);    
                copy($srcDir.$srcScriptRegistros, $dirAssets.$destScriptRegistros);    
                $objSPVP->setXml_registros_banco_hom_para_prod_ARQUIVO($destXMLRegistros);
                Helper::imprimirMensagem("XML Registros inexistentes.", MENSAGEM_WARNING);
            }
            
            if(is_file($srcDir.$srcXMLRegistros)){
                copy($srcDir.$srcXMLRegistros, $destDir.$destXMLRegistros);
                copy($srcDir.$srcXMLRegistros, $dirAssets.$destXMLRegistros);
                
                $objSPVP->setScript_registros_banco_hom_para_prod_ARQUIVO($destScriptRegistros);
                Helper::imprimirMensagem("Script Registros inexistentes.", MENSAGEM_WARNING);
            }
            
            
            $objSPVP->formatarParaSQL();
            $objSPVP->update($obj->getSistema_projetos_versao_produto_id_INT());
            
        }
        public static function criaSistemaTabelaXML($idPVBB, $destDir, $dirAssets){
            //TODO
            
            $bancoHomologacao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBB);
            $bancoHomologacao->query("SELECT id,
nome,
data_modificacao_estrutura_DATETIME,
frequencia_sincronizador_INT,
banco_mobile_BOOLEAN,
banco_web_BOOLEAN,
transmissao_web_para_mobile_BOOLEAN,
transmissao_mobile_para_web_BOOLEAN,
tabela_sistema_BOOLEAN,
excluida_BOOLEAN,
atributos_chave_unica
                FROM sistema_tabela");
            $matriz = Helper::getResultSetToMatriz($bancoHomologacao->result);
//            $destDir, $dirAssets
            $handleDir = fopen($destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_SISTEMA_TABELA,"w+");
            $handleAssets = fopen($dirAssets.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_SISTEMA_TABELA,
            "w+");
            fwrite($handleDir, "<?xml version=\"1.0\"?>");
            for($i = 0 ; $i< count($matriz); $i++){
                $tupla = $matriz[$i];
                $xml = "";
                $xml .= "<sistema_tabela>";
                $xml .= "   <id>{$tupla[0]}</id>";
                $xml .= "   <data_modificacao_estrutura_DATETIME>{$tupla[1]}</data_modificacao_estrutura_DATETIME>";
                $xml .= "   <nome>{$tupla[2]}</nome>";
                $xml .= "   <data_modificacao_estrutura_DATETIME>{$tupla[3]}</data_modificacao_estrutura_DATETIME>";
                $xml .= "   <frequencia_sincronizador_INT>{$tupla[4]}</frequencia_sincronizador_INT>";
                $xml .= "   <banco_mobile_BOOLEAN>{$tupla[5]}</banco_mobile_BOOLEAN>";
                $xml .= "   <banco_web_BOOLEAN>{$tupla[6]}</banco_web_BOOLEAN>";
                $xml .= "   <transmissao_web_para_mobile_BOOLEAN>{$tupla[7]}</transmissao_web_para_mobile_BOOLEAN>";
                $xml .= "   <transmissao_mobile_para_web_BOOLEAN>{$tupla[8]}</transmissao_mobile_para_web_BOOLEAN>";
                $xml .= "   <tabela_sistema_BOOLEAN>{$tupla[9]}</tabela_sistema_BOOLEAN>";
                $xml .= "</sistema_tabela>";
                fwrite($handleDir,$xml);
                fwrite($handleAssets,$xml);
                
            }
            fclose($handleDir);
            fclose($handleAssets);
        }
        
        
        public static function executaAnexarArquivosObrigatoriosNaApk($id = null){
            
            if($id == null)
            $id = Helper::POST(Param_Get::ID1);
            $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
            $obj->select($id);
            
            $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
            $objSPVP->select($obj->getSistema_projetos_versao_produto_id_INT());
            $idPVBB = $objSPVP->getPrototipo_projetos_versao_banco_banco_id_INT();
            
            $objDiretorio = new EXTDAO_Diretorio_android();
            $objDiretorio->select(EXTDAO_Diretorio_android::getIdDiretorio($idPVBB));
            
            $arqXML = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_PUBLICACAO_ANDROID;
            
            $destDir = $objSPVP->getDiretorioDefault();
            $dirAssets = $objDiretorio->getRaizAssets();
            EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::criaSistemaTabelaXML($idPVBB, $destDir, $dirAssets);
            $xmlConteudo = $obj->getXMLPublicacaoMobile();
            $handle = fopen($destDir.$arqXML, "w+");
            fwrite($handle, $xmlConteudo);
            fclose($handle);
            $obj->setXml_publicacao_ARQUIVO($arqXML);
            $obj->formatarParaSQL();
            $obj->update($id);
            
            //Copia arquivo de instrucoes XML para o assets;
            copy($destDir.$arqXML, $dirAssets.$arqXML);
            
            $ret->dirAssets = $dirAssets;
            $ret->arquivos = array();
            $ret->arquivos[count($ret->arquivos)] = $dirAssets.$arqXML;
            EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::copiaArquivosDoProjetosVersaoBancoBanco($id,  $dirAssets);
            
            $listaIdAntecessor = $objSPVP->getListaPrototipoPVBBAntecessor();
            $objSPVPAntecssor = new EXTDAO_Sistema_projetos_versao_produto();
            
            $xmlInstrucoes = "<?xml version=\"1.0\"?>
                ";
            
            for($i = 0 ; $i < count($listaIdAntecessor); $i++){
                $idSPVPAntecessor = $listaIdAntecessor[$i];
                $objSPVPAntecssor->select($idSPVPAntecessor);
                $dirAntecessor = $objSPVPAntecssor->getDiretorioDefault();
                $idSPVSPMAntecessor = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getIdDoSistemaProjetosVersaoProduto($idSPVPAntecessor);
                $objSPVSPMAntecessor = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
                $objSPVSPMAntecessor->select($idSPVSPMAntecessor);
                $xmlAntecessor= $objSPVPAntecssor->getXml_estrutura_banco_hom_para_prod_ARQUIVO();
                //Copia o script de atualizacao da estrutura da versao anterior para a atual
                copy($dirAntecessor.$xmlAntecessor, $dirAssets.EXTDAO_Sistema_projetos_versao_produto::getNomeXmlHomProd($idSPVPAntecessor));
                copy($dirAntecessor.$xmlAntecessor, $destDir.EXTDAO_Sistema_projetos_versao_produto::getNomeXmlHomProd($idSPVPAntecessor));
                $xmlInstrucoes .= "
                    <versao>
                        <arquivo_atualizacao_estrutura>".EXTDAO_Sistema_projetos_versao_produto::getNomeXmlHomProd($idSPVPAntecessor)."</arquivo_atualizacao_estrutura>
                        <sistema_projetos_versao_produto>".$idSPVPAntecessor."</sistema_projetos_versao_produto>
                        <sistema_projetos_versao_sistema_produto_mobile>".$idSPVSPMAntecessor."</sistema_projetos_versao_sistema_produto_mobile>
                        <resetar_banco_no_fim_da_atualizacao_BOOLEAN>".$objSPVSPMAntecessor->getResetar_banco_no_fim_da_atualizacao_BOOLEAN()."</resetar_banco_no_fim_da_atualizacao_BOOLEAN>
                        <enviar_dados_locais_do_sincronizador_BOOLEAN>".$objSPVSPMAntecessor->getEnviar_dados_locais_do_sincronizador_BOOLEAN()."</enviar_dados_locais_do_sincronizador_BOOLEAN>
                    </versao>
                    ";
                
            }
            $xmlInstrucoes .= "
                    <versao>
                        <arquivo_atualizacao_estrutura>".$objSPVP->getXml_estrutura_banco_hom_para_prod_ARQUIVO()."</arquivo_atualizacao_estrutura>
                        <sistema_projetos_versao_produto>".$objSPVP->getId()."</sistema_projetos_versao_produto>
                        <sistema_projetos_versao_sistema_produto_mobile>".$obj->getId()."</sistema_projetos_versao_sistema_produto_mobile>
                        <resetar_banco_no_fim_da_atualizacao_BOOLEAN>".$obj->getResetar_banco_no_fim_da_atualizacao_BOOLEAN()."</resetar_banco_no_fim_da_atualizacao_BOOLEAN>
                        <enviar_dados_locais_do_sincronizador_BOOLEAN>".$obj->getEnviar_dados_locais_do_sincronizador_BOOLEAN()."</enviar_dados_locais_do_sincronizador_BOOLEAN>
                    </versao>
                    ";
            
            $handleInstrucoesAssets = fopen($dirAssets.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_INSTRUCOES_ATUALIZACAO, "w+");
            fwrite($handleInstrucoesAssets, $xmlInstrucoes);
            fclose($handleInstrucoesAssets);
            $ret->arquivos[count($ret->arquivos)] = $dirAssets.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_INSTRUCOES_ATUALIZACAO;
            $handleInstrucoesDir = fopen($destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_INSTRUCOES_ATUALIZACAO, "w+");
            fwrite($handleInstrucoesDir, $xmlInstrucoes);
            fclose($handleInstrucoesDir);
            return $ret;
//            $url = EXTDAO_Projetos_versao_processo_estrutura::actionUrlExecutaProcesso(true);
            
//            return array("location: $url");
        }
        public static function getIdDoSistemaProjetosVersaoProduto($idSPVP){
            $q = "SELECT spvspm.*
FROM sistema_projetos_versao_sistema_produto_mobile spvspm 
		JOIN sistema_produto_mobile spm 
			ON spvspm.sistema_produto_mobile_id_INT = spm.id
		JOIN sistema_produto sp 
			ON sp.id = spm.sistema_produto_id_INT 
		JOIN sistema_projetos_versao_produto spvp 
			ON spvspm.sistema_projetos_versao_id_INT = spvp.sistema_projetos_versao_id_INT AND spvp.sistema_produto_id_INT = sp.id
WHERE spvp.id = $idSPVP AND spvp.sistema_produto_id_INT = spvp.sistema_produto_id_INT ";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        public static function executaProcessoUsuarioInformaDadosParaPublicacaoMobile(){
            
            $id = Helper::POST(Param_Get::ID1);
            $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
            $obj->select($id);
            
            $obj->__actionEdit();
            
            $url = EXTDAO_Projetos_versao_processo_estrutura::actionUrlExecutaProcesso(true);
            
            return array("location: $url");
        }
        public static function executaProcessoPublicVersaoDoSistemaMobile(){
            $deletarArquivos = Helper::POST(Param_Get::DELETAR_ARQUIVOS);
            if(!strlen($deletarArquivos))
                $deletarArquivos = false;
            $id = Helper::POST(Param_Get::ID1);
            $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
            $obj->select($id);
            
            $obj->__actionEdit();
//            
            $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
            $objSPVP->select($obj->getSistema_projetos_versao_produto_id_INT());
            
            $arqXML = $obj->getXml_publicacao_ARQUIVO();
            $destDir = $objSPVP->getDiretorioDefault();
            
            $objSPVP->setData_producao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objSPVP->formatarParaSQL();
            $objSPVP->update($obj->getSistema_projetos_versao_produto_id_INT());
            
            copy($obj->getDiretorioDefault().$obj->getPrograma_ARQUIVO(), $destDir.$obj->getPrograma_ARQUIVO());
            
            $listaIdAntecessor = $objSPVP->getListaPrototipoPVBBAntecessor();
            $objSPVPAntecssor = new EXTDAO_Sistema_projetos_versao_produto();
            $vetorArquivoPacote = array(
                        $destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_SISTEMA_TABELA,
                        $destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_XML_INSTRUCOES_ATUALIZACAO);
            
            if(strlen($objSPVP->getXml_estrutura_banco_hom_para_prod_ARQUIVO()))
                $vetorArquivoPacote[count($vetorArquivoPacote)] = $destDir.$objSPVP->getXml_estrutura_banco_hom_para_prod_ARQUIVO();
            
            if(strlen($objSPVP->getScript_estrutura_banco_hom_para_prod_ARQUIVO()))
                $vetorArquivoPacote[count($vetorArquivoPacote)] = $destDir.$objSPVP->getScript_estrutura_banco_hom_para_prod_ARQUIVO();
            
            if(strlen(strlen($arqXML)))
                $vetorArquivoPacote[count($vetorArquivoPacote)] =  $destDir.$arqXML;
            
            if(strlen($obj->getPrograma_ARQUIVO()))
                $vetorArquivoPacote[count($vetorArquivoPacote)] =  $destDir.$obj->getPrograma_ARQUIVO();
                    
            $vetorArquivoASerDeletado = array();
            for($i = 0 ; $i < count($listaIdAntecessor); $i++){
                $idSPVPAntecessor = $listaIdAntecessor[$i];
                $objSPVPAntecssor->select($idSPVPAntecessor);
                $dirAntecessor = $objSPVPAntecssor->getDiretorioDefault();
                
                //Adiciona ao pacote
                $vetorArquivoPacote[count($vetorArquivoPacote)] = EXTDAO_Sistema_projetos_versao_produto::getNomeXmlHomProd($idSPVPAntecessor);
                $vetorArquivoASerDeletado[count($vetorArquivoASerDeletado)] = $dirAntecessor.EXTDAO_Sistema_projetos_versao_produto::getNomeXmlHomProd($idSPVPAntecessor);
            }
            if(file_exists($destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO))
                unlink ($destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO);
            
            if(Helper::createZip(
                    $vetorArquivoPacote,
                    $destDir.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO,
                    true)
               ){
                $obj->setPacote_completo_zipado_ARQUIVO(EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO);
                $obj->formatarParaSQL();
                $obj->update($id);
            }
            for($i = 0 ; $i < count($vetorArquivoASerDeletado); $i ++){
                if(is_file($vetorArquivoASerDeletado[$i]))
                    unlink($vetorArquivoASerDeletado[$i]);
            }
            $url = EXTDAO_Projetos_versao_processo_estrutura::actionUrlExecutaProcesso(true);
            
            return array("location: $url");
        }
//
        public static function getIdDoPrototipoDoPVBB($idPVBB){
            $consulta = "SELECT DISTINCT spvspm.id
FROM sistema_projetos_versao_produto spvp
	JOIN sistema_projetos_versao_sistema_produto_mobile spvspm
            ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id
        JOIN sistema_produto sp 
            ON spvp.sistema_produto_id_INT = sp.id
        JOIN sistema s 
            ON sp.sistema_id_INT = s.id
WHERE spvp.prototipo_projetos_versao_banco_banco_id_INT = $idPVBB
    AND s.prototipo_BOOLEAN = '1'
ORDER BY spvspm.id DESC	
LIMIT 0,1";
            $db = new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public static function getListaVersao($idSistemaProdutoMobile, $idSitemaProjetosVersaoSistemaProdutoMobile){
          
            $consulta = "SELECT spvspm.id
                        FROM sistema_projetos_versao_sistema_produto_mobile spvspm JOIN
                            sistema_projetos_versao spv ON spvspm.sistema_projetos_versao_id_INT = spv.id
                        WHERE spvspm.sistema_produto_mobile_id_INT = {$idSistemaProdutoMobile} ";
            if(strlen($idSitemaProjetosVersaoSistemaProdutoMobile) 
                    && $idSitemaProjetosVersaoSistemaProdutoMobile != null)
                $consulta .= " AND spvspm.id > {$idSitemaProjetosVersaoSistemaProdutoMobile} ";
            $consulta .= " ORDER BY spvspm.id ASC";
            $db = new Database();
            $db->query($consulta);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
                    
        }
        
        public static function getIdDaUltimaVersao(
            $idSistemaProdutoMobile
            , $idSistemaProjetosVersaoSistemaProdutoMobile = null
            , $db = null){
            $query = "SELECT spvspm.id

FROM sistema_projetos_versao_sistema_produto_mobile spvspm

JOIN sistema_projetos_versao_produto spvp

ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id

JOIN sistema_produto_mobile spm

ON spm.id = spvspm.sistema_produto_mobile_id_INT

JOIN sistema_projetos_versao spv

ON spv.id = spvspm.sistema_projetos_versao_id_INT 

WHERE spm.id = $idSistemaProdutoMobile

AND spv.data_producao_DATETIME IS NULL

";
            if(strlen($idSistemaProjetosVersaoSistemaProdutoMobile))
                $query  .= " AND spvspm.id > $idSistemaProjetosVersaoSistemaProdutoMobile ";
$query .= " ORDER BY spv.data_producao_DATETIME DESC

LIMIT 0,1
                ";
if($db==null)
            $db = new Database();
            $db->query($query);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        
        
        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
//UPLOAD DO ARQUIVO Programa_ARQUIVO
                if(Helper::verificarUploadArquivo("programa_ARQUIVO{$i}")){
                    if(is_array($arquivo = $this->__uploadPrograma_ARQUIVO($i, $urlErro))){
                        $_POST["programa_ARQUIVO{$i}"] = $arquivo[0];
                        $this->setPrograma_ARQUIVO($arquivo[0]);
                    }
                }
                $this->formatarParaSQL();
                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

             
            

            
          
            $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
            $objSPVP->select($this->getSistema_projetos_versao_produto_id_INT());
            $idPrototipoPVBB = $objSPVP->getPrototipo_projetos_versao_banco_banco_id_INT();
            
            $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBB->select($idPrototipoPVBB);
            $diretorioPVBB = $objPVBB->getDiretorioDefault();
            
            
            
            
            $diretorioApk = $objSPVP->getDiretorioDefault();
            
            $programaArquivo = $this->getPrograma_ARQUIVO();
            $dirSPVSPM = $this->getDiretorioDefault();
            copy($dirSPVSPM.$programaArquivo, $diretorioApk.$programaArquivo);
            
            copy($diretorioPVBB.$objPVBB->getScript_estrutura_banco_hom_para_prod_ARQUIVO(), $diretorioApk.$objPVBB->getScript_estrutura_banco_hom_para_prod_ARQUIVO());
            $objSPVP->setScript_estrutura_banco_hom_para_prod_ARQUIVO($objPVBB->getScript_estrutura_banco_hom_para_prod_ARQUIVO());
            
            copy($diretorioPVBB.$objPVBB->getXml_estrutura_banco_hom_para_prod_ARQUIVO(), $diretorioApk.$objPVBB->getXml_estrutura_banco_hom_para_prod_ARQUIVO());
            $objSPVP->setXml_estrutura_banco_hom_para_prod_ARQUIVO($objPVBB->getXml_estrutura_banco_hom_para_prod_ARQUIVO());
            
            
            
            
            $objSPVP->formatarParaSQL();
            $objSPVP->update($this->getSistema_projetos_versao_produto_id_INT());
            
            Helper::createZip( Helper::getListaArquivoDoDiretorio($diretorioApk), $diretorioApk.EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO, true);
            
            $this->updateCampo($this->getId(), "pacote_completo_zipado_ARQUIVO", EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::ARQUIVO_PACOTE_COMPLETO);

    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }
        
	}

    
