<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Mobile_conectado
    * NOME DA CLASSE DAO: DAO_Mobile_conectado
    * DATA DE GERAÇÃO:    13.03.2013
    * ARQUIVO:            EXTDAO_Mobile_conectado.php
    * TABELA MYSQL:       mobile_conectado
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Mobile_conectado extends DAO_Mobile_conectado
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Mobile_conectado";



        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_identificador = "Identificador";
			$this->label_imei = "Imei";
			$this->label_data_login_DATETIME = "Data Login";
                        $this->label_data_atualizacao_DATETIME = "Data Atualização";
			$this->label_data_logout_DATETIME = "Data Logout";
                        $this->label_mobile_identificador_id_INT = "Identificador do Telefone";
                        $this->label_usuario_INT = "Usuário";
                        $this->label_corporacao_INT = "Corporação";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Mobile_conectado();

        }

        
//        public function telefoneConectado($idMobileIdentificador, $idUsuario, $idCorporacao){
//            $consulta = "SELECT mc.id
//                        FROM mobile_conectado mc
//                        WHERE mc.mobile_identificador_id_INT = {$idMobileIdentificador} ";
//            if(strlen($idUsuario))
//                $consulta .= " AND mc.usuario_INT = {$idUsuario} ";
//            if(strlen($idCorporacao))
//                $consulta .= " AND mc.corporacao_INT = {$idCorporacao} ";
//           
//            $consulta .= "LIMIT 0,1";
//            $db = new Database();
//            $db->query($consulta);
//            return $db->getPrimeiraTuplaDoResultSet("id");
//        }
        
        public static function consulta($idMobileIdentificador, $idUsuario, $idCorporacao, $db = null){
            $consulta = "SELECT mc.id
                        FROM mobile_conectado mc
                        WHERE mc.mobile_identificador_id_INT = {$idMobileIdentificador} ";
            if(strlen($idUsuario))
                $consulta .= " AND mc.usuario_INT = {$idUsuario} ";
            if(strlen($idCorporacao))
                $consulta .= " AND mc.corporacao_INT = {$idCorporacao} ";
           
            $consulta .= " LIMIT 0,1";
            if($db == null)
            $db = new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet("id");
        }
        
        
        
        public static function getTotalMobileConectado($idMobileConectado, $idUsuario, $idCorporacao){
            $consulta = "SELECT mc.id
                        FROM mobile_conectado mc
                        WHERE mc.mobile_identificador_id_INT = {$idMobileConectado} ";
            if(strlen($idUsuario))
                $consulta .= " AND mc.usuario_INT = {$idUsuario} ";
            if(strlen($idCorporacao))
                $consulta .= " AND mc.corporacao_INT = {$idCorporacao} ";
           
            $consulta .= "LIMIT 0,1";
            $db = new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet("id");
        }
        
        public static function getTotalMobileDesconectado($idMobileConectado, $idUsuario, $idCorporacao){
            $consulta = "SELECT mc.id
                        FROM mobile_conectado mc
                        WHERE mc.mobile_identificador_id_INT = {$idMobileConectado} ";
            if(strlen($idUsuario))
                $consulta .= " AND mc.usuario_INT = {$idUsuario} ";
            if(strlen($idCorporacao))
                $consulta .= " AND mc.corporacao_INT = {$idCorporacao} ";
           
            $consulta .= "LIMIT 0,1";
            $db = new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet("id");
        }

	}

    
