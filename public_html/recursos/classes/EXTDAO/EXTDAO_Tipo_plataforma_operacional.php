<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_plataforma_operacional
    * NOME DA CLASSE DAO: DAO_Tipo_plataforma_operacional
    * DATA DE GERAÇÃO:    11.06.2013
    * ARQUIVO:            EXTDAO_Tipo_plataforma_operacional.php
    * TABELA MYSQL:       tipo_plataforma_operacional
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_plataforma_operacional extends DAO_Tipo_plataforma_operacional
    {
        
        
        const WEB_PHP = 1;
        const NATIVO_ANDROID = 2;
        const NATIVO_IPHONE = 3;
        const NATIVO_WINDOWS_8_PHONE = 4;

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_plataforma_operacional";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_plataforma_operacional();

        }

	}

    
