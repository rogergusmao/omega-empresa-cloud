<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Estado_operacao_sistema_mobile
    * NOME DA CLASSE DAO: DAO_Estado_operacao_sistema_mobile
    * DATA DE GERAÇÃO:    31.03.2013
    * ARQUIVO:            EXTDAO_Estado_operacao_sistema_mobile.php
    * TABELA MYSQL:       estado_operacao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Estado_operacao_sistema_mobile extends DAO_Estado_operacao_sistema_mobile
    {

        //const TIPO = 1;
        const AGUARDANDO = 1;
        const PROCESSANDO = 2;
        const CONCLUIDA = 3;
        const ERRO_EXECUCAO = 4;
        const CANCELADA = 5;
        const AVULSA = 6;
        
       public static function imprimirMensagem($tipo){
           switch ($tipo) {
               case EXTDAO_Estado_operacao_sistema_mobile::AGUARDANDO:
                   Helper::imprimirMensagem("Aguardando o celular receber a operação de sistema.", MENSAGEM_INFO);
                   break;
                case EXTDAO_Estado_operacao_sistema_mobile::PROCESSANDO:
                    Helper::imprimirMensagem("O celular está processando a operação de sistema.", MENSAGEM_WARNING);
                   break;
               case EXTDAO_Estado_operacao_sistema_mobile::CONCLUIDA:
                   Helper::imprimirMensagem("A operação foi concluída com sucesso.", MENSAGEM_OK);
                   break;
               case EXTDAO_Estado_operacao_sistema_mobile::ERRO_EXECUCAO:
                   Helper::imprimirMensagem("Erro de execução da operação de sistema.", MENSAGEM_ERRO);
                   break;
               case EXTDAO_Estado_operacao_sistema_mobile::CANCELADA:
                   Helper::imprimirMensagem("Operação de sistema cancelada.", MENSAGEM_ERRO);
                   break;
               default:
                   break;
           }
           
       }
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Estado_operacao_sistema_mobile";


        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_nome = "";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Estado_operacao_sistema_mobile();

        }

	}

    
