<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_log_erro_script_comando_banco
    * NOME DA CLASSE DAO: DAO_Sistema_log_erro_script_comando_banco
    * DATA DE GERAÇÃO:    05.04.2013
    * ARQUIVO:            EXTDAO_Sistema_log_erro_script_comando_banco.php
    * TABELA MYSQL:       sistema_log_erro_script_comando_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_log_erro_script_comando_banco extends DAO_Sistema_log_erro_script_comando_banco
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_log_erro_script_comando_banco";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_script_comando_banco_id_INT = "Script do Comando de Banco";
			$this->label_sistema_log_erro_mobile_id_INT = "Relativo Ao Erro";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_log_erro_script_comando_banco();

        }

	}

    
