<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_versao
    * NOME DA CLASSE DAO: DAO_Projetos_versao
    * DATA DE GERAÃ‡ÃƒO:    28.01.2013
    * ARQUIVO:            EXTDAO_Projetos_versao.php
    * TABELA MYSQL:       projetos_versao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Projetos_versao extends DAO_Projetos_versao
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_versao";



        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_projetos_id_INT = "Projeto";
			$this->label_arquivo_atualizacao_mobile = "Arquivo de AtualizaÃ§Ã£o Estrutural do Banco Mobile";
			$this->label_arquivo_atualizacao_web = "Arquivo de AtualizaÃ§Ã£o Estrutural do Banco Web";
			$this->label_arquivo_desatualizacao_mobile = "Arquivo de Backup Estrutural do Banco Web";
			$this->label_arquivo_desatualizacao_web = "Arquivo de Backup Estrutural do Banco Mobile";
			$this->label_data_homologacao_DATETIME = "Data de HomologaÃ§Ã£o";
			$this->label_data_producao_DATETIME = "Data de ProduÃ§Ã£o";
			$this->label_homologacao_banco_id_INT = "Banco de HomologaÃ§Ã£o";
			$this->label_producao_banco_id_INT = "Banco de ProduÃ§Ã£o";
                        $this->label_copia_do_projetos_versao_id_INT = "CÃ³pia da versÃ£o de projeto";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_versao();

        }
        public static function publica($idPV){
            
            $obj = new EXTDAO_Projetos_versao();
            $obj->select($idPV);
            $dataPublicacao = Helper::getDiaEHoraAtualSQL();
            $obj->setData_producao_DATETIME($dataPublicacao);
            $idProjetos = $obj->getProjetos_id_INT();
            $obj->formatarParaSQL();
            $obj->update($idPV);
            return EXTDAO_Projetos_versao::cadastra($idProjetos, $idPV);
            
        }
        
//        function select($id)
//    {
//
//    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_homologacao_DATETIME) AS data_homologacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_producao_DATETIME) AS data_producao_DATETIME_UNIX FROM projetos_versao WHERE id = $id;";
//    	$this->database->query($sql);
//    	$result = $this->database->result;
//    	$row = $this->database->fetchObject($result);
//
//    
//        $this->id = $row->id;
//        
//        $this->nome = $row->nome;
//        
//        $this->projetos_id_INT = $row->projetos_id_INT;
//        if($this->projetos_id_INT)
//			$this->getFkObjProjetos()->select($this->projetos_id_INT);
//
//        $this->data_homologacao_DATETIME = $row->data_homologacao_DATETIME;
//        $this->data_homologacao_DATETIME_UNIX = $row->data_homologacao_DATETIME_UNIX;
//
//        $this->data_producao_DATETIME = $row->data_producao_DATETIME;
//        $this->data_producao_DATETIME_UNIX = $row->data_producao_DATETIME_UNIX;
//
//        $this->copia_do_projetos_versao_id_INT = $row->copia_do_projetos_versao_id_INT;
//
//			$this->getFkObjCopia_do_projetos_versao()->select($this->copia_do_projetos_versao_id_INT);
//
//        $this->tipo_analise_projeto_id_INT = $row->tipo_analise_projeto_id_INT;
//        if($this->tipo_analise_projeto_id_INT)
//			$this->getFkObjTipo_analise_projeto()->select($this->tipo_analise_projeto_id_INT);
//
//        $this->sinc_do_projetos_versao_id_INT = $row->sinc_do_projetos_versao_id_INT;
//
//		$this->getFkObjSinc_do_projetos_versao()->select($this->sinc_do_projetos_versao_id_INT);
//
//
//    }
        
        public static function cadastraCopiaProjetosVersao($idPVBB, $objPVPE, $db = null){
            $objPVBB = new EXTDAO_Projetos_versao_banco_banco($db);
            $objPVBB->select($idPVBB);
            $objPVCopia = new EXTDAO_Projetos_versao();
            $objPVCopia->select($objPVBB->getProjetos_versao_id_INT());
            $objPVCopia->setCopia_do_projetos_versao_id_INT($objPVBB->getProjetos_versao_id_INT());
            $objPVCopia->setTipo_analise_projeto_id_INT(EXTDAO_Tipo_analise_projeto::COMPARACAO);
            $objPVCopia->setId(null);
            $objPVCopia->formatarParaSQL();
            $objPVCopia->insert();
            $objPVCopia->selectUltimoRegistroInserido();
            //fazendo uma nova copia do projetos_versao_banco_banco
            $vCopiaPVBB = new EXTDAO_Projetos_versao_banco_banco();
            $vCopiaPVBB->select($objPVBB->getId());
            $vCopiaPVBB->setId(null);
            $vCopiaPVBB->setProjetos_versao_id_INT($objPVCopia->getId());
            $vCopiaPVBB->setCopia_do_projetos_versao_banco_banco_id_INT($objPVBB->getId());
            $vCopiaPVBB->setTipo_analise_projeto_id_INT(EXTDAO_Tipo_analise_projeto::COMPARACAO);
            $vCopiaPVBB->formatarParaSQL();
            $vCopiaPVBB->insert();
            $vCopiaPVBB->selectUltimoRegistroInserido();

            $objEditaPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
            $objEditaPVPE->select($objPVPE->getId());
            $objEditaPVPE->setCopia_projetos_versao_banco_banco_id_INT($vCopiaPVBB->getId());
            $objEditaPVPE->formatarParaSQL();
            $objEditaPVPE->update($objPVPE->getId());
        }
        
        public static function cadastra($idProjetos, $idPVUltimaVersao = null){
            $objNovo = new EXTDAO_Projetos_versao();
            if($idPVUltimaVersao){
                $objNovo->select($idPVUltimaVersao);
                $objNovo->setId(null);
                $objNovo->setData_producao_DATETIME(null);
            }
            $objNovo->setProjetos_id_INT($idProjetos);
            $objNovo->setData_homologacao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objNovo->formatarParaSQL();
            $objNovo->insert();
            $objNovo->selectUltimoRegistroInserido();
            
            if($idPVUltimaVersao != null){
                EXTDAO_Projetos_versao::copiaProjetosVersao($objNovo->getId(), $idPVUltimaVersao);
            }
            
            return $objNovo->getId();
        }
        
        public static function copiaProjetosVersao($idDestino, $idFonte){
            $objDest = new EXTDAO_Projetos_versao();
            $objDest->select($idDestino);
            $objFont = new EXTDAO_Projetos_versao();
            $objFont->select($idFonte);
            
            $vetorPVBBFonte = EXTDAO_Projetos_versao_banco_banco::getVetorId($idFonte);
            if($vetorPVBBFonte != null)
            foreach ($vetorPVBBFonte as $idPVBBFonte) {
                $objPVBBDest = new EXTDAO_Projetos_versao_banco_banco();
                $objPVBBDest->select($idPVBBFonte);
                $objPVBBDest->setId(null);
                $objPVBBDest->setProjetos_versao_id_INT($idDestino);
                $objPVBBDest->setScript_estrutura_banco_hom_para_prod_ARQUIVO(null);
                $objPVBBDest->setScript_registros_banco_hom_para_prod_ARQUIVO(null);
                $objPVBBDest->setXml_estrutura_banco_hom_para_prod_ARQUIVO(null);
                $objPVBBDest->setXml_registros_banco_hom_para_prod_ARQUIVO(null);
                $objPVBBDest->formatarParaSQL();
                $objPVBBDest->insert();
            }
            //Realizando a publicacao dos produtos do prototipo
            $idSistema = EXTDAO_Sistema::getSistemaPrototipoDoProjeto($objFont->getProjetos_id_INT());
            $idSPV = EXTDAO_Sistema_projetos_versao::consultaUltimaVersaoDoSistema($idSistema);
            
            //inicializando a nova versao do prototipo do sistema
            $objSPVNovo = new EXTDAO_Sistema_projetos_versao();
            
            $objSPVNovo->setSistema_id_INT($idSistema);
            $objSPVNovo->setProjetos_versao_id_INT($idDestino);
            $objSPVNovo->setData_homologacao_DATETIME(Helper::getDiaEHoraAtual());
            $objSPVNovo->setData_producao_DATETIME(null);
            $objSPVNovo->formatarParaSQL();
            $objSPVNovo->insert();
            $objSPVNovo->selectUltimoRegistroInserido();
            $listaDeProdutos = EXTDAO_Sistema_produto::getListaIdentificadorNo($idSistema);
            $objSPVPNovo = new EXTDAO_Sistema_projetos_versao_produto();
            $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
            for($i = 0 ; $i < count($listaDeProdutos); $i++){
                $regSP = $listaDeProdutos[$i];
                $idSP = $regSP[0];
                $idSPVP = EXTDAO_Sistema_projetos_versao_produto::getIdSPVPDaUltimaVersaoDoProduto($idSP);
                
                $idSPVSPM = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getIdDoSistemaProjetosVersaoProduto($idSPVP);
                $objSPVP->select($idSPVP);
                
                
                $objSPVPNovo->setServidor_projetos_versao_banco_banco_id_INT($objSPVP->getServidor_projetos_versao_banco_banco_id_INT());
                
                $idBBAntigo = $objSPVP->getFkObjPrototipo_projetos_versao_banco_banco()->getBanco_banco_id_INT();
                
                $idPVBBNovo = EXTDAO_Projetos_versao_banco_banco::getIdRelacionadoComSistemaProjetosVersaoProduto($objSPVNovo->getId(), $idBBAntigo);
                $objSPVPNovo->setPrototipo_projetos_versao_banco_banco_id_INT($idPVBBNovo);
                
                $objSPVPNovo->setSistema_produto_id_INT($idSP);
                $objSPVPNovo->setSistema_projetos_versao_id_INT($objSPVNovo->getId());
                $objSPVPNovo->setScript_estrutura_banco_hom_ARQUIVO(null);
                $objSPVPNovo->setScript_estrutura_banco_prod_ARQUIVO(null);
                $objSPVPNovo->setScript_registros_banco_hom_para_prod_ARQUIVO(null);
                $objSPVPNovo->setXml_registros_banco_hom_para_prod_ARQUIVO(null);
                $objSPVPNovo->setScript_estrutura_banco_hom_para_prod_ARQUIVO(null);
                $objSPVPNovo->setXml_estrutura_banco_hom_para_prod_ARQUIVO(null);
                
                $objSPVPNovo->setData_homologacao_DATETIME(Helper::getDiaEHoraAtual());
                $objSPVPNovo->setData_producao_DATETIME(null);
                $objSPVPNovo->formatarParaSQL();
                $objSPVPNovo->insert();
                $objSPVPNovo->selectUltimoRegistroInserido();
                
                if(strlen($idSPVSPM)){
                    $objSPVSPM = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
                    $objSPVSPM->select($idSPVSPM);
                    $objSPVSPMNovo = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
                    $objSPVSPMNovo->setId(null);
                    $objSPVSPMNovo->setSistema_produto_mobile_id_INT($objSPVSPM->getSistema_produto_mobile_id_INT());
                    $objSPVSPMNovo->setSistema_projetos_versao_produto_id_INT($objSPVPNovo->getId());
                    $objSPVSPMNovo->setSistema_projetos_versao_id_INT($objSPVNovo->getId());
                    $objSPVSPMNovo->formatarParaSQL();
                    $objSPVSPMNovo->insert();
                }
            }  
        }

        public static function getPathDiretorioScript($idPV){
            return GerenciadorArquivo::getPathDoTipo(
                $idPV,
                "Projetos_versao",
                GerenciadorArquivo::ID_SCRIPT
                );
        }
        
        
        
        public function atualizaEstruturaSincronizacao(){
            $q = "SELECT id
                FROM projetos_versao
                WHERE sinc_do_projetos_versao_id_INT = {$this->getId()}
                LIMIT 0,1";
            $db = new Database();
            $db->query($q);
            $idPVSinc = $db->getPrimeiraTuplaDoResultSet(0);
            
            if(!strlen($idPVSinc)){
                $objPVSincNovo = new EXTDAO_Projetos_versao();
                $objPVSincNovo->setNome($this->getNome());
                $objPVSincNovo->setProjetos_id_INT($this->getProjetos_id_INT());
                $objPVSincNovo->setData_homologacao_DATETIME(Helper::getDiaEHoraAtualSQL());
                $objPVSincNovo->setTipo_analise_projeto_id_INT(EXTDAO_Tipo_analise_projeto::SINCRONIZACAO);
                $objPVSincNovo->setSinc_do_projetos_versao_id_INT($this->getId());
                $objPVSincNovo->formatarParaSQL();
                $objPVSincNovo->insert();
                $objPVSincNovo->selectUltimoRegistroInserido();
                $idPVSinc = $objPVSincNovo->getId();
            }
            $q = "SELECT pvbb.id, bb.homologacao_banco_id_INT
                FROM projetos_versao_banco_banco pvbb JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.projetos_versao_id_INT = {$this->getId()}
                    AND pvbb.tipo_plataforma_operacional_id_INT IN (
                        ".EXTDAO_Tipo_plataforma_operacional::WEB_PHP."
                    )
                    AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO;
                
            $db->query($q);
            $idPVBBWeb = $db->getPrimeiraTuplaDoResultSet(0);
            
            $idBancoHomologacaoWeb = $db->getPrimeiraTuplaDoResultSet(1);
            $objPVBBWeb = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBBWeb->select($idPVBBWeb);
             $q = "SELECT pvbb.id, bb.homologacao_banco_id_INT
                FROM projetos_versao_banco_banco pvbb JOIN banco_banco bb 
                    ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.projetos_versao_id_INT = {$this->getId()}
                    AND pvbb.tipo_plataforma_operacional_id_INT IN (
                         ".EXTDAO_Tipo_plataforma_operacional::NATIVO_ANDROID.",
                        ".EXTDAO_Tipo_plataforma_operacional::NATIVO_IPHONE.", 
                        ".EXTDAO_Tipo_plataforma_operacional::NATIVO_WINDOWS_8_PHONE."
                    )
                    AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO;
            
             $db->query($q);
             $listaPVBBMobile = Helper::getResultSetToMatriz($db->result);
             //Cadastrando as versoes de projeto relacionadas
             for($i = 0; $i < count($listaPVBBMobile); $i++){
                 $PVBBMobile = $listaPVBBMobile[$i];
                 $idPVBBMobile = $PVBBMobile[0];
                 $idBancoHomologacaoMobile = $PVBBMobile[1];
                 $objPVBBMobile = new EXTDAO_Projetos_versao_banco_banco();
                 $objPVBBMobile->select($idPVBBMobile);
                 $idSincPVBB = EXTDAO_Projetos_versao_banco_banco::getVersaoSinc($idPVBBMobile, EXTDAO_Tipo_analise_projeto::SINCRONIZACAO);
                 
                 if(!strlen($idSincPVBB)){
                     $objBBWebMobile = new EXTDAO_Banco_banco();
                     $objBBWebMobile->setHomologacao_banco_id_INT($idBancoHomologacaoWeb);
                     $objBBWebMobile->setProducao_banco_id_INT($idBancoHomologacaoMobile);
                     $objBBWebMobile->setTipo_banco_id_INT(EXTDAO_Tipo_banco::$TIPO_BANCO_WEB);
                     $objBBWebMobile->setProjetos_id_INT($this->getProjetos_id_INT());
                     $objBBWebMobile->formatarParaSQL();
                     $objBBWebMobile->insert();
                     $objBBWebMobile->selectUltimoRegistroInserido();
                     
                     $objPVBBWebMobile = new EXTDAO_Projetos_versao_banco_banco();
                     $objPVBBWebMobile->setNome($objPVBBMobile->getNome());
                     $objPVBBWebMobile->setProjetos_versao_id_INT($idPVSinc);
                     $objPVBBWebMobile->setBanco_banco_id_INT($objBBWebMobile->getId());
                     $objPVBBWebMobile->setTipo_analise_projeto_id_INT(EXTDAO_Tipo_analise_projeto::SINCRONIZACAO);
                     $objPVBBWebMobile->setTipo_plataforma_operacional_id_INT($objPVBBMobile->getTipo_plataforma_operacional_id_INT());
                     $objPVBBWebMobile->setSinc_do_projetos_versao_banco_banco_id_INT($idPVBBMobile);
                     $objPVBBWebMobile->formatarParaSQL();
                     $objPVBBWebMobile->insert();
                     $objPVBBWebMobile->selectUltimoRegistroInserido();
                     $listaPVBBMobile[$i] = $objPVBBWebMobile->getId();
                 }
             }
                
        }
        
        
        
        public static function getIdVersaoProjeto($pIdProjeto){
            
            $db = new Database();
            $db->query("SELECT MAX(id)
                FROM projetos_versao 
                WHERE projetos_id_INT = ".$pIdProjeto."
                    AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                ORDER BY id DESC
                LIMIT 0,1");
            return Helper::getResultSetToPrimeiroResultado($db->result);
        }
        
        public function isEstruturaInicializada($pIdProjetoVersao){
            if(Helper::SESSION("estrutura_projeto_inicializada") == $pIdProjetoVersao) return true;
            else return false;
        }
        
        
//        
//        public function inicializaEstruturaBanco($pIdProjetoVersao, $pForcarInicializacao = false){
////            if(!$pForcarInicializacao){
////                if(Seguranca::getEstruturaInicializada() == $pIdProjetoVersao) return;
////            }
//            $this->select($pIdProjetoVersao);
//            
//            $this->inicializaEstruturaTabela();
//            $this->inicializaEstruturaAtributo();
//            
//            EXTDAO_Tabela_tabela::procedimentoAtualizaTipoOperacaoAtualizacaoTabelaTabela($this->getId());
//            $this->carregaSistemaTabelaDoBancoDeProducao($pIdProjetoVersao);
//            Seguranca::setEstruturaInicializada($pIdProjetoVersao);
//        }
        
        //TODO responsavel por inicializar a estrutura do banco relativa a nova versÃ£o,
        //nesse caso Ã© realizada a copia da estrutura do banco de produÃ§Ã£o para de homologaÃ§Ã£o
        //Classes envolvidas: banco_banco
        private function inicializaEstruturaTabela(){
            
            $objProjetoVersaoBancoBanco = new EXTDAO_Projetos_versao_banco_banco();
            
            $vVetorBancoBanco = $objProjetoVersaoBancoBanco->getVetorBancoBancoDoProjetoVersao($this->getId());
            if(!empty($vVetorBancoBanco)){
                foreach ($vVetorBancoBanco as $vIdBancoBanco) {
                    $objBancoBanco = new EXTDAO_Banco_banco();
                    $objBancoBanco->select($vIdBancoBanco);
                    $objBancoBanco->inicializaEstruturaDeTabelaDoProjetoVersao($this->getId());
                }
            }
        }
        
        
        public static function carregaSistemaTabelaDoBancoDeProducao(){
            $pIdProjetoVersao = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
            $objProjetoVersaoBancoBanco = new EXTDAO_Projetos_versao_banco_banco();
            
            $vVetorBancoBanco = $objProjetoVersaoBancoBanco->getVetorBancoBancoDoProjetoVersao($pIdProjetoVersao);
            if(!empty($vVetorBancoBanco)){
                foreach ($vVetorBancoBanco as $vIdBancoBanco) {
                    $objBancoBanco = new EXTDAO_Banco_banco();
                    $objBancoBanco->select($vIdBancoBanco);
                    $objBancoBanco->atualizaTabelaProducaoComDadosDeSincronizacaoDoBancoDeProducao($pIdProjetoVersao);
                    $objBancoBanco->atualizaTabelaHomologacaoComDadosDeSincronizacaoDoBancoDeProducao($pIdProjetoVersao);
                }
            }
        }
        
        public static function atualizaSistemaTabelaDoBancoDeHomologacao($idPVBB = null){
            set_time_limit(10 * 60);
            if($idPVBB == null){
                //caso a chamada seja por post
                $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
                //Atualiza em tabela@bibliote_nuvem os dados do formulario de sincronizao
                $objTabela = new EXTDAO_Tabela();
                $objTabela->__actionEdit();
            }
            //Atualiza em sistema_tabela@banco_web os dados contidos em tabela@biblioteca_nuvem
            //relativa a sincronizacao
            $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBB->select($idPVBB);
            $db = new Database();
            //$objPVBB->getFkObjBanco_banco()->atualizaSistemaTabelaDoBancoDeHomologacao($objPVBB->getProjetos_versao_id_INT());
            $idPVBBAndroid = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoAndroidDoProjetosVersao(
                $objPVBB->getProjetos_versao_id_INT(), 
                $db);
            //$objPVBB->select($idPVBBAndroid);
            $objBancoAndroid = EXTDAO_Projetos_versao_banco_banco::getObjBancoHomologacao($idPVBBAndroid);
            
            $objPVBBAux = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBBAux->select($idPVBBAndroid);
            $idPVAndroid= $objPVBBAux->getProjetos_versao_id_INT();
            
            $idPVBBWeb = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoWebDoProjetosVersao(
                $objPVBB->getProjetos_versao_id_INT(), 
                $db);
            $objBancoWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoHomologacao($idPVBBWeb);
            $objPVBBAux->select($idPVBBWeb);
            $idPVWeb = $objPVBBAux->getProjetos_versao_id_INT();
            //Atualizando o sistema_tabela do banco sincronizador_web
            $idPVBBSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getVersaoSinc(
                $idPVBBWeb, 
                EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB, 
                $db);
            $objBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBSincronizadorWeb);
           
            
            EXTDAO_Sistema_tabela::atualizaSistemaTabelaDoBanco(
                $objBancoWeb, 
                $objBancoAndroid, 
                $objBancoSincronizadorWeb, 
                $idPVWeb, 
                $idPVAndroid,
                $db);
            
            $mensagemSucesso = "AtualizaÃ§Ã£o realizada com sucesso.";
            Helper::imprimirMensagem($mensagemSucesso);
//            $urlSuccess = Helper::getUrlAction("lists_gerencia_sincronizacao_tabela");
//            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }
        
         
        public static function imprimiCabecalhoIdentificador(){
            ?>
<input type="hidden" name="<?php echo   Param_Get::ID_PROJETOS_VERSAO;?>" id="<?php echo   Param_Get::ID_PROJETOS_VERSAO;?>" value="<?php echo Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO); ?>">
<input type="hidden" name="<?php echo   Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO;?>" id="<?php echo   Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO;?>" value="<?php echo Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO); ?>">


            
            <?php
        }
//        private function inicializaEstruturaAtributo(){
//            
//            $objProjetoVersaoBancoBanco = new EXTDAO_Projetos_versao_banco_banco();
//            
//            $objTabelaTabela = new EXTDAO_Tabela_tabela();
//            $vVetorBancoBanco = $objProjetoVersaoBancoBanco->getVetorBancoBancoDoProjetoVersao($this->getId());
//            if(count($vVetorBancoBanco)){
//                foreach ($vVetorBancoBanco as $vIdBancoBanco) {
//                    $vVetorTabelaTabela = $objTabelaTabela->getVetorIdTabelaTabela($this->getId(), $vIdBancoBanco);
//                    foreach ($vVetorTabelaTabela as $idTabelaTabela) {
//                            $objBancoBanco = new EXTDAO_Banco_banco();
//                            $objBancoBanco->select($vIdBancoBanco);
//                            $objBancoBanco->inicializaEstruturaDeAtributo($idTabelaTabela);    
//                    }
//                }
//            }
//         }
        
        public function publicar()
        {
             
            $idPV = Helper::GET(Param_Get::ID_PROJETOS_VERSAO);
            if(strlen($idPV)){
                $obj = new EXTDAO_Projetos_versao();
                $obj->select($idPV);
                $obj->setData_producao_DATETIME(Helper::getDiaEHoraAtualSQL());
                $obj->formatarParaSQL();
                $obj->update($idPV);
                $vIdVersao = EXTDAO_Projetos_versao::getIdVersaoProjeto($idPV);
                
                return array("location: index.php?tipo=pages&page=seleciona_projetos_versao&".Param_Get::ID_PROJETOS_VERSAO."=".$vIdVersao);
                
            }
            
        }
        
        
        public static function getListaIdentificadorNo($idProjetos){
            $q = "SELECT id, nome
                FROM projetos_versao 
                WHERE projetos_id_INT = $idProjetos 
                    AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                    ORDER BY nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        
        
        public static function getListaIdentificadorNoSincronizacao($idProjetos){
            $q = "SELECT id, nome
                FROM projetos_versao 
                WHERE projetos_id_INT = $idProjetos 
                    AND copia_do_projetos_versao_id_INT IS NULL
                    AND tipo_analise_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                    ORDER BY nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }

        
        
}

    
