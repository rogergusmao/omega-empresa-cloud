<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tabela_chave_atributo
    * NOME DA CLASSE DAO: DAO_Tabela_chave_atributo
    * DATA DE GERAÇÃO:    06.04.2013
    * ARQUIVO:            EXTDAO_Tabela_chave_atributo.php
    * TABELA MYSQL:       tabela_chave_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tabela_chave_atributo extends DAO_Tabela_chave_atributo
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tabela_chave_atributo";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_tabela_chave_id_INT = "Chave da Tabela";
			$this->label_atributo_id_INT = "Atributo";
			$this->label_seq_INT = "Sequência";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tabela_chave_atributo();

        }
        
        
    public function valorCampoLabel(){

    	$vIdAtributo = $this->getAtributo_id_INT();
        $obj = new EXTDAO_Atributo();
        $obj->select($vIdAtributo);
        return $obj->getNome();
    }
    
         public static function getListaIdOrdenadoAtributoDaChave($pIdTabelaChave, $db = null){
             if($db == null)
            $db = new Database();
            $db->query("SELECT a.id, tca.seq_INT
                FROM tabela_chave_atributo tca JOIN atributo a ON tca.atributo_id_INT = a.id
                WHERE tca.tabela_chave_id_INT = $pIdTabelaChave
                ORDER BY tca.seq_INT");
            $vId=  Helper::getResultSetToArrayDeUmCampo($db->result);
            if(count($vId) > 0)return $vId;
            else return null;
        }

        public static function getNomesAtributosChaveUnica($pIdTabelaChave, $db = null){
            if($db == null)
                $db = new Database();
            $db->query("SELECT a.nome, tca.seq_INT
                FROM tabela_chave_atributo tca JOIN atributo a ON tca.atributo_id_INT = a.id
                WHERE tca.tabela_chave_id_INT = $pIdTabelaChave
                ORDER BY tca.seq_INT");
            $vId=  Helper::getResultSetToArrayDeUmCampo($db->result);
            if(count($vId) > 0)return $vId;
            else return null;
        }


        public static function getListaNomeOrdenadoAtributoDaChave($pIdTabelaChave, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.nome, tca.seq_INT
                FROM tabela_chave_atributo tca JOIN atributo a ON tca.atributo_id_INT = a.id
                WHERE tca.tabela_chave_id_INT = $pIdTabelaChave
                ORDER BY tca.seq_INT");
            $vId=  Helper::getResultSetToArrayDeUmCampo($db->result);
            if(count($vId) > 0)return $vId;
            else return null;
        }
        
        public static function getTabelaChaveAtributo($pIdAtributo, $pIdTabelaChave, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT id
                FROM tabela_chave_atributo
                WHERE atributo_id_INT = $pIdAtributo
                    AND tabela_chave_id_INT = $pIdTabelaChave
                LIMIT 0,1 ");
            $vId=  Helper::getResultSetToPrimeiroResultado($db->result);
            if(strlen($vId))return $vId;
            else return null;
                
        }

        public static function getDadosChavesNaoUnica($idTabela, $db = null){
            if($db == null)
                $db = new Database();
            $db->query("select tc.nome, a.nome
from tabela_chave tc 
	join tabela_chave_atributo tca
		on tc.id=tca.tabela_chave_id_INT
	join atributo a
		on tca.atributo_id_INT = a.id
where tc.tabela_id_INT = $idTabela 
  and tc.non_unique_BOOLEAN = '1'");
            $vId=  Helper::getResultSetToMatriz($db->result);
            if(count($vId) > 0)return $vId;
            else return null;
        }
        
	}

    
