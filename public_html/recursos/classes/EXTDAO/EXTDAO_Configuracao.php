<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Configuracao
    * NOME DA CLASSE DAO: DAO_Configuracao
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            EXTDAO_Configuracao.php
    * TABELA MYSQL:       configuracao
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Configuracao extends DAO_Configuracao
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Configuracao";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_numero_niveis_estrutura_org_INT = "Número de Níveis na Estrutura Organizacional";
			$this->label_nome_projeto = "Nome do Projeto";

        }

        public static function factory(){

            return new EXTDAO_Configuracao();

        }

        public static function getRaizDaAplicação(){

            //print_r(session_get_cookie_params());
            $pathAbsoluto = Helper::getPathComBarrasUnix(__DIR__);
            $pathRelativo = str_replace($_SERVER["DOCUMENT_ROOT"], "", $pathAbsoluto);

            $pathRelativoRaiz = substr($pathRelativo, 0, strrpos($pathRelativo, "recursos"));

            return $pathRelativoRaiz;

        }

	}

