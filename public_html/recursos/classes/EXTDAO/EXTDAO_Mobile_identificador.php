<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Mobile_identificador
 * NOME DA CLASSE DAO: DAO_Mobile_identificador
 * DATA DE GERAÃ‡?O:    29.03.2013
 * ARQUIVO:            EXTDAO_Mobile_identificador.php
 * TABELA MYSQL:       mobile_identificador
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡?O DA CLASSE
// **********************

class EXTDAO_Mobile_identificador extends DAO_Mobile_identificador {
    
    const ESTADO_MOBILE_OFFLINE = 0;
    const ESTADO_SOFTWARE_NAO_ESTA_SENDO_EXECUTADO = 5;
    const ESTADO_CONECTANDO = 1;
    const ESTADO_CONECTADO = 2;
    const ESTADO_FALHA_CONEXAO = 3;
    const ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB = 4;
    
    
    const SUPORTE_DESLIGADO = 0;
    const SUPORTE_REQUISITADO_PELA_WEB = 1;
    const SUPORTE_CONECTADO_AO_TELEFONE = 2;

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Mobile_identificador";

    }

    public function setLabels() {

        $this->label_id = "Id";
        $this->label_mobile_id_INT = "Telefone";
        $this->label_usuario_id_INT = "UsuÃ¡rio";
        $this->label_corporacao_id_INT = "CorporaÃ§?o";
        $this->label_sistema_projetos_versao_sistema_produto_mobile_id_INT = "Vers?o do Produto Mobile";
        $this->label_mobile_id_INT = "Telefone";
        $this->label_sistema_projetos_versao_sistema_produto_mobile_id_INT = "Produto";
        $this->label_data_cadastro_DATETIME = "Data cadastro";
        $this->label_reinstalar_a_versao_id_INT = "reinstalar a vers?o";
        $this->label_atualizacao_versao_ativa_BOOLEAN = "atualizaÃ§?o de vers?o estÃ¡ ativa?";
    }

    public function setDiretorios() {
        
    }
    
    public function getIdentificador(){
        return $this->getFkObjMobile()->getIdentificador();
    }
    
    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Mobile_identificador();
    }

    public static function consulta($imei, $sistemaProjetosVersaoSistemaProdutoMobile) {
        $consulta = "SELECT mpv.id
                        FROM mobile_identificador mpv JOIN mobile m ON mpv.mobile_id_INT = m.id
                        WHERE m.id = '{$imei}'";

        $consulta .= "  AND mpv.sistema_projetos_versao_sistema_produto_mobile_id_INT = {$sistemaProjetosVersaoSistemaProdutoMobile}
                        LIMIT 0,1";

        $db = Registry::get('Database');
        $db->query($consulta);
        return $db->getPrimeiraTuplaDoResultSet("id");
    }
    
    
    public static function modoSuporteAtivo($id) {
        $obj = new EXTDAO_Mobile_identificador();
        $obj->select($id);
        $x = $obj->getModo_suporte_ativo_BOOLEAN();
        return !strlen($x) ? false : $x;
        
    }
    public static function getMobileConectadoEmExecucao($idMI){

        $consulta = "SELECT mc.id, data_atualizacao_DATETIME
                        FROM mobile_conectado mc JOIN mobile_identificador mi
                            ON mc.mobile_identificador_id_INT = mi.id
                        WHERE mi.id = {$idMI}
                        ORDER BY data_atualizacao_DATETIME DESC
                        LIMIT 0,1";

        $db = new Database();
        $db->query($consulta);
        
        $regs =  Helper::getResultSetToMatriz($db->result);
        if(count($regs)){
            $reg = $regs[0];
            $data = $reg[1];
            if(strlen($data)){
                $totSegAtualizacao = strtotime($data) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE;
                $totSegAtual = strtotime(Helper::getDiaEHoraAtualSQL());

                if($totSegAtualizacao > $totSegAtual)
                    
                    $reg["conectado"] = true;
                else $reg["conectado"] = false;
            } else $reg["conectado"] = false;
            return $reg;
        } else return null;
        
        
    }

    public static function isSoftwareEmExecucao($idMI){

        $consulta = "SELECT data_atualizacao_DATETIME
                    FROM mobile_conectado mc JOIN mobile_identificador mi 
                        ON mc.mobile_identificador_id_INT = mi.id
                    WHERE mi.id = {$idMI} 
                    ORDER BY data_atualizacao_DATETIME DESC
                    LIMIT 0,1";

        $db = new Database();
        $db->query($consulta);
        $data = $db->getPrimeiraTuplaDoResultSet(0);
        if(strlen($data)){
            $totSegAtualizacao = strtotime($data) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE;
            $totSegAtual = strtotime(Helper::getDiaEHoraAtualSQL());
            
            if($totSegAtualizacao > $totSegAtual)
                return true;
            else return false;
        } else return false;
    }

    public static function getInformacoes($idMI){
        $objMI = new EXTDAO_Mobile_identificador();
        $objMI->select($idMI);
        $ret = array();
        $ret["id"] = $idMI;
        //$ret["softwareEmExecucao"]= EXTDAO_Mobile_identificador::isSoftwareEmExecucao($idMI) ? "Online" : "Offline" ;
        $regMC = EXTDAO_Mobile_identificador::getMobileConectadoEmExecucao($idMI);
        $ret["softwareEmExecucao"]=  "Offline";
        if($regMC != null){
            $ret["softwareEmExecucao"]= $regMC["conectado"] ? "Online" : "Offline";
        }
        $ret["idMobileConectado"] = $regMC["id"];
        $ret["tipoBanco"]= EXTDAO_Mobile_identificador::getTipoBanco($idMI);
        $msgStatus = $objMI->getMensagemStatusSuporte();
        if($msgStatus != null){
            $ret["processandoOperacaoSistema"]= $msgStatus->processando;
            $ret["statusSuporte"] = $msgStatus->mensagem;
        }
        $ret["identificador"] = $objMI->getIdentificador();
        $ret["idMobile"] = $objMI->getMobile_id_INT();
        
        $lista = EXTDAO_Mobile_identificador::getListaIdentificadorNo($idMI);
        if(count($lista))
            $ret["versao"] = $lista[0][1];
        else $ret["versao"] = "-";
        return $ret;
    }
    
    public static function getListaIdentificadorNoProdutos($mobile) {
        $q = "SELECT DISTINCT(mi.id), sp.nome
                FROM mobile_identificador mi
                JOIN sistema_projetos_versao_sistema_produto_mobile spvspm
                        ON mi.sistema_projetos_versao_sistema_produto_mobile_id_INT = spvspm.id
                    JOIN sistema_produto_mobile spm
                        ON spm.id = spvspm.sistema_produto_mobile_id_INT
                    JOIN sistema_produto sp
                        ON spm.sistema_produto_id_INT = sp.id
                WHERE mi.mobile_id_INT = $mobile
                ORDER BY sp.nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    public static function getListaIdentificadorNo($mobileIdentificador) {
            $q = "SELECT DISTINCT (mi.id), pv.nome
                    FROM mobile_identificador mi
                    JOIN sistema_projetos_versao_sistema_produto_mobile spvspm
                            ON mi.sistema_projetos_versao_sistema_produto_mobile_id_INT = spvspm.id
                        JOIN sistema_projetos_versao spv
                            ON spv.id = spvspm.sistema_projetos_versao_id_INT
                        JOIN projetos_versao pv
                            ON spv.projetos_versao_id_INT = pv.id
                    WHERE mi.id = $mobileIdentificador
                    ORDER BY pv.nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public function isSuporteAtivo(){
//            SUPORTE_OFFLINE
//            SUPORTE_REQUISITADO_PELA_WEB
//            SUPORTE_CONECTADO_AO_TELEFONE
            $data= $this->getRequisicao_modo_suporte_DATETIME();
            //Soma a data de requisacao
            $data = Helper::somaSegundosAData($data, LIMITE_SEGUNDOS_PARA_DESCONECTAR_SUPORTE);
            $totSegReq = strtotime($data);
            $now = Helper::getDiaEHoraAtualSQL();
            $totSegNow =  strtotime($now);
            if($totSegReq < $totSegNow) return false;
            else return true;
        }
        
        public static function getListaMobileIdentificadorDoPVBB($idPVBB) {
            $q = "SELECT DISTINCT(mi.id)
                    FROM mobile_identificador mi
                            JOIN sistema_projetos_versao_sistema_produto_mobile spvspm
                            ON spvspm.id = mi.sistema_projetos_versao_sistema_produto_mobile_id_INT
                            
                           JOIN sistema_projetos_versao_produto spvp 
                            ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id
                            
                           JOIN mobile m
                            ON mi.mobile_id_INT = m.id
                    WHERE spvp.prototipo_projetos_versao_banco_banco_id_INT = $idPVBB 
                        OR spvp.servidor_projetos_versao_banco_banco_id_INT = $idPVBB 
                    ORDER BY m.identificador";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
    
        public static function getMobileIdentificadorHospedeiro($idPVBB){
            $consulta = "SELECT mi.id
                        FROM mobile_identificador mi 
                        
                            JOIN sistema_projetos_versao_sistema_produto_mobile spvspm
                                ON mi.sistema_projetos_versao_sistema_produto_mobile_id_INT = spvspm.id
                                
                            JOIN sistema_projetos_versao_produto spvp
                                    ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id
                                    
                        WHERE mi.is_hospedeiro_BOOLEAN = '1' 
                            AND (spvp.prototipo_projetos_versao_banco_banco_id_INT = '$idPVBB'
                                OR
                                spvp.servidor_projetos_versao_banco_banco_id_INT = '$idPVBB'
                                )
                        LIMIT 0,1";
            
            $db = new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public function ativaHospedeiro(){
            $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);
            $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
            $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
            $varGET = Param_Get::getIdentificadorProjetosVersao();
            $idMIAtual = EXTDAO_Mobile_identificador::getMobileIdentificadorHospedeiro($idPVBB);
            $obj = new EXTDAO_Mobile_identificador();
            if($idMIAtual != $idMI && $idMIAtual != null){
                $obj->select($idMIAtual);
                $obj->setIs_hospedeiro_BOOLEAN("0");
                $obj->formatarParaSQL();
                $obj->update($idMIAtual);   
            } 
           
           $obj->select($idMI);
           $obj->setIs_hospedeiro_BOOLEAN("1");
           $obj->formatarParaSQL();
           $obj->update($idMI);   
           Helper::imprimirMensagem("O telefone foi setado como hospedeiro.");
        }
        
        public static function isProdutoEmExecucao($idMobileIdentificador){
            $consulta = "SELECT data_atualizacao_DATETIME
                        FROM mobile_conectado mc
                        WHERE mc.mobile_identificador_id_INT = {$idMobileIdentificador} 
                        ORDER BY data_atualizacao_DATETIME DESC
                        LIMIT 0,1";
            
            $db = new Database();
            $db->query($consulta);
            $data = $db->getPrimeiraTuplaDoResultSet(0);
            if(strlen($data)){
                $totSegAtualizacao = strtotime($data) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE;
                $totSegAtual = strtotime(Helper::getDiaEHoraAtualSQL());
                if($totSegAtualizacao < $totSegAtual)
                    return true;
                else return false;
            } else return false;
        }
        
        public function getEstadoConexaoMobileNoModoSuporte(){
            $totTempoReqModoSupt= $this->getRequisicao_modo_suporte_DATETIME_UNIX();
            if($totTempoReqModoSupt == null){
                return EXTDAO_Mobile_identificador::ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB;
            }
            elseif( !EXTDAO_Mobile::isMobileConectado($this->getMobile_id_INT()) == true ){
                    return EXTDAO_Mobile_identificador::ESTADO_MOBILE_OFFLINE;
            }
            elseif(!EXTDAO_Mobile_identificador::isProdutoEmExecucao($this->getId())){
                return EXTDAO_Mobile_identificador::ESTADO_SOFTWARE_NAO_ESTA_SENDO_EXECUTADO;
            }
            elseif($this->isMobileNoModoSuporte()){
                return EXTDAO_Mobile_identificador::ESTADO_CONECTADO;
            }
            else{
                
                $totTempoReqModoSupt += LIMITE_SEGUNDOS_PARA_DESCONECTAR_SUPORTE;
                $totTempoAtual = strtotime(Helper::getDiaEHoraAtualSQL());
                if($totTempoReqModoSupt < $totTempoAtual){
                    //Caso a tentativa de conex?o seja antiga
                    return EXTDAO_Mobile_identificador::ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB;
                }
                else{
                    $valor = $this->getTotalSegundoRestanteProximaVerificacaoDoMobileDeSuporte();
                    if($valor <= 0 ) return EXTDAO_Mobile_identificador::ESTADO_FALHA_CONEXAO;
                    else return EXTDAO_Mobile_identificador::ESTADO_CONECTANDO;
                }
            }

        }
        
        public static function getTipoBanco($idMI){
             $consulta = "SELECT bb.tipo_banco_id_INT
                        FROM mobile_identificador mi 
                            JOIN sistema_projetos_versao_sistema_produto_mobile spvspm
                                    ON mi.sistema_projetos_versao_sistema_produto_mobile_id_INT = spvspm.id
                                    
                                JOIN sistema_projetos_versao_produto spvp
                                    ON spvspm.sistema_projetos_versao_produto_id_INT= spvp.id
                                    
                                JOIN projetos_versao_banco_banco pvbb 
                                    ON spvp.prototipo_projetos_versao_banco_banco_id_INT = pvbb.id
                                    
                               JOIN banco_banco bb
                                ON pvbb.banco_banco_id_INT = bb.id
                                
                        WHERE mi.id= {$idMI}
                        LIMIT 0,1";
            $db = new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public function imprimirMensagemStatus(){
            
            $ret = new Protocolo_mensagem_status();
            $data= $this->getRequisicao_modo_suporte_DATETIME();
            $estado = $this->getEstadoConexaoMobileNoModoSuporte();
            $ret->processando = false;
            $ret->mensagem = "";
            
            switch ($estado){
                case EXTDAO_Mobile_identificador::ESTADO_MOBILE_OFFLINE:
                    $ret->processando = false;
                    $ret->mensagem = Helper::getMensagem("Falha na conex?o.</br>O telefone pode estar desconectado, ou o software n?o estÃ¡ sendo executado no telefone.", MENSAGEM_ERRO);
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_CONECTANDO:
                    $ret->processando = true;
                    $valor = $this->getTotalSegundoRestanteProximaVerificacaoDoMobileDeSuporte();
                    $valor /= 60;
                    $ret->mensagem = Helper::getMensagem("Conectando... </br>Tempo restante: $valor min", MENSAGEM_WARNING);
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_CONECTADO:
                    $ret->processando = false;
                    $ret->mensagem = Helper::getMensagem("Conectado", MENSAGEM_OK);
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_FALHA_CONEXAO:
                    $ret->processando = false;
                    $ret->mensagem = Helper::getMensagem("Falha na conex?o.</br>O telefone pode estar desconectado.", MENSAGEM_ERRO);
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_SOFTWARE_NAO_ESTA_SENDO_EXECUTADO:
                    $ret->processando = false;
                    $ret->mensagem = Helper::getMensagem("Falha na conex?o.</br>O software n?o estÃ¡ sendo executado no telefone.", MENSAGEM_ERRO);
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB:
                    $ret->processando = false;
                    $ret->mensagem = Helper::getMensagem("Falha na requisiÃ§?o.</br>Se persistir o problema comunique ao nosso suporte.", MENSAGEM_ERRO);
                    break;
                default:
                    return null;
            }
            $ret->mensagem = rawurlencode($ret->mensagem);
            
            $token = json_encode($ret);
            
            return $token;
            
        }
        
        
        public function getMensagemStatusSuporte(){
            
            $ret = new Protocolo_mensagem_status();
            $data= $this->getRequisicao_modo_suporte_DATETIME();
            $estado = $this->getEstadoConexaoMobileNoModoSuporte();
            $ret->processando = false;
            $ret->mensagem = "";
            
            switch ($estado){
                case EXTDAO_Mobile_identificador::ESTADO_MOBILE_OFFLINE:
                    $ret->processando = false;
                    $ret->mensagem = "Falha na conex?o.</br>O telefone pode estar desconectado, ou o software n?o estÃ¡ sendo executado no telefone.";
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_CONECTANDO:
                    $ret->processando = true;
                    $valor = $this->getTotalSegundoRestanteProximaVerificacaoDoMobileDeSuporte();
                    $valor /= 60;
                    $ret->mensagem = "Conectando... </br>Tempo restante: $valor min";
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_CONECTADO:
                    $ret->processando = false;
                    $ret->mensagem = "Conectado";
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_FALHA_CONEXAO:
                    $ret->processando = false;
                    $ret->mensagem = "Falha na conex?o.</br>O telefone pode estar desconectado.";
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_SOFTWARE_NAO_ESTA_SENDO_EXECUTADO:
                    $ret->processando = false;
                    $ret->mensagem = "Falha na conex?o.</br>O software n?o estÃ¡ sendo executado no telefone.";
                    break;
                case EXTDAO_Mobile_identificador::ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB:
                    $ret->processando = false;
                    $ret->mensagem = "N?o foi requisitado o modo de suporte.";
                    break;
                default:
                    return null;
            }
            
            return $ret;
            
        }
        
        public function getTotalSegundoRestanteProximaVerificacaoDoMobileDeSuporte(){
            
            $data= Helper::getDiaEHoraAtualSQL();
            //Soma a data de requisacao
            $margenToleranciaSeg = 10;
            $LIMITE = LIMITE_SEGUNDOS_PARA_DESCONECTAR_SUPORTE + $margenToleranciaSeg;
            $totSegHA = strtotime($data);
            $horarioDeVerificacaoDoSuporteAtivo = $this->getMobile_resposta_suporte_DATETIME();
            
            
            if(!strlen($horarioDeVerificacaoDoSuporteAtivo)){
                return -1;
            } else{
                $horarioDeVerificacaoDoSuporteAtivo = Helper::somaSegundosAData($data, $LIMITE);
                $totSegHV5 =  strtotime($horarioDeVerificacaoDoSuporteAtivo);
                $tempo = $totSegHV5 - $totSegHA;
                if($tempo < 0) return 0;
                else return $tempo;
            }
            
        }
        
        public function isMobileNoModoSuporte(){
//            SUPORTE_OFFLINE
//            SUPORTE_REQUISITADO_PELA_WEB
//            SUPORTE_CONECTADO_AO_TELEFONE
            $data= $this->getRequisicao_modo_suporte_DATETIME();
            $totSegReq = null;
            if($data == null){
                return false;
            }
            $data = Helper::somaSegundosAData($data, LIMITE_SEGUNDOS_PARA_DESCONECTAR_SUPORTE);    
            $totSegReq = strtotime($data);
            $horarioDeVerificacaoDoSuporteAtivo = $this->getMobile_resposta_suporte_DATETIME();
            if(! strlen($horarioDeVerificacaoDoSuporteAtivo))
                return false;
            else{
                $totSegNow =  strtotime($horarioDeVerificacaoDoSuporteAtivo);
                if($totSegReq < $totSegNow) return false;
                else return true;
            }
            
        }
    
        public function imprimirMensagemStatusReinstalacao($idMI){
            $obj = new EXTDAO_Mobile_identificador();
            $obj->select($idMI);
            if($obj->getAtualizacao_versao_ativa_BOOLEAN() == "0"){
                Helper::imprimirMensagem("O aplicativo estÃ¡ bloqueado para atualizaÃ§Ã£o.", MENSAGEM_ERRO);
            }else if(!strlen($obj->getReinstalar_a_versao_id_INT())){
                Helper::imprimirMensagem("A ordem de reinstalar a versÃ£o jÃ¡ foi enviada.", MENSAGEM_OK);
            }else if(strlen($obj->getReinstalar_a_versao_id_INT())){
                Helper::imprimirMensagem("Aguardando o envio da ordem de reinstalar a versÃ£o ".$obj->getReinstalar_a_versao_id_INT(), MENSAGEM_OK);
            }
            
            EXTDAO_Estado_operacao_sistema_mobile::imprimirMensagem($idE);
        }
        
        public function reinstalarVersaoAppMobile(){
            $novaVersao = Helper::POSTGET("reinstalar_a_versao_id_INT");
            $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);
            
            $this->select($idMI);
            $this->setReinstalar_a_versao_id_INT($novaVersao);    
            $this->setAtualizacao_versao_ativa_BOOLEAN("1");    
            $this->formatarParaSQL();
            $this->update($idMI);
            return array("location: popup.php?tipo=page&pages=status_reinstala_versao_app_mobile&".Param_Get::ID_MOBILE_IDENTIFICADOR."=$idMI");

        }
        
}

