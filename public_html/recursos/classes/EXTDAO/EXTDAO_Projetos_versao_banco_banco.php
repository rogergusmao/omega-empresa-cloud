<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Projetos_versao_banco_banco
 * NOME DA CLASSE DAO: DAO_Projetos_versao_banco_banco
 * DATA DE GERAÃ‡ÃƒO:    29.01.2013
 * ARQUIVO:            EXTDAO_Projetos_versao_banco_banco.php
 * TABELA MYSQL:       projetos_versao_banco_banco
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡ÃƒO DA CLASSE
// **********************

class EXTDAO_Projetos_versao_banco_banco extends DAO_Projetos_versao_banco_banco {

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Projetos_versao_banco_banco";

    }
    
    public static function getIdRelacionadoComSistemaProjetosVersaoProduto($idSPVDesejado, $idBBEquivalente){
        
        $q = "SELECT DISTINCT pvbb.id
FROM projetos_versao_banco_banco pvbb 
	JOIN projetos_versao pv ON pv.id = pvbb.projetos_versao_id_INT
	JOIN sistema_projetos_versao spv ON spv.projetos_versao_id_INT = pv.id
WHERE pvbb.banco_banco_id_INT = $idBBEquivalente AND spv.id = $idSPVDesejado";
                $db = new Database();
                $db->query($q);
                return $db->getPrimeiraTuplaDoResultSet(0);
    }
    
    public static function atualizaConfiguracaoAtualizacaoRegistroBancoProducao($idPVBB = null){
            if($idPVBB == null)
            $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
            //Atualiza em tabela@bibliote_nuvem os dados do formulario de sincronizao
            $objTabela = new EXTDAO_Tabela();
            $objTabela->__actionEdit();

            
            $mensagemSucesso = "AtualizaÃ§Ã£o realizada com sucesso.";
            Helper::imprimirMensagem($mensagemSucesso);
//            $urlSuccess = Helper::getUrlAction("lists_gerencia_sincronizacao_tabela");
//            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }
        

        public function valorCampoLabel(){
            
            return $this->getId."@".$this->getNome();
            

        }
    
//    function select($id)
//    {
//
//    	$sql =  "SELECT *  FROM projetos_versao_banco_banco WHERE id = $id;";
//    	$this->database->query($sql);
//    	$result = $this->database->result;
//    	$row = $this->database->fetchObject($result);
//
//    
//        $this->id = $row->id;
//        
//        $this->nome = $row->nome;
//        
//        $this->projetos_versao_id_INT = $row->projetos_versao_id_INT;
//        if($this->projetos_versao_id_INT)
//			$this->getFkObjProjetos_versao()->select($this->projetos_versao_id_INT);
//
//        $this->banco_banco_id_INT = $row->banco_banco_id_INT;
//        if($this->banco_banco_id_INT)
//			$this->getFkObjBanco_banco()->select($this->banco_banco_id_INT);
//
//        $this->script_estrutura_banco_hom_para_prod_ARQUIVO = $row->script_estrutura_banco_hom_para_prod_ARQUIVO;
//        
//        $this->script_registros_banco_hom_para_prod_ARQUIVO = $row->script_registros_banco_hom_para_prod_ARQUIVO;
//        
//        $this->xml_estrutura_banco_hom_para_prod_ARQUIVO = $row->xml_estrutura_banco_hom_para_prod_ARQUIVO;
//        
//        $this->xml_registros_banco_hom_para_prod_ARQUIVO = $row->xml_registros_banco_hom_para_prod_ARQUIVO;
//        
//        $this->tipo_analise_projeto_id_INT = $row->tipo_analise_projeto_id_INT;
//        if($this->tipo_analise_projeto_id_INT)
//			$this->getFkObjTipo_analise_projeto()->select($this->tipo_analise_projeto_id_INT);
//
//        $this->copia_do_projetos_versao_banco_banco_id_INT = $row->copia_do_projetos_versao_banco_banco_id_INT;
//
//			$this->getFkObjCopia_do_projetos_versao_banco_banco()->select($this->copia_do_projetos_versao_banco_banco_id_INT);
//
//        $this->sinc_do_projetos_versao_banco_banco_id_INT = $row->sinc_do_projetos_versao_banco_banco_id_INT;
//
//			$this->getFkObjSinc_do_projetos_versao_banco_banco()->select($this->sinc_do_projetos_versao_banco_banco_id_INT);
//
//        $this->tipo_plataforma_operacional_id_INT = $row->tipo_plataforma_operacional_id_INT;
//        if($this->tipo_plataforma_operacional_id_INT)
//			$this->getFkObjTipo_plataforma_operacional()->select($this->tipo_plataforma_operacional_id_INT);
//
//
//    }
    public function setLabels() {

        $this->label_id = "Id";
        $this->label_nome = "Nome";
        $this->label_banco_banco_id_INT = "Banco do Projeto";
        $this->label_projetos_versao_id_INT = "VersÃ£o do Projeto";
        $this->label_script_estrutura_banco_hom_para_prod_ARQUIVO = "Arquivo AtualizaÃ§Ã£o da Estrutura de HomologaÃ§Ã£o Para ProduÃ§Ã£o";
        $this->label_script_registros_banco_hom_para_prod_ARQUIVO = "Arquivo AtualizaÃ§Ã£o dos Registros de HomologaÃ§Ã£o Para ProduÃ§Ã£o";
        $this->label_copia_do_projetos_versao_banco_banco_id_INT = "CÃ³pia do relacionamento dos bancos versÃ£o de projeto";
        $this->label_tipo_plataforma_operacional_id_INT = "Plataforma Operacional";
        
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Projetos_versao_banco_banco();
    }
   
    public static function getVersaoSinc($idPVBB, $tipoAnaliseProjeto, $db = null){
        $q = "SELECT id
            FROM projetos_versao_banco_banco
            WHERE sinc_do_projetos_versao_banco_banco_id_INT = {$idPVBB}"
            . " AND tipo_analise_projeto_id_INT = $tipoAnaliseProjeto";
        
        if($db == null)
            $db = new Database();
        $db->query($q);
        
        return $db->getPrimeiraTuplaDoResultSet(0);
    }
    
    public static function getObjDatabaseDoBancoProducao($idPVBB, $db = null ){
        if($db == null)
        $db = new Database();
        $vConsulta = "SELECT bb.producao_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
            WHERE bb.producao_banco_id_INT IS NOT NULL 
                AND pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        $vIdBanco = Helper::getResultSetToPrimeiroResultado($db->result);
        $obj = new EXTDAO_Banco();
        $obj->select($vIdBanco);
        return $obj->getObjDatabase();
    }
    
    public static function getObjBancoProducao($idPVBB, $db = null ){
        if($db == null)
        $db = new Database();
        $vConsulta = "SELECT bb.producao_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
            WHERE bb.producao_banco_id_INT IS NOT NULL 
                AND pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        $vIdBanco = Helper::getResultSetToPrimeiroResultado($db->result);
        $obj = new EXTDAO_Banco();
        $obj->select($vIdBanco);
        return $obj;
    }
    
    public static function getObjDatabaseDoBancoHomologacao($idPVBB, $db = null){
        if($db == null)
        $db = new Database();
        $vConsulta = "SELECT bb.homologacao_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
            WHERE bb.homologacao_banco_id_INT IS NOT NULL 
                AND pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        $vIdBanco = Helper::getResultSetToPrimeiroResultado($db->result);
        $obj = new EXTDAO_Banco();
        $obj->select($vIdBanco);
        return $obj->getObjDatabase();
    }
    
    
    public static function getObjBancoBanco($idPVBB, $db = null){
        if($db == null)
        $db = new Database();
        $vConsulta = "SELECT pvbb.banco_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
            WHERE pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        $id = Helper::getResultSetToPrimeiroResultado($db->result);
        $obj = new EXTDAO_Banco_banco();
        $obj->select($id);
        return $obj;
    }
    
    public static function getObjBancoHomologacao($idPVBB, $db = null){
        if($db == null)
        $db = new Database();
        $vConsulta = "SELECT bb.homologacao_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
            WHERE bb.homologacao_banco_id_INT IS NOT NULL 
                AND pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        $vIdBanco = Helper::getResultSetToPrimeiroResultado($db->result);
        $obj = new EXTDAO_Banco();
        $obj->select($vIdBanco);
        return $obj;
    }
    
    
    public static function getIdBancoHomologacao($idPVBB, $db = null){
        if($db == null)
            $db = new Database();
        $vConsulta = "SELECT bb.homologacao_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
            WHERE bb.homologacao_banco_id_INT IS NOT NULL 
                AND pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        return Helper::getResultSetToPrimeiroResultado($db->result);
    }
    
    public static function getIdBancoProducao($idPVBB, $db = null){
        if($db == null)
        $db = new Database();
        $vConsulta = "SELECT bb.producao_banco_id_INT
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
            WHERE bb.producao_banco_id_INT IS NOT NULL 
                AND pvbb.id = {$idPVBB}";
        $db->query($vConsulta);
        return Helper::getResultSetToPrimeiroResultado($db->result);
    }
    
    
    
    public static function getIdPVBB($pProjetosVersao, $pBancoBanco, Database $db = null) {
        $vConsulta = "SELECT id
                FROM projetos_versao_banco_banco 
                WHERE projetos_versao_id_INT = {$pProjetosVersao} AND
                    banco_banco_id_INT = {$pBancoBanco}";
        if($db == null)
            $db = new Database();
        $db->query($vConsulta);
        $vId = Helper::getResultSetToPrimeiroResultado($db->result);
        if (strlen($vId))
            return $vId;
        else
            return null;
    }

    
    public function getFiltroComboBoxAllBanco_banco($objArgumentos) {
        

        $objArgumentos->nome="banco_banco_id_INT";
        $objArgumentos->id="banco_banco_id_INT";
        $objArgumentos->valueReplaceId=false;

        
        $vObjBancoBanco = new EXTDAO_Banco_banco();
        $vIdBanco = $vObjBancoBanco->getIdBancoHomologacao($this->getId());

        $objArgumentos->query = "SELECT bb.id, CONCAT(bb.id, ' - ', b1.nome, '@', b2.nome )
                FROM banco_banco bb
                    JOIN banco b1 ON bb.homologacao_banco_id_INT = b1.id
                    JOIN banco b2 ON bb.producao_banco_id_INT = b2.id";
        $vRetorno = $this->getFkObjBanco_banco()->getComboBox($objArgumentos);

        $objArgumentos->strFiltro = "";
        $objArgumentos->isDisabled = false;
        return $vRetorno;
    }
    
    public static function getIdProjetosEProjetosVersao($idPVBB, $db = null) {
        if($db == null) $db = new Database();
        $q = "SELECT pv.projetos_id_INT idProjetos, pv.id idProjetosVersao"
            . " FROM projetos_versao_banco_banco pvbb "
            . "     JOIN projetos_versao pv "
            . "         ON pvbb.projetos_versao_id_INT = pv.id"
            . " WHERE pvbb.id = $idPVBB";
        
        $db->query($q);
        $ret = Helper::getResultSetToMatriz($db->result);
        return count($ret ) ? $ret[0] : null;
        
    }

    public static function getIdProjetosVersao($idPVBB, $db = null) {
        if($db == null) $db = new Database();
        $q = "SELECT  pvbb.projetos_versao_id_INT "
            . " FROM projetos_versao_banco_banco pvbb "
            . " WHERE pvbb.id = $idPVBB";

        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);

    }
    
    public function getVetorAtributoAtributo($db = null) {
        if($db == null)
        $db = new Database();
        $db->query("SELECT DISTINCT aa.id
                FROM atributo_atributo aa 
                    JOIN atributo t ON aa.homologacao_atributo_id_INT = t.id 
                        OR aa.producao_atributo_id_INT = t.id
                WHERE aa.projetos_versao_banco_banco_id_INT= {$this->getId()} ");
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
    public function getVetorTabelaChaveTabelaChaveNaoIgnorada($db = null){
         if($db == null)
        $db = new Database();
        $q = "SELECT DISTINCT(tctc.id)
FROM tabela_chave_tabela_chave tctc
	LEFT JOIN tabela_chave tcHmg ON tctc.homologacao_tabela_chave_id_INT 
	LEFT JOIN tabela_chave tcPrd ON tctc.producao_tabela_chave_id_INT 
WHERE tctc.projetos_versao_banco_banco_id_INT = {$this->getId()}  
AND (
	SELECT COUNT(tt.id)
	FROM tabela_tabela tt
	WHERE (tt.producao_tabela_id_INT  IS NULL OR tt.producao_tabela_id_INT = tcPrd.tabela_id_INT)
		AND (tt.homologacao_tabela_id_INT IS NULL OR tt.homologacao_tabela_id_INT = tcHmg.tabela_id_INT)
		AND tt.projetos_versao_banco_banco_id_INT = {$this->getId()}
		AND tt.tipo_operacao_atualizacao_banco_id_INT != ".EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE."
) > 0";
        $db->query($q);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
    public function getVetorTabelaChaveTabelaChave($db = null) {
        if($db == null)
        $db = new Database();
//        $q = "SELECT DISTINCT tctc.id
//            FROM tabela_chave_tabela_chave tctc
//            WHERE tctc.projetos_versao_banco_banco_id_INT= {$this->getId()} ";
        $q = "
SELECT DISTINCT(tctc.id)
FROM tabela_chave_tabela_chave tctc
    LEFT JOIN tabela_chave tcHmg 
        ON tctc.homologacao_tabela_chave_id_INT = tcHmg.id
    LEFT JOIN tabela_chave tcPrd 
        ON tctc.producao_tabela_chave_id_INT = tcPrd.id
WHERE tctc.projetos_versao_banco_banco_id_INT = {$this->getId()}  
    AND (tcHmg.id IS NOT NULL OR tcPrd.id IS NOT NULL)
    AND (
        SELECT COUNT(tt.id)
        FROM tabela_tabela tt
        WHERE 
            tt.tipo_operacao_atualizacao_banco_id_INT != ".EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE."
            AND tt.projetos_versao_banco_banco_id_INT = tctc.projetos_versao_banco_banco_id_INT  		
            AND (tt.producao_tabela_id_INT  IS NULL OR tt.producao_tabela_id_INT = tcPrd.tabela_id_INT)
            AND (tt.homologacao_tabela_id_INT IS NULL OR tt.homologacao_tabela_id_INT = tcHmg.tabela_id_INT)
        LIMIT 0,1
    ) > 0";
        $db->query($q);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    public function getVetorTabelaTabela($db = null) {
        if($db == null)
        $db = new Database();
        $consulta = "SELECT DISTINCT tt.id
                FROM tabela_tabela tt JOIN tabela t ON tt.homologacao_tabela_id_INT = t.id 
                            OR tt.producao_tabela_id_INT = t.id
                WHERE tt.projetos_versao_banco_banco_id_INT = {$this->getId()}";
        
        $db->query($consulta);

        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    public function getVetorTabelaTabelaDaHomologacao() {
        $db = new Database();
        $db->query("SELECT DISTINCT tt.id
                FROM tabela_tabela tt JOIN tabela t ON tt.homologacao_tabela_id_INT = t.id 
                                        OR tt.producao_tabela_id_INT = t.id
                WHERE tt.projetos_versao_banco_banco_id_INT = {$this->getId()} 
                        AND tt.homologacao_tabela_id_INT IS NOT NULL");
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
    public function isMobile(){
        return $this->getFkObjBanco_banco()->isMobile();
    }
    
    public static function getTipoBanco($idPVBB, $db = null) {
        if($db == null)
        $db = new Database();
        $q = "SELECT bb.tipo_banco_id_INT
                FROM projetos_versao_banco_banco pvbb 
                    JOIN banco_banco bb
                        ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id = $idPVBB
                LIMIT 0,1";
        
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }
    
    public static function getTipoAnaliseProjeto($idPVBB, $db = null) {
        if($db == null)
        $db = new Database();
        $db->query("SELECT pv.tipo_analise_projeto_id_INT
                FROM projetos_versao_banco_banco pvbb 
                    JOIN projetos_versao pv 
                        ON pvbb.projetos_versao_id_INT = pv.id
                WHERE pvbb.id = $idPVBB
                LIMIT 0,1");
        return $db->getPrimeiraTuplaDoResultSet(0);
    }
    
    public function atualizaTipoOperacaoAtributoAtributo() {

        $vVetor = $this->getVetorAtributoAtributo();
        if($vVetor != null)
        foreach ($vVetor as $vAtributoAtributo) {
            $obj = new EXTDAO_Atributo_atributo();
            $obj->select($vAtributoAtributo);
            $vTipoOperacao = $obj->atualizaTipoOperacaoAtualizacaoBanco($this->getId());
            $obj->setTipo_operacao_atualizacao_banco_id_INT($vTipoOperacao);
            $obj->formatarParaSQL();
            $obj->update($vAtributoAtributo);
        }
    }

    public function getVetorBancoBancoDoProjetoVersao($pIdProjetoVersao, $pTipoBanco = false) {

        $vQuerySelect = "SELECT DISTINCT pvbb.banco_banco_id_INT ";
        $vQueryFrom = "FROM projetos_versao_banco_banco pvbb ";
        $vQueryWhere = "WHERE pvbb.projetos_versao_id_INT= {$pIdProjetoVersao} ";

        if ($pTipoBanco) {
            $vQueryFrom .= " JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id ";
            $vQueryWhere .= " AND bb.tipo_banco_id_INT = " . $pTipoBanco;
        }
        $vQuery = $vQuerySelect . $vQueryFrom . $vQueryWhere;
        $this->database->query($vQuery);
        return Helper::getResultSetToArrayDeUmCampo($this->database->result);
    }
    
    
    public static function getVetorId($pIdProjetoVersao, $pTipoBanco = false) {

        $vQuerySelect = "SELECT DISTINCT pvbb.id ";
        $vQueryFrom = "FROM projetos_versao_banco_banco pvbb ";
        $vQueryWhere = "WHERE pvbb.projetos_versao_id_INT= {$pIdProjetoVersao} ";

        if ($pTipoBanco) {
            $vQueryFrom .= " JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id ";
            $vQueryWhere .= " AND bb.tipo_banco_id_INT = " . $pTipoBanco;
        }
        $vQuery = $vQuerySelect . $vQueryFrom . $vQueryWhere;
        $db = new Database();
        $db->query($vQuery);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    public static function getListaIdentificadorNoSincronizacao($idPV){
        
        $q = "SELECT id
            FROM projetos_versao_banco_banco 
            WHERE projetos_versao_id_INT = $idPV "
            . " AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."";
        
        
        $db = new Database();
        $db->query($q);
        $idsPVBB = Helper::getResultSetToArrayDeUmCampo($db->result);
        if(count($idsPVBB)){
            $in = "";
            for($i = 0 ; $i < count($idsPVBB); $i++){
               if($i > 0 )  $in .= ", ";
               $in .= " '$idsPVBB[$i]'";
            }
            
            $q = "SELECT id, nome
            FROM projetos_versao_banco_banco 
            WHERE sinc_do_projetos_versao_banco_banco_id_INT IN ($in)
                AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::SINCRONIZACAO."
            ORDER BY nome";
            
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        } else{
            return false;
        }
        
        
    }
    public static function getIdPVPrototipoCorrespondente($idPV, $db = null){
        $objPV = new EXTDAO_Projetos_versao($db);
        $objPV->select($idPV) ;
        if($objPV->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::SINCRONIZACAO){
            $idPV = $objPV->getSinc_do_projetos_versao_id_INT();
        } else if($objPV->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::COMPARACAO){
            $idPV = $objPV->getCopia_do_projetos_versao_id_INT();
        }else if($objPV->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::DESENVOLVIMENTO_APP){
            $idPV = $objPV->getSinc_do_projetos_versao_id_INT();
        }
        return $idPV;
    }
    
    public static function getListaIdentificadorNoBancoSincronizadorWeb($idPV, $db = null){
        
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        $q = "SELECT id
            FROM projetos_versao_banco_banco 
            WHERE projetos_versao_id_INT = $idPV "
            . " AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."";
        
        if($db == null)
        $db = new Database();
        $db->query($q);
        
        
        
        $idsPVBB = Helper::getResultSetToArrayDeUmCampo($db->result);
        if(count($idsPVBB)){
            
            $in = "";
            for($i = 0 ; $i < count($idsPVBB); $i++){
               if($i > 0 )  $in .= ", ";
               $in .= " '$idsPVBB[$i]'";
            }
            
            $q = "SELECT id, nome
            FROM projetos_versao_banco_banco 
            WHERE sinc_do_projetos_versao_banco_banco_id_INT IN ($in)
                AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB."
            ORDER BY nome";
                
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        } else{
            return false;
        }
    }

    public static function getIdPVBBWebParaSincronizadorWebHmg($idPV, $db = null){

        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV, $db);
        $q = "SELECT id
            FROM projetos_versao_banco_banco 
            WHERE projetos_versao_id_INT = $idPV "
            . " AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."";

        if($db == null)
            $db = new Database();
        $db->query($q);



        $idsPVBB = Helper::getResultSetToArrayDeUmCampo($db->result);
        if(count($idsPVBB)){

            $in = "";
            for($i = 0 ; $i < count($idsPVBB); $i++){
                if($i > 0 )  $in .= ", ";
                $in .= " '$idsPVBB[$i]'";
            }

            $q = "SELECT id, nome
            FROM projetos_versao_banco_banco 
            WHERE sinc_do_projetos_versao_banco_banco_id_INT IN ($in)
                AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB."
            ORDER BY nome";

            $db->query($q);
            $rs= Helper::getResultSetToMatriz($db->result);
            if(count($rs)){
                return $rs[0][0];
            } else return null;
        } else{
            return null;
        }
    }
    
    public static function getIdPVBBSincronizadorWebHmgParaProd($idPV, $db = null){
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        $ret = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoBancoSincronizadorWeb($idPV, $db = null);
        if(count($ret)){
            return $ret[0][0];
        } else return false;
    }
    
    
    public static function getIdPVBBWebHmgESincronizadorWebHmg($idPV, $db = null){
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        $ret = EXTDAO_Projetos_versao_banco_banco::getListaIdentificadorNoAtualizaSincronizadorWebProd($idPV, $db = null);
        if(count($ret)){
            return $ret[0][0];
        } else return false;
    }
    
        public static function getListaIdentificadorNoAtualizaSincronizadorWebProd($idPV, $db = null){
        
        $q = "SELECT id
            FROM projetos_versao_banco_banco 
            WHERE projetos_versao_id_INT = $idPV "
            . " AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."";
        
        if($db == null) $db = new Database();
        
        $db->query($q);
        $idsPVBB = Helper::getResultSetToArrayDeUmCampo($db->result);
        if(count($idsPVBB)){
            $in = "";
            for($i = 0 ; $i < count($idsPVBB); $i++){
               if($i > 0 )  $in .= ", ";
               $in .= " '$idsPVBB[$i]'";
            }
            
            $q = "SELECT id, nome
            FROM projetos_versao_banco_banco 
            WHERE sinc_do_projetos_versao_banco_banco_id_INT IN ($in)
                AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web."
            ORDER BY nome";
            
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        } else{
            return false;
        }
        
        
    }
    
    
    public static function getIdsPVBBProjetosVersao($idPV, $db = null){
        
        $objPV = new EXTDAO_Projetos_versao();
        $objPV->select($idPV) ;
        if($objPV->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::SINCRONIZACAO){
            $idPV = $objPV->getSinc_do_projetos_versao_id_INT();
        } else if($objPV->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::COMPARACAO){
            $idPV = $objPV->getCopia_do_projetos_versao_id_INT();
        }else if($objPV->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::DESENVOLVIMENTO_APP){
            $idPV = $objPV->getSinc_do_projetos_versao_id_INT();
        }
        
        
        $q = "SELECT pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb
                    ON pvbb.banco_banco_id_INT = bb.id
            WHERE pvbb.sinc_do_projetos_versao_banco_banco_id_INT IS NULL
                AND bb.tipo_banco_id_INT = ".EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID."
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                AND pvbb.projetos_versao_id_INT = $idPV    
            LIMIT 0,1 ";
        
        if($db == null) $db = new Database();
            
        $db->query($q);
        
        
        return $db->getPrimeiraTuplaDoResultSet(0);
        
    }
    
    
    public static function getIdPVBBSincronizacao($idPV, $db = null){
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        
         $q = "SELECT pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN projetos_versao pv 
                    ON pvbb.projetos_versao_id_INT = pv.id
            WHERE pv.sinc_do_projetos_versao_id_INT = $idPV 
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::SINCRONIZACAO."
            LIMIT 0,1 ";
        
        if($db == null) $db = new Database();
            
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }
    
    public static function getIdPVBBPrototipoAndroidDoProjetosVersao($idPV, $db = null){
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        
        
        $q = "SELECT pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb
                    ON pvbb.banco_banco_id_INT = bb.id
            WHERE pvbb.sinc_do_projetos_versao_banco_banco_id_INT IS NULL
                AND bb.tipo_banco_id_INT = ".EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID."
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                AND pvbb.projetos_versao_id_INT = $idPV    
            LIMIT 0,1 ";
        
        if($db == null) $db = new Database();
            
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
        
    }
    
    public static function getIdPVePVBBPrototipoAndroidDoProjetosVersao($idPV, $db = null){
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        
        
        $q = "SELECT pvbb.projetos_versao_id_INT, pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb
                    ON pvbb.banco_banco_id_INT = bb.id
            WHERE pvbb.sinc_do_projetos_versao_banco_banco_id_INT IS NULL
                AND bb.tipo_banco_id_INT = ".EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID."
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                AND pvbb.projetos_versao_id_INT = $idPV    
            LIMIT 0,1 ";
        
        if($db == null) $db = new Database();
            
        $db->query($q);
        return Helper::getResultFirstObject($db->result);
    }
    
    public static function getIdPVBBPrototipoWebDoProjetosVersao($idPV, $db = null){
        
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        
        
        $q = "SELECT pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb
                    ON pvbb.banco_banco_id_INT = bb.id
            WHERE pvbb.sinc_do_projetos_versao_banco_banco_id_INT IS NULL
                AND bb.tipo_banco_id_INT = ".EXTDAO_Tipo_banco::$TIPO_BANCO_WEB."
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                AND pvbb.projetos_versao_id_INT = $idPV    
            LIMIT 0,1 ";
        
        if($db == null) $db = new Database();
            
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
        
    }
    
     public static function getIdPVePVBBPrototipoWebDoProjetosVersao($idPV, $db = null){
        
        $idPV = EXTDAO_Projetos_versao_banco_banco::getIdPVPrototipoCorrespondente($idPV);
        
        
        $q = "SELECT pvbb.projetos_versao_id_INT, pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN banco_banco bb
                    ON pvbb.banco_banco_id_INT = bb.id
            WHERE pvbb.sinc_do_projetos_versao_banco_banco_id_INT IS NULL
                AND bb.tipo_banco_id_INT = ".EXTDAO_Tipo_banco::$TIPO_BANCO_WEB."
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
                AND pvbb.projetos_versao_id_INT = $idPV    
            LIMIT 0,1 ";
        
        if($db == null) $db = new Database();
            
        $db->query($q);
        return Helper::getResultFirstObject($db->result);   
    }
    
    public static function getListaIdentificadorNo($idPV){
        $q = "SELECT id, nome
            FROM projetos_versao_banco_banco 
            WHERE projetos_versao_id_INT = $idPV 
            ORDER BY nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    
    public static function getListaIdentificadorNoAnaliseBancos(){
        $q = "SELECT id, nome
            FROM projetos_versao_banco_banco 
            WHERE  tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_MySql."
            ORDER BY nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    public static function getListaIdSincronizacao($idPV){
        $q = "SELECT pvbb.id
            FROM projetos_versao_banco_banco pvbb 
                JOIN projetos_versao pv 
                    ON pvbb.projetos_versao_id_INT = pv.id
            WHERE pv.sinc_do_projetos_versao_id_INT = $idPV 
                AND pvbb.tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::SINCRONIZACAO."
            ";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
    
    public static function getListaIdPrototipo($idPV){
        $q = "SELECT id
            FROM projetos_versao_banco_banco 
            WHERE projetos_versao_id_INT = $idPV 
                AND tipo_analise_projeto_id_INT = ".EXTDAO_Tipo_analise_projeto::PROTOTIPO."
            ORDER BY nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
    
    
    public static function downloadScriptAtualizacaoBanco(){
        $idPVBB = Helper::GET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
        
        $raiz = Helper::acharRaiz();

        $pathArq = $raiz.Gerador_script_banco_banco::getPathArquivo($idPVBB).Gerador_script_banco_banco::getNomeArquivo(ARQUIVO_SCRIPT_ESTRUTURA."_$idPVBB");
        if(is_file($pathArq)){
            
            $objDownload = new Download($pathArq);
            print $objDownload->ds_download();
        }
    }

    public static function procedimentoAtualizaSistemaTabelaDoBancoProducao(
        $idPVBBPrototipo, $idPVPrototipo, $db, $idPVBBSistemaTabela = null){
        
        $dbProducao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoProducao($idPVBBPrototipo, $db);
        $objBancoPrototipo = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBPrototipo, $db);
        $idBancoPrototipo = $objBancoPrototipo->getId();
        if($idPVBBSistemaTabela != null){
            
            $dbDestinoAtualizacao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoProducao(
                $idPVBBSistemaTabela, $db);
             
        } else {
            $dbDestinoAtualizacao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoProducao(
                $idPVBBPrototipo, $db);
        }
        $varGET= Param_Get::getIdentificadorProjetosVersao();
        
        $dbDestinoAtualizacao->query("select nome from sistema_tabela");

        $tabelas = Helper::getResultSetToArrayDeUmCampo($dbDestinoAtualizacao->result);

        $objTabela = new EXTDAO_Tabela();
        for($i = 0 ;$i < count($tabelas); $i++){

            $table = $tabelas[$i];
            $idTabela = EXTDAO_Tabela::existeTabela($table, $idBancoPrototipo, $idPVPrototipo, $db);
            if(!strlen($idTabela)){
                Helper::imprimirMensagem ("[$table] Tabela inexistente no banco de dados [$idBancoPrototipo] do projetos versao [$idPVPrototipo], atualize os dados do projeto no Biblioteca Nuvem", MENSAGEM_ERRO);
                
                continue;
            }
            echo "Registro: $table [$idTabela]<br/>";
            
            $objTabela->select($idTabela);
            $idTabelaChave = EXTDAO_Tabela_chave::getIdDaChaveUnicaDaTabela($idTabela, $db);

            if(strlen($idTabelaChave)){
                $vetorAtributoChaveUnica = EXTDAO_Tabela_chave_atributo::getListaIdOrdenadoAtributoDaChave($idTabelaChave, $db);
            }
            else continue;
             $strChaveUnica = "";
            if(count($vetorAtributoChaveUnica) > 0){

                //public void setUniqueKey(String pVetorAttributeName[])
                $objAtributo = new EXTDAO_Atributo();
                for($j = 0 ; $j < count($vetorAtributoChaveUnica); $j++){
                    $objAtributo->select($vetorAtributoChaveUnica[$j]);
                    $nomeAtributo = $objAtributo->getNome();


                    if($j > 0)$strChaveUnica .= ", ";
                    $strChaveUnica .= $nomeAtributo;
                }
            }
//if($table == "empresa_perfil"){
//                    print_r($dbDestinoAtualizacao);    
//                    exit();
//                }
            if(strlen($strChaveUnica)){
                $q = "UPDATE sistema_tabela"
                    . " SET atributos_chave_unica = '$strChaveUnica'"
                    . " WHERE nome LIKE '$table'";
                $dbDestinoAtualizacao->query($q);
                echo $q;
                Helper::imprimirMensagem("[$table] Chaves atualizadas com sucesso", MENSAGEM_OK);
            } else{
                Helper::imprimirMensagem("[$table] A tabela nÃ£o possui chaves unicas, isso Ã© um problema para a solucao de duplicaidades durante a sincronizacao.", 
                    MENSAGEM_WARNING);
            }
        }
    }
}

