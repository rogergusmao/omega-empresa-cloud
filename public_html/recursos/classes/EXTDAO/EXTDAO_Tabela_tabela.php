<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Tabela_tabela
 * NOME DA CLASSE DAO: DAO_Tabela_tabela
 * DATA DE GERAÃ‡ÃƒO:    28.01.2013
 * ARQUIVO:            EXTDAO_Tabela_tabela.php
 * TABELA MYSQL:       tabela_tabela
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡ÃƒO DA CLASSE
// **********************

class EXTDAO_Tabela_tabela extends DAO_Tabela_tabela {

    const FATOR_NOME = 1;

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Tabela_tabela";

    }

    public function setLabels() {

        $this->label_id = "Id";
        $this->label_homologacao_tabela_id_INT = "Tabela de HomologaÃ§Ã£o";
        $this->label_producao_tabela_id_INT = "Tabela de ProduÃ§Ã£o";
        $this->label_tipo_operacao_atualizacao_banco_id_INT = "Tipo de OperaÃ§Ã£o de AtualizaÃ§Ã£o";
        $this->label_nova_BOOLEAN = "Ã‰ tabela nova?";
        $this->label_projetos_versao_id_INT = "VersÃ£o do Projeto";
        $this->label_status_verificacao_BOOLEAN = "Verificada?";

        $this->label_inserir_tuplas_na_homologacao_BOOLEAN = "Inserir Tuplas do Banco da ProduÃ§Ã£o que nÃ£o existem na HomologaÃ§Ã£o";
        $this->label_remover_tuplas_na_homologacao_BOOLEAN = "Remover Tuplas do Banco da ProduÃ§Ã£o que nÃ£o existem na HomologaÃ§Ã£o";
        $this->label_editar_tuplas_na_homologacao_BOOLEAN = "Atualizar as Tuplas do Banco de ProduÃ§Ã£o com os dados de HomologaÃ§Ã£o";
        $this->label_tabela_sistema_BOOLEAN = "Ã‰ do sistema";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Tabela_tabela();
    }

    public function ignorarOperacaoDeAtualizacaoDoBancoHmgParaProd($ignorarAtributos = false){
        $this->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE);
        $this->formatarParaSQL();
        $this->update($this->getId());
        
        if($ignorarAtributos){
            
            $idAAs = EXTDAO_Atributo_atributo::getListIdAtributoAtributo(
                $this->getId(), 
                $this->database);
            
            if(!empty($idAAs)){
                $objAA= new EXTDAO_Atributo_atributo();
                for($i = 0 ;$i < count($idAAs); $i++){
                    $idAA = $idAAs[$i];
                    $objAA->select($idAA);
                    $objAA->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE);
                    $objAA->formatarParaSQL();
                    
                    $objAA->update($idAA);
                }
            }
        }
    }
    
    public function existeAtributoSemVerificacao() {
        $vConsulta = "SELECT DISTINCT aa.id
                FROM atributo_atributo aa JOIN atributo a ON aa.homologacao_atributo_id_INT = a.id OR aa.producao_atributo_id_INT
                    JOIN tabela_tabela tt ON aa.tabela_tabela_id_INT = tt.id
                WHERE tt.id = {$this->getId()} AND aa.status_verificacao_BOOLEAN = '0' OR aa.status_verificacao_BOOLEAN IS NULL
                LIMIT 0,1";
        $database = new Database();
        $database->query($vConsulta);
        $vId = $database->getPrimeiraTuplaDoResultSet(0);
        if (!strlen($vId))
            return false;
        else
            return true;
    }

    public static function getVetorIdTabelaTabela($idPVBB, $pIdBancoBanco, $db = null) {
        if($db == null)
        $db = new Database();
        $q = "SELECT tt.id 
FROM tabela t JOIN tabela_tabela tt ON tt.homologacao_tabela_id_INT = t.id 
JOIN projetos_versao_banco_banco pvbb ON tt.projetos_versao_banco_banco_id_INT = pvbb.id 
JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
WHERE pvbb.id = $idPVBB 
    AND pvbb.banco_banco_id_INT = $pIdBancoBanco 
    AND t.banco_id_INT = bb.homologacao_banco_id_INT";
        
        $db->query($q);
        

        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
    
    
    public static function getVetorIdTabelaTabelaSoProducaoComTipoOperacaoBanco(
            $idPVBB, 
            $pIdBancoBanco, 
            $vetorTipoOperacaoBanco ) {
        $db = new Database();
        $str = "";
        for($i = 0; $i < count($vetorTipoOperacaoBanco); $i++){
            if($i > 0 ) $str .= ", ";
            $str .= $vetorTipoOperacaoBanco[$i];
        }
        $q = "SELECT DISTINCT tt.id 
FROM tabela t JOIN tabela_tabela tt ON tt.producao_tabela_id_INT = t.id 
JOIN projetos_versao_banco_banco pvbb ON tt.projetos_versao_banco_banco_id_INT = pvbb.id
JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
WHERE pvbb.id = $idPVBB 
    AND pvbb.banco_banco_id_INT = $pIdBancoBanco 
    AND tt.tipo_operacao_atualizacao_banco_id_INT IN (".$str.")
        AND tt.homologacao_tabela_id_INT IS NULL";
        
        $db->query($q);
        
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    
    
    public static function getListaIdentificadorNoHomologacao($idPVBB) {
        $db = new Database();
        $q = "SELECT tt.id , t.nome
FROM tabela t JOIN tabela_tabela tt ON tt.homologacao_tabela_id_INT = t.id 
WHERE tt.projetos_versao_banco_banco_id_INT = $idPVBB";
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    public static function getListaIdentificadorNoProducao($idPVBB) {
        $db = new Database();
        $q = "SELECT tt.id , t.nome
FROM tabela t JOIN tabela_tabela tt ON tt.producao_tabela_id_INT = t.id 
WHERE tt.projetos_versao_banco_banco_id_INT = $idPVBB";
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    
    public static function getObjDaTabelaHomETabelaProd(
        $idPVBB, 
        $pIdTabelaHom, 
        $pIdTabelaProd, 
        $considerarValorNull = true,
        Database $db = null) {

        $vConsulta = "SELECT id
                FROM tabela_tabela ";

        $vConsulta .= "WHERE projetos_versao_banco_banco_id_INT = {$idPVBB} ";
        if (strlen($pIdTabelaHom))
            $vConsulta .= "AND homologacao_tabela_id_INT = {$pIdTabelaHom} ";
        else if ($considerarValorNull)
            $vConsulta .= " AND producao_tabela_id_INT IS NULL";

        if (strlen($pIdTabelaProd))
            $vConsulta .= " AND producao_tabela_id_INT = {$pIdTabelaProd}";
        else if ($considerarValorNull)
            $vConsulta .= " AND producao_tabela_id_INT IS NULL";
        if($db == null)
            $db = new Database();
        $db->query($vConsulta);
        $vIdTabelaTabela = $db->getPrimeiraTuplaDoResultSet("id");
        if ($vIdTabelaTabela != null) {
            $vObjTabelaTabela = new EXTDAO_Tabela_tabela();
            $vObjTabelaTabela->select($vIdTabelaTabela);
            return $vObjTabelaTabela;
        }
        else
            return null;
    }

    public static function existeRelacaoTabelaHomologacao(
        $pIdTabelaHomologacao, $idPVBB, Database $db = null) {
        if($db == null)
            $db = new Database();
        $db->query("SELECT id 
                FROM tabela_tabela 
                WHERE  homologacao_tabela_id_INT= {$pIdTabelaHomologacao} 
                        AND projetos_versao_banco_banco_id_INT = {$idPVBB} 
                LIMIT 0,1");
        return (Helper::getResultSetToPrimeiroResultado($db->result) == null) ? false : true;
    }

    public function atualizaTipoOperacaoAtualizacaoBanco() {

        if (!strlen($this->getProducao_tabela_id_INT()))
            return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
        else if (!strlen($this->getHomologacao_tabela_id_INT()))
            return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
        else if (strlen($this->getHomologacao_tabela_id_INT()) &&
                strlen($this->getProducao_tabela_id_INT())) {
            $vObjTabelaHom = $this->getObjTabelaHomologacao();
            $vObjTabelaProd = $this->getObjTabelaProducao();
            if ($vObjTabelaHom->getNome() != $vObjTabelaProd->getNome()){
                
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
            }

            if ($vObjTabelaHom != null) {
                if (count(EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveInserida($this->getProjetos_versao_banco_banco_id_INT(), $vObjTabelaHom)) > 0){
                 
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }
            }
            if ($vObjTabelaProd != null) {
                if (count(EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveRemovida($this->getProjetos_versao_banco_banco_id_INT(), $vObjTabelaProd)) > 0){
                 
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }
            }
            return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
        }
        return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
    }

    public function comparaTabelaHomologacaoComProducao() {
        $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
        $vFatores = array();
        $vFatoresTipoOperacao = array();
        if (!strlen($this->getProducao_tabela_id_INT()))
            $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
        else if (!strlen($this->getHomologacao_tabela_id_INT()))
            $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
        else if ($this->getHomologacao_tabela_id_INT() != null &&
                $this->getProducao_tabela_id_INT() != null) {
            $vObjTabelaHom = $this->getObjTabelaHomologacao();
            $vObjTabelaProd = $this->getObjTabelaProducao();
            if ($vObjTabelaHom->getNome() != $vObjTabelaProd->getNome()) {
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                $vFatores[count($vFatores)] = EXTDAO_Tabela_tabela::FATOR_NOME;
                if (strlen($vObjTabelaHom->getNome()) &&
                        !strlen($vObjTabelaProd->getNome()))
                    $vFatoresTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                else if (!strlen($vObjTabelaHom->getNome()) &&
                        strlen($vObjTabelaProd->getNome()))
                    $vFatoresTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                else
                    $vFatoresTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
            }
            else {
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            }
        }

        return array("tipo_modificacao" => $vTipoModificacao,
            "fatores_tipo_operacao" => $vFatoresTipoOperacao,
            "fatores" => $vFatores);
    }

    public function atualizaChaves($idPVBB, $db = null) {
        if($db == null) $db = new Database();
        $pObjTabelaHomologacao = $this->getObjTabelaHomologacao();
        $pObjTabelaProducao = $this->getObjTabelaProducao();

        $idChavesTabelaProdParaDeletar = array();
        $idChavesTabelaHomParaInserir = array();

        $idChavesIguaisHomProd = array();
        //$vIdChavesIguaisHomProd[0] = 1(HOM) => 2(PROD)
        if ($pObjTabelaHomologacao == null) {
            //Se a tabela foi removida, significa que suas chaves tambem
            $idChavesTabelaProdParaDeletar = EXTDAO_Tabela_chave::getListaChaveTabela(
                $pObjTabelaProducao->getId(), 
                $db);
            
        } else if ($pObjTabelaProducao == null) {
            $idChavesTabelaHomParaInserir = EXTDAO_Tabela_chave::getListaChaveTabela(
                $pObjTabelaHomologacao->getId(), 
                $db);
            
        } else {

            $idChavesTabelaProdEncontrada = array();
            $idsTabelaChaveHom = EXTDAO_Tabela_chave::getListaObjChaveTabela(
                $pObjTabelaHomologacao->getId(), $db);
            $idsTabelaChaveProd = EXTDAO_Tabela_chave::getListaObjChaveTabela(
                $pObjTabelaProducao->getId(), $db);
//            exit();
            
            $atributosChaveProd = null;
            $matrizAtributoProd = array();
            $matrizAtributoHom = array();
            if($idsTabelaChaveHom!=null)
            foreach ($idsTabelaChaveHom as $obj) {
                $idChaveHom = $obj[0];
                $nomeChaveHom = $obj[1];
                
                $atributosChaveHom = null;
                //confere se os atributos da chave foram carregados na memoria
                if ( empty($matrizAtributoHom[$idChaveHom]) ) {
                    $atributosChaveHom = EXTDAO_Tabela_chave_atributo::getListaIdOrdenadoAtributoDaChave($idChaveHom, $db);
                    $matrizAtributoHom[$idChaveHom] = $atributosChaveHom;
                }
                else
                    $atributosChaveHom = $matrizAtributoHom[$idChaveHom];

                $idChaveProdEncontrado = null;
                $i = 0;
                
                for ($i = 0; $i < count($idsTabelaChaveProd); $i++) {
                    
                    $idChaveProd = $idsTabelaChaveProd[$i][0];
                    $nomeChaveProd = $idsTabelaChaveProd[$i][1];
                    
                    if ($idChaveProd == null)
                        continue;

                    //confere se os atributos da chave de produÃ§Ã£o foram carregados na memoria
                    if ( empty($matrizAtributoProd[$idChaveProd]) ) {
                        $atributosChaveProd = EXTDAO_Tabela_chave_atributo::getListaIdOrdenadoAtributoDaChave($idChaveProd, $db);
                        $matrizAtributoProd[$idChaveProd] = $atributosChaveProd;
                    }
                    else
                        $atributosChaveProd = $matrizAtributoProd[$idChaveProd];
                    
                    //se a contagem for diferente as chaves de prod e hom nao sao iguais
                    if (count($atributosChaveProd) != count($atributosChaveHom)){
//                        echo "entrouif";
//                exit();
                        continue;
                    }
//                    echo "$vNomeChaveHom != $vNomeChaveProd";
//                    exit();
                    $attrChaveIguais = true;
                    if($nomeChaveHom != $nomeChaveProd){
                        $attrChaveIguais = false;    
                    } else {
                        for ($k = 0; $k < count($atributosChaveHom); $k++) {
                            $attrHom = $atributosChaveHom[$k];
                            $attrProd = $atributosChaveProd[$k];
                            $idAtributoAtributo = EXTDAO_Atributo_atributo::getIdAtributoAtributo(
                                            $idPVBB, $attrHom, $attrProd, $db);
                            if ($idAtributoAtributo == null) {
                                $attrChaveIguais = false;
                                break;
                            }
                        }
                    }
                    
                    if ($attrChaveIguais) {
                        $idChaveProdEncontrado = $idChaveProd;
                        break;
                    }
                }
                if ($idChaveProdEncontrado == null) {

                    $idChavesTabelaHomParaInserir[count($idChavesTabelaHomParaInserir)] = $idChaveHom;
                } else {
                    $idChavesTabelaProdEncontrada[count($idChavesTabelaProdEncontrada)] = $idChaveProd;
                    $idChavesIguaisHomProd[$idChaveHom] = $idChaveProd;
                    EXTDAO_Tabela_chave_tabela_chave::procedimentoChaveIgual(
                        $idPVBB, $idChaveHom, $idChaveProd, 
                        $atributosChaveHom, $atributosChaveProd, $db);
                    $idsTabelaChaveProd[$i] = null;
                }
            }
            
            //depois de ter comparado todas as chaves da tabela de homologacao relacionada a tabela de producao
            //ocorre a verificacao de qual chave de producao linkada com uma de homologacao
            for ($i = 0; $i < count($idsTabelaChaveProd); $i++) {
                $idChaveProd = $idsTabelaChaveProd[$i][0];
                if ($idChaveProd == null)
                    continue;
                if (!in_array($idChaveProd, $idChavesTabelaProdEncontrada)) {
                    $idChavesTabelaProdParaDeletar[count($idChavesTabelaProdParaDeletar)] = $idChaveProd;
                }
            }
            
        }
        if(!empty($idChavesTabelaProdParaDeletar ))
        foreach ($idChavesTabelaProdParaDeletar as $id)
            EXTDAO_Tabela_chave_tabela_chave::procedimentoChaveDeletada($idPVBB, $id, $db);
//if($this->getId() == 14145){
//                echo "ENTROUUU";
//                
//                echo "PROD: ";
//                print_r($idsTabelaChaveProd);
//                
//                echo "<br/>ENCONTRADAS: ";
//                print_r($idChavesTabelaProdParaDeletar);
//                exit();
//            }
        if(!empty($idChavesTabelaHomParaInserir ))
        foreach ($idChavesTabelaHomParaInserir as $id)
            EXTDAO_Tabela_chave_tabela_chave::procedimentoChaveCriada($idPVBB, $id, $db);
//        echo "PROD: ".$pObjTabelaProducao->getId();
//        
//        echo "DFG";
//        print_r($chavesDaTabelaProd);
//        
//        echo "ASDF";
//        print_r($idChavesTabelaProdParaDeletar);
//        exit();
        return array(
            "chaves_prod_para_deletar" => $idChavesTabelaProdParaDeletar,
            "chaves_hom_para_inserir" => $idChavesTabelaHomParaInserir,
            "chaves_iguais_hom_prod" => $idChavesIguaisHomProd
        );
    }

    public static function existeRelacaoTabelaProducao($idPVBB, $pIdTabelaHomologacao) {
        $db = new Database();
        $db->query("SELECT id 
                FROM tabela_tabela 
                WHERE  producao_tabela_id_INT= {$pIdTabelaHomologacao} 
                    AND projetos_versao_banco_banco_id_INT = {$idPVBB}
                LIMIT 0,1");
        return (Helper::getResultSetToPrimeiroResultado($db->result) == null) ? false : true;
    }

    public function getComboBoxAllProducao_tabela($objArgumentos) {

        $objArgumentos->nome = "producao_tabela_id_INT";
        $objArgumentos->id = "producao_tabela_id_INT";
        $objArgumentos->valueReplaceId = false;

        $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
        $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);



        $vObjBancoBanco = new EXTDAO_Banco_banco();
        $vIdBanco = $vObjBancoBanco->getIdBancoProducao($idPVBB);


        $objArgumentos->strFiltro = " tp.banco_id_INT = " . $vIdBanco . " AND tp.projetos_versao_banco_banco_id_INT = " . $idPVBB;

        $vRetorno = $this->getFkObjProducao_tabela()->getComboBox($objArgumentos);
        $objArgumentos->strFiltro = "";
        return $vRetorno;
    }

    public function getComboBoxAllHomologacao_tabela($objArgumentos) {

        $objArgumentos->nome = "homologacao_tabela_id_INT";
        $objArgumentos->id = "homologacao_tabela_id_INT";
        $objArgumentos->valueReplaceId = false;
        $objArgumentos->isDisabled = true;
        $vRetorno = $this->getFkObjHomologacao_tabela()->getComboBox($objArgumentos);
        $objArgumentos->isDisabled = false;
        return $vRetorno;
    }

    public function getComboBoxGerenciaListAllHomologacao_tabela($objArgumentos) {

        $objArgumentos->nome = "homologacao_tabela_id_INT";
        $objArgumentos->id = "homologacao_tabela_id_INT";
        $objArgumentos->valueReplaceId = false;


        $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
        $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);


        $vObjBancoBanco = new EXTDAO_Banco_banco();
        $vIdBanco = $vObjBancoBanco->getIdBancoHomologacao($idPVBB);

        $objArgumentos->strFiltro = " tp.banco_id_INT = " . $vIdBanco . " AND tp.projetos_versao_banco_banco_id_INT = " . $idPVBB;
        $vRetorno = $this->getFkObjHomologacao_tabela()->getComboBox($objArgumentos);

        $objArgumentos->strFiltro = "";
        $objArgumentos->isDisabled = false;
        return $vRetorno;
    }

    public function getComboBoxGerenciaListAllTipo_operacao_atualizacao_banco($objArgumentos) {

        $objArgumentos->nome = "tipo_operacao_atualizacao_banco_id_INT";
        $objArgumentos->id = "tipo_operacao_atualizacao_banco_id_INT";
        $objArgumentos->valueReplaceId = false;

        $vRetorno = $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);
        $objArgumentos->isDisabled = false;
        return $vRetorno;
    }

    public function getComboBoxAllTipo_operacao_atualizacao_banco($objArgumentos) {

        $objArgumentos->nome = "tipo_operacao_atualizacao_banco_id_INT";
        $objArgumentos->id = "tipo_operacao_atualizacao_banco_id_INT";
        $objArgumentos->valueReplaceId = false;
        $objArgumentos->isDisabled = true;
        $vRetorno = $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);
        $objArgumentos->isDisabled = false;
        return $vRetorno;
    }

    public function getObjTabelaHomologacao() {
        
        if ($this->getHomologacao_tabela_id_INT() != null) {
            $obj = new EXTDAO_Tabela($this->getDatabase());
            $obj->select($this->getHomologacao_tabela_id_INT());
            return $obj;
        }
        else
            return null;
    }

    public function getObjTabelaProducao() {
        
        if ($this->getProducao_tabela_id_INT() != null) {
            $obj = new EXTDAO_Tabela();
            $obj->select($this->getProducao_tabela_id_INT());
            return $obj;
        }
        else
            return null;
    }

    public static function getMensagemTabelaHomologacaoComTabelaProducaoDuplicada($idPVBB, $pIdTabelaProducao) {
        $objBanco = new Database();
        $objBanco->query("SELECT t.nome nome_tabela_homologacao
FROM tabela_tabela tt JOIN tabela t ON tt.homologacao_tabela_id_INT = t.id 
    JOIN tabela t2 JOIN tt.producao_tabela_id_INT = t2.id
WHERE t2.id = $pIdTabelaProducao 
    AND tt.projetos_versao_banco_banco_id_INT = $idPVBB
ORDER BY t.nome");
        $vVetorNome = Helper::getResultSetToArrayDeUmCampo($objBanco->result);
        if (count($vVetorNome) == 0)
            return null;
        else {
            $str = "";
            for ($i = 0; $i < count($vVetorNome); $i++) {
                if (strlen($str) > 0)
                    $str .= ", " . $vVetorNome[$i];
                else
                    $str = $vVetorNome[$i];
            }
            return $str;
        }
    }

    public function setByPost($numReg) {

        $this->id = $this->formatarDados($_POST["id{$numReg}"]);
        $this->homologacao_tabela_id_INT = $this->formatarDados($_POST["homologacao_tabela_id_INT{$numReg}"]);
        $this->producao_tabela_id_INT = $this->formatarDados($_POST["producao_tabela_id_INT{$numReg}"]);
        $this->tipo_operacao_atualizacao_banco_id_INT = $this->formatarDados($_POST["tipo_operacao_atualizacao_banco_id_INT{$numReg}"]);
        $this->projetos_versao_id_INT = $this->formatarDados($_POST["projetos_versao_id_INT{$numReg}"]);
        $this->projetos_versao_banco_banco_id_INT = $this->formatarDados($_POST["projetos_versao_banco_banco_id_INT{$numReg}"]);
        $vStatus = $this->formatarDados($_POST["status_verificacao_BOOLEAN{$numReg}"]);
        $this->status_verificacao_BOOLEAN = (strlen($vStatus) && $vStatus == "1") ? "1" : "0";
    }

    public function __actionEdit() {

        $numeroRegistros = Helper::POST("numeroRegs");

        $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
        $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);

        $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
        $objPVBB->select($idPVBB);

        $vObjBancoBanco = new EXTDAO_Banco_banco();
        $vIdBancoBanco = $vObjBancoBanco->getIdBancoBanco($idPVBB);
        $vObjBancoBanco->select($vIdBancoBanco);

        $varGET = Param_Get::getIdentificadorProjetosVersao();
        $varGet =$varGET ;
        $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id")) . $varGet;
        $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id")) . $varGet;
        for ($i = 1; $i <= $numeroRegistros; $i++) {
            $this->setByPost($i);
            //Ajustando o fato do <select> ser disativado
            $vObjTabelaTabelaSendoEditado = new EXTDAO_Tabela_tabela();
            $vObjTabelaTabelaSendoEditado->select($this->getId());
            //se nao houver alteracao
            if ($vObjTabelaTabelaSendoEditado->getProducao_tabela_id_INT() == $this->getProducao_tabela_id_INT())
                continue;

            $this->setHomologacao_tabela_id_INT($vObjTabelaTabelaSendoEditado->getHomologacao_tabela_id_INT());
            $this->setTipo_operacao_atualizacao_banco_id_INT($vObjTabelaTabelaSendoEditado->getTipo_operacao_atualizacao_banco_id_INT());

            $idTProducao = $this->getProducao_tabela_id_INT();
            $idTTProducao = EXTDAO_Tabela_tabela::getIdTabelaTabelaDaTabelaProducao($idPVBB, $idTProducao);

            $vObjTabelaTabelaRelacionadoTabelaProducao = null;
            if ($idTTProducao != null) {
                $vObjTabelaTabelaRelacionadoTabelaProducao = new EXTDAO_Tabela_tabela();
                $vObjTabelaTabelaRelacionadoTabelaProducao->select($idTTProducao);
            }

            if ($idTTProducao == null ||
                    $idTTProducao == $this->getId() ||
                    $vObjTabelaTabelaRelacionadoTabelaProducao == null ||
                    (
                    $vObjTabelaTabelaRelacionadoTabelaProducao != null &&
                    !strlen($vObjTabelaTabelaRelacionadoTabelaProducao->getHomologacao_tabela_id_INT())
                    )
            ) {
                $reinicializaEstrutura = false;
                if ($idTTProducao != $this->getId())
                    $reinicializaEstrutura = true;
                //Se o relacionamento da tabela de producao atual for com outra tabela entao esse deve ser deletada
                if ($vObjTabelaTabelaRelacionadoTabelaProducao != null) {
                    $this->delete($idTTProducao);
                } else if ($vObjTabelaTabelaRelacionadoTabelaProducao == null &&
                        strlen($vObjTabelaTabelaSendoEditado->getProducao_tabela_id_INT())) {
                    //se for desassociando a atual tabela de produÃ§Ã£o
                    $vObjTabelaTabelaRelacionadoTabelaProducao = new EXTDAO_Tabela_tabela();
                    $vObjTabelaTabelaRelacionadoTabelaProducao->setProducao_tabela_id_INT($vObjTabelaTabelaSendoEditado->getProducao_tabela_id_INT());
                    $vObjTabelaTabelaRelacionadoTabelaProducao->setProjetos_versao_id_INT($vObjTabelaTabelaSendoEditado->getProjetos_versao_id_INT());
                    $vObjTabelaTabelaRelacionadoTabelaProducao->setProjetos_versao_banco_banco_id_INT($vObjTabelaTabelaSendoEditado->getProjetos_versao_banco_banco_id_INT());
                    $vObjTabelaTabelaRelacionadoTabelaProducao->setStatus_verificacao_BOOLEAN("0");
                    $vObjTabelaTabelaRelacionadoTabelaProducao->formatarParaSQL();
                    $vObjTabelaTabelaRelacionadoTabelaProducao->insert();
                    $vObjTabelaTabelaRelacionadoTabelaProducao->selectUltimoRegistroInserido();
                    $vIdTipoOperacao = $vObjTabelaTabelaRelacionadoTabelaProducao->atualizaTipoOperacaoAtualizacaoBanco();

                    $vObjTabelaTabelaRelacionadoTabelaProducao->setTipo_operacao_atualizacao_banco_id_INT($vIdTipoOperacao);

                    $vObjTabelaTabelaRelacionadoTabelaProducao->formatarParaSQL();

                    $vObjTabelaTabelaRelacionadoTabelaProducao->update($vObjTabelaTabelaRelacionadoTabelaProducao->getId());

                    $vObjBancoBanco->inicializaEstruturaDeAtributo($idPVBB, $vObjTabelaTabelaRelacionadoTabelaProducao);
                    $objPVBB->atualizaTipoOperacaoAtributoAtributo();
                }
                if ($reinicializaEstrutura) {
                    $this->delete($this->getId());

                    $this->formatarParaSQL();

                    $this->insert();

                    $this->selectUltimoRegistroInserido();

                    $vObjBancoBanco->inicializaEstruturaDeAtributo($idPVBB, $this);
                    $objPVBB->atualizaTipoOperacaoAtributoAtributo();
                } else {
                    $this->formatarParaSQL();

                    $this->update();
                }


                $this->select($this->getId());

                $vIdTipoOperacao = $this->atualizaTipoOperacaoAtualizacaoBanco();

                $this->setTipo_operacao_atualizacao_banco_id_INT($vIdTipoOperacao);

                $this->formatarParaSQL();

                $this->update($this->getId());
            } else {
                $vStrMensagemAlertaDuplicadaDeTabelaProducao = EXTDAO_Tabela_tabela::getMensagemTabelaHomologacaoComTabelaProducaoDuplicada($idPVBB, $idTProducao);
                if (strlen($vStrMensagemAlertaDuplicadaDeTabelaProducao) > 0)
                    $vStrMensagemAlertaDuplicadaDeTabelaProducao = "Existe associaÃ§Ã£o de outra tabela de homologaÃ§Ã£o com a tabela de produÃ§Ã£o selecionada, resolva as pendÃªncias de: " . $vStrMensagemAlertaDuplicadaDeTabelaProducao;
                else
                    $vStrMensagemAlertaDuplicadaDeTabelaProducao = "";

                return array("location: $urlErro&$varGET&msgErro=$vStrMensagemAlertaDuplicadaDeTabelaProducao&id{$i}={$this->getId()}");
            }
        }

        return array("location: $urlSuccess&$varGET&msgSucesso=Relacionamento entre os tabelas alterado com sucesso");
    }

    public function salvaListaVerificada() {
        $nextAction = "lists_gerencia_tabela_tabela";
        $varGET = Param_Get::getIdentificadorProjetosVersao();

        $urlSuccess = Helper::getUrlAction($nextAction, Helper::POST("id"));
        $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));
        $numeroRegistros = Helper::POST("numeroRegs");

        $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
        $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);

        $vObjBancoBanco = new EXTDAO_Banco_banco();
        $vIdBancoBanco = $vObjBancoBanco->getIdBancoBanco($idPVBB);
        $vObjBancoBanco->select($vIdBancoBanco);

        for ($i = 1; $i <= $numeroRegistros; $i++) {
            $vStatus = $this->formatarDados($_POST["status_verificacao_BOOLEAN{$i}"]);
            $vId = $_POST["id{$i}"];
            if (!strlen($vId))
                break;
            $this->select($vId);
            $this->setStatus_verificacao_BOOLEAN((strlen($vStatus) && $vStatus == "1") ? "1" : "0");
            $this->formatarParaSQL();
            $this->update($this->getId());
        }

        return array("location: {$urlSuccess}{$varGET}&msgSucesso=As tabelas foram marcadas como verificada.");
    }

    
    
    public static function getIdTabelaTabelaDaTabelaProducao($idPVBB, $pIdTabelaProducao) {
        if (!is_numeric($pIdTabelaProducao))
            return null;
        $db = new Database();
        $q = "SELECT id
FROM tabela_tabela
WHERE producao_tabela_id_INT = $pIdTabelaProducao 
    AND projetos_versao_banco_banco_id_INT = $idPVBB 
    LIMIT 0,1 " ;
        
        $db->query($q);
        
        return Helper::getResultSetToPrimeiroResultado($db->result);
    }
    
    public static function getIdTabelaTabelaDaTabelaHomologacao(
        $idPVBB, $pIdTabelaHomologacao, Database $db = null) {
        if (!is_numeric($pIdTabelaHomologacao))
            return null;
        if($db == null)
            $db = new Database();
        $db->query("SELECT id
FROM tabela_tabela
WHERE homologacao_tabela_id_INT = $pIdTabelaHomologacao 
    AND projetos_versao_banco_banco_id_INT = $idPVBB 
    LIMIT 0,1 ");
        return Helper::getResultSetToPrimeiroResultado($db->result);
    }

    public static function chavePrimariaDiferente($idPVBB, $idTT, $db = null) {
        if($db == null)
        $db = new Database();
        $db->query("SELECT aa1.id
                FROM atributo a1 JOIN atributo_atributo aa1 
                    ON aa1.homologacao_atributo_id_INT = a1.id
                WHERE a1.primary_key_BOOLEAN = '1' 
                    AND aa1.tabela_tabela_id_INT = {$idTT} 
                    AND aa1.projetos_versao_banco_banco_id_INT = {$idPVBB}
                
                AND (
                        SELECT COUNT(a2.id)
                        FROM atributo a2
                        WHERE aa1.producao_atributo_id_INT = a2.id
                        LIMIT 0,1
                    ) = 0
                 LIMIT 0,1 ");
        $vId = Helper::getResultSetToPrimeiroResultado($db->result);
        if (strlen($vId))
            return true;
        else
            return false;
    }

    public static function getIdentificador($idTT) {
        $consulta = "SELECT CONCAT(
				 IF(t1.nome is null, 'null', t1.nome) , 
				'-',
				IF(t2.nome is null, 'null', t2.nome) 
		) tag
FROM tabela_tabela tt
		LEFT JOIN tabela t1
				ON tt.homologacao_tabela_id_INT = t1.id
		LEFT JOIN tabela t2
				ON tt.producao_tabela_id_INT = t2.id
WHERE tt.id = {$idTT}
LIMIT 0,1";
        $db = new Database();
        $db->query($consulta);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    
    public static function existeChaveExtrangeiraASerDeletada($pObjTabelaHomologacao, $pObjTabelaProducao, $idPVBB, $db = null) {

        $vConsulta = "SELECT 
                t2.nome AS tabela_prod,
                a2.fk_nome AS atributo_prod
FROM tabela_tabela tt 
		JOIN tabela t2 ON tt.producao_tabela_id_INT = t2.id
		JOIN atributo a2 ON a2.tabela_id_INT = t2.id 
		JOIN tabela t_fk2 ON a2.fk_tabela_id_INT = t_fk2.id
		JOIN atributo a_fk2 ON a2.fk_atributo_id_INT = a_fk2.id
		JOIN atributo_atributo aa2 ON aa2.producao_atributo_id_INT = a2.id AND aa2.projetos_versao_banco_banco_id_INT = $idPVBB
		LEFT JOIN tabela t1 ON t1.id = tt.homologacao_tabela_id_INT
		LEFT JOIN atributo a1 ON a1.id = aa2.homologacao_atributo_id_INT

WHERE 


		((tt.homologacao_tabela_id_INT IS NULL ) 
	OR


		(aa2.homologacao_atributo_id_INT IS NULL)
	OR
		(	
                               aa2.homologacao_atributo_id_INT IS NOT NULL

			AND
				(
                                        a1.valor_default != a2.valor_default
                                OR
                                        a1.tamanho_INT != a2.tamanho_INT
                                OR
                                        a1.tipo_sql != a2.tipo_sql
                                OR
                                        a1.nome != a2.nome
                                OR
					a1.not_null_BOOLEAN != a2.not_null_BOOLEAN
                                OR
					a1.update_tipo_fk != a2.update_tipo_fk
				OR
					a1.delete_tipo_fk != a2.delete_tipo_fk
				OR
					( 


							SELECT COUNT(tt_fk.id)
								FROM tabela_tabela tt_fk JOIN 
									atributo_atributo aa_fk ON aa_fk.tabela_tabela_id_INT = tt_fk.id
							WHERE a2.fk_tabela_id_INT = tt_fk.producao_tabela_id_INT
									AND a1.fk_tabela_id_INT = tt_fk.homologacao_tabela_id_INT 
									AND a2.fk_atributo_id_INT = aa_fk.producao_atributo_id_INT
									AND a1.fk_atributo_id_INT = aa_fk.homologacao_atributo_id_INT
                                                                        
							LIMIT 0,1
						) = 0

				)
		))   

";
        if ($pObjTabelaHomologacao != null && strlen($pObjTabelaHomologacao->getId()))
            $vConsulta .= " AND tt.homologacao_tabela_id_INT = {$pObjTabelaHomologacao->getId()}";
        else
            $vConsulta .= " AND tt.homologacao_tabela_id_INT IS NULL";

        if ($pObjTabelaProducao != null && strlen($pObjTabelaProducao->getId()))
            $vConsulta .= " AND tt.producao_tabela_id_INT = {$pObjTabelaProducao->getId()}";
        else
            $vConsulta .= " AND tt.producao_tabela_id_INT IS NULL";

        $vConsulta .= " LIMIT 0,1 ";
        if($db == null)
        $db = new Database();
        $db->query($vConsulta);
        
        $valor = $db->getPrimeiraTuplaDoResultSet(0);
        if(strlen($valor))return true;
        else return false;
    }
    
    public static function getResultSetChaveExtrangeiraDeletada($pObjTabelaHomologacao, $pObjTabelaProducao, $idPVBB, $db = null) {

        $vConsulta = "SELECT 
                t2.nome AS tabela_prod,
                a2.fk_nome AS atributo_prod
FROM tabela_tabela tt 
		JOIN tabela t2 ON tt.producao_tabela_id_INT = t2.id
		JOIN atributo a2 ON a2.tabela_id_INT = t2.id 
		JOIN tabela t_fk2 ON a2.fk_tabela_id_INT = t_fk2.id
		JOIN atributo a_fk2 ON a2.fk_atributo_id_INT = a_fk2.id
		JOIN atributo_atributo aa2 ON aa2.producao_atributo_id_INT = a2.id AND aa2.projetos_versao_banco_banco_id_INT = $idPVBB
		LEFT JOIN tabela t1 ON t1.id = tt.homologacao_tabela_id_INT
		LEFT JOIN atributo a1 ON a1.id = aa2.homologacao_atributo_id_INT

WHERE 


		((tt.homologacao_tabela_id_INT IS NULL ) 
	OR


		(aa2.homologacao_atributo_id_INT IS NULL)
	OR
		(	
                               aa2.homologacao_atributo_id_INT IS NOT NULL

			AND
				(
                                        a1.valor_default != a2.valor_default
                                OR
                                        a1.tamanho_INT != a2.tamanho_INT
                                OR
                                        a1.tipo_sql != a2.tipo_sql
                                OR
                                        a1.nome != a2.nome
                                OR
					a1.not_null_BOOLEAN != a2.not_null_BOOLEAN
                                OR
					a1.fk_nome != a2.fk_nome        
                                OR
					a1.update_tipo_fk != a2.update_tipo_fk
				OR
					a1.delete_tipo_fk != a2.delete_tipo_fk
				OR
					( 


							SELECT COUNT(tt_fk.id)
								FROM tabela_tabela tt_fk JOIN 
									atributo_atributo aa_fk ON aa_fk.tabela_tabela_id_INT = tt_fk.id
							WHERE a2.fk_tabela_id_INT = tt_fk.producao_tabela_id_INT
									AND a1.fk_tabela_id_INT = tt_fk.homologacao_tabela_id_INT 
									AND a2.fk_atributo_id_INT = aa_fk.producao_atributo_id_INT
									AND a1.fk_atributo_id_INT = aa_fk.homologacao_atributo_id_INT
                                                                        
							LIMIT 0,1
						) = 0

				)
		))     
";
        if ($pObjTabelaHomologacao != null && strlen($pObjTabelaHomologacao->getId()))
            $vConsulta .= " AND tt.homologacao_tabela_id_INT = {$pObjTabelaHomologacao->getId()}";
        else
            $vConsulta .= " AND tt.homologacao_tabela_id_INT IS NULL ";

        if ($pObjTabelaProducao != null && strlen($pObjTabelaProducao->getId()))
            $vConsulta .= " AND tt.producao_tabela_id_INT = {$pObjTabelaProducao->getId()} ";
        else
            $vConsulta .= " AND tt.producao_tabela_id_INT IS NULL ";
        if($db == null)
        $db = new Database();
        $db->query($vConsulta);
        return $db->result;
    }

    public static function getResultSetChaveExtrangeiraCriada($pObjTabelaHomologacao, $pObjTabelaProducao, $idPVBB, $db = null) {

        $consulta = "SELECT t1.nome AS tabela_hom,
a1.nome AS atributo_hom,
t_fk1.nome AS tabela_fk_hom, 
a_fk1.nome AS atributo_fk_hom,
t2.nome AS tabela_prod,
a2.nome AS atributo_prod,
a2.update_tipo_fk AS on_update_prod,
a1.update_tipo_fk AS on_update_hom,
a2.delete_tipo_fk AS on_delete_prod,
a1.delete_tipo_fk AS on_delete_hom,
a2.fk_tabela_id_INT AS tabela_fk_prod,
a1.fk_tabela_id_INT AS tabela_fk_hom,
a1.fk_nome AS nome_fk
FROM tabela_tabela tt 
		JOIN tabela t1 ON tt.homologacao_tabela_id_INT = t1.id
		JOIN atributo a1 ON a1.tabela_id_INT = t1.id 
		JOIN atributo a_fk1 ON a1.fk_atributo_id_INT = a_fk1.id
		JOIN tabela t_fk1 ON a1.fk_tabela_id_INT = t_fk1.id
		JOIN atributo_atributo aa1 ON aa1.homologacao_atributo_id_INT = a1.id AND aa1.tabela_tabela_id_INT = tt.id
		LEFT JOIN tabela t2 ON t2.id = tt.producao_tabela_id_INT
		LEFT JOIN atributo a2 ON a2.id = aa1.producao_atributo_id_INT
		

WHERE ((tt.producao_tabela_id_INT IS NULL) 
	OR
		(aa1.producao_atributo_id_INT IS NULL)
	OR
		(	
				aa1.producao_atributo_id_INT IS NOT NULL
			AND
				aa1.producao_atributo_id_INT = a2.id
			AND
				(
                                        a1.valor_default != a2.valor_default
                                OR
                                        a1.tamanho_INT != a2.tamanho_INT
                                OR
                                        a1.tipo_sql != a2.tipo_sql
                                OR
                                        a1.nome != a2.nome
                                OR
					a1.not_null_BOOLEAN != a2.not_null_BOOLEAN
                                OR
					a2.fk_nome != a1.fk_nome
                                OR
					a2.update_tipo_fk != a1.update_tipo_fk
				OR
					a2.delete_tipo_fk != a1.delete_tipo_fk
				OR ( 

							SELECT COUNT(tt_fk.id)
								FROM tabela_tabela tt_fk JOIN 
									atributo_atributo aa_fk ON aa_fk.tabela_tabela_id_INT = tt_fk.id
							WHERE a1.fk_tabela_id_INT = tt_fk.homologacao_tabela_id_INT
									AND a2.fk_tabela_id_INT = tt_fk.producao_tabela_id_INT 
									AND a1.fk_atributo_id_INT = aa_fk.homologacao_atributo_id_INT
									AND a2.fk_atributo_id_INT = aa_fk.producao_atributo_id_INT
							LIMIT 0,1
						) = 0

				)
		))      
";
        $consulta .= " AND tt.projetos_versao_banco_banco_id_INT = $idPVBB ";
        if ($pObjTabelaHomologacao != null && strlen($pObjTabelaHomologacao->getId()))
            $consulta .= " AND tt.homologacao_tabela_id_INT = {$pObjTabelaHomologacao->getId()}";
        else
            $consulta .= " AND tt.homologacao_tabela_id_INT IS NULL";

        if ($pObjTabelaProducao != null && strlen($pObjTabelaProducao->getId()))
            $consulta .= " AND tt.producao_tabela_id_INT = {$pObjTabelaProducao->getId()}";
        else
            $consulta .= " AND tt.producao_tabela_id_INT IS NULL";
        if($db == null)
        $db = new Database();
        $db->query($consulta);
        return $db->result;
    }

    
    //ATENÃ‡ÃƒO esse metodo pode deletar ou editar o registro $idTT passado na entrada
    //essa funcao nÃ£o Ã© executada se o tipo de operaÃ§Ã£o da tabela for ignore.
    public static function simulaAtualizacaoHmgParaProd($idTT, $db = null){
        
        if($db == null) $db = new Database();
        $objTT = new EXTDAO_Tabela_tabela();
        $objTT->select($idTT);
        
        if($objTT->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE)
            return;
        //TODO retirar linha abaixo
        //$trunk = new Trunk();
        $trunk = Trunk::factory();
        //$trunk = new Trunk();
        $idPVBBTabelaProducao = $trunk->getPVBBDoBancoDeDadosDeProducao($objTT->getProjetos_versao_banco_banco_id_INT());
//        $objBancoProducao = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBTabelaProducao);
        $idBancoPrd = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($idPVBBTabelaProducao, $db);
        $idPVTabelaProducao = $trunk->getPVDoBancoDeDadosDeProducao($objTT->getProjetos_versao_id_INT());
        $idPVTabelaHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($objTT->getProjetos_versao_id_INT());
        
        $prefixoProd = "";
        
        $tipoAnalise = $trunk->getTipoAnaliseDoProjetosVersao( $objTT->getProjetos_versao_id_INT());
        //$objPV = new EXTDAO_Projetos_versao();
        switch ($tipoAnalise) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                $prefixoProd =  "__";
                break;
            default:
                break;
        } 
        
        $objTabelaHmg= $objTT->getObjTabelaHomologacao();
        $objTabelaPrd = $objTT->getObjTabelaProducao();
        
        switch ($objTT->getTipo_operacao_atualizacao_banco_id_INT()) {
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                
                if($objTabelaHmg != null && $objTabelaPrd == null){
                    
                    $nomeTabelaProducao = $prefixoProd.$objTabelaHmg->getNome();
                    
                    $idTabelaPrd = EXTDAO_Tabela::getIdTabela($idPVTabelaProducao, $idBancoPrd, $nomeTabelaProducao, $db);
                    //faz uma copia da tabela do banco hmg em prd
                    $objTabelaHmg->setId(null);
                    $objTabelaHmg->setBanco_id_INT($idBancoPrd);
                    $objTabelaHmg->setProjetos_versao_id_INT($idPVTabelaProducao);
                    
                    $objTabelaHmg->setNome($nomeTabelaProducao);
                    $objTabelaHmg->formatarParaSQL();
                    if(strlen($idTabelaPrd)){
                        $objTabelaHmg->update($idTabelaPrd);
                    } else {
                        $objTabelaHmg->insert();
                        $idTabelaPrd = $objTabelaHmg->getIdDoUltimoRegistroInserido();
                    }
                    $objTabelaHmg->select($idTabelaPrd);
                    
                    $objTT->setProducao_tabela_id_INT($idTabelaPrd);
                    $objTT->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION);
                    $objTT->formatarParaSQL();
                    $objTT->update($objTT->getId());
                }
                break;
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
                if($objTabelaHmg == null && $objTabelaPrd != null){
                    $objTabelaPrd->delete($objTabelaPrd->getId());
                    $objTT->delete($idTT);
                }
                break;
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION:
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                if($objTabelaHmg != null && $objTabelaPrd != null){
                     //faz uma copia da tabela do banco hmg em prd
                    
                    $objTabelaHmg->setId(null);
                    $objTabelaHmg->setBanco_id_INT($objTabelaPrd->getBanco_id_INT());
                    $objTabelaHmg->setProjetos_versao_id_INT($objTabelaPrd->getProjetos_versao_id_INT());
                    
                    $nomeTabelaProducao = $prefixoProd.$objTabelaHmg->getNome();
                    
                    $objTabelaHmg->setNome($nomeTabelaProducao);
                    $objTabelaHmg->formatarParaSQL();
                    
                    $objTabelaHmg->update($objTabelaPrd->getId());

                    $objTabelaHmg->select($objTabelaPrd->getId());

                    $objTT->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION);
                    $objTT->formatarParaSQL();
                    $objTT->update($idTT);
                }
                break;

            default:
                break;
        }
       
    }
}

