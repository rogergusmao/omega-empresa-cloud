<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Diretorio_windows_mobile
    * NOME DA CLASSE DAO: DAO_Diretorio_windows_mobile
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Diretorio_windows_mobile.php
    * TABELA MYSQL:       diretorio_windows_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Diretorio_windows_mobile extends DAO_Diretorio_windows_mobile
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Diretorio_windows_mobile";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_androidDiretorioClassesDAO = "Diretório Classe DAO";
			$this->label_androidDiretorioClassesEXTDAO = "Diretório Classe EXTDAO";
			$this->label_androidForms = "Diretório Forms";
			$this->label_androidFiltros = "Diretório Filtros";
			$this->label_androidLists = "Diretório Lists";
			$this->label_androidAdapter = "Diretório Adapter";
			$this->label_androidItemList = "Diretório Itemlist";
			$this->label_androidDetail = "Diretório Detail";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Diretorio_windows_mobile();

        }

	}

    
