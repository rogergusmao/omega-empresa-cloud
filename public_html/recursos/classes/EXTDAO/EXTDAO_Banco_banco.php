<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Banco_banco
 * NOME DA CLASSE DAO: DAO_Banco_banco
 * DATA DE GERAÃ‡ÃƒO:    28.01.2013
 * ARQUIVO:            EXTDAO_Banco_banco.php
 * TABELA MYSQL:       banco_banco
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡ÃƒO DA CLASSE
// **********************

class EXTDAO_Banco_banco extends DAO_Banco_banco {

    public $prefixoNomeTabelaProducao = "";
    public $prefixoNomeTabelaHomologacao = "";
    
    public function setPrefixoNomeTabelaHomologacao($token){
        $this->prefixoNomeTabelaHomologacao = $token;
    }
    public function setPrefixoNomeTabelaProducao($token){
        $this->prefixoNomeTabelaProducao = $token;
    }
    
    public function __construct($configDAO=null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Banco_banco";

       
    }
    
    
    public function valorCampoLabel(){
        $valor =  "";
        if(strlen($this->getFkObjHomologacao_banco()->valorCampoLabel()))
            $valor .= "</br><b>HMG:</b> ".$this->getFkObjHomologacao_banco()->valorCampoLabel();
        if(strlen($this->getFkObjProducao_banco()->valorCampoLabel()))
            $valor .= "</br><b>PROD:</b> ".$this->getFkObjProducao_banco()->valorCampoLabel();
        return $valor;
    }


    public function setLabels() {

        $this->label_id = "Id";
        $this->label_homologacao_banco_id_INT = "Banco de HomologaÃ§Ã£o";
        $this->label_producao_banco_id_INT = "Banco de ProduÃ§Ã£o";
        $this->label_projetos_versao_id_INT = "VersÃ£o do Projeto";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Banco_banco();
    }

    public function populaTabelaDoProjetoVersaoDoBancoDeProducao($idPV) {
        $vIdBancoProducao = $this->getProducao_banco_id_INT();

        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);
        
        $vVetorTabelaProducao = $vObjBancoProducao->showTables();
        if($vVetorTabelaProducao != null)
        foreach ($vVetorTabelaProducao as $tabelaProducao) {
            
            $vIdTabelaProducaoExistente = EXTDAO_Tabela::existeTabela($tabelaProducao, $vIdBancoProducao, $idPV);
            if ($vIdTabelaProducaoExistente == null) {
                $objTabela = new EXTDAO_Tabela();    
                $objTabela->setBanco_id_INT($vIdBancoProducao);
                $objTabela->setNome($tabelaProducao);
                $objTabela->setProjetos_versao_id_INT($idPV);
                $objTabela->formatarParaSQL();
                $objTabela->insert();
            }
        }
        
    }

    public function populaTabelaDoProjetoVersaoDoBancoDeHomologacao($idPV, $db = null) {
        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();
        if($db == null) $db = new Database();
        $vObjBancoHomologacao = new EXTDAO_Banco($db);
        $vObjBancoHomologacao->select($vIdBancoHomologacao);

        $vVetorTabelaHomologacao = $vObjBancoHomologacao->showTables();
//        print_r($vVetorTabelaHomologacao);
//        exit();



        if($vVetorTabelaHomologacao != null) {
            $objTabela = new EXTDAO_Tabela($db);
            foreach ($vVetorTabelaHomologacao as $tabelaHomologacao) {

                $vIdTabelaHomologacaoExistente = EXTDAO_Tabela::existeTabela($tabelaHomologacao, $vIdBancoHomologacao, $idPV, $db);
                if ($vIdTabelaHomologacaoExistente == null) {

                    $objTabela->clear();
                    $objTabela->setBanco_id_INT($vIdBancoHomologacao);
                    $objTabela->setNome($tabelaHomologacao);
                    $objTabela->setProjetos_versao_id_INT($idPV);
                    $objTabela->formatarParaSQL();
                    $objTabela->insert();
                }
            }
        }
    }

    public function getObjBancoHomologacao() {

        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();
        
        $vObjBancoHomologacao = new EXTDAO_Banco();
        $vObjBancoHomologacao->select($vIdBancoHomologacao);
        return $vObjBancoHomologacao->getObjDatabase();
    }
    
    public function getEXTDAOBancoHomologacao() {

        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();
        
        $vObjBancoHomologacao = new EXTDAO_Banco();
        $vObjBancoHomologacao->select($vIdBancoHomologacao);
        return $vObjBancoHomologacao;
    }
    public function getEXTDAOBancoProducao() {

        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);
        return $vObjBancoProducao;
    }
    public function getObjBancoProducao() {

        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);
        return $vObjBancoProducao->getObjDatabase();
    }

    public function populaAtributoDoProjetoVersaoDoBanco(
        $pIdBancoHomologacao, 
        $pIdTabela, 
        $pIdProjetoVersao, 
        $db = null) {
        
        if($db == null)
            $db = new Database();
        $vObjBanco = new EXTDAO_Banco($db);
        $vObjBanco->select($pIdBancoHomologacao);
        
        $objTabela = new EXTDAO_Tabela($db);
        $objTabela->select($pIdTabela);
        
        
        $dbExterno = $vObjBanco->getObjDatabase();
        $vVetorAtributo = $vObjBanco->getVetorObjAtributoDaTabela($objTabela->getNome(), $dbExterno);
        


        $objAtributoAux = new EXTDAO_Atributo($db);


        $vVetorChaveExtrangeira = $vObjBanco->getListaChaveExtrangeiraDaTabelaDoBanco($objTabela->getNome(), $dbExterno);
        $vVetorChaveUnica = $vObjBanco->getListaChaveUnicaDaTabelaDoBanco($objTabela->getNome(), $dbExterno);
        
        $numeroDeInsercoes = 0;

        $objAtributoRef = new EXTDAO_Atributo($db);
        $objAtributo = new EXTDAO_Atributo($db);
        $objTC = new EXTDAO_Tabela_chave($db);
        if($vVetorAtributo != null)
        foreach ($vVetorAtributo as $atributo) {
            $objAtributoRef->clear();
            $objAtributo->clear();
            $vIdAtributoExistente = $objAtributo->existeAtributo($atributo["Field"], $pIdTabela, $pIdProjetoVersao);
            
            
            if ($vIdAtributoExistente != null) {
                //inicializando o atributo apenas se necessario. Quando a funcao clear() for criada na DAO nÃ£o haverÃ¡ mais necessidade
                if($objAtributo == null)
                    $objAtributo = new EXTDAO_Atributo($db);
                $objAtributo->select($vIdAtributoExistente);
            } else {
                
                $objAtributo = new EXTDAO_Atributo($db);
            }
            $objAtributo->setBanco_id_INT($pIdBancoHomologacao);
            $vStrTipo = $atributo["Type"];

            $i = strpos($vStrTipo, "(");
            $vTamanho = 0;
            $vDecimal = 0;
            if ($i > 0) {
                $j = strpos($vStrTipo, ")");
                $l = strpos($vStrTipo, ",");
                $vStrTipo = substr($atributo["Type"], 0, $i);
                if ($l > $i) {
                    $vTamanho = substr($atributo["Type"], $i + 1, $l - ($i + 1));
                    $vDecimal = substr($atributo["Type"], $l + 1, $l - ($j - 1));
                } else if ($j > $i)
                    $vTamanho = substr($atributo["Type"], $i + 1, $j - ($i + 1));
            }

            $objAtributo->setNome($atributo["Field"]);
            $objAtributo->setTabela_id_INT($pIdTabela);
            $objAtributo->setProjetos_versao_id_INT($pIdProjetoVersao);
            $objAtributo->setTipo_sql($vStrTipo);
            $objAtributo->setTamanho_INT($vTamanho);

            if ($atributo["Null"] == "NO")
                $objAtributo->setNot_null_BOOLEAN("1");
            else
                $objAtributo->setNot_null_BOOLEAN("0");

            if ($atributo["Key"] == "PRI")
                $objAtributo->setPrimary_key_BOOLEAN("1");
            else
                $objAtributo->setPrimary_key_BOOLEAN("0");

            if (strlen($atributo["Default"]))
                $objAtributo->setValor_default($atributo["Default"]);

            if ($atributo["Extra"] == "auto_increment")
                $objAtributo->setAuto_increment_BOOLEAN("1");
            else
                $objAtributo->setAuto_increment_BOOLEAN("0");
            
            $objChaveExtrangeiraDoAtributoAtual = null;
            if($vVetorChaveExtrangeira != null)
            foreach ($vVetorChaveExtrangeira as $vObjExtrangeira) {
              
                if ($vObjExtrangeira["atributo"] == $atributo["Field"]) {
                    $objChaveExtrangeiraDoAtributoAtual = $vObjExtrangeira;
                    break;
                }
            }
           
            
            $vIsChaveUnica = false;
            if($vVetorChaveUnica != null)
            foreach ($vVetorChaveUnica as $vObjUnica) {
                if ($vObjUnica["atributo"] == $atributo["Field"]) {
                    $vIsChaveUnica = true;
                    $objAtributo->setUnique_nome($vObjUnica["nome_chave"]);
                    $objAtributo->setUnique_BOOLEAN("1");
                    break;
                }
            }
            if (!$vIsChaveUnica) {
                $objAtributo->setUnique_nome(null);
                $objAtributo->setUnique_BOOLEAN("0");
            }
            
            if ($objChaveExtrangeiraDoAtributoAtual != null) {
                
                $vIdTabelaFK = EXTDAO_Tabela::existeTabela(
                    $objChaveExtrangeiraDoAtributoAtual["tabela_referencia"], 
                    $vObjBanco->getId(), 
                    $pIdProjetoVersao, 
                    $db);
                //TODO REMOVER
//                if($objTabela->getNome() == "tela" && $atributo["Field"] == "tela_estado_id_INT"){
//                    print_r($vObjChaveExtrangeiraDoAtributoAtual);
//                    echo "::{$vObjBanco->getId()}::$pIdProjetoVersao";
//                    exit();
//                }
                if ($vIdTabelaFK != null) {
                    $objAtributo->setFk_tabela_id_INT($vIdTabelaFK);

                    $vIdAtributoRef = $objAtributoAux->existeAtributo(
                        $objChaveExtrangeiraDoAtributoAtual["atributo_referencia"], 
                        $vIdTabelaFK, 
                        $pIdProjetoVersao);
                    if ($vIdAtributoRef == null) {
                        
                        $objAtributoRef->setNome($objChaveExtrangeiraDoAtributoAtual["atributo_referencia"]);
                        $objAtributoRef->setProjetos_versao_id_INT($pIdProjetoVersao);
                        $objAtributoRef->setTabela_id_INT($vIdTabelaFK);
                        $objAtributoRef->setBanco_id_INT($pIdBancoHomologacao);
                        $objAtributoRef->formatarParaSQL();
                        $objAtributoRef->insert();

                        $vIdAtributoRef = $objAtributoRef->getIdDoUltimoRegistroInserido();
                    }
                    if ($vIdAtributoRef != null) {
                        $objAtributo->setBanco_id_INT($pIdBancoHomologacao);
                        $objAtributo->setFk_atributo_id_INT($vIdAtributoRef);
                        $objAtributo->setUpdate_tipo_fk($objChaveExtrangeiraDoAtributoAtual["on_update"]);
                        $objAtributo->setDelete_tipo_fk($objChaveExtrangeiraDoAtributoAtual["on_delete"]);
                        $objAtributo->setFk_nome($objChaveExtrangeiraDoAtributoAtual["nome_chave"]);
                    }
                }
            }
// if($objTabela->getNome()  == "usuario_categoria_permissao" 
//                  && $atributo["Field"]  == "usuario_id_INT"){
//                print_r($objChaveExtrangeiraDoAtributoAtual);
////                print_r($vVetorAtributo);
//                echo "<br/>$vIdAtributoExistente::$vIdTabelaFK::$vIdAtributoRef<br/>";
////                print_r($vVetorChaveExtrangeira);
//                exit();
//            }
            $objAtributo->formatarParaSQL();
            if ($vIdAtributoExistente != null)
                $objAtributo->update($vIdAtributoExistente);
            else
                $objAtributo->insert();
        }
        //TODO REMOVER
//        if($objTabela->getNome() == "tela"){
//            
//            exit();
//        }
        
        $vVetorObjChave = $vObjBanco->getChavesDaTabelaDoBanco($objTabela->getNome(), $dbExterno);
        $vVetorIdTabelaChave = EXTDAO_Tabela_chave::getListaChaveTabela($objTabela->getId(), $db);
        if($vVetorIdTabelaChave != null)
        foreach ($vVetorIdTabelaChave as $idTabelaChave) {
            $objTC->clear();
            $objTC->select($idTabelaChave);
            $nomeChaveTC = $objTC->getNome();
            
            $achou = false;
            if($vVetorObjChave != null)
            foreach ($vVetorObjChave as $vChave) {
                $vNomeChave = $vChave["Key_name"];
                if($vNomeChave == $nomeChaveTC){
                    $achou = true;
                    break;
                }
            }
            if(!$achou)
                $objTC->delete ($idTabelaChave);
        }
        if($vVetorObjChave != null)
        foreach ($vVetorObjChave as $vChave) {
            $vNomeChave = $vChave["Key_name"];
            
            $vIdChave = EXTDAO_Tabela_chave::getIdDaChave($vNomeChave, $objTabela->getId(), $db);
            $vObjChave = new EXTDAO_Tabela_chave();
            if (strlen($vIdChave)) {
                $vObjChave->select($vIdChave);
            }
            $vObjChave->setTabela_id_INT($pIdTabela);
            $vObjChave->setNome($vNomeChave);
            $vObjChave->setIndex_type($vChave["Index_type"]);

            $vObjChave->setNon_unique_BOOLEAN(strlen($vChave["Non_unique"]) ? $vChave["Non_unique"] : null);
            $vObjChave->formatarParaSQL();
            if (strlen($vIdChave)) {
                $vObjChave->update($vIdChave);
            } else {
                $vObjChave->insert();
                $vObjChave->selectUltimoRegistroInserido();
            }

            $vIdAtributo = EXTDAO_Atributo::getIdAtributo($vChave["Column_name"], $objTabela->getId(), $db);

            if (!strlen($vIdAtributo)) {
                Helper::imprimirMensagem("Atributo inexistente: " . $vChave[4], MENSAGEM_ERRO);
                exit();
            }
            $objTCA = new EXTDAO_Tabela_chave_atributo();
            $vIdTCA = null;
            $vIdTCA = EXTDAO_Tabela_chave_atributo::getTabelaChaveAtributo($vIdAtributo, $vObjChave->getId(), $db);
            if (strlen($vIdTCA))
                $objTCA->select($vIdTCA);

            $objTCA->setAtributo_id_INT($vIdAtributo);
            $objTCA->setSeq_INT($vChave["Seq_in_index"]);
            $objTCA->setTabela_chave_id_INT($vObjChave->getId());
            $objTCA->formatarParaSQL();
            if (strlen($vIdTCA))
                $objTCA->update($vIdTCA);
            else
                $objTCA->insert();
        }
    }

    public function atualizaTabelaProducaoComDadosDeSincronizacaoDoBancoDeProducao($idPV) {

        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);
        
        $db = $this->database;
        $trunk = Trunk::factory();
        //$pvBancos = $trunk->getProjetosVersaoDoBancoDeDados($idPV, $db);
        
        $idPVTabelaProducao = $trunk->getPVDoBancoDeDadosDeProducao($idPV);
        
//        $idPVTabelaProducao = $pvBancos['idPVTabelaProducao'];
        
        
        if ($vObjBancoProducao != null) {
            //$vVetorTabelaProducao = $vObjBancoProducao->showTables();
            $vVetorTabelaProducao = EXTDAO_Tabela::getVetorTabela($idPVTabelaProducao, $vIdBancoProducao, $db);
            if (in_array("sistema_tabela", $vVetorTabelaProducao)) {
                $vObjDatabaseProducao = $vObjBancoProducao->getObjDatabase();
                $vObjDatabaseProducao->query("SELECT nome, 
                        transmissao_web_para_mobile_BOOLEAN, 
                        transmissao_mobile_para_web_BOOLEAN, 
                        frequencia_sincronizador_INT 
                    FROM sistema_tabela");
                $vMatrizProducao = Helper::getResultSetToMatriz($vObjDatabaseProducao->result);
                for ($i = 0; $i < count($vMatrizProducao); $i++) {
                    $vObjSistemaTabela = $vMatrizProducao[$i];
                    if (in_array($vObjSistemaTabela[0], $vVetorTabelaProducao)) {
                        $objTabela = new EXTDAO_Tabela();
                        $vIdTabelaProducao = EXTDAO_Tabela::existeTabela($this->prefixoNomeTabelaProducao.$vObjSistemaTabela[0], $vIdBancoProducao, $idPV);
                        if ($vIdTabelaProducao != null) {
                            $objTabela->select($vIdTabelaProducao);
                            
                            $objTabela->setTransmissao_web_para_mobile_BOOLEAN($vObjSistemaTabela[1]);
                            $objTabela->setTransmissao_mobile_para_web_BOOLEAN($vObjSistemaTabela[2]);
                            $objTabela->setFrequencia_sincronizador_INT($vObjSistemaTabela[3]);
                            $objTabela->formatarParaSQL();
                            $objTabela->update($vIdTabelaProducao);

                        }
                    }
                }
            }
        }
    }

    public function atualizaTabelaComDadosDoSistemaTabela(
        $idPVWeb, 
        $idPVAndroid, 
        $objBancoPrototipoWeb, 
        $objBancoPrototipoAndroid,
        $homologacao = true) {
        
        $tabelasWeb = $objBancoPrototipoWeb->showTables();
        $tabelasAndroid = $objBancoPrototipoAndroid->showTables();

        if (in_array("sistema_tabela", $tabelasWeb)) {
            
            $objDatabase = $objBancoPrototipoWeb->getObjDatabase();
            
            $objDatabase->query("SELECT nome, 
                    transmissao_web_para_mobile_BOOLEAN, 
                    transmissao_mobile_para_web_BOOLEAN, 
                    frequencia_sincronizador_INT 
                FROM sistema_tabela");
            $matriz = Helper::getResultSetToMatriz($objDatabase->result);

            $db = new Database();
            for ($i = 0; $i < count($matriz); $i++) {
                $retSistemaTabela = $matriz[$i];
                if (in_array($retSistemaTabela[0], $tabelasWeb)) {
                    $nomeTabela = null;
                    if($homologacao){
                        $nomeTabela = $this->prefixoNomeTabelaHomologacao.$retSistemaTabela[0];
                    } else {
                        $nomeTabela = $this->prefixoNomeTabelaProducao.$retSistemaTabela[0];
                    }
                    $idTabela = EXTDAO_Tabela::existeTabela(
                        $nomeTabela, 
                        $objBancoPrototipoWeb->getId(), 
                        $idPVWeb,
                        $db);
//                    echo "TABELA: $idTabela";
                    if ($idTabela != null) {
                        $objTabela = new EXTDAO_Tabela($db);
                        $objTabela->select($idTabela);

                        $objTabela->setTransmissao_web_para_mobile_BOOLEAN($retSistemaTabela[1]);
                        $objTabela->setTransmissao_mobile_para_web_BOOLEAN($retSistemaTabela[2]);
                        $objTabela->setFrequencia_sincronizador_INT($retSistemaTabela[3]);
                        $objTabela->formatarParaSQL();
                        $objTabela->update($idTabela);
                    }
                }
                if (in_array($retSistemaTabela[0], $tabelasAndroid)) {
                    $nomeTabela = null;
                    if($homologacao){
                        $nomeTabela = $this->prefixoNomeTabelaHomologacao.$retSistemaTabela[0];
                    } else {
                        $nomeTabela = $this->prefixoNomeTabelaProducao.$retSistemaTabela[0];
                    }
                    $idTabela = EXTDAO_Tabela::existeTabela(
                        $nomeTabela, 
                        $objBancoPrototipoAndroid->getId(), 
                        $idPVAndroid,
                        $db);
                    if ($idTabela != null) {
                        $objTabela = new EXTDAO_Tabela($db);
                        $objTabela->select($idTabela);

                        $objTabela->setTransmissao_web_para_mobile_BOOLEAN($retSistemaTabela[1]);
                        $objTabela->setTransmissao_mobile_para_web_BOOLEAN($retSistemaTabela[2]);
                        $objTabela->setFrequencia_sincronizador_INT($retSistemaTabela[3]);
                        $objTabela->formatarParaSQL();
                        $objTabela->update($idTabela);
                    }
                }
            }
        }
    }


    public function bkpAtualizaSistemaTabelaDoBancoDeHomologacao($idPVPrototipo) {
//        $objBBSincronizadorWeb = new EXTDAO_Banco_banco();
//        $objBBSincronizadorWeb->select( $idBancoBancoSincronizadorWeb);
//        $databaseSW = $objBBSincronizadorWeb->getObjDatabase();
//        
        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();
        $vObjBancoHomologacao = new EXTDAO_Banco();

        if ($vIdBancoHomologacao != null) {
            $vObjBancoHomologacao->select($vIdBancoHomologacao);
            $vObjDatabaseHomologacao = $vObjBancoHomologacao->getObjDatabase();
            //vetor de tabela da base web
            $vVetorTabelaHomologacao = $vObjBancoHomologacao->showTables();
            
            if (in_array("sistema_tabela", $vVetorTabelaHomologacao)) {

                $vObjDatabaseHomologacao->query("SELECT nome, transmissao_web_para_mobile_BOOLEAN, transmissao_mobile_para_web_BOOLEAN , frequencia_sincronizador_INT 
                                                    FROM sistema_tabela");
                $vVetorObjSistemaTabela = Helper::getResultSetToMatriz($vObjDatabaseHomologacao->result);
                $vObjDatabaseHomologacao->query("SELECT nome
                                                    FROM sistema_tabela");
                $vVetorNomeSistemaTabela = Helper::getResultSetToArrayDeUmCampo($vObjDatabaseHomologacao->result, 1, 1, 0);
                $objDatabase = new Database();
                $objDatabase->query("SELECT t.nome, t.transmissao_web_para_mobile_BOOLEAN, t.transmissao_mobile_para_web_BOOLEAN , t.frequencia_sincronizador_INT 
                            FROM tabela t JOIN tabela_tabela tt ON tt.homologacao_tabela_id_INT = t.id
                            WHERE tt.projetos_versao_id_INT = " . $idPVPrototipo." 
                                AND t.banco_id_INT = $vIdBancoHomologacao");
                $vVetorObjTabela = Helper::getResultSetToMatriz($objDatabase->result);
                //se transmissao_web_para_mobile_BOOLEAN == 1
                
                
                $vVetorSistemaTabelaEncontrado = array();
                for ($i = 0; $i < count($vVetorObjTabela); $i++) {
                    //se transmissao_web_para_mobile_BOOLEAN == 1
                    
                    $banco_mobile_BOOLEAN = "0";
                    if($vVetorObjTabela[$i][1] != "0"){
                        $banco_mobile_BOOLEAN = "1";
                    }
                    if($vVetorObjTabela[$i][2] != "0"){
                        $banco_mobile_BOOLEAN = "1";
                    }
                    $banco_web_BOOLEAN = "1";
                    //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
                    //da 'tabela' deve ser atualizada
                    $vIndexSistemaTabela = array_search($vVetorObjTabela[$i][0], $vVetorNomeSistemaTabela);
                    $vVetorSistemaTabelaEncontrado[$vVetorObjTabela[$i][0]] = true;
                    for($j = 1 ; $j <= 3; $j ++){
                        $vVetorObjSistemaTabela[$vIndexSistemaTabela][$j] = strlen($vVetorObjSistemaTabela[$vIndexSistemaTabela][$j]) ? $vVetorObjSistemaTabela[$vIndexSistemaTabela][$j] : "0";
                        
                    }
                    
                    
                    
                    
                    
                    if (is_numeric($vIndexSistemaTabela)) {
                        
                        $vObjDatabaseHomologacao->query("UPDATE sistema_tabela
                                                    SET transmissao_web_para_mobile_BOOLEAN = {$vVetorObjTabela[$i][1]}, 
                                                        transmissao_mobile_para_web_BOOLEAN = {$vVetorObjTabela[$i][2]}, 
                                                            frequencia_sincronizador_INT = {$vVetorObjTabela[$i][3]},
                                                                banco_mobile_BOOLEAN = $banco_mobile_BOOLEAN,
                                                                banco_web_BOOLEAN = $banco_web_BOOLEAN,    
                                                                excluida_BOOLEAN = '0'
                                                    WHERE nome = '{$vVetorObjTabela[$i][0]}'");
                    } else {
                        $vObjDatabaseHomologacao->query("INSERT sistema_tabela
                                                    (nome, 
                                                     transmissao_web_para_mobile_BOOLEAN,
                                                     transmissao_mobile_para_web_BOOLEAN ,
                                                     frequencia_sincronizador_INT,
                                                      banco_mobile_BOOLEAN,
                                                      banco_web_BOOLEAN,
                                                      excluida_BOOLEAN)
                                                     VALUES(
                                                        '{$vVetorObjTabela[$i][0]}',
                                                        {$vVetorObjTabela[$i][1]}, 
                                                        {$vVetorObjTabela[$i][2]}, 
                                                        {$vVetorObjTabela[$i][3]},
                                                         $banco_mobile_BOOLEAN,
                                                         $banco_web_BOOLEAN,
                                                         0
                                                     )");
                    }
                }
                for($i = 0 ; $i < count($vVetorNomeSistemaTabela); $i++){
                    //se a tabela contida no sistema_tabela 
                    //nao foi encontrada na atual estrutura da base web
                    if($vVetorSistemaTabelaEncontrado[$vVetorNomeSistemaTabela[$i]]!= true ){
                         $vObjDatabaseHomologacao->query("UPDATE sistema_tabela
                                                    SET excluida_BOOLEAN = 1
                                                    WHERE nome = '{$vVetorNomeSistemaTabela[$i]}'");
                    }
                }
            }
        }
    }
    public function populaTabelaTabelaDoProjetoVersao($idPV, Database $db = null) {
        if($db == null)
            $db = new Database();
        
        $idPVBB = EXTDAO_Projetos_versao_banco_banco::getIdPVBB($idPV, $this->getId(), $db);
        
//        $trunk = new Trunk();
        $trunk = Trunk::factory();
//        $pvBancos = $trunk->getProjetosVersaoDoBancoDeDados($idPV, $db);
        
//        $idPVTabelaProducao = $pvBancos['idPVTabelaProducao'];
//        $idPVTabelaHomologacao = $pvBancos['idPVTabelaHomologacao'];
        $idPVTabelaHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($idPV);
        $idPVTabelaProducao = $trunk->getPVDoBancoDeDadosDeProducao($idPV);
        
        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();

        $vObjBancoHomologacao = new EXTDAO_Banco($db);
        $vObjBancoHomologacao->select($vIdBancoHomologacao);

        
        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco($db);
        $vObjBancoProducao->select($vIdBancoProducao);
        
        $vVetorTabelaProducao = EXTDAO_Tabela::getVetorTabela($idPVTabelaProducao,$vIdBancoProducao,   $db);
        $vVetorTabelaHomologacao = EXTDAO_Tabela::getVetorTabela($idPVTabelaHomologacao,$vIdBancoHomologacao, $db);
        
        if($vVetorTabelaProducao != null)
        //Para toda tabela existente no banco de producao e homologacao
        foreach ($vVetorTabelaProducao as $tabelaProducao) {
            $tabelaProducaoSemPrefixo = $tabelaProducao;
            if(strlen($this->prefixoNomeTabelaProducao) ){
                
                $tabelaProducaoSemPrefixo = substr ($tabelaProducao, strlen($this->prefixoNomeTabelaProducao) );
                
            }
            if (in_array($tabelaProducaoSemPrefixo, $vVetorTabelaHomologacao)) {

                $vIdTabelaProducao = EXTDAO_Tabela::existeTabela(
                    $tabelaProducao, 
                    $vIdBancoProducao, 
                    $idPVTabelaProducao, 
                    $db);

                $vIdTabelaHomologacao = EXTDAO_Tabela::existeTabela(
                    $tabelaProducaoSemPrefixo, 
                    $vIdBancoHomologacao, 
                    $idPVTabelaHomologacao, 
                    $db);

                if ($vIdTabelaProducao != null && $vIdTabelaHomologacao != null) {
                    if (EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd(
                        $idPVBB, 
                        $vIdTabelaHomologacao, 
                        $vIdTabelaProducao, 
                        $db) != null)
                        continue;

                    $objTabelaTabela = new EXTDAO_Tabela_tabela($db);

                    if (!EXTDAO_Tabela_tabela::existeRelacaoTabelaHomologacao(
                        $idPVBB, $vIdTabelaHomologacao, $db)) {

                        $objTabelaTabela->setHomologacao_tabela_id_INT($vIdTabelaHomologacao);
                        $objTabelaTabela->setProducao_tabela_id_INT($vIdTabelaProducao);
                        $objTabelaTabela->setProjetos_versao_id_INT($idPV);
                        $objTabelaTabela->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTabelaTabela->setStatus_verificacao_BOOLEAN("0");
                        $objTabelaTabela->formatarParaSQL();
                        $objTabelaTabela->insert();
                    }
                }
            }
        }

         if($vVetorTabelaHomologacao != null)
        //Para toda tabela existente somente no banco de homologacao
        foreach ($vVetorTabelaHomologacao as $tabelaHomologacao) {


            if (!in_array($this->prefixoNomeTabelaProducao.$tabelaHomologacao, $vVetorTabelaProducao)) {

                $vIdTabelaHomologacao = EXTDAO_Tabela::existeTabela(
                    $this->prefixoNomeTabelaHomologacao.$tabelaHomologacao, 
                    $vIdBancoHomologacao, 
                    $idPVTabelaHomologacao,
                    $db);

                if ($vIdTabelaHomologacao != null) {
                    if (EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $vIdTabelaHomologacao, $vIdTabelaProducao) != null)
                        continue;

                    $objTabelaTabela = new EXTDAO_Tabela_tabela();

                    if (!EXTDAO_Tabela_tabela::existeRelacaoTabelaHomologacao($idPVBB, $vIdTabelaHomologacao)) {
                        $objTabelaTabela = new EXTDAO_Tabela_tabela();
                        $objTabelaTabela->setHomologacao_tabela_id_INT($vIdTabelaHomologacao);
                        $objTabelaTabela->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTabelaTabela->setProjetos_versao_id_INT($idPV);
                        $objTabelaTabela->setStatus_verificacao_BOOLEAN("0");
                        $objTabelaTabela->formatarParaSQL();
                        $objTabelaTabela->insert();
                    }
                }
            }
        }
        if($vVetorTabelaProducao != null)
        //Para toda tabela existente somente no banco de producao
        foreach ($vVetorTabelaProducao as $tabelaProducao) {
            $tabelaProducaoSemPrefixo = $tabelaProducao;
            if(strlen($this->prefixoNomeTabelaProducao) )
                $tabelaProducaoSemPrefixo = substr ($tabelaProducao, strlen($this->prefixoNomeTabelaProducao) );
            if (!in_array($tabelaProducaoSemPrefixo, $vVetorTabelaHomologacao)) {
                $vIdTabelaProducao = EXTDAO_Tabela::existeTabela(
                    $this->prefixoNomeTabelaProducao.$tabelaProducao, 
                    $vIdBancoProducao, 
                    $idPVTabelaProducao);

                if ($vIdTabelaProducao != null) {
                    if (EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $vIdTabelaHomologacao, $vIdTabelaProducao) != null)
                        continue;
                    
                    if (!EXTDAO_Tabela_tabela::existeRelacaoTabelaProducao($idPVBB, $vIdTabelaProducao)) {
                        $objTabelaTabela = new EXTDAO_Tabela_tabela();
                        $objTabelaTabela->setProducao_tabela_id_INT($vIdTabelaProducao);
                        $objTabelaTabela->setProjetos_versao_id_INT($idPV);
                        $objTabelaTabela->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTabelaTabela->setStatus_verificacao_BOOLEAN("0");
                        $objTabelaTabela->formatarParaSQL();
                        $objTabelaTabela->insert();
                    }
                }
            }
        }
    }

    public function bkpPopulaTabelaTabelaDoProjetoVersao($idPV, $idPVBB) {
        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();

        $vObjBancoHomologacao = new EXTDAO_Banco();
        $vObjBancoHomologacao->select($vIdBancoHomologacao);

        $idPVBB = EXTDAO_Projetos_versao_banco_banco::getIdPVBB($idPV, $this->getId());
        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);

        $vVetorTabelaProducao = $vObjBancoProducao->showTables();
        $vVetorTabelaHomologacao = $vObjBancoHomologacao->showTables();
        if($vVetorTabelaProducao != null){
        //Para toda tabela existente no banco de producao e homologacao
        foreach ($vVetorTabelaProducao as $tabelaProducao) {
            $tabelaProducaoSemPrefixo = $tabelaProducao;
            if(strlen($this->prefixoNomeTabelaProducao) ){
                
                $tabelaProducaoSemPrefixo = substr ($tabelaProducao, strlen($this->prefixoNomeTabelaProducao) );
                
            }
            if (in_array($tabelaProducaoSemPrefixo, $vVetorTabelaHomologacao)) {

                $vIdTabelaProducao = EXTDAO_Tabela::existeTabela($tabelaProducao, $vIdBancoProducao, $idPV);

                $vIdTabelaHomologacao = EXTDAO_Tabela::existeTabela($tabelaProducaoSemPrefixo, $vIdBancoHomologacao, $idPV);

                if ($vIdTabelaProducao != null && $vIdTabelaHomologacao != null) {
                    if (EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $vIdTabelaHomologacao, $vIdTabelaProducao) != null)
                        continue;

                    $objTabelaTabela = new EXTDAO_Tabela_tabela();

                    if (!EXTDAO_Tabela_tabela::existeRelacaoTabelaHomologacao($idPVBB, $vIdTabelaHomologacao)) {

                        $objTabelaTabela->setHomologacao_tabela_id_INT($vIdTabelaHomologacao);
                        $objTabelaTabela->setProducao_tabela_id_INT($vIdTabelaProducao);
                        $objTabelaTabela->setProjetos_versao_id_INT($idPV);
                        $objTabelaTabela->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTabelaTabela->setStatus_verificacao_BOOLEAN("0");
                        $objTabelaTabela->formatarParaSQL();
                        $objTabelaTabela->insert();
                    }
                }
            }
        }
        }
if($vVetorTabelaHomologacao != null){
        //Para toda tabela existente somente no banco de homologacao
        foreach ($vVetorTabelaHomologacao as $tabelaHomologacao) {


            if (!in_array($this->prefixoNomeTabelaProducao.$tabelaHomologacao, $vVetorTabelaProducao)) {

                $vIdTabelaHomologacao = EXTDAO_Tabela::existeTabela($this->prefixoNomeTabelaHomologacao.$tabelaHomologacao, $vIdBancoHomologacao, $idPV);

                if ($vIdTabelaHomologacao != null) {
                    if (EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $vIdTabelaHomologacao, $vIdTabelaProducao) != null)
                        continue;

                    $objTabelaTabela = new EXTDAO_Tabela_tabela();

                    if (!EXTDAO_Tabela_tabela::existeRelacaoTabelaHomologacao($idPVBB, $vIdTabelaHomologacao)) {
                        $objTabelaTabela = new EXTDAO_Tabela_tabela();
                        $objTabelaTabela->setHomologacao_tabela_id_INT($vIdTabelaHomologacao);
                        $objTabelaTabela->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTabelaTabela->setProjetos_versao_id_INT($idPV);
                        $objTabelaTabela->setStatus_verificacao_BOOLEAN("0");
                        $objTabelaTabela->formatarParaSQL();
                        $objTabelaTabela->insert();
                    }
                }
            }
        }
}
if($vVetorTabelaProducao != null){
        //Para toda tabela existente somente no banco de producao
        foreach ($vVetorTabelaProducao as $tabelaProducao) {
            $tabelaProducaoSemPrefixo = $tabelaProducao;
            if(strlen($this->prefixoNomeTabelaProducao) )
                $tabelaProducaoSemPrefixo = substr ($tabelaProducao, strlen($this->prefixoNomeTabelaProducao) );
            if (!in_array($tabelaProducaoSemPrefixo, $vVetorTabelaHomologacao)) {
                $vIdTabelaProducao = EXTDAO_Tabela::existeTabela($this->prefixoNomeTabelaProducao.$tabelaProducao, $vIdBancoProducao, $idPV);

                if ($vIdTabelaProducao != null) {
                    if (EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $vIdTabelaHomologacao, $vIdTabelaProducao) != null)
                        continue;
                    
                    if (!EXTDAO_Tabela_tabela::existeRelacaoTabelaProducao($idPVBB, $vIdTabelaProducao)) {
                        $objTabelaTabela = new EXTDAO_Tabela_tabela();
                        $objTabelaTabela->setProducao_tabela_id_INT($vIdTabelaProducao);
                        $objTabelaTabela->setProjetos_versao_id_INT($idPV);
                        $objTabelaTabela->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTabelaTabela->setStatus_verificacao_BOOLEAN("0");
                        $objTabelaTabela->formatarParaSQL();
                        $objTabelaTabela->insert();
                    }
                }
            }
        }
}
        
                    }

    public function populaAtributoAtributoDoProjetoVersao(
        $idPVBB, $vObjTabelaTabela, $idPVHomologacao, $idPVProducao,$db = null) {
        if($db == null)
            $db = new Database();
        
        $pIdProjetoVersao = $vObjTabelaTabela->getProjetos_versao_id_INT();
        $idPVBB = $vObjTabelaTabela->getProjetos_versao_banco_banco_id_INT();

        $vIdAtributoHomologacao = $vObjTabelaTabela->getHomologacao_tabela_id_INT();
        $vIdAtributoProducao = $vObjTabelaTabela->getProducao_tabela_id_INT();

        $vObjTabelaHomologacao = $vObjTabelaTabela->getObjTabelaHomologacao();
        $vObjTabelaProducao = $vObjTabelaTabela->getObjTabelaProducao();

        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();
        $vObjBancoHomologacao = new EXTDAO_Banco();
        $vObjBancoHomologacao->select($vIdBancoHomologacao);
        $databaseHomologacao = $vObjBancoHomologacao->getObjDatabase();
        
        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);
        $databaseProducao = $vObjBancoProducao->getObjDatabase();
            

        $vVetorAtributoProducao = null;
        if ($vObjTabelaProducao != null) {
            //$vObjBancoProducao->getVetorObjAtributoDaTabela($vObjTabelaProducao->getNome(), $databaseProducao);
            //$vVetorAtributoProducao = $vObjBancoProducao->showColumnsDaTabela($vObjTabelaProducao->getNome(), $databaseProducao);
            $vVetorAtributoProducao = EXTDAO_Atributo::getAtributosDaTabela(
                $vObjTabelaProducao->getId(), $db);
        }

        $vVetorAtributoHomologacao = null;
        if ($vObjTabelaHomologacao != null) {
            //$vObjBancoHomologacao->getVetorObjAtributoDaTabela($vObjTabelaHomologacao->getNome(), $databaseHomologacao);
            //$vVetorAtributoHomologacao = $vObjBancoHomologacao->showColumnsDaTabela($vObjTabelaHomologacao->getNome(), $databaseHomologacao);
            $vVetorAtributoHomologacao = EXTDAO_Atributo::getAtributosDaTabela(
                $vObjTabelaHomologacao->getId(), $db);
        }


        $objAtributoAtributo = new EXTDAO_Atributo_atributo();
        $objAtributo = new EXTDAO_Atributo();

        if ($vObjTabelaProducao != null 
            && $vObjTabelaHomologacao != null
            && $vVetorAtributoProducao != null){
        //Para toda tabela existente somente no banco de producao e homologacao
            foreach ($vVetorAtributoProducao as $atributoProducao) {

                if (in_array($atributoProducao, $vVetorAtributoHomologacao)) {

                    $vIdAtributoProducao = $objAtributo->existeAtributo(
                        $atributoProducao, $vObjTabelaProducao->getId(), $idPVProducao);
                    $vIdAtributoHomologacao = $objAtributo->existeAtributo(
                        $atributoProducao, $vObjTabelaHomologacao->getId(), $idPVHomologacao);

                    if ($vIdAtributoProducao != null && $vIdAtributoHomologacao != null) {
                        //if (EXTDAO_Atributo_atributo::getObjAtributoAtributo($idPVBB, $vIdAtributoHomologacao, $vIdAtributoProducao, $db) != null)
                        if(strlen(EXTDAO_Atributo_atributo::getIdAtributoAtributo(
                            $idPVBB, $vIdAtributoHomologacao, $vIdAtributoProducao, $db))){
                            
                            
                            continue;
                        }

                        if (!EXTDAO_Atributo_atributo::existeRelacaoAtributoHomologacao($idPVBB, $vIdAtributoHomologacao, $db)) {
                            $objAtributoAtributo->setHomologacao_atributo_id_INT($vIdAtributoHomologacao);
                            $objAtributoAtributo->setProducao_atributo_id_INT($vIdAtributoProducao);
                            $objAtributoAtributo->setProjetos_versao_banco_banco_id_INT($idPVBB);
                            $objAtributoAtributo->setProjetos_versao_id_INT($pIdProjetoVersao);
                            $objAtributoAtributo->setStatus_verificacao_BOOLEAN("0");
                            $objAtributoAtributo->setTabela_tabela_id_INT($vObjTabelaTabela->getId());
                            $objAtributoAtributo->formatarParaSQL();
                            $objAtributoAtributo->insert();
                        }
                    }
                }
            }
        }
        if ($vObjTabelaHomologacao != null && $vVetorAtributoHomologacao != null){
            $objAtributoAtributo = new EXTDAO_Atributo_atributo();
        //Para toda atributo existente somente no banco de homologacao
            foreach ($vVetorAtributoHomologacao as $atributoHomologacao) {
                if (!in_array($atributoHomologacao, $vVetorAtributoProducao)) {

                    $vIdAtributoHomologacao = $objAtributo->existeAtributo(
                        $atributoHomologacao, $vObjTabelaHomologacao->getId(), $idPVHomologacao);

                    if ($vIdAtributoHomologacao != null) {
                        //if (EXTDAO_Atributo_atributo::getObjAtributoAtributo($idPVBB, $vIdAtributoHomologacao, $vIdAtributoProducao, $db) != null)
                        if(strlen(EXTDAO_Atributo_atributo::getIdAtributoAtributoHmg(
                            $idPVBB, $vIdAtributoHomologacao, $db)))
                            continue;
                        
                        if (!EXTDAO_Atributo_atributo::existeRelacaoAtributoHomologacao(
                            $idPVBB, $vIdAtributoHomologacao, $db)) {
                            
                            $objAtributoAtributo->setHomologacao_atributo_id_INT($vIdAtributoHomologacao);
                            $objAtributoAtributo->setProjetos_versao_id_INT($pIdProjetoVersao);
                            $objAtributoAtributo->setProjetos_versao_banco_banco_id_INT($idPVBB);
                            $objAtributoAtributo->setStatus_verificacao_BOOLEAN("0");
                            $objAtributoAtributo->setTabela_tabela_id_INT($vObjTabelaTabela->getId());
                            $objAtributoAtributo->formatarParaSQL();
                            $objAtributoAtributo->insert();
                        }
                    }
                }
            }
            
        }
        if ($vObjTabelaProducao != null && $vVetorAtributoProducao != null){
            $objAtributoAtributo = new EXTDAO_Atributo_atributo();
        //Para toda atributo existente somente no banco de producao
            foreach ($vVetorAtributoProducao as $atributoProducao) {
                if (!in_array($atributoProducao, $vVetorAtributoHomologacao)) {

                    $vIdAtributoProducao = $objAtributo->existeAtributo(
                        $atributoProducao, $vObjTabelaProducao->getId(), $idPVProducao);

                    if ($vIdAtributoProducao != null) {
                        //if (EXTDAO_Atributo_atributo::getObjAtributoAtributo($idPVBB, $vIdAtributoHomologacao, $vIdAtributoProducao, $db) != null)
                        
                        if(strlen(EXTDAO_Atributo_atributo::getIdAtributoAtributoProd(
                            $idPVBB, $vIdAtributoProducao, $db)))
                            return;
                        
                        if (!EXTDAO_Atributo_atributo::existeRelacaoAtributoProducao(
                            $idPVBB, $vIdAtributoProducao, $db)) {
                            
                            $objAtributoAtributo->setProducao_atributo_id_INT($vIdAtributoProducao);
                            $objAtributoAtributo->setProjetos_versao_id_INT($pIdProjetoVersao);
                            $objAtributoAtributo->setProjetos_versao_banco_banco_id_INT($idPVBB);
                            $objAtributoAtributo->setStatus_verificacao_BOOLEAN("0");
                            $objAtributoAtributo->setTabela_tabela_id_INT($vObjTabelaTabela->getId());
                            $objAtributoAtributo->formatarParaSQL();
                            $objAtributoAtributo->insert();
                        }
                    }
                }
            }
        }
    }

    public function getIdBancoBanco($idPVBB) {

        $this->database->query("SELECT bb.id
                FROM banco_banco bb JOIN projetos_versao_banco_banco pvbb 
                    ON bb.id = pvbb.banco_banco_id_INT
                WHERE pvbb.id={$idPVBB}
                LIMIT 0,1");
        return Helper::getResultSetToPrimeiroResultado($this->database->result);
    }

    public function getIdBancoProducao($idPVBB) {


        $this->database->query("SELECT bb.producao_banco_id_INT 
                FROM projetos_versao_banco_banco pvbb JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id={$idPVBB} 
                LIMIT 0,1");
        return Helper::getResultSetToPrimeiroResultado($this->database->result);
    }

    public function getIdBancoHomologacao($idPVBB) {

        $this->database->query("SELECT bb.homologacao_banco_id_INT 
                FROM projetos_versao_banco_banco pvbb JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id={$idPVBB} 
                LIMIT 0,1");
        return Helper::getResultSetToPrimeiroResultado($this->database->result);
    }

    //Chamada apos o cadastro do projeto versao, nesse instante o banco 
    //de producao ja deve estar criado com as tabelas que compoe sua estrutura
    public function inicializaEstruturaDeTabelaDoProjetoVersao($pIdProjetoVersao, $idPVBB, 
        Database $db = null) {
        //TODO responsavel por popular as tabela 'tabela' relativo ao banco de homologacao do respectivo tipo e projeto
        //nesse caso Ã© realizada a copia da estrutura do banco de produÃ§Ã£o para de homologaÃ§Ã£o
        $this->populaTabelaDoProjetoVersaoDoBancoDeProducao($pIdProjetoVersao, $db);
        //TODO responsavel por popular as tabela 'tabela' relativo ao banco de homologacao do respectivo tipo e projeto
        //nesse caso Ã© realizada a copia da estrutura do banco de produÃ§Ã£o para de homologaÃ§Ã£o
        $this->populaTabelaDoProjetoVersaoDoBancoDeHomologacao($pIdProjetoVersao, $db);
        $this->populaTabelaTabelaDoProjetoVersao($pIdProjetoVersao, $db);
        //Retira as tabelas inexistentes no banco e que estao cadastradas 
        //na tabela 'Tabela' da BibliotecaNuvem
        //=> toda tupla tabela_tabela tb eh retirada
        $this->retiraTabelasInexistentes($pIdProjetoVersao, $db);
    }


    public function retiraTabelasInexistentes(
        $idPV,
        $homologacao =  true, 
        $producao = true, 
        Database $db = null) {
        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();

        $vObjBancoHomologacao = new EXTDAO_Banco();
        $vObjBancoHomologacao->select($vIdBancoHomologacao);


        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);

        $vVetorTabelaProducao = $vObjBancoProducao->showTables();
        $vVetorTabelaHomologacao = $vObjBancoHomologacao->showTables();

        if($homologacao){
            $vVetorTabelaHomologacaoNaBibliotecaNuvem = EXTDAO_Tabela::getVetorTabelaDoBanco(
                $idPV, 
                $this->getHomologacao_banco_id_INT(), 
                $db);

            $objTabela = new EXTDAO_Tabela($db);
            //se o banco estiver vazio
            if (empty($vVetorTabelaHomologacao) ) {
                if($vVetorTabelaHomologacaoNaBibliotecaNuvem != null)
                    foreach ($vVetorTabelaHomologacaoNaBibliotecaNuvem as $vTabelaBibliotecaNuvem)
                        $objTabela->delete($vTabelaBibliotecaNuvem["id"]);
            } else {
                if($vVetorTabelaHomologacaoNaBibliotecaNuvem != null ){
                    foreach ($vVetorTabelaHomologacaoNaBibliotecaNuvem as $vTabelaBibliotecaNuvem) {
                        $vObjTabelaHomologacao = null;
                        if($vVetorTabelaHomologacao != null ){
                            foreach ($vVetorTabelaHomologacao as $vNomeTabelaHomologacao) {
                                if ($vNomeTabelaHomologacao == $vTabelaBibliotecaNuvem["nome"]) {
                                    $vObjTabelaHomologacao = $vTabelaBibliotecaNuvem;
                                    break;
                                }
                            }
                        }
                        if ($vObjTabelaHomologacao == null)
                            $objTabela->delete($vTabelaBibliotecaNuvem["id"]);
                    }
                }
            }
        }
        
        if($producao){
            $vVetorTabelaProducaoNaBibliotecaNuvem = EXTDAO_Tabela::getVetorTabelaDoBanco(
                $idPV, 
                $this->getProducao_banco_id_INT(),
                $db);

            //se o banco estiver vazio
            if (empty($vVetorTabelaProducao) ) {
                if($vVetorTabelaProducaoNaBibliotecaNuvem != null){
                    foreach ($vVetorTabelaProducaoNaBibliotecaNuvem as $vTabelaBibliotecaNuvem)
                    $objTabela->delete($vTabelaBibliotecaNuvem["id"]);
                }
            } else {
                if($vVetorTabelaProducaoNaBibliotecaNuvem != null){
                    foreach ($vVetorTabelaProducaoNaBibliotecaNuvem as $vTabelaBibliotecaNuvem) {
                        $vObjTabelaProducao = null;
                        if($vVetorTabelaProducao != null){
                            foreach ($vVetorTabelaProducao as $vNomeTabelaProducao) {
                                if ($vNomeTabelaProducao == $vTabelaBibliotecaNuvem["nome"]) {
                                    $vObjTabelaProducao = $vTabelaBibliotecaNuvem;
                                    break;
                                }
                            }
                        }
                        
                        if ($vObjTabelaProducao == null)
                            $objTabela->delete($vTabelaBibliotecaNuvem["id"]);
                    }
                }
            }
        }
        
    }
    public function retiraAtributosInexistentesDaTabela($idPVBB, $objTabela, $db = null) {
        if($db == null)
            $db = new Database();
        
        $vNomeTabela = $objTabela->getNome();
        

        $vObjBanco = new EXTDAO_Banco();
        $vObjBanco->select($objTabela->getBanco_id_INT());

        $vVetorAtributo = $vObjBanco->showColumnsDaTabela($vNomeTabela);
        $objAtributo = new EXTDAO_Atributo();
        $vVetorAtributoNaBibliotecaNuvem = EXTDAO_Atributo::getIdNomeAtributosDaTabela($objTabela->getId());

        if (empty($vVetorAtributo)) {
            if($vVetorAtributoNaBibliotecaNuvem != null){
                foreach ($vVetorAtributoNaBibliotecaNuvem as $vAtributoBibliotecaNuvem)
                    $objAtributo->delete($vAtributoBibliotecaNuvem[0]);
            }
        } else {
            if($vVetorAtributoNaBibliotecaNuvem != null){
                foreach ($vVetorAtributoNaBibliotecaNuvem as $vAtributoBibliotecaNuvem) {
                    $vObjAtributoHomologacao = null;
                    if($vVetorAtributo != null){
                        foreach ($vVetorAtributo as $vNomeAtributoHomologacao) {
                            if ($vNomeAtributoHomologacao == $vAtributoBibliotecaNuvem[1]) {
                                $vObjAtributoHomologacao = $vAtributoBibliotecaNuvem;
                                break;
                            }
                        }
                    }
                    
                    if ($vObjAtributoHomologacao == null)
                        $objAtributo->delete($vObjAtributoHomologacao[0]);
                }
            }
            
        }
    }
    public function retiraAtributosInexistentes($idPVBB, $objTabelaTabela, $db = null) {
        if($db == null)
            $db = new Database();
        $vIdBancoHomologacao = $this->getHomologacao_banco_id_INT();

        $vObjBancoHomologacao = new EXTDAO_Banco();
        $vObjBancoHomologacao->select($vIdBancoHomologacao);
        

        $vIdBancoProducao = $this->getProducao_banco_id_INT();
        $vObjBancoProducao = new EXTDAO_Banco();
        $vObjBancoProducao->select($vIdBancoProducao);

//        $vObjTabelaTabela = new EXTDAO_Tabela_tabela();
//        $vObjTabelaTabela->select($idTT);
        $vIdTabelaProducao = $objTabelaTabela->getProducao_tabela_id_INT();
        $vIdTabelaHomologacao = $objTabelaTabela->getHomologacao_tabela_id_INT();

        $vVetorAtributoProducao = array();
        $vObjTabelaProducao = new EXTDAO_Tabela();
        if (strlen($vIdTabelaProducao)) {
            $vObjTabelaProducao->select($vIdTabelaProducao);
            $vNomeTabela = $vObjTabelaProducao->getNome();
            $vVetorAtributoProducao = $vObjBancoProducao->showColumnsDaTabela($vNomeTabela);
        }

        $vVetorAtributoHomologacao = array();
        $vObjTabelaHomologacao = new EXTDAO_Tabela();
        if (strlen($vIdTabelaHomologacao)) {
            $vObjTabelaHomologacao->select($vIdTabelaHomologacao);
            $vNomeTabela = $vObjTabelaHomologacao->getNome();
            $vVetorAtributoHomologacao = $vObjBancoHomologacao->showColumnsDaTabela($vNomeTabela);
        }

        $objAtributo = new EXTDAO_Atributo();
        $vVetorAtributoHomologacaoNaBibliotecaNuvem = EXTDAO_Atributo_atributo::getVetorAtributoHomologacao(
            $idPVBB, $objTabelaTabela->getId(), $db);

        if (empty($vVetorAtributoHomologacao) ) {
            if($vVetorAtributoHomologacaoNaBibliotecaNuvem != null){
                foreach ($vVetorAtributoHomologacaoNaBibliotecaNuvem as $vAtributoBibliotecaNuvem)
                    $objAtributo->delete($vAtributoBibliotecaNuvem["id"]);
            }
            
        } else {
            if($vVetorAtributoHomologacaoNaBibliotecaNuvem != null){
                foreach ($vVetorAtributoHomologacaoNaBibliotecaNuvem as $vAtributoBibliotecaNuvem) {
                    $vObjAtributoHomologacao = null;
                    if($vVetorAtributoHomologacao != null){
                        foreach ($vVetorAtributoHomologacao as $vNomeAtributoHomologacao) {
                            if ($vNomeAtributoHomologacao == $vAtributoBibliotecaNuvem["nome"]) {
                                $vObjAtributoHomologacao = $vAtributoBibliotecaNuvem;
                                break;
                            }
                        }
                    }
                    
                    if ($vObjAtributoHomologacao == null)
                        $objAtributo->delete($vObjAtributoHomologacao["id"]);
                }
            }
            
        }
        $vVetorAtributoProducaoNaBibliotecaNuvem = EXTDAO_Atributo_atributo::getVetorAtributoProducao(
            $idPVBB, $objTabelaTabela->getId(), $db);
        //se o banco estiver vazio
        if (empty($vVetorAtributoProducao) ) {
            if($vVetorAtributoProducaoNaBibliotecaNuvem != null){
                foreach ($vVetorAtributoProducaoNaBibliotecaNuvem as $vAtributoBibliotecaNuvem)
                $objAtributo->delete($vAtributoBibliotecaNuvem["id"]);
            }
            
        } else {
            if($vVetorAtributoProducaoNaBibliotecaNuvem != null){
                
                foreach ($vVetorAtributoProducaoNaBibliotecaNuvem as $vAtributoBibliotecaNuvem) {
                    $vObjAtributoProducao = null;
                    if($vVetorAtributoProducao != null){
                        foreach ($vVetorAtributoProducao as $vNomeAtributoProducao) {
                            if ($vNomeAtributoProducao == $vAtributoBibliotecaNuvem["nome"]) {
                                $vObjAtributoProducao = $vAtributoBibliotecaNuvem;
                                break;
                            }
                        }
                    }
                    
                    if ($vObjAtributoProducao == null)
                        $objAtributo->delete($vObjAtributoProducao["id"]);
                }
            }
            
        }
    }

    public function inicializaEstruturaDeAtributoDaTabela($idPVBB, $objTabela, $homologacao = true, $db = null) {
        if($db = null)
            $db = new Database();

        
//        if($idTT == "1807"){
//            $i = 0;
//            $i += 1;
//        }
        $idPV = $objTabela->getProjetos_versao_id_INT();

        $trunk = Trunk::factory();
        if($homologacao)
            $idPVAtributoHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($idPV);
        else 
            $idPVAtributoHomologacao = $trunk->getPVDoBancoDeDadosDeProducao($idPV);
        
        if($homologacao){
            $this->populaAtributoDoProjetoVersaoDoBanco(
                $this->getHomologacao_banco_id_INT(), 
                $objTabela->getId(), 
                $idPVAtributoHomologacao, 
                $db);
        } else {
            $this->populaAtributoDoProjetoVersaoDoBanco(
                $this->getProducao_banco_id_INT(), 
                $objTabela->getId(), 
                $idPVAtributoHomologacao, 
                $db);
        }
        
        $this->retiraAtributosInexistentesDaTabela($idPVBB,  $objTabela, $db ) ;
        
    }

    public function inicializaEstruturaDeAtributo($idPVBB, $objTabelaTabela, $db = null) {
        if($db = null)
            $db = new Database();

        
//        if($idTT == "1807"){
//            $i = 0;
//            $i += 1;
//        }
        $idPV = $objTabelaTabela->getProjetos_versao_id_INT();

        $trunk = Trunk::factory();

        $idPVAtributoHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($idPV);
        $idPVAtributoProducao = $trunk->getPVDoBancoDeDadosDeProducao($idPV);
        //$pvBancos = $trunk->getProjetosVersaoDoBancoDeDados($idPV, $db);        
//        $idPVAtributoProducao = $pvBancos['idPVTabelaProducao'];
//        $idPVAtributoHomologacao = $pvBancos['idPVTabelaHomologacao'];
        
        if ($objTabelaTabela->getHomologacao_tabela_id_INT() != null)
            $this->populaAtributoDoProjetoVersaoDoBanco(
                    $this->getHomologacao_banco_id_INT(), 
                    $objTabelaTabela->getHomologacao_tabela_id_INT(), 
                    $idPVAtributoHomologacao, 
                    $db);

        if ($objTabelaTabela->getProducao_tabela_id_INT() != null)
            $this->populaAtributoDoProjetoVersaoDoBanco(
                    $this->getProducao_banco_id_INT(), 
                    $objTabelaTabela->getProducao_tabela_id_INT(), 
                    $idPVAtributoProducao, 
                    $db);


        $this->populaAtributoAtributoDoProjetoVersao(
            $idPVBB, $objTabelaTabela, $idPVAtributoHomologacao, $idPVAtributoProducao, $db);

        $this->retiraAtributosInexistentes($idPVBB, $objTabelaTabela, $db);
    }

    public function getVetorTabela($idPV) {
        $db = new Database();
        $db->query("SELECT DISTINCT t.id
                FROM tabela t JOIN projetos_versao_banco_banco pvbb ON pvbb.projetos_versao_id_INT = t.projetos_versao_id_INT                
                WHERE pvbb.banco_banco_id_INT = {$this->getId()}  AND t.projetos_versao_id_INT = " . $idPV);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    public function getVetorAtributo() {
        $db = new Database();
        $db->query("SELECT DISTINCT a.id
                FROM atributo a JOIN projetos_versao_banco_banco pvbb ON pvbb.projetos_versao_id_INT = a.projetos_versao_id_INT
                WHERE pvbb.banco_banco_id_INT = {$this->getId()}");
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    public function isMobile(){
        $tb = $this->getTipo_banco_id_INT();
        switch ($tb) {
            case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
            case EXTDAO_Tipo_banco::$TIPO_BANCO_IPHONE:
            case EXTDAO_Tipo_banco::$TIPO_BANCO_WINDOWS_PHONE:
                return true;
            default:
                return false;
        }
    }
    
    //TODO responsavel por copiar a estrutura da tabela de tgt para src
    public function copiaEstruturaTabela($pObjTabelaSrc, $pObjTabelaTgt) {
        
    }

    //TODO responsavel por copiar a estrutura das tabelas da web para os bancos 
    //mobile (android, iphone, windows8), as tabelas copiadas sao as que possuem sincronizacao
    //do tipo: web->mobile, web<->mobile
    //OBS.:Tabelas com nome igual a tabela web nao devem ser copiadas, deve ser emitido
    //um alerta para esses casos.
    public function copiaEstruturaBancoWebParaMobile() {
        
    }

    //TODO responsavel por copiar a estrutura das tabelas do mobile(android, iphone, windows8), 
    //para web, as tabelas copiadas sao as que possuem sincronizacao
    //do tipo: web<-mobile, web<->mobile
    //OBS.: Cuidado com a diferenca de tipo de atributo das tabelas do banco sqlite3 para sql,
    //ex.: date, datetime.
    public function copiaEstruturaBancoMobileParaWeb() {
        
    }

    
    public static function getListaIdentificadorNoBancoProducao($idConexoes) {
        $q = "SELECT bb.id, b.nome
                FROM banco_banco bb 
                    JOIN banco b
                        ON bb.producao_banco_id_INT = b.id
                WHERE b.conexoes_id_INT = {$idConexoes}
                ORDER BY b.nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    
    public static function getListaIdentificadorNoBancoHomologacao($idConexoes) {
        $q = "SELECT bb.id, b.nome
                FROM banco_banco bb 
                    JOIN banco b
                        ON bb.homologacao_banco_id_INT = b.id
                WHERE b.conexoes_id_INT = {$idConexoes}
                ORDER BY b.nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
}

