<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos
    * NOME DA CLASSE DAO: DAO_Projetos
    * DATA DE GERAÇÃO:    08.01.2013
    * ARQUIVO:            EXTDAO_Projetos.php
    * TABELA MYSQL:       projetos
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos extends DAO_Projetos
    {
        
        public static $INDICE_BANCO_WEB_PRODUCAO = 2;
        public static $INDICE_BANCO_MOBILE_PRODUCAO = 1;
        public static $INDICE_BANCO_WEB_HOMOLOGACAO = 4;
        public static $INDICE_BANCO_MOBILE_HOMOLOGACAO= 3;
        
        public static $VETOR_TIPO_BANCO = array("Banco Web Homologação", "Banco Web Produção", "Banco Android Homologação", "Banco Android Produção");
        
        public static function VETOR_ID_TIPO_BANCO(){
            return array(EXTDAO_Tipo_banco::$TIPO_BANCO_WEB, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID);
        }
        
        public static function VETOR_BANCO_HOMOLOGACAO(){
            return array(true, false, true, false);
        }
        
        public static function getVetorObjetoBancoDoProjeto($pIdProjeto){
            $vVetorIdBanco = EXTDAO_Projetos::getVetorIdBancoDoProjeto($pIdProjeto);
            $vetorRetorno = array();
            if($vVetorIdBanco != null)
            foreach ($vVetorIdBanco as $id) {
                $vObjBanco = null;
                if(is_numeric($id)){
                    $vObjBanco = new EXTDAO_Banco();
                    $vObjBanco->select($id);
                }
                $vetorRetorno[count($vetorRetorno)] = $vObjBanco;
            }
            return $vetorRetorno;
        }
        
        public static function getVetorIdBancoDoProjeto($pIdProjeto){
            $obj = new EXTDAO_Banco();
            
            return array(
                
                $obj->getIdBancoDoProjeto($pIdProjeto, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB, true), 
                $obj->getIdBancoDoProjeto($pIdProjeto, EXTDAO_Tipo_banco::$TIPO_BANCO_WEB, false), 
                $obj->getIdBancoDoProjeto($pIdProjeto, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID, true),
                $obj->getIdBancoDoProjeto($pIdProjeto, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID, false)
            );
        }
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos";

         

        }

        public function setLabels(){

            $this->label_id = "Id";
            $this->label_nome = "Nome";
            $this->label_conexaoBanco_INT = "Conexão Com O Banco";
            $this->label_nomeBanco = "Nome do Banco";
            $this->label_diretorioClassesDAO = "Diretório da Classe DAO Web";
            $this->label_diretorioClassesEXTDAO = "Diretório Classe EXTDAO Web";
            $this->label_diretorioForms = "Diretório Forms Web";
            $this->label_diretorioFiltros = "Diretório Filtros Web";
            $this->label_diretorioLists = "Diretório Lists Web";
            $this->label_diretorioAjaxForms = "Diretório Ajax Forms Web";
            $this->label_diretorioAjaxPages = "Diretório Ajax Pages Web";
            $this->label_colunasForms_INT = "Colunas Forms";
            $this->label_chaveComNomeTabela = "Nome Identificador da Tabela";
            $this->label_underlineNomeClasse = "";
            $this->label_status_INT = "Status";
            $this->label_androidDiretorioClassesDAO = "Diretório Classe DAO Android";
            $this->label_androidDiretorioClassesEXTDAO = "Diretório Classe EXTDAO Android";
            $this->label_androidForms = "Diretório Forms Android";
            $this->label_androidFiltros = "Diretório Filtros Android";
            $this->label_androidLists = "Diretório Lists Android";
            $this->label_androidAdapter = "Diretório Adapter Android";
            $this->label_androidItemList = "Diretório Itemlist Android";
            $this->label_androidDetail = "Diretório Detail Android";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos();

        }
        
        public static function getNomeProjetoPeloId($id){
            if(strlen($id)){
                $obj = new EXTDAO_Projetos();
                $obj->select($id);
                return $obj->getNome();
            }
        }
        
        
        
        
         public function selecionaProjetoAtual()
        {
             
            $vIdProjeto = Helper::GET("id");
            if(strlen($vIdProjeto)){
                Seguranca::setIdDoProjetoSelecionado($vIdProjeto);
                $vIdVersao = EXTDAO_Projetos_versao::getIdVersaoProjeto($vIdProjeto);
                
                if($vIdVersao == null){
                    return array("location: actions.php?tipo=lists&p=lists&msgErro=O Projeto atual não está versionado, favor criar uma versão para iniciar a homologação do projeto.");
                }
                else{
                    Seguranca::setIdDoProjetoVersaoSelecionado($vIdVersao);
                    return array("location: index.php?tipo=pages&page=seleciona_banco_banco&".Param_Get::ID_PROJETOS_VERSAO."=".$vIdVersao);
                    //return array("location: index.php?msgSucesso=Projeto selecionado com sucesso.");
                }
                
            }
            
        }
        
        public static function getListaIdentificadorNo(){
            $q = "SELECT id, nome FROM projetos ORDER BY nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        
	}

    
