<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Cidade
    * NOME DA CLASSE DAO: DAO_Cidade
    * DATA DE GERAÇÃO:    06.08.2011
    * ARQUIVO:            EXTDAO_Cidade.php
    * TABELA MYSQL:       cidade
    * BANCO DE DADOS:     websaude
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Cidade extends DAO_Cidade
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Cidade";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_uf_id_INT = "Uf";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Cidade();

        }

	}

    
