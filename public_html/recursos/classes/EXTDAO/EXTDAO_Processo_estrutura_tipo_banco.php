<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Processo_estrutura_tipo_banco
    * NOME DA CLASSE DAO: DAO_Processo_estrutura_tipo_banco
    * DATA DE GERAÇÃO:    28.05.2013
    * ARQUIVO:            EXTDAO_Processo_estrutura_tipo_banco.php
    * TABELA MYSQL:       processo_estrutura_tipo_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Processo_estrutura_tipo_banco extends DAO_Processo_estrutura_tipo_banco
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Processo_estrutura_tipo_banco";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_processo_estrutura_id_INT = "Processo";
			$this->label_tipo_banco_id_INT = "Tipo de Banco";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Processo_estrutura_tipo_banco();

        }

	}

    
