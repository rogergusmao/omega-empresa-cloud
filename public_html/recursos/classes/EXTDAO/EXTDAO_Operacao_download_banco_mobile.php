<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Operacao_download_banco_mobile
 * NOME DA CLASSE DAO: DAO_Operacao_download_banco_mobile
 * DATA DE GERAÇÃO:    30.03.2013
 * ARQUIVO:            EXTDAO_Operacao_download_banco_mobile.php
 * TABELA MYSQL:       operacao_download_banco_mobile
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Operacao_download_banco_mobile extends DAO_Operacao_download_banco_mobile {

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Operacao_download_banco_mobile";

    }

    public function setLabels() {
        $this->label_destino_banco_id_INT = "Banco de Destino";
        $this->label_id = "Id";
        $this->label_path_script_sql_banco = "Script do Sql do Banco";
        $this->label_observacao = "Observação";
        $this->label_operacao_sistema_mobile_id_INT = "Operação do Sistema do Telefone";
        $this->label_popular_tabela_BOOLEAN = "Popular Tabela?";
        $this->label_drop_tabela_BOOLEAN = "Inserir script de apagar a tabela?";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public static function factory() {

        return new EXTDAO_Operacao_download_banco_mobile();
    }

    public static function selectOperacaoSistema($pIdOperacaoSistemaMobile, $pCabecalhoSelect = "*") {
        $consulta = "SELECT {$pCabecalhoSelect}
                        FROM operacao_download_banco_mobile ";

        $consulta .= " WHERE operacao_sistema_mobile_id_INT = {$pIdOperacaoSistemaMobile} ";

        $consulta .= " ORDER BY id
                             LIMIT 0, 1";
        $db = new Database();
        $db->query($consulta);
        $vMatriz = Helper::getResultSetToMatriz($db->result);
        if (count($vMatriz))
            return $vMatriz[0];
        else
            return null;
    }
    
    public static function getIdOperacaoSistemaMobile($pIdOperacaoSistemaMobile) {
        $consulta = "SELECT id
                        FROM operacao_download_banco_mobile ";

        $consulta .= " WHERE operacao_sistema_mobile_id_INT = {$pIdOperacaoSistemaMobile} ";

        $consulta .= " ORDER BY id
                             LIMIT 0, 1";
        $db = new Database();
        $db->query($consulta);
        return $db->getPrimeiraTuplaDoResultSet(0);
        
    }
    
    public static function getPathDiretorio($idOSM){
        $path = GerenciadorArquivo::getPathDoTipo(
                        $idOSM,
                        "operacao_sistema_mobile",
                        GerenciadorArquivo::ID_SCRIPT);
        return $path;
    }
    
    
    public static function downloadScript(){
        
        
        $idODBM = Helper::POSTGET(Param_Get::ID_OPERACAO_DOWNLOAD_BANCO_MOBILE);
        $obj = new EXTDAO_Operacao_download_banco_mobile();
        $obj->select($idODBM);
        $obj->getOperacao_sistema_mobile_id_INT();
        $nomeArquivo = $obj->getPath_script_sql_banco();
        $idOSM = $obj->getOperacao_sistema_mobile_id_INT();
        $raiz = Helper::acharRaiz();

        $pathArq = $raiz.EXTDAO_Operacao_download_banco_mobile::getPathDiretorio($idOSM).$nomeArquivo;
        if(is_file($pathArq)){
            
            $objDownload = new Download($pathArq);
            print $objDownload->df_download();
        }
    }

	 public function __actionAdd(){

            $idMI = Helper::POST(Param_Get::ID_MOBILE_IDENTIFICADOR);
            $idOSM = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($idMI, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_DOWNLOAD_BANCO_MOBILE);
            
            $this->setByPost(1);
            $this->setOperacao_sistema_mobile_id_INT($idOSM);
            $nomeArqScript = Helper::getNomeAleatorio("banco_android", ".sql");
            $this->setPath_script_sql_banco($nomeArqScript);
            
            $this->formatarParaSQL();

            $this->insert();
            $this->selectUltimoRegistroInserido();

            return array("location: popup.php?tipo=pages&page=status_operacao_sistema_mobile&".Param_Get::ID_OPERACAO_SISTEMA_MOBILE."=".$idOSM);

        }

        public static function criaAvulsoDownloadBancoSQLiteMobile($idMI, $prefixoArquivo = null){
            return self::criaAvulso($idMI, $prefixoArquivo, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_DOWNLOAD_BANCO_SQLITE);
        }
        
        public static function criaAvulsoDownloadBancoMobile($idMI, $prefixoArquivo = null){
            return self::criaAvulso($idMI, $prefixoArquivo, EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_DOWNLOAD_BANCO_MOBILE);
        }
        private static function criaAvulso($idMI, $prefixoArquivo = null, $idTIpoOperacaoMobile){
            $msg = null;
            try{
                $objOSM = new EXTDAO_Operacao_download_banco_mobile();
                $idOSM = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile(
                    $idMI, 
                    $idTIpoOperacaoMobile,
                    EXTDAO_Estado_operacao_sistema_mobile::AVULSA);

                $objOSM->setOperacao_sistema_mobile_id_INT($idOSM);
                if($prefixoArquivo == null){
                    $prefixoArquivo = "sqlite";
                    $nomeArqScript = Helper::getNomeAleatorio($prefixoArquivo, ".sql");
                }
                else 
                    $nomeArqScript = $prefixoArquivo;
                $objOSM->setPath_script_sql_banco($nomeArqScript);

                $objOSM->formatarParaSQL();

                $objOSM->insert();
                $idODBM = $objOSM->getIdDoUltimoRegistroInserido();
                $mensagemRet = new Mensagem_protocolo( 
                    new Protocolo_operacao_avulsa($idOSM, $idODBM),
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, 
                    "Gerada com sucesso");
                $msg = $mensagemRet;
            } catch (Exception $ex) {
                $msg =new Mensagem(null,null,$ex);
            }
            echo json_encode($msg);
        }
        
    
}

