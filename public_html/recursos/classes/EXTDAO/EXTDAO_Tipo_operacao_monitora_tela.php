<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_operacao_monitora_tela
    * NOME DA CLASSE DAO: DAO_Tipo_operacao_monitora_tela
    * DATA DE GERAÇÃO:    25.10.2013
    * ARQUIVO:            EXTDAO_Tipo_operacao_monitora_tela.php
    * TABELA MYSQL:       tipo_operacao_monitora_tela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_operacao_monitora_tela extends DAO_Tipo_operacao_monitora_tela
    {
        const PRINT_SCREEN_MOBILE_PARA_WEB = 1;
        //const print_screen_mobile_para_web=1;
        const acao_de_clique_web_para_mobile=2;
        const desenha_circulo_na_tela_do_mobile =3 ;
        const desenha_retangulo_na_tela_do_mobile = 4;
        const mensagem_toast = 5;
        const dialog_message = 6;
        const fechar_monitoramento = 7;
        const resposta_mobile_web_da_requisicao = 8;
        const dimensao_da_tela = 9;
        const nao_desconecta_continua_conectado_mobile = 10;
        const desenha_reta_na_tela_do_mobile = 11;
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_operacao_monitora_tela";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_operacao_monitora_tela();

        }

	}

    
