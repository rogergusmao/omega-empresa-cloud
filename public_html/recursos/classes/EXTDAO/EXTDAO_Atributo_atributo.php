<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Atributo_atributo
    * NOME DA CLASSE DAO: DAO_Atributo_atributo
    * DATA DE GERAÃ‡ÃƒO:    28.01.2013
    * ARQUIVO:            EXTDAO_Atributo_atributo.php
    * TABELA MYSQL:       atributo_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // *****************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Atributo_atributo extends DAO_Atributo_atributo
    {
        
        const FATOR_TAMANHO = 1;
        const FATOR_DECIMAL = 2;
        const FATOR_NOME = 3;
        
        const FATOR_NAO_NULO = 4;
        const FATOR_CHAVE_PRIMARIA = 5;
        const FATOR_AUTO_INCREMENTO = 6;
        const FATOR_DEFAULT = 7;
        const FATOR_CHAVE_EXTRANGEIRA = 8;
        const FATOR_ON_UPDATE = 9;
        const FATOR_ON_DELETE = 10;
        const FATOR_TIPO_SQL = 11;
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Atributo_atributo";

          

        }

        public function __actionEdit(){

            $numeroRegistros = Helper::POST("numeroRegs");
            
            $idTT = Helper::POST(Param_Get::TABELA_TABELA);
            $idPV = Helper::POST(Param_Get::ID_PROJETOS_VERSAO);
            $idPVBB = Helper::POST(Param_Get::ID_PROJETOS_VERSAO);
            $varGet = Param_Get::getIdentificadorProjetosVersao();
            $varGet .= Param_Get::TABELA_TABELA."={$idTT}";
            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id")).$varGet;
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id")).$varGet;
            
            for($i=1; $i <= $numeroRegistros; $i++){
                $this->setByPost($i);
                
                //Ajustando o fato do <select> ser disativado
                $vObjAtributoAtributoSendoEditado = new EXTDAO_Atributo_atributo();
                $vObjAtributoAtributoSendoEditado->select($this->getId());
                //se nao houver alteracao
                if($vObjAtributoAtributoSendoEditado->getProducao_atributo_id_INT() == $this->getProducao_atributo_id_INT())
                    continue;
                
                $this->setHomologacao_atributo_id_INT($vObjAtributoAtributoSendoEditado->getHomologacao_atributo_id_INT());
                $this->setTipo_operacao_atualizacao_banco_id_INT($vObjAtributoAtributoSendoEditado->getTipo_operacao_atualizacao_banco_id_INT());
                
                $vIdAtributoProducao = $this->getProducao_atributo_id_INT();
                
                $idAAProducao = EXTDAO_Atributo_atributo::getIdAtributoAtributoDoAtributoProducao($idPVBB, $vIdAtributoProducao ) ;
                
                $vObjAtributoAtributoRelacionadoAtributoProducao = null;
                if(strlen($idAAProducao)){
                    $vObjAtributoAtributoRelacionadoAtributoProducao = new EXTDAO_Atributo_atributo();
                    $vObjAtributoAtributoRelacionadoAtributoProducao->select($idAAProducao);
                }
                    
                if(!strlen($idAAProducao) ||
                    $idAAProducao == $this->getId()
                   || 
                    $vObjAtributoAtributoRelacionadoAtributoProducao == null ||
                    (
                        $vObjAtributoAtributoRelacionadoAtributoProducao != null &&
                        !strlen($vObjAtributoAtributoRelacionadoAtributoProducao->getHomologacao_atributo_id_INT())
                    )
                   ){
                    
                    if($vObjAtributoAtributoRelacionadoAtributoProducao != null){
                        $this->delete($idAAProducao);
                    } else if($vObjAtributoAtributoRelacionadoAtributoProducao == null &&
                            strlen($vObjAtributoAtributoSendoEditado->getProducao_atributo_id_INT()) ){
                        //se for desassociando a atual atributo de produÃ§Ã£o
                        $vObjAtributoAtributoRelacionadoAtributoProducao = new EXTDAO_Atributo_atributo();
                        $vObjAtributoAtributoRelacionadoAtributoProducao->setProducao_atributo_id_INT($vObjAtributoAtributoSendoEditado->getProducao_atributo_id_INT());
                        $vObjAtributoAtributoRelacionadoAtributoProducao->setProjetos_versao_id_INT($vObjAtributoAtributoSendoEditado->getProjetos_versao_id_INT());
                        $vObjAtributoAtributoRelacionadoAtributoProducao->setProjetos_versao_banco_banco_id_INT($vObjAtributoAtributoSendoEditado->setProjetos_versao_banco_banco_id_INT());
                        $vObjAtributoAtributoRelacionadoAtributoProducao->setStatus_verificacao_BOOLEAN("0");
                        $vObjAtributoAtributoRelacionadoAtributoProducao->formatarParaSQL();
                        $vObjAtributoAtributoRelacionadoAtributoProducao->insert();
                        $vObjAtributoAtributoRelacionadoAtributoProducao->selectUltimoRegistroInserido();
                        $vIdTipoOperacao = $vObjAtributoAtributoRelacionadoAtributoProducao->atualizaTipoOperacaoAtualizacaoBanco($idPVBB);

                        $vObjAtributoAtributoRelacionadoAtributoProducao->setTipo_operacao_atualizacao_banco_id_INT($vIdTipoOperacao);

                        $vObjAtributoAtributoRelacionadoAtributoProducao->formatarParaSQL();

                        $vObjAtributoAtributoRelacionadoAtributoProducao->update($vObjAtributoAtributoRelacionadoAtributoProducao->getId());
                        
                    }
                    
                    $this->formatarParaSQL();

                    $this->update($this->getId(), $_POST, $i);

                    $this->select($this->getId());
                
                    $vIdTipoOperacao = $this->atualizaTipoOperacaoAtualizacaoBanco($idPVBB);
                    
                    $this->setTipo_operacao_atualizacao_banco_id_INT($vIdTipoOperacao);
                    
                    $this->formatarParaSQL();
                    
                    $this->update($this->getId());
                    
                    
                } else{
                    $vStrMensagemAlertaDuplicadaDeAtributoProducao = EXTDAO_Atributo_atributo::getMensagemAtributoHomologacaoComAtributoProducaoDuplicada($idPVBB, $vIdAtributoProducao);
                    if(strlen($vStrMensagemAlertaDuplicadaDeAtributoProducao ) > 0 )
                        $vStrMensagemAlertaDuplicadaDeAtributoProducao = "Existe associaÃ§Ã£o de outro atributo de homologaÃ§Ã£o com o atributo de produÃ§Ã£o selecionado, resolva as pendÃªncias de: " . $vStrMensagemAlertaDuplicadaDeAtributoProducao;
                    else $vStrMensagemAlertaDuplicadaDeAtributoProducao = "";
                    
                    return array("location: $urlErro&{$varGet}&msgErro=$vStrMensagemAlertaDuplicadaDeAtributoProducao&id{$i}={$this->getId()}");
                }
            }

            return array("location: $urlSuccess&{$varGet}&msgSucesso=Relacionamento entre os atributos alterado com sucesso");

        }

        public static function existeAtributoAtributoComOTipoOperacao($idPVBB, $idTOAB){
            $str = "";
            if(is_array($idTOAB)){
                foreach ($idTOAB as $idTipo) {
                    if(strlen($str))
                        $str .= ", ".$idTipo;
                    else
                        $str .= $idTipo;
                }
                $str = " IN($str)";
            } else {
                $str = " = $idTOAB";
            }
         $objBanco = new Database();
            $objBanco->query("SELECT id
FROM atributo_atributo
WHERE tipo_operacao_atualizacao_banco_id_INT $str
    AND projetos_versao_banco_banco_id_INT =  $idPVBB
    LIMIT 0,1 ");
            if(strlen($objBanco->getPrimeiraTuplaDoResultSet(0)))
                return true;
            else return false;
                    
        }
        public static function getIdAtributoAtributoDoAtributoProducao($idPVBB, $idAProducao){
            $objBanco = new Database();
            $objBanco->query("SELECT id
FROM atributo_atributo
WHERE producao_atributo_id_INT = $idAProducao 
    AND projetos_versao_banco_banco_id_INT = $idPVBB 
    LIMIT 0,1 ");
            return Helper::getResultSetToPrimeiroResultado($objBanco->result);
        }
        
        public static function getMensagemAtributoHomologacaoComAtributoProducaoDuplicada($idPVBB, $pIdAtributoProducao){
            $objBanco = new Database();
            $objBanco->query("SELECT t.nome nome_atributo_homologacao, t2.nome nome_atributo_producao
FROM atributo_atributo tt JOIN atributo t ON tt.homologacao_atributo_id_INT = t.id 
    JOIN atributo t2 ON tt.producao_atributo_id_INT = t2.id
WHERE t2.id = $pIdAtributoProducao AND tt.projetos_versao_banco_banco_id_INT = $idPVBB
GROUP BY tt.id");
            $vVetorNome = Helper::getResultSetToMatriz($objBanco->result);
            if(count($vVetorNome) == 0 ) return null;
            else {
                $str = "";
                if($vVetorNome != null)
                foreach ($vVetorNome as $vRelacao) {
                    if(strlen ($str) > 0 ) $str .= ", ";
                    $str = $vRelacao["nome_atributo_homologacao"].":".$vRelacao["nome_atributo_producao"];
                    
                }
                return  $str;
            }
            
        }
        
        public function setLabels(){

			$this->label_id = "Id";
			$this->label_homologacao_atributo_id_INT = "Atributo de HomologaÃ§Ã£o";
			$this->label_producao_atributo_id_INT = "Atributo de ProduÃ§Ã£o";
			$this->label_novo_nome = "Novo Nome do Atributo de ProduÃ§Ã£o";
			$this->label_tipo_operacao_atualizacao_banco_id_INT = "Tipo de OperaÃ§Ã£o de AtualizaÃ§Ã£o";
			$this->label_projetos_versao_id_INT = "VersÃ£o do Projeto";
                        $this->label_status_verificacao_BOOLEAN = "Foi verificada?";

        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }
        
        public static function existeRelacaoAtributoHomologacao($idPVBB, $pIdAtributoHomologacao, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT id 
                FROM atributo_atributo 
                WHERE  homologacao_atributo_id_INT= {$pIdAtributoHomologacao} 
                   AND projetos_versao_banco_banco_id_INT = {$idPVBB}
                LIMIT 0,1");
            return (Helper::getResultSetToPrimeiroResultado($db->result) == null) ? false : true;
        }
        
        public static function existeRelacaoAtributoProducao($idPVBB, $pIdAtributoProducao, $db = null){
            if($db == null) $db = new Database();
                
            $db->query("SELECT id 
                FROM atributo_atributo 
                WHERE  producao_atributo_id_INT= {$pIdAtributoProducao} 
                    AND projetos_versao_banco_banco_id_INT = {$idPVBB}
                    LIMIT 0,1");
            return (Helper::getResultSetToPrimeiroResultado($db->result) == null) ? false : true;
        }

        public static function factory(){

            return new EXTDAO_Atributo_atributo();

        }
        
        public function getObjAtributoHomologacao(){
            if($this->getHomologacao_atributo_id_INT() != null){
                $obj = new EXTDAO_Atributo($this->getDatabase());
                $vIdAtributo = $this->getHomologacao_atributo_id_INT();
                if($vIdAtributo != null){
                    $obj->select($vIdAtributo);
                    return $obj;
                }
            }
            return null;
        }
        
        public function getComboBoxAllTipo_operacao_atualizacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;
                $objArgumentos->isDisabled = true;
		$vRetorno = $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);
                $objArgumentos->isDisabled = false;
                return $vRetorno;

	}
        
        public function getComboBoxGerenciaListAllTipo_operacao_atualizacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_atualizacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;
                
		$vRetorno = $this->getFkObjTipo_operacao_atualizacao_banco()->getComboBox($objArgumentos);
                
                return $vRetorno;

	}
        
        public function getComboBoxAllHomologacao_atributo($objArgumentos){

		$objArgumentos->nome="homologacao_atributo_id_INT";
		$objArgumentos->id="homologacao_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;
                $objArgumentos->isDisabled = true;
		$vRetorno = $this->getFkObjHomologacao_atributo()->getComboBox($objArgumentos);
                $objArgumentos->isDisabled = false;
                return $vRetorno;

	}
        
        public function getComboBoxGerenciaListAllHomologacao_atributo($objArgumentos){

		$objArgumentos->nome="homologacao_atributo_id_INT";
		$objArgumentos->id="homologacao_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;
                
                $vIdAtributoAtributo = Helper::GET("atributo_atributo_id_INT");
                
                $vObjAtributoAtributo = new EXTDAO_Atributo_atributo();
                $vObjAtributoAtributo->select($vIdAtributoAtributo);
                
                $vIdAtributoHom = $vObjAtributoAtributo->getHomologacao_atributo_id_INT();
                if(!strlen($vIdAtributoHom))
                    $vIdAtributoHom = -1;
                $objArgumentos->strFiltro = " tp.atributo_id_INT = ".$vIdAtributoHom;
                
                
		$vRetorno = $this->getFkObjHomologacao_atributo()->getComboBox($objArgumentos);
                $objArgumentos->strFiltro = "";
                return $vRetorno;

	}
        
        public function getObjAtributoProducao(){
            if($this->getProducao_atributo_id_INT() != null){
                $obj = new EXTDAO_Atributo();
                $vIdAtributo = $this->getProducao_atributo_id_INT();
                if($vIdAtributo != null){
                    $obj->select($vIdAtributo);
                    return $obj;
                }
            }
            return null;
        }
       
        public function comparaAtributoHomologacaoComProducao($idPVBB){
            
            $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            $vFatores = array();
            $vFatoresTipoOperacao = array();
            $vAtributoAtributoFK = null;
            if($this->getProducao_atributo_id_INT() == null) 
                $vTipoModificacao= EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
            else if($this->getHomologacao_atributo_id_INT() == null) 
                $vTipoModificacao =EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
            else if($this->getHomologacao_atributo_id_INT() != null && 
                    $this->getProducao_atributo_id_INT() != null){
                
                $vObjAtributoHom = $this->getObjAtributoHomologacao();
                $vObjAtributoProd = $this->getObjAtributoProducao();
                if($vObjAtributoHom->getNome() != $vObjAtributoProd->getNome()){
                    $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_NOME;
                    if(strlen($vObjAtributoHom->getNome()) &&
                            !strlen($vObjAtributoProd->getNome()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getNome()) &&
                            strlen($vObjAtributoProd->getNome()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }
                    
                 if($vObjAtributoHom->getTipo_sql() != $vObjAtributoProd->getTipo_sql()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_TIPO_SQL;
                    if(strlen($vObjAtributoHom->getTipo_sql()) &&
                            !strlen($vObjAtributoProd->getTipo_sql()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TIPO_SQL] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getTipo_sql()) &&
                            strlen($vObjAtributoProd->getTipo_sql()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TIPO_SQL] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TIPO_SQL] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                 }
                 if($vObjAtributoHom->getNot_null_BOOLEAN() != $vObjAtributoProd->getNot_null_BOOLEAN()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_NAO_NULO;
                    
                    if(strlen($vObjAtributoHom->getNot_null_BOOLEAN()) &&
                            !strlen($vObjAtributoProd->getNot_null_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NAO_NULO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getNot_null_BOOLEAN()) &&
                            strlen($vObjAtributoProd->getNot_null_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NAO_NULO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NAO_NULO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    
                 }
                
                 if($vObjAtributoHom->getPrimary_key_BOOLEAN() != $vObjAtributoProd->getPrimary_key_BOOLEAN()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_CHAVE_PRIMARIA;
                    if(strlen($vObjAtributoHom->getPrimary_key_BOOLEAN()) &&
                            !strlen($vObjAtributoProd->getPrimary_key_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_PRIMARIA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getPrimary_key_BOOLEAN()) &&
                            strlen($vObjAtributoProd->getPrimary_key_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_PRIMARIA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_PRIMARIA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    
                 }
                
                 if($vObjAtributoHom->getAuto_increment_BOOLEAN() != $vObjAtributoProd->getAuto_increment_BOOLEAN()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_AUTO_INCREMENTO;
                    if(strlen($vObjAtributoHom->getAuto_increment_BOOLEAN()) &&
                            !strlen($vObjAtributoProd->getAuto_increment_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_AUTO_INCREMENTO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getAuto_increment_BOOLEAN()) &&
                            strlen($vObjAtributoProd->getAuto_increment_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_AUTO_INCREMENTO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_AUTO_INCREMENTO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    
                 }
                
                 if($vObjAtributoHom->getTamanho_INT() != $vObjAtributoProd->getTamanho_INT()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_TAMANHO;
                    if(strlen($vObjAtributoHom->getTamanho_INT()) &&
                            !strlen($vObjAtributoProd->getTamanho_INT()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TAMANHO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getTamanho_INT()) &&
                            strlen($vObjAtributoProd->getTamanho_INT()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TAMANHO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TAMANHO] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    
                 }
                 if($vObjAtributoHom->getDecimal_INT() != $vObjAtributoProd->getDecimal_INT()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_DECIMAL;
                    
                    if(strlen($vObjAtributoHom->getDecimal_INT()) &&
                            !strlen($vObjAtributoProd->getDecimal_INT()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DECIMAL] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getDecimal_INT()) &&
                            strlen($vObjAtributoProd->getDecimal_INT()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DECIMAL] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DECIMAL] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    
                 }
                
                 if($vObjAtributoHom->getValor_default() != $vObjAtributoProd->getValor_default()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_DEFAULT;
                    
                    if(strlen($vObjAtributoHom->getValor_default()) &&
                            !strlen($vObjAtributoProd->getValor_default()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DEFAULT] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getValor_default()) &&
                            strlen($vObjAtributoProd->getValor_default()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DEFAULT] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DEFAULT] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    
                 }
                 
                $vObjComparaFK = $this->comparaChaveExtrangeira($idPVBB);
                
                if($vObjComparaFK["tipo_modificacao"] == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
                    $vFatores = array_merge($vObjComparaFK["fatores"], $vFatores);
                    if($vObjComparaFK["fatores_tipo_operacao"] != null)
                    foreach ($vObjComparaFK["fatores_tipo_operacao"] as $key => $value) {
                        $vFatoresTipoOperacao[$key] = $value;
                    }
                    $vAtributoAtributoFK = $vObjComparaFK["atributo_atributo_fk"];
                }
                
                
            }
            return array("tipo_modificacao" => $vTipoModificacao,
                "fatores" => $vFatores,
                "fatores_tipo_operacao" => $vFatoresTipoOperacao,
                "atributo_atributo_fk" => $vAtributoAtributoFK
            );
        }
        
        public function atualizaTipoOperacaoAtualizacaoBanco($idPVBB){
            
            if($this->getProducao_atributo_id_INT() == null)
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
            else if($this->getHomologacao_atributo_id_INT() == null)
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
            else if($this->getHomologacao_atributo_id_INT() != null && 
                    $this->getProducao_atributo_id_INT() != null){
                
                $vObjAtributoHom = $this->getObjAtributoHomologacao();
                $vObjAtributoProd = $this->getObjAtributoProducao();
                
                if($vObjAtributoHom->getNome() != $vObjAtributoProd->getNome()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                else if($vObjAtributoHom->getTipo_sql() != $vObjAtributoProd->getTipo_sql()){
                    
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                else if($vObjAtributoHom->getNot_null_BOOLEAN() != $vObjAtributoProd->getNot_null_BOOLEAN()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                else if($vObjAtributoHom->getPrimary_key_BOOLEAN() != $vObjAtributoProd->getPrimary_key_BOOLEAN()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                
                else if($vObjAtributoHom->getAuto_increment_BOOLEAN() != $vObjAtributoProd->getAuto_increment_BOOLEAN()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                
                else if($vObjAtributoHom->getTamanho_INT() != $vObjAtributoProd->getTamanho_INT()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                
                else if($vObjAtributoHom->getDecimal_INT() != $vObjAtributoProd->getDecimal_INT()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                
                else if($vObjAtributoHom->getValor_default() != $vObjAtributoProd->getValor_default()){
                    $tipo = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    return $tipo;
                }
                else {
                    $vTipoAtualizacao = $this->checaTipoOperacaoAtualizacaoBancoDaChaveExtrangeira($idPVBB);
                    if($vTipoAtualizacao != EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION)
                        return $vTipoAtualizacao;
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
                }

            }
            if($tipo != null) return $tipo;
            
            return null;
        }
        
        public function checaTipoOperacaoAtualizacaoBancoDaChaveExtrangeira($idPVBB){
            $objAtributoHom = $this->getObjAtributoHomologacao();
            $objAtributoProd = $this->getObjAtributoProducao();
            if($objAtributoHom == null && $objAtributoProd == null)
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            else if($objAtributoHom == null && $objAtributoProd != null)
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
            else if($objAtributoHom != null && $objAtributoProd == null)
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
            else{
//                echo "ENTROU".$objAtributoHom->getFk_nome()." != ".$objAtributoProd->getFk_nome();
                
                if($objAtributoHom->getUpdate_tipo_fk() != $objAtributoProd->getUpdate_tipo_fk())
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                else if($objAtributoHom->getDelete_tipo_fk() != $objAtributoProd->getDelete_tipo_fk())
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                else if ($objAtributoHom->getFk_nome() != $objAtributoProd->getFk_nome())
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                $atributoFKHomologacao = $objAtributoHom->getFk_atributo_id_INT();
                $atributoFKProducao = $objAtributoProd->getFk_atributo_id_INT();
                if(strlen($atributoFKHomologacao) > 0 && strlen($atributoFKProducao) > 0){
                    $objAtributoAtributo = EXTDAO_Atributo_atributo::getObjAtributoAtributo($idPVBB, $atributoFKHomologacao, $atributoFKProducao);

                    //se a atributo so foi renomeada do banco de homologacao para o de producao entao nao ocorre nada
                    if($objAtributoAtributo == null) 
                        return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }else if(strlen($atributoFKHomologacao) > 0 || strlen($atributoFKProducao) > 0) {
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                } 
                
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            }
            
        }
        
        
        public function comparaChaveExtrangeira($idPVBB){
            $vFatoresTipoOperacao = array();
            $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            $vFatores = array();
            $idAtributoAtributoFK = null;
            $vObjAtributoHom = $this->getObjAtributoHomologacao();
            $vObjAtributoProd = $this->getObjAtributoProducao();
            if($vObjAtributoHom == null && $vObjAtributoProd == null)
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            else if($vObjAtributoHom == null && $vObjAtributoProd != null)
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
            else if($vObjAtributoHom != null && $vObjAtributoProd == null)
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
            else{
                if($vObjAtributoHom->getUpdate_tipo_fk() != $vObjAtributoProd->getUpdate_tipo_fk()){
                    $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_ON_UPDATE;
                    if(strlen($vObjAtributoHom->getUpdate_tipo_fk()) &&
                            !strlen($vObjAtributoProd->getUpdate_tipo_fk()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_UPDATE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getUpdate_tipo_fk()) &&
                            strlen($vObjAtributoProd->getUpdate_tipo_fk()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_UPDATE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_UPDATE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
                    
                }
                if($vObjAtributoHom->getDelete_tipo_fk() != $vObjAtributoProd->getDelete_tipo_fk()){
                     $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                     $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_ON_DELETE;
                     
                    if(strlen($vObjAtributoHom->getUpdate_tipo_fk()) &&
                            !strlen($vObjAtributoProd->getUpdate_tipo_fk()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_DELETE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjAtributoHom->getUpdate_tipo_fk()) &&
                            strlen($vObjAtributoProd->getUpdate_tipo_fk()))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_DELETE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_DELETE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
                    
                }
                
                $vAtributoFKHomologacao = $vObjAtributoHom->getFk_atributo_id_INT();
                $vAtributoFKProducao = $vObjAtributoProd->getFk_atributo_id_INT();
                if(strlen($vAtributoFKHomologacao) > 0 && strlen($vAtributoFKProducao) > 0){
                    $vObjAtributoAtributo = EXTDAO_Atributo_atributo::getObjAtributoAtributo($idPVBB, $vAtributoFKHomologacao, $vAtributoFKProducao);

                    //se a atributo so foi renomeada do banco de homologacao para o de producao entao nao ocorre nada
                    if($vObjAtributoAtributo == null) {
                        $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                        $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA;
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                                
                        
                    }else{
                        $idAtributoAtributoFK = $vObjAtributoAtributo->getId();
                    }
                } else if(strlen($vAtributoFKHomologacao) > 0 || strlen($vAtributoFKProducao) > 0) {
                    $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA;
                    
                    if(strlen($vAtributoFKHomologacao) &&
                        !strlen($vAtributoFKProducao))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vAtributoFKHomologacao) &&
                            strlen($vAtributoFKProducao))
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;

                } 
                
                
            }
            $ret = array("tipo_modificacao" => $vTipoModificacao,
                    "fatores" => $vFatores,
                    "atributo_atributo_fk" => $idAtributoAtributoFK,
                    "fatores_tipo_operacao" => $vFatoresTipoOperacao);
                
//            if($vObjAtributoHom->getId() == "29657"){
//                print_r($ret);
//                exit();
//            }
            
            return $ret;
        }
        
        public static function getIdAtributoAtributoProd($idPVBB, $pIdAtributoProd, $db = null){
            $vConsulta = "SELECT id
                FROM atributo_atributo
                WHERE projetos_versao_banco_banco_id_INT = {$idPVBB} ";
              
            $vConsultaWhere = "";
            
            if(strlen($pIdAtributoProd))
                $vConsultaWhere .= " AND producao_atributo_id_INT = {$pIdAtributoProd} ";
            else
                $vConsultaWhere .= " AND producao_atributo_id_INT IS NULL ";
            
            $vConsulta .= " ".$vConsultaWhere;
            if($db == null)
            $db = new Database();
            $db->query($vConsulta);
            return $db->getPrimeiraTuplaDoResultSet("id");
        }
        
        
        public static function getIdAtributoAtributoHmg($idPVBB, $pIdAtributoProd, $db = null){
            $vConsulta = "SELECT id
                FROM atributo_atributo
                WHERE projetos_versao_banco_banco_id_INT = {$idPVBB} ";
              
            $vConsultaWhere = "";
            
            if(strlen($pIdAtributoProd))
                $vConsultaWhere .= " AND homologacao_atributo_id_INT = {$pIdAtributoProd} ";
            else
                $vConsultaWhere .= " AND homologacao_atributo_id_INT IS NULL ";
            
            $vConsulta .= " ".$vConsultaWhere;
            if($db == null)
            $db = new Database();
            $db->query($vConsulta);
            return $db->getPrimeiraTuplaDoResultSet("id");
        }
        
        public static function getIdAtributoAtributo($idPVBB, $pIdAtributoHom, $pIdAtributoProd, $db = null){
            $vConsulta = "SELECT id
                FROM atributo_atributo
                WHERE projetos_versao_banco_banco_id_INT = {$idPVBB} ";
              
            $vConsultaWhere = "";
            
            if(strlen($pIdAtributoHom))
                $vConsultaWhere .= " AND homologacao_atributo_id_INT = {$pIdAtributoHom} ";
            else
                $vConsultaWhere .= " AND homologacao_atributo_id_INT IS NULL ";
            
            if(strlen($pIdAtributoProd)){
                if(strlen($vConsultaWhere))
                    $vConsultaWhere .= " AND ";
                $vConsultaWhere .= "producao_atributo_id_INT = {$pIdAtributoProd} ";
            }
            else{
                if(strlen($vConsultaWhere))
                    $vConsultaWhere .= " AND ";
                $vConsultaWhere .= " producao_atributo_id_INT IS NULL ";
            }
            $vConsulta .= " ".$vConsultaWhere;
            if($db == null)
            $db = new Database();
            $db->query($vConsulta);
            return $db->getPrimeiraTuplaDoResultSet("id");
        }
        
        public static function getObjAtributoAtributo($idVPBB, $pIdAtributoHom, $pIdAtributoProd, $db = null){
           
            $vIdAtributoAtributo = EXTDAO_Atributo_atributo::getIdAtributoAtributo($idVPBB, $pIdAtributoHom, $pIdAtributoProd, $db);
            if(strlen($vIdAtributoAtributo )){
                $vObjAtributoAtributo = new EXTDAO_Atributo_atributo();
                $vObjAtributoAtributo->select($vIdAtributoAtributo);
                return $vObjAtributoAtributo;
            } 
            return null;
        }
        
        public function getComboBoxAllProducao_atributo($objArgumentos){

		$objArgumentos->nome="producao_atributo_id_INT";
		$objArgumentos->id="producao_atributo_id_INT";
		$objArgumentos->valueReplaceId=false;
                $vIdTabelaTabela = Helper::GET(Param_Get::TABELA_TABELA);
                
                $vObjTabelaTabela = new EXTDAO_Tabela_tabela();
                $vObjTabelaTabela->select($vIdTabelaTabela);
                $vIdProducaoTabela = $vObjTabelaTabela->getProducao_tabela_id_INT();

                $objArgumentos->strFiltro = " tp.tabela_id_INT = {$vIdProducaoTabela}";
                
		$vRetorno = $this->getFkObjProducao_atributo()->getComboBox($objArgumentos);
                $objArgumentos->strFiltro = "";
                return $vRetorno;

	}
        
        public static function getListIdAtributoAtributoDaTabelaDeHomologacao(
                $idPVBB
                , $pIdTabelaHomologacao
                , $database = null){
            if($database==null)
            $database = new Database();
            $database->query("SELECT aa.id 
                FROM atributo_atributo aa JOIN atributo a ON aa.homologacao_atributo_id_INT = a.id
                WHERE a.tabela_id_INT = $pIdTabelaHomologacao 
                    AND aa.projetos_versao_banco_banco_id_INT = {$idPVBB}");            
            return Helper::getResultSetToArrayDeUmCampo($database->result);
            
        }
        
        public static function getListIdAtributoAtributoApenasDaTabelaDeProducao(
            $idPVBB, $pIdTabelaProducao, $database = null){
            if($database == null)
                $database = new Database();
            $q = "SELECT aa.id 
                FROM atributo_atributo aa JOIN atributo a ON aa.producao_atributo_id_INT = a.id
                WHERE a.tabela_id_INT = $pIdTabelaProducao
                    AND aa.homologacao_atributo_id_INT IS NULL
                    AND aa.projetos_versao_banco_banco_id_INT = {$idPVBB}";
            
            $database->query($q);
                    
            return Helper::getResultSetToArrayDeUmCampo($database->result);
            
        }
        
        public static function getListIdAtributoAtributoDaTabelaDeProducao(
            $idPVBB, $pIdTabelaProducao, $database = null){
            if($database == null)
                $database = new Database();
            $q = "SELECT aa.id 
                FROM atributo_atributo aa JOIN atributo a ON aa.producao_atributo_id_INT = a.id
                WHERE a.tabela_id_INT = $pIdTabelaProducao
                    AND aa.projetos_versao_banco_banco_id_INT = {$idPVBB}";
//            echo $q;
//            exit();
            $database->query($q);
                    
            return Helper::getResultSetToArrayDeUmCampo($database->result);
            
        }
        
        public static function getListIdAtributoDaAtributoDeHomologacao($idPVBB, $pIdAtributoHomologacao){
            $database = new Database();
            $database->query("SELECT DISTINCT a.id 
                FROM atributo_atributo aa JOIN atributo a ON aa.homologacao_atributo_id_INT = a.id
                WHERE a.atributo_id_INT = $pIdAtributoHomologacao 
                    AND aa.projetos_versao_banco_banco_id_INT = {$idPVBB}"); 
            return Helper::getResultSetToArrayDeUmCampo($database->result);
            
        }
        
        public static function getListIdAtributoAtributoDaAtributoDeProducao($idPVBB, $pIdAtributoProducao){
            $database = new Database();
            $database->query("SELECT aa.id 
                FROM atributo_atributo aa JOIN atributo a ON aa.producao_atributo_id_INT = a.id
                WHERE a.atributo_id_INT = $pIdAtributoProducao
                    AND aa.projetos_versao_banco_banco_id_INT = {$idPVBB}");            
            return Helper::getResultSetToArrayDeUmCampo($database->result);
            
        }
        
        
        
        public static function getListIdAtributoAtributo($idTT, Database $db = null){
            if($db == null)
                $db = new Database();
            $q = "SELECT aa.id 
                FROM atributo_atributo aa
                WHERE aa.tabela_tabela_id_INT = $idTT";
             
            $db->query($q); 
            return Helper::getResultSetToArrayDeUmCampo($db->result);
            
        }
        
        public function getIdAtributoProducao(){
            $vObjAtributoProd = $this->getObjAtributoProducao();
            if($vObjAtributoProd != null){
                return $vObjAtributoProd->getAtributo_id_INT();
            }
            return null;
        }
        
        public function getIdAtributoHomologacao(){
            $vObjAtributoHom = $this->getObjAtributoHomologacao();
            if($vObjAtributoHom != null){
                return $vObjAtributoHom->getAtributo_id_INT();
            }
            return null;
        }
        
        public static function getVetorAtributoHomologacao($idPVBB, $idTT, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.id, a.nome
FROM atributo a JOIN atributo_atributo aa ON aa.homologacao_atributo_id_INT = a.id 
    JOIN tabela_tabela tt ON tt.homologacao_tabela_id_INT = a.tabela_id_INT
    JOIN projetos_versao pv ON aa.projetos_versao_id_INT = pv.id 
    JOIN projetos_versao_banco_banco pvbb ON pv.id = pvbb.projetos_versao_id_INT 
    JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
WHERE pvbb.id= $idPVBB AND 
        a.banco_id_INT = bb.homologacao_banco_id_INT AND
        tt.id = {$idTT}");
                
                
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public static function getVetorAtributoProducao($idPVBB, $idTT, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.id, a.nome
FROM atributo a JOIN atributo_atributo aa ON aa.producao_atributo_id_INT = a.id 
    JOIN tabela_tabela tt ON tt.producao_tabela_id_INT = a.tabela_id_INT
    JOIN projetos_versao pv ON aa.projetos_versao_id_INT = pv.id 
    JOIN projetos_versao_banco_banco pvbb ON pv.id = pvbb.projetos_versao_id_INT 
    JOIN banco_banco bb ON pvbb.banco_banco_id_INT = bb.id
WHERE pvbb.id= $idPVBB AND  
        a.banco_id_INT = bb.producao_banco_id_INT AND
        tt.id = {$idTT}");
                
                
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public function salvaListaVerificada(){
            $nextAction = "lists_gerencia_atributo_atributo";
            $varGet = Param_Get::getIdentificadorProjetosVersao();
            $urlSuccess = Helper::getUrlAction($nextAction, Helper::POST("id")).$varGet;
            
            $numeroRegistros = Helper::POSTGET("numeroRegs");
            
            $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
            $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
            $vObjBancoBanco = new EXTDAO_Banco_banco();
            $vIdBancoBanco = $vObjBancoBanco->getIdBancoBanco($idPVBB );
            $vObjBancoBanco->select($vIdBancoBanco);
                        
            for($i=1; $i <= $numeroRegistros; $i++){
                $vStatus = $this->formatarDados($_POST["status_verificacao_BOOLEAN{$i}"]); 
                $vId = $_POST["id{$i}"];
                if(!strlen($vId)) break;
                $this->select($vId);
                $this->setStatus_verificacao_BOOLEAN((strlen($vStatus) && $vStatus == "1") ? "1" : "0");
                $this->formatarParaSQL();
                $this->update($this->getId());
            }

            return array("location: {$urlSuccess}&{$varGet}&msgSucesso=Os atributos foram marcados como verificados.");

        }
        
        public static function existeRegistroNoEstado($pIdTipoOperacaoAtualizacaoBanco, $pIdTabelaTabela){
            $consulta = "SELECT id
                FROM atributo_atributo
                WHERE tabela_tabela_id_INT = $pIdTabelaTabela 
            AND tipo_operacao_atualizacao_banco_id_INT = $pIdTipoOperacaoAtualizacaoBanco
            LIMIT 0,1";
            $db= new Database();
            $db->query($consulta);
            $valor = $db->getPrimeiraTuplaDoResultSet(0);
            if($valor == null || ! strlen($valor)){
                return false;
            }
            else return true;
        }
    
        public static function getIdentificador($idAA){
            $consulta = "SELECT 
                    CONCAT(
                        IF(a1.nome IS NULL, 'null', a1.nome) ,  
                        '@',
                        IF(t1.nome IS NULL, 'null', t1.nome) ,  
                        '-',
                        IF(a2.nome IS NULL, 'null', a2.nome) ,  
                        '@',
                        IF(t2.nome IS NULL, 'null', t2.nome) 
                    ) tag
                FROM atributo_atributo aa
                    LEFT JOIN atributo a1
                        ON aa.homologacao_atributo_id_INT = a1.id
                    JOIN tabela t1
                        ON a1.tabela_id_INT = t1.id
                    LEFT JOIN atributo a2
                        ON aa.producao_atributo_id_INT=  a2.id
                    JOIN tabela t2
                        ON a2.tabela_id_INT = t2.id
                WHERE aa.id = {$idAA}
                LIMIT 0,1";
            $db= new Database();
            $db->query($consulta);
            return $db->getPrimeiraTuplaDoResultSet(0);
            
        }
        private static function simulaAtualizacaoFkHmgParaProd(
            $objAA, $idTipoOperacaoRealizada, 
            $idPVBancoProd, $idBancoPrd, $prefixoProd, 
            $db = null){
            
            if($db == null){
                $db =new Database();
            }
            $objAtributoHmg = $objAA->getObjAtributoHomologacao();
            $objAtributoPrd = $objAA->getObjAtributoProducao();
            switch ($idTipoOperacaoRealizada) {
              case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
                  $objAtributoFk = $objAtributoHmg->getObjFKAtributo();
                  if($objAtributoFk != null){
                      $objTabelaFk =  $objAtributoHmg->getObjFKTabela();
                      $nomeChaveFk = EXTDAO_Script_comando_banco_cache::getNomeChavePeloIdAtributo($objAtributoHmg->getId(), $db);
                      if(!strlen($nomeChaveFk)){
                          Helper::imprimirMensagem(
                              "A chave fk criada deveria possuir o nome na cache de script comando. O atributo em questÃ£o que possui a fk Ã©: ".$objAtributoHmg->getId(), 
                              MENSAGEM_ERRO);
                          exit();
                      }
                      
                      
                      $tabelaFk = $objTabelaFk->getNome();
                      if(strlen($prefixoProd)){
                        $tabelaFk = $prefixoProd.$tabelaFk;
                      }
                      //Procurando tabela equivalente
                      $idTabelaFkPrd = EXTDAO_Tabela::getIdTabela($idPVBancoProd, $idBancoPrd, $tabelaFk, $db);
                      
                      if(!strlen($idTabelaFkPrd)){
                          throw new Exception("A tabela $tabelaFk jÃ¡ deveria ter sido mapeada no banco $idBancoPrd para a versÃ£o de projeto $idPVBancoProd");
                      }
                      $atributoFk = $objAtributoFk->getNome();
                      $idAtributoFkPrd = EXTDAO_Atributo::getIdAtributo($atributoFk, $idTabelaFkPrd, $db);
                      if(!strlen($idAtributoFkPrd)){
                          $objAtributoFkPrd = new EXTDAO_Atributo($db);
                          $objAtributoFkPrd->select($objAtributoFk->getId());
                          $objAtributoFkPrd->setId(null);
                          $objAtributoFkPrd->setProjetos_versao_id_INT($idPVBancoProd);
                          $objAtributoFkPrd->setTabela_id_INT($idTabelaFkPrd);
                          $objAtributoFkPrd->setFk_atributo_id_INT(null);
                          $objAtributoFkPrd->setFk_nome(null);
                          $objAtributoFkPrd->setFk_tabela_id_INT(null);
                          $objAtributoFkPrd->formatarParaSQL();
                          $objAtributoFkPrd->insert();
                          $idAtributoFkPrd = $objAtributoFkPrd->getIdDoUltimoRegistroInserido();
                          
                      }
                      $objAtributoPrd->setFk_nome($nomeChaveFk);
                      $objAtributoPrd->setFk_atributo_id_INT($idAtributoFkPrd);
//                      $objAtributoPrd->setFk_tabela_id_INT($idTabelaFkPrd);
                      $objAtributoPrd->formatarParaSQL();
                      $objAtributoPrd->update($objAtributoPrd->getId());
                  }
              case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
                    $objAtributoFk = $objAtributoHmg->getObjFKAtributo();

                  if($objAtributoFk != null){
                      $objTabelaFk =  $objAtributoHmg->getObjFKTabela();
                      $tabelaFk = $objTabelaFk->getNome();
                      if(strlen($prefixoProd)){
                        $tabelaFk = $prefixoProd.$tabelaFk;
                      }
                      $nomeChaveFk = EXTDAO_Script_comando_banco_cache::getNomeChavePeloIdAtributo(
                          $objAtributoHmg->getId(), $db);
                      
                      //Procurando tabela equivalente
                      $idTabelaFkPrd = EXTDAO_Tabela::getIdTabela(
                          $idPVBancoProd, $idBancoPrd, $tabelaFk, $db);
                      
                      if(!strlen($idTabelaFkPrd)){
                          throw new Exception("A tabela $tabelaFk jÃ¡ deveria ter sido mapeada no banco $idBancoPrd para a versÃ£o de projeto $idPVBancoProd");
                      }
                      $atributoFk = $objAtributoFk->getNome();
                      $idAtributoFkPrd = EXTDAO_Atributo::getIdAtributo($atributoFk, $idTabelaFkPrd, $db);
                      if(!strlen($idAtributoFkPrd)){
                          $objAtributoFkPrd = new EXTDAO_Atributo($db);
                          $objAtributoFkPrd->select($objAtributoFk->getId());
                          $objAtributoFkPrd->setId(null);
                          $objAtributoFkPrd->setProjetos_versao_id_INT($idPVBancoProd);
                          $objAtributoFkPrd->setTabela_id_INT($idTabelaFkPrd);
                          $objAtributoFkPrd->setFk_atributo_id_INT(null);
                          $objAtributoFkPrd->setFk_nome(null);
                          $objAtributoFkPrd->setFk_tabela_id_INT(null);
                          $objAtributoFkPrd->formatarParaSQL();
                          $objAtributoFkPrd->insert();
                          $idAtributoFkPrd = $objAtributoFkPrd->getIdDoUltimoRegistroInserido();
                          
                      }
                      //se a chave tiver sido criada a pouco tempo
                      if(strlen($nomeChaveFk)){
                        $objAtributoPrd->setFk_nome($nomeChaveFk);    
                      }
                      $objAtributoPrd->setFk_atributo_id_INT($idAtributoFkPrd);
                      $objAtributoPrd->setFk_tabela_id_INT($idTabelaFkPrd);
                      $objAtributoPrd->formatarParaSQL();
                      $objAtributoPrd->update($objAtributoPrd->getId());
                  }
                  break;
              default:
                  break;
            }
        }
      
        public function getObjTabelaTabela(){
            if($this->getTabela_tabela_id_INT() != null){
                $obj = new EXTDAO_Tabela_tabela();
                $obj->select($this->getTabela_tabela_id_INT());
                return $obj;
            }
            return null;
        }  
    //ATENÃ‡ÃƒO esse metodo pode deletar ou editar o registro $idTT passado na entrada
    public static function simulaAtualizacaoHmgParaProd($idAA, $tipoAnaliseProjeto,  $db = null){
        if($db == null) $db = new Database();
        
        $objAA = new EXTDAO_Atributo_atributo();
        $objAA->select($idAA);
        
        $objTT = $objAA->getObjTabelaTabela();
        
        $tipoOperacao = $objAA->getTipo_operacao_atualizacao_banco_id_INT();
        if($tipoOperacao == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE)
            return;
        $idTabelaHmg = $objTT->getHomologacao_tabela_id_INT();
        $idTabelaPrd =$objTT->getProducao_tabela_id_INT();
        //TODO retirar linha abaixo
        //$trunk = new Trunk();
        $trunk = Trunk::factory();
        //$trunk = new Trunk();
        if($tipoAnaliseProjeto != EXTDAO_Tipo_analise_projeto::SINCRONIZACAO)
            $idPVBBAtributoProducao = $trunk->getPVBBDoBancoDeDadosDeProducao($objAA->getProjetos_versao_banco_banco_id_INT());
        else $idPVBBAtributoProducao = $objAA->getProjetos_versao_banco_banco_id_INT ();
        
        $idBancoProd = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($idPVBBAtributoProducao, $db);
        
//        $objBancoProducao = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBAtributoProducao, $db);
        
        $idPVAtributoProducao = $trunk->getPVDoBancoDeDadosDeProducao($objAA->getProjetos_versao_id_INT());
        $idPVAtributoHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($objAA->getProjetos_versao_id_INT());
        
        $prefixoProd = null;
        
        $tipoAnalise = $trunk->getTipoAnaliseDoProjetosVersao( $objAA->getProjetos_versao_id_INT());
        //$objPV = new EXTDAO_Projetos_versao();
        switch ($tipoAnalise) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                $prefixoProd =  "__";
                break;
            default:
                break;
        } 
        
        
        $objAtributoHmg= $objAA->getObjAtributoHomologacao();
        $objAtributoPrd = $objAA->getObjAtributoProducao();
        
        
//        if($objAA->getId() == '45670'){
//            echo "ACHOU<br/>";
//            exit();
//        }
        switch ($tipoOperacao) {
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT:
//                echo "entrou 1<br/>";
                if($objAtributoHmg != null ){
//                    echo "ACHOU 1<br/>";
                    $atributoHmg = $objAtributoHmg->getNome();
                    //verifica se o atributo de produÃ§Ã£o fora criado.
//                    echo "saiu 1.1<br/>";    
                    $idAtributoPrd = EXTDAO_Atributo::getIdAtributo($atributoHmg, $idTabelaPrd, $db);
//                    echo "saiu 1.2<br/>";    
                    //faz uma copia da Atributo do banco hmg em prd
                    $objAtributoHmg->setId(null);
                    $objAtributoHmg->setBanco_id_INT($idBancoProd);
                    $objAtributoHmg->setProjetos_versao_id_INT($idPVAtributoProducao);
                    $objAtributoHmg->setTabela_id_INT($idTabelaPrd) ;
                    $objAtributoHmg->setFk_atributo_id_INT(null);
                    $objAtributoHmg->setFk_tabela_id_INT(null);
                    $nomeAtributoProducao = $objAtributoHmg->getNome();    
                
                    $objAtributoHmg->setNome($nomeAtributoProducao);
                    $objAtributoHmg->formatarParaSQL();
                    if(strlen($idAtributoPrd)){
                        $objAtributoHmg->update($idAtributoPrd);
                        $idAtributo = $idAtributoPrd;
                    } else {
                        $objAtributoHmg->insert();
                        $idAtributo = $objAtributoHmg->getIdDoUltimoRegistroInserido();
                    }
                    
                    
                    $objAA->setProducao_atributo_id_INT($idAtributo);
                    //TODO descomentar linha abaixo
                    $objAA->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION);
                    
                    $objAA->formatarParaSQL();
                    $objAA->update($objAA->getId());
//                    echo "saiu 1.3<br/>";    
                    EXTDAO_Atributo_atributo::simulaAtualizacaoFkHmgParaProd(
                        $objAA, $tipoOperacao, $idPVAtributoProducao, 
                        $idBancoProd, $prefixoProd, $db);
//                    echo "saiu 1.4<br/>";    
                }
//                echo "saiu 1<br/>";
                break;
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE:
//                echo "entrou 2<br/>";
                if($objAtributoPrd != null){
//                    echo "ACHOU 2<br/>";
                    $objAtributoPrd->delete($objAtributoPrd->getId());
                    $objAA->delete($idAA);
                }
                break;
            case EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT:
//                echo "entrou 3<br/>";
                if($objAtributoHmg != null && $objAtributoPrd != null){
//                    echo "ACHOU 3<br/>";
                     //faz uma copia da Atributo do banco hmg em prd
                    $objAtributoHmg->setId(null);
                    $objAtributoHmg->setBanco_id_INT($idBancoProd);
                    $objAtributoHmg->setProjetos_versao_id_INT($idPVAtributoProducao);
                    $objAtributoHmg->setTabela_id_INT($idTabelaPrd);
                    $nomeAtributoProducao = $objAtributoHmg->getNome();    
                    $objAtributoHmg->setNome($nomeAtributoProducao);
                    $objAtributoHmg->setFk_atributo_id_INT(null);
                    $objAtributoHmg->setFk_tabela_id_INT(null);
                    $objAtributoHmg->formatarParaSQL();
                    $objAtributoHmg->update($objAtributoPrd->getId());
                    //TODO descomentar linha abaixo
                    $objAA->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION);
                    $objAA->formatarParaSQL();
                    $objAA->update($idAA);
                    $objAA->select($idAA);
                    if(strlen($objAtributoHmg->getFk_atributo_id_INT() )) {
                        EXTDAO_Atributo_atributo::simulaAtualizacaoFkHmgParaProd(
                            $objAA, $tipoOperacao, $idPVAtributoProducao, 
                            $idBancoProd, $prefixoProd, $db);    
                    } 
                    
                }
                break;

            default:
                break;
        }
       
    }
    
	}

    
