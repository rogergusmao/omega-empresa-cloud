<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Monitora_tela_mobile_para_web
    * NOME DA CLASSE DAO: DAO_Monitora_tela_mobile_para_web
    * DATA DE GERAÇÃO:    25.10.2013
    * ARQUIVO:            EXTDAO_Monitora_tela_mobile_para_web.php
    * TABELA MYSQL:       monitora_tela_mobile_para_web
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Monitora_tela_mobile_para_web extends DAO_Monitora_tela_mobile_para_web
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Monitora_tela_mobile_para_web";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_largura_tela_INT = "Largura da Tela";
			$this->label_altura_tela_INT = "Altura da Tela";
			$this->label_teclado_ativo_BOOLEAN = "Teclado Está Ativo?";
			$this->label_programa_aberto_BOOLEAN = "O Programa Está Aberto?";
			$this->label_data_ocorrencia_DATETIME = "Data de Ocorrência da Operação";
			$this->label_sequencia_operacao_INT = "Sequência da Operação";
			$this->label_mobile_identificador_id_INT = "Identificador do Telefone";
			$this->label_tipo_operacao_monitora_tela_id_INT = "Tipo de Operação de Monitoramento";
			$this->label_imagem_ARQUIVO = "Printscreen";


        }

        public function setDiretorios(){

			$this->diretorio_imagem_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final


        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Monitora_tela_mobile_para_web();

        }

	}

    
