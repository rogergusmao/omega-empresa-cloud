<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_permissao
    * NOME DA CLASSE DAO: DAO_Tipo_permissao
    * DATA DE GERAÇÃO:    19.10.2013
    * ARQUIVO:            EXTDAO_Tipo_permissao.php
    * TABELA MYSQL:       tipo_permissao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_permissao extends DAO_Tipo_permissao
    {
        public static $MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA = 1;
        public static $ACESSO_A_FUNCIONALIDADE_DO_SISTEMA = 3;
        public static $ACESSO_A_TELA = 2;
        public static $MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS = 4;
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_permissao";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }
        
        

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_permissao();

        }

	}

    
