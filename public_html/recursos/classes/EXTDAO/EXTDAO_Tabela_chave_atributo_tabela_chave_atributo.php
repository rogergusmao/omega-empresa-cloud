<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tabela_chave_atributo_tabela_chave_atributo
    * NOME DA CLASSE DAO: DAO_Tabela_chave_atributo_tabela_chave_atributo
    * DATA DE GERAÇÃO:    20.04.2013
    * ARQUIVO:            EXTDAO_Tabela_chave_atributo_tabela_chave_atributo.php
    * TABELA MYSQL:       tabela_chave_atributo_tabela_chave_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tabela_chave_atributo_tabela_chave_atributo extends DAO_Tabela_chave_atributo_tabela_chave_atributo
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tabela_chave_atributo_tabela_chave_atributo";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_homologacao_tabela_chave_atributo_id_INT = "Atributo da Tabela Chave de Homologação";
			$this->label_producao_tabela_chave_atributo_id_INT = "Atributo da Tabela Chave de Produção";
			$this->label_tabela_chave_tabela_chave_id_INT = "Relacionamento Entre A Chave da Tabela de Homologação E Produção";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();

        }

        public static function getListaId($idPVBB, $pIdTabelaChaveTabelaChave, $db = null){
           
            $consulta = "SELECT id
                FROM tabela_chave_atributo_tabela_chave_atributo
                WHERE projetos_versao_banco_banco_id_INT = $idPVBB
                    AND tabela_chave_tabela_chave_id_INT = {$pIdTabelaChaveTabelaChave}";
            if($db == null) $db = new Database();
            
            $db->query($consulta);    
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        
        public static function getIdTabelaChaveAtributoTabelaChaveAtributo($idPVBB, $pIdTabelaChaveAtributoHom, $pIdTabelaChaveAtributoProd, $db = null){
           
            $consulta = "SELECT id
                FROM tabela_chave_atributo_tabela_chave_atributo
                WHERE projetos_versao_banco_banco_id_INT = $idPVBB
                    AND homologacao_tabela_atributo_id_INT = {$pIdTabelaChaveAtributoHom}
                    AND producao_tabela_atributo_id_INT = {$pIdTabelaChaveAtributoProd}";
            if($db == null)
            $db = new Database();
            $db->query($consulta);    
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public function getIdAtributoAtributo($idPVBB){
            if(strlen($this->getHomologacao_tabela_chave_atributo_id_INT()) &&
                    strlen($this->getProducao_tabela_chave_atributo_id_INT())){
                $consulta = "SELECT DISTINCT aa.id
                FROM tabela_chave_atributo_tabela_chave_atributo tcatca
                    JOIN tabela_chave_atributo tcaHom
                        ON tcaHom.id = tcatca.homologacao_tabela_chave_atributo_id_INT
                    JOIN tabela_chave_atributo tcaProd
                        ON tcaProd.id = tcatca.producao_tabela_chave_atributo_id_INT
                    JOIN atributo aHom 
                        ON aHom.id = tcaHom.atributo_id_INT
                    JOIN atributo aProd
                        ON aProd.id = tcaProd.atributo_id_INT
                    JOIN atributo_atributo aa
                        ON aa.homologacao_atributo_id_INT = aHom.id
                            AND aa.producao_atributo_id_INT = aProd.id
                WHERE aProd.id != aHom.id 
                    AND tcatca.projetos_versao_banco_banco_id_INT = $idPVBB
                    AND tcatca.id = {$this->getId()}
                    ";
//                    AND tcatca.producao_tabela_chave_atributo_id_INT = {$this->getProducao_tabela_chave_atributo_id_INT()}
//                    AND tcatca.homologacao_tabela_chave_atributo_id_INT = {$this->getHomologacao_tabela_chave_atributo_id_INT()}"
                $db = new Database();
                $db->query($consulta);
                return $db->getPrimeiraTuplaDoResultSet(0);
            }
            return null;
        }
        
	}

    
