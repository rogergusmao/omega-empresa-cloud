<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_versao_log_erro_script
    * NOME DA CLASSE DAO: DAO_Projetos_versao_log_erro_script
    * DATA DE GERAÇÃO:    01.05.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_log_erro_script.php
    * TABELA MYSQL:       projetos_versao_log_erro_script
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos_versao_log_erro_script extends DAO_Projetos_versao_log_erro_script
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_versao_log_erro_script";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_script_comando_banco_id_INT = "Script do Comando de Banco";
			$this->label_projetos_versao_log_erro_id_INT = "Log de Erro da Versão de Projetos";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_versao_log_erro_script();

        }

        public static function getConsultaSQL($idPVLE){
            $q = "SELECT scb.consulta
                FROM projetos_versao_log_erro_script pvles
                    JOIN script_comando_banco scb 
                        ON pvles.script_comando_banco_id_INT = scb.id
                WHERE pvles.projetos_versao_log_erro_id_INT = {$idPVLE}";
                $db = new Database();
                $db->query($q);
                return Helper::getResultSetToPrimeiroResultado($db->result);
            
        }
        
        public static function getListaIdPVLE($idPVBB, $idTipoScriptBanco, $db = null){
            $q = "SELECT pvle.id
                FROM projetos_versao_log_erro_script pvles
                    JOIN projetos_versao_log_erro pvle
                        ON pvles.projetos_versao_log_erro_id_INT = pvle.id
                    JOIN script_comando_banco scb 
                        ON scb.id = pvles.script_comando_banco_id_INT
                    JOIN script_tabela_tabela stt 
                        ON stt.id = scb.script_tabela_tabela_id_INT
                    JOIN script_projetos_versao_banco_banco spvbb
                        ON spvbb.id = stt.script_projetos_versao_banco_banco_id_INT
                WHERE pvle.projetos_versao_banco_banco_id_INT = {$idPVBB}
                    AND spvbb.tipo_script_banco_id_INT = $idTipoScriptBanco ";
                if($db == null)
                $db = new Database();
                $db->query($q);
                return Helper::getResultSetToArrayDeUmCampo($db->result);
            
        }
        
	}

    
