<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Sistema
 * NOME DA CLASSE DAO: DAO_Sistema
 * DATA DE GERAÇÃO:    15.03.2013
 * ARQUIVO:            EXTDAO_Sistema.php
 * TABELA MYSQL:       sistema
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Sistema extends DAO_Sistema {

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Sistema";

    }

    public function setLabels() {

        $this->label_id = "Id";
        $this->label_nome = "Nome";
        $this->label_projetos_id_INT = "Projeto";
        $this->label_manutencao_BOOLEAN = "Está em Manutenção?";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Sistema();
    }

    
    
        public static function getListaIdentificadorNo($idProjetos){
            $q = "SELECT id, nome
                FROM sistema
                WHERE projetos_id_INT = $idProjetos 
                ORDER BY nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public static function getSistemaPrototipoDoProjeto($idProjetos){
            $q = "SELECT id, nome
                FROM sistema
                WHERE projetos_id_INT = $idProjetos AND prototipo_BOOLEAN = '1'
                ORDER BY nome";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
}

