<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Operacao_de_sistema_mobile
    * NOME DA CLASSE DAO: DAO_Operacao_de_sistema_mobile
    * DATA DE GERAÇÃO:    13.03.2013
    * ARQUIVO:            EXTDAO_Operacao_de_sistema_mobile.php
    * TABELA MYSQL:       operacao_de_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Operacao_sistema_mobile extends DAO_Operacao_sistema_mobile
    {
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Operacao_sistema_mobile";

 

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_tipo_operacao_sistema_id_INT = "Tipo de Operação";
			$this->label_estado_operacao_sistema_mobile_id_INT = "Estado Ta Operação";
			$this->label_mobile_identificador_id_INT = "Identificador do Telefone";
			$this->label_data_abertura_DATETIME = "Data de Abertura";
			$this->label_data_processamento_DATETIME = "Data de Processamento";
			$this->label_data_conclusao_DATETIME = "Data de Conclusão";


        }

        public static function addOperacaoMonitorarTela(){
            $objOSM = new EXTDAO_Operacao_sistema_mobile();
            $objOSM->setByPost(1);
            $objOSM->setMobile_identificador_id_INT(Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR));
            $objOSM->setTipo_operacao_sistema_id_INT(EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_MONITORAR_TELA);
            $objOSM->setData_abertura_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objOSM->setEstado_operacao_sistema_mobile_id_INT(EXTDAO_Estado_operacao_sistema_mobile::AGUARDANDO);
            $objOSM->formatarParaSQL();

            $objOSM->insert();
            $objOSM->selectUltimoRegistroInserido();
            return array("location: popup.php?tipo=pages&page=monitorar_tela&".Param_Get::ID_OPERACAO_SISTEMA_MOBILE."=".$objOSM->getId());
            
        }
        
        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Operacao_sistema_mobile();

        }
        
        public static function getIdentificadorDoPrototipoDaVersaoDoMobile($idOSM, $db = null){
            if($db == null)
                $db = new Database();
            $db->query("SELECT pvbb.projetos_versao_id_INT, 
	bb.homologacao_banco_id_INT
FROM
	operacao_sistema_mobile osm
	JOIN mobile_identificador mi 
		ON osm.mobile_identificador_id_INT = mi.id

	JOIN sistema_projetos_versao_sistema_produto_mobile spvspm 
		ON mi.sistema_projetos_versao_sistema_produto_mobile_id_INT = spvspm.id

	JOIN sistema_projetos_versao_produto spvp 
		ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id 

	JOIN projetos_versao_banco_banco pvbb 
		ON pvbb.id = spvp.prototipo_projetos_versao_banco_banco_id_INT

	JOIN banco_banco bb 
		ON pvbb.banco_banco_id_INT = bb.id
WHERE osm.id =$idOSM
");
            return Helper::getResultSetToMatriz($db->result);
            
        }
        
        public static function insereOperacaoSistemaMobile($idMI, $idTI, $estado = EXTDAO_Estado_operacao_sistema_mobile::AGUARDANDO){
            $obj = new EXTDAO_Operacao_sistema_mobile();
            $obj->setTipo_operacao_sistema_id_INT($idTI);
            $obj->setData_abertura_DATETIME(Helper::getDiaEHoraAtualSQL());
            $obj->setEstado_operacao_sistema_mobile_id_INT($estado);
            $obj->setMobile_identificador_id_INT($idMI);
            
            $obj->formatarParaSQL();
            $obj->insert();

            $id = $obj->getIdDoUltimoRegistroInserido();

            switch($idTI){

                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_COMPARACAO_BANCO_SQLITE_MOBILE:
                case EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_DOWNLOAD_BANCO_SQLITE:

                    $objAux = new EXTDAO_Operacao_download_banco_mobile();
                    $objAux->setOperacao_sistema_mobile_id_INT($id);
                    $objAux->setPath_script_sql_banco(Helper::getNomeAleatorio("sqlite", ".db"));
                    $objAux->formatarParaSQL();
                    $objAux->insert();
                    break;

                default:
                    break;

            }

            return $id;
        }
        
        
        public function imprimirMensagemStatus($idOSM){
            $obj = new EXTDAO_Operacao_sistema_mobile();
            $obj->select($idOSM);
            $idE = $obj->getEstado_operacao_sistema_mobile_id_INT();
            EXTDAO_Estado_operacao_sistema_mobile::imprimirMensagem($idE);
        }
        
        public static function consultaOperacaoSistemaPorEstado( 
                $pIdMobileIdentificador, 
                $pVetorIdEstado){
            
            $consulta = "SELECT osm.id, osm.tipo_operacao_sistema_id_INT
                        FROM operacao_sistema_mobile osm  ";
            $consulta .= " WHERE osm.mobile_identificador_id_INT = $pIdMobileIdentificador";
            
            if(is_array($pVetorIdEstado)){
                for($i = 0 ; $i < count($pVetorIdEstado); $i++){
                    if($i == 0){
                        $consulta .= " IN ( {$pVetorIdEstado}" ;
                    }
                    else {
                        $consulta .=     ", $pVetorIdEstado";
                    }
                    if($i == count($pVetorIdEstado) - 1){
                        $consulta .= ") ";
                    }
                }
            } elseif(strlen ($pVetorIdEstado)){
                $consulta .= " AND estado_operacao_sistema_mobile_id_INT = {$pVetorIdEstado}";
            }
                            
            $consulta .=     " ORDER BY id LIMIT 0, 1";
            $db = new Database();
            $db->query($consulta);
            return $db->result;
                    
        }
        
        public static function atualizaEstado($pIdSistema, $pIdEstado){
            $obj = new EXTDAO_Operacao_sistema_mobile();
            $obj->select($pIdSistema);
            $obj->setEstado_operacao_sistema_mobile_id_INT($pIdEstado);
            $obj->formatarParaSQL();
            $obj->update($pIdSistema);
            
        }
        
        public static function getIdOperacaoSistemaMobileFilho($idOSM, $nomeTabela){
            $db = new Database();
            $db->query("SELECT id
                FROM {$nomeTabela}
                WHERE operacao_sistema_mobile_id_INT = {$idOSM}");
            return $db->getPrimeiraTuplaDoResultSet(0);
        }

         
        public function cadastraOperacaoSistamMobileFilho($idMI , $idTOS ){
            
            $idOSM = EXTDAO_Operacao_sistema_mobile::insereOperacaoSistemaMobile($idMI, $idTOS);
            Helper::mudarLocation("popup.php?tipo=pages&page=status_operacao_sistema_mobile&".Param_Get::ID_OPERACAO_SISTEMA_MOBILE."=".$idOSM);

        }
}
?>
