<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Diretorio_web
    * NOME DA CLASSE DAO: DAO_Diretorio_web
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Diretorio_web.php
    * TABELA MYSQL:       diretorio_web
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Diretorio_web extends DAO_Diretorio_web
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Diretorio_web";

          

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_diretorioClassesDAO = "Diretório da Classe DAO Web";
			$this->label_diretorioClassesEXTDAO = "Diretório Classe EXTDAO Web";
			$this->label_diretorioForms = "Diretório Forms Web";
			$this->label_diretorioFiltros = "Diretório Filtros Web";
			$this->label_diretorioLists = "Diretório Lists Web";
			$this->label_diretorioAjaxForms = "Diretório Ajax Forms Web";
			$this->label_diretorioAjaxPages = "Diretório Ajax Pages Web";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Diretorio_web();

        }

        
        
        public static function getIdDiretorio($idPVBB, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT id FROM diretorio_web WHERE projetos_versao_banco_banco_id_INT = $idPVBB LIMIT 0,1";
//            echo $q;
//            exit();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
            
        }
        
	}

    
