<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Trunk
    * NOME DA CLASSE DAO: DAO_Trunk
    * DATA DE GERAÇÃO:    07.02.2015
    * ARQUIVO:            EXTDAO_Trunk.php
    * TABELA MYSQL:       trunk
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Trunk extends DAO_Trunk
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Trunk";

           


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_projetos_tipo_conjunto_analise_id_INT = "Tipo Conjunto Análise Projeto";
			$this->label_inicio_DATETIME = "Data Inicial de Versão";
			$this->label_fim_DATETIME = "Data Final da Versão";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Trunk();

        }
        
        
        
        public static function getProjetosVersaoDoTrunk($idProjeto, $idTipoConjuntoAnalise, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT 
                ca.tipo_analise_projeto_id_INT, 
                tca.projetos_versao_id_INT, 
                tca.projetos_versao_banco_banco_id_INT,
                bb.tipo_banco_id_INT
FROM trunk_conjunto_analise tca  
                JOIN projetos_versao_banco_banco pvbb ON pvbb.id = tca.projetos_versao_banco_banco_id_INT
                JOIN banco_banco bb ON bb.id = pvbb.banco_banco_id_INT
		JOIN trunk t ON tca.trunk_id_INT = t.id
		JOIN conjunto_analise ca ON tca.conjunto_analise_id_INT = ca.id
		JOIN projetos_tipo_conjunto_analise ptca ON ptca.tipo_conjunto_analise_id_INT = ca.tipo_conjunto_analise_id_INT
                
WHERE ptca.projetos_id_INT = $idProjeto
    AND ptca.tipo_conjunto_analise_id_INT = $idTipoConjuntoAnalise 
ORDER BY ca.seq_INT ";
            
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public static function getTipoAnalisePorDadosVersaoDoTrunk(
            $idProjeto, 
            $idTipoConjuntoAnalise, 
            $idTipoAnaliseProjeto, 
            $idTipoBanco = null, 
            $db = null){
            
            if($db == null)
                $db = new Database();
            $q = "SELECT 
                tca.projetos_versao_id_INT projetos_versao_id_INT, 
                tca.projetos_versao_banco_banco_id_INT projetos_versao_banco_banco_id_INT
                
FROM trunk_conjunto_analise tca  
                JOIN projetos_versao_banco_banco pvbb ON pvbb.id = tca.projetos_versao_banco_banco_id_INT
                JOIN banco_banco bb ON bb.id = pvbb.banco_banco_id_INT
		JOIN trunk t ON tca.trunk_id_INT = t.id
		JOIN conjunto_analise ca ON tca.conjunto_analise_id_INT = ca.id
		JOIN projetos_tipo_conjunto_analise ptca ON ptca.tipo_conjunto_analise_id_INT = ca.tipo_conjunto_analise_id_INT
                
WHERE ptca.projetos_id_INT = $idProjeto
    AND ptca.tipo_conjunto_analise_id_INT = $idTipoConjuntoAnalise 
    AND ca.tipo_analise_projeto_id_INT  = $idTipoAnaliseProjeto
    AND bb.tipo_banco_id_INT = $idTipoBanco
ORDER BY ca.seq_INT 
LIMIT 0,1";
            
            $db->query($q);
            return Helper::getResultFirstObject($db->result, 0, 1);
        }
        
        public static function getTiposAnaliseDoProjeto($idProjeto, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT DISTINCT ptca.tipo_conjunto_analise_id_INT
FROM trunk_conjunto_analise tca  
		JOIN trunk t ON tca.trunk_id_INT = t.id
		JOIN conjunto_analise ca ON tca.conjunto_analise_id_INT = ca.id
		JOIN projetos_tipo_conjunto_analise ptca ON ptca.tipo_conjunto_analise_id_INT = ca.tipo_conjunto_analise_id_INT
WHERE ptca.projetos_id_INT = $idProjeto");
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }

}

    
