<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_versao_projetos_banco
    * NOME DA CLASSE DAO: DAO_Projetos_versao_projetos_banco
    * DATA DE GERAÇÃO:    29.01.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_projetos_banco.php
    * TABELA MYSQL:       projetos_versao_projetos_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos_versao_projetos_banco extends DAO_Projetos_versao_projetos_banco
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_versao_projetos_banco";

         

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_projetos_banco_id_INT = "Banco do Projeto";
			$this->label_projetos_versao_id_INT = "Versão do Projeto";
			$this->label_arquivo_atualizacao_estrutura = "Arquivo Atualização da Estrutura de Homologação Para Produção";
			$this->label_arquivo_backup_estrutura = "Arquivo de Backup da Estrutura de Produção Para Homologação";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_versao_projetos_banco();

        }

        
        //TODO responsavel por gerar o arquivo contendo a estrutura 
        //para atualizacao do banco de homologacao para producao da respectiva versao de projeto
        public function gerarArquivoUpdateEstrutura(){
            
        }
        
        //TODO responsavel por gerar o arquivo contendo a estrutura 
        //para atualizacao do banco de producao para homologacao da respectiva versao de projeto
        public function gerarArquivoBackupEstrutura(){
            
        }
        
	}

    
