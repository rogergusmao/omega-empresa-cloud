<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_produto_mobile
    * NOME DA CLASSE DAO: DAO_Sistema_produto_mobile
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Sistema_produto_mobile.php
    * TABELA MYSQL:       sistema_produto_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_produto_mobile extends DAO_Sistema_produto_mobile
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_produto_mobile";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_sistema_produto_id_INT = "Produto";
			$this->label_diretorio_android_id_INT = "Diretório do Projeto Android";
			$this->label_diretorio_iphone_id_INT = "Diretório do Projeto Iphone";
			$this->label_diretorio_windows_mobile_id_INT = "Diretório do Projeto Windows Mobile";
			$this->label_sistema_tipo_mobile_id_INT = "Sistema Operacional do Telefone";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_produto_mobile();

        }

        
        
         public static function getListaIdentificadorNo($idSistema){
            $q = "SELECT DISTINCT sp.id, sp.nome
                FROM sistema s JOIN sistema_produto sp ON sp.sistema_id_INT = s.id
                    JOIN sistema_produto_mobile spm ON  sp.id = spm.sistema_produto_id_INT
                WHERE s.id = $idSistema 
                ORDER BY sp.nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public static function getSistemaProdutoMobile($idSistemaProduto){
            $q = "SELECT spm.id
                FROM sistema_produto_mobile spm 
                WHERE spm.sistema_produto_id_INT = $idSistemaProduto ";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
	}

    
