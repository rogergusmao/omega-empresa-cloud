<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_conjunto_analise
    * NOME DA CLASSE DAO: DAO_Tipo_conjunto_analise
    * DATA DE GERAÇÃO:    07.02.2015
    * ARQUIVO:            EXTDAO_Tipo_conjunto_analise.php
    * TABELA MYSQL:       tipo_conjunto_analise
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_conjunto_analise extends DAO_Tipo_conjunto_analise
    {
        const Projeto_Android_Web_Sincronizado = 1;
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_conjunto_analise";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Tipo_conjunto_analise();

        }

	}

    
