<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Conexoes
 * NOME DA CLASSE DAO: DAO_Conexoes
 * DATA DE GERAÇÃO:    09.01.2013
 * ARQUIVO:            EXTDAO_Conexoes.php
 * TABELA MYSQL:       conexoes
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Conexoes extends DAO_Conexoes {

   
        public function __construct($configDAO = null){

            parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Conexoes";

    }

    public function setLabels() {

        $this->label_id = "Id";
        $this->label_nome = "Nome";
        $this->label_hostnameBanco = "Hostname";
        $this->label_usuarioBanco = "Usuário";
        $this->label_senhaBanco = "Senha";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Conexoes();
    }

    public static function getListaIdentificadorNo() {
        $q = "SELECT id, nome
                FROM conexoes
                ORDER BY nome";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }

}

