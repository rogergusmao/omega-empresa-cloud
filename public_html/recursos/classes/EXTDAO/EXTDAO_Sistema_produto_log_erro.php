<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_produto_log_erro
    * NOME DA CLASSE DAO: DAO_Sistema_produto_log_erro
    * DATA DE GERAÃ‡ÃƒO:    08.05.2013
    * ARQUIVO:            EXTDAO_Sistema_produto_log_erro.php
    * TABELA MYSQL:       sistema_produto_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Sistema_produto_log_erro extends DAO_Sistema_produto_log_erro
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_produto_log_erro";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_classe = "Classe";
			$this->label_funcao = "FunÃ§Ã£o";
                        $this->label_linha_INT = "Linha";
                        $this->label_nome_arquivo = "Arquivo";
                        $this->label_identificador_erro = "Identificador erro";
			$this->label_descricao = "DescriÃ§Ã£o";
                        $this->label_stacktrace = "Stacaktrace";
                        $this->label_data_visualizacao_DATETIME = "VisualizaÃ§Ã£o";
                        $this->label_data_ocorrida_DATETIME = "OcorrÃªncia";
                        $this->label_sistema_tipo_log_erro_id_INT = "Tipo de Log";
			$this->label_sistema_projetos_produto_id_INT = "Produto";
                        $this->label_sistema_projetos_versao_produto_id_INT = "VersÃ£o do Produto";
                        $this->label_id_usuario_INT = "UsuÃ¡rio";
			$this->label_id_corporacao_INT = "CorporaÃ§Ã£o";
                        $this->label_sistema_projetos_versao_id_INT = "Vers?o do projeto";
                        $this->label_sistema_log_identificador_id_INT = "";
                        $this->label_mobile_conectado_id_INT = "Identificador Mobile Conectado";
                        


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_produto_log_erro();

        }
        const ESTADO_NAO_VISUALIZADO = 0;
        const ESTADO_ERRO = 1;
        const ESTADO_CORRIGIDA = 2;
        
        public function getEstado(){
            $totSegVisualizacao = $this->getData_visualizacao_DATETIME_UNIX();
            if(strlen($totSegVisualizacao) || $totSegVisualizacao > 0){
                $totSegundosCorrecao = $this->getFkObjSistema_log_identificador()->getData_correcao_DATETIME_UNIX();
                if($totSegVisualizacao != null && $totSegVisualizacao <= $totSegundosCorrecao)
                    return EXTDAO_Sistema_produto_log_erro::ESTADO_CORRIGIDA;
                else return EXTDAO_Sistema_produto_log_erro::ESTADO_ERRO;
            } else{
                return EXTDAO_Sistema_produto_log_erro::ESTADO_NAO_VISUALIZADO;
            }
        }
        
        public function consultaSistemaLogIdentificador(){
            
            
            $db = $this->database;
            
            $q = "SELECT sli.id 
                    FROM sistema_log_identificador sli  
                    WHERE "; 
            
            
            
            //$erro = $this->getSistema_projetos_versao_id_INT().$this->getIdentificador_erro().$this->getDescricao();
            
            $where = " 
                sli.stacktrace = '{$this->getStacktrace()}'
                LIMIT 0, 1";
            
            $db->query($q.$where);
            return $db->getPrimeiraTuplaDoResultSet(0);
            
            
        }

	}

    
