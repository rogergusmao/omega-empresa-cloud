<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Script_tabela_tabela
    * NOME DA CLASSE DAO: DAO_Script_tabela_tabela
    * DATA DE GERAÇÃO:    04.04.2013
    * ARQUIVO:            EXTDAO_Script_tabela_tabela.php
    * TABELA MYSQL:       script_tabela_tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Script_tabela_tabela extends DAO_Script_tabela_tabela
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Script_tabela_tabela";

          
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_is_atualizacao_BOOLEAN = "Atualização do Banco de Homologação Para Produção";
			$this->label_tipo_operacao_atualizacao_banco_id_INT = "Tipo de Operação de Atualização";
			$this->label_tabela_tabela_id_INT = "Relacionamento Entre Tabela de Homologação E Produção";
			$this->label_projetos_versao_banco_banco_id_INT = "Relacionamento Entre Banco de Homologação E Produção";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Script_tabela_tabela();

        }

        
        public static function getListaId($pIdScriptProjetosVersaoBancoBanco){
            $vConsulta = "SELECT id
                FROM script_tabela_tabela
                WHERE script_projetos_versao_banco_banco_id_INT = {$pIdScriptProjetosVersaoBancoBanco}";
            $db = new Database();
            $db->query($vConsulta);
            $vVetor = Helper::getResultSetToArrayDeUmCampo($db->result);
            return count($vVetor) > 0 ? $vVetor : null;
        }
        
        
        
        
	}

    
