<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_banco
    * NOME DA CLASSE DAO: DAO_Tipo_banco
    * DATA DE GERAÇÃO:    08.01.2013
    * ARQUIVO:            EXTDAO_Tipo_banco.php
    * TABELA MYSQL:       tipo_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_banco extends DAO_Tipo_banco
    {
        public static $TIPO_BANCO_WEB  = 1;
        public static $TIPO_BANCO_ANDROID  = 2;
        public static $TIPO_BANCO_IPHONE  = 3;
        public static $TIPO_BANCO_WINDOWS_PHONE  = 4;
        public static function VETOR_TIPO_BANCO(){
            return array(EXTDAO_Tipo_banco::$TIPO_BANCO_WEB, EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID);
        }
        
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_banco";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_banco();

        }
        
        public static function getImg($raiz, $tipoBanco){
            $img = "";
            switch ($tipoBanco) {
                case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                    $img = "<img src=\"{$raiz}/adm/imgs/mobile_android.png\" height=\"32\" width=\"32\">";
                    break;
                case EXTDAO_Tipo_banco::$TIPO_BANCO_IPHONE:
                    $img = "<img src=\"{$raiz}/adm/imgs/mobile_iphone.png\" height=\"32\" width=\"32\">";
                    break;
                case EXTDAO_Tipo_banco::$TIPO_BANCO_WINDOWS_PHONE:
                    $img = "<img src=\"{$raiz}/adm/imgs/mobile_windows_phone.png\" height=\"32\" width=\"32\">";
                    break;
                default:
                    break;
            }
            return $img;
        }

	}

    
