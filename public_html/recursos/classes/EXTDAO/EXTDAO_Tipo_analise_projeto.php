<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_analise_projeto
    * NOME DA CLASSE DAO: DAO_Tipo_analise_projeto
    * DATA DE GERAÇÃO:    11.06.2013
    * ARQUIVO:            EXTDAO_Tipo_analise_projeto.php
    * TABELA MYSQL:       tipo_analise_projeto
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_analise_projeto extends DAO_Tipo_analise_projeto
    {

        const PROTOTIPO = 1;
        const SINCRONIZACAO = 2;
        const COMPARACAO = 3;
        const Atualiza_banco_de_dados_MySql = 4;
        const ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB = 6;
        const DESENVOLVIMENTO_APP = 7;
        const Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web = 9;
            
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_analise_projeto";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_analise_projeto();

        }

	}

    
