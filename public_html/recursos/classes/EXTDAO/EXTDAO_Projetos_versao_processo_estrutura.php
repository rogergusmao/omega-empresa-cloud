<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_versao_processo_estrutura
    * NOME DA CLASSE DAO: DAO_Projetos_versao_processo_estrutura
    * DATA DE GERAÇÃO:    23.04.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_processo_estrutura.php
    * TABELA MYSQL:       projetos_versao_processo_estrutura
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos_versao_processo_estrutura extends DAO_Projetos_versao_processo_estrutura
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_versao_processo_estrutura";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_projetos_versao_banco_banco_id_INT = "Relacionamento Entre Banco de Homologação E Produção";
			$this->label_processo_estrutura_id_INT = "Processo de Análise de Estrutura";
			$this->label_data_inicio_DATETIME = "Data de Início da Execução da Etapa do Processo";
			$this->label_data_fim_DATETIME = "Data de Fim da Execução da Etapa do Processo";
			$this->label_id_atual_projetos_versao_caminho_INT = "Etapa Atual do Processo";
			$this->label_get_vetor_id = "Parametro Get Vetor_id";
			$this->label_get_indice_atual = "Parametro Get Indice_atual";
                        $this->label_total_ciclo_INT = "Total de Ciclos";
                        $this->label_ciclo_atual_INT = "Ciclo Atual";
                        $this->label_total_tempo_gasto_seg_INT = "Tempo Total Gasto Exec Ciclo(seg)";
                        $this->label_copia_projetos_versao_banco_banco_id_INT = "Utiliza cópia do Relacionamento dos Bancos da Versão de Projeto ";
                        
                        
                        


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_versao_processo_estrutura();

        }

        public function getNome(){
            $obj = new EXTDAO_Processo_estrutura();
            $obj->select($this->getProcesso_estrutura_id_INT());
            return $obj->getNome();
        }
        
        public static function actionUrlExecutaProcesso($outProcessoConcluido){
            $cicloPagina = true;
            $idPVPE = Helper::POST(Param_Get::ID_PROJETOS_VERSAO_PROCESSO_ESTRUTURA);
            $idPVC = Helper::POST(Param_Get::ID_PROJETOS_VERSAO_CAMINHO);
            $objPVC = new EXTDAO_Projetos_versao_caminho();
            $objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
            $objPVPE->select($idPVPE);
            $objPVC->select($idPVC);
            if($outProcessoConcluido){
                if($objPVPE->existeProximaEtapa())
                    EXTDAO_Projetos_versao_processo_estrutura::proximaEtapa($objPVPE->getId());
                else{
                    $objPVC->finalizaEtapa();
                    EXTDAO_Projetos_versao_processo_estrutura::finalizaCiclo($objPVPE->getId());
                    $cicloPagina = false;
                }
            }
            
            if ($cicloPagina) {
                //gravando o estado atual no variavel GET
                $url = EXTDAO_Projetos_versao_processo_estrutura::getUrlDeExecucaoDaEtapa($idPVPE);
                return $url;
                //return array("location: index.php?page=compara_banco_hom_prod&tipo=pages&".$varGET);
            } else {
                $url = EXTDAO_Projetos_versao_processo_estrutura::getUrlDeFinalizacaoDoProcessoEstrutura($idPVPE);
                return $url;
            }
        }
        
        public static function getUrlDeFinalizacaoDoProcessoEstrutura($idPVPE){
            $idPVBB = Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO);
            $idPV = Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO);
             $varGET = Helper::formataGET(
              array(
                Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA,
                Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                Param_get::ID_PROJETOS_VERSAO
              ), array(
                $idPVPE,
                $idPVBB,
                $idPV
              )
            );

    
            return "index.php?page=menu_banco_banco&tipo=pages&" . $varGET;
        }
        
         public static function getUrlDeInicializacaoDeProcesso($idPV, $idPVBB ){
            

            $varGET = Helper::formataGET(
                  array(
                        
                        Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                        Param_get::ID_PROJETOS_VERSAO,
                        Param_get::ID_SISTEMA_PRODUTO,
                  ), array(
                        $idPVBB,
                        $idPV,
                      Helper::POSTGET(Param_get::ID_SISTEMA_PRODUTO)
                  )
            );
            $url = "index.php?tipo=pages&page=iniciar_projetos_versao_processo_estrutura&$varGET";
            return $url;
    
        }
        public static function getUrlDeExecucaoDaEtapa($idPVPE){
            

            $varGET = Helper::formataGET(
                  array(
                        Param_get::PROCESSO_INICIADO,
                        Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA,
                        Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                        Param_get::ID_PROJETOS_VERSAO
                  ), array(
                        "true",
                        $idPVPE,
                        Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO),
                        Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO)
                  )
            );

            return "index.php?page=executa_processo&tipo=pages&" . $varGET;
    
        }
        
        public function getListaIdProjetosVersaoCaminho(){
            
            $consultaRegistros = "SELECT pvc.id 
                FROM projetos_versao_caminho pvc
                    JOIN processo_estrutura_caminho pec 
                    ON pvc.processo_estrutura_caminho_id_INT = pec.id
                WHERE pvc.projetos_versao_processo_estrutura_id_INT= {$this->getId()}
                ORDER BY pec.seq_INT";
            $db = new Database();
            $db->query($consultaRegistros);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public function isFaseFinalizada($status = null){
            $cicloFinalizado = false;
            if($status == null)
            $status = $this->getStatus();
            switch ($status) {
                case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
                    $cicloFinalizado = true;
                    break;
                case EXTDAO_Projetos_versao_caminho::EXECUTADO:
                    break;
                case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
                    $cicloFinalizado = true;
                    break;
                case EXTDAO_Projetos_versao_caminho::FINALIZADO:
                    break;
                case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
                    $cicloFinalizado = true;
                    break;
                default:
                    break;
            }
            return $cicloFinalizado;
        }
        
        public function getStatus(){
            $list = $this->getListaIdProjetosVersaoCaminho();
            $obj = new EXTDAO_Projetos_versao_caminho();
            $statusEstrutura = 1000000;
            
            for($i = 0 ; $i < count($list); $i++){
                $id = $list[$i];
                $obj->select($id);
                
                $statusCaminho = $obj->getStatus();
                if($statusCaminho < $statusEstrutura)
                    $statusEstrutura = $statusCaminho;
                
            }
            
            
            return $statusEstrutura;
        }
        
        public static function getIdDoProcessoEstrutura($idPVBB, $pIdProcessoEstrutura, Database $db = null){
            $q = "SELECT id
                FROM projetos_versao_processo_estrutura 
                WHERE projetos_versao_banco_banco_id_INT = {$idPVBB} 
                    AND processo_estrutura_id_INT = {$pIdProcessoEstrutura}";
            if($db == null)
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public function getTotalEtapas(){
            $q = "SELECT COUNT(DISTINCT pvc.id)
                FROM projetos_versao_processo_estrutura pvpe JOIN
                                projetos_versao_caminho pvc
                                ON pvc.projetos_versao_processo_estrutura_id_INT = pvpe.id
                WHERE pvpe.id = {$this->getId()}";
            $db = new Database();
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public function isEtapaAtualTerminada(){
            if($this->getCiclo_atual_INT() < $this->getTotal_ciclo_INT())
                return false;
            else return true;
        }
        
        public function registraCiclo($cicloAtual, $strVetorId, $tempoGasto, $db =null){
            $obj = new EXTDAO_Projetos_versao_processo_estrutura();
            $obj->select($this->getId());
            
            $obj->setCiclo_atual_INT($cicloAtual); 
            
            $tempo = $this->getTotal_tempo_gasto_seg_INT();
            if(!strlen($tempo))
                $tempo  =$tempoGasto;
            else $tempo += $tempoGasto;
            
            $obj->setGet_vetor_id($strVetorId);
            $obj->formatarParaSQL();
            $obj->update($this->getId());
            //atualiza o objeto atual
            $this->select($this->getId());
            
        }
        
        public function existeProximaEtapa($db=null){
            
                $idPVC = $this->getId_atual_projetos_versao_caminho_INT();
            
            if(strlen($idPVC)){


                $objPVC = new EXTDAO_Projetos_versao_caminho($db);
                $objPVC->select($idPVC);

                
                $vIdPos = $objPVC->getIdDaEtapaPosterior($db);
                if(strlen($vIdPos)) return true;
            }
            return false;
        }
        
        public function limpaDadosCiclo(){
            $this->setGet_vetor_id(null);
            $this->setCiclo_atual_INT(null);
            $this->setTotal_ciclo_INT(null);
        }
        
        public static function proximaEtapa($id, $db = null){
            $obj = new EXTDAO_Projetos_versao_processo_estrutura($db);
            $obj->select($id);
            $idPVC = $obj->getId_atual_projetos_versao_caminho_INT();
            if(strlen($idPVC)){
                $objPVC = new EXTDAO_Projetos_versao_caminho($db);
                $objPVC->select($idPVC);
                $objPVC->finalizaEtapa($db);
                $vIdPos = $objPVC->getIdDaEtapaPosterior($db);
                if(strlen($vIdPos)){
                    $obj->setId_atual_projetos_versao_caminho_INT($vIdPos);
                    $obj->limpaDadosCiclo();
                    $obj->formatarParaSQL();
                    $obj->update($id);
                    
                    $objPVC->select($vIdPos);
                    $objPVC->inicializaEtapa();
                    return $vIdPos;
                } else  //para o ciclo
                    return false;
            }
            return false;
        }
        
        public static function reinicializaCicloNaEtapa($id, $pIdPVCaminho, $db = null){
            $obj = new EXTDAO_Projetos_versao_processo_estrutura($db);
            $obj->select($id);
            $idPVC = $obj->getId_atual_projetos_versao_caminho_INT();
            
            $objPVC = new EXTDAO_Projetos_versao_caminho($db);
            if(strlen($idPVC)){
                $objPVC->select($idPVC);
                $objPVC->finalizaEtapa($db);
            }
            
            $vIdPos = $pIdPVCaminho;
            if(strlen($vIdPos)){
                $obj->setId_atual_projetos_versao_caminho_INT($vIdPos);
                $obj->limpaDadosCiclo();
                $obj->formatarParaSQL();
                $obj->update($id);

                $objPVC->select($vIdPos);
                $objPVC->inicializaEtapa($db);
                return true;
            } else  //para o ciclo
                return false;
            
        }
        
        public static function finalizaCiclo($id, $db = null){
            $obj = new EXTDAO_Projetos_versao_processo_estrutura($db);
            $obj->select($id);
            
            
            $obj->setId_atual_projetos_versao_caminho_INT(null);
            $obj->limpaDadosCiclo();
            $obj->formatarParaSQL();
            $obj->update($id);
        }
        
        public function getDataInicioProcesso(){
            $idPVC = EXTDAO_Projetos_versao_processo_estrutura::getIdPrimeiraEtapa($this->getId());
            $obj = new EXTDAO_Projetos_versao_caminho();
            $obj->select($idPVC);
            return $obj->getData_inicio_DATETIME();
        }
        
        public function getDataFim(){
            $idPVC = EXTDAO_Projetos_versao_processo_estrutura::getIdUltimaEtapa($this->getId());
            $obj = new EXTDAO_Projetos_versao_caminho();
            $obj->select($idPVC);
            
            
            return $obj->getData_fim_DATETIME();
        }
        
        public static function getStatusPVBB($idPVBB){
            if(!EXTDAO_Projetos_versao_processo_estrutura::validaListaProcessoEstrutura($idPVBB))
                return EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO;
            
            $objBanco = new Database();
            $consultaRegistros = "SELECT id 
                    FROM projetos_versao_processo_estrutura 
                    WHERE projetos_versao_banco_banco_id_INT = $idPVBB
                    ORDER BY id";

            $objBanco->query($consultaRegistros);
    
            $statusPredominante = null;
            $idPVPEAntigo = null;
            $obj = new EXTDAO_Projetos_versao_processo_estrutura();
            for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

                $obj->select($regs[0]);
                $obj->formatarParaExibicao();
                $status = $obj->getStatus();
                if($statusPredominante != null
                   && ($statusPredominante == EXTDAO_Projetos_versao_caminho::EXECUTANDO
                     || $statusPredominante == EXTDAO_Projetos_versao_caminho::DESATUALIZADO
                      )
                   && $status != EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO
                  ){
                        $status = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
                } else if($statusPredominante == null) 
                    $statusPredominante = $status;
                else if($statusPredominante > $status)
                    $status = $statusPredominante;
                else{
                    if($statusPredominante == EXTDAO_Projetos_versao_caminho::EXECUTADO
                        && $idPVPEAntigo != null
                        && $statusPredominante != EXTDAO_Projetos_versao_caminho::DESATUALIZADO){
                        if($obj->desatualizadoComparado($idPVPEAntigo))
                            $status = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
                    }
                }
                $idPVPEAntigo = $obj->getId();
            }
            if($statusPredominante == null) 
                return EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO;
            else 
                return $status;
        }
        
        public static function getIdUltimaEtapa($pIdProjetosVersaoProcessoEstrutura) {
        $q = "SELECT p.id, pec.seq_INT
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$pIdProjetosVersaoProcessoEstrutura}
                ORDER BY pec.seq_INT DESC
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }
    
        public function desatualizadoComparado($idPVBBAnterior) {
        //desatualizado => se a consulta retornar algum valor
        $q = "SELECT 1
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$idPVBBAnterior}
                    AND data_fim_DATETIME > (
                        SELECT p1.data_inicio_DATETIME
                        FROM projetos_versao_caminho p1
                            JOIN processo_estrutura_caminho pec1 
                                ON pec1.id = p1.processo_estrutura_caminho_id_INT
                        WHERE p1.projetos_versao_processo_estrutura_id_INT = {$this->getId()}
                        ORDER BY pec1.seq_INT ASC
                        LIMIT 0,1
                    )
                ORDER BY pec.seq_INT DESC
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        $id = $d->getPrimeiraTuplaDoResultSet(0);
        if(strlen($id)){
            return true;
        } else 
            return false;
    }
    
        public function getDataFimProcesso() {
        $q = "SELECT p.id, pec.seq_INT
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$this->getId()}
                    AND data_fim_DATETIME > (
                        SELECT p1.data_inicio_DATETIME
                        FROM projetos_versao_caminho p1
                            JOIN processo_estrutura_caminho pec1 
                                ON pec1.id = p1.processo_estrutura_caminho_id_INT
                        WHERE p1.projetos_versao_processo_estrutura_id_INT = {$this->getId()}
                        ORDER BY pec1.seq_INT ASC
                        LIMIT 0,1
                    )
                ORDER BY pec.seq_INT DESC
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        $id = $d->getPrimeiraTuplaDoResultSet(0);
        if(strlen($id)){
            $obj = new EXTDAO_Projetos_versao_caminho();
            $obj->select($id);
            return $obj->getData_fim_DATETIME();
        } else 
            return null;
    }
    
        public static function getIdPrimeiraEtapa($pIdProjetosVersaoProcessoEstrutura, $db = null) {
        $q = "SELECT p.id, pec.seq_INT
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$pIdProjetosVersaoProcessoEstrutura}
                ORDER BY pec.seq_INT ASC
                LIMIT 0,1";
        if($db==null)
        $db = new Database();
            $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }
        
        public static function inicializaCiclo($id, $db = null){
            $pIdEtapaPVC = EXTDAO_Projetos_versao_processo_estrutura::getIdPrimeiraEtapa($id, $db);
            $obj = new EXTDAO_Projetos_versao_processo_estrutura($db);
            //$idProcessoAtual = $PROCESSO_INICIALIZA;
            $objPVC = new EXTDAO_Projetos_versao_caminho($db);
            $objPVC->select($pIdEtapaPVC);
            $objPVC->inicializaEtapa($db);

            //zera os marcadores de andamento da etapa do processo
            $obj->select($id);
            $obj->limpaDadosCiclo();
            $obj->setTotal_tempo_gasto_seg_INT(null);
            $obj->setId_atual_projetos_versao_caminho_INT($pIdEtapaPVC);
            $obj->formatarParaSQL();
            $obj->update($id);
            
            return $pIdEtapaPVC;
        }
        
        public function getObjProcessoEstrutura($db = null){
            $obj = new EXTDAO_Processo_estrutura($db);
            $obj->select($this->getProcesso_estrutura_id_INT());
            return $obj;
        }
        
         public function imprimirAcao($varGET){
             
            if(
                $this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::CARREGAR_RELACIONAMENTOS_DA_ANALISE_DO_PROJETO
                ||
                ($this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::CARREGA_ESTRUTURA_BANCO_DE_HOM_PARA_PROD
                    && $status = EXTDAO_Projetos_versao_caminho::EXECUTADO
                )
                ||
                ($this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::MAPEANDO_ESTRUTURA_DOS_BANCOS_HMG_SINC_E_WEB
                    && $status = EXTDAO_Projetos_versao_caminho::EXECUTADO
                )
                
                ){


                 ?>
                 <a href="index.php?tipo=lists&page=gerencia_tabela_tabela&<?php echo $varGET; ?>" target="_self" >Estrutura do banco</a>

                 <?php
             }
             ?>

             <?php
            if((
                $this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM
                || $this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::GERAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_DO_BANCO_DE_PRODUCAO
                || $this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::GERAR_SCRIPT_SQL_DE_ATUALIZACAO_DOS_REGISTROS_DO_BANCO_DE_PRODUCAO
                )
                && $status = EXTDAO_Projetos_versao_caminho::EXECUTADO){


             ?>

                 <a href="ajax.php?tipo=lists&page=gerenciar_script_comando_banco&<?php  echo $varGET;?>&id_processo_estrutura=<?php  echo $this->getProcesso_estrutura_id_INT();?>" target="_self">Script SQL</a>

             <?php
             }
             ?>

             <?php
            if(($this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::ATUALIZA_ESTRUTURA_BANCO_DE_PROD
                || $this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::EXECUTAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_BANCO_PRD)
                    ){


             ?>
             <a href="index.php?tipo=lists&page=gerencia_projetos_versao_log_erro&<?php echo $varGET;?>" target="_self">Log Consultas</a>
             <?php
             }
            if($this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::CARREGAR_CONFIGURACOES_SINCRONIZACAO
                    && $status = EXTDAO_Projetos_versao_caminho::EXECUTADO){


             ?>
             <a href="index.php?tipo=lists&page=gerencia_sincronizacao_tabela&<?php echo $varGET;?>" target="_self">Configuração de sincronização das tabelas web/android </a>
             <?php
             } 



             ?>



             <?php
            if(($this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::COMPARAR_ESTRUTURA_BANCO_DE_PROD_COM_HOM
                    || $this->getProcesso_estrutura_id_INT() == EXTDAO_Processo_estrutura::COMPARAR_ESTRUTURA_SINCRONIZACAO_BANCO_WEB_ANDROID)
                    && $status = EXTDAO_Projetos_versao_caminho::EXECUTADO){

               if($this->getCopia_projetos_versao_banco_banco_id_INT() != null)  {

                   $objPVBBCopia = new EXTDAO_Projetos_versao_banco_banco();
                   $idPVBBCopia = $this->getCopia_projetos_versao_banco_banco_id_INT();
                   $objPVBBCopia->select($idPVBBCopia);
                   $varGETCopia =  Helper::formataGET(array(
                         Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                         Param_Get::ID_PROJETOS_VERSAO),
                         array($idPVBBCopia, $objPVBBCopia->getProjetos_versao_id_INT())
                     );


                ?>
                <a href="index.php?tipo=lists&page=gerencia_tabela_tabela&<?php echo $varGETCopia; ?>" target="_self" >Estrutura do banco da analise da comparação</a>

                <?php
                }
         }
                        
                        
     }
        public static function getIdProcessoEstrutura($idPVPE){
             $q = "SELECT pvpe.processo_estrutura_id_INT, pvpe.processo_estrutura_id_INT
                FROM projetos_versao_processo_estrutura pvpe
                WHERE pvpe.id = {$idPVPE}
                LIMIT 0,1";
            $d = new Database();
            $d->query($q);
            return $d->getPrimeiraTuplaDoResultSet(0);

        }
        
        
        public static function validaListaProcessoEstruturaCaminho($idPVPE, $idPVBB){
            $idPE = EXTDAO_Projetos_versao_processo_estrutura::getIdProcessoEstrutura($idPVPE);
            $q = "SELECT pvc.processo_estrutura_caminho_id_INT
                FROM projetos_versao_processo_estrutura pvpe
                    JOIN projetos_versao_caminho pvc
                        ON pvpe.id = pvc.projetos_versao_processo_estrutura_id_INT
                WHERE pvpe.id = $idPVPE
                    ORDER BY pvc.processo_estrutura_caminho_id_INT";
            
            $q2 = "SELECT pec.id
                FROM processo_estrutura pe
                    JOIN processo_estrutura_caminho pec
                        ON pe.id = pec.processo_estrutura_id_INT
                WHERE pe.id = $idPE
                   ORDER BY pec.id";
            
            $db = new Database();
            
            $db->query($q);
            $vetorPVPE = Helper::getResultSetToArrayDeUmCampo($db->result);
            
            $db->query($q2);
            $vetorPE = Helper::getResultSetToArrayDeUmCampo($db->result);
            if(count($vetorPVPE) != count($vetorPE)){
                return false;
            } else {
                for($i = 0;  $i < count($vetorPE); $i++){
                    if($vetorPE[$i] != $vetorPVPE[$i])
                        return false;
                    
                }
                return true;
            }
            
        }
        
        public static function validaListaProcessoEstrutura($idPVBB){
            $q = "SELECT DISTINCT pvpe.processo_estrutura_id_INT, petb.seq_INT
                FROM projetos_versao_processo_estrutura pvpe 
                    JOIN processo_estrutura pe   
                        ON  pvpe.processo_estrutura_id_INT = pe.id
                    JOIN processo_estrutura_tipo_banco petb 
                        ON pe.id = petb.processo_estrutura_id_INT
                    JOIN projetos_versao_banco_banco pvbb
                        ON pvbb.id = pvpe.projetos_versao_banco_banco_id_INT
                    JOIN banco_banco bb
                        ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id = $idPVBB
                        AND bb.tipo_banco_id_INT = petb.tipo_banco_id_INT
                        AND petb.tipo_analise_projeto_id_INT = pvbb.tipo_analise_projeto_id_INT
                ORDER BY petb.seq_INT";
            
            $q2 = "SELECT pe.id
                FROM processo_estrutura pe 
                    JOIN processo_estrutura_tipo_banco petb 
                        ON pe.id = petb.processo_estrutura_id_INT
                WHERE petb.tipo_banco_id_INT = ".EXTDAO_Projetos_versao_banco_banco::getTipoBanco($idPVBB)."
                    AND petb.tipo_analise_projeto_id_INT = ".EXTDAO_Projetos_versao_banco_banco::getTipoAnaliseProjeto($idPVBB)."
                ORDER BY petb.seq_INT";
            
            $db = new Database();
            
            $db->query($q);
            $vetorPVPE = Helper::getResultSetToArrayDeUmCampo($db->result);
            
            $db->query($q2);
            $vetorPE = Helper::getResultSetToArrayDeUmCampo($db->result);
            
            if(count($vetorPVPE) == 0 || count($vetorPVPE) != count($vetorPE)){
                return false;
            } 
            else {
                for($i = 0;  $i < count($vetorPE); $i++){
                    if($vetorPE[$i] != $vetorPVPE[$i])
                        return false;
                    
                }
                $vetorPVPE = EXTDAO_Projetos_versao_processo_estrutura::getListaIdPVPE($idPVBB);
               if($vetorPVPE != null)
                foreach ($vetorPVPE as $idPVPE) {
                if(!EXTDAO_Projetos_versao_processo_estrutura::validaListaProcessoEstruturaCaminho($idPVPE, $idPVBB))
                    return false;
                }
                return true;         
                
                
            }
            
        }
        
        public static function getListaIdPVPE($idPVBB, $db = null){
             $q = "SELECT DISTINCT pvpe.id, pvpe.processo_estrutura_id_INT
                FROM projetos_versao_processo_estrutura pvpe
                WHERE pvpe.projetos_versao_banco_banco_id_INT = $idPVBB
                ORDER BY pvpe.processo_estrutura_id_INT";
             if($db == null)
                $db = new Database();
            
            $db->query($q);
            $ids= Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
        public static function getListaIdPE($idPVBB){
             $q = "SELECT DISTINCT pvpe.processo_estrutura_id_INT, petb.seq_INT
                FROM projetos_versao_processo_estrutura pvpe 
                    JOIN processo_estrutura pe   
                        ON  pvpe.processo_estrutura_id_INT = pe.id
                    JOIN processo_estrutura_tipo_banco petb 
                        ON pe.id = petb.processo_estrutura_id_INT
                    JOIN projetos_versao_banco_banco pvbb
                        ON pvbb.id = pvpe.projetos_versao_banco_banco_id_INT
                    JOIN banco_banco bb
                        ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id = $idPVBB
                    AND bb.tipo_banco_id_INT = petb.tipo_banco_id_INT
                    AND petb.tipo_analise_projeto_id_INT = pvbb.tipo_analise_projeto_id_INT
                ORDER BY petb.seq_INT";
          
             $db = new Database();
            
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result, 0, 1);
        }
        
        public static function getListaObjProcessoEstrutura($idPVBB, $db = null){
            $q = "SELECT pvpe.processo_estrutura_id_INT, pvpe.id
                FROM projetos_versao_processo_estrutura pvpe
                    JOIN projetos_versao_banco_banco pvbb
                        ON pvbb.id = pvpe.projetos_versao_banco_banco_id_INT
                    JOIN processo_estrutura_tipo_banco petb
                        ON petb.processo_estrutura_id_INT = pvpe.processo_estrutura_id_INT
                    JOIN banco_banco bb
                        ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id = $idPVBB
                    AND bb.tipo_banco_id_INT = petb.tipo_banco_id_INT
                    AND petb.tipo_analise_projeto_id_INT = pvbb.tipo_analise_projeto_id_INT
                ORDER BY petb.seq_INT";
            
            $q2 = "SELECT pe.id
                FROM processo_estrutura pe 
                    JOIN processo_estrutura_tipo_banco petb 
                        ON pe.id = petb.processo_estrutura_id_INT
                WHERE petb.tipo_banco_id_INT = ".EXTDAO_Projetos_versao_banco_banco::getTipoBanco($idPVBB)."
                    AND petb.tipo_analise_projeto_id_INT = ".EXTDAO_Projetos_versao_banco_banco::getTipoAnaliseProjeto($idPVBB)."
                ORDER BY petb.seq_INT";
            if($db == null)
            $db = new Database();
            
            $db->query($q);
            $vetorPVPE = Helper::getResultSetToMatriz($db->result);
            
            $db->query($q2);
            $vetorPE = Helper::getResultSetToArrayDeUmCampo($db->result);
            
            $vetorInserir = array();
            $vetorDeletar = array();

            for($i = 0;  $i < count($vetorPE); $i++){
                $achou = false;
                for($j = 0;  $j < count($vetorPVPE); $j++){
                    if($vetorPE[$i] == $vetorPVPE[$j][0]){
                        $achou = true;
                    }
                }
                if(!$achou)
                    $vetorInserir[count($vetorInserir)] = $vetorPE[$i];
            }
            for($j = 0;  $j < count($vetorPVPE); $j++){
                $achou = false;
                for($i = 0;  $i < count($vetorPE); $i++){
                    if($vetorPE[$i] == $vetorPVPE[$j][0]){
                        $achou = true;
                    }
                }
                if(!$achou)
                    $vetorDeletar[count($vetorDeletar)] = $vetorPVPE[$j][1];
            }

            return array("deletarPVPE" => $vetorDeletar, "inserirPE" => $vetorInserir);
        }
        
        public static function atualizaEstruturaProjetosVersao($idPVBB, $idPV){
            
            $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
            $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);

            $varGET = Param_Get::getIdentificadorProjetosVersao();

            $listaObjPE = EXTDAO_Projetos_versao_processo_estrutura::getListaObjProcessoEstrutura($idPVBB);
            $vetorDeletar = $listaObjPE["deletarPVPE"];
            $vetorInserir = $listaObjPE["inserirPE"];

            $objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
            if($vetorDeletar !=null)
            foreach ($vetorDeletar as $idPVPE) {
                $objPVPE->delete($idPVPE);
            }
            if($vetorInserir != null)
            foreach ($vetorInserir as $idPE) {
                $objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
                $objPVPE->setProcesso_estrutura_id_INT($idPE);
                $objPVPE->setProjetos_versao_banco_banco_id_INT($idPVBB);
                $objPVPE->formatarParaSQL();
                $objPVPE->insert();
            }

            $listaIdPVPE = EXTDAO_Projetos_versao_processo_estrutura::getListaIdPVPE($idPVBB);
            for($i = 0 ; $i < count($listaIdPVPE); $i++){
                $idPVPE = $listaIdPVPE[$i];
                $listaObjPEC = EXTDAO_Projetos_versao_processo_estrutura::getListaObjProcessoEstruturaCaminho($idPVBB, $idPVPE);
                $vetorDeletar = $listaObjPEC["deletarPVC"];
                $vetorInserir = $listaObjPEC["inserirPEC"];


                $objPVC = new EXTDAO_Projetos_versao_caminho();
                if($vetorDeletar != null)
                foreach ($vetorDeletar as $idPVC) {
                    $objPVC->delete($idPVC);
                }

                for($i = 0 ; $i < count($vetorInserir); $i++){

                    $idPC =$vetorInserir[$i];

                    $objPVC = new EXTDAO_Projetos_versao_caminho();
                    $objPVC->setProcesso_estrutura_caminho_id_INT($idPC);
                    $objPVC->setProjetos_versao_processo_estrutura_id_INT($idPVPE);
                    $objPVC->formatarParaSQL();
                    $objPVC->insert();
                }
            }
        }
        
        public static function getListaObjProcessoEstruturaCaminho($idPVBB, $idPVPE){
            
            $idPE = EXTDAO_Projetos_versao_processo_estrutura::getIdProcessoEstrutura($idPVPE);
            
            $q = "SELECT DISTINCT pvc.processo_estrutura_caminho_id_INT, pvc.id
                FROM projetos_versao_processo_estrutura pvpe
                    JOIN projetos_versao_caminho pvc
                        ON pvpe.id = pvc.projetos_versao_processo_estrutura_id_INT
                WHERE pvpe.id = $idPVPE
                    ORDER BY pvc.processo_estrutura_caminho_id_INT";
            
            $q2 = "SELECT pec.id
                FROM processo_estrutura pe
                    JOIN processo_estrutura_caminho pec
                        ON pe.id = pec.processo_estrutura_id_INT
                WHERE pe.id = $idPE
                   ORDER BY pec.id";
            $db = new Database();
            
            $db->query($q);
            //pvc.processo_estrutura_caminho_id_INT, pvc.id, pvpe.id
            $vetorPVPE = Helper::getResultSetToMatriz($db->result);
            //pec.id
            $db->query($q2);
            $vetorPE = Helper::getResultSetToArrayDeUmCampo($db->result);
            
            $vetorInserir = array();
            $vetorDeletar = array();

            for($i = 0;  $i < count($vetorPE); $i++){
                $achou = false;
                for($j = 0;  $j < count($vetorPVPE); $j++){
                    if($vetorPE[$i] == $vetorPVPE[$j][0]){
                        $achou = true;
                    }
                }
                if(!$achou){
                    $vetorInserir[count($vetorInserir)] = $vetorPE[$i];
                } 
                
            }
            for($j = 0;  $j < count($vetorPVPE); $j++){
                $achou = false;
                for($i = 0;  $i < count($vetorPE); $i++){
                    if($vetorPE[$i] == $vetorPVPE[$j][0]){
                        $achou = true;
                    }
                }
                if(!$achou){
                    $vetorDeletar[count($vetorDeletar)] = $vetorPVPE[$j][1];
                    
                }
            }

            return array("deletarPVC" => $vetorDeletar, "inserirPEC" => $vetorInserir);
                
            
            
        }

        public static function getIdRelativoAoProcessoEstrutura($idPVBB, $idPE, $db)
        {
            $q = "SELECT DISTINCT pvpe.id
                FROM projetos_versao_processo_estrutura pvpe
                    JOIN projetos_versao_banco_banco pvbb
                        ON pvbb.id = pvpe.projetos_versao_banco_banco_id_INT
                    JOIN processo_estrutura_tipo_banco petb
                        ON petb.processo_estrutura_id_INT = pvpe.processo_estrutura_id_INT
                    JOIN banco_banco bb
                        ON pvbb.banco_banco_id_INT = bb.id
                WHERE pvbb.id = $idPVBB
                    AND pvpe.processo_estrutura_id_INT=$idPE
                    AND bb.tipo_banco_id_INT = petb.tipo_banco_id_INT
                    AND petb.tipo_analise_projeto_id_INT = pvbb.tipo_analise_projeto_id_INT
                ";
            $db->query($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
	}

    
