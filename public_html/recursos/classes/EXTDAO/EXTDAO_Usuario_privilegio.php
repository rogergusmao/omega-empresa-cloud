<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Usuario_privilegio
    * NOME DA CLASSE DAO: DAO_Usuario_privilegio
    * DATA DE GERAÇÃO:    13.07.2010
    * ARQUIVO:            EXTDAO_Usuario_privilegio.php
    * TABELA MYSQL:       usuario_privilegio
    * BANCO DE DADOS:     DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    
    class EXTDAO_Usuario_privilegio extends DAO_Usuario_privilegio
    {

        public function __construct($configDAO = null){
        
            parent::__construct($configDAO);
        
            	$this->nomeClasse = "EXTDAO_Usuario_privilegio";

        }
        
        public function setLabels(){
        
			$this->label_id = "Id";
			$this->label_usuario_id_INT = "Usuário";
			$this->label_identificador_funcionalidade = "Identificador da Funcionalidade";

        
        }
        
        public function setDiretorios(){
                
        
        
        }
        
        public function setDimensoesImagens(){
                
     
        
        }
        
        public static function factory(){
                
            return new EXTDAO_Usuario_privilegio();        
        
        }
        
	}
    
    
