<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_produto_web
    * NOME DA CLASSE DAO: DAO_Sistema_produto_web
    * DATA DE GERAÇÃO:    28.04.2013
    * ARQUIVO:            EXTDAO_Sistema_produto_web.php
    * TABELA MYSQL:       sistema_produto_web
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_produto_web extends DAO_Sistema_produto_web
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_produto_web";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_sistema_produto_id_INT = "Produto";
			$this->label_diretorio_web_id_INT = "Diretório do Projeto Web";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_produto_web();

        }
        
         public static function getListaIdentificadorNo($idSistema){
            $q = "SELECT DISTINCT sp.id, sp.nome
                FROM sistema s JOIN sistema_produto sp ON sp.sistema_id_INT = s.id
                    JOIN sistema_produto_web spw ON  sp.id = spw.sistema_produto_id_INT
                WHERE s.id = $idSistema 
                ORDER BY sp.nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }

	}

    
