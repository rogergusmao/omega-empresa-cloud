<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Mobile
 * NOME DA CLASSE DAO: DAO_Mobile
 * DATA DE GERAÇÃO:    14.03.2013
 * ARQUIVO:            EXTDAO_Mobile.php
 * TABELA MYSQL:       mobile
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Mobile extends DAO_Mobile {
    
    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Mobile";

    }

    public static function getInformacoesMobileIdentificadorDoMobile($idM){
        $mobileIdentificadores = EXTDAO_Mobile_identificador::getListaIdentificadorNoProdutos($idM);
        $identificadores = array();
        for($i = 0 ; $i < count($mobileIdentificadores); $i++){
            $identificadores[$i] = EXTDAO_Mobile_identificador::getInformacoes($mobileIdentificadores[$i][0]);
            $identificadores[$i]["produto"] = $mobileIdentificadores[$i][1];
        }
        
        return $identificadores;
        
        
    }
    
        public static function isMobileConectado($idMobile){
            
            $consulta = "SELECT data_atualizacao_DATETIME
                        FROM mobile_conectado mc JOIN mobile_identificador mi 
                            ON mc.mobile_identificador_id_INT = mi.id
                        WHERE mi.mobile_id_INT = {$idMobile} 
                        ORDER BY data_atualizacao_DATETIME DESC
                        LIMIT 0,1";
            
            $db = new Database();
            $db->query($consulta);
            $data = $db->getPrimeiraTuplaDoResultSet(0);
            if(strlen($data)){
                $totSegAtualizacao = strtotime($data) + LIMITE_SEGUNDOS_PARA_DESCONECTAR_TELEFONE;
                $totSegAtual = strtotime(Helper::getDiaEHoraAtualSQL());
                if($totSegAtualizacao < $totSegAtual)
                    return true;
                else return false;
            } else return false;
        }
        
    public function setLabels() {

        $this->label_id = "Id";
        $this->label_identificador = "Identificador";
        $this->label_imei = "Imei";
    }

    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Mobile();
    }

    public static function consulta($imei, $db = null) {
        $consulta = "SELECT id
                        FROM mobile
                        WHERE imei = '{$imei}'
                        LIMIT 0,1";
        if($db == null) $db = new Database();
        $db->query($consulta);
        return $db->getPrimeiraTuplaDoResultSet("id");
    }

    public static function getListaIdentificadorNo() {
        $q = "SELECT id, identificador
                FROM mobile
                ORDER BY identificador";
        $db = new Database();
        $db->query($q);
        return Helper::getResultSetToMatriz($db->result);
    }
    
    

    public static function getTotalVersoesDeSoftwareInstalado(){
        $q = "SELECT COUNT(DISTINCT(mi.sistema_projetos_versao_sistema_produto_mobile_id_INT))
FROM mobile_identificador mi";
        $db = new Database();
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public static function getTotalMobileConectado(){
        $data = EXTDAO_Mobile::getDataParametroConectado();
        $dataSQL = Helper::formatarDataParaComandoSQL($data);
        $q = "SELECT COUNT(DISTINCT(m.id))
FROM mobile m 
        JOIN mobile_identificador mi
            ON m.id = mi.mobile_id_INT
        JOIN mobile_conectado mc
            ON mc.mobile_identificador_id_INT = mi.id
WHERE mc.data_atualizacao_DATETIME > $dataSQL";
        $db = new Database();
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }    
    public static function getDataParametroConectado(){
        return Helper::somaSegundosAData(Helper::getDiaEHoraAtualSQL(), -TEMPO_TOTAL_EM_SEGUNDOS_TELFONE_FICAR_OFFLINE);
    }
    
    public static function getTotalMobileDesconectado(){
        $data = EXTDAO_Mobile::getDataParametroConectado();
        $dataSQL = Helper::formatarDataParaComandoSQL($data);
        $q = "SELECT COUNT(DISTINCT(m.id))
FROM mobile m 
WHERE (SELECT mc.data_atualizacao_DATETIME
	FROM mobile_conectado mc
	ORDER BY mc.id DESC
	LIMIT 0,1) < $dataSQL";
        $db = new Database();
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }    
    
    
    
    
}

