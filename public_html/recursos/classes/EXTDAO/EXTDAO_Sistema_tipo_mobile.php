<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_tipo_mobile
    * NOME DA CLASSE DAO: DAO_Sistema_tipo_mobile
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Sistema_tipo_mobile.php
    * TABELA MYSQL:       sistema_tipo_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_tipo_mobile extends DAO_Sistema_tipo_mobile
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_tipo_mobile";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_tipo_mobile();

        }

	}

    
