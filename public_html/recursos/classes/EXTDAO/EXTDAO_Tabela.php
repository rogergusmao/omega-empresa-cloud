<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tabela
    * NOME DA CLASSE DAO: DAO_Tabela
    * DATA DE GERAÇÃO:    28.01.2013
    * ARQUIVO:            EXTDAO_Tabela.php
    * TABELA MYSQL:       tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tabela extends DAO_Tabela
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tabela";

           

        }

        public function setLabels(){
            $this->label_id = "Id";
            $this->label_nome = "Nome";
            $this->label_banco_id_INT = "Banco";
            $this->label_banco_versao_id_INT = "Versão do Banco";
            $this->label_projetos_versao_id_INT = "Versão do Projeto";
            $this->label_frequencia_sincronizador_INT = "Frequência de Sincronização";
            $this->label_transmissao_web_para_mobile_BOOLEAN = "Transmissão da web para o mobile?";
            $this->label_transmissao_mobile_para_web_BOOLEAN = "Transmissão do mobile para a web?";
        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        
        
        public static function factory(){

            return new EXTDAO_Tabela();

        }
        
        public function getNomeClasseAndroid(){
            $nomeTabela = $this->getNome();
            $variavelTabela = str_replace("_", " ", $nomeTabela);
            
            $variavelTabela = ucwords($variavelTabela);
            $variavelTabela = str_replace(" ", "", $variavelTabela);
            return trim($variavelTabela);
        }
        
        public function getNomeClasseWeb(){
            $nomeTabela = $this->getNome();
            $variavelTabela = str_replace(" ", "_", $nomeTabela);
            $variavelTabela = ucfirst($variavelTabela);
            
            return trim($variavelTabela);
        }
        
        
        public function getNomeBanco($id){
            $db = new Database();
            $consulta="SELECT b.nome
                FROM tabela t
                    JOIN banco b
                        ON t.banco_id_INT = b.id
                    WHERE t.id = {$id}
                LIMIT 0,1";
                    
            $db->query($consulta);
            return Helper::getResultSetToPrimeiroResultado($db->result);
            
        }
        
        public static function existeTabela($pTabela, $pIdBanco, $pIdProjetoVersao, Database $db = null){
            if($db == null)
            $db = new Database();
            $consulta="SELECT id 
                FROM tabela 
                    WHERE nome = '{$pTabela}' 
                    AND banco_id_INT={$pIdBanco} 
                    AND projetos_versao_id_INT={$pIdProjetoVersao} 
                LIMIT 0,1";
                    
            $db->query($consulta);
            $id = Helper::getResultSetToPrimeiroResultado($db->result);
            return !strlen($id) ? null : $id;
            
        }
        
        public static function getTabelasSemPrefixo($pIdBanco, $pIdProjetoVersao, $prefixo, Database $db = null){
            if($db == null) $db = new Database();
            
            
            $prefixoEscape = Helper::escapeStringSQL($prefixo);
            $consulta="SELECT id 
                FROM tabela 
                    WHERE NOT nome  LIKE '$prefixoEscape%' 
                    AND banco_id_INT={$pIdBanco} 
                    AND projetos_versao_id_INT={$pIdProjetoVersao} ";
            
            $db->query($consulta);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
            
        }
        
        public static function getTabelasComPrefixo($pIdBanco, $pIdProjetoVersao, $prefixo, Database $db = null){
            if($db == null)
                $db = new Database();
            $prefixoEscape = Helper::escapeStringSQL($prefixo);
            $consulta="SELECT id, nome
                FROM tabela 
                    WHERE nome LIKE '$prefixoEscape%' 
                    AND banco_id_INT={$pIdBanco} 
                    AND projetos_versao_id_INT={$pIdProjetoVersao} ";
            
            $db->query($consulta);
            return Helper::getResultSetToMatriz($db->result, 0 , 1);
            
        }

        public function getListaChaveUnica(){
            $db = new Database();
            $db->query("SELECT DISTINCT a.unique_nome
                FROM atributo a JOIN tabela t ON a.tabela_id_INT = t.id 
                WHERE a.unique_nome IS NOT NULL 
                    AND LENGTH(a.unique_nome) > 0
                    AND a.tabela_id_INT ={$this->getId()}
                ORDER BY a.unique_nome");
            return Helper::getResultSetToArrayDeUmCampo($db->result);
            
        }
        
        public static function getHierarquiaTabelaBanco($idPV, $idBanco, Database $db = null){
            
            $vetorIdTabelaRaiz = EXTDAO_Tabela::getListaTabelaRaiz($idPV, $idBanco, $db);

//            $redis = HelperRedis::getSingleton();
//            $keyRedis = "getHierarquiaTabelaBanco($idPV, $idBanco)";
//            if($redis->exists($keyRedis)){
//                $str = $redis->get($keyRedis);
//                $arvore = unserialize($str);
//                if($arvore != false)
//                    return $arvore;
//            }

            $vetorRet = array();
            
            $arvore = array();
            $arvore["nodos"] = array();
            for($i = 0 ; $i < count($vetorIdTabelaRaiz); $i++){
                
                $idTabelaRaiz = $vetorIdTabelaRaiz[$i];
                $arvore[$idTabelaRaiz] = new stdClass();
                $arvore[$idTabelaRaiz]->cor = EXTDAO_Tabela::CINZA;
                $arvore[$idTabelaRaiz]->filhos = array();
                $arvore["nodos"][count($arvore["nodos"])] = $idTabelaRaiz;
            }
            
            for($i = 0 ; $i < count($vetorIdTabelaRaiz); $i++){
                $idTabelaRaiz = $vetorIdTabelaRaiz[$i];
                
                $arvore = EXTDAO_Tabela::mergetListaTabelaDependente(
                    $arvore, $idPV, $idBanco, $idTabelaRaiz, $db);
            }
            //Lista de id tabela
            $a= $arvore["nodos"];
//            $str =serialize($a);
//            $redis->set($keyRedis, $str);

            return $a;
        }
        const FREQUENCIA_SINCRONIZACAO_ZERO = "0";
        const FREQUENCIA_SINCRONIZACAO_UM = "1";
        const FREQUENCIA_SINCRONIZACAO_SEMPRE = "-1";
        
        public function getEstadoTabelaPai($objTabelaPai){
            $objTabelaPai = new EXTDAO_Tabela();
            $msg = "";
            $tipoMsg = MENSAGEM_OK;
            if($this->getTransmissao_web_para_mobile_BOOLEAN() == "1"){
                if($objTabelaPai->getTransmissao_web_para_mobile_BOOLEAN() != "1"){
                    $tipoMsg= MENSAGEM_WARNING;
                    $msg = "A tabela pai deve ser enviada, a não ser que a sincronização ocorra em outra rotina, ou os registros da tabela pai sejam fixos.";
                } else{
                    if($objTabelaPai->getFrequencia_sincronizador_INT() == EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_UM
                            && $this->getFrequencia_sincronizador_INT() == EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_SEMPRE){
                        $tipoMsg= MENSAGEM_WARNING;
                        $msg = "A frequencia da tabela pai é inferior a tabela filho. Isso é inválido se a tabela pai, não for uma tabela com registros fixos.";
                    }
                }
            }
            $obj = new stdClass();
            $obj->tipoMsg = $tipoMsg;
            $obj->msg = $msg;
            return $obj;
        }
        
        public static function corrigeFrequenciaDasTabelas($idPV, $idBanco){
            $vetor = EXTDAO_Tabela::getListaTabela($idPV, $idBanco);
            $objTabela = new EXTDAO_Tabela();
            for($i = 0 ; $i < count($vetor); $i++){
                $idTabela = $vetor[$i];
                $objTabela->select($idTabela);
                $modificada = false;
                if($objTabela->getTransmissao_web_para_mobile_BOOLEAN() == "1"){
                    if($objTabela->getFrequencia_sincronizador_INT() == EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_ZERO){
                        $objTabela->setFrequencia_sincronizador_INT(EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_UM);
                        $modificada = true;
                    }
                } else {
                    //Se nao houver sincronizacao da web para o mobile
                    if($objTabela->getFrequencia_sincronizador_INT() == EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_UM 
                            || $objTabela->getFrequencia_sincronizador_INT() == EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_SEMPRE){
                        $objTabela->setFrequencia_sincronizador_INT(EXTDAO_Tabela::FREQUENCIA_SINCRONIZACAO_ZERO);
                        $modificada = true;
                    }
                }
                if($modificada){
                $objTabela->formatarParaSQL();
                $objTabela->update($idTabela);    
                }
                
            }
            
        }
        
        public static function getListaIdTabelaPaiAteRaiz($idTabela , $idPV, $idBanco){
            
            $arvore = array();
            $arvore["nodos"] = array();

            $arvore[$idTabela]->cor = EXTDAO_Tabela::BRANCO;
            $arvore[$idTabela]->filhos = array();
            
            
            $arvore = EXTDAO_Tabela::mergetListaTabelaPai($arvore, $idPV, $idBanco, $idTabela);

            //Lista de id tabela
            return $arvore["nodos"];
        }
        
        public static function getListaIdTabelaDependente($idTabela , $idPV, $idBanco, Database $db = null){
            $vetorTabelaRaiz = array($idTabela);
            $vetorRet = array($idTabela);
            $arvore = array();
            $arvore["nodos"] = array();
            for($i = 0 ; $i < count($vetorRet); $i++){
                
                $idTabelaRaiz = $vetorRet[$i];
                $nodo = new stdClass();
                $nodo->cor = EXTDAO_Tabela::BRANCO;
                $nodo->filhos = array();
                $arvore[$idTabelaRaiz] = $nodo;
            }
            
            for($i = 0 ; $i < count($vetorTabelaRaiz); $i++){
                $idTabelaRaiz = $vetorTabelaRaiz[$i];
                $arvore = EXTDAO_Tabela::mergetListaTabelaDependente($arvore, $idPV, $idBanco, $idTabelaRaiz, $db);
            }
            //Lista de id tabela
            return $arvore["nodos"];
        }
        public static function mergetListaTabelaPai($arvore, $idPV, $idBanco, $idTabela){
            
            
            if($arvore[$idTabela] != null){
                if($arvore[$idTabela]->cor == EXTDAO_Tabela::PRETO)
                    return $arvore;
            } 
                
            $arvore[$idTabela]->cor = EXTDAO_Tabela::CINZA;

            
            $vetorPai= EXTDAO_Tabela::getListaTabelaPai($idPV, $idBanco, $idTabela);
            
            if(!empty($vetorPai)){
                for($i = 0 ; $i < count($vetorPai); $i++){
                    $idTabelaPai = $vetorPai[$i];
                    
                    if($arvore[$idTabelaPai] == null){
                        
                        $arvore[$idTabelaPai]->cor = EXTDAO_Tabela::BRANCO;
                        $arvore[$idTabelaPai]->filhos = array();
                        
                    }
                    $corPai = $arvore[$idTabelaPai]->cor ;
                    
                    if($corPai == EXTDAO_Tabela::BRANCO){
                        $arvoreAux = EXTDAO_Tabela::mergetListaTabelaPai($arvore, $idPV, $idBanco, $idTabelaPai);
                        $arvore = $arvoreAux;
                    }
                }
            }
            $arvore[$idTabela]->cor = EXTDAO_Tabela::PRETO;
            $arvore["nodos"][count($arvore["nodos"])] = $idTabela;
            return $arvore;
        }
        
        const BRANCO = 0;
        const CINZA = 1;
        const PRETO = 2;
        public static function mergetListaTabelaDependente($arvore, $idPV, $idBanco, $idTabela, Database $db = null){
            
            
            if($arvore[$idTabela] != null){
                if($arvore[$idTabela]->cor == EXTDAO_Tabela::PRETO)
                    return $arvore;
            } 
                
            $arvore[$idTabela]->cor = EXTDAO_Tabela::CINZA;
            $arvore[$idTabela]->filhos = EXTDAO_Tabela::getListaTabelaFilho(
                $idPV, $idBanco, $idTabela, $db);

            
            
            $vetorPai= EXTDAO_Tabela::getListaTabelaPai(
                $idPV, $idBanco, $idTabela, $db);
            
            $todoPaiPreto = true;
            if(count($vetorPai)){
                
                for($i = 0 ; $i < count($vetorPai); $i++){
                    $idTabelaPai= $vetorPai[$i];
                    if(isset($arvore[$idTabelaPai]) && $arvore[$idTabelaPai] != null){
                        $nodoPai = $arvore[$idTabelaPai];
                        if($nodoPai->cor != EXTDAO_Tabela::PRETO){
                            $todoPaiPreto = false;
                            break;
                        }
                    } else {
                        $todoPaiPreto = false;
                        break;
                    }
                }
            }
            if($todoPaiPreto){
                
                if(!in_array($idTabela, $arvore["nodos"]))
                    $arvore["nodos"][count($arvore["nodos"])] = $idTabela;
                $arvore[$idTabela]->cor = EXTDAO_Tabela::PRETO;
            }
            
            
            if(count($arvore[$idTabela]->filhos)){
                for($i = 0 ; $i < count($arvore[$idTabela]->filhos); $i++){
                    $idTabelaFilho = $arvore[$idTabela]->filhos[$i];
             
                    if(!isset($arvore[$idTabelaFilho]) || $arvore[$idTabelaFilho] == null){
                        $arvore[$idTabelaFilho] = new stdClass();
                        $arvore[$idTabelaFilho]->cor = EXTDAO_Tabela::BRANCO;
                        $arvore[$idTabelaFilho]->filhos = array();
                        
                    }
                    $corFilho = $arvore[$idTabelaFilho]->cor ;
                    
                    if($corFilho != EXTDAO_Tabela::PRETO){
                        $arvoreAux = EXTDAO_Tabela::mergetListaTabelaDependente(
                            $arvore, $idPV, $idBanco, $idTabelaFilho, $db);
                        $arvore = $arvoreAux;
                    }
                }
            }
            
            return $arvore;
        }
        
        public static function getListaTabelaPai(
            $idPV, $idBanco, $idTabela, Database $db = null){
            $q = "SELECT DISTINCT a.fk_tabela_id_INT
                FROM tabela t JOIN atributo a ON a.tabela_id_INT = t.id
                WHERE  t.id = {$idTabela}
                    AND t.projetos_versao_id_INT = $idPV
                    AND t.banco_id_INT = $idBanco
                    AND NOT a.fk_tabela_id_INT IS NULL
                    AND a.fk_tabela_id_INT != $idTabela
                ORDER BY a.fk_tabela_id_INT";
            if($db == null)
                $db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        
        public static function getListaTabela($idPV, $idBanco, $db = null){
            $q = "SELECT DISTINCT t.id
                FROM tabela t 
                WHERE t.projetos_versao_id_INT = $idPV
                    AND t.banco_id_INT = $idBanco
                ORDER BY t.id";
            if($db  == null)$db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        
        public static function getListaNomeTabela($idPV, $idBanco, $db = null){
            $q = "SELECT DISTINCT t.nome, t.id
                FROM tabela t 
                WHERE t.projetos_versao_id_INT = $idPV
                    AND t.banco_id_INT = $idBanco
                ORDER BY t.id";
            if($db  == null)$db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        
        public static function getListaNomeTabelaQueSincronizam($idPV, $idBanco, $db = null){
            $q = "SELECT DISTINCT t.nome, t.id
                FROM tabela t 
                WHERE t.projetos_versao_id_INT = $idPV
                    AND t.banco_id_INT = $idBanco
                    AND (t.frequencia_sincronizador_INT = -1
                    OR t.frequencia_sincronizador_INT = 1)
                    AND (t.transmissao_web_para_mobile_BOOLEAN  = 1
                    AND t.transmissao_mobile_para_web_BOOLEAN  = 1)
                ORDER BY t.id";
            
            if($db  == null)$db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getListaTabelaFilho($idPV, $idBanco, $idTabela, $db = null){
            $q = "SELECT DISTINCT t.id
                FROM tabela t
                WHERE t.projetos_versao_id_INT = $idPV 
                    AND t.banco_id_INT = $idBanco
                    AND t.id != $idTabela
                    AND (
                        SELECT COUNT(a.id)
                        FROM atributo a
                        WHERE a.tabela_id_INT = t.id AND a.fk_tabela_id_INT = {$idTabela}
                        LIMIT 0,1 ) >= 1
                ORDER BY t.id";
//            echo $q;
//            exit();
            if($db == null)
                $db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getListaTabelaRaiz($idPV, $idBanco, $db = null){
            $q = "SELECT t.id
                FROM tabela t
                WHERE t.projetos_versao_id_INT = $idPV 
                    AND t.banco_id_INT = $idBanco
                    AND (
                        SELECT COUNT(a.id)
                        FROM atributo a
                        WHERE a.tabela_id_INT = t.id AND NOT a.fk_tabela_id_INT IS NULL AND a.fk_tabela_id_INT != t.id
                        LIMIT 0,1 ) = 0
                ORDER BY t.id";
//            echo $q;
//            exit();
            if($db == null)
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getCaminhoAteTabelaRaiz(){
            
        }
        
        public function getTabelasPai($idTabelaRaiz, $vetorProcurado){
            
            $vetorProcurado[count($vetorProcurado)] = $idTabelaRaiz;
            $vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($idTabelaRaiz);
            
            for($i = 0 ; $i < count($vetorIdAtributo); $i++){
                $idAtributo = $vetorIdAtributo[$i];
                $objAtributo = new EXTDAO_Atributo();
                $objAtributo->select($idAtributo);
                $idTabelaFk = $objAtributo->getFk_tabela_id_INT();

                if(strlen($idTabelaFk)){
                    if(!in_array($idTabelaFk, $vetorProcurado)){
                        $objTabelaFk = new EXTDAO_Tabela();
                        $objTabelaFk->select($idTabelaFk);
                        
                        $vetorProcuradoAux = getTabelasPai($vetorProcurado);
                        
                        if(count($vetorProcuradoAux)){
                            $vetorProcurado = $vetorProcuradoAux;
                        }
                    }
                }
            }
            return $vetorProcurado;
        }

        public function getListaObjAtributoChaveExtrangeira(){
            $db = new Database();
            //ALTER TABLE `teste` 
//ADD FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`)
            
            $db->query("SELECT a.id, t.nome, a.nome, t2.nome, a2.nome
                FROM atributo a JOIN tabela t ON a.tabela_id_INT = t.id
                    JOIN tabela t2 ON a.fk_tabela_id_INT = t2.id
                    JOIN atributo a2 ON a.fk_atributo_id_INT = a2.id
                WHERE a.fk_tabela_id_INT IS NOT NULL 
                    AND a.tabela_id_INT ={$this->getId()}
                    AND a.fk_atributo_id_INT IS NOT NULL
                GROUP BY a.id");
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public function getListaIdAtributoChaveUnica(){
            $db = new Database();
            
            $db->query("SELECT a.id, a.unique_nome, a.nome
                FROM atributo a JOIN tabela t ON a.tabela_id_INT = t.id 
                WHERE a.unique_nome IS NOT NULL AND LENGTH(a.unique_nome) > 0 AND a.tabela_id_INT ={$this->getId()}
                ORDER BY a.unique_nome");
            $vVetorChaveUnica = Helper::getResultSetToMatriz($db->result);
            
            if(count($vVetorChaveUnica) > 0){
                $vUltimoNome = "";
                $vVetorAtributo = array();
                $vVetorRetorno = array();
                if($vVetorChaveUnica != null)
                foreach ($vVetorChaveUnica as $vObjChaveUnica) {
                    $vObj = new EXTDAO_Atributo();
                    $vObj->select($vObjChaveUnica[0]);
                            //unique_nome
                    if($vObjChaveUnica[1] !=  $vUltimoNome){
                        if(strlen($vUltimoNome)){
                            $vVetorRetorno[$vUltimoNome] = $vVetorAtributo;
                            $vVetorAtributo = array();
                        }
                        $vUltimoNome = $vObjChaveUnica[1];
                    }
                    if($vObjChaveUnica[1] ==  $vUltimoNome){
                        $vVetorAtributo[count($vVetorAtributo)] = $vObj;
                    } 
                }
                if(strlen($vUltimoNome)){
                    $vVetorRetorno[$vUltimoNome] = $vVetorAtributo;
                    $vVetorAtributo = array();
                }
                return $vVetorRetorno ;
            }
            return null;
        }
        
        public static function getVetorIdAtributo($pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.id
FROM atributo a 
WHERE a.tabela_id_INT = {$pIdTabela}");
                
                
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        
        public static function getNomeTabela($idTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT t.nome
FROM tabela t
WHERE t.id = $idTabela");
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        public static function getIdTabela($idPV, $idBanco, $nome, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT t.id
FROM tabela t
WHERE t.projetos_versao_id_INT = $idPV 
       AND t.banco_id_INT = $idBanco
       AND t.nome = '$nome' ");
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        public static function getIdentificadorTabela($idPV, $idBanco, $nome, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT t.id, t.nome
FROM tabela t
WHERE t.projetos_versao_id_INT = $idPV 
       AND t.banco_id_INT = $idBanco
       AND t.nome = '$nome' ";
            
            $db->query($q);
            $ret = Helper::getResultSetToMatriz($db->result);
            if(count($ret)){
                return $ret[0];
            } else {
                return null;
            }
        }
        
        public static function getVetorTabela($idPV, $idBanco, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT t.nome
FROM tabela t
WHERE t.projetos_versao_id_INT = $idPV 
       AND t.banco_id_INT = $idBanco");
                
                
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        public static function getVetorIdentificadorAtributo($pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.id, a.nome
FROM atributo a 
WHERE a.tabela_id_INT = {$pIdTabela}");
                
                
            return Helper::getResultSetToMatriz($db->result);
        }
        
        
        public static function getVetorIdAtributoFk($pIdTabela){
            $db = new Database();
            $db->query("SELECT a.id
FROM atributo a 
WHERE a.tabela_id_INT = {$pIdTabela}
    AND a.fk_tabela_id_INT NOT IS NULL ");
                
                
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getNomeAtributosDaChavePrimaria($pIdTabelaTabela, $pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.nome, aa.id
                FROM atributo a JOIN atributo_atributo aa 
                    ON a.id = aa.producao_atributo_id_INT 
                        OR a.id = aa.homologacao_atributo_id_INT
                WHERE a.tabela_id_INT = {$pIdTabela} 
                    AND aa.tabela_tabela_id_INT ={$pIdTabelaTabela}
                AND a.primary_key_BOOLEAN = 1
                ORDER BY aa.id");
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getNomeAtributosSemSerDaChavePrimaria($pIdTabelaTabela, $pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT a.nome, aa.id
                FROM atributo a JOIN atributo_atributo aa ON 
                 a.id = aa.producao_atributo_id_INT 
                        OR a.id = aa.homologacao_atributo_id_INT
                WHERE a.tabela_id_INT = {$pIdTabela} 
                    AND aa.tabela_tabela_id_INT ={$pIdTabelaTabela}
                AND a.primary_key_BOOLEAN = 0
                ORDER BY aa.id");
            return Helper::getResultSetToArrayDeUmCampo($db->result);
                
        }
        
        public static function getNomeAtributosOrdenados($idPVBB, $pIdTabelaTabela, $pIdTabela, $db = null){
            if($db== null)
                $db = new Database();
            $db->query("SELECT a.nome, aa.id
                FROM atributo a JOIN atributo_atributo aa ON 
                    a.id = aa.producao_atributo_id_INT 
                        OR a.id = aa.homologacao_atributo_id_INT
                WHERE a.tabela_id_INT = {$pIdTabela} 
                    AND aa.tabela_tabela_id_INT ={$pIdTabelaTabela}
                    AND aa.projetos_versao_banco_banco_id_INT = $idPVBB
                ORDER BY aa.id");
            return Helper::getResultSetToArrayDeUmCampo($db->result);
                
        }

         public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");
            $idPVBB = Helper::POST(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){
                $this->clear();
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
                $vObjTT = EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $this->getId(), null, false);
                if($vObjTT == null){
                    Helper::imprimirMensagem("Não existe relacionamento tabela_tabela mapeado. Tente atualziar a estrutura de tabelas.", MENSAGEM_ERRO);
                    exit();
                }
                
                if(strlen(Helper::POST("inserir_tuplas_na_homologacao_BOOLEAN{$i}") ))
                    $vObjTT->setInserir_tuplas_na_homologacao_BOOLEAN(Helper::POST("inserir_tuplas_na_homologacao_BOOLEAN{$i}"));
                
                if(strlen(Helper::POST( "remover_tuplas_na_homologacao_BOOLEAN{$i}")) )
                    $vObjTT->setRemover_tuplas_na_homologacao_BOOLEAN(Helper::POST( "remover_tuplas_na_homologacao_BOOLEAN{$i}"));
                
                if(strlen(Helper::POST("editar_tuplas_na_homologacao_BOOLEAN{$i}") ))
                    $vObjTT->setEditar_tuplas_na_homologacao_BOOLEAN(Helper::POST("editar_tuplas_na_homologacao_BOOLEAN{$i}"));
                
                if(strlen(Helper::POST("tabela_sistema_BOOLEAN{$i}")))
                    $vObjTT->setTabela_sistema_BOOLEAN (Helper::POST("tabela_sistema_BOOLEAN{$i}"));
                
                

                $vObjTT->formatarParaSQL();
                $vObjTT->update($vObjTT->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }
        
    
        
        public static function getVetorTabelaDoBanco(
            $pIdProjetoVersao, $pIdBanco, Database $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT t.id, t.nome
FROM tabela t 
WHERE t.projetos_versao_id_INT= $pIdProjetoVersao AND 
    t.banco_id_INT = $pIdBanco ");
                
                
            return Helper::getResultSetToMatriz($db->result);
        }
        
        public static function getVetorTabelaSemDependenciaDoBanco(
            $pIdProjetoVersao, $pIdBanco, Database $db = null){
            if($db == null)$db = new Database();
            $db->query("SELECT t.id, t.nome
FROM tabela t 
WHERE t.projetos_versao_id_INT= $pIdProjetoVersao AND 
    t.banco_id_INT = $pIdBanco 
                    AND (
                    SELECT COUNT(a.id)
                    FROM atributo a
                    WHERE a.tabela_id_INT = t.id
                        AND a.fk_tabela_id_INT IS NULL
                    LIMIT 0, 1
                    ) = 0");
                
                
            return Helper::getResultSetToMatriz($db->result);
        }
        
        
        public static function getDadosAtributosFk($id, Database $db = null){
            if($db == null)
                $db = new Database();
            $consulta="SELECT a.id, a.nome, t2.nome, a2.nome
                FROM atributo a 
                    JOIN tabela t ON a.tabela_id_INT = t.id
                    LEFT JOIN tabela t2 ON a.fk_tabela_id_INT = t2.id
                    LEFT JOIN atributo a2 ON a.fk_atributo_id_INT = a2.id
                WHERE t.id = '$id' ";
//            echo $consulta;
            $db->query($consulta);
            $dados = Helper::getResultSetToMatriz($db->result, false, true, false);
            
            return $dados;
            
        }
        
        public static function getDadosDasTabelas(array $ids, Database $db){
            $ret = array();
            
            for($i = 0 ; $i < count($ids);){
                $strIn = "";
                for($j = 0 ; $j < 20 && $i < count($ids); $j++, $i++){
                    if($j > 0)
                        $strIn .= ", ";
                    $strIn .= $ids[$i];
                }
                if(!empty($strIn)){
                    
                    $q = "SELECT
	t.id,
	t.nome,
	a.id,
	a.nome,
	a.fk_tabela_id_INT,
        t2.nome
FROM
	tabela t
LEFT JOIN atributo a ON a.tabela_id_INT = t.id AND a.fk_tabela_id_INT IS NOT NULL
LEFT JOIN tabela t2 ON a.fk_tabela_id_INT = t2.id
WHERE t.id IN ($strIn)
ORDER BY t.id";
                    $db->query($q);
                    if($db->result){
                        
                        $atributos = array();
                        $objT = null;
                        while ($obj = $db->result->fetch_array (MYSQL_NUM)) {
                            if($objT == null || $objT->id != $obj[0]){
                                if($objT != null){
                                    $objT->atributosFks = $atributos;
                                    $ret[count($ret)] = $objT;
                                } 
                                $objT = new stdClass();
                                $objT->id = $obj[0];
                                $objT->nome = $obj[1];
                                $atributos = array();
                            }
                            
                            if(isset($obj[2])){
                                $attr = new stdClass();
                                $attr->id = $obj[2];
                                $attr->nome = $obj[3];
                                $attr->idTabelaFk = $obj[4];
                                $attr->tabelaFk = $obj[5];
                                $atributos[count($atributos)] = $attr;
                            }
                        }
                        if($objT != null){
                            $objT->atributosFks = $atributos;
                            $ret[count($ret)] = $objT;
                        }
                    }
                }
            }
            for($i = 0 ; $i < count($ret); $i++){
                $souFk = false;
                for($j = 0 ; $j < count($ret); $j++){
                    if($i == $j 
                        || empty($ret[$j]->atributosFks))
                        continue;
                    else{
                        foreach ($ret[$j]->atributosFks as  $atributo) {
                            if($atributo->idTabelaFk == $ret[$i]->id){
                                $souFk = true;
                                break;
                            }
                        }
                        if($souFk)
                            break;
                    }
                }
                $ret[$i]->souFk=$souFk;
            }
            return $ret;
        }
        
	}

    
