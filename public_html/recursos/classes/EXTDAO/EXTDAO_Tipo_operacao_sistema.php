<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_operacao_sistema
    * NOME DA CLASSE DAO: DAO_Tipo_operacao_sistema
    * DATA DE GERAÇÃO:    31.03.2013
    * ARQUIVO:            EXTDAO_Tipo_operacao_sistema.php
    * TABELA MYSQL:       tipo_operacao_sistema
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_operacao_sistema extends DAO_Tipo_operacao_sistema
    {
        const TRANSFERIR_ARQUIVO_BANCO_SQLITE_DO_SERVIDOR_PARA_MOBILE = 1;
        const EXECUTA_SCRIPT = 2;
        const DOWNLOAD_BANCO_SQLITE_MOBILE = 7;
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tipo_operacao_sistema";



        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tipo_operacao_sistema();

        }

	}

    
