<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_banco
    * NOME DA CLASSE DAO: DAO_Projetos_banco
    * DATA DE GERAÇÃO:    29.01.2013
    * ARQUIVO:            EXTDAO_Projetos_banco.php
    * TABELA MYSQL:       projetos_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos_banco extends DAO_Projetos_banco
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_banco";



        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_homologacao_banco_id_INT = "Banco de Homologação";
			$this->label_producao_banco_id_INT = "Banco de Produção";
			$this->label_tipo_banco_id_INT = "Tipo de Banco";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_banco();

        }

	}

    
