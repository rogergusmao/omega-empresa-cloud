<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Processo_estrutura_caminho
    * NOME DA CLASSE DAO: DAO_Processo_estrutura_caminho
    * DATA DE GERAÇÃO:    22.04.2013
    * ARQUIVO:            EXTDAO_Processo_estrutura_caminho.php
    * TABELA MYSQL:       processo_estrutura_caminho
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Processo_estrutura_caminho extends DAO_Processo_estrutura_caminho
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Processo_estrutura_caminho";

         

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_tipo_processo_estrutura_id_INT = "Tipo de Processo de Atualização de Estrutura";
			$this->label_processo_estrutura_id_INT = "Processo de Atualização de Estrutura";
			$this->label_seq_INT = "Sequência";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Processo_estrutura_caminho();

        }
        
        public function getDescricao(){
            $obj = new EXTDAO_Tipo_processo_estrutura();
            $obj->select($this->getTipo_processo_estrutura_id_INT());
            return $obj->getNome();
            
        }

	}

    
