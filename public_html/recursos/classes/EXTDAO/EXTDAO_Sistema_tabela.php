<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_tabela
    * NOME DA CLASSE DAO: DAO_Sistema_tabela
    * DATA DE GERAï¿½ï¿½O:    21.06.2014
    * ARQUIVO:            EXTDAO_Sistema_tabela.php
    * TABELA MYSQL:       sistema_tabela
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAï¿½ï¿½O DA CLASSE
    // **********************

    class EXTDAO_Sistema_tabela extends DAO_Sistema_tabela
    {
        static $hashTabelas = array();
        
       
       public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_tabela";



        }
        public function getAtributosChaveUnica(){
            $strAtributos = $this->getAtributos_chave_unica();
            if(strlen($strAtributos)){

                $attrs = Helper::explode(",", $strAtributos);
                for($i = 0 ; $i < count($attrs); $i++){
                    $attrs[$i] = trim( $attrs[$i]);
                }
                return $attrs;
            }
            return null;
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_data_modificacao_estrutura_DATETIME = "Data de Modificaï¿½ï¿½o da Estrutura";
			$this->label_frequencia_sincronizador_INT = "Frequï¿½ncia do Sincronizador";
			$this->label_banco_mobile_BOOLEAN = "Banco Mobile?";
			$this->label_banco_web_BOOLEAN = "Banco Web?";
			$this->label_transmissao_web_para_mobile_BOOLEAN = "Transmissao Web Para Mobile?";
			$this->label_transmissao_mobile_para_web_BOOLEAN = "Transmissï¿½o Mobile Para Web?";
			$this->label_tabela_sistema_BOOLEAN = "Tabela do Sistema?";
			$this->label_excluida_BOOLEAN = "Excluida?";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Sistema_tabela();

        }
        
        
        
        public static function getIdSistemaTabela($nomeTabela,  $db = null){
            if($db == null){
                $db = new Database();
            }
            if(isset(EXTDAO_Sistema_tabela::$hashTabelas[$nomeTabela])){
                return EXTDAO_Sistema_tabela::$hashTabelas[$nomeTabela];
            }
            
            $db->query("SELECT id"
                . " FROM sistema_tabela"
                . " WHERE nome LIKE '$nomeTabela'");
            $id = $db->getPrimeiraTuplaDoResultSet(0);
            if(!strlen($id)) return null;
            else {
                EXTDAO_Sistema_tabela::$hashTabelas[$nomeTabela] = $id;
                return $id;
            }
        }
        
        
        
        
        public static function getTabelasDownload($db = null){
            
            $q = "SELECT DISTINCT(st.id)"
                . " FROM sistema_tabela st"
                . " WHERE st.transmissao_web_para_mobile_BOOLEAN = '1' "
                . " AND st.frequencia_sincronizador_INT = '-1' ";
            if($db ==null){
                $db = new Database();
            }
            $db->query($q);
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
        public static function getTabelasUpload($db = null){
            
            $q = "SELECT st.id, st.nome"
                . " FROM sistema_tabela st"
                . " WHERE st.transmissao_mobile_para_web_BOOLEAN = '1' "
                . " AND st.frequencia_sincronizador_INT = '-1' ";
            if($db ==null){
                $db = new Database();
            }
            $db->query($q);
            $ids = Helper::getResultSetToMatriz($db->result, 1, 0);
            return $ids;
        }
        
        public static function getNomeDaTabela($idSistemaTabela, $db=null){
            $db=new Database();
            $db->query("SELECT nome "
                . " FROM sistema_tabela"
                . " WHERE id=$idSistemaTabela");
            $id= $db->getPrimeiraTuplaDoResultSet(0);
            return $id;
        }
        

        public static function getIdDaSistemaTabela($pNomeTabela){
            if(strlen($pNomeTabela) > 0 ){   

                $objBanco = new Database();

                    //checa a existencia do funcionario
                    $query = "SELECT ts.id as id
                              FROM sistema_tabela ts
                              WHERE ts.nome = '".Helper::formatarCampoTextoHTMLParaSQL($pNomeTabela)."' ";


                    $objBanco->query($query);
                    
                    $id = $objBanco->getPrimeiraTuplaDoResultSet("id");
//                    $objBanco->close();
                        //o usuario ja eh existente
                    if (strlen($id)) {
                        
                        return $id;
                    }
                    else return null;
                }
            }
            
    

    public static function atualizaSistemaTabelaDoBancoSincronizadorWeb(
        $objBancoSrc, 
        $objBancoTgt, 
        $db = null) {
//        $objBBSincronizadorWeb = new EXTDAO_Banco_banco();
//        $objBBSincronizadorWeb->select( $idBancoBancoSincronizadorWeb);
//        $databaseSW = $objBBSincronizadorWeb->getObjDatabase();
//        
        if($db == null)
            $db = new Database();
//            $vObjBancoHomologacao->select($vIdBancoHomologacao);
        $dbWebSrc = $objBancoSrc->getObjDatabase();
        $dbSincronizadorWebTgt = $objBancoTgt->getObjDatabase();
        $dbSincronizadorWebTgt->query("SELECT id FROM sistema_tabela");
        $idsSistemaTabelaSincronizadorWebTgt = Helper::getResultSetToArrayDeUmCampo($dbSincronizadorWebTgt->result);
        $idsEncontrados = array();
        $dbWebSrc->query("SELECT
                             id,
                        nome, 
                        transmissao_web_para_mobile_BOOLEAN,
                        transmissao_mobile_para_web_BOOLEAN ,
                        frequencia_sincronizador_INT,
                        banco_mobile_BOOLEAN,
                        banco_web_BOOLEAN,
                        atributos_chave_unica,
                        excluida_BOOLEAN
                      FROM sistema_tabela");
        
        
        if(mysqli_num_rows($dbWebSrc->result) > 0){
            mysqli_data_seek($dbWebSrc->result, 0);
        }
        
        for($i=0; $registroSrc = mysqli_fetch_array($dbWebSrc->result, MYSQL_NUM); $i++){

            $reg1 = Helper::formatarStringParaComandoSQL($registroSrc[1]);
            $reg2 = Helper::formatarBooleanParaComandoSQL($registroSrc[2]);
            $reg3 = Helper::formatarBooleanParaComandoSQL($registroSrc[3]);
            $reg4 = Helper::formatarIntegerParaComandoSQL($registroSrc[4]);
            $reg5 = Helper::formatarBooleanParaComandoSQL($registroSrc[5]);
            $reg6 = Helper::formatarBooleanParaComandoSQL($registroSrc[6]);
            $reg7 = Helper::formatarStringParaComandoSQL($registroSrc[7]);
            $reg8 = Helper::formatarBooleanParaComandoSQL($registroSrc[8]);

            if(in_array($registroSrc[0], $idsSistemaTabelaSincronizadorWebTgt)){
                $idsEncontrados[count($idsEncontrados)] = $registroSrc[0];
                $qUpdate = "UPDATE sistema_tabela
SET nome = $reg1, 
    transmissao_web_para_mobile_BOOLEAN= $reg2,
    transmissao_mobile_para_web_BOOLEAN = $reg3,
    frequencia_sincronizador_INT= $reg4,
    banco_mobile_BOOLEAN= $reg5,
    banco_web_BOOLEAN= $reg6,
    atributos_chave_unica= $reg7,
    excluida_BOOLEAN= $reg8
WHERE id = '{$registroSrc[0]}'";
               $msg = $dbSincronizadorWebTgt->queryMensagem($qUpdate);
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));
            } else {
                
           echo "lsdk: $reg1";
            $qInsert = "INSERT sistema_tabela
                      ( id,
                        nome, 
                        transmissao_web_para_mobile_BOOLEAN,
                        transmissao_mobile_para_web_BOOLEAN ,
                        frequencia_sincronizador_INT,
                        banco_mobile_BOOLEAN,
                        banco_web_BOOLEAN,
                        atributos_chave_unica,
                        excluida_BOOLEAN)
                      VALUES(
                           '{$registroSrc[0]}',
                           $reg1, 
                           $reg2, 
                           $reg3, 
                           $reg4,
                           $reg5, 
                           $reg6,
                           $reg7,
                           $reg8 
                       )";    
                $msg = $dbSincronizadorWebTgt->queryMensagem($qInsert);
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));
            }
        }
        for($i = 0 ; $i < count($idsSistemaTabelaSincronizadorWebTgt); $i++){
            if(!in_array($idsSistemaTabelaSincronizadorWebTgt[$i], $idsEncontrados)){
                $msg = $dbSincronizadorWebTgt->queryMensagem("UPDATE sistema_tabela "
                    . " SET excluido_BOOLEAN = '1' "
                    . " WHERE id = '{$idsSistemaTabelaSincronizadorWebTgt[$i]}'");
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));
            }
        }
    }
    public static function atualizaSistemaTabelaDoBanco(
        $objBancoWeb, 
        $objBancoAndroid, 
        $objBancoSincronizadorWeb, 
        $idPVPrototipoWeb,
        $idPVPrototipoAndroid,
        $db = null) {

        if($db == null)
            $db = new Database();
        
        $idBancoWeb = $objBancoWeb->getId();
        $idBancoAndroid = $objBancoAndroid->getId();
        
        $dbWeb = $objBancoWeb->getObjDatabase();
        
        $dbSincronizadorWeb = $objBancoSincronizadorWeb->getObjDatabase();
        
        $dbWeb->query("SELECT id, nome
                      FROM sistema_tabela");
        $objsSistemaTabelaWeb =  Helper::getResultSetToMatriz($dbWeb->result, 0 , 1);
        
        $tabelasDoSistemaTabelaWeb = array();
        for($j = 0 ; $j < count($objsSistemaTabelaWeb); $j++){
            $tabelasDoSistemaTabelaWeb[$j] = $objsSistemaTabelaWeb[$j][1];
        }
        
        $q = "SELECT t.nome, 
                        t.transmissao_web_para_mobile_BOOLEAN, 
                        t.transmissao_mobile_para_web_BOOLEAN , 
                        t.frequencia_sincronizador_INT
                    FROM tabela t 
                    WHERE t.projetos_versao_id_INT = $idPVPrototipoWeb 
                        AND t.banco_id_INT = $idBancoWeb";
        
        $db->query($q);
        $registrosTabelaDoPrototipoWeb = Helper::getResultSetToMatriz($db->result, 0 , 1);
//        echo $q;
//        print_r($registrosTabelaDoPrototipoWeb);
//        exit();
        $nomesTabelasPrototipoWeb = array();
        for($i = 0; $i < count($registrosTabelaDoPrototipoWeb); $i++){
            $nomesTabelasPrototipoWeb[count($nomesTabelasPrototipoWeb)] = $registrosTabelaDoPrototipoWeb[0];
        }
//        print_r($q);
//        exit();
        $q = "SELECT t.nome, 
                        t.transmissao_web_para_mobile_BOOLEAN, 
                        t.transmissao_mobile_para_web_BOOLEAN , 
                        t.frequencia_sincronizador_INT
                    FROM tabela t 
                    WHERE t.projetos_versao_id_INT = $idPVPrototipoAndroid
                        AND t.banco_id_INT = $idBancoAndroid";
        
        $db->query($q);

        $registrosTabelaDoPrototipoAndroid = Helper::getResultSetToMatriz($db->result, 0 , 1);
        $nomesTabelasPrototipoAndroid = array();
        for($i = 0; $i < count($registrosTabelaDoPrototipoAndroid); $i++){
            $nomesTabelasPrototipoAndroid[count($nomesTabelasPrototipoAndroid)] = $registrosTabelaDoPrototipoAndroid[0];
        }
        $tabelasSistemaTabelaWebPorFlagEncontrada = array();
        for ($i = 0; $i < count($registrosTabelaDoPrototipoWeb); $i++) {
            $banco_mobile_BOOLEAN = "0";
            
            if(!in_array($registrosTabelaDoPrototipoWeb[$i][0] , $nomesTabelasPrototipoAndroid)){
                $banco_mobile_BOOLEAN = "1";
            } else {
                $banco_mobile_BOOLEAN = "0";
            }
            $banco_web_BOOLEAN = "1";

            //se o sistema tabela jï¿½ existe
            //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
            //da 'tabela' deve ser atualizada
            $idSistemaTabelaWeb = array_search($registrosTabelaDoPrototipoWeb[$i][0], 
                $tabelasDoSistemaTabelaWeb);

            $transmissaoWebMobile = Helper::formatarBooleanParaComandoSQL($registrosTabelaDoPrototipoWeb[$i][1]);
            $transmissaoMobileWeb = Helper::formatarBooleanParaComandoSQL($registrosTabelaDoPrototipoWeb[$i][2]);
            $frequencia_sincronizador_INT = Helper::formatarIntegerParaComandoSQL($registrosTabelaDoPrototipoWeb[$i][3]);
            $nomeTabelaWeb = Helper::formatarStringParaComandoSQL($registrosTabelaDoPrototipoWeb[$i][0]);
//            echo "ID SISTEMA TABELA: $idSistemaTabelaWeb";
//            print_r($tabelasDoSistemaTabelaWeb)
//            exit();
            
            if (is_numeric($idSistemaTabelaWeb)) {
                $tabelasSistemaTabelaWebPorFlagEncontrada[$registrosTabelaDoPrototipoWeb[$i][0]] = true;
                
                $qUpdate = "UPDATE sistema_tabela
                SET transmissao_web_para_mobile_BOOLEAN = $transmissaoWebMobile, 
                    transmissao_mobile_para_web_BOOLEAN = $transmissaoMobileWeb, 
                        frequencia_sincronizador_INT = $frequencia_sincronizador_INT,
                            banco_mobile_BOOLEAN = $banco_mobile_BOOLEAN,
                            banco_web_BOOLEAN = $banco_web_BOOLEAN,    
                            excluida_BOOLEAN = '0'
                WHERE nome =  $nomeTabelaWeb ";
                
                $msg = $dbWeb->queryMensagem($qUpdate);
                if($msg != null && $msg->erroComServidor()) throw new Exception(json_encode($msg));
            } else {
                 $qInsert = "INSERT sistema_tabela
                            (nome, 
                             transmissao_web_para_mobile_BOOLEAN,
                             transmissao_mobile_para_web_BOOLEAN ,
                             frequencia_sincronizador_INT,
                             banco_mobile_BOOLEAN,
                             banco_web_BOOLEAN,
                             excluida_BOOLEAN)
                             VALUES(
                                $nomeTabelaWeb,
                                $transmissaoWebMobile,
                                $transmissaoMobileWeb, 
                                $frequencia_sincronizador_INT, 
                                $banco_mobile_BOOLEAN,
                                $banco_web_BOOLEAN,
                                0
                             )";
                $msg = $dbWeb->queryMensagem($qInsert);
                if($msg != null && $msg->erroComServidor()) throw new Exception(json_encode($msg));
            }
        }
        for ($i = 0; $i < count($registrosTabelaDoPrototipoAndroid); $i++) {
            //se transmissao_web_para_mobile_BOOLEAN == 1

            $banco_web_BOOLEAN = "1";
            //confere se o registro jï¿½ foi atualizado
            if($tabelasSistemaTabelaWebPorFlagEncontrada[$registrosTabelaDoPrototipoAndroid[$i][0]] == true ){
                continue;
            }
            else if(!in_array($registrosTabelaDoPrototipoAndroid[$i][0] , $nomesTabelasPrototipoWeb)){
                $banco_web_BOOLEAN = "1";
            } else {
                $banco_web_BOOLEAN = "0";
            }
            $banco_mobile_BOOLEAN = "1";

            //se o sistema tabela jï¿½ existe

            $transmissaoWebMobile = Helper::formatarBooleanParaComandoSQL($registrosTabelaDoPrototipoAndroid[$i][1]);
            $transmissaoMobileWeb = Helper::formatarBooleanParaComandoSQL($registrosTabelaDoPrototipoAndroid[$i][2]);
            $frequencia_sincronizador_INT = Helper::formatarIntegerParaComandoSQL($registrosTabelaDoPrototipoAndroid[$i][3]);
            $nomeTabelaAndroid = $db->realEscapeString( $registrosTabelaDoPrototipoAndroid[$i][0]);
            
            //Se ja existe a tupla da tabela 'tabela' na tabela 'sistema_tabela', entao a tupla
            //da 'tabela' deve ser atualizada
            $idSistemaTabelaWeb = array_search($registrosTabelaDoPrototipoAndroid[$i][0], $tabelasDoSistemaTabelaWeb);

            if (is_numeric($idSistemaTabelaWeb)) {
                 $qUpdate = "UPDATE sistema_tabela
                        SET transmissao_web_para_mobile_BOOLEAN = $transmissaoWebMobile, 
                            transmissao_mobile_para_web_BOOLEAN = $transmissaoMobileWeb, 
                            frequencia_sincronizador_INT = $frequencia_sincronizador_INT,
                            banco_mobile_BOOLEAN = $banco_mobile_BOOLEAN,
                            banco_web_BOOLEAN = $banco_web_BOOLEAN,    
                            excluida_BOOLEAN = '0'
                        WHERE nome = '$nomeTabelaAndroid'";
                 
                $tabelasSistemaTabelaWebPorFlagEncontrada[$registrosTabelaDoPrototipoAndroid[$i][0]] = true;
                $msg = $dbWeb->queryMensagem($qUpdate);
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));


            } else {
                $qInsert = "INSERT sistema_tabela
                            ( nome, 
                              transmissao_web_para_mobile_BOOLEAN,
                              transmissao_mobile_para_web_BOOLEAN ,
                              frequencia_sincronizador_INT,
                              banco_mobile_BOOLEAN,
                              banco_web_BOOLEAN,
                              excluida_BOOLEAN)
                             VALUES(
                                '$nomeTabelaAndroid',
                                $transmissaoWebMobile, 
                                $transmissaoMobileWeb, 
                                $frequencia_sincronizador_INT,
                                $banco_mobile_BOOLEAN,
                                $banco_web_BOOLEAN,
                                0
                             )";
                $msg = $dbWeb->queryMensagem($qInsert);
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));
            }
        }
       
        //Percorre os registros da tabela 'sistema_tabela' 
        //retira as tabelas que nï¿½o foram encontradas
        for($i = 0 ; $i < count($objsSistemaTabelaWeb); $i++){
           $idSistemaTabela =  $objsSistemaTabelaWeb[$i][0];
           $nomeSistemaTabela =  $objsSistemaTabelaWeb[$i][1];
            //se a tabela contida no sistema_tabela 
            //nao foi encontrada na atual estrutura da base web
            if($tabelasSistemaTabelaWebPorFlagEncontrada[$nomeSistemaTabela]!= true ){
                $msg = $dbWeb->queryMensagem("UPDATE sistema_tabela
                                            SET excluida_BOOLEAN = 1
                                            WHERE id = '{$idSistemaTabela}'");
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));
                $msg = $dbWeb->queryMensagem("UPDATE sistema_atributo "
                    . " SET excluido_BOOLEAN = '1' "
                    . " WHERE sistema_tabela_id_INT='$idSistemaTabela' ");
                if($msg != null && $msg->erroSqlSemSerChaveDuplicada()) throw new Exception(json_encode($msg));
            }
        }
         
    }
    
}

    
