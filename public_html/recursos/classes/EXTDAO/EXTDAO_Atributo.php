<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Atributo
    * NOME DA CLASSE DAO: DAO_Atributo
    * DATA DE GERAÇÃO:    28.01.2013
    * ARQUIVO:            EXTDAO_Atributo.php
    * TABELA MYSQL:       atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Atributo extends DAO_Atributo
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Atributo";

        


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_tabela_id_INT = "Tabela";
			$this->label_tipo_sql = "Tipo Sql";
			$this->label_tamanho_INT = "Tamanho";
                        $this->label_decimal_INT = "Decimal";
			$this->label_not_null_BOOLEAN = "Não Nulo";
                        $this->label_primary_key_BOOLEAN = "Chave Primária";
			$this->label_auto_increment_BOOLEAN = "Auto Incremento";
			$this->label_valor_default = "Default";
			$this->label_fk_tabela_id_INT = "Chave Extrangeira da Tabela";
                        $this->label_fk_atributo_id_INT = "Chave Extrangeira do Atributo";
			$this->label_update_tipo_fk = "On Update";
			$this->label_delete_tipo_fk = "On Delete";
			$this->label_masculino_BOOLEAN = "Masculino";
			$this->label_label = "Título";
			$this->label_unique_BOOLEAN = "Única";
                        $this->label_unique_nome = "Nome Chave Unica";
			$this->label_banco_id_INT = "Banco";
			$this->label_projetos_versao_id_INT = "Versão do Projeto";

                        $this->label_fk_nome = "Nome chave extrangeira";
        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Atributo();

        }

        
        
        
        public function existeAtributo($pAtributo, $pIdTabela, $pIdProjetoVersao){
            
            $this->database->query("SELECT id 
                FROM atributo 
                WHERE nome = '{$pAtributo}' AND tabela_id_INT={$pIdTabela} AND projetos_versao_id_INT={$pIdProjetoVersao}
                LIMIT 0,1");
            return Helper::getResultSetToPrimeiroResultado($this->database->result);
            
        }
        
        public function getListaIdAtributoDaTabela($pIdTabela){
            
            $this->database->query("SELECT id 
                FROM atributo 
                WHERE tabela_id_INT={$pIdTabela}");
            return Helper::getResultSetToArrayDeUmCampo($this->database->result);
        }
        
        public static function getAtributosDaTabela($pIdTabela, $db = null){
            if($db == null) $db = new Database();
            
            $db->query("SELECT nome 
                FROM atributo 
                WHERE tabela_id_INT={$pIdTabela}");
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        
        public static function getIdNomeAtributosDaTabela($pIdTabela, $db = null){
            if($db == null) $db = new Database();
            
            $db->query("SELECT id, nome 
                FROM atributo 
                WHERE tabela_id_INT={$pIdTabela}");
            return Helper::getResultSetToMatriz($db->result);
        }
        
        
        
        public function getListaObjAtributoDaTabela($pIdTabela){
            
            $this->database->query("SELECT id 
                FROM atributo 
                WHERE tabela_id_INT={$pIdTabela}");
            $vVetor = Helper::getResultSetToPrimeiroResultado($this->database->result);
            if($vVetor != null && count($vVetor) > 0){
                $vVetorObj = array();
                if($vVetor != null)
                foreach ($vVetor as $idAtributo) {
                    $vObjAtributo = new EXTDAO_Atributo();
                    $vObjAtributo->select($idAtributo);
                    $vVetorObj[count($vObjAtributo)] = $vObjAtributo;
               }
               return $vVetor;
            }
        }
        
        
        public function getObjFKTabela(){
            
            if($this->getFk_tabela_id_INT() != null){
                $vTabelaHomologacao = new EXTDAO_Tabela();    
                $vTabelaHomologacao->select($this->getFk_tabela_id_INT());
                return $vTabelaHomologacao;
            }
            return null;
        }
        
        public function getObjFKAtributo(){
            
            if($this->getFk_atributo_id_INT() != null){
                $vAtributoHomologacao = new EXTDAO_Atributo();    
                $vAtributoHomologacao->select($this->getFk_atributo_id_INT());
                return $vAtributoHomologacao;
            }
            return null;
        }
        
        
        public function getObjTabela(){
            
            if($this->getTabela_id_INT() != null){
                $vTabela = new EXTDAO_Tabela();    
                $vTabela->select($this->getTabela_id_INT());
                return $vTabela;
            }
            return null;
        }
        
        
        public static function getIdAtributo($pNome, $pIdTabela, $db = null){
            if($db == null) $db = new Database();
            
            $db->query("SELECT id
                FROM atributo
                WHERE nome = '$pNome'
                    AND tabela_id_INT = {$pIdTabela}");
            $vId=  Helper::getResultSetToPrimeiroResultado($db->result);
            if(strlen($vId))return $vId;
            else return null;
                
        }
        public static function getNomeAtributo($id, $db = null){
            if($db == null) $db = new Database();

            $db->query("SELECT nome
                FROM atributo
                WHERE id = '$id'");
            return $db->getPrimeiraTuplaDoResultSet(0);

        }
        public static function getIdsAtributosDoProjetosVersao($idPV, $idBanco, $db = null){
            if($db == null) $db = new Database();
            
            $db->query("SELECT id
                FROM atributo
                WHERE projetos_versao_id_INT = $idPV"
                . " AND banco_id_INT = $idBanco ");
            return  Helper::getResultSetToArrayDeUmCampo($db->result);
            
            
        }
        
        public function getVetorAtributoDaTabelaDoBanco($pIdProjetoVersao, $pIdBanco, $pIdTabela){
            
            $this->database->query("SELECT a.id, a.nome
FROM atributo a 
WHERE a.banco_id_INT = $pIdBanco 
    AND a.projetos_versao_id_INT = {$pIdProjetoVersao} 
    AND tabela_id_INT = $pIdTabela
     ");
                
                
            return Helper::getResultSetToMatriz($this->database->result);
        }
        
        public function tipoSQLReal(){
            $vTipoSQL = $this->getTipo_sql();
            if(substr_count($vTipoSQL, "float") || substr_count($vTipoSQL, "double"))
                    return true;
            else return false;
        }
        public function getDeleteTypeRelationFKAndroid(){
            $var = strtoupper($this->getDelete_tipo_fk());
            switch ($var) {
                case "CASCADE":
                    return "TYPE_RELATION_FK.CASCADE";
                case "SET NULL":
                    return "TYPE_RELATION_FK.SET_NULL";
                case "NO ACTION":
                    return "TYPE_RELATION_FK.NO_ACTION";    
                case "RESTRICT":
                    return "TYPE_RELATION_FK.RESTRICT";
                case "SET DEFAULT":
                    return "TYPE_RELATION_FK.SET_DEFAULT";
                
                default:
                    echo Helper::imprimirMensagem("Relacionamento não identificado.", MENSAGEM_ERRO);
                    return null;
            }
        }
        
        public function getUpdateTypeRelationFKAndroid(){
            $var = strtoupper($this->getUpdate_tipo_fk());
            switch ($var) {
                case "CASCADE":
                    return "TYPE_RELATION_FK.CASCADE";
                case "SET NULL":
                    return "TYPE_RELATION_FK.SET_NULL";
                case "NO ACTION":
                    return "TYPE_RELATION_FK.NO_ACTION";    
                case "RESTRICT":
                    return "TYPE_RELATION_FK.RESTRICT";
                case "SET DEFAULT":
                    return "TYPE_RELATION_FK.SET_DEFAULT";
                
                default:
                    echo Helper::imprimirMensagem("Relacionamento não identificado.", MENSAGEM_ERRO);
                    return null;
            }
        }
        
        
        
        public function getTipoSQLiteDAOAndroid(){
            $var = strtoupper( $this->getTipo_sql());
            switch ($var) {
                case "BIGINT":
                case "INT":
                    return "SQLLITE_TYPE.INTEGER";
                case "INTEGER":
                    return "SQLLITE_TYPE.INTEGER";
                case "CHAR":
                    return "SQLLITE_TYPE.VARCHAR";    
                case "VARCHAR":
                    return "SQLLITE_TYPE.VARCHAR";    
                case "INTEGER":
                    return "SQLLITE_TYPE.INTEGER";
                case "LONG":
                    return "SQLLITE_TYPE.LONG";
                case "VARCHAR":
                    return "SQLLITE_TYPE.TEXT";
                case "TINYTEXT":
                    return "SQLLITE_TYPE.TINYTEXT";
                case "TEXT":
                    return "SQLLITE_TYPE.TEXT";
                case "DOUBLE":
                    return "SQLLITE_TYPE.DOUBLE";
                case "FLOAT":
                    return "SQLLITE_TYPE.DOUBLE";
                case "BLOB":
                    return "SQLLITE_TYPE.BLOB";
                case "REAL":
                    return "SQLLITE_TYPE.REAL";
                case "DATETIME":
                    return "SQLLITE_TYPE.DATETIME";
                case "DATE":
                    return "SQLLITE_TYPE.DATE";
                case "TIME":
                    return "SQLLITE_TYPE.TIME";
                default:
                    echo Helper::imprimirMensagem("Tipo SQLite não detectado: $var", MENSAGEM_ERRO);
                    return null;
            }
        }
        
        
        
        public static function apagaVetorAtributoSemRelacionamento($idBanco, $idPV) {
            $db = new Database();
            $consulta = "DELETE FROM atributo
    WHERE banco_id_INT = $idBanco
                    AND projetos_versao_id_INT = $idPV
    AND (
        SELECT COUNT(atributo.id)
        FROM atributo_atributo aa 
        WHERE aa.homologacao_atributo_id_INT = atributo.id 
                        OR aa.producao_atributo_id_INT = atributo.id
        LIMIT 0,1
    ) = 0";
            $db->query($consulta);
            
            
        }
        
	}

    
