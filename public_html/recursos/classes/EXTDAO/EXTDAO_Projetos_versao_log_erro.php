<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_versao_log_erro
    * NOME DA CLASSE DAO: DAO_Projetos_versao_log_erro
    * DATA DE GERAÇÃO:    01.05.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_log_erro.php
    * TABELA MYSQL:       projetos_versao_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos_versao_log_erro extends DAO_Projetos_versao_log_erro
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_versao_log_erro";

       

        }

//    public function formatarParaSQL(){
//
//        if($this->usuario_id_INT == ""){
//
//                $this->usuario_id_INT = "null";
//
//        }
//
//        if($this->projetos_tipo_log_erro_id_INT == ""){
//
//                $this->projetos_tipo_log_erro_id_INT = "null";
//
//        }
//
//        if($this->projetos_versao_banco_banco_id_INT == ""){
//
//                $this->projetos_versao_banco_banco_id_INT = "null";
//
//        }
//
//        if(strlen($this->descricao_HTML)){
//            $this->descricao_HTML = Helper::formatarCampoConsultaSQLParaSQL($this->descricao_HTML);
//        }
//
//	$this->data_ocorrida_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ocorrida_DATETIME); 
//
//
//    }
        public function setLabels(){

			$this->label_id = "Id";
			$this->label_classe = "Classe";
			$this->label_funcao = "Função";
			$this->label_descricao_HTML = "Descrição";
			$this->label_data_ocorrida_DATETIME = "Data Que Ocorreu";
			$this->label_usuario_id_INT = "Usuário";
			$this->label_sistema_tipo_log_erro_id_INT = "Tipo de Erro";
			$this->label_projetos_versao_banco_banco_id_INT = "Bancos da Versão de Projeto";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_versao_log_erro();

        }

	}

    
