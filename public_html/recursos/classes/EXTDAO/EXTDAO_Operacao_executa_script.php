<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Operacao_executa_script
    * NOME DA CLASSE DAO: DAO_Operacao_executa_script
    * DATA DE GERAÇÃO:    20.06.2013
    * ARQUIVO:            EXTDAO_Operacao_executa_script.php
    * TABELA MYSQL:       operacao_executa_script
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Operacao_executa_script extends DAO_Operacao_executa_script
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Operacao_executa_script";

            

        }

        public function setLabels(){
            $this->label_xml_script_sql_banco_ARQUIVO = "Script xml sql do banco";
            $this->label_id = "Id";
            $this->label_path_script_sql_banco = "Path do Script Sql do Banco";
            $this->label_observacao = "Observação";
            $this->label_operacao_sistema_mobile_id_INT = "Operação do Sistema do Telefone";
            $this->label_destino_banco_id_INT = "Banco de Destino";


        }
        
        

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Operacao_executa_script();

        }

        public function executaProcessoInicializaExecucaoDoScriptDeAtualizacaoDaEstruturaDoMobile(){
            $idPVPE = Helper::POST(Param_Get::ID_PROJETOS_VERSAO_PROCESSO_ESTRUTURA);
            $idPVC = Helper::POST(Param_Get::ID_PROJETOS_VERSAO_CAMINHO);
            $objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
            $objPVPE->select($idPVPE);
            $idPVBB = $objPVPE->getProjetos_versao_banco_banco_id_INT();
            $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBB->select($idPVBB);
            $obj = new EXTDAO_Operacao_executa_script();
            
            $objOSM = new EXTDAO_Operacao_sistema_mobile();
            $objOSM->setData_abertura_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objOSM->setTipo_operacao_sistema_id_INT(EXTDAO_Tipo_operacao_sistema::EXECUTA_SCRIPT);
            $objOSM->formatarParaSQL();
            $objOSM->insert();
            
            $obj->__actionAdd();
            $obj->selectUltimoRegistroInserido();
            $obj->setOperacao_sistema_mobile_id_INT($objOSM->getId());
            $obj->formatarParaSQL();
            $obj->update($obj->getId());
            
            
            $url = EXTDAO_Projetos_versao_processo_estrutura::actionUrlExecutaProcesso(true);
            
            return array("location: $url");
        }
        
	}

    
