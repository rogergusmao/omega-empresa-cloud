<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Lista
    * NOME DA CLASSE DAO: DAO_Lista
    * DATA DE GERAÇÃO:    07.02.2015
    * ARQUIVO:            EXTDAO_Lista.php
    * TABELA MYSQL:       lista
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Lista extends DAO_Lista
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Lista";

      

        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_nome = "Nome";
			$this->label_descricao = "Descrição";
			$this->label_tabela_id_INT = "Tabela";
			$this->label_ultima_alteracao_usuario_id_INT = "Usuário Que Alterou Pela última Vez";
			$this->label_ultima_alteracao_DATETIME = "Data da última Alteração";
			$this->label_criacao_DATETIME = "Data da Criação";
			$this->label_criador_usuario_id_INT = "Usuário Que Criou";
			$this->label_projetos_versao_id_INT = "Versão do Projeto";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Lista();

        }
        
        public static function salvar(){
            echo $POST;
            exit();
        }

	}

    
