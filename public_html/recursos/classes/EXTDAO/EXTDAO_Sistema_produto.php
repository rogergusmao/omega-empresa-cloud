<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_produto
    * NOME DA CLASSE DAO: DAO_Sistema_produto
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Sistema_produto.php
    * TABELA MYSQL:       sistema_produto
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_produto extends DAO_Sistema_produto
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Sistema_produto";

          
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_sistema_id_INT = "Sistema";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Sistema_produto();

        }
        
        
        
         public static function getListaIdentificadorNo($idSistema){
            $q = "SELECT DISTINCT sp.id, sp.nome
                FROM sistema s JOIN sistema_produto sp ON sp.sistema_id_INT = s.id
                WHERE s.id = $idSistema 
                ORDER BY sp.nome";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }

        
        public static function getIdDaUltimaVersao($idSistemaProduto, $db = null){
            $query = "SELECT DISTINCT(id_produto)
FROM (
(
    SELECT DISTINCT (spvspm.sistema_projetos_versao_produto_id_INT) id_produto



    FROM sistema_projetos_versao_sistema_produto_mobile spvspm



    JOIN sistema_projetos_versao_produto spvp



    ON spvspm.sistema_projetos_versao_produto_id_INT = spvp.id



    JOIN sistema_produto_mobile spm



    ON spm.id = spvspm.sistema_produto_mobile_id_INT



    JOIN sistema_projetos_versao spv



    ON spv.id = spvspm.sistema_projetos_versao_id_INT 



    WHERE spvp.sistema_produto_id_INT = $idSistemaProduto



    AND spv.data_producao_DATETIME IS NULL



    ORDER BY spv.data_homologacao_DATETIME DESC



    LIMIT 0,1
    )



UNION ALL 

    (

    SELECT DISTINCT (spvspw.sistema_projetos_versao_produto_id_INT) id_produto



    FROM sistema_projetos_versao_sistema_produto_web spvspw



    JOIN sistema_projetos_versao_produto spvp



    ON spvspw.sistema_projetos_versao_produto_id_INT = spvp.id



    JOIN sistema_produto_web spw



    ON spw.id = spvspw.sistema_produto_web_id_INT



    JOIN sistema_projetos_versao spv



    ON spv.id = spvspw.sistema_projetos_versao_id_INT 



    WHERE spvp.sistema_produto_id_INT = $idSistemaProduto



    AND spv.data_producao_DATETIME IS NULL



    ORDER BY spv.data_homologacao_DATETIME DESC



    LIMIT 0,1
    ) 

)sistema_produto_2

LIMIT 0,1";
            if($db == null)
            $db = new Database();
            $db->query($query);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        
	}

    
