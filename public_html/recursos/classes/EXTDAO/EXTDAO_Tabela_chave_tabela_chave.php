<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tabela_chave_tabela_chave
    * NOME DA CLASSE DAO: DAO_Tabela_chave_tabela_chave
    * DATA DE GERAÇÃO:    20.04.2013
    * ARQUIVO:            EXTDAO_Tabela_chave_tabela_chave.php
    * TABELA MYSQL:       tabela_chave_tabela_chave
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tabela_chave_tabela_chave extends DAO_Tabela_chave_tabela_chave
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tabela_chave_tabela_chave";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_homologacao_tabela_chave_id_INT = "Chave da Tabela de Homologação";
			$this->label_producao_tabela_chave_id_INT = "Chave da Tabela de Produção";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tabela_chave_tabela_chave();

        }
        
        public function getObjProducao(){
            if(strlen($this->getProducao_tabela_chave_id_INT())){
                $obj = new EXTDAO_Tabela_chave();
                $obj->select($this->getProducao_tabela_chave_id_INT());
                return $obj;
            }
            return null;
        }
        
        public function getObjHomologacao(){
            if(strlen($this->getHomologacao_tabela_chave_id_INT())){
                $obj = new EXTDAO_Tabela_chave();
                $obj->select($this->getHomologacao_tabela_chave_id_INT());
                return $obj;
            }
            return null;
        }
        const FATOR_NOME = 1;
        const FATOR_INDEX_TYPE = 2;
        const FATOR_INDEX_METHOD = 3;
        const FATOR_NON_UNIQUE_BOOLEAN = 4;
        public function comparaChaveHomologacaoComProducao(){
            $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            $vFatores = array();
            $vFatoresTipoOperacao = array();
            if(!strlen($this->getProducao_tabela_chave_id_INT())) 
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
            else if(!strlen($this->getHomologacao_tabela_chave_id_INT()))
                $vTipoModificacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
            else if($this->getHomologacao_tabela_chave_id_INT() != null && 
                    $this->getProducao_tabela_chave_id_INT() != null){
                $vObjHom = $this->getObjHomologacao();
                $vObjProd = $this->getObjProducao();
                if($vObjHom->getNome() != $vObjProd->getNome()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Tabela_chave_tabela_chave::FATOR_NOME;
                    if(strlen($vObjHom->getNome()) &&
                            !strlen($vObjProd->getNome()))
                        $vFatoresTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjHom->getNome()) &&
                            strlen($vObjProd->getNome()))
                        $vFatoresTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }
                else{
                    
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
                }
                
                if($vObjHom->getIndex_method() != $vObjProd->getIndex_method()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_TYPE;
                    if(strlen($vObjHom->getIndex_method()) &&
                            !strlen($vObjProd->getIndex_method()))
                        $vFatoresTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_TYPE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjHom->getIndex_method()) &&
                            strlen($vObjProd->getIndex_method()))
                        $vFatoresTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_TYPE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_TYPE] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }
                else{
                    
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
                }
                
                if($vObjHom->getNon_unique_BOOLEAN() != $vObjProd->getNon_unique_BOOLEAN()){
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                    $vFatores[count($vFatores)] = EXTDAO_Tabela_chave_tabela_chave::FATOR_NON_UNIQUE_BOOLEAN;
                    if(strlen($vObjHom->getNon_unique_BOOLEAN()) &&
                            !strlen($vObjProd->getNon_unique_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_NON_UNIQUE_BOOLEAN] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
                    else if(!strlen($vObjHom->getNon_unique_BOOLEAN()) &&
                            strlen($vObjProd->getNon_unique_BOOLEAN()))
                        $vFatoresTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_NON_UNIQUE_BOOLEAN] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
                    else
                        $vFatoresTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_NON_UNIQUE_BOOLEAN] = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                }
                else{
                    $vTipoModificacao =  EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
                    
                }
                
                
            }
            
           return array("tipo_modificacao" => $vTipoModificacao,
               "fatores_tipo_operacao" => $vFatoresTipoOperacao,
                "fatores" => $vFatores);
        }
        
        
        public function atualizaTipoOperacaoAtualizacaoBanco(){
            if(strlen($this->getProducao_tabela_chave_id_INT()) 
                && !strlen($this->getHomologacao_tabela_chave_id_INT())){
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE;
            } else if(!strlen($this->getProducao_tabela_chave_id_INT()) 
                && strlen($this->getHomologacao_tabela_chave_id_INT())){
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT;
            } else{
                $vObjHom = $this->getObjHomologacao();
                $vObjProd= $this->getObjProducao();
//                TODO nome nao eh levado em consideracaos
//                if($vObjHom->getNome() != $vObjProd->getNome())
//                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
//                
                if($vObjHom->getIndex_method() != $vObjProd->getIndex_method())
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                
                if($vObjHom->getNon_unique_BOOLEAN() != $vObjProd->getNon_unique_BOOLEAN())
                    return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
                
                return EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
            }
        }
        
        public static function procedimentoChaveCriada($idPVBB, $pIdChaveHom, $db = null){
            if($db == null)
                $db = new Database();
            $vIdTabelaChave = EXTDAO_Tabela_chave_tabela_chave::getIdTabelaChaveTabelaChave(
                $idPVBB, $pIdChaveHom, null, false, $db);
            $objTabelaChaveTabelaChave = new EXTDAO_Tabela_chave_tabela_chave();    
            //insert
            if(!strlen($vIdTabelaChave)){
                $objTabelaChaveTabelaChave->setHomologacao_tabela_chave_id_INT($pIdChaveHom);
                $objTabelaChaveTabelaChave->setProjetos_versao_banco_banco_id_INT($idPVBB);
                $objTabelaChaveTabelaChave->formatarParaSQL();
                $objTabelaChaveTabelaChave->insert();
                $objTabelaChaveTabelaChave->selectUltimoRegistroInserido();
            }
            //edit
            else{ 
                $objTabelaChaveTabelaChave->select($vIdTabelaChave);
                if(strlen($objTabelaChaveTabelaChave->getProducao_tabela_chave_id_INT())){
                    $objTabelaChaveTabelaChave->setProjetos_versao_banco_banco_id_INT($idPVBB);
                    $objTabelaChaveTabelaChave->setProducao_tabela_chave_id_INT(null);
                    $objTabelaChaveTabelaChave->formatarParaSQL();
                    $objTabelaChaveTabelaChave->update($vIdTabelaChave);
                }
                $vListaId = EXTDAO_Tabela_chave_atributo_tabela_chave_atributo::getListaId($idPVBB, $objTabelaChaveTabelaChave->getId(), $db);
                if(count($vListaId)){
                    $objTCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
                    if($vListaId != null)
                    foreach ($vListaId as $vIdTCATCA) {
                        $objTCA->delete($vIdTCATCA);
                    }    
                }
            }
            
            
        }
        
        public static function getListaIdChaveInserida($idPVBB, $pObjTabelaHomologacao, $db = null){
            if($pObjTabelaHomologacao == null ) return null;
            $consulta = "SELECT DISTINCT tcHom.id
                    FROM tabela_chave_tabela_chave tctc 
                        JOIN tabela_chave tcHom 
                            ON tctc.homologacao_tabela_chave_id_INT = tcHom.id
                    WHERE tctc.producao_tabela_chave_id_INT IS NULL 
                        AND tctc.projetos_versao_banco_banco_id_INT = $idPVBB
                        AND tcHom.tabela_id_INT = {$pObjTabelaHomologacao->getId()} ";
            if($db == null)
            $db = new Database();
            $db->query($consulta);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getListaTabelaTabelaIdChaveIgual($idPVBB, $pObjTabelaHomologacao, $pObjTabelaProducao){
            $consulta = "SELECT DISTINCT tt.id
                    FROM tabela_chave_tabela_chave tctc JOIN tabela_chave tcProd
                            ON tctc.producao_tabela_chave_id_INT = tcProd.id
                        JOIN tabela_chave tcHom 
                            ON tctc.homologacao_tabela_chave_id_INT = tcHom.id
                        JOIN tabela_tabela tt
                            ON tt.homologacao_tabela_id_INT = tcHom.tabela_id_INT
                                OR tt.producao_tabela_id_INT = tcProd.tabela_id_INT
                    WHERE (tcProd.tabela_id_INT = {$pObjTabelaProducao->getId()}
                        OR tcHom.tabela_id_INT = {$pObjTabelaHomologacao->getId()})
                        AND tctc.projetos_versao_banco_banco_id_INT = $idPVBB
                        AND tcHom.id != tcProd.id";
            $db = new Database();
            $db->query($consulta);
            
            $vListaObj = Helper::getResultSetToMatriz($db->result);
            $ret = array();
            if($vListaObj != null)
            foreach ($vListaObj as $obj) {
                $ret[$obj[1]] = $obj[2];
            }
            return $ret;
        }
        
        public static function getListaIdChaveIgual($idPVBB, $pObjTabelaHomologacao, $pObjTabelaProducao){
            if($pObjTabelaHomologacao == null || $pObjTabelaProducao == null)return null;
            $consulta = "SELECT tctc.id, 
                        tctc.homologacao_tabela_chave_id_INT, 
                        tctc.producao_tabela_chave_id_INT
                    FROM tabela_chave_tabela_chave tctc 
                        JOIN tabela_chave tc1
                            ON tctc.homologacao_tabela_chave_id_INT = tc1.id
                        JOIN tabela_chave tc2
                            ON tctc.producao_tabela_chave_id_INT = tc2.id
                    WHERE 
                        tc1.tabela_id_INT = {$pObjTabelaHomologacao->getId()}
                        AND tc2.tabela_id_INT = {$pObjTabelaProducao->getId()}
                        AND tctc.projetos_versao_banco_banco_id_INT = $idPVBB  ";
            $db = new Database();
            $db->query($consulta);
            
            $vListaObj = Helper::getResultSetToMatriz($db->result);
            $ret = array();
            if($vListaObj != null)
            foreach ($vListaObj as $obj) {
                $ret[$obj[1]] = $obj[2];
            }
            return $ret;
        }
        
        public static function getListaIdChaveRemovida($idPVBB, $pObjTabelaProducao, $db = null){
            if($pObjTabelaProducao == null ) return null;
            $consulta = "SELECT DISTINCT tcProd.id
                    FROM tabela_chave_tabela_chave tctc 
                        JOIN tabela_chave tcProd ON tctc.producao_tabela_chave_id_INT = tcProd.id
                    WHERE tctc.homologacao_tabela_chave_id_INT IS NULL 
                        AND tctc.projetos_versao_banco_banco_id_INT = $idPVBB
                        AND tcProd.tabela_id_INT = {$pObjTabelaProducao->getId()} ";
            if($db == null)
            $db = new Database();
            $db->query($consulta);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function existeTabelaChaveRemovida($idPVBB, $pObjTabelaProducao){
            if($pObjTabelaProducao == null ) return null;
            $consulta = "SELECT DISTINCT tcProd.id
                    FROM tabela_chave_tabela_chave tctc JOIN tabela_chave tcProd
                        ON tctc.producao_tabela_chave_id_INT = tcProd.id
                    WHERE tctc.homologacao_tabela_chave_id_INT IS NULL 
                        AND tctc.projetos_versao_banco_banco_id_INT = $idPVBB
                        AND tcProd.tabela_id_INT = {$pObjTabelaProducao->getId()} 
                     LIMIT 0,1 ";
            $db = new Database();
            $db->query($consulta);
            if(strlen($db->getPrimeiraTuplaDoResultSet(0))) return true;
            else return false;
                    
        }
        
        
        public static function procedimentoChaveIgual(
            $idPVBB, $pIdChaveHom, $pIdChaveProd, 
            $pAtributosChaveHom, $pAtributosChaveProd, $db = null){
            if(!strlen($pIdChaveHom) && !strlen($pIdChaveProd)) return;
            if(count($pAtributosChaveHom) != count($pAtributosChaveProd)){
                throw new Exception();
            }
            $objTabelaChaveTabelaChave = new EXTDAO_Tabela_chave_tabela_chave();    
            $vIdTabelaChaveSoProd = EXTDAO_Tabela_chave_tabela_chave::getIdTabelaChaveTabelaChave($idPVBB, null, $pIdChaveProd, false, $db);
            if($vIdTabelaChaveSoProd != null)
                $objTabelaChaveTabelaChave->delete ($vIdTabelaChaveSoProd);
            
            $vIdTabelaChaveSoHom = EXTDAO_Tabela_chave_tabela_chave::getIdTabelaChaveTabelaChave($idPVBB, $pIdChaveHom, null, false, $db);
            if($vIdTabelaChaveSoHom != null)
                $objTabelaChaveTabelaChave->delete ($vIdTabelaChaveSoHom);
            
            $vIdTabelaChave = EXTDAO_Tabela_chave_tabela_chave::getIdTabelaChaveTabelaChave($idPVBB, $pIdChaveHom, $pIdChaveProd, false, $db);
            
            //insert
            if(!strlen($vIdTabelaChave)){
                $objTabelaChaveTabelaChave->setHomologacao_tabela_chave_id_INT($pIdChaveHom);
                $objTabelaChaveTabelaChave->setProducao_tabela_chave_id_INT($pIdChaveProd);
                $objTabelaChaveTabelaChave->setProjetos_versao_banco_banco_id_INT($idPVBB);
                $objTabelaChaveTabelaChave->formatarParaSQL();
                $objTabelaChaveTabelaChave->insert();
                $objTabelaChaveTabelaChave->selectUltimoRegistroInserido();
                
                for($i = 0 ; $i < count($pAtributosChaveHom); $i++) {
                    $hom = $pAtributosChaveHom[$i];
                    $prod = $pAtributosChaveProd[$i];
                    $objTCATCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
                    $objTCATCA->setTabela_chave_tabela_chave_id_INT($objTabelaChaveTabelaChave->getId());
                    $objTCATCA->setProjetos_versao_banco_banco_id_INT($idPVBB);
                    $objTCATCA->setHomologacao_tabela_chave_atributo_id_INT(EXTDAO_Tabela_chave_atributo::getTabelaChaveAtributo($hom, $pIdChaveHom, $db));
                    $objTCATCA->setProducao_tabela_chave_atributo_id_INT(EXTDAO_Tabela_chave_atributo::getTabelaChaveAtributo($prod, $pIdChaveProd, $db));
                    $objTCATCA->formatarParaSQL();
                    $objTCATCA->insert();
                }
            }
            //edit
            else{ 
                $objTCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
                $objTabelaChaveTabelaChave->select($vIdTabelaChave);
                
                $vListaId = EXTDAO_Tabela_chave_atributo_tabela_chave_atributo::getListaId($idPVBB, $objTabelaChaveTabelaChave->getId(), $db);
                $vValidade = true;
                if($pChaveAtributoHomPorProd != null){
                    
                
                foreach ($pChaveAtributoHomPorProd as $hom => $prod) {
                    $vIdTCATCA = EXTDAO_Tabela_chave_atributo_tabela_chave_atributo::getIdTabelaChaveAtributoTabelaChaveAtributo($idPVBB, $hom, $prod, $db);
                    if(!strlen($vIdTCATCA)){
                        $objTCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
                        $objTCA->setProjetos_versao_banco_banco_id_INT($idPVBB);
                        $objTCA->setProducao_tabela_chave_atributo_id_INT($prod);
                        $objTCA->setHomologacao_tabela_chave_atributo_id_INT($hom);
                        $objTCA->formatarParaSQL();
                        $objTCA->insert();
                    }
                    $indice = array_search($vIdTCATCA, $vListaId);
                    if($indice){
                        $vListaId[$indice] = null;
                    }
                    
                    foreach ($vListaId as $vIdTCATCA) {
                        $objTCA->delete($indice); 
                    }
                }
                }
                if(!empty($vListaId) ){
                    $objTCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
                    
                    foreach ($vListaId as $vIdTCATCA) {
                        $objTCA->delete($vIdTCATCA);
                    }    
                }
            }
            
        }
        
        
        
        public static function procedimentoChaveDeletada($idPVBB, $idTabelaChaveProd, $db = null){
            if(!strlen($idTabelaChaveProd)) return;
            if($db == null)
                $db = new Database();
            $idTabelaChaveTabelaChave = EXTDAO_Tabela_chave_tabela_chave::getIdTabelaChaveTabelaChave(
                $idPVBB, null, $idTabelaChaveProd, false, $db );
            
            $objTabelaChaveTabelaChave = new EXTDAO_Tabela_chave_tabela_chave();    
            //insert
            if(!strlen($idTabelaChaveTabelaChave)){
                $objTabelaChaveTabelaChave->setProjetos_versao_banco_banco_id_INT($idPVBB);
                $objTabelaChaveTabelaChave->setProducao_tabela_chave_id_INT($idTabelaChaveProd);
                $objTabelaChaveTabelaChave->formatarParaSQL();
                $objTabelaChaveTabelaChave->insert();
//                $objTabelaChaveTabelaChave->selectUltimoRegistroInserido();
                $idTabelaChaveTabelaChave=$objTabelaChaveTabelaChave->getIdDoUltimoRegistroInserido();
//               echo "<br/>INSERIU: ";
//            echo "$idTabelaChaveTabelaChave, $idPVBB, $idTabelaChaveProd<br/>";
//            exit();
            }
            //edit
            else{ 
                $objTabelaChaveTabelaChave->select($idTabelaChaveTabelaChave);
                if(strlen($objTabelaChaveTabelaChave->getHomologacao_tabela_chave_id_INT())){
                    $objTabelaChaveTabelaChave->setHomologacao_tabela_chave_id_INT(null);
                    $objTabelaChaveTabelaChave->formatarParaSQL();
                    $objTabelaChaveTabelaChave->update($idTabelaChaveTabelaChave);
                }
                $vListaId = EXTDAO_Tabela_chave_atributo_tabela_chave_atributo::getListaId($idPVBB, $objTabelaChaveTabelaChave->getId(), $db);
                if(!empty($vListaId)){
                    $objTCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
                    
                    foreach ($vListaId as $vIdTCATCA) {
                        $objTCA->delete($vIdTCATCA);
                    }    
                }
            }
//            echo "NAO ENTROU";
//            echo "$idTabelaChaveTabelaChave, $idPVBB, $idTabelaChaveProd";
//            exit();
        }
        
        public static function getIdTabelaChaveTabelaChave(
                $idPVBB,
                $pIdChaveTabelaHomologacao, 
                $pIdChaveTabelaProducao, 
                $pConsiderarNulo = true,
                $db = null){
            
            if(!strlen($pIdChaveTabelaHomologacao) &&
                    !strlen($pIdChaveTabelaProducao))
                return;
            if($db == null)
            $db = new Database();
            $consulta = "SELECT id
                    FROM tabela_chave_tabela_chave
                    WHERE ";
            $where = "projetos_versao_banco_banco_id_INT = $idPVBB  ";
            if(strlen($pIdChaveTabelaHomologacao))
                $where .= " AND homologacao_tabela_chave_id_INT = $pIdChaveTabelaHomologacao";
            else if($pConsiderarNulo) 
                $where .= " AND homologacao_tabela_chave_id_INT IS NULL";
            
            if(strlen($pIdChaveTabelaProducao)){
                if(strlen($where))
                    $where .= " AND ";
                $where .= " producao_tabela_chave_id_INT = $pIdChaveTabelaProducao";
            }
            else if($pConsiderarNulo){
                if(strlen($where))
                    $where.= " AND ";
                $where.= " producao_tabela_chave_id_INT IS NULL";
            }
                
            $db->query($consulta.$where);
            
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        //ATENÇÃO esse metodo pode deletar ou editar o registro $idTT passado na entrada
    public static function simulaAtualizacaoHmgParaProd($idTCTC,  $db = null){
        if($db == null) $db = new Database();
        $objTCTC = new EXTDAO_Tabela_chave_tabela_chave();
        $objTCTC->select($idTCTC);
        
        $trunk = Trunk::factory();
//        $trunk = new Trunk();
        $idPVBBTabelaProducao = $trunk->getPVBBDoBancoDeDadosDeProducao(
            $objTCTC->getProjetos_versao_banco_banco_id_INT());
//        $objBancoProducao = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBTabelaProducao);
        $idBancoPrd = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao(
            $idPVBBTabelaProducao, 
            $db);
        $idPVTabelaProducao = $trunk->getPVDoBancoDeDadosDeProducaoRelacionadoAoPVBB(
            $objTCTC->getProjetos_versao_banco_banco_id_INT());
        $prefixoProd = "";
        
        $tipoAnalise = $trunk->getTipoAnaliseDoProjetosVersaoBancoBanco( $idPVBBTabelaProducao);
        //$objPV = new EXTDAO_Projetos_versao();
        switch ($tipoAnalise) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                $prefixoProd =  "__";
                break;
            default:
                break;
        } 
        
        $objTCHmg = $objTCTC->getObjHomologacao();
        $objTCPrd = $objTCTC->getObjProducao();
        $idTabelaHmg = null;
        $tabelaHmg = null;
        if($objTCHmg != null ){
            $idTabelaHmg = $objTCHmg->getTabela_id_INT();
            $tabelaHmg = EXTDAO_Tabela::getNomeTabela($idTabelaHmg, $db);
        }
        $idTabelaPrd = null;
        
        if($objTCPrd != null ){
            $idTabelaPrd = $objTCPrd->getTabela_id_INT();
            $tabelaPrd = EXTDAO_Tabela::getNomeTabela($idTabelaPrd, $db);
        } else {
            $identificadorTabelaPrd = EXTDAO_Tabela::getIdentificadorTabela(
                $idPVTabelaProducao, $idBancoPrd, $prefixoProd.$tabelaHmg, $db);
            
//            print_r($identificadorTabelaPrd);
            if($identificadorTabelaPrd != null){
                $idTabelaPrd = $identificadorTabelaPrd['id'];
                $tabelaPrd = $identificadorTabelaPrd['nome'];
            }
        }
        
        if($objTCHmg == null && $objTCPrd != null){
           $objTCTC->delete($objTCTC->getId()) ;
        } else if($objTCHmg != null && $objTCPrd == null){
            $atributosHmg = EXTDAO_Tabela_chave_atributo::getListaNomeOrdenadoAtributoDaChave($objTCHmg->getId(), $db);
            if(count($atributosHmg)){
                $idsAtributosPrd = array();
                if($idTabelaPrd != null){
                    for($i = 0 ; $i < count($atributosHmg); $i++){
                        $idsAtributosPrd[count($idsAtributosPrd)] = EXTDAO_Atributo::getIdAtributo(
                            $atributosHmg[$i], 
                            $idTabelaPrd, 
                            $db);
                    }
                }
                
                $objTCPrd = new EXTDAO_Tabela_chave();
                $objTCPrd->select($objTCHmg->getId());
                $objTCPrd->setTabela_id_INT($idTabelaPrd);
                
                $nomeChave = EXTDAO_Script_comando_banco_cache::getNomeChavePeloIdTabelaChave(
                    $objTCHmg->getId(),
                    $db);
                if(!strlen($nomeChave)){
                    Helper::imprimirMensagem(
                        "O nome da chave é gerado aleatorimente durante a geração de script. "
                        . "Todos os nomes gerados devem estar cadastrados na tabela script_comando_banco_cache, "
                        . "no caso o nome da chave de id {$objTCHmg->getId()} não estava mapeada.", 
                            MENSAGEM_ERRO);
                        exit();
                }
                $objTCPrd->setNome($nomeChave);
                $objTCPrd->formatarParaSQL();
                $objTCPrd->insert();
                $idTCProd = $objTCPrd->getIdDoUltimoRegistroInserido();
                $objTCA = new EXTDAO_Tabela_chave_atributo();
                for($i = 0 ; $i < count($idsAtributosPrd); $i++){
                    //TODO  descobrir o nome da chave utilizando a cache de chave
                    $objTCA->setId(null);
                    $objTCA->setTabela_chave_id_INT($idTCProd);
                    $objTCA->setAtributo_id_INT($idsAtributosPrd[$i]);
                    $objTCA->setSeq_INT($i + 1);
                    $objTCA->formatarParaSQL();
                    $objTCA->insert();
                }
                $objTCTC->setProducao_tabela_chave_id_INT($idTCProd);
                $objTCTC->formatarParaSQL();
                $objTCTC->update($objTCTC->getId());
            }
        }
    }
        
}

    
