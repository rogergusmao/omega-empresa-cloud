<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Projetos_tipo_log_erro
    * NOME DA CLASSE DAO: DAO_Projetos_tipo_log_erro
    * DATA DE GERAÇÃO:    01.05.2013
    * ARQUIVO:            EXTDAO_Projetos_tipo_log_erro.php
    * TABELA MYSQL:       projetos_tipo_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Projetos_tipo_log_erro extends DAO_Projetos_tipo_log_erro
    {
        const ATUALIZA_BANCO_DE_DADOS_PRODUCAO = 1;
        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Projetos_tipo_log_erro";

           
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Projetos_tipo_log_erro();

        }

	}

    
