<?php

//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:     EXTDAO_Projetos_versao_caminho
 * NOME DA CLASSE DAO: DAO_Projetos_versao_caminho
 * DATA DE GERAÃ‡ÃƒO:    23.04.2013
 * ARQUIVO:            EXTDAO_Projetos_versao_caminho.php
 * TABELA MYSQL:       projetos_versao_caminho
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

// **********************
// DECLARAÃ‡ÃƒO DA CLASSE
// **********************

class EXTDAO_Projetos_versao_caminho extends DAO_Projetos_versao_caminho {
    const EXECUTANDO = 1;
    const DESATUALIZADO = 2;
    
    const EXECUTADO = 3;
    const FINALIZADO = 4;
    const NAO_INICIALIZADO = 5;
    

    public function __construct($configDAO= null) {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Projetos_versao_caminho";

    }

    public function setLabels() {

        $this->label_id = "Id";
        $this->label_projetos_versao_processo_estrutura_id_INT = "Processo de AnÃ¡lise de Estrutura da VersÃ£o de Projeto";
        $this->label_processo_estrutura_caminho_id_INT = "Processo Estrutura Caminho";
        $this->label_data_inicio_DATETIME = "Data de InÃ­cio da ExecuÃ§Ã£o da Etapa do Processo";
        $this->label_data_fim_DATETIME = "Data de Fim da ExecuÃ§Ã£o da Etapa do Processo";
        $this->label_data_atualizacao_DATETIME = "Data da Ãºltima ExecuÃ§Ã£o da Etapa Processo";
        $this->label_id_atual_projetos_versao_caminho_INT = "Caminho atual";
        $this->label_total_execucao_INT = "Total de ExecuÃ§Ãµes Realizadas";
        $this->label_tempo_total_gasto_seg_INT = "Tempo Total Gasto Exec Processo";
        $this->label_data_atualizacao_DATETIME = "Data Ãšltima Exec Processo";
            
            
    }

    public function getTipoProcessoEstrutura(){
        
        return $this->getFkObjProcesso_estrutura_caminho()->getTipo_processo_estrutura_id_INT();
    }
    
    
    public function setDiretorios() {
        
    }

    public function setDimensoesImagens() {
        
    }

    public function factory() {

        return new EXTDAO_Projetos_versao_caminho();
    }

    public static function getIdPVBBPEC($idPVBB, $pIdPE) {
        $q = "SELECT p.id
                FROM projetos_versao_caminho p
                WHERE p.projetos_versao_banco_banco_processo_estrutura_id_INT = {$idPVBB}
                    AND p.processo_estrutura = {$pIdPE}
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }

    public function getIdDaEtapaAnterior() {
        $q = "SELECT p.id, per.seq_INT
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_banco_banco_processo_estrutura_id_INT = {$this->getProjetos_versao_processo_estrutura_id_INT()}
                    AND per.seq_INT < {$this->getSeq_INT()}
                ORDER BY per.seq_INT DESC
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }


    public function getIdDaEtapaPosterior($db = null) {
        $q = "SELECT p.id, pec.seq_INT
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$this->getProjetos_versao_processo_estrutura_id_INT()}
                    AND pec.seq_INT > {$this->getSeq_INT()}
                    AND p.id != {$this->getId()}
                ORDER BY pec.seq_INT ASC
                LIMIT 0,1";
        if($db==null)
            $db = new Database();
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public function isEtapaDesatualizada() {
        $obj = $this->factory();
        $obj->select($this->getId());
        $obj->formatarParaSQL();
        //verifica se a etapa anterior possui data de atualizacao maior que a data
        //de criacao ou atualizacao da etapa atual
        //caso encontre algum resulta => a etapa esta desatualizada
        $q = "SELECT p.id, pec.seq_INT
                FROM projetos_versao_caminho p
                    JOIN processo_estrutura_caminho pec 
                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$obj->getProjetos_versao_processo_estrutura_id_INT()}
                    AND pec.seq_INT < {$obj->getSeq_INT()}
                    AND (
                            (
                                p.data_atualizacao_DATETIME IS NOT NULL
                                AND (
                                    p.data_atualizacao_DATETIME > {$obj->getData_atualizacao_DATETIME()} 
                                    OR  p.data_atualizacao_DATETIME > {$obj->getData_inicio_DATETIME()} 
                                )

                            )
                            OR 
                            (
                                p.data_fim_DATETIME IS NULL
                                AND p.data_inicio_DATETIME > {$obj->getData_inicio_DATETIME()} 
                            )
                    )
                    AND p.id != {$obj->getId()}
                ORDER BY pec.seq_INT DESC
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        $r = $d->getPrimeiraTuplaDoResultSet(0);
        if (strlen($r))
            return true;
        else
            return false;
    }

    public function getSeq_INT() {
        $q = "SELECT pec.seq_INT
                FROM projetos_versao_caminho p
                        JOIN processo_estrutura_caminho pec 
                                        ON pec.id = p.processo_estrutura_caminho_id_INT
                WHERE p.projetos_versao_processo_estrutura_id_INT = {$this->getProjetos_versao_processo_estrutura_id_INT()}
                        AND p.id = {$this->getId()}
                ORDER BY pec.seq_INT ASC
                LIMIT 0,1;";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }

    public function getOrdemNaListaDeEtapas() {
        $q = "SELECT COUNT(DISTINCT pvc.id)
                FROM projetos_versao_caminho pvc JOIN
                    processo_estrutura_caminho pec 
                        ON pvc.processo_estrutura_caminho_id_INT = pec.id
                    JOIN projetos_versao_processo_estrutura pvpe
                        ON pvpe.id = pvc.projetos_versao_processo_estrutura_id_INT
                WHERE pec.seq_INT < {$this->getSeq_INT()} 
                    AND pvc.id != {$this->getId()}
                    AND pvpe.id = {$this->getProjetos_versao_processo_estrutura_id_INT()}";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }

    public function getNome() {
        $q = "SELECT tpe.NOME
                FROM projetos_versao_caminho pvc 
                    JOIN processo_estrutura_caminho pec 
                        ON pvc.processo_estrutura_caminho_id_INT = pec.id
                    JOIN tipo_processo_estrutura tpe
                        ON tpe.id = pec.tipo_processo_estrutura_id_INT
                WHERE pvc.id = {$this->getId()} 
                LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }

    public static function statusToString($status){
        switch ($status) {
           case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
            return "NÃ£o inicializado";
            
        case EXTDAO_Projetos_versao_caminho::EXECUTADO:
            return "Executado";
            
        case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
            return "Executando";
            
        case EXTDAO_Projetos_versao_caminho::FINALIZADO:
            return "Finalizado";
            
        case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
            return "Desatualizado";
        default:
            return null;
        }
    }
    
    public function getStatus() {
        if (!strlen($this->getData_inicio_DATETIME())) {
            return EXTDAO_projetos_versao_caminho::NAO_INICIALIZADO;
        } else {
            if (strlen($this->getData_inicio_DATETIME()) &&
                    !strlen($this->getData_fim_DATETIME()))
                return EXTDAO_projetos_versao_caminho::EXECUTANDO;
            else if ($this->isEtapaDesatualizada())
                return EXTDAO_projetos_versao_caminho::DESATUALIZADO;
            else if (strlen($this->getData_fim_DATETIME()))
                return EXTDAO_projetos_versao_caminho::EXECUTADO;
            else //!DataInicio && stlren(DataFim)
                return EXTDAO_projetos_versao_caminho::FINALIZADO;
        }
    }

    public function finalizaEtapa($db = null) {
        $id = $this->getId();
        $obj = new EXTDAO_projetos_versao_caminho($db);
        $obj->select($id);
        $vDataFim = Helper::getDiaEHoraAtualSQL();
        $obj->setData_fim_DATETIME($vDataFim);
        $obj->setData_atualizacao_DATETIME($vDataFim);
        $tot = 0;
        if (strlen($obj->getTotal_execucao_INT()))
            $tot = $obj->getTotal_execucao_INT();
        $tot += 1;

        $obj->getTotal_execucao_INT($tot);
        $obj->formatarParaSQL();
        $obj->update($this->getId());
    }

    public function inicializaEtapa($db=null) {
        $id = $this->getId();
        $obj = new EXTDAO_projetos_versao_caminho($db);
        $obj->select($id);
        $obj->setData_inicio_DATETIME(Helper::getDiaEHoraAtualSQL());
        $obj->setData_fim_DATETIME(null);
        $obj->formatarParaSQL();
        $obj->update($id);
    }

    public function getPagina() {

        $q = "SELECT tpe.pagina
                    FROM projetos_versao_caminho pvc
                            JOIN processo_estrutura_caminho pec
                                ON pvc.processo_estrutura_caminho_id_INT = pec.id
                            JOIN tipo_processo_estrutura tpe
                                ON pec.tipo_processo_estrutura_id_INT = tpe.id
                    WHERE pvc.id = {$this->getId()}
                    LIMIT 0,1";
        $d = new Database();
        $d->query($q);
        return $d->getPrimeiraTuplaDoResultSet(0);
    }

}

