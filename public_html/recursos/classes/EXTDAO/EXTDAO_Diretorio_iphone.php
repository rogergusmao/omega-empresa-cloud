<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Diretorio_iphone
    * NOME DA CLASSE DAO: DAO_Diretorio_iphone
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Diretorio_iphone.php
    * TABELA MYSQL:       diretorio_iphone
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Diretorio_iphone extends DAO_Diretorio_iphone
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Diretorio_iphone";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_androidDiretorioClassesDAO = "Diretório Classe DAO";
			$this->label_androidDiretorioClassesEXTDAO = "Diretório Classe EXTDAO";
			$this->label_androidForms = "Diretório Forms";
			$this->label_androidFiltros = "Diretório Filtros";
			$this->label_androidLists = "Diretório Lists";
			$this->label_androidAdapter = "Diretório Adapter";
			$this->label_androidItemList = "Diretório Itemlist";
			$this->label_androidDetail = "Diretório Detail";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Diretorio_iphone();

        }

	}

    
