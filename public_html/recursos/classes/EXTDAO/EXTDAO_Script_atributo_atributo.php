<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Script_atributo_atributo
    * NOME DA CLASSE DAO: DAO_Script_atributo_atributo
    * DATA DE GERAÇÃO:    04.04.2013
    * ARQUIVO:            EXTDAO_Script_atributo_atributo.php
    * TABELA MYSQL:       script_atributo_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Script_atributo_atributo extends DAO_Script_atributo_atributo
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Script_atributo_atributo";


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_consulta = "Consulta";
			$this->label_seq_INT = "Sequência";
			$this->label_script_tabela_tabela_id_INT = "Script da Tabela";
			$this->label_tipo_comando_banco_id_INT = "Tipo de Comando do Banco";
			$this->label_atributo_atributo_id_INT = "Relacionamento Entre Atributo de Homologação E Produção";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Script_atributo_atributo();

        }

	}

    
