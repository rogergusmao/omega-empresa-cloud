<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Backup_automatico
    * NOME DA CLASSE DAO: DAO_Backup_automatico
    * DATA DE GERAÇÃO:    25.08.2010
    * ARQUIVO:            EXTDAO_Backup_automatico.php
    * TABELA MYSQL:       backup_automatico
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    
    class EXTDAO_Backup_automatico extends DAO_Backup_automatico
    {

        public function __construct($configDAO = null){
        
            parent::__construct($configDAO);
        
            	$this->nomeClasse = "EXTDAO_Backup_automatico";
            	$this->database = new Database(NOME_BANCO_DE_DADOS_BACKUP);

            
                    
        }
        
        public function setLabels(){
        
			$this->label_id = "Id";
			$this->label_hora_base_TIME = "Hora";
			$this->label_status_BOOLEAN = "Status";

        
        }
        
        public function setDiretorios(){
                
        
        
        }
        
        public function setDimensoesImagens(){
                
     
        
        }
        
        public function fazerBackupAutomatico(){
            
            $dadosBackup = $this->verificarBackupsAutomaticos();
            $horaBase = $dadosBackup[1];
            
            if($dadosBackup[0] === false){
                
                $dataAtual = date("d/m/Y H:i:s");
                //faz backup automatico
                $_POST["descricao"] = "Backup Automático do banco de dados integral feito em {$dataAtual}";
                
                $objBackup = new Database_Backup();
            
                $objBackup->fazerBackup();
                
                $objControle = new EXTDAO_Backup_automatico_controle();
                $objControle->setHora_base_TIME($horaBase);
                $objControle->setData_DATE(date("Y-m-d"));
                $objControle->setHora_real_TIME(date("H:i:s"));
                $objControle->formatarParaSQL();
                
                $objControle->insert();

            }

        }
        
        private function verificarBackupsAutomaticos(){

            $horaAtual = date("'H:i:s'");
            $dataHoje = date("'Y-m-d'");
            
            $this->database->query("SELECT MAX(DISTINCT hora_base_TIME)
            						FROM backup_automatico 
            						WHERE hora_base_TIME <={$horaAtual}");
            
            if($this->database->rows() == 1){
                
                $horaBase = $this->database->getPrimeiraTuplaDoResultSet(0);

            }
            
            //verifica se o backup jah foi feito
            $this->database->query("SELECT COUNT(id) 
            						FROM backup_automatico_controle
            						WHERE hora_base_TIME='{$horaBase}' AND data_DATE={$dataHoje}");
            
            if($this->database->getPrimeiraTuplaDoResultSet(0) > 0){
                
                return array(true, $horaBase);
                
            }
            else{
                
                return array(false, $horaBase);
                
            }            
            
        }
        
        public static function factory(){
                
            return new EXTDAO_Backup_automatico();        
        
        }
        
	}
    
    
