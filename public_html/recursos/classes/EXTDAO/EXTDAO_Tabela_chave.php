<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tabela_chave
    * NOME DA CLASSE DAO: DAO_Tabela_chave
    * DATA DE GERAÇÃO:    06.04.2013
    * ARQUIVO:            EXTDAO_Tabela_chave.php
    * TABELA MYSQL:       tabela_chave
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tabela_chave extends DAO_Tabela_chave
    {

        public function __construct($configDAO = null){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tabela_chave";

        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_index_type = "Index Type";
			$this->label_index_method = "Index Method";
                        $this->label_tabela_id_INT = "Tabela";
                        $this->label_non_unique_BOOLEAN = "Non Unique";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public static function factory(){

            return new EXTDAO_Tabela_chave();

        }
        public static function getIdentificadoresDasChavesNaoUnica($idTabela, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT DISTINCT tc.id, tc.nome
                    FROM tabela_chave tc 
                        JOIN tabela_chave_atributo tca 
                            ON tc.id = tca.tabela_chave_id_INT
                    WHERE tc.tabela_id_INT = $idTabela 
                        AND tc.non_unique_BOOLEAN = '1' ";
            $db->query($q);
            return Helper::getResultSetToMatriz($db->result);
        }
        public static function getIdsDasChavesNaoUnica($idTabela, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT DISTINCT tc.id
                    FROM tabela_chave tc 
                        JOIN tabela_chave_atributo tca 
                            ON tc.id = tca.tabela_chave_id_INT
                    WHERE tc.tabela_id_INT = $idTabela 
                        AND tc.non_unique_BOOLEAN = '1' ";
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
            //return $db->getPrimeiraTuplaDoResultSet(0);
        }
        public static function getIdDaChaveCriadaJuntoAFkDoAtributo($idTabela, $idAtributo, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT DISTINCT tc.id
                    FROM tabela_chave tc 
                        JOIN tabela_chave_atributo tca 
                            ON tc.id = tca.tabela_chave_id_INT
                    WHERE tc.tabela_id_INT = $idTabela 
                        AND tca.atributo_id_INT = $idAtributo
                        AND tc.non_unique_BOOLEAN = '1'
                        AND (SELECT COUNT(tca1.id)
                            FROM tabela_chave_atributo tca1
                            WHERE tca1.tabela_chave_id_INT = tc.id
                            LIMIT 0, 1) = 1
                    LIMIT 0,1 ";
            $db->query($q);
            
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public static function getIdsChaveUnicaDaTabela($idTabela, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT DISTINCT tc.id
                    FROM tabela_chave tc 
                        JOIN tabela_chave_atributo tca 
                            ON tc.id = tca.tabela_chave_id_INT
                    WHERE tc.tabela_id_INT = $idTabela 
                        AND tc.non_unique_BOOLEAN = '0'";
            $db->query($q);
            
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        public static function getIdDaChave($pNomeChave, $pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT tc.id
                    FROM tabela_chave tc 
                    WHERE tc.tabela_id_INT = $pIdTabela 
                        AND tc.nome = '$pNomeChave' 
                    LIMIT 0,1 ");
            $vId = Helper::getResultSetToPrimeiroResultado($db->result);
            if(strlen($vId)) return $vId;
            else return null;
            
        }
        
        public static function getIdDaChaveUnicaDaTabela($pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT DISTINCT tc.id
                    FROM tabela_chave tc JOIN tabela_chave_atributo tca ON tc.id = tca.tabela_chave_id_INT
                        JOIN atributo a ON tca.atributo_id_INT = a.id
                    WHERE a.tabela_id_INT = {$pIdTabela}
                        AND tc.non_unique_BOOLEAN = '0'
                    LIMIT 0,1");
            $vId = Helper::getResultSetToPrimeiroResultado($db->result);
            if(strlen($vId)) return $vId;
            else return null;
            
        }
        public static function getIdentificadorDaChaveUnicaDaTabela($pIdTabela){
            $db = new Database();
            $db->query("SELECT tc.id, tc.nome
                    FROM tabela_chave tc JOIN tabela_chave_atributo tca ON tc.id = tca.tabela_chave_id_INT
                        JOIN atributo a ON tca.atributo_id_INT = a.id
                    WHERE a.tabela_id_INT = {$pIdTabela}
                        AND tc.non_unique_BOOLEAN = '0'
                    LIMIT 0,1");
            
            $objs = Helper::getResultSetToMatriz($db->result);
            if(count($objs)) return $objs[0];
            else return null;
            
        }
        
        public static function getListaChaveTabela($pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $db->query("SELECT DISTINCT tc.id
                    FROM tabela_chave tc 
                    WHERE tc.tabela_id_INT = {$pIdTabela}");
                    
            $vId = Helper::getResultSetToArrayDeUmCampo($db->result);
            if(count($vId) > 0) return $vId;
            else return null;
        }
        
        public static function getListaObjChaveTabela($pIdTabela, $db = null){
            if($db == null)
            $db = new Database();
            $q = "SELECT DISTINCT tc.id, tc.nome
                    FROM tabela_chave tc 
                    WHERE tc.tabela_id_INT = {$pIdTabela}";
//                    echo $q;
            $db->query($q);
            $vId = Helper::getResultSetToMatriz($db->result);
            if(count($vId) > 0) return $vId;
            else return null;
        }
        
	}

    
