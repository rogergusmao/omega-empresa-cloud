<?php



define('MENSAGEM_AVISO_RELATORIO_ABERTO_EM_POPUP', "O relatório está sendo aberto em uma janela popup.
                                                    Caso o bloqueador de popups esteja ativado, desative-o e atualize a página.");

define('MENSAGEM_EM_CONSTRUCAO', "<font align=center color=red>EM CONSTRUÇÃO</font>");

define('MENSAGEM_LOGOUT', "Logout realizado com sucesso.");

define('MENSAGEM_EXPIRACAO_SESSAO', "Seu tempo de sessão expirou. Por segurança, faça o login novamente.");

define('MENSAGEM_PARA_SALVAR_ALTERACOES', "Para salvar as alterações feitas");

define('MENSAGEM_CLIQUE_AQUI', "clique aqui");

define('ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA', "Reenvio de senha - " . TITULO_PAGINAS);

define('MENSAGEM_EMAIL_REENVIO_SENHA_SUCESSO', "O email com a sua senha foi enviado com sucesso.");

define('MENSAGEM_EMAIL_REENVIO_SENHA_ERRO', "Não foi possível encontrar o email especificado em nosso banco de dados.");

define('MENSAGEM_ALTERAR_POSICAO_FUNCAO', "Atenção, ao alterar a posição da função, automaticamente todas as vagas associadas serão realocadas.");

define('MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO', "Você não tem autorização para acessar esta área.");

define('MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO_ACAO', "Você não tem autorização para executar esta ação.");

define('LABEL_BOTAO_DE_IMPRESSAO', "Versão para Impressão");

?>
