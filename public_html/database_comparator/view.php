<?php

header("Content-type: text/html; charset=iso-8859-1", true);


include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../recursos/php/funcoes.php';


$viewPath = Helper::GET("path");

if(!$viewPath){

    Helper::imprimirMensagem("View n�o definida.");

}
else{

    include Helper::acharRaizAngularJS() . "app/common/common.php";
    include("{$viewPath}.php");

}

?>