<?php

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../recursos/php/funcoes.php';

date_default_timezone_set("America/Sao_Paulo");

$basePath = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/") + 1);
$basePathHtml5 = $_SERVER['SCRIPT_NAME'];
?>

<html ng-app="databaseComparatorApp">

    <head>
        <base href="<?=$basePathHtml5 ?>#">
        <title>Database Comparator</title>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- Angular JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular.js"></script>

        <!-- Angular ng-Route JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular-route.js"></script>

        <!-- Angular ng-Animate JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular-animate.js"></script>

        <!-- Angular UI Bootstrap -->
        <script src="<?=$basePath ?>js/ui-bootstrap-tpls-0.13.0.js"></script>

        <!-- Checklist Model -->
        <script src="<?=$basePath ?>js/checklist-model.js"></script>

        <!--
        Arquivos da Aplica��o
        -->

        <link rel="stylesheet" href="<?=$basePath ?>css/app.css">

        <script src="<?=$basePath ?>app/app.js"></script>
        <script src="<?=$basePath ?>app/appConstants.js"></script>
        <script src="<?=$basePath ?>app/appHelper.js"></script>
        <script src="<?=$basePath ?>app/appRootScope.js"></script>
        <script src="<?=$basePath ?>app/appDataService.js"></script>

        <script src="<?=$basePath ?>app/comparison/controller.js"></script>
        <script src="<?=$basePath ?>app/corporacaoGrid/controller.js"></script>
        <script src="<?=$basePath ?>app/dataGenerationWeb/controller.js"></script>
        <script src="<?=$basePath ?>app/dataGenerationMobile/controller.js"></script>
        <script src="<?=$basePath ?>app/dataGenerationMultipleMobile/controller.js"></script>
    </head>

    <body class="databaseComparatorApp">

        <div ng-view></div>

    </body>
</html>
