<?php


include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../recursos/php/funcoes.php';


if(isset($_GET["jsonOutput"]))
    header("Content-type: application/json; charset=iso-8859-1", true);

if (isset($_POST["class"])) {

    $classe = Helper::POST("class");
    $action = Helper::POST("action");
    $registroDaOperacao = Helper::POST("id1");

}
elseif (isset($_GET["class"])) {

    $classe = Helper::GET("class");
    $action = Helper::GET("action");
    $registroDaOperacao = Helper::GET("id1");

}
else {

    exit();

}

$obj = call_user_func_array(array($classe, "factory"), array());

//---------------------------------
//*
//*
// INICIAR TRANSACAO
//*
//*
//---------------------------------


$objBanco = new Database();
$objBanco->iniciarTransacao();
$retorno = call_user_func(array($obj, $action));

//---------------------------------
//*
//*
// FINALIZAR TRANSACAO (COMMIT OU ROLLBACK EM CASO DE ERRO)
//*
//*
//---------------------------------

if ($retorno[0]) {

    if (strpos($retorno[0], "msgErro") !== false) {

        $objBanco->rollbackTransacao();

    } else {

        $objBanco->commitTransacao();

    }

    header($retorno[0]);

} else {

    $objBanco->commitTransacao();

}

?>
