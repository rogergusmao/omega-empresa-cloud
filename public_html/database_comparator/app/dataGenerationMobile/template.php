
<div class="modal-header">
    <h3 class="custom">Gerador de Massa de Dados de Teste - Mobile</h3>

    <fieldset>
        <label class="destaque">Preenchimento Aleatorio:</label>

        <div class="col-sm-12">

            <div class="col-sm-6">

                <label>
                    Preencher com valor <input type="input" class="small-input" ng-model="parameters.operationCount" /> em
                    <input type="input" class="small-input" ng-model="parameters.tableCount" /> tabelas aletatorias.
                </label>

            </div>

            <div class="col-sm-6">

                <input type="reset" class="btn btn-info" value="Preencher"
                       ng-click="fillTableParameters()" />

                <input type="reset" class="btn btn-danger" value="Limpar"
                       ng-click="clearAllTablesRecordCount()" />

                <input type="reset" class="btn btn-primary" value="Restaurar Padrao"
                       ng-click="restoreDefaults()" />

            </div>

        </div>

    </fieldset>
    <fieldset>
        <label class="destaque">Operacoes:</label>

        <div class="col-sm-12">

            <label>
                <input type="checkbox" ng-model="parameters.operation.insert" />
                Insercao
            </label>

            <label>
                <input type="checkbox" ng-model="parameters.operation.update" />
                Edicao
            </label>

            <label>
                <input type="checkbox" ng-model="parameters.operation.delete" />
                Exclusao
            </label>

        </div>

    </fieldset>

    <fieldset>
        <label class="destaque">Repetição:</label>

        <div class="col-sm-12">

            <label>
                Repetir preenchimento em intervalos de
                <input type="input" class="small-input" ng-model="parameters.repeatInterval" /> em {{parameters.repeatInterval}} segundo(s).
            </label>
            <br />
            {{remainingTimeDisplayText}}

        </div>

    </fieldset>

</div>

    <div class="modal-body">

        <div ng-show="getShowResults()" class="spacer-bottom-20">

            <div id="mobile-generation-progress-panel" class="well comparison-log-panel" ng-show="dataGenerationMobile.log.length > 0">

                <div class="caption">
                    <h5><b>Progresso: <img ng-show="dataGenerationMobile.isRunning" src="img/loading.gif" /></b></h5>
                    <p ng-repeat="logLine in dataGenerationMobile.log track by $index">{{logLine}}</p>
                </div>
            </div>
            
            <input type="reset" class="btn btn-info" value="Cancelar Operações Mobile Pendentes"
                       ng-if="dataGenerationMobile.isRunning"
                       ng-click="stopDataGenerationMobileProcess()" />

            <input type="reset" class="btn btn-info" value="Cancelar Repetições"
                   ng-if="generationDataMobileHandler != null"
                   ng-click="cancelRepetitions()" />
            
            
                <input type="reset" class="btn btn-primary" value="Nova Geracao de Dados"
                       ng-if="showResults && generationDataMobileHandler == null"
                       ng-click="newDataGeneration()" />



        </div>

        <div ng-show="getShowTableList()" class="spacer-bottom-20">

            <fieldset>
                <label class="destaque">Tabelas:</label>

                <div class="col-sm-12">
                    <div class="col-sm-6" ng-repeat="table in tableList">
                        <label>
                            <input type="input" class="small-input" ng-model="table.recordCount" />
                            {{table.tableName}}
                        </label>
                    </div>
                </div>

            </fieldset>
            <br />

            <div class="col-sm-12 text-right">

                <input type="reset" class="btn btn-danger" value="Fechar"
                       ng-click="closeModal()" />

                <input type="submit" class="btn btn-primary" value="Gerar Dados"
                       ng-click="startDataGenerationMobileProcess()" />
                
            </div>

            <div class="col-sm-12 spacer-top-20">

                <div class="alert alert-danger" ng-if="hasFormError">{{formErrors}}</div>

            </div>

        </div>

    </div>
