
databaseComparatorApp.controller('corporacaoGridController',
    ["$scope", "$rootScope", "$window", "$modal", "$interval", "$timeout", "dataService",
        function corporacaoGridController($scope, $rootScope, $window, $modal, $interval, $timeout, dataService) {

            $scope.globalErrors = [];

            $rootScope.corporacoesData = [];
            $scope.currentlyShowPanel = null;
            $scope.dadosCelularDataGenerator = null;

            var resetComparisonData = function(){

                $scope.priorComparisons = null;
                $scope.comparisonsHistory = [];
                $scope.comparisonData = {
                    isRunning: false,
                    comparisonId: null,
                    log: []
                };

            };

            resetComparisonData();

            var comparisonCheckingHandler = null;

            var getCorporacaoData = function() {

                dataService.getCorporacaoGridData().then(function (result) {

                    $rootScope.corporacoesData = result.data;
                    //console.log($rootScope.corporacoesData);

                });

            };

            //chamada inicial;
            getCorporacaoData();

            //repetições para refresh
            var corporacaoPanelRefresh = $interval(getCorporacaoData, 60000);

            $scope.isServerOperationRunning = function(){

                return $scope.comparisonData.isRunning;

            };

            $scope.showDataGenerationPanel = function(idCorporacao, dadosMobile, dadosMobileIdentificador, nomeCorporacao){

                $scope.dadosCelularDataGenerator = angular.copy(dadosMobile);
                angular.extend($scope.dadosCelularDataGenerator, dadosMobileIdentificador);
                $scope.dadosCelularDataGenerator.idCorporacao = idCorporacao;
                $scope.dadosCelularDataGenerator.nomeCorporacao = nomeCorporacao;
                $scope.currentlyShowPanel = 'data_generator';

            };

            $scope.openDataGenerationWebModal = function(idCorporacao, idMobileIdentificador, nomeCorporacao){

                $modal.open({
                    templateUrl: './view.php?path=app/dataGenerationWeb/template',
                    controller: 'dataGenerationWebController',
                    windowClass: 'data-generation-modal-window',
                    backdrop: 'static',
                    keyboard : true,
                    resolve: {
                        idCorporacao: function () {
                            return idCorporacao;
                        },
                        idMobileIdentificador: function () {
                            return idMobileIdentificador;
                        },
                        nomeCorporacao: function() {
                            return nomeCorporacao;
                        },

                    }

                });

            };
            
            
            $scope.openDataGenerationMultipleMobileModal = function(idCorporacao, arrMobiles){

                $modal.open({
                    templateUrl: './view.php?path=app/dataGenerationMultipleMobile/template',
                    controller: 'dataGenerationMultipleMobileController',
                    windowClass: 'data-generation-modal-window',
                    backdrop: 'static',
                    keyboard : true,
                    resolve: {
                        idCorporacao: function () {
                            return idCorporacao;
                        },
                        arrMobiles: function(){
                            return arrMobiles;
                        }
                    }

                });

            };

            $scope.openDataGenerationMobileModal = function(idCorporacao, idMobileIdentificador, nomeCorporacao){

                $modal.open({
                    templateUrl: './view.php?path=app/dataGenerationMobile/template',
                    controller: 'dataGenerationMobileController',
                    windowClass: 'data-generation-modal-window',
                    backdrop: 'static',
                    keyboard : true,
                    resolve: {
                        idCorporacao: function () {
                            return idCorporacao;
                        },
                        idMobileIdentificador: function () {
                            return idMobileIdentificador;
                        },
                        nomeCorporacao: function() {
                            return nomeCorporacao;
                        }

                    }

                });

            };

            $scope.showComparatorPanel = function(idCorporacao, dadosMobile, dadosMobileIdentificador){

                $scope.dadosCelularComparacao = angular.copy(dadosMobile);
                angular.extend($scope.dadosCelularComparacao, dadosMobileIdentificador);
                $scope.dadosCelularComparacao.idCorporacao = idCorporacao;

                //resetar variaveis do painel
                resetComparisonData();
                $scope.currentlyShowPanel = 'comparator';

            };

            $scope.showPriorComparisons = function(){

                var parameters = {
                    corporacaoId: $scope.dadosCelularComparacao.idCorporacao,
                    mobileId: $scope.dadosCelularComparacao.idMobile,
                    mobileIdentificadorId: $scope.dadosCelularComparacao.idMobileIdentificador
                };

                dataService.showPriorComparisons(parameters).then(function(result){

                    $scope.priorComparisons = result.data;
                    $scope.comparisonData.log.push("Comparações anteriores recuperadas com sucesso!");

                },
                function(result){

                    $scope.comparisonData.log.push("Falha ao recuperar comparações anteriores...");

                });

            };

            $scope.$watch('comparisonData.log.length', function(){

                $("#comparison-progress-panel").animate({ scrollTop: $('#comparison-progress-panel')[0].scrollHeight}, 1000);

            });

            $scope.startComparisonProcess = function(){

                var comparisonParameters = {
                    corporacaoId: $scope.dadosCelularComparacao.idCorporacao,
                    mobileId: $scope.dadosCelularComparacao.idMobile,
                    mobileIdentificadorId: $scope.dadosCelularComparacao.idMobileIdentificador
                };

                //log inicial
                $scope.comparisonData.log = [];
                $scope.comparisonData.log.push("Iniciando Comparação...");
                $scope.comparisonData.isRunning = true;

                dataService.startComparisonProcess(comparisonParameters).then(function(result){

                    $scope.comparisonData.comparisonId = result.data.comparisonId;
                    $scope.comparisonData.log.push("Registro de comparação criado no banco de dados...");

                    comparisonCheckingHandler = $interval(checkComparisonProgress, 10000);

                },
                function(errorResult){

                    $scope.comparisonData.isRunning = false;
                    $scope.comparisonData.log.push("Falha ao iniciar comparação...");

                });

            };

            $scope.stopComparisonProcess = function(){

                //pára o processo de checar o status da comparação
                $interval.cancel(comparisonCheckingHandler);
                $scope.comparisonData.isRunning = false;

                dataService.cancelComparisonProcess($scope.comparisonData.comparisonId).then(function(result){

                    $scope.comparisonData.log.push(result.data.message);

                },
                function(errorResult){

                    $scope.comparisonData.log.push("Falha ao cancelar comparação...");

                });

            };

            $scope.openComparisonModal = function(comparisonIndex, type){

               type = type || 'actual';
               var comparisonId = null;
                var corporacaoId= null;
               if(type == 'actual' && $scope.comparisonsHistory.length > comparisonIndex) {

                   if ($scope.comparisonsHistory[comparisonIndex].comparisonId != $scope.comparisonData.comparisonId) {

                       $scope.comparisonData = $scope.comparisonsHistory[comparisonIndex]
                       comparisonId = $scope.comparisonData.comparisonId;

                   }

               }
               else if(type == 'prior' && $scope.priorComparisons.length > comparisonIndex){

                   comparisonId = $scope.priorComparisons[comparisonIndex].comparisonId;

               }
                corporacaoId = $scope.dadosCelularComparacao.idCorporacao;
               if(comparisonId != null) {

                   $modal.open({
                       templateUrl: './view.php?path=app/comparison/template',
                       controller: 'comparisonController',
                       windowClass: 'comparison-modal-window',
                       backdrop: 'static',
                       keyboard: true,
                       resolve: {
                          
                           comparisonId: function () {
                               return comparisonId;
                           },
                            corporacaoId: function () {
                               return corporacaoId;
                           },

                       }

                   });

               }

            };

            var checkComparisonProgress = function(){

                if($scope.comparisonData.comparisonId != null){

                    var comparisonParameters = { comparisonId: $scope.comparisonData.comparisonId };
                    dataService.getComparisonProgress(comparisonParameters).then(function(result){

                        var resultObj = result.data;
                        $scope.comparisonData.log.push(angular.copy(resultObj.message));

                        switch(resultObj.status){

                            case Constants.MobileOperationStatus.PROCESS_CANCELED:
                            case Constants.MobileOperationStatus.PROCESS_FINISHED_WITH_ERROR:

                                $interval.cancel(comparisonCheckingHandler);
                                $scope.comparisonData.isRunning = false;
                                break;

                            case Constants.MobileOperationStatus.WAITING_DEVICE_TO_START_PROCESS:
                            case Constants.MobileOperationStatus.PROCESS_RECEIVED_BY_DEVICE:

                                break;

                            case Constants.MobileOperationStatus.FILE_UPLOADED_TO_SERVER:

                                $interval.cancel(comparisonCheckingHandler);
                                $scope.comparisonData.isRunning = false;
                                $scope.comparisonsHistory.push(angular.copy($scope.comparisonData));

                                break;

                        }

                    },
                    function(errorResult){

                        $scope.comparisonData.isRunning = false;
                        $scope.comparisonData.log.push("Falha ao recuperar status da comparação...");
                        $interval.cancel(comparisonCheckingHandler);

                    });

                }
                else{

                    $interval.cancel(comparisonCheckingHandler);

                }

            };

        }]);