
<div class="alert alert-danger" role="alert" ng-if="globalErrors != null && globalErrors.length > 0">
    <p ng-repeat="singleError in globalErrors track by $index">{{singleError}}</p>
</div>

<!-- Painel de Comparação -->
<div class="col-sm-12" id="comparatorPanel" ng-show="currentlyShowPanel=='comparator'">
    <div class="panel panel-success">
        <div class="panel-heading">Comparador de Bancos de Dados</div>
        <div class="panel-body">
            <div class="col-sm-6">
                <h4>{{dadosCelularComparacao.nomeMobile}}</h4>
                <p><b>Corporação: </b>{{dadosCelularComparacao.idCorporacao}}</p>
                <p><b>Marca: </b>{{dadosCelularComparacao.marcaMobile}}</p>
                <p><b>Modelo: </b>{{dadosCelularComparacao.modeloMobile}}</p>
                <p><b>CPU: </b>{{dadosCelularComparacao.cpuMobile}}</p>
                <p><b>Produto: </b>{{dadosCelularComparacao.projectName}}</p>
                <p><b>Versão: </b>{{dadosCelularComparacao.projectVersionName}}</p>
                <p><b>Id mobile identificador: </b>{{dadosCelularComparacao.idMobileIdentificador}}</p>

                <div>
                    <button class="btn btn-danger spacer-right-5 spacer-bottom-5" tooltip="{{comparison.datetime}}" type="button" ng-repeat="comparison in priorComparisons track by $index" ng-click="openComparisonModal($index, 'prior')">Comparação {{comparison.comparisonId}}</button>
                    <button class="btn btn-info spacer-right-5 spacer-bottom-5" type="button" ng-repeat="comparison in comparisonsHistory track by $index" ng-click="openComparisonModal($index)">Comparação {{comparison.comparisonId}}</button>
                </div>

            </div>
            <div class="col-sm-6">

                <div style="margin-bottom: 10px;">
                    <button ng-if="dadosCelularComparacao.connectionStatus" class="btn btn-info btn-large" type="button" ng-click="startComparisonProcess()" ng-disabled="comparisonData.comparisonRunning">Iniciar Processo de Comparação</button>
                    <button class="btn btn-danger btn-large" type="button" ng-click="stopComparisonProcess()" ng-show="comparisonData.comparisonRunning">Cancelar Comparação</button>
                    <button class="btn btn-success btn-large" type="button" ng-click="showPriorComparisons()" >Comparações Anteriores</button>
                </div>

                <div id="comparison-progress-panel" class="well comparison-log-panel" ng-show="comparisonData.log.length > 0">

                    <div class="caption">
                        <h5><b>Progresso:</b></h5>
                        <p ng-repeat="logLine in comparisonData.log track by $index">{{logLine}}</p>
                        <img ng-show="comparisonData.comparisonRunning" src="img/loading.gif" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Painel de Geração de Dados -->
<div class="col-sm-12" id="dataGenerationPanel" ng-show="currentlyShowPanel=='data_generator'">
    <div class="panel panel-success">
        <div class="panel-heading">Gerador de Massa de Dados de Teste</div>
        <div class="panel-body">
            <div class="col-sm-6">
                <h4>{{dadosCelularDataGenerator.nomeMobile}}</h4>
                <p><b>Corporação: </b>{{dadosCelularDataGenerator.idCorporacao}}</p>
                <p><b>Marca: </b>{{dadosCelularDataGenerator.marcaMobile}}</p>
                <p><b>Modelo: </b>{{dadosCelularDataGenerator.modeloMobile}}</p>
                <p><b>CPU: </b>{{dadosCelularDataGenerator.cpuMobile}}</p>
                <p><b>Produto: </b>{{dadosCelularDataGenerator.projectName}}</p>
                <p><b>Versão: </b>{{dadosCelularDataGenerator.projectVersionName}}</p>
            </div>
            <div class="col-sm-6">

                <div class="col-sm-12">

                    <div style="margin-bottom: 10px;">
                        <button class="btn btn-info btn-large" type="button" ng-click="openDataGenerationWebModal(dadosCelularDataGenerator.idCorporacao, dadosCelularDataGenerator.idMobileIdentificador, dadosCelularDataGenerator.nomeCorporacao)">Gerar Dados Web</button>
                        <button class="btn btn-primary btn-large" type="button" ng-click="openDataGenerationMobileModal(dadosCelularDataGenerator.idCorporacao, dadosCelularDataGenerator.idMobileIdentificador, dadosCelularDataGenerator.nomeCorporacao)">Gerar Dados Mobile</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-sm-6" ng-repeat="dadosCorporacao in corporacoesData">
    <div class="panel panel-primary">
        <div class="panel-heading">Corporação {{dadosCorporacao.nomeCorporacao}} {{dadosCorporacao.idCorporacao}}</div>
        <div class="panel-body">
            <div class="col-sm-4"  >
            <ul class="list-group">
                <li class="list-group-item" >
                    <div class="panel-heading">Gerar Dados Mobile Multiplo</div>
                    <div class="panel-body">
                        
                        <div class="col-sm-12" ng-repeat="dadosCelular in dadosCorporacao.arrMobiles">
                            <div  style="padding: 5px; display: inline-block; " >
                                <div style="cursor: pointer;" ng-repeat="dadosMobileIdentificador in dadosCelular.arrMobileIdentificador" ng-if="$last">
                                    <span class="label label-danger" ng-if="!dadosMobileIdentificador.connectionStatus"><b>MI</b>{{dadosMobileIdentificador.idMobileIdentificador}}</span>
                                    <span class="label label-success" ng-if="dadosMobileIdentificador.connectionStatus"><b>MI</b>{{dadosMobileIdentificador.idMobileIdentificador}}</span>
                                </div>
                            </div> 
                        </div>
                     
                        <div class="col-sm-6" style="margin-top: 10px;">
                            <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()"  ng-click="openDataGenerationMultipleMobileModal(dadosCorporacao.idCorporacao, dadosCorporacao.arrMobiles)" >Gerar</button>
                        </div>
                        
                    </div>
                </li>
               </ul>
            </div>
            <div class="col-sm-4"  style="padding-left: 5px; padding-right: 5px;" ng-repeat="dadosCelular in dadosCorporacao.arrMobiles">
                <div class="well" style="padding: 5px;">
                    <div class="caption">
                        <h6>{{dadosCelular.nomeMobile}}</h6>
                        <p><b>Marca: </b>{{dadosCelular.marcaMobile}}</p>
                        <p><b>Modelo: </b>{{dadosCelular.modeloMobile}}</p>
                        <p><b>CPU: </b>{{dadosCelular.cpuMobile}}</p>
                    </div>

                    <ul class="list-group">
                        <li class="list-group-item" ng-repeat="dadosMobileIdentificador in dadosCelular.arrMobileIdentificador">
                            <p><b>Id mobile identificador: </b>{{dadosMobileIdentificador.idMobileIdentificador}}</p>
                            <p style="cursor: pointer;" ng-click="dadosMobileIdentificador.showActions = !dadosMobileIdentificador.showActions">
                                {{dadosMobileIdentificador.projectVersionName}}
                                <span class="label label-danger" ng-if="!dadosMobileIdentificador.connectionStatus">Offline</span>
                                <span class="label label-success" ng-if="dadosMobileIdentificador.connectionStatus">Online</span>
                            </p>
                            <div class="btn-group-vertical" role="group" ng-show="dadosMobileIdentificador.showActions" style="width: 100%;">
                                <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()" >Download SQLite</button>
                                <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()">Download SQL</button>
                                <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()">Log de Erro</button>
                                <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()">Monitorar Tela</button>
                                <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()" ng-click="showComparatorPanel(dadosCorporacao.idCorporacao, dadosCelular, dadosMobileIdentificador)">Comparador</button>
                                <button type="button" class="btn btn-grey" ng-disabled="isServerOperationRunning()" ng-click="showDataGenerationPanel(dadosCorporacao.idCorporacao, dadosCelular, dadosMobileIdentificador, dadosCorporacao.nomeCorporacao)">Gerar Dados</button>
                            </div>
                        </li>
                    </ul>
                    
                    
                </div>
            </div>

            <br />

        </div>
    </div>
</div>

