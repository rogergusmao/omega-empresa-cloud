
var databaseComparatorApp = angular.module('databaseComparatorApp', ['ngRoute', 'ui.bootstrap', 'checklist-model', 'ngAnimate']);

databaseComparatorApp.config(
    ["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {

        $routeProvider
            .when("/", {
                templateUrl: "./view.php?path=app/corporacaoGrid/template",
                controller: "corporacaoGridController"
            })
            .when("/comparison", {
                templateUrl: "./view.php?path=app/comparison/template",
                controller: "comparisonController"
            })
            .otherwise({
                redirectTo: "/"
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: true
        });

    }]
);


