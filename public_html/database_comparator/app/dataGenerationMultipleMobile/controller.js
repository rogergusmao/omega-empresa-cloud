databaseComparatorApp.controller('dataGenerationMultipleMobileController',
    ["$scope", "$window", "$modalInstance", "$interval", "$timeout", "dataService", "idCorporacao", "arrMobiles" ,
        function dataGenerationMultipleMobileController($scope, $window, $modalInstance, $interval, $timeout, dataService, idCorporacao, arrMobiles) {

            $scope.tableList = null;
            $scope.selectedTables = [];
            $scope.successMessage = null;
            $scope.dataGenerationMobile = {
                isRunning: false,
                operationId: null,
                log: []
            };
            $scope.idsMobileIdentificador = [];
            for(var i = 0 ; i < arrMobiles.length; i++){
                $scope.idsMobileIdentificador.push(arrMobiles[i].arrMobileIdentificador[arrMobiles[i].arrMobileIdentificador.length - 1].idMobileIdentificador);
            }
            
            var originalTableList = null;

            $scope.parameters = {
                operationCount: 10,
                tableCount: 10,
                repeatInterval: null,
                operation: {
                    insert: true,
                    update: true,
                    delete: true
                }
            };

            $scope.fillTableParameters = function(){

                if(!isNaN($scope.parameters.operationCount)
                    && !isNaN($scope.parameters.tableCount)){

                    for(var i=0; i < $scope.parameters.tableCount; i++)
                    {
                        var index = AppHelper.getRandomArbitrary(0, $scope.tableList.length-1);
                        var table = $scope.tableList[index];
                        if(table.recordCount == null || table.recordCount == '') {
                            table.recordCount = $scope.parameters.operationCount;
                        }

                    }

                }

            };

            $scope.clearAllTablesRecordCount = function(){

                angular.forEach($scope.tableList, function(table){

                    table.recordCount = null;

                });

            };

            $scope.restoreDefaults = function(){

                $scope.tableList = angular.copy(originalTableList);

            };

            $scope.newDataGeneration = function(){

                $scope.showTableListPanel = true;
                $scope.showResults = false;
                $scope.cancelRepetitions();

            };

            $scope.showTableListPanel = true;
            $scope.getShowTableList = function(){

                AppHelper.fixHeight();
                return $scope.showTableListPanel;

            };

            $scope.showResults = false;
            $scope.getShowResults = function(){

                AppHelper.fixHeight();
                fixProgressBoxHeight();
                return $scope.showResults;

            };

            var fixProgressBoxHeight = function(){

                var newHeight = $('body').height() - 120 - $('.data-generation-modal-window > .modal-dialog > .modal-content > .modal-header').outerHeight();
                $('#mobile-generation-progress-panel').css('max-height', newHeight + 'px');

            }

            var tableListPromise = dataService.getTableListDataGenerationMobile(idCorporacao);
            tableListPromise.then(function(result){

                $scope.tableList = result.data;
                originalTableList = angular.copy($scope.tableList);

            });

            $scope.generationDataMobileHandler = null;
            var calculateRemainingTimeHandler = null;
            var decrementingCounter = 0;
            var counterStep = 1;

            $scope.remainingTimeDisplayText = null;
            var calculateRemainingTime = function(){

                decrementingCounter++;
                $scope.remainingTimeDisplayText = "Nova repeti��o em " + (($scope.parameters.repeatInterval)-(decrementingCounter*counterStep)) + " segundos.";
                console.log('teste... ', $scope.remainingTimeDisplayText);

            };

            var arrOperationIds = [];
            var generateDataMobile = function(){

                decrementingCounter = 0;
                $scope.dataGenerationMobile.isRunning = true;

                var arrTables = [];
                //monta array de tabelas
                angular.forEach($scope.tableList, function(table){
                    if(table.recordCount != null && !isNaN(table.recordCount)) {
                        table.recordCount = parseInt(table.recordCount)
                        arrTables.push(table);
                    }
                });
                var ids = [];
                var strIdsMI = "";
                angular.forEach($scope.idsMobileIdentificador , function(id){
                    if(strIdsMI.length > 0 )
                        strIdsMI += ",";
                    strIdsMI += id;
                    ids.push(id);
                });
                
                var parameters =
                {
                    numberOfOperations: 10,
                    percentageOfUpdates: 5,
                    percentageOfDeletes: 5,
                    idsMobileIdentificador: ids
                };

                dataService.startDataGenerationMultipleMobileProcess(parameters).then(function(result){

                    if(result.data != null && result.data.operationIds != null){

                        $scope.dataGenerationMobile.operationIds = result.data.operationIds;
                        var str = "";
                        for(var i = 0 ; i < result.data.operationIds.length; i++){
                            arrOperationIds.push(result.data.operationIds[i]);
                            if(str.length > 0 )
                                str += ",";
                            str += result.data.operationIds[i];
                        }

                        $scope.dataGenerationMobile.log.push("Opera��o " + str + " de gera��o de dados mobile criada com sucesso.");

                        if(dataGenerationMobileCheckingHandler == null)
                            dataGenerationMobileCheckingHandler = $interval(checkDataGenerationMobileProgress, 10000);

                        $scope.showTableListPanel = false;
                        $scope.showResults = true;
                        $scope.dataGenerationMobile.isRunning = true;

                    }

                },
                function(result){

                    $scope.dataGenerationMobile.isRunning = false;
                    $scope.showTableListPanel = false;
                    $scope.showResults = true;

                });

            };

            var dataGenerationMobileCheckingHandler = null;
            $scope.startDataGenerationMobileProcess = function(){

                var interval = $scope.parameters.repeatInterval || null;

                if(interval != null && interval != 0 && !isNaN(interval)){

                    generateDataMobile();
                    $scope.generationDataMobileHandler = $interval(generateDataMobile, interval*1000);
                    calculateRemainingTimeHandler = $interval(calculateRemainingTime, counterStep*1000);

                }
                else{

                    generateDataMobile();

                }

            };

            $scope.cancelRepetitions = function(){

                $interval.cancel($scope.generationDataMobileHandler);
                $interval.cancel(calculateRemainingTimeHandler);

                $scope.generationDataMobileHandler = null;
                calculateRemainingTimeHandler = null;
                $scope.remainingTimeDisplayText = null;
                decrementingCounter = 0;

            };

            $scope.stopDataGenerationMobileProcess = function(){

                //para a verifica��o
                $interval.cancel(dataGenerationMobileCheckingHandler);
                dataGenerationMobileCheckingHandler = null;

                if(arrOperationIds == null || arrOperationIds.length == 0){

                    $scope.dataGenerationMobile.log.push("N�o existem opera��es pendentes.");

                    if($scope.generationDataMobileHandler == null) {

                        $timeout(function () {

                            $scope.dataGenerationMobile.isRunning = false;
                            $scope.showMobileDataGenerationParameters = true;
                            $scope.dataGenerationMobile.log = [];

                        }, 3000);

                    }

                    return;

                }

                var arrTemp = [];
                angular.copy(arrOperationIds, arrTemp);

                for(var i=0; i < arrTemp.length; i++){

                    var operationId = arrTemp[i];
                    var cancelamentoOperacoes = function(operationIdInterno) {

                        dataService.stopDataGenerationMobileProcess(operationIdInterno).then(function (result) {

                            if (result.data != null && result.data) {

                                $scope.dataGenerationMobile.log.push("Opera��o " + operationIdInterno + " cancelada com sucesso.");

                                var index = arrOperationIds.indexOf(operationIdInterno);
                                if(index > -1)
                                    arrOperationIds.splice(index, 1);

                                if (i == arrTemp.length - 1 && $scope.generationDataMobileHandler == null) {

                                    $timeout(function(){

                                        $scope.dataGenerationMobile.isRunning = false;
                                        $scope.showMobileDataGenerationParameters = true;
                                        $scope.dataGenerationMobile.log = [];

                                    }, 3000);

                                }

                            }

                        },
                        function (result) {

                            $scope.globalErrors.push("Erro ao cancelar opera��o de gera��o de dados mobile.")

                        });

                    }

                    cancelamentoOperacoes(operationId);

                }

            };

            var checkDataGenerationMobileProgress = function(){

                if(arrOperationIds != null && arrOperationIds.length > 0){

                    var currentOperationId = arrOperationIds[0];

                    var dataGenerationParameters = { operationId: currentOperationId };
                    dataService.getDataGenerationMobileProgress(dataGenerationParameters).then(function(result){

                            var resultObj = result.data;
                            $scope.dataGenerationMobile.log.push(angular.copy(resultObj.message));

                            switch(resultObj.status){

                                case Constants.MobileOperationStatus.PROCESS_CANCELED:
                                case Constants.MobileOperationStatus.PROCESS_FINISHED_WITH_ERROR:

                                    //remove o primeiro elemento
                                    arrOperationIds.shift();

                                    if(arrOperationIds.length == 0) {

                                        $interval.cancel(dataGenerationMobileCheckingHandler);
                                        dataGenerationMobileCheckingHandler = null;
                                        $scope.dataGenerationMobile.isRunning = false;

                                    }
                                    break;

                                case Constants.MobileOperationStatus.WAITING_DEVICE_TO_START_PROCESS:
                                case Constants.MobileOperationStatus.PROCESS_RECEIVED_BY_DEVICE:

                                    break;

                                case Constants.MobileOperationStatus.FILE_UPLOADED_TO_SERVER:

                                    //remove o primeiro elemento
                                    arrOperationIds.shift();

                                    if(arrOperationIds.length == 0) {

                                        $interval.cancel(dataGenerationMobileCheckingHandler);
                                        dataGenerationMobileCheckingHandler = null;
                                        $scope.dataGenerationMobile.isRunning = false;

                                    }
                                    break;

                            }

                        },
                        function(errorResult){

                            $scope.dataGenerationMobile.isRunning = false;
                            $scope.dataGenerationMobile.log.push("Falha ao recuperar status da compara��o...");
                            $interval.cancel(comparisonCheckingHandler);

                        });

                }
                else{

                    $interval.cancel(comparisonCheckingHandler);

                }

            };

            $scope.$watch('dataGenerationMobile.log.length', function(){

                $("#mobile-generation-progress-panel").animate({ scrollTop: $('#mobile-generation-progress-panel')[0].scrollHeight}, 1000);

            });

            $scope.closeModal = function(){

                $modalInstance.close();

            };

        }

    ]

)