
databaseComparatorApp.factory('dataService',
    ["$http",
        function ($http) {

            //Gerador de dados - Web
            var getTableListDataGenerationWeb = function(idCorporacao) {

                return $http({
                    url: "actions.php?class=Database_Populator_Helper&jsonOutput=true&action=getTableListWeb",
                    method: "POST",
                    data: { idCorporacao: idCorporacao},
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };
            
            var generateWebTestData = function(tableList, corporacaoId, parameters){

                return $http({
                    url: "actions.php?class=Database_Populator_Helper&jsonOutput=true&action=generateWebTestData",
                    method: "POST",
                    data:  {tableList: tableList, corporacaoId: corporacaoId, parameters: parameters},
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            //Gerador de Dados - Mobile
            var getTableListDataGenerationMobile = function(idCorporacao) {

                return $http({
                    url: "actions.php?class=Database_Populator_Helper&jsonOutput=true&action=getTableListWeb",
                    method: "POST",
                    data: { idCorporacao: idCorporacao},
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var startDataGenerationMobileJsonProcess = function(parameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=startDataGenerationMobileJsonProcess",
                    method: "POST",
                    data: parameters,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var startDataGenerationMobileProcess = function(parameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=startDataGenerationMobileProcess",
                    method: "POST",
                    data: parameters,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };
            
            var startDataGenerationMultipleMobileProcess = function(parameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=startDataGenerationMultipleMobileProcess",
                    method: "POST",
                    data: parameters,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var stopDataGenerationMobileProcess = function(operationId){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=stopDataGenerationMobileProcess",
                    method: "POST",
                    data: { operationId: operationId },
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var getDataGenerationMobileProgress = function(dataGenerationParameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=getDataGenerationMobileProgress",
                    method: "POST",
                    data: dataGenerationParameters,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }

                });

            };

            //Comparador
            var getTableList = function(comparisonId) {

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=getTableList",
                    method: "POST",
                    data: { comparisonId: comparisonId},
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });
            };

            var getSQLiteTableList = function(sqlitePath) {

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=getSQLiteTables",
                    method: "POST",
                    data: {sqliteFile: sqlitePath},
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var getFilteredResults = function (dataFilter) {

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=getFilteredResults",
                    method: "POST",
                    data: dataFilter,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var getCorporacaoGridData = function(dataFilter){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=getCorporacaoGridData",
                    method: "POST",
                    data: dataFilter,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var startComparisonProcess = function(comparisonParameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=startComparisonProcess",
                    method: "POST",
                    data: comparisonParameters,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var getComparisonProgress = function(comparisonParameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=getComparisonProgress",
                    method: "POST",
                    data: comparisonParameters,
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }

                });

            };

            var cancelComparisonProcess = function(comparisonId) {

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=cancelComparisonProcess",
                    method: "POST",
                    data: { comparisonId: comparisonId},
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            var showPriorComparisons = function(parameters){

                return $http({
                    url: "actions.php?class=Database_Comparator_Helper&jsonOutput=true&action=showPriorComparisons",
                    method: "POST",
                    data: { corporacaoId: parameters.corporacaoId, mobileIdentificadorId: parameters.mobileIdentificadorId },
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                });

            };

            return {
                getSQLiteTableList: getSQLiteTableList,
                getTableList: getTableList,
                getFilteredResults: getFilteredResults,
                getCorporacaoGridData: getCorporacaoGridData,
                startComparisonProcess: startComparisonProcess,
                getComparisonProgress: getComparisonProgress,
                cancelComparisonProcess: cancelComparisonProcess,
                getTableListDataGenerationWeb: getTableListDataGenerationWeb,
                getTableListDataGenerationMobile: getTableListDataGenerationMobile,
                generateWebTestData: generateWebTestData,
                showPriorComparisons: showPriorComparisons,
                startDataGenerationMobileJsonProcess: startDataGenerationMobileJsonProcess,
                startDataGenerationMobileProcess: startDataGenerationMobileProcess,
                stopDataGenerationMobileProcess: stopDataGenerationMobileProcess,
                getDataGenerationMobileProgress: getDataGenerationMobileProgress,
                startDataGenerationMultipleMobileProcess: startDataGenerationMultipleMobileProcess
            };

        }]);