
<!-- INICIO - Painel de loading, com mascara -->
<div class="modal omega-loading" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ng-show="showLoadingDialog()">
    <div class="modal-header">
        <h2>Processando...</h2>
    </div>
    <div class="modal-body">
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: 100%;"></div>
        </div>
    </div>
</div>
<!-- FIM - Painel de loading, com mascara -->
