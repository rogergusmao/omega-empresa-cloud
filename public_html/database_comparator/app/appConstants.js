var Constants = {

    MobileOperationStatus: {

        WAITING_DEVICE_TO_START_PROCESS: 1,
        PROCESS_RECEIVED_BY_DEVICE: 2,
        FILE_UPLOADED_TO_SERVER: 3,
        PROCESS_CANCELED: 4,
        PROCESS_FINISHED_WITH_ERROR: 5

    }

};