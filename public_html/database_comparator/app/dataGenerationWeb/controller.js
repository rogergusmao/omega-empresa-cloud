databaseComparatorApp.controller('dataGenerationWebController',
    ["$scope", "$window", "$modalInstance", "$interval", "$timeout", "dataService", "idCorporacao", "idMobileIdentificador",
        function dataGenerationWebController($scope, $window, $modalInstance, $interval, $timeout, dataService, idCorporacao, idMobileIdentificador) {

            $scope.tableList = null;
            $scope.selectedTables = [];
            $scope.successMessage = null;
            $scope.androidJsonAvailable = false;
            var originalTableList = null;

            $scope.dataGenerationMobile = {
                isRunning: false,
                operationId: null,
                log: []
            };

            $scope.parameters = {
                operationCount: 10,
                tableCount: 10,
                repeatInterval: null,
                operation: {
                    insert: true,
                    update: true,
                    delete: true
                },
                runOperationsOnMobile: false,
                operationsOnMobile: {
                    insert: true,
                    update: true,
                    delete: false
                },
                desativarOperacoesChaveEstrangeira: true
            };

            $scope.fillTableParameters = function(){

                if(!isNaN($scope.parameters.operationCount)
                    && !isNaN($scope.parameters.tableCount)){

                    for(var i=0; i < $scope.parameters.tableCount; i++)
                    {

                        var index = AppHelper.getRandomArbitrary(0, $scope.tableList.length-1);
                        var table = $scope.tableList[index];
                        if(table.recordCount == null || table.recordCount == '') {
                            table.recordCount = $scope.parameters.operationCount;
                        }

                    }

                }

            };

            $scope.clearAllTablesRecordCount = function(){

                angular.forEach($scope.tableList, function(table){

                    table.recordCount = null;

                });

            };

            $scope.restoreDefaults = function(){

                $scope.tableList = angular.copy(originalTableList);

            };

            $scope.newDataGeneration = function(){

                $scope.showTableListPanel = true;
                $scope.showResults = false;
                $scope.androidJsonAvailable = false;
                $scope.urlAndroidJson = null;
                $scope.cancelRepetitions();

            };

            $scope.showTableListPanel = true;
            $scope.getShowTableList = function(){

                AppHelper.fixHeight();
                return $scope.showTableListPanel;

            };

            $scope.showResults = false;
            $scope.getShowResults = function(){

                AppHelper.fixHeight();
                return $scope.showResults;

            };

            var tableListPromise = dataService.getTableListDataGenerationWeb(idCorporacao);
            tableListPromise.then(function(result){

                $scope.tableList = result.data;
                originalTableList = angular.copy($scope.tableList);

            });
            
            $scope.generationDataWebHandler = null;
            var calculateRemainingTimeHandler = null;
            var decrementingCounter = 0;
            var counterStep = 1;

            $scope.remainingTimeDisplayText = null;
            var calculateRemainingTime = function(){
                
                decrementingCounter++;
                $scope.remainingTimeDisplayText = "Nova repeti��o em " + (($scope.parameters.repeatInterval)-(decrementingCounter*counterStep)) + " segundos.";
                console.log('teste... ', $scope.remainingTimeDisplayText);
                
            };
            
            $scope.generateDataWebAction = function(){

                if($scope.parameters.runOperationsOnMobile){

                    $scope.parameters.repeatInterval = null;

                }
                                
                var interval = $scope.parameters.repeatInterval || null;
                
                if(interval != null && interval != 0 && !isNaN(interval)){
                    
                    generateDataWeb();
                    $scope.generationDataWebHandler = $interval(generateDataWeb, interval*1000);
                    calculateRemainingTimeHandler = $interval(calculateRemainingTime, counterStep*1000);
                    
                }
                else{
                    
                    generateDataWeb();
                    
                }
                
            };
            
            $scope.cancelRepetitions = function(){
                
                $interval.cancel($scope.generationDataWebHandler);
                $interval.cancel(calculateRemainingTimeHandler);
                
                $scope.generationDataWebHandler = null;
                calculateRemainingTimeHandler = null;
                $scope.remainingTimeDisplayText = null;
                decrementingCounter = 0;
                doingDataGeneration = false;
                
            };


            var doingDataGeneration = false;
            var generateDataWeb = function(){

                decrementingCounter = 0;
                var arrTables = [];

                //se j� tiver no meio de uma opera��o, mata a atual e espera a proxima itera��o para verificar novamente
                if(doingDataGeneration) {

                    console.log("Aguardando proxima itera��o para fazer a gera��o de dados, a opera��o corrente ainda n�o terminou...")
                    return;

                }

                //sinaliza que h� uma opera��o em andamento
                doingDataGeneration = true;

                //monta array de tabelas
                angular.forEach($scope.tableList, function(table){
                    if(table.recordCount != null && !isNaN(table.recordCount)) {
                        table.recordCount = parseInt(table.recordCount)
                        arrTables.push(table);
                    }
                });

                var parameters = {

                    runOperationsOnMobile: $scope.parameters.runOperationsOnMobile,
                    operationsOnMobile: $scope.parameters.operationsOnMobile,
                    desativarOperacoesChaveEstrangeira: $scope.parameters.desativarOperacoesChaveEstrangeira,
                    operations: $scope.parameters.operation

                };

                dataService.generateWebTestData(arrTables, idCorporacao, parameters).then(function(result){

                    if(result.data.success){

                        $scope.successMessage = result.data.message;
                        $scope.errorMessage = null;
                        $scope.returnedResult = result.data.arrTables;

                        if($scope.parameters.runOperationsOnMobile) {

                            var urlJson = result.data.jsonFileUrl;
                            if (typeof urlJson != 'undefined' && urlJson != null){

                                //enviar json para o Android;
                                $scope.urlAndroidJson = urlJson;
                                $scope.androidJsonAvailable = true;

                            }
                            else{

                                $scope.urlAndroidJson = null;
                                $scope.androidJsonAvailable = false;

                            }

                        }

                    }
                    else{

                        $scope.errorMessage = result.data.message;
                        $scope.successMessage = null;
                        $scope.returnedResult = null;

                    }

                    $scope.showTableListPanel = false;
                    $scope.showResults = true;

                    //sinaliza que a opera��o foi encerrada
                    doingDataGeneration = false;

                },
                function(result){

                    $scope.errorMessage = "Houve um erro ao gerar os dados.";
                    $scope.successMessage = null;
                    $scope.returnedResult = null;

                    $scope.showTableListPanel = false;
                    $scope.showResults = true;

                    //sinaliza que a opera��o foi encerrada
                    doingDataGeneration = false;

                });

            };


            /*

                INICIO - OPERACOES MOBILE

             */

            $scope.showMobileResults = false;
            $scope.getShowMobileResults = function(){

                AppHelper.fixHeight();
                fixMobileProgressBoxHeight();
                return $scope.showMobileResults;

            };

            var fixMobileProgressBoxHeight = function(){

                var newHeight = $('body').height() - 120 - $('.data-generation-modal-window > .modal-dialog > .modal-content > .modal-header').outerHeight();
                $('#mobile-generation-progress-panel').css('max-height', newHeight + 'px');

            }

            var dataGenerationMobileCheckingHandler = null;
            $scope.startMobilePopulationProcess = function(){

                var url = $scope.urlAndroidJson;

                var parameters = {

                    urlJson: url,
                    mobileIdentificadorId: idMobileIdentificador

                };

                dataService.startDataGenerationMobileJsonProcess(parameters).then(function(result){

                    if(result.data != null && result.data.operationId != null){

                        $scope.dataGenerationMobile.operationId = result.data.operationId;
                        $scope.dataGenerationMobile.log.push("Opera��o de gera��o de dados mobile criada com sucesso.");

                        dataGenerationMobileCheckingHandler = $interval(checkDataGenerationMobileProgress, 10000);
                        $scope.showTableListPanel = false;
                        $scope.showMobileResults = true;
                        $scope.showResults = false;
                        $scope.dataGenerationMobile.isRunning = true;

                    }

                },
                function(result){

                    $scope.dataGenerationMobile.isRunning = false;
                    $scope.showTableListPanel = false;
                    $scope.showMobileResults = false;
                    $scope.showResults = true;


                });

            };

            $scope.stopDataGenerationMobileProcess = function(){

                //para a verifica��o
                $interval.cancel(dataGenerationMobileCheckingHandler);

                var operationId = $scope.dataGenerationMobile.operationId;
                dataService.stopDataGenerationMobileProcess(operationId).then(function(result){

                        if(result.data != null && result.data){

                            $scope.dataGenerationMobile.log.push("Opera��o cancelada com sucesso.");

                            $timeout(function(){

                                $scope.dataGenerationMobile.isRunning = false;
                                $scope.showMobileDataGenerationParameters = false;
                                $scope.dataGenerationMobile.log = [];
                                $scope.showResults = true;

                            }, 3000);

                        }

                    },
                    function(result){

                        $scope.globalErrors.push("Erro ao cancelar opera��o de gera��o de dados mobile.")

                    });

            };

            var checkDataGenerationMobileProgress = function(){

                if($scope.dataGenerationMobile.operationId != null){

                    var dataGenerationParameters = { operationId: $scope.dataGenerationMobile.operationId };
                    dataService.getDataGenerationMobileProgress(dataGenerationParameters).then(function(result){

                            var resultObj = result.data;
                            $scope.dataGenerationMobile.log.push(angular.copy(resultObj.message));

                            switch(resultObj.status){

                                case Constants.MobileOperationStatus.PROCESS_CANCELED:
                                case Constants.MobileOperationStatus.PROCESS_FINISHED_WITH_ERROR:

                                    $interval.cancel(dataGenerationMobileCheckingHandler);
                                    $scope.dataGenerationMobile.isRunning = false;
                                    break;

                                case Constants.MobileOperationStatus.WAITING_DEVICE_TO_START_PROCESS:
                                case Constants.MobileOperationStatus.PROCESS_RECEIVED_BY_DEVICE:

                                    break;

                                case Constants.MobileOperationStatus.FILE_UPLOADED_TO_SERVER:

                                    $interval.cancel(dataGenerationMobileCheckingHandler);
                                    $scope.dataGenerationMobile.isRunning = false;
                                    $scope.showResults = true;

                                    break;

                            }

                        },
                        function(errorResult){

                            $scope.dataGenerationMobile.isRunning = false;
                            $scope.showResults = true;
                            $scope.dataGenerationMobile.log.push("Falha ao recuperar status da compara��o...");
                            $interval.cancel(comparisonCheckingHandler);

                        });

                }
                else{

                    $interval.cancel(comparisonCheckingHandler);

                }

            };

            $scope.$watch('dataGenerationMobile.log.length', function(){

                $("#mobile-generation-progress-panel").animate({ scrollTop: $('#mobile-generation-progress-panel')[0].scrollHeight}, 1000);

            });

             /*

                FIM - OPERACOES MOBILE

             */

            $scope.closeModal = function(){

                $scope.cancelRepetitions();
                $modalInstance.close();

            };

        }

    ]

)