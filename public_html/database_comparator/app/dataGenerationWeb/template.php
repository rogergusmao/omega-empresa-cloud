
<div class="modal-header">
    <h3 class="custom">Gerador de Massa de Dados de Teste - Web</h3>

    <fieldset>
        <label class="destaque">Preenchimento Aleatório:</label>

        <div class="col-sm-12">

            <div class="col-sm-6">

                <label>
                    Preencher com valor <input type="input" class="small-input" ng-model="parameters.operationCount" /> em
                    <input type="input" class="small-input" ng-model="parameters.tableCount" /> tabelas aleatórias.
                </label>

            </div>

            <div class="col-sm-6">

                <input type="reset" class="btn btn-info" value="Preencher"
                       ng-click="fillTableParameters()" />

                <input type="reset" class="btn btn-danger" value="Limpar"
                       ng-click="clearAllTablesRecordCount()" />

                <input type="reset" class="btn btn-primary" value="Restaurar Padrao"
                       ng-click="restoreDefaults()" />

            </div>

        </div>

    </fieldset>
    <fieldset>
        <label class="destaque">Operações:</label>

        <div class="col-sm-12">

            <label>
                <input type="checkbox" ng-model="parameters.operation.insert" />
                Inserção
            </label>

            <label>
                <input type="checkbox" ng-model="parameters.operation.update" />
                Edição
            </label>

            <label>
                <input type="checkbox" ng-model="parameters.operation.delete" />
                Exclusão
            </label>

        </div>

    </fieldset>
    
    <fieldset ng-if="!parameters.runOperationsOnMobile">
        <label class="destaque">Repetição:</label>

        <div class="col-sm-12">

            <label>
                Repetir preenchimento em intervalos de 
                <input type="input" class="small-input" ng-model="parameters.repeatInterval" /> em {{parameters.repeatInterval}} segundo(s).
            </label>
            <br />
            {{remainingTimeDisplayText}}
            
        </div>

    </fieldset>

    <fieldset>
        <label class="destaque">Opções Avançadas:</label>

        <div class="col-sm-12">

            <label>
                <input type="checkbox" ng-model="parameters.runOperationsOnMobile" />
                Fazer operações no Mobile após operações da Web
            </label>

        </div>

        <div class="col-sm-12"  style="padding-left: 30px;" ng-show="parameters.runOperationsOnMobile">

            <label>
                <input type="checkbox" ng-model="parameters.operationsOnMobile.insert" />
                Inserção
            </label>

            <label>
                <input type="checkbox" ng-model="parameters.operationsOnMobile.update" />
                Edição
            </label>

            <label>
                <input type="checkbox" ng-model="parameters.operationsOnMobile.delete" />
                Exclusão
            </label>

        </div>

        <div class="col-sm-12">

            <label>
                <input type="checkbox" ng-model="parameters.desativarOperacoesChaveEstrangeira" />
                Desativar operações envolvendo chaves estrangeiras
            </label>

        </div>

    </fieldset>

</div>

    <div class="modal-body">

        <div ng-show="getShowResults()" class="spacer-bottom-20">

            <div class="alert alert-success" role="alert" ng-show="successMessage != null">{{successMessage}}</div>
            <div class="alert alert-danger" role="alert" ng-show="errorMessage != null">{{errorMessage}}</div>

            <div class="well data-generation-log-panel" ng-show="returnedResult != null && returnedResult.length > 0">

                <div class="caption" ng-repeat="objTable in returnedResult track by $index">
                    <p class="destaque">{{objTable.tableName}}</p>
                    <p ng-repeat="historyItem in objTable.history track by $index">{{historyItem}}</p>
                </div>
            </div>

        </div>

        <div ng-show="getShowMobileResults()" class="spacer-bottom-20">

            <div id="mobile-generation-progress-panel" class="well comparison-log-panel" ng-show="dataGenerationMobile.log.length > 0">

                <div class="caption">
                    <h5><b>Progresso das Operações no Mobile: <img ng-show="dataGenerationMobile.isRunning" src="img/loading.gif" /></b></h5>
                    <p ng-repeat="logLine in dataGenerationMobile.log track by $index">{{logLine}}</p>
                </div>
            </div>

        </div>

        <div class="alert alert-success" role="alert" ng-show="androidJsonAvailable && !dataGenerationMobile.isRunning">Arquivo para envio ao dispositivo móvel gerado com sucesso!</div>

        <input type="reset" class="btn btn-info" value="Cancelar Repetições"
                   ng-if="generationDataWebHandler != null"
                   ng-click="cancelRepetitions()" />

        <input type="reset" class="btn btn-info" value="Cancelar Operação"
               ng-if="dataGenerationMobile.isRunning"
               ng-click="stopDataGenerationMobileProcess()" />

        <input type="reset" class="btn btn-primary" value="Nova Geracao de Dados"
               ng-if="showResults && !dataGenerationMobile.isRunning"
               ng-click="newDataGeneration()" />

        <input type="reset" class="btn btn-danger" value="Iniciar Geração de dados no Mobile"
               ng-if="androidJsonAvailable && !dataGenerationMobile.isRunning"
               ng-click="startMobilePopulationProcess()" />

        <div ng-show="getShowTableList()" class="spacer-bottom-20">

            <fieldset>
                <label class="destaque">Tabelas:</label>

                <div class="col-sm-12">
                    <div class="col-sm-6" ng-repeat="table in tableList">
                        <label>
                            <input type="input" class="small-input" ng-model="table.recordCount" />
                            {{table.tableName}}
                        </label>
                    </div>
                </div>

            </fieldset>
            <br />

            <div class="col-sm-12 text-right">
                
                <input type="reset" class="btn btn-info" value="Cancelar Repetições"
                       ng-if="generationDataWebHandler != null"
                       ng-click="cancelRepetitions()" />
                
                <input type="reset" class="btn btn-danger" value="Fechar"
                       ng-click="closeModal()" />

                <input type="submit" class="btn btn-primary" value="Gerar Dados"
                       ng-click="generateDataWebAction()" />
                

            </div>

            <div class="col-sm-12 spacer-top-20">

                <div class="alert alert-danger" ng-if="hasFormError">{{formErrors}}</div>

            </div>

        </div>

    </div>
