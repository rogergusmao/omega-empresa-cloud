
databaseComparatorApp.controller('comparisonController',
    ["$scope", "$window", "$modalInstance", "dataService", "comparisonId", "corporacaoId",
        function comparisonController($scope, $window, $modalInstance, dataService, comparisonId, corporacaoId) {

            var tableResultsPerPage = 10;

            $scope.filterData = {};
            $scope.tempFilterData = angular.copy($scope.filterData);
            $scope.tempFilterData.selectedTables = [];
            $scope.tempFilterData.comparisonId = comparisonId;
            $scope.tempFilterData.corporacaoId = corporacaoId;
            $scope.currentResultPage = 1;
            $scope.numberOfPages = 1;
            $scope.showParametersPanel = true;

            var tableListPromise = dataService.getTableList(comparisonId);
            tableListPromise.then(function(result){

                $scope.tableList = result.data;
                angular.forEach($scope.tableList, function(table){
                    if(table.isSelected)
                        $scope.tempFilterData.selectedTables.push(table.name);
                });

            });

            $scope.selectAllTables = false;
            $scope.checkAllTables = function() {

                if($scope.selectAllTables){

                    $scope.tempFilterData.selectedTables = [];
                    angular.forEach($scope.tableList, function(table){
                        $scope.tempFilterData.selectedTables.push(table.name);
                    });

                }
                else{

                    $scope.tempFilterData.selectedTables = [];

                }

            };

            $scope.closeModal = function(){

                $modalInstance.close();

            };

            $scope.goToResultPage = function(pageNumber){

                if(pageNumber == 'next' && $scope.currentResultPage <= $scope.numberOfPages){

                    $scope.currentResultPage++;
                    filterResultsPerPage();

                }
                else if(pageNumber == 'prev' && $scope.currentResultPage >= 0){

                    $scope.currentResultPage--;
                    filterResultsPerPage();

                }
                else{

                    $scope.currentResultPage = pageNumber;

                }

                filterResultsPerPage();

            };

            $scope.getShowParametersPanel = function(){

                var newHeight = $('body').height() - 60 - $('.comparison-modal-window > .modal-dialog > .modal-content > .modal-header').outerHeight();
                $('.comparison-modal-window > .modal-dialog > .modal-content > .modal-body').height(newHeight + 'px');

                return $scope.showParametersPanel;

            };

            var entireResultSet = null;
            var filterResultsPerPage = function(){

                var receivedResults = angular.copy(entireResultSet);
                var lastIndex = ($scope.currentResultPage * tableResultsPerPage) < receivedResults.arrTableResults.length ?
                ($scope.currentResultPage * tableResultsPerPage) : receivedResults.arrTableResults.length;

                receivedResults.arrTableResults = receivedResults.arrTableResults.slice(($scope.currentResultPage-1) * tableResultsPerPage, lastIndex);
                $scope.comparisonResults = receivedResults;

            };

            $scope.toggleParametersVisibility = function(){

                $scope.showParametersPanel = !$scope.showParametersPanel;

            };

            $scope.filterResults = function () {

                //filtrar os resultados
                dataService.getFilteredResults($scope.tempFilterData).then(

                    function (results) {

                        $scope.filterData = angular.copy($scope.tempFilterData);

                        var tempArrTables = [];
                        var arrTables = results.data[0].arrTableResults;
                        
                        for(var i=0; i < arrTables.length; i++){
                            
                            if(arrTables[i].overallResult == 1)
                                tempArrTables.push(arrTables[i]);
                            
                            
                        }
                        
                        entireResultSet = results.data[0];
                        entireResultSet.arrTableResults = tempArrTables;
                        
                        $scope.numberOfPages = Math.ceil(entireResultSet.arrTableResults.length / tableResultsPerPage);
                        filterResultsPerPage();

                        $scope.hasFormError = false;
                        $scope.showResults = true;
                        $scope.showParametersPanel = false;

                    },
                    function (results) {

                        $scope.hasFormError = true;
                        $scope.formErrors = results.statusText || "Ocorreu um erro ao recuperar os resultados da comparação.";
                        $scope.showResults = false;

                    }

                );

                $scope.filterData = angular.copy($scope.tempFilterData);

            };

            $scope.resetForm = function () {

            };

        }]);