
    <div class="modal-header">
        <h3>Comparacao de Bancos de Dados</h3>
        <a ng-if="showResults" name="colapseLink" ng-click="toggleParametersVisibility()">{{showParametersPanel ? '(esconder parâmetros)' : '(mostrar parâmetros)'}}</a>
    </div>

    <div class="modal-body">

        <div ng-show="getShowParametersPanel()" class="spacer-bottom-20">

            <fieldset>
                <label>Tabelas</label>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label>
                            <input type="checkbox"
                                   ng-model="selectAllTables" ng-click="checkAllTables()" />
                            Selecionar Todas
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4" ng-repeat="table in ::tableList">
                        <label>
                            <input checklist-value="table.name"
                                   type="checkbox"
                                   checklist-model="tempFilterData.selectedTables" />
                            {{table.name}}
                        </label>
                    </div>
                </div>

            </fieldset>
            <br />

            <div class="col-sm-12 text-right">

                <input type="reset" class="btn btn-danger" value="Fechar"
                       ng-click="closeModal()" />

                <input type="submit" class="btn btn-primary" value="Comparar"
                       ng-click="filterResults()" />

            </div>

            <div class="col-sm-12 spacer-top-20">

                <div class="alert alert-danger" ng-if="hasFormError">{{formErrors}}</div>

            </div>

        </div>

        <div ng-if="showResults">

            <h4 class="media-heading">
                <b>Banco SQLite: </b>{{::comparisonResults.databaseFilename}}
            </h4>

            <h4 class="media-heading">
                <span class="label label-danger" ng-if="::comparisonResults.overallResult == 1">Bancos Diferentes</span>
                <span class="label label-success" ng-if="::comparisonResults.overallResult == 0">Bancos Iguais</span>
            </h4>
            <br />

            <nav>
                <ul class="pagination">
                    <li>
                        <a ng-click="goToResultPage('prev')" aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li ng-repeat="page in getPagesArray(numberOfPages)" class="{{currentResultPage==page ? 'active' : ''}}"><a ng-click="goToResultPage(page)">{{page}}</a></li>
                    <li>
                        <a ng-click="goToResultPage('next')" aria-label="PrÃ³xima">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>

            <br />

            <div class="panel panel-primary" ng-repeat="tableResults in comparisonResults.arrTableResults | filter: { overallResult: 1 }">

                <div class="panel-heading">Tabela {{::tableResults.tableName}} </div>
                <div class="panel-body" style="padding-top: 5px;">

                    <h5><b>Tabela Existe:</b> {{::tableResults.tableExists ? "Sim" : "Não"}}</h5>
                    <h5 class="media-heading">
                        <span class="label label-danger" ng-if="::tableResults.overallResult == 1">Tabelas Diferentes</span>
                        <span class="label label-success" ng-if="::tableResults.overallResult == 0">Tabelas Iguais</span>
                    </h5>
                    <br />

                    <div class="col-sm-12" style="padding-left: 0px; overflow-x: scroll; overflow-y: scroll; max-height: 300px; margin-bottom: 20px;" ng-if="tableResults.recordsSobrando != null">

                        <table class="table table-bordered table-hover">
                            <tr class="success">
                                <th style="min-width: 200px;" colspan="{{::tableResults.columns.length}}">Registros só da Web</th>
                            </tr>
                            <tr class="success">
                                <th style="min-width: 200px;" ng-repeat="columnName in ::tableResults.columns">{{::columnName}}</th>
                            </tr>

                            <tr ng-repeat="recordSobrando in ::tableResults.recordsSobrando">
                                <td ng-repeat="columnValue in ::recordSobrando track by $index">{{::columnValue}}</td>
                            </tr>
                        </table>

                    </div>

                    <div class="col-sm-12" style="padding-left: 0px; overflow-x: scroll; overflow-y: scroll; max-height: 300px; margin-bottom: 20px;" ng-if="tableResults.recordsFaltando != null">

                        <table class="table table-bordered table-hover">
                            <tr class="danger">
                                <th style="min-width: 200px;" colspan="{{::tableResults.columns.length}}">Registros Só do Android</th>
                            </tr>
                            <tr class="danger">
                                <th style="min-width: 200px;" ng-repeat="columnName in ::tableResults.columns">{{::columnName}}</th>
                            </tr>

                            <tr ng-repeat="recordFaltando in ::tableResults.recordsFaltando">
                                <td ng-repeat="columnValue in ::recordFaltando track by $index">{{::columnValue}}</td>
                            </tr>
                        </table>

                    </div>

                    <div class="col-sm-12" style="padding-left: 0px; overflow-x: scroll; overflow-y: scroll; max-height: 300px; margin-bottom: 20px;" ng-if="tableResults.recordsDiferentes != null">

                        <table class="table table-bordered table-hover">
                            <tr class="warning">
                                <th style="min-width: 200px;" colspan="{{::tableResults.columns.length+1}}">Registros Diferentes</th>
                            </tr>
                            <tr class="warning">
                                <th style="min-width: 100px;">Banco</th>
                                <th style="min-width: 200px;" ng-repeat="columnName in ::tableResults.columns">{{::columnName}}</th>
                            </tr>

                            <tbody ng-repeat="recordDiferente in ::tableResults.recordsDiferentes">
                                <tr>
                                    <td style="min-width: 100px;" class="{{$odd ? 'tr-lighter': 'tr-darker'}}">MySQL</th>
                                    <td ng-repeat="columnValue in ::recordDiferente[0] track by $index" class="{{$parent.$odd ? 'tr-lighter': 'tr-darker'}}">{{::columnValue}}</td>
                                </tr>
                                <tr>
                                    <td style="min-width: 100px;" class="{{$odd ? 'tr-lighter': 'tr-darker'}}">SQLite</th>
                                    <td ng-repeat="columnValue in ::recordDiferente[1] track by $index" class="{{$parent.$odd ? 'tr-lighter': 'tr-darker'}}">{{::columnValue}}</td>
                                </tr>
                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

    </div>
