
databaseComparatorApp.run(
    ["$http", "$rootScope", function($http, $rootScope) {

        $rootScope.showLoadingDialog = function(){

            if($http.pendingRequests.length == 0){ 
                $('#pleaseWaitDialog').modal('hide');
            }
            else{
                $('#pleaseWaitDialog').modal();
            }

            return ($http.pendingRequests.length > 0);

        };

        $rootScope.getPagesArray = function(num){

            var arrReturn = [];
            for(var i=0; i < num; i++){

                arrReturn.push(i+1);

            }

            return arrReturn;

        };

    }]);