
var AppHelper = {

    getRandomArbitrary : function(min, max) {

        return Math.round(Math.random() * (max - min) + min);

    },

    calculateModalHeight: function(){

        var newHeight = $('body').height() - 60 - $('.data-generation-modal-window > .modal-dialog > .modal-content > .modal-header').outerHeight();
        return newHeight;

    },

    fixHeight : function(){

        var newHeight = AppHelper.calculateModalHeight();
        $('.data-generation-modal-window > .modal-dialog > .modal-content > .modal-body').height(newHeight + 'px');

    }

};