<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao_log_erro
    * NOME DA CLASSE DAO: DAO_Projetos_versao_log_erro
    * DATA DE GERA��O:    01.05.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_log_erro.php
    * TABELA MYSQL:       projetos_versao_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $obj = new EXTDAO_Projetos_versao_log_erro();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();
   
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);

    $varGET = Param_Get::getIdentificadorProjetosVersao();
    
    $objBanco = new Database();

    $consultaRegistros = "SELECT 
            pvle.id idPVLE, 
            tcb.nome tipo_comando_banco, 
            scb.atributo_atributo_id_INT idAA, 
            scb.seq_INT seqINT, 
            stt.tabela_tabela_id_INT idTT,
            stt.tipo_operacao_atualizacao_banco_id_INT idTOAB,
            toab.nome tipo_operacao_atualizacao_banco
        FROM projetos_versao_log_erro pvle
            JOIN projetos_versao_log_erro_script pvles
                ON pvles.projetos_versao_log_erro_id_INT = pvle.id
            JOIN script_comando_banco scb
                ON pvles.script_comando_banco_id_INT = scb.id
            JOIN tipo_comando_banco tcb
                ON tcb.id = scb.tipo_comando_banco_id_INT
            JOIN script_tabela_tabela stt
                ON scb.script_tabela_tabela_id_INT = stt.id
            JOIN tipo_operacao_atualizacao_banco toab
                ON toab.id = stt.tipo_operacao_atualizacao_banco_id_INT
        WHERE  pvle.projetos_versao_banco_banco_id_INT = {$idPVBB}
        ORDER BY scb.seq_INT ";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Log Dos Erros Ocorrido Na Vers�o De Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="5%" />
			<col width="5%" />
			<col width="30%" />
			<col width="25%" />
			<col width="25%" />
                        <col width="10%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
                        <td class="td_list_titulos">Ordem de execu��o</td>
                        <td class="td_list_titulos">Comando do banco</td>
			
			<td class="td_list_titulos">Relacionamento entre atributo de hom e prod</td>
                        <td class="td_list_titulos">Relacionamento entre tabela de hom e prod</td>
                        
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs["idPVLE"]);
    	$obj->formatarParaExibicao();
        $idAA = $regs["idAA"];
        $idTT = $regs["idTT"];
        $seqINT = $regs["seqINT"];
        
        //  idPVLE, 
        $nomeTCB = $regs["tipo_comando_banco"];
        $idTOAB  = $regs["idTOAB"];
        $nomeTOAB  = $regs["nomeTOAB"];
    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"

    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$seqINT ?>
    		</td>


    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$nomeTCB ?>
    		</td>


                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <?
                        
                        
                        $identificadorAA  = null;
                        //Se for um comando diretamente ligado a atributo_atributo
                        //
                        //Banco Hom - Banco Prod
                        //id@tarefa - id@tarefa
                        if(strlen($idAA)){
                            $identificadorAA  = EXTDAO_Atributo_atributo::getIdentificador($idAA);
                        ?>
                        <a href="index.php?tipo=forms&page=compara_atributo_hom_atributo_prod&<?=$varGET;?>&id1=<?=$idAA;?>">
                            <?=$identificadorAA ;?>
                        </a>
                        <?
                        }else{
                            ?>
                            -
                            <?
                        }
                        ?>
                </td>
                
                
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <?
                        
                        
                        $identificadorTT  = "-";
                        //Se for um comando diretamente ligado a tabela_tabela
                        //
                        //Banco Hom - Banco Prod
                        //id@tarefa - id@tarefa
                        if(strlen($idTT) && $identificadorAA == null){
                            $identificadorTT  = EXTDAO_Tabela_tabela::getIdentificador($idTT);
                        ?>
                        <a href="index.php?tipo=forms&page=compara_tabela_hom_tabela_prod&<?=$varGET;?>&id1=<?=$idTT;?>">
                            <?=$identificadorTT ;?>
                        </a>
                        <?
                        }else{
                            ?>
                            -
                            <?
                        }
                        ?>
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=gerencia_projetos_versao_log_erro&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?
