<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       operacao_sistema_mobile
    * NOME DA CLASSE DAO: DAO_Operacao_sistema_mobile
    * DATA DE GERAÇÃO:    14.05.2013
    * ARQUIVO:            EXTDAO_Operacao_sistema_mobile.php
    * TABELA MYSQL:       operacao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/gerencia_operacao_sistema_mobile.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Operacao_sistema_mobile();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getTipo_operacao_sistema_id_INT())){

            $strCondicao[] = "tipo_operacao_sistema_id_INT={$obj->getTipo_operacao_sistema_id_INT()}";
            $strGET[] = "tipo_operacao_sistema_id_INT={$obj->getTipo_operacao_sistema_id_INT()}";

        }

         if(!Helper::isNull($obj->getEstado_operacao_sistema_mobile_id_INT())){

            $strCondicao[] = "estado_operacao_sistema_mobile_id_INT={$obj->getEstado_operacao_sistema_mobile_id_INT()}";
            $strGET[] = "estado_operacao_sistema_mobile_id_INT={$obj->getEstado_operacao_sistema_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getMobile_identificador_id_INT())){

            $strCondicao[] = "mobile_identificador_id_INT={$obj->getMobile_identificador_id_INT()}";
            $strGET[] = "mobile_identificador_id_INT={$obj->getMobile_identificador_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM operacao_sistema_mobile " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM operacao_sistema_mobile " . $consulta . " ORDER BY estado_operacao_sistema_mobile_id_INT, id DESC LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Operações Do Sistema Do Telefone</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="5%" />
			<col width="18%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao_sistema_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_estado_operacao_sistema_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_mobile_identificador_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_abertura_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_processamento_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_conclusao_DATETIME ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
        $idEstado = $obj->getEstado_operacao_sistema_mobile_id_INT();
        $classTr = "";
        
        switch ($idEstado) {
            
        case EXTDAO_Estado_operacao_sistema_mobile::AGUARDANDO:
                
            $classTr = ($i%2)?"tr_list_conteudo_cinza_impar":"tr_list_conteudo_cinza_par";
            break;
        case EXTDAO_Estado_operacao_sistema_mobile::CANCELADA:
            $classTr = ($i % 2) ? ".tr_list_conteudo_laranja_impar" : ".tr_list_conteudo_laranja_par";
            break;
        case EXTDAO_Estado_operacao_sistema_mobile::CONCLUIDA:
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Estado_operacao_sistema_mobile::ERRO_EXECUCAO:
            $classTr = ($i%2)?"tr_list_conteudo_delete_impar":"tr_list_conteudo_delete_par";
            break;
            
        case EXTDAO_Estado_operacao_sistema_mobile::PROCESSANDO:
            $classTr = ($i%2)?"tr_list_conteudo_edit_impar":"tr_list_conteudo_edit_par";
            break;
        default:
            break;
        }

            

    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_operacao_sistema_id_INT())){
                
                        $obj->getFkObjTipo_operacao_sistema()->select($obj->getTipo_operacao_sistema_id_INT());
                        $obj->getFkObjTipo_operacao_sistema()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_operacao_sistema()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getEstado_operacao_sistema_mobile_id_INT())){
                
                        $obj->getFkObjEstado_operacao_sistema_mobile()->select($obj->getEstado_operacao_sistema_mobile_id_INT());
                        $obj->getFkObjEstado_operacao_sistema_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjEstado_operacao_sistema_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getMobile_identificador_id_INT())){
                
                        $obj->getFkObjMobile_identificador()->select($obj->getMobile_identificador_id_INT());
                        $obj->getFkObjMobile_identificador()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjMobile_identificador()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_abertura_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_processamento_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_conclusao_DATETIME() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				
                                <img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=gerencia_operacao_sistema_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Operacao_sistema_mobile&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='ajax_tab.php?tipo=lists&page=gerencia_operacao_sistema_mobile&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
