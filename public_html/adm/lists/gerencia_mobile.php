<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       mobile
    * NOME DA CLASSE DAO: DAO_Mobile
    * DATA DE GERA��O:    02.04.2013
    * ARQUIVO:            EXTDAO_Mobile.php
    * TABELA MYSQL:       mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/mobile.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Mobile();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

     if(!Helper::isNull($obj->getIdentificador())){

        $strCondicao[] = "identificador LIKE '%{$obj->getIdentificador()}%'";
        $strGET[] = "identificador={$obj->getIdentificador()}";

    }

     if(!Helper::isNull($obj->getImei())){

        $strCondicao[] = "imei LIKE '%{$obj->getImei()}%'";
        $strGET[] = "imei={$obj->getImei()}";

    }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM mobile " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM mobile " . $consulta . " ORDER BY identificador LIMIT {$limites[0]},{$limites[1]}";
    $boMobile = new BO_Mobile();
    
    $objBanco->query($consultaRegistros);
    include 'pages/informacao_mobile.php';

    ?>

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Telefones</legend>

        <table class="tabela_list">
   		
    	    <tbody>

                <?

                for($i=1; $regs = $objBanco->fetchArray(); $i++){

                    $obj->select($regs[0]);
                    $obj->formatarParaExibicao();

                    $classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


                ?>

                    <tr class="<?=$classTr ?>">

                        <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                            <?=pagina_informacao_mobile($regs[0]); ?>
                        </td>

                   </tr>

                <? } ?>

            </tbody>
        </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Pagina��o

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=mobile&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	