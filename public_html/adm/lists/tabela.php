<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       tabela
    * NOME DA CLASSE DAO: DAO_Tabela
    * DATA DE GERAÇÃO:    05.02.2013
    * ARQUIVO:            EXTDAO_Tabela.php
    * TABELA MYSQL:       tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/tabela.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Tabela();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getNome())){

            $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
            $strGET[] = "nome={$obj->getNome()}";

        }

         if(!Helper::isNull($obj->getBanco_id_INT())){

            $strCondicao[] = "banco_id_INT={$obj->getBanco_id_INT()}";
            $strGET[] = "banco_id_INT={$obj->getBanco_id_INT()}";

        }

         if(!Helper::isNull($obj->getProjetos_versao_id_INT())){

            $strCondicao[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";
            $strGET[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";

        }

         if(!Helper::isNull($obj->getFrequencia_sincronizador_INT())){

            $strCondicao[] = "frequencia_sincronizador_INT={$obj->getFrequencia_sincronizador_INT()}";
            $strGET[] = "frequencia_sincronizador_INT={$obj->getFrequencia_sincronizador_INT()}";

        }

         if(!Helper::isNull($obj->getTransmissao_web_para_mobile_BOOLEAN())){

            $strCondicao[] = "transmissao_web_para_mobile_BOOLEAN={$obj->getTransmissao_web_para_mobile_BOOLEAN()}";
            $strGET[] = "transmissao_web_para_mobile_BOOLEAN={$obj->getTransmissao_web_para_mobile_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getTransmissao_mobile_para_web_BOOLEAN())){

            $strCondicao[] = "transmissao_mobile_para_web_BOOLEAN={$obj->getTransmissao_mobile_para_web_BOOLEAN()}";
            $strGET[] = "transmissao_mobile_para_web_BOOLEAN={$obj->getTransmissao_mobile_para_web_BOOLEAN()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM tabela " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM tabela " . $consulta . " ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Tabelas Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_frequencia_sincronizador_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_transmissao_web_para_mobile_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_transmissao_mobile_para_web_BOOLEAN ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getBanco_id_INT())){
                
                        $obj->getFkObjBanco()->select($obj->getBanco_id_INT());
                        $obj->getFkObjBanco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjBanco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_versao_id_INT())){
                
                        $obj->getFkObjProjetos_versao()->select($obj->getProjetos_versao_id_INT());
                        $obj->getFkObjProjetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getFrequencia_sincronizador_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTransmissao_web_para_mobile_BOOLEAN()?"Sim":"Não" ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTransmissao_mobile_para_web_BOOLEAN()?"Sim":"Não" ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=tabela&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=tabela&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Tabela&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=tabela&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
