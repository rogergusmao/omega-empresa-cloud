<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       mobile_identificador
    * NOME DA CLASSE DAO: DAO_Mobile_identificador
    * DATA DE GERAÇÃO:    21.10.2013
    * ARQUIVO:            EXTDAO_Mobile_identificador.php
    * TABELA MYSQL:       mobile_identificador
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/mobile_identificador.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Mobile_identificador();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getMobile_id_INT())){

            $strCondicao[] = "mobile_id_INT={$obj->getMobile_id_INT()}";
            $strGET[] = "mobile_id_INT={$obj->getMobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT())){

            $strCondicao[] = "sistema_projetos_versao_sistema_produto_mobile_id_INT={$obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT()}";
            $strGET[] = "sistema_projetos_versao_sistema_produto_mobile_id_INT={$obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getData_cadastro_DATETIME())){

            $strCondicao[] = "data_cadastro_DATETIME={$obj->getData_cadastro_DATETIME()}";
            $strGET[] = "data_cadastro_DATETIME={$obj->getData_cadastro_DATETIME()}";

        }

         if(!Helper::isNull($obj->getReinstalar_a_versao_id_INT())){

            $strCondicao[] = "reinstalar_a_versao_id_INT={$obj->getReinstalar_a_versao_id_INT()}";
            $strGET[] = "reinstalar_a_versao_id_INT={$obj->getReinstalar_a_versao_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM mobile_identificador " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM mobile_identificador " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Identificadores Dos Telefones</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_projetos_versao_sistema_produto_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_cadastro_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_reinstalar_a_versao_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getMobile_id_INT())){
                
                        $obj->getFkObjMobile()->select($obj->getMobile_id_INT());
                        $obj->getFkObjMobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjMobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT())){
                
                        $obj->getFkObjSistema_projetos_versao_sistema_produto_mobile()->select($obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT());
                        $obj->getFkObjSistema_projetos_versao_sistema_produto_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_projetos_versao_sistema_produto_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_cadastro_DATETIME() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getReinstalar_a_versao_id_INT())){
                
                        $obj->getFkObjReinstalar_a_versao()->select($obj->getReinstalar_a_versao_id_INT());
                        $obj->getFkObjReinstalar_a_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjReinstalar_a_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=mobile_identificador&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=mobile_identificador&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Mobile_identificador&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=mobile_identificador&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
