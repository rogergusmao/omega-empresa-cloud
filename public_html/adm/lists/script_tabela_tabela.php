<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       script_tabela_tabela
    * NOME DA CLASSE DAO: DAO_Script_tabela_tabela
    * DATA DE GERAÇÃO:    05.04.2013
    * ARQUIVO:            EXTDAO_Script_tabela_tabela.php
    * TABELA MYSQL:       script_tabela_tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/script_tabela_tabela.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Script_tabela_tabela();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getTipo_operacao_atualizacao_banco_id_INT())){

            $strCondicao[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";
            $strGET[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getTabela_tabela_id_INT())){

            $strCondicao[] = "tabela_tabela_id_INT={$obj->getTabela_tabela_id_INT()}";
            $strGET[] = "tabela_tabela_id_INT={$obj->getTabela_tabela_id_INT()}";

        }

         if(!Helper::isNull($obj->getScript_projetos_versao_banco_banco_id_INT())){

            $strCondicao[] = "script_projetos_versao_banco_banco_id_INT={$obj->getScript_projetos_versao_banco_banco_id_INT()}";
            $strGET[] = "script_projetos_versao_banco_banco_id_INT={$obj->getScript_projetos_versao_banco_banco_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM script_tabela_tabela " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM script_tabela_tabela " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Script De Atualização Das Tabelas De Homologação</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao_atualizacao_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tabela_tabela_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_script_projetos_versao_banco_banco_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_operacao_atualizacao_banco_id_INT())){
                
                        $obj->getFkObjTipo_operacao_atualizacao_banco()->select($obj->getTipo_operacao_atualizacao_banco_id_INT());
                        $obj->getFkObjTipo_operacao_atualizacao_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_operacao_atualizacao_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTabela_tabela_id_INT())){
                
                        $obj->getFkObjTabela_tabela()->select($obj->getTabela_tabela_id_INT());
                        $obj->getFkObjTabela_tabela()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTabela_tabela()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getScript_projetos_versao_banco_banco_id_INT())){
                
                        $obj->getFkObjScript_projetos_versao_banco_banco()->select($obj->getScript_projetos_versao_banco_banco_id_INT());
                        $obj->getFkObjScript_projetos_versao_banco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjScript_projetos_versao_banco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=script_tabela_tabela&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=script_tabela_tabela&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Script_tabela_tabela&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=script_tabela_tabela&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
