<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       sistema_produto_mobile
    * NOME DA CLASSE DAO: DAO_Sistema_produto_mobile
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            EXTDAO_Sistema_produto_mobile.php
    * TABELA MYSQL:       sistema_produto_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/sistema_produto_mobile.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Sistema_produto_mobile();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getSistema_produto_id_INT())){

            $strCondicao[] = "sistema_produto_id_INT={$obj->getSistema_produto_id_INT()}";
            $strGET[] = "sistema_produto_id_INT={$obj->getSistema_produto_id_INT()}";

        }

         if(!Helper::isNull($obj->getSistema_tipo_mobile_id_INT())){

            $strCondicao[] = "sistema_tipo_mobile_id_INT={$obj->getSistema_tipo_mobile_id_INT()}";
            $strGET[] = "sistema_tipo_mobile_id_INT={$obj->getSistema_tipo_mobile_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM sistema_produto_mobile " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM sistema_produto_mobile " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Produtos Para Telefone</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_produto_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_diretorio_android_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_diretorio_iphone_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_diretorio_windows_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_tipo_mobile_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_produto_id_INT())){
                
                        $obj->getFkObjSistema_produto()->select($obj->getSistema_produto_id_INT());
                        $obj->getFkObjSistema_produto()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_produto()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getDiretorio_android_id_INT())){
                
                        $obj->getFkObjDiretorio_android()->select($obj->getDiretorio_android_id_INT());
                        $obj->getFkObjDiretorio_android()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjDiretorio_android()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getDiretorio_iphone_id_INT())){
                
                        $obj->getFkObjDiretorio_iphone()->select($obj->getDiretorio_iphone_id_INT());
                        $obj->getFkObjDiretorio_iphone()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjDiretorio_iphone()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getDiretorio_windows_mobile_id_INT())){
                
                        $obj->getFkObjDiretorio_windows_mobile()->select($obj->getDiretorio_windows_mobile_id_INT());
                        $obj->getFkObjDiretorio_windows_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjDiretorio_windows_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_tipo_mobile_id_INT())){
                
                        $obj->getFkObjSistema_tipo_mobile()->select($obj->getSistema_tipo_mobile_id_INT());
                        $obj->getFkObjSistema_tipo_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_tipo_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_produto_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_produto_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Sistema_produto_mobile&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=sistema_produto_mobile&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
