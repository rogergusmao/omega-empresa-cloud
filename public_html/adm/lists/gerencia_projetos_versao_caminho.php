<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos_versao_caminho
 * NOME DA CLASSE DAO: DAO_Projetos_versao_caminho
 * DATA DE GERAÇÃO:    23.04.2013
 * ARQUIVO:            EXTDAO_Projetos_versao_caminho.php
 * TABELA MYSQL:       projetos_versao_caminho
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
$acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
$acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
$acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";


$objPE = $objPVPE->getObjProcessoEstrutura();

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Projetos_versao_caminho();



$strCondicao = array();
$strGET = array();

$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
$objPVPE->select($idPVPE);
$vIndicePartido = $objPVPE->getId_atual_projetos_versao_caminho_INT();


$objBanco = new Database();
$consultaRegistros = "SELECT pvc.id 
    FROM projetos_versao_caminho pvc
        JOIN processo_estrutura_caminho pec 
        ON pvc.processo_estrutura_caminho_id_INT = pec.id
        WHERE pvc.projetos_versao_processo_estrutura_id_INT={$idPVPE}
        ORDER BY pec.seq_INT";

$objBanco->query($consultaRegistros);
$varGET = Param_Get::getIdentificadorProjetosVersao(null, $idPV, $idPVBB);

?>



<fieldset class="fieldset_list">
    <legend class="legend_list">PVPE[<?=$objPVPE->getId();?>]<?=  $objPVPE->getNome();?></legend>
    
    
                
    
    <div>
    <? $objPVPE->imprimirAcao($varGET); ?><br/>
     </div>
    <small><?="[ID PROCESSO ESTRUTURA - ".$objPVPE->getProcesso_estrutura_id_INT()."] " ;?></small>
    <table class="tabela_list">
        <colgroup>
            <col width="10%" />
            <col width="5%" />
            <col width="8%" />
            <col width="30%" />
            <col width="30%" />
            <col width="13%" />
            <col width="13%" />
            <col width="13%" />
            <col width="19%" />
            
        </colgroup>
        <thead>
            
            <tr class="tr_list_titulos">
                <td class="td_list_titulos">Id processo estrutura caminho</td>
                <td class="td_list_titulos">Status</td>
                <td class="td_list_titulos">Estado</td>
                <td class="td_list_titulos"><?= $obj->label_processo_estrutura_caminho_id_INT ?></td>
                <td class="td_list_titulos">Página</td>
                <td class="td_list_titulos"><?= $obj->label_data_inicio_DATETIME ?></td>
                <td class="td_list_titulos"><?= $obj->label_data_fim_DATETIME ?></td>
                <td class="td_list_titulos"><?= $obj->label_data_atualizacao_DATETIME ?></td>
                <td class="td_list_titulos">Ações</td>

            </tr>
        </thead>
        <tbody>

<?
$pontoDeInicioDoCiclo = false;
for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

    $obj->select($regs[0]);
    $obj->formatarParaExibicao();
    $vStatus = $obj->getStatus();
    if($obj->getFkObjProcesso_estrutura_caminho()->getId() == "56"){
        $o = 0;
        $o += 1;
    }
    switch ($statusPEAnterior) {
        case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
            if($vStatus != EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO){
                $vStatus = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
            }
            break;
            case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
            if($vStatus != EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO){
                $vStatus = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
            }
            break;
        default:
            break;
    }
    
    $classTr = "";
    switch ($vStatus) {
        case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
            $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";
            break;
        case EXTDAO_Projetos_versao_caminho::EXECUTADO:
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
            $classTr = ($i%2)?"tr_list_conteudo_edit_impar":"tr_list_conteudo_edit_par";
            break;
        case EXTDAO_Projetos_versao_caminho::FINALIZADO:
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
            $classTr = ($i%2)?"tr_list_conteudo_delete_impar":"tr_list_conteudo_delete_par";
            break;
        default:


            break;
    }
    
    ?>

            
                <tr class="<?= $classTr ?>">
                    <td class="td_list_conteudo" style="text-align: center; padding-left: 5px;">
                        <?= $obj->getProcesso_estrutura_caminho_id_INT(); ?>
                    </td>
                    <td class="td_list_conteudo" style="text-align: center; padding-left: 5px;">
                <?
                $vImagem = "";
                switch ($vStatus) {
                    case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
                        $vImagem = "processo";
                        break;
                    case EXTDAO_Projetos_versao_caminho::EXECUTADO:
                        $vImagem = "processo_verde";
                        break;
                    case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
                        $vImagem = "processo_azul";
                        break;
                    case EXTDAO_Projetos_versao_caminho::FINALIZADO:
                        $vImagem = "processo_verde";
                        break;
                    case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
                        $vImagem = "processo_vermelho";
                        break;
                    default:
                        
                            
                        break;
                }
                ?>
                        <img border="0" src="imgs/<?= $vImagem; ?>.png">
                    </td>
  <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                <?
                $vTexto = "";
                switch ($vStatus) {
                    case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
                        $vTexto = "NÃO INICIALIZADO";
                        break;
                    case EXTDAO_Projetos_versao_caminho::EXECUTADO:
                        $vTexto = "EXECUTADO";
                        break;
                    
                    case EXTDAO_Projetos_versao_caminho::FINALIZADO:
                        $vTexto = "FINALIZADO";
                        break;
                    case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
                        $vTexto = "EXECUTANDO";
                        break;
                    case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
                        $vTexto = "DESATUALIZADO";
                        break;
                    default:
                        break;
                }
                ?>
                        <?= $vTexto; ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getProcesso_estrutura_caminho_id_INT())) {

                            $obj->getFkObjProcesso_estrutura_caminho()->select($obj->getProcesso_estrutura_caminho_id_INT());
                            
                            ?>

        <?= $obj->getFkObjProcesso_estrutura_caminho()->getDescricao() ?>

                        <? } ?>

                    </td>
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getPagina() ?>
                    </td>
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getData_inicio_DATETIME() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getData_fim_DATETIME() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $obj->getData_atualizacao_DATETIME() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: center;">
                        
                        <?
                        $varGETLocal2 = "&".Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA."=".$idPVPE;
                        $varGETLocal2 .= "&".$varGET;
                        $varGETLocal2 .= "&".Param_get::PROJETOS_VERSAO_CAMINHO."=".$obj->getId();
                        
                        if(!$cicloFinalizado){
                            
                            
                            if (strlen($vIndicePartido) 
                                    && $vIndicePartido == $obj->getId()) {
                                $pontoDeInicioDoCiclo = true;
                                ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&projetos_versao_processo_estrutura=<?=$varGETLocal2;?>" target="_self" >
                                    Continuar ciclo
                                </a> 
                                <?
//                                if( ($objPE->getFluxo_independente_BOOLEAN() != "1") || $i == 1){
                                    ?>
                                    &nbsp;&nbsp;
                                    <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                       Reiniciar aqui
                                    </a>
                                    <?
//                                }
                            } else if (
                                    strlen($vIndicePartido) 
                                    && !$pontoDeInicioDoCiclo 
                                    && (($objPE->getFluxo_independente_BOOLEAN() != "1") || $i == 1)
                            ) {
                                ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                   Reiniciar aqui
                                </a>
                                <?
                            }
                            else if (!strlen($vIndicePartido) 
                                    && $i == 1) {
                                ?>      
                                <a href="index.php?tipo=pages&page=executa_processo&projetos_versao_processo_estrutura=<?=$varGETLocal2;?>" target="_self" >
                                    Iniciar ciclo aqui
                                </a>
                                <?
                                ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                   Reiniciar ciclo aqui
                                </a>
                                <?
                            } else if(
                                    !strlen($vIndicePartido) 
                                    && $vStatus == EXTDAO_Projetos_versao_caminho::EXECUTADO 
                                    && ($objPE->getFluxo_independente_BOOLEAN() != "1")
                             ) {
                            ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                   Reiniciar ciclo aqui
                                </a>
                                <?
                            } else {
                            ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                   Reiniciar ciclo aqui
                                </a>
                                <?    
                            }
                        } else if($objPE->getFluxo_independente_BOOLEAN() == "1"){
                            
                            if (strlen($vIndicePartido) 
                                    && $vIndicePartido == $obj->getId()) {
                                $pontoDeInicioDoCiclo = true;
                                ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&projetos_versao_processo_estrutura=<?=$varGETLocal2;?>" target="_self" >
                                    Continuar ciclo
                                </a> 
                                <?
                                if( ($objPE->getFluxo_independente_BOOLEAN() != "1") || $i == 1){
                                    ?>
                                    &nbsp;&nbsp;
                                    <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                       Reiniciar aqui
                                    </a>
                                    <?
                                }
                            } else if (
                                    strlen($vIndicePartido) 
                                    && !$pontoDeInicioDoCiclo 
                                    && (($objPE->getFluxo_independente_BOOLEAN() != "1") || $i == 1)
                            ) {
                                ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                   Reiniciar aqui
                                </a>
                                <?
                            }
                            else if (!strlen($vIndicePartido) 
                                    && $i == 1) {
                                ?>      
                                <a href="index.php?tipo=pages&page=executa_processo&projetos_versao_processo_estrutura=<?=$varGETLocal2;?>" target="_self" >
                                    Iniciar ciclo aqui
                                </a>
                                <?
                            } else  {
                            ?>    

                                <a href="index.php?tipo=pages&page=executa_processo&id_caminho_ciclo_reinicializado=<?=$obj->getId();?>&<?=$varGETLocal2;?>" target="_self" >
                                   Reiniciar ciclo aqui
                                </a>
                                <?
                            }
                        }
    ?>
                    </td>



                </tr>

                    <? } ?>

        </tbody>
    </table>

</fieldset>
