<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       operacao_crud_aleatorio
    * NOME DA CLASSE DAO: DAO_Operacao_crud_aleatorio
    * DATA DE GERAÇÃO:    14.07.2013
    * ARQUIVO:            EXTDAO_Operacao_crud_aleatorio.php
    * TABELA MYSQL:       operacao_crud_aleatorio
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/operacao_crud_aleatorio.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Operacao_crud_aleatorio();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getOperacao_sistema_mobile_id_INT())){

            $strCondicao[] = "operacao_sistema_mobile_id_INT={$obj->getOperacao_sistema_mobile_id_INT()}";
            $strGET[] = "operacao_sistema_mobile_id_INT={$obj->getOperacao_sistema_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getTotal_insercao_INT())){

            $strCondicao[] = "total_insercao_INT={$obj->getTotal_insercao_INT()}";
            $strGET[] = "total_insercao_INT={$obj->getTotal_insercao_INT()}";

        }

         if(!Helper::isNull($obj->getPorcentagem_remocao_FLOAT())){

            $strCondicao[] = "porcentagem_remocao_FLOAT={$obj->getPorcentagem_remocao_FLOAT()}";
            $strGET[] = "porcentagem_remocao_FLOAT={$obj->getPorcentagem_remocao_FLOAT()}";

        }

         if(!Helper::isNull($obj->getPorcentagem_edicao_FLOAT())){

            $strCondicao[] = "porcentagem_edicao_FLOAT={$obj->getPorcentagem_edicao_FLOAT()}";
            $strGET[] = "porcentagem_edicao_FLOAT={$obj->getPorcentagem_edicao_FLOAT()}";

        }

         if(!Helper::isNull($obj->getSincronizar_BOOLEAN())){

            $strCondicao[] = "sincronizar_BOOLEAN={$obj->getSincronizar_BOOLEAN()}";
            $strGET[] = "sincronizar_BOOLEAN={$obj->getSincronizar_BOOLEAN()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM operacao_crud_aleatorio " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM operacao_crud_aleatorio " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Operações Crud Aleatório</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_operacao_sistema_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_total_insercao_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_porcentagem_remocao_FLOAT ?></td>
			<td class="td_list_titulos"><?=$obj->label_porcentagem_edicao_FLOAT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sincronizar_BOOLEAN ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getOperacao_sistema_mobile_id_INT())){
                
                        $obj->getFkObjOperacao_sistema_mobile()->select($obj->getOperacao_sistema_mobile_id_INT());
                        $obj->getFkObjOperacao_sistema_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjOperacao_sistema_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTotal_insercao_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPorcentagem_remocao_FLOAT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPorcentagem_edicao_FLOAT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getSincronizar_BOOLEAN()?"Sim":"Não" ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=operacao_crud_aleatorio&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=operacao_crud_aleatorio&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Operacao_crud_aleatorio&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=operacao_crud_aleatorio&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
