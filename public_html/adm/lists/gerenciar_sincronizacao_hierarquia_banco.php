<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       tabela
    * NOME DA CLASSE DAO: DAO_Tabela
    * DATA DE GERAÇÃO:    05.02.2013
    * ARQUIVO:            EXTDAO_Tabela.php
    * TABELA MYSQL:       tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Tabela();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $vetorTabelaOrdenado = EXTDAO_Tabela::getHierarquiaTabelaBanco($objPVBB->getProjetos_versao_id_INT(), $objPVBB->getFkObjBanco_banco()->getHomologacao_banco_id_INT());
    $vetorTabelaDependente = null;
    
    $idTabelaRaiz= Helper::POSTGET(Param_Get::ID1);
    //$vetorTabelaDependente = EXTDAO_Tabela::getListaIdTabelaDependente($idTabelaRaiz, $objPVBB->getProjetos_versao_id_INT(), $objPVBB->getFkObjBanco_banco()->getHomologacao_banco_id_INT());
    //$vetorTabelaDependente = EXTDAO_Tabela::getListaIdTabelaPaiAteRaiz($idTabelaRaiz, $objPVBB->getProjetos_versao_id_INT(), $objPVBB->getFkObjBanco_banco()->getHomologacao_banco_id_INT());
    $vetorTabelaDependente = EXTDAO_Tabela::getListaTabelaPai($objPVBB->getProjetos_versao_id_INT(), $objPVBB->getFkObjBanco_banco()->getHomologacao_banco_id_INT(), $idTabelaRaiz);
    $objTabelaRaiz = new EXTDAO_Tabela();
    $objTabelaRaiz->select($idTabelaRaiz);
    $objBanco = new Database();
    $objTabelaTabela = new EXTDAO_Tabela_tabela();
    
    
    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Tabelas Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="5%" />
			<col width="10%" />
			<col width="40%" />
			<col width="10%" />
			<col width="10%" />
                        <col width="10%" />
			<col width="10%" />
                        <col width="5%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos">Id Tabela Homologação</td>
                        <td class="td_list_titulos"><?=$objTabelaTabela->label_status_verificacao_BOOLEAN?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_frequencia_sincronizador_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_transmissao_web_para_mobile_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_transmissao_mobile_para_web_BOOLEAN ?></td>
                        
                        <td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? 
    $cont = 1;
    for($i=1; $i < count($vetorTabelaOrdenado); $i++){
        if(!in_array($vetorTabelaOrdenado[$i], $vetorTabelaDependente) || 
                $vetorTabelaOrdenado[$i] == $objTabelaRaiz->getId())
            continue;
        else $cont += 1;
    	$obj->select($vetorTabelaOrdenado[$i]);
        $objTabelaTabela = EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB,$obj->getId(), null, false);
        $objEstado = $objTabelaRaiz->getEstadoTabelaPai($obj);
    	$obj->formatarParaExibicao();

    	$classTr = ($cont%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
            switch ($objEstado->tipoMsg) {
                case MENSAGEM_OK:
                    $classTr = ($cont%2)?"tr_list_conteudo_insert_par":"tr_list_conteudo_insert_impar";
                    break;
                case MENSAGEM_WARNING:
                    $classTr = ($cont%2)?"tr_list_conteudo_edit_par":"tr_list_conteudo_edit_impar";
                    break;


                default:
                    break;
            }    

    ?>

    	
    	<tr class="<?=$classTr ?>">
                <input type="hidden" name="id<?=$i ?>" id="id<?=$i ?>" value="<?=$obj->getId(); ?>">
                <input type="hidden" name="nome<?=$i ?>" id="nome<?=$i ?>" value="<?=$obj->getNome(); ?>">
                <input type="hidden" name="banco_id_INT<?=$i ?>" id="banco_id_INT<?=$i ?>" value="<?=$obj->getBanco_id_INT(); ?>">
                <input type="hidden" name="projetos_versao_id_INT<?=$i ?>" id="projetos_versao_id_INT<?=$i ?>" value="<?=$obj->getProjetos_versao_id_INT(); ?>">
                

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <?

                $objArg->numeroDoRegistro = $i;
                $objArg->label = $objTabelaTabela->label_status_verificacao_BOOLEAN;
                $objArg->labelTrue = "Ativo";
                $objArg->labelFalse = "Inativo";
                $objArg->valor = $objTabelaTabela->getStatus_verificacao_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;

                ?>


                <td class="td_form_campo"><?=$objTabelaTabela->imprimirCampoStatus_verificacao_BOOLEAN($objArg); ?></td>


    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>


    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <?  $strSelectUma= "";
                        $strSelectZero= "";
                        $strSelectSempre= "";
                        $vFrequencia = $obj->getFrequencia_sincronizador_INT();
                        if($vFrequencia != null)
                        switch ($vFrequencia) {
                            case -1:
                                $strSelectSempre = "selected";
                                break;
                            case 0:
                                $strSelectZero = "selected";
                                break;
                            case 1:
                                $strSelectUma = "selected";
                                break;
                            default:
                                break;
                        }
                    ?>
                    <select <?="name=\"frequencia_sincronizador_INT$i\""?> <?="id=\"frequencia_sincronizador_INT$i\""?> >
                        <option value="0" <?=$strSelectZero?>>Zero</option>
                        <option value="1" <?=$strSelectUma?>>Uma vez</option>
                        <option value="-1" <?=$strSelectSempre?>>Sempre</option>
                    </select>
    		</td>



    		<?

    			$objArg->numeroDoRegistro = $i;
    			$objArg->label = $obj->label_transmissao_web_para_mobile_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getTransmissao_web_para_mobile_BOOLEAN() ;
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>


    			<td class="td_form_campo"><?=$obj->imprimirCampoTransmissao_web_para_mobile_BOOLEAN($objArg); ?></td>
    			
    		<?

    			$objArg->numeroDoRegistro = $i;
    			$objArg->label = $obj->label_transmissao_mobile_para_web_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getTransmissao_mobile_para_web_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>
    			
    			<td class="td_form_campo"><?=$obj->imprimirCampoTransmissao_mobile_para_web_BOOLEAN($objArg); ?></td>

                        <td class="td_list_conteudo" style="text-align: center;">
				
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=lists&page=gerenciar_sincronizacao_hierarquia_banco&id1=<?=$obj->getId(); ?>&<?=$varGET;?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				
			</td>
    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>
	
