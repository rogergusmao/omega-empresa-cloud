<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       diretorio_android
    * NOME DA CLASSE DAO: DAO_Diretorio_android
    * DATA DE GERAÇÃO:    30.05.2013
    * ARQUIVO:            EXTDAO_Diretorio_android.php
    * TABELA MYSQL:       diretorio_android
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/diretorio_android.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Diretorio_android();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getProjetos_versao_banco_banco_id_INT())){

            $strCondicao[] = "projetos_versao_banco_banco_id_INT={$obj->getProjetos_versao_banco_banco_id_INT()}";
            $strGET[] = "projetos_versao_banco_banco_id_INT={$obj->getProjetos_versao_banco_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getRaiz())){

            $strCondicao[] = "raiz LIKE '%{$obj->getRaiz()}%'";
            $strGET[] = "raiz={$obj->getRaiz()}";

        }

         if(!Helper::isNull($obj->getRaiz_biblioteca_database())){

            $strCondicao[] = "raiz_biblioteca_database LIKE '%{$obj->getRaiz_biblioteca_database()}%'";
            $strGET[] = "raiz_biblioteca_database={$obj->getRaiz_biblioteca_database()}";

        }

         if(!Helper::isNull($obj->getDAO())){

            $strCondicao[] = "DAO LIKE '%{$obj->getDAO()}%'";
            $strGET[] = "DAO={$obj->getDAO()}";

        }

         if(!Helper::isNull($obj->getEXTDAO())){

            $strCondicao[] = "EXTDAO LIKE '%{$obj->getEXTDAO()}%'";
            $strGET[] = "EXTDAO={$obj->getEXTDAO()}";

        }

         if(!Helper::isNull($obj->getForms())){

            $strCondicao[] = "forms LIKE '%{$obj->getForms()}%'";
            $strGET[] = "forms={$obj->getForms()}";

        }

         if(!Helper::isNull($obj->getFiltros())){

            $strCondicao[] = "filtros LIKE '%{$obj->getFiltros()}%'";
            $strGET[] = "filtros={$obj->getFiltros()}";

        }

         if(!Helper::isNull($obj->getLists())){

            $strCondicao[] = "lists LIKE '%{$obj->getLists()}%'";
            $strGET[] = "lists={$obj->getLists()}";

        }

         if(!Helper::isNull($obj->getAdapter())){

            $strCondicao[] = "adapter LIKE '%{$obj->getAdapter()}%'";
            $strGET[] = "adapter={$obj->getAdapter()}";

        }

         if(!Helper::isNull($obj->getItemList())){

            $strCondicao[] = "itemList LIKE '%{$obj->getItemList()}%'";
            $strGET[] = "itemList={$obj->getItemList()}";

        }

         if(!Helper::isNull($obj->getDetail())){

            $strCondicao[] = "detail LIKE '%{$obj->getDetail()}%'";
            $strGET[] = "detail={$obj->getDetail()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM diretorio_android " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM diretorio_android " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Diretórios Dos Projetos Android</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_versao_banco_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_raiz ?></td>
			<td class="td_list_titulos"><?=$obj->label_raiz_biblioteca_database ?></td>
			<td class="td_list_titulos"><?=$obj->label_DAO ?></td>
			<td class="td_list_titulos"><?=$obj->label_EXTDAO ?></td>
			<td class="td_list_titulos"><?=$obj->label_forms ?></td>
			<td class="td_list_titulos"><?=$obj->label_filtros ?></td>
			<td class="td_list_titulos"><?=$obj->label_lists ?></td>
			<td class="td_list_titulos"><?=$obj->label_adapter ?></td>
			<td class="td_list_titulos"><?=$obj->label_itemList ?></td>
			<td class="td_list_titulos"><?=$obj->label_detail ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_versao_banco_banco_id_INT())){
                
                        $obj->getFkObjProjetos_versao_banco_banco()->select($obj->getProjetos_versao_banco_banco_id_INT());
                        $obj->getFkObjProjetos_versao_banco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos_versao_banco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getRaiz() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getRaiz_biblioteca_database() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getDAO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getEXTDAO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getForms() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getFiltros() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getLists() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getAdapter() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getItemList() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getDetail() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=diretorio_android&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=diretorio_android&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Diretorio_android&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=diretorio_android&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
