<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       sistema_produto_log_erro
    * NOME DA CLASSE DAO: DAO_Sistema_produto_log_erro
    * DATA DE GERAÇÃO:    04.07.2013
    * ARQUIVO:            EXTDAO_Sistema_produto_log_erro.php
    * TABELA MYSQL:       sistema_produto_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/gerencia_sistema_produto_log_erro_mobile_identificador.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Sistema_produto_log_erro();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);

         if(!Helper::isNull($obj->getData_ocorrida_DATETIME())){

            $strCondicao[] = "sple.data_ocorrida_DATETIME <= {$obj->getData_ocorrida_DATETIME()}";
            $strGET[] = "data_ocorrida_DATETIME={$obj->getData_ocorrida_DATETIME()}";

        }


         if(!Helper::isNull($obj->getId_usuario_INT())){

            $strCondicao[] = "sple.id_usuario_INT={$obj->getId_usuario_INT()}";
            $strGET[] = "id_usuario_INT={$obj->getId_usuario_INT()}";

        }

         if(!Helper::isNull($obj->getId_corporacao_INT())){

            $strCondicao[] = "sple.id_corporacao_INT={$obj->getId_corporacao_INT()}";
            $strGET[] = "id_corporacao_INT={$obj->getId_corporacao_INT()}";

        }

         if(!Helper::isNull($obj->getMobile_conectado_id_INT())){

            $strCondicao[] = "sple.mobile_conectado_id_INT={$obj->getMobile_conectado_id_INT()}";
            $strGET[] = "mobile_conectado_id_INT={$obj->getMobile_conectado_id_INT()}";

        }
        if(strlen($idMI)){
            $strCondicao[] = "mi.id={$idMI}";
            $strGET[] = "mobile_identificador={$idMI}";
        }
    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(sple.id) "
        . " FROM sistema_produto_log_erro sple "
        . "     JOIN mobile_conectado mc ON sple.mobile_conectado_id_INT = mc.id "
        . "     JOIN mobile_identificador mi ON mi.id = mc.mobile_identificador_id_INT " 
        . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT sple.id "
        . " FROM sistema_produto_log_erro sple "
         . "    JOIN mobile_conectado mc ON sple.mobile_conectado_id_INT = mc.id "
        . "     JOIN mobile_identificador mi ON mi.id = mc.mobile_identificador_id_INT " 
        . " " . $consulta . " "
        . " ORDER BY sple.id DESC "
        . " LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Log De Erros Ocorridos Nas Versões Do Produto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="5%" />
			<col width="5%" />
			<col width="5%" />
			<col width="5%" />
			<col width="5%" />
			<col width="5%" />
			<col width="5%" />
			<col width="5%" />
			
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_classe ?></td>
			<td class="td_list_titulos"><?=$obj->label_funcao ?></td>
			<td class="td_list_titulos"><?=$obj->label_linha_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome_arquivo ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_ocorrida_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_tipo_log_erro_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getClasse() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getFuncao() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getLinha_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome_arquivo() ?>
    		</td>

    		
    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_ocorrida_DATETIME() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_tipo_log_erro_id_INT())){
                
                        $obj->getFkObjSistema_tipo_log_erro()->select($obj->getSistema_tipo_log_erro_id_INT());
                        $obj->getFkObjSistema_tipo_log_erro()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_tipo_log_erro()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>


                <td class="td_list_conteudo" style="text-align: center;">
                        
                        <img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_produto_log_erro&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        <img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Sistema_produto_log_erro&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                </td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=gerencia_sistema_produto_log_erro_mobile_identificador&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
