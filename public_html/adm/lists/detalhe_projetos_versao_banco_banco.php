<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao_banco_banco
    * NOME DA CLASSE DAO: DAO_Projetos_versao_banco_banco
    * DATA DE GERAÇÃO:    27.05.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_banco_banco.php
    * TABELA MYSQL:       projetos_versao_banco_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";
    Helper::imprimirMensagem("Para publicar a versão atual, acesse o menu Andamento para verificar os protótipos que ainda possuem processos pendentes de execução.", MENSAGEM_INFO);
    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    
    $objPVBB->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    $idPV = Helper::GET(Param_Get::ID_PROJETOS_VERSAO);

    if(strlen($idPV)){

        $strCondicao[] = "projetos_versao_id_INT=$idPV";
        

    }

        

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM projetos_versao_banco_banco " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM projetos_versao_banco_banco " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Protótipos (Apps/Sites)</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="5%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$objPVBB->label_id ?></td>
			<td class="td_list_titulos"><?=$objPVBB->label_nome ?></td>
			<td class="td_list_titulos"><?=$objPVBB->label_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos"><?=$objPVBB->label_banco_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$objPVBB->label_script_estrutura_banco_hom_para_prod_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$objPVBB->label_script_registros_banco_hom_para_prod_ARQUIVO ?></td>
                        <td class="td_list_titulos"><?=$objPVBB->label_tipo_plataforma_operacional_id_INT ?></td>
			
			

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$objPVBB->select($regs[0]);
    	$objPVBB->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$objPVBB->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$objPVBB->getNome() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($objPVBB->getProjetos_versao_id_INT())){
                
                        $objPVBB->getFkObjProjetos_versao()->select($objPVBB->getProjetos_versao_id_INT());
                        $objPVBB->getFkObjProjetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$objPVBB->getFkObjProjetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($objPVBB->getBanco_banco_id_INT())){
                
                        $objPVBB->getFkObjBanco_banco()->select($objPVBB->getBanco_banco_id_INT());
                        $objPVBB->getFkObjBanco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$objPVBB->getFkObjBanco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$objPVBB->getScript_estrutura_banco_hom_para_prod_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$objPVBB->getScript_registros_banco_hom_para_prod_ARQUIVO() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                     <? if(strlen($objPVBB->getTipo_plataforma_operacional_id_INT())){
                
                        $objPVBB->getFkObjTipo_plataforma_operacional()->select($objPVBB->getTipo_plataforma_operacional_id_INT());
                        $objPVBB->getFkObjTipo_plataforma_operacional()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$objPVBB->getFkObjTipo_plataforma_operacional()->valorCampoLabel() ?>

                    <? } ?>
                </td>

			


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=projetos_versao_banco_banco&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
