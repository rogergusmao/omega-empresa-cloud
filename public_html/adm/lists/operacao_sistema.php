<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       operacao_sistema
    * NOME DA CLASSE DAO: DAO_Operacao_sistema
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            EXTDAO_Operacao_sistema.php
    * TABELA MYSQL:       operacao_sistema
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    * GERENCIADOR DE DATAGRIDS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    
    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";
    
    include("filters/operacao_sistema.php");
    
    $registrosPorPagina = REGISTROS_POR_PAGINA;
    
    $registrosPesquisa = 1;
    
    $obj = new EXTDAO_Operacao_sistema();    
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();
    
    $strCondicao = array();
    $strGET = array();
    
    
     
         if(!Helper::isNull($obj->getUsuario_id_INT())){
         
            $strCondicao[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";
            $strGET[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";

        }
     
         if(!Helper::isNull($obj->getTipo_operacao())){
         
            $strCondicao[] = "tipo_operacao LIKE '%{$obj->getTipo_operacao()}%'";
            $strGET[] = "tipo_operacao={$obj->getTipo_operacao()}";

        }
     
         if(!Helper::isNull($obj->getPagina_operacao())){
         
            $strCondicao[] = "pagina_operacao LIKE '%{$obj->getPagina_operacao()}%'";
            $strGET[] = "pagina_operacao={$obj->getPagina_operacao()}";

        }
     
         if(!Helper::isNull($obj->getEntidade_operacao())){
         
            $strCondicao[] = "entidade_operacao LIKE '%{$obj->getEntidade_operacao()}%'";
            $strGET[] = "entidade_operacao={$obj->getEntidade_operacao()}";

        }
     
         if(!Helper::isNull($obj->getChave_registro_operacao_INT())){
         
            $strCondicao[] = "chave_registro_operacao_INT={$obj->getChave_registro_operacao_INT()}";
            $strGET[] = "chave_registro_operacao_INT={$obj->getChave_registro_operacao_INT()}";

        }
     
         if(!Helper::isNull($obj->getData_operacao_DATETIME())){
         
            $strCondicao[] = "data_operacao_DATETIME={$obj->getData_operacao_DATETIME()}";
            $strGET[] = "data_operacao_DATETIME={$obj->getData_operacao_DATETIME()}";

        }
    
    $consulta = "";
    
    for($i=0; $i<count($strCondicao); $i++){
        
        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else 
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];
        
    }
    
    $consultaNumero = "SELECT COUNT(id) FROM operacao_sistema " . $consulta;
    
    $objBanco = new Database();
    
    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);
    
    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);
    
    $consultaRegistros = "SELECT id FROM operacao_sistema " . $consulta . " ORDER BY data_operacao_DATETIME DESC LIMIT {$limites[0]},{$limites[1]}";
    
    $objBanco->query($consultaRegistros);
    
    ?>
    
    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Operações No Sistema</legend>
    
   <table class="tabela_list">
   		<colgroup>
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_usuario_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao ?></td>
			<td class="td_list_titulos"><?=$obj->label_entidade_operacao ?></td>
			<td class="td_list_titulos"><?=$obj->label_chave_registro_operacao_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_operacao_DATETIME ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){
    
    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();
    	
    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"
    	
    	
    ?>
    
    	<tr class="<?=$classTr ?>">

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                	<? $obj->getFkObjUsuario()->select($obj->getUsuario_id_INT()) ?>
                    <? $obj->getFkObjUsuario()->formatarParaExibicao() ?>
                	<?=$obj->getFkObjUsuario()->valorCampoLabel() ?>
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTipo_operacao() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=ucfirst($obj->getEntidade_operacao()) ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getChave_registro_operacao_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_operacao_DATETIME() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=operacao_sistema&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=operacao_sistema&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Operacao_sistema&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>

    
    
		</tr>
    
    <? } ?>
    
    </tbody>
    </table>
    
    </fieldset>
    
    <br/>
    <br/>
    
    <?
    
    //Paginação
    
    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);
    
    if($numeroPaginas > 1){
    
    ?>
    
    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>
    
	<table class="table_paginacao">

	<?
	
	for($i=1; $i <= $numeroPaginas; $i++){
	
		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao";
		
		if(($i-1) % 30 == 0){
	
		    echo "<tr class=\"tr_paginacao\">";
		    
		}
		    
	?>
		
		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=operacao_sistema&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>
	
	<?
	
	    if(($i-1) % 30 == 29){
	
		    echo "</tr>";
		    
		}
	
	 } ?>

	</table>
	
	</fieldset>
	
	<? } ?>
	
	
