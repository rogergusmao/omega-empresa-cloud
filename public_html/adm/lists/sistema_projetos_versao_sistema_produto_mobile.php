<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       sistema_projetos_versao_sistema_produto_mobile
    * NOME DA CLASSE DAO: DAO_Sistema_projetos_versao_sistema_produto_mobile
    * DATA DE GERAÇÃO:    27.06.2013
    * ARQUIVO:            EXTDAO_Sistema_projetos_versao_sistema_produto_mobile.php
    * TABELA MYSQL:       sistema_projetos_versao_sistema_produto_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/sistema_projetos_versao_sistema_produto_mobile.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getSistema_produto_mobile_id_INT())){

            $strCondicao[] = "sistema_produto_mobile_id_INT={$obj->getSistema_produto_mobile_id_INT()}";
            $strGET[] = "sistema_produto_mobile_id_INT={$obj->getSistema_produto_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getSistema_projetos_versao_id_INT())){

            $strCondicao[] = "sistema_projetos_versao_id_INT={$obj->getSistema_projetos_versao_id_INT()}";
            $strGET[] = "sistema_projetos_versao_id_INT={$obj->getSistema_projetos_versao_id_INT()}";

        }

         if(!Helper::isNull($obj->getPrograma_ARQUIVO())){

            $strCondicao[] = "programa_ARQUIVO={$obj->getPrograma_ARQUIVO()}";
            $strGET[] = "programa_ARQUIVO={$obj->getPrograma_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getSistema_projetos_versao_produto_id_INT())){

            $strCondicao[] = "sistema_projetos_versao_produto_id_INT={$obj->getSistema_projetos_versao_produto_id_INT()}";
            $strGET[] = "sistema_projetos_versao_produto_id_INT={$obj->getSistema_projetos_versao_produto_id_INT()}";

        }

         if(!Helper::isNull($obj->getVersion_code())){

            $strCondicao[] = "version_code LIKE '%{$obj->getVersion_code()}%'";
            $strGET[] = "version_code={$obj->getVersion_code()}";

        }

         if(!Helper::isNull($obj->getVersion_name())){

            $strCondicao[] = "version_name LIKE '%{$obj->getVersion_name()}%'";
            $strGET[] = "version_name={$obj->getVersion_name()}";

        }

         if(!Helper::isNull($obj->getPacote_completo_zipado_ARQUIVO())){

            $strCondicao[] = "pacote_completo_zipado_ARQUIVO={$obj->getPacote_completo_zipado_ARQUIVO()}";
            $strGET[] = "pacote_completo_zipado_ARQUIVO={$obj->getPacote_completo_zipado_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getBanco_db_ARQUIVO())){

            $strCondicao[] = "banco_db_ARQUIVO={$obj->getBanco_db_ARQUIVO()}";
            $strGET[] = "banco_db_ARQUIVO={$obj->getBanco_db_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getXml_publicacao_ARQUIVO())){

            $strCondicao[] = "xml_publicacao_ARQUIVO={$obj->getXml_publicacao_ARQUIVO()}";
            $strGET[] = "xml_publicacao_ARQUIVO={$obj->getXml_publicacao_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getResetar_banco_no_fim_da_atualizacao_BOOLEAN())){

            $strCondicao[] = "resetar_banco_no_fim_da_atualizacao_BOOLEAN={$obj->getResetar_banco_no_fim_da_atualizacao_BOOLEAN()}";
            $strGET[] = "resetar_banco_no_fim_da_atualizacao_BOOLEAN={$obj->getResetar_banco_no_fim_da_atualizacao_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getEnviar_dados_locais_do_sincronizador_BOOLEAN())){

            $strCondicao[] = "enviar_dados_locais_do_sincronizador_BOOLEAN={$obj->getEnviar_dados_locais_do_sincronizador_BOOLEAN()}";
            $strGET[] = "enviar_dados_locais_do_sincronizador_BOOLEAN={$obj->getEnviar_dados_locais_do_sincronizador_BOOLEAN()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM sistema_projetos_versao_sistema_produto_mobile " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM sistema_projetos_versao_sistema_produto_mobile " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Versões Dos Produtos Mobile Do Sistema</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_produto_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_programa_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_projetos_versao_produto_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_version_code ?></td>
			<td class="td_list_titulos"><?=$obj->label_version_name ?></td>
			<td class="td_list_titulos"><?=$obj->label_pacote_completo_zipado_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_banco_db_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_xml_publicacao_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_resetar_banco_no_fim_da_atualizacao_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_enviar_dados_locais_do_sincronizador_BOOLEAN ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_produto_mobile_id_INT())){
                
                        $obj->getFkObjSistema_produto_mobile()->select($obj->getSistema_produto_mobile_id_INT());
                        $obj->getFkObjSistema_produto_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_produto_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_projetos_versao_id_INT())){
                
                        $obj->getFkObjSistema_projetos_versao()->select($obj->getSistema_projetos_versao_id_INT());
                        $obj->getFkObjSistema_projetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_projetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPrograma_ARQUIVO() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_projetos_versao_produto_id_INT())){
                
                        $obj->getFkObjSistema_projetos_versao_produto()->select($obj->getSistema_projetos_versao_produto_id_INT());
                        $obj->getFkObjSistema_projetos_versao_produto()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_projetos_versao_produto()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getVersion_code() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getVersion_name() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPacote_completo_zipado_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getBanco_db_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getXml_publicacao_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getResetar_banco_no_fim_da_atualizacao_BOOLEAN()?"Sim":"Não" ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getEnviar_dados_locais_do_sincronizador_BOOLEAN()?"Sim":"Não" ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_projetos_versao_sistema_produto_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_projetos_versao_sistema_produto_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Sistema_projetos_versao_sistema_produto_mobile&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=sistema_projetos_versao_sistema_produto_mobile&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
