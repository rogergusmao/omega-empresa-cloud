<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao
    * NOME DA CLASSE DAO: DAO_Projetos_versao
    * DATA DE GERAÇÃO:    27.05.2013
    * ARQUIVO:            EXTDAO_Projetos_versao.php
    * TABELA MYSQL:       projetos_versao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/projetos_versao.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Projetos_versao();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getNome())){

            $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
            $strGET[] = "nome={$obj->getNome()}";

        }

         if(!Helper::isNull($obj->getProjetos_id_INT())){

            $strCondicao[] = "projetos_id_INT={$obj->getProjetos_id_INT()}";
            $strGET[] = "projetos_id_INT={$obj->getProjetos_id_INT()}";

        }

         if(!Helper::isNull($obj->getData_homologacao_DATETIME())){

            $strCondicao[] = "data_homologacao_DATETIME={$obj->getData_homologacao_DATETIME()}";
            $strGET[] = "data_homologacao_DATETIME={$obj->getData_homologacao_DATETIME()}";

        }

         if(!Helper::isNull($obj->getData_producao_DATETIME())){

            $strCondicao[] = "data_producao_DATETIME={$obj->getData_producao_DATETIME()}";
            $strGET[] = "data_producao_DATETIME={$obj->getData_producao_DATETIME()}";

        }

         if(!Helper::isNull($obj->getCopia_do_projetos_versao_id_INT())){

            $strCondicao[] = "copia_do_projetos_versao_id_INT={$obj->getCopia_do_projetos_versao_id_INT()}";
            $strGET[] = "copia_do_projetos_versao_id_INT={$obj->getCopia_do_projetos_versao_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM projetos_versao " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM projetos_versao " . $consulta . " ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Versões Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_homologacao_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_producao_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_copia_do_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_id_INT())){
                
                        $obj->getFkObjProjetos()->select($obj->getProjetos_id_INT());
                        $obj->getFkObjProjetos()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_homologacao_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_producao_DATETIME() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getCopia_do_projetos_versao_id_INT())){
                
                        $obj->getFkObjCopia_do_projetos_versao()->select($obj->getCopia_do_projetos_versao_id_INT());
                        $obj->getFkObjCopia_do_projetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjCopia_do_projetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=projetos_versao&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=projetos_versao&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Projetos_versao&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=projetos_versao&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
