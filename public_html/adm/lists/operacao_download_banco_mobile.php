<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       operacao_download_banco_mobile
    * NOME DA CLASSE DAO: DAO_Operacao_download_banco_mobile
    * DATA DE GERAÇÃO:    30.05.2013
    * ARQUIVO:            EXTDAO_Operacao_download_banco_mobile.php
    * TABELA MYSQL:       operacao_download_banco_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/operacao_download_banco_mobile.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Operacao_download_banco_mobile();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getPath_script_sql_banco())){

            $strCondicao[] = "path_script_sql_banco LIKE '%{$obj->getPath_script_sql_banco()}%'";
            $strGET[] = "path_script_sql_banco={$obj->getPath_script_sql_banco()}";

        }

         if(!Helper::isNull($obj->getObservacao())){

            $strCondicao[] = "observacao LIKE '%{$obj->getObservacao()}%'";
            $strGET[] = "observacao={$obj->getObservacao()}";

        }

         if(!Helper::isNull($obj->getOperacao_sistema_mobile_id_INT())){

            $strCondicao[] = "operacao_sistema_mobile_id_INT={$obj->getOperacao_sistema_mobile_id_INT()}";
            $strGET[] = "operacao_sistema_mobile_id_INT={$obj->getOperacao_sistema_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getPopular_tabela_BOOLEAN())){

            $strCondicao[] = "popular_tabela_BOOLEAN={$obj->getPopular_tabela_BOOLEAN()}";
            $strGET[] = "popular_tabela_BOOLEAN={$obj->getPopular_tabela_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getDrop_tabela_BOOLEAN())){

            $strCondicao[] = "drop_tabela_BOOLEAN={$obj->getDrop_tabela_BOOLEAN()}";
            $strGET[] = "drop_tabela_BOOLEAN={$obj->getDrop_tabela_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getDestino_banco_id_INT())){

            $strCondicao[] = "destino_banco_id_INT={$obj->getDestino_banco_id_INT()}";
            $strGET[] = "destino_banco_id_INT={$obj->getDestino_banco_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM operacao_download_banco_mobile " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM operacao_download_banco_mobile " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Operação De Sistema Do Telefone</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_path_script_sql_banco ?></td>
			<td class="td_list_titulos"><?=$obj->label_observacao ?></td>
			<td class="td_list_titulos"><?=$obj->label_operacao_sistema_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_popular_tabela_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_drop_tabela_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_destino_banco_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPath_script_sql_banco() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getObservacao() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getOperacao_sistema_mobile_id_INT())){
                
                        $obj->getFkObjOperacao_sistema_mobile()->select($obj->getOperacao_sistema_mobile_id_INT());
                        $obj->getFkObjOperacao_sistema_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjOperacao_sistema_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPopular_tabela_BOOLEAN()?"Sim":"Não" ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getDrop_tabela_BOOLEAN()?"Sim":"Não" ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getDestino_banco_id_INT())){
                
                        $obj->getFkObjDestino_banco()->select($obj->getDestino_banco_id_INT());
                        $obj->getFkObjDestino_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjDestino_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=operacao_download_banco_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=operacao_download_banco_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Operacao_download_banco_mobile&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=operacao_download_banco_mobile&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
