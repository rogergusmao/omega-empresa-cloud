<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       permissao_sistema_mobile
    * NOME DA CLASSE DAO: DAO_Permissao_sistema_mobile
    * DATA DE GERAÇÃO:    19.10.2013
    * ARQUIVO:            EXTDAO_Permissao_sistema_mobile.php
    * TABELA MYSQL:       permissao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
  


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";
    
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    
    if(!$objPVBB->isMobile()){
        Helper::imprimirMensagem("O protótipo não é do Android.");
        return;
    } else if(!strlen(Helper::GET("verificada"))){
        $q = "SELECT id
            FROM permissao_sistema_mobile p 
            WHERE p.projetos_versao_banco_banco_id_INT = $idPVBB";
        
        $db = new Database();
        $db->query($q);
        
            
            
            $dbHomologacao = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao(28);
            $dbHomologacao->query("SELECT id, pai_permissao_id_INT, nome, tag
                FROM permissao");
            $matrix = Helper::getResultSetToMatriz($dbHomologacao->result);
            
            
            $idDir = EXTDAO_Diretorio_android::getIdDiretorio($idPVBB);
            $objDA = new EXTDAO_Diretorio_android();
            $objDA->select($idDir);
            $raiz = $objDA->getRaiz();
            $raizRes = $raiz."res/";
            $raizDrawable = $raizRes."drawable";
            
            
            for($i = 0 ; $i < count($matrix); $i++){
                $item = $matrix[$i];
                $objPSM = new EXTDAO_Permissao_sistema_mobile();
                $idPermissao = EXTDAO_Permissao_sistema_mobile::getIdPermissao($idPVBB, $item[3]);
                if(strlen($idPermissao))
                    continue;
                if(strlen($item[1]))
                    $objPSM->setPai_permissao_id_INT($item[1]);
                $objPSM->setNome($item[2]);
                $objPSM->setTag($item[3]);
                $objPSM->setProjetos_versao_banco_banco_id_INT($idPVBB);
                
                $prefixoNomeArquivoImagem = str_replace(" ", "_", $item[2]);
                $prefixoNomeArquivoImagem = Helper::strtolowerlatin1($prefixoNomeArquivoImagem);
                //Verifica a existencia dos arquivos nas pastas
                $dimensoes = array("_32", "_64", "_128", "_256");
                for($i = 0 ; $i < count($dimensoes); $i ++){
                    $nomeArquivo = $prefixoNomeArquivoImagem.$dimensoes[$i].".png";
                    switch ($i) {
                        case 0:
                            if(is_file($raizDrawable.$nomeArquivo)){
                                $objPSM->setIcone_32_ARQUIVO($nomeArquivo);
                            }
                            break;
                        case 1:
                            if(is_file($raizDrawable.$nomeArquivo)){
                                $objPSM->setIcone_64_ARQUIVO($nomeArquivo);
                            }
                            break;
                       case 2:
                            if(is_file($raizDrawable.$nomeArquivo)){
                                $objPSM->setIcone_128_ARQUIVO($nomeArquivo);
                            }
                            break;
                       case 3:
                            if(is_file($raizDrawable.$nomeArquivo)){
                                $objPSM->setIcone_256_ARQUIVO($nomeArquivo);
                            }
                            break;
                        default:
                            break;
                    }
                }
              
                $objPSM->formatarParaSQL();
                $objPSM->insert();    
            }
            
                    
        
        $varGET .= "&verificada=1";
    }
    
   // include("filters/gerencia_permissao_sistema_mobile.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    
    
    $registrosPesquisa = 1;

    $obj = new EXTDAO_Permissao_sistema_mobile();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getNome())){

            $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
            $strGET[] = "nome1={$obj->getNome()}";

        }

         if(!Helper::isNull($obj->getTag())){

            $strCondicao[] = "tag LIKE '%{$obj->getTag()}%'";
            $strGET[] = "tag1={$obj->getTag()}";

        }

         if(!Helper::isNull($obj->getPai_permissao_id_INT())){

            $strCondicao[] = "pai_permissao_id_INT1={$obj->getPai_permissao_id_INT()}";
            $strGET[] = "pai_permissao_id_INT1={$obj->getPai_permissao_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_permissao_id_INT())){

            $strCondicao[] = "tipo_permissao_id_INT1={$obj->getTipo_permissao_id_INT()}";
            $strGET[] = "tipo_permissao_id_INT1={$obj->getTipo_permissao_id_INT()}";

        }

         if(!Helper::isNull($obj->getIcone_32_ARQUIVO())){

            $strCondicao[] = "icone_32_ARQUIVO1={$obj->getIcone_32_ARQUIVO()}";
            $strGET[] = "icone_32_ARQUIVO1={$obj->getIcone_32_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getIcone_64_ARQUIVO())){

            $strCondicao[] = "icone_64_ARQUIVO1={$obj->getIcone_64_ARQUIVO()}";
            $strGET[] = "icone_64_ARQUIVO1={$obj->getIcone_64_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getIcone_128_ARQUIVO())){

            $strCondicao[] = "icone_128_ARQUIVO1={$obj->getIcone_128_ARQUIVO()}";
            $strGET[] = "icone_128_ARQUIVO1={$obj->getIcone_128_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getIcone_256_ARQUIVO())){

            $strCondicao[] = "icone_256_ARQUIVO1={$obj->getIcone_256_ARQUIVO()}";
            $strGET[] = "icone_256_ARQUIVO1={$obj->getIcone_256_ARQUIVO()}";

        }

         if(!Helper::isNull($obj->getProjetos_versao_banco_banco_id_INT())){

            $strCondicao[] = "projetos_versao_banco_banco_id_INT1={$obj->getProjetos_versao_banco_banco_id_INT()}";
            $strGET[] = "projetos_versao_banco_banco_id_INT1={$obj->getProjetos_versao_banco_banco_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i < count($strCondicao); $i++){

        $consulta .= " AND " . $strCondicao[$i];

    }
    
    for($i=0; $i < count($strGET); $i++){

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM permissao_sistema_mobile WHERE excluido_BOOLEAN=0 {$consulta}";

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM permissao_sistema_mobile WHERE excluido_BOOLEAN=0 {$consulta} ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Permissões Do Sistema Mobile</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
			<col width="9%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_tag ?></td>
			<td class="td_list_titulos"><?=$obj->label_pai_permissao_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_permissao_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_icone_32_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_icone_64_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_icone_128_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_icone_256_ARQUIVO ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_versao_banco_banco_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? 
    
    if($objBanco->rows() == 0){
    
    ?>
    
    <tr class="tr_list_conteudo_impar">
        <td  colspan="11">
            <?=Helper::imprimirMensagem("Nenhuma permissão do sistema mobile foi cadastrada até o momento.") ?>
        </td>
    </tr>

    <?

    }

    for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTag() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getPai_permissao_id_INT())){
                
                        $obj->getFkObjPai_permissao()->select($obj->getPai_permissao_id_INT());
                        $obj->getFkObjPai_permissao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjPai_permissao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_permissao_id_INT())){
                
                        $obj->getFkObjTipo_permissao()->select($obj->getTipo_permissao_id_INT());
                        $obj->getFkObjTipo_permissao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_permissao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getIcone_32_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getIcone_64_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getIcone_128_ARQUIVO() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getIcone_256_ARQUIVO() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_versao_banco_banco_id_INT())){
                
                        $obj->getFkObjProjetos_versao_banco_banco()->select($obj->getProjetos_versao_banco_banco_id_INT());
                        $obj->getFkObjProjetos_versao_banco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos_versao_banco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=permissao_sistema_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=permissao_sistema_mobile&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Permissao_sistema_mobile&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=permissao_sistema_mobile&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } 



?>
