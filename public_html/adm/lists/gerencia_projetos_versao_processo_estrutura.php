<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos_versao_processo_estrutura
 * NOME DA CLASSE DAO: DAO_Projetos_versao_processo_estrutura
 * DATA DE GERAÇÃO:    27.04.2013
 * ARQUIVO:            EXTDAO_Projetos_versao_processo_estrutura.php
 * TABELA MYSQL:       projetos_versao_processo_estrutura
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

if(strlen(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO))){
    $idSP = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO);
    
    
    $idSPVP = EXTDAO_Sistema_produto::getIdDaUltimaVersao($idSP);
    
    //$objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP->select($idSPVP);
    $idPVBB = $objSPVP->getServidor_projetos_versao_banco_banco_id_INT();
    
    
} else{
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    
}
$objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $idPV = $objPVBB->getProjetos_versao_id_INT();
$objProjetosVersao = new EXTDAO_Projetos_versao();
$objProjetosVersao->select($idPV);
$legend= $objProjetosVersao->getFkObjTipo_analise_projeto()->valorCampoLabel();



$varGET = Param_Get::getIdentificadorProjetosVersao(null, $idPV, $idPVBB);

$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
$objBanco = new Database();

if( !EXTDAO_Projetos_versao_processo_estrutura::validaListaProcessoEstrutura($idPVBB)){
    Helper::imprimirMensagem("O processo de atualização ainda não foi iniciado, <a target=\"_self\" href=\"".EXTDAO_Projetos_versao_processo_estrutura::getUrlDeInicializacaoDeProcesso($idPV, $idPVBB)."\" >clique aqui</a> para iniciar.", MENSAGEM_WARNING);
    exit();
}

$consultaRegistros = "SELECT id 
        FROM projetos_versao_processo_estrutura 
        WHERE projetos_versao_banco_banco_id_INT = $idPVBB
        ORDER BY id";

$objBanco->query($consultaRegistros);

?>
<fieldset class="fieldset_list">
    <legend class="legend_list"><?=$legend;?></legend>

    <table class="tabela_list">
        <colgroup>
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">

                <td class="td_list_titulos"><?= $objPVPE->label_id ?></td>
                
                <td class="td_list_titulos"><?= $objPVPE->label_processo_estrutura_id_INT ?></td>
                <td class="td_list_titulos"><?= $objPVPE->label_data_inicio_DATETIME ?></td>
                <td class="td_list_titulos"><?= $objPVPE->label_data_fim_DATETIME ?></td>
                
                <td class="td_list_titulos">Ações</td>

            </tr>
        </thead>
        <tbody>

<?
$statusPredominante = null;
$idPVPEAntigo = null;
for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

    $objPVPE->select($regs[0]);
    $objPVPE->formatarParaExibicao();
    $status = $objPVPE->getStatus();
    if($statusPredominante != null
       && ($statusPredominante == EXTDAO_Projetos_versao_caminho::EXECUTANDO
         || $statusPredominante == EXTDAO_Projetos_versao_caminho::DESATUALIZADO
          )
       && $status != EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO
      ){
            $status = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
    } else if($statusPredominante == null) 
        $statusPredominante = $status;
    else if($statusPredominante > $status)
        $status = $statusPredominante;
    else{
        if($statusPredominante == EXTDAO_Projetos_versao_caminho::EXECUTADO
            && $idPVPEAntigo != null
            && $statusPredominante != EXTDAO_Projetos_versao_caminho::DESATUALIZADO){
            if($objPVPE->desatualizadoComparado($idPVPEAntigo))
                $status = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
        }
    }
    $idPVPEAntigo = $objPVPE->getId();
    
    $classTr = "";
    switch ($status) {
        case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
            $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";
            break;
        case EXTDAO_Projetos_versao_caminho::EXECUTADO:
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
            $classTr = ($i%2)?"tr_list_conteudo_edit_impar":"tr_list_conteudo_edit_par";
            break;
        case EXTDAO_Projetos_versao_caminho::FINALIZADO:
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
            $classTr = ($i%2)?"tr_list_conteudo_delete_impar":"tr_list_conteudo_delete_par";
            break;
        default:


            break;
    }
    ?>

                <tr class="<?= $classTr ?>">

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $objPVPE->getId() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

    <?
    if (strlen($objPVPE->getProcesso_estrutura_id_INT())) {

        $objPVPE->getFkObjProcesso_estrutura()->select($objPVPE->getProcesso_estrutura_id_INT());
        $objPVPE->getFkObjProcesso_estrutura()->formatarParaExibicao();
        ?>

                            <?= $objPVPE->getFkObjProcesso_estrutura()->valorCampoLabel() ?>

                        <? } ?>

                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $objPVPE->getDataInicioProcesso() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $objPVPE->getDataFimProcesso() ?>
                    </td>

                
                    <td class="td_list_conteudo" style="text-align: center;">
                        
                        <?
                        $objPVPE->imprimirAcao($varGET);
                        ?>
                    </td>



                </tr>
                

<? } 

 
?>
            
        </tbody>
    </table>

</fieldset>

<br/>
<br/>

