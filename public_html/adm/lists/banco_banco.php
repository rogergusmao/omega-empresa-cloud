<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       banco_banco
    * NOME DA CLASSE DAO: DAO_Banco_banco
    * DATA DE GERAÇÃO:    29.01.2013
    * ARQUIVO:            EXTDAO_Banco_banco.php
    * TABELA MYSQL:       banco_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/banco_banco.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Banco_banco();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getHomologacao_banco_id_INT())){

            $strCondicao[] = "homologacao_banco_id_INT={$obj->getHomologacao_banco_id_INT()}";
            $strGET[] = "homologacao_banco_id_INT={$obj->getHomologacao_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getProducao_banco_id_INT())){

            $strCondicao[] = "producao_banco_id_INT={$obj->getProducao_banco_id_INT()}";
            $strGET[] = "producao_banco_id_INT={$obj->getProducao_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_banco_id_INT())){

            $strCondicao[] = "tipo_banco_id_INT={$obj->getTipo_banco_id_INT()}";
            $strGET[] = "tipo_banco_id_INT={$obj->getTipo_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getProjetos_id_INT())){

            $strCondicao[] = "projetos_id_INT={$obj->getProjetos_id_INT()}";
            $strGET[] = "projetos_id_INT={$obj->getProjetos_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM banco_banco " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM banco_banco " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Bancos Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_homologacao_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_producao_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getHomologacao_banco_id_INT())){
                
                        $obj->getFkObjHomologacao_banco()->select($obj->getHomologacao_banco_id_INT());
                        $obj->getFkObjHomologacao_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjHomologacao_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProducao_banco_id_INT())){
                
                        $obj->getFkObjProducao_banco()->select($obj->getProducao_banco_id_INT());
                        $obj->getFkObjProducao_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProducao_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_banco_id_INT())){
                
                        $obj->getFkObjTipo_banco()->select($obj->getTipo_banco_id_INT());
                        $obj->getFkObjTipo_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_id_INT())){
                
                        $obj->getFkObjProjetos()->select($obj->getProjetos_id_INT());
                        $obj->getFkObjProjetos()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=banco_banco&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=banco_banco&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Banco_banco&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=banco_banco&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
