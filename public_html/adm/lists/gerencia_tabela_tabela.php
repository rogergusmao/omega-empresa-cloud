<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       tabela_tabela
    * NOME DA CLASSE DAO: DAO_Tabela_tabela
    * DATA DE GERAÇÃO:    29.01.2013
    * ARQUIVO:            EXTDAO_Tabela_tabela.php
    * TABELA MYSQL:       tabela_tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_gerencia_atributo_homologacao"] = "Clique aqui para gerenciar a equivalência dos atributos da tabela de homologação e produção";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";
    $acoes["tooltip_existe_atributo_sem_verificacao"] = "Atenção existe atributos ainda não verificados";
//
//    include("filters/gerencia_list_tabela_tabela.php");
//    
    
    
    
    
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
//    $objProjetoVersao = new EXTDAO_Projetos_versao();
//    $objProjetoVersao->inicializaEstruturaBanco(Seguranca::getIdDoProjetoVersaoSelecionado());
    


    $registrosPesquisa = 1;

    $obj = new EXTDAO_Tabela_tabela();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

         if(!Helper::isNull($obj->getHomologacao_tabela_id_INT())){

            $strCondicao[] = "homologacao_tabela_id_INT={$obj->getHomologacao_tabela_id_INT()}";
            $strGET[] = "homologacao_tabela_id_INT={$obj->getHomologacao_tabela_id_INT()}";

        }

         if(!Helper::isNull($obj->getProducao_tabela_id_INT())){

            $strCondicao[] = "producao_tabela_id_INT={$obj->getProducao_tabela_id_INT()}";
            $strGET[] = "producao_tabela_id_INT={$obj->getProducao_tabela_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_operacao_atualizacao_banco_id_INT())){

            $strCondicao[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";
            $strGET[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";

        }



        $strCondicao[] = "projetos_versao_banco_banco_id_INT=".$idPVBB;
        

         if(!Helper::isNull($obj->getStatus_verificacao_BOOLEAN())){

            $strCondicao[] = "status_verificacao_BOOLEAN={$obj->getStatus_verificacao_BOOLEAN()}";
            $strGET[] = "status_verificacao_BOOLEAN={$obj->getStatus_verificacao_BOOLEAN()}";

        }

        if(!Helper::isNull($obj->getInserir_tuplas_na_homologacao_BOOLEAN())){

            $strCondicao[] = "inserir_tuplas_na_homologacao_BOOLEAN={$obj->getInserir_tuplas_na_homologacao_BOOLEAN()}";
            $strGET[] = "inserir_tuplas_na_homologacao_BOOLEAN={$obj->getInserir_tuplas_na_homologacao_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getRemover_tuplas_na_homologacao_BOOLEAN())){

            $strCondicao[] = "remover_tuplas_na_homologacao_BOOLEAN={$obj->getRemover_tuplas_na_homologacao_BOOLEAN()}";
            $strGET[] = "remover_tuplas_na_homologacao_BOOLEAN={$obj->getRemover_tuplas_na_homologacao_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getEditar_tuplas_na_homologacao_BOOLEAN())){

            $strCondicao[] = "editar_tuplas_na_homologacao_BOOLEAN={$obj->getEditar_tuplas_na_homologacao_BOOLEAN()}";
            $strGET[] = "editar_tuplas_na_homologacao_BOOLEAN={$obj->getEditar_tuplas_na_homologacao_BOOLEAN()}";

        }

        
        
    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];
    }
    
    
    for($i=0; $i<count($strGET); $i++)
        $varGET .= "&" . $strGET[$i];
    
    
    $consultaNumero = "SELECT COUNT(id) FROM tabela_tabela " . $consulta;

    $objBanco = new Database();

    $consultaRegistros = "SELECT id FROM tabela_tabela " . $consulta . " ORDER BY tipo_operacao_atualizacao_banco_id_INT";
    
    $objBanco->query($consultaRegistros);
    
    
    $class = $obj->nomeClasse;
    $action = "salvaListaVerificada";
    $postar = "actions.php";
    ?>

    
    <?=$obj->getCabecalhoFormulario($postar); ?>

    <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    <input type="hidden" name="class" id="class" value="<?=$class; ?>">
    <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    <input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>gerencia_tabela_tabela">
    



   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Relacionamentos Entre Tabelas Do Banco De Homologação E Produção</legend>

   <table class="tabela_list">
   		<colgroup>
                        <col width="5%" />
			<col width="5%" />
			<col width="25%" />
			<col width="25%" />
			<col width="10%" />
			
                        <col width="20%" />
			<col width="8%" />
                        
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">
                        <td class="td_list_titulos">Foi verificada?</td>
			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_homologacao_tabela_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_producao_tabela_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao_atualizacao_banco_id_INT ?></td>
			
			<td class="td_list_titulos">Atributos no Estado</td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = "";
        $vChecked = "";
        if(strlen($obj->getStatus_verificacao_BOOLEAN())
                && $obj->getStatus_verificacao_BOOLEAN() == "1")
            $vChecked = "checked";
        
        if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
        }else if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
            $classTr = ($i%2)?"tr_list_conteudo_edit_impar":"tr_list_conteudo_edit_par";
        }else if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
            $classTr = ($i%2)?"tr_list_conteudo_delete_impar":"tr_list_conteudo_delete_par";
        }else if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION){
            $classTr = ($i%2)?"tr_list_conteudo_no_modification_impar":"tr_list_conteudo_no_modification_par";
        }else{
            $classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
        }
            

    ?>

    	<tr class="<?=$classTr ?>">
                <input type="hidden" name="id<?=$i ?>" id="id<?=$i ?>" value="<?=$obj->getId(); ?>">
                <td class="td_list_conteudo" style="text-align: center; padding-left: 5px;">
                    <input type="checkbox" <?=$vChecked;?> name="status_verificacao_BOOLEAN<?=$i;?>" id="status_verificacao_BOOLEAN<?=$i;?>" value="1">
                </td>
    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="index.php?tipo=forms&page=compara_tabela_hom_tabela_prod&<?=$varGET?>&id1=<?=$obj->getId()?>" target="_self" >
                           <?=$obj->getId() ?>
                   </a>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getHomologacao_tabela_id_INT())){
                
                        $obj->getFkObjHomologacao_tabela()->select($obj->getHomologacao_tabela_id_INT());
                        $obj->getFkObjHomologacao_tabela()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjHomologacao_tabela()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProducao_tabela_id_INT())){
                
                        $obj->getFkObjProducao_tabela()->select($obj->getProducao_tabela_id_INT());
                        $obj->getFkObjProducao_tabela()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProducao_tabela()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_operacao_atualizacao_banco_id_INT())){
                
                        $obj->getFkObjTipo_operacao_atualizacao_banco()->select($obj->getTipo_operacao_atualizacao_banco_id_INT());
                        $obj->getFkObjTipo_operacao_atualizacao_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_operacao_atualizacao_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>
    		

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                     
                     <?
                     $vetorEstadoAtributo = array(false, false, false,false);
                     if($obj->existeAtributoSemVerificacao()){
                         $vetorEstadoAtributo[3] = true;
                     }
                     
                     if(EXTDAO_Atributo_atributo::existeRegistroNoEstado(
                            EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT, 
                            $obj->getId())
                    )
                             $vetorEstadoAtributo[0] = true;
                       
                    
                    if(EXTDAO_Atributo_atributo::existeRegistroNoEstado(
                            EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT, 
                            $obj->getId())
                    )
                            $vetorEstadoAtributo[1] = true;
                        
                    
                    if(EXTDAO_Atributo_atributo::existeRegistroNoEstado(
                            EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE, 
                            $obj->getId())
                    )
                            $vetorEstadoAtributo[2] = true;
                    
                    $vValidade = false;
                    for($j = 0 ;$j < count($vetorEstadoAtributo); $j++){
                        if($vetorEstadoAtributo[$j]){
                            $vValidade = true;
                            break;
                        }
                    }
                    if($vValidade){
                        ?>
                            
                            
                            <?
                        for($j = 0 ;$j < count($vetorEstadoAtributo); $j++){
                            if(!$vetorEstadoAtributo[$j]) continue;
                            switch ($j) {
                                case 0:
                                    ?>
                                    <a style="color: #006400"> INSERIDO - </a>
                                    <? 

                                    break;
                                case 1:
                                    ?>
                                    <a style="color: purple">EDITADO - </a>
                                    <? 

                                    break;
                                case 2:
                                    ?>
                                    <a style="color:#FA2828">DELETADO - </a>
                                    <? 

                                    break;
                                case 3:
                                    ?>
                                    <a style="color: #B71616">NÃO VERIFICADO - </a>
                                    <? 

                                    break;
                                default:
                                    break;
                            }
                            
                        }    
                    }
                     ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
                                <?
                                
                                    ?>
                                        <img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=lists&page=gerencia_atributo_atributo&id1=<?=$obj->getId(); ?>&<?=Param_Get::TABELA_TABELA;?>=<?=$obj->getId(); ?>&<?=$varGET;?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_gerencia_atributo_homologacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                                    <?
                                
                                ?>
                                <?
                                if($obj->getTipo_operacao_atualizacao_banco_id_INT() != EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
                                    ?>
                                        <img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=gerencia_tabela_tabela&id1=<?=$obj->getId(); ?>&<?=$varGET;?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                                    <?
                                }
                                ?>
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>


	
