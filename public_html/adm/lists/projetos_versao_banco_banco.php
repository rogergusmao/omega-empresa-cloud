<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao_banco_banco
    * NOME DA CLASSE DAO: DAO_Projetos_versao_banco_banco
    * DATA DE GERAÇÃO:    19.06.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_banco_banco.php
    * TABELA MYSQL:       projetos_versao_banco_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/projetos_versao_banco_banco.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Projetos_versao_banco_banco();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getProjetos_versao_id_INT())){

            $strCondicao[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";
            $strGET[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";

        }

         if(!Helper::isNull($obj->getBanco_banco_id_INT())){

            $strCondicao[] = "banco_banco_id_INT={$obj->getBanco_banco_id_INT()}";
            $strGET[] = "banco_banco_id_INT={$obj->getBanco_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getCopia_do_projetos_versao_banco_banco_id_INT())){

            $strCondicao[] = "copia_do_projetos_versao_banco_banco_id_INT={$obj->getCopia_do_projetos_versao_banco_banco_id_INT()}";
            $strGET[] = "copia_do_projetos_versao_banco_banco_id_INT={$obj->getCopia_do_projetos_versao_banco_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_analise_projeto_id_INT())){

            $strCondicao[] = "tipo_analise_projeto_id_INT={$obj->getTipo_analise_projeto_id_INT()}";
            $strGET[] = "tipo_analise_projeto_id_INT={$obj->getTipo_analise_projeto_id_INT()}";

        }

         if(!Helper::isNull($obj->getSinc_do_projetos_versao_banco_banco_id_INT())){

            $strCondicao[] = "sinc_do_projetos_versao_banco_banco_id_INT={$obj->getSinc_do_projetos_versao_banco_banco_id_INT()}";
            $strGET[] = "sinc_do_projetos_versao_banco_banco_id_INT={$obj->getSinc_do_projetos_versao_banco_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_plataforma_operacional_id_INT())){

            $strCondicao[] = "tipo_plataforma_operacional_id_INT={$obj->getTipo_plataforma_operacional_id_INT()}";
            $strGET[] = "tipo_plataforma_operacional_id_INT={$obj->getTipo_plataforma_operacional_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM projetos_versao_banco_banco " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM projetos_versao_banco_banco " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Bancos Da Versão Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
			<col width="11%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_banco_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_copia_do_projetos_versao_banco_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_analise_projeto_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sinc_do_projetos_versao_banco_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_plataforma_operacional_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_versao_id_INT())){
                
                        $obj->getFkObjProjetos_versao()->select($obj->getProjetos_versao_id_INT());
                        $obj->getFkObjProjetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getBanco_banco_id_INT())){
                
                        $obj->getFkObjBanco_banco()->select($obj->getBanco_banco_id_INT());
                        $obj->getFkObjBanco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjBanco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getCopia_do_projetos_versao_banco_banco_id_INT())){
                
                        $obj->getFkObjCopia_do_projetos_versao_banco_banco()->select($obj->getCopia_do_projetos_versao_banco_banco_id_INT());
                        $obj->getFkObjCopia_do_projetos_versao_banco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjCopia_do_projetos_versao_banco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_analise_projeto_id_INT())){
                
                        $obj->getFkObjTipo_analise_projeto()->select($obj->getTipo_analise_projeto_id_INT());
                        $obj->getFkObjTipo_analise_projeto()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_analise_projeto()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSinc_do_projetos_versao_banco_banco_id_INT())){
                
                        $obj->getFkObjSinc_do_projetos_versao_banco_banco()->select($obj->getSinc_do_projetos_versao_banco_banco_id_INT());
                        $obj->getFkObjSinc_do_projetos_versao_banco_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSinc_do_projetos_versao_banco_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_plataforma_operacional_id_INT())){
                
                        $obj->getFkObjTipo_plataforma_operacional()->select($obj->getTipo_plataforma_operacional_id_INT());
                        $obj->getFkObjTipo_plataforma_operacional()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_plataforma_operacional()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=projetos_versao_banco_banco&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=projetos_versao_banco_banco&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Projetos_versao_banco_banco&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=projetos_versao_banco_banco&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
