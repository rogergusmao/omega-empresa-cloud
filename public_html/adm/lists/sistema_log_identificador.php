<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       sistema_log_identificador
    * NOME DA CLASSE DAO: DAO_Sistema_log_identificador
    * DATA DE GERAÇÃO:    26.05.2013
    * ARQUIVO:            EXTDAO_Sistema_log_identificador.php
    * TABELA MYSQL:       sistema_log_identificador
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/sistema_log_identificador.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Sistema_log_identificador();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getIdentificador_erro())){

            $strCondicao[] = "identificador_erro LIKE '%{$obj->getIdentificador_erro()}%'";
            $strGET[] = "identificador_erro={$obj->getIdentificador_erro()}";

        }

         if(!Helper::isNull($obj->getDescricao())){

            $strCondicao[] = "descricao LIKE '%{$obj->getDescricao()}%'";
            $strGET[] = "descricao={$obj->getDescricao()}";

        }

         if(!Helper::isNull($obj->getStacktrace())){

            $strCondicao[] = "stacktrace LIKE '%{$obj->getStacktrace()}%'";
            $strGET[] = "stacktrace={$obj->getStacktrace()}";

        }

         if(!Helper::isNull($obj->getSistema_projetos_versao_produto_id_INT())){

            $strCondicao[] = "sistema_projetos_versao_produto_id_INT={$obj->getSistema_projetos_versao_produto_id_INT()}";
            $strGET[] = "sistema_projetos_versao_produto_id_INT={$obj->getSistema_projetos_versao_produto_id_INT()}";

        }

         if(!Helper::isNull($obj->getCorrigida_BOOLEAN())){

            $strCondicao[] = "corrigida_BOOLEAN={$obj->getCorrigida_BOOLEAN()}";
            $strGET[] = "corrigida_BOOLEAN={$obj->getCorrigida_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getData_correcao_DATETIME())){

            $strCondicao[] = "data_correcao_DATETIME={$obj->getData_correcao_DATETIME()}";
            $strGET[] = "data_correcao_DATETIME={$obj->getData_correcao_DATETIME()}";

        }

         if(!Helper::isNull($obj->getCorrigida_pelo_usuario_id_INT())){

            $strCondicao[] = "corrigida_pelo_usuario_id_INT={$obj->getCorrigida_pelo_usuario_id_INT()}";
            $strGET[] = "corrigida_pelo_usuario_id_INT={$obj->getCorrigida_pelo_usuario_id_INT()}";

        }

         if(!Helper::isNull($obj->getSistema_projetos_versao_id_INT())){

            $strCondicao[] = "sistema_projetos_versao_id_INT={$obj->getSistema_projetos_versao_id_INT()}";
            $strGET[] = "sistema_projetos_versao_id_INT={$obj->getSistema_projetos_versao_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM sistema_log_identificador " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM sistema_log_identificador " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Identificadores Dos Logs</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_identificador_erro ?></td>
			<td class="td_list_titulos"><?=$obj->label_descricao ?></td>
			<td class="td_list_titulos"><?=$obj->label_stacktrace ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_projetos_versao_produto_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_corrigida_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_correcao_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_corrigida_pelo_usuario_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getIdentificador_erro() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getDescricao() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getStacktrace() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_projetos_versao_produto_id_INT())){
                
                        $obj->getFkObjSistema_projetos_versao_produto()->select($obj->getSistema_projetos_versao_produto_id_INT());
                        $obj->getFkObjSistema_projetos_versao_produto()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_projetos_versao_produto()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getCorrigida_BOOLEAN()?"Sim":"Não" ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_correcao_DATETIME() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getCorrigida_pelo_usuario_id_INT())){
                
                        $obj->getFkObjCorrigida_pelo_usuario()->select($obj->getCorrigida_pelo_usuario_id_INT());
                        $obj->getFkObjCorrigida_pelo_usuario()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjCorrigida_pelo_usuario()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_projetos_versao_id_INT())){
                
                        $obj->getFkObjSistema_projetos_versao()->select($obj->getSistema_projetos_versao_id_INT());
                        $obj->getFkObjSistema_projetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjSistema_projetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_log_identificador&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_log_identificador&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Sistema_log_identificador&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=sistema_log_identificador&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
