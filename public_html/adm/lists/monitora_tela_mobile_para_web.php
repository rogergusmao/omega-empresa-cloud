<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       monitora_tela_mobile_para_web
    * NOME DA CLASSE DAO: DAO_Monitora_tela_mobile_para_web
    * DATA DE GERAÇÃO:    26.10.2013
    * ARQUIVO:            EXTDAO_Monitora_tela_mobile_para_web.php
    * TABELA MYSQL:       monitora_tela_mobile_para_web
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/monitora_tela_mobile_para_web.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Monitora_tela_mobile_para_web();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getOperacao_sistema_mobile_id_INT())){

            $strCondicao[] = "operacao_sistema_mobile_id_INT={$obj->getOperacao_sistema_mobile_id_INT()}";
            $strGET[] = "operacao_sistema_mobile_id_INT={$obj->getOperacao_sistema_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getMobile_identificador_id_INT())){

            $strCondicao[] = "mobile_identificador_id_INT={$obj->getMobile_identificador_id_INT()}";
            $strGET[] = "mobile_identificador_id_INT={$obj->getMobile_identificador_id_INT()}";

        }

         if(!Helper::isNull($obj->getSequencia_operacao_INT())){

            $strCondicao[] = "sequencia_operacao_INT={$obj->getSequencia_operacao_INT()}";
            $strGET[] = "sequencia_operacao_INT={$obj->getSequencia_operacao_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_operacao_monitora_tela_id_INT())){

            $strCondicao[] = "tipo_operacao_monitora_tela_id_INT={$obj->getTipo_operacao_monitora_tela_id_INT()}";
            $strGET[] = "tipo_operacao_monitora_tela_id_INT={$obj->getTipo_operacao_monitora_tela_id_INT()}";

        }

         if(!Helper::isNull($obj->getMonitora_tela_web_para_mobile_id_INT())){

            $strCondicao[] = "monitora_tela_web_para_mobile_id_INT={$obj->getMonitora_tela_web_para_mobile_id_INT()}";
            $strGET[] = "monitora_tela_web_para_mobile_id_INT={$obj->getMonitora_tela_web_para_mobile_id_INT()}";

        }

         if(!Helper::isNull($obj->getData_ocorrencia_DATETIME())){

            $strCondicao[] = "data_ocorrencia_DATETIME={$obj->getData_ocorrencia_DATETIME()}";
            $strGET[] = "data_ocorrencia_DATETIME={$obj->getData_ocorrencia_DATETIME()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM monitora_tela_mobile_para_web " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM monitora_tela_mobile_para_web " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Operações De Monitoramento Mobile Para Web</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
			<col width="13%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_operacao_sistema_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_mobile_identificador_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_sequencia_operacao_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao_monitora_tela_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_monitora_tela_web_para_mobile_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_ocorreu_erro_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_ocorrencia_DATETIME ?></td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getOperacao_sistema_mobile_id_INT())){
                
                        $obj->getFkObjOperacao_sistema_mobile()->select($obj->getOperacao_sistema_mobile_id_INT());
                        $obj->getFkObjOperacao_sistema_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjOperacao_sistema_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getMobile_identificador_id_INT())){
                
                        $obj->getFkObjMobile_identificador()->select($obj->getMobile_identificador_id_INT());
                        $obj->getFkObjMobile_identificador()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjMobile_identificador()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getSequencia_operacao_INT() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_operacao_monitora_tela_id_INT())){
                
                        $obj->getFkObjTipo_operacao_monitora_tela()->select($obj->getTipo_operacao_monitora_tela_id_INT());
                        $obj->getFkObjTipo_operacao_monitora_tela()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_operacao_monitora_tela()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getMonitora_tela_web_para_mobile_id_INT())){
                
                        $obj->getFkObjMonitora_tela_web_para_mobile()->select($obj->getMonitora_tela_web_para_mobile_id_INT());
                        $obj->getFkObjMonitora_tela_web_para_mobile()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjMonitora_tela_web_para_mobile()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getOcorreu_erro_BOOLEAN()?"Sim":"Não" ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_ocorrencia_DATETIME() ?>
    		</td>



    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=monitora_tela_mobile_para_web&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
