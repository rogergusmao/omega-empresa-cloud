<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       atributo_producao
    * NOME DA CLASSE DAO: DAO_Atributo_producao
    * DATA DE GERAÇÃO:    09.01.2013
    * ARQUIVO:            EXTDAO_Atributo_producao.php
    * TABELA MYSQL:       atributo_producao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/atributo_producao.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Atributo_producao();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getNome())){

            $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
            $strGET[] = "nome={$obj->getNome()}";

        }

         if(!Helper::isNull($obj->getTabela_producao_id_INT())){

            $strCondicao[] = "tabela_producao_id_INT={$obj->getTabela_producao_id_INT()}";
            $strGET[] = "tabela_producao_id_INT={$obj->getTabela_producao_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_sql())){

            $strCondicao[] = "tipo_sql LIKE '%{$obj->getTipo_sql()}%'";
            $strGET[] = "tipo_sql={$obj->getTipo_sql()}";

        }

         if(!Helper::isNull($obj->getFk_tabela_producao_id_INT())){

            $strCondicao[] = "fk_tabela_producao_id_INT={$obj->getFk_tabela_producao_id_INT()}";
            $strGET[] = "fk_tabela_producao_id_INT={$obj->getFk_tabela_producao_id_INT()}";

        }

         if(!Helper::isNull($obj->getUnique_BOOLEAN())){

            $strCondicao[] = "unique_BOOLEAN={$obj->getUnique_BOOLEAN()}";
            $strGET[] = "unique_BOOLEAN={$obj->getUnique_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getBanco_id_INT())){

            $strCondicao[] = "banco_id_INT={$obj->getBanco_id_INT()}";
            $strGET[] = "banco_id_INT={$obj->getBanco_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM atributo_producao " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM atributo_producao " . $consulta . " ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Atributos Do Banco De Produção</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
			<col width="10%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_sql ?></td>
			<td class="td_list_titulos"><?=$obj->label_tamanho_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_not_null_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_fk_tabela_producao_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_fk_on_update ?></td>
			<td class="td_list_titulos"><?=$obj->label_fk_on_delete ?></td>
			<td class="td_list_titulos"><?=$obj->label_banco_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTipo_sql() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getTamanho_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNot_null_BOOLEAN()?"Sim":"Não" ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getFk_tabela_producao_id_INT())){
                
                        $obj->getFkObjFk_tabela_producao()->select($obj->getFk_tabela_producao_id_INT());
                        $obj->getFkObjFk_tabela_producao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjFk_tabela_producao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getFk_on_update() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getFk_on_delete() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getBanco_id_INT())){
                
                        $obj->getFkObjBanco()->select($obj->getBanco_id_INT());
                        $obj->getFkObjBanco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjBanco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=atributo_producao&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=atributo_producao&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Atributo_producao&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=atributo_producao&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
