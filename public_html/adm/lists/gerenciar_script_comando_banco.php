<?php

/*
*
* -------------------------------------------------------
* NOME DA LIST:       script_comando_banco
* NOME DA CLASSE DAO: DAO_Script_comando_banco
* DATA DE GERAÇÃO:    10.04.2013
* ARQUIVO:            EXTDAO_Script_comando_banco.php
* TABELA MYSQL:       script_comando_banco
* BANCO DE DADOS:     biblioteca_nuvem
* -------------------------------------------------------
*
*/
$idPVBB = Helper::GET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
$acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
$acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
$acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";



$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Script_comando_banco();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();
$classTr="";
$varGET = "";
$strCondicao = array();
$strGET = array();

$consulta = "";

if(!Helper::isNull($obj->getConsulta())){

    $strCondicao[] = "scb.consulta LIKE '%{$obj->getConsulta()}%'";
    $strGET[] = "consulta={$obj->getConsulta()}";

}

if(!Helper::isNull($obj->getSeq_INT())){

    $strCondicao[] = "scb.seq_INT={$obj->getSeq_INT()}";
    $strGET[] = "seq_INT={$obj->getSeq_INT()}";

}

if(!Helper::isNull($obj->getScript_tabela_tabela_id_INT())){

    $strCondicao[] = "scb.script_tabela_tabela_id_INT={$obj->getScript_tabela_tabela_id_INT()}";
    $strGET[] = "script_tabela_tabela_id_INT={$obj->getScript_tabela_tabela_id_INT()}";

}

if(!Helper::isNull($obj->getTipo_comando_banco_id_INT())){

    $strCondicao[] = "scb.tipo_comando_banco_id_INT={$obj->getTipo_comando_banco_id_INT()}";
    $strGET[] = "tipo_comando_banco_id_INT={$obj->getTipo_comando_banco_id_INT()}";

}

if(!Helper::isNull($obj->getAtributo_atributo_id_INT())){

    $strCondicao[] = "scb.atributo_atributo_id_INT={$obj->getAtributo_atributo_id_INT()}";
    $strGET[] = "atributo_atributo_id_INT={$obj->getAtributo_atributo_id_INT()}";

}
$idTipoScriptBanco =null;
if(!Helper::isNull(Helper::POSTGET("id_processo_estrutura"))){
    $idPE = Helper::POSTGET("id_processo_estrutura");
    $objPE = new EXTDAO_Processo_estrutura();
    $objPE->select($idPE);
    $json = $objPE->getJson_constante();
    if(strlen($json)){
        $objParametro = json_decode($json);
        $idTipoScriptBanco = $objParametro->idTipoScriptBanco;
    }
}
if($idTipoScriptBanco == null)
    $idTipoScriptBanco = Helper::POSTGET("id_tipo_script_banco");

if(!Helper::isNull($idTipoScriptBanco)){
    $idPE = Helper::POSTGET("id_tipo_script_banco");
    if(strlen($consulta)) $consulta .= " AND ";

    $strGET[] = "id_tipo_script_banco={$idTipoScriptBanco}";
    $strCondicao[] = "spvbb.tipo_script_banco_id_INT={$idTipoScriptBanco}";
}

$strGET[] = "projetos_versao_banco_banco_id_INT={$idPVBB}";
$strCondicao[] = "spvbb.projetos_versao_banco_banco_id_INT={$idPVBB}";

for($i=0; $i<count($strCondicao); $i++){
    if(strlen($consulta))
        $consulta .= " AND ";
    $consulta .=  $strCondicao[$i];
    $varGET .= "&" . $strGET[$i];
}


$objBanco = new Database();
if(strlen($consulta))
    $consulta = " WHERE $consulta ";
$consultaRegistros = "SELECT scb.id 
        FROM script_comando_banco scb
            JOIN script_tabela_tabela stt
                ON scb.script_tabela_tabela_id_INT = stt.id
            JOIN script_projetos_versao_banco_banco spvbb
                ON stt.script_projetos_versao_banco_banco_id_INT = spvbb.id
                
        " . $consulta
    . " 
        ORDER BY scb.script_tabela_tabela_id_INT, scb.seq_INT";

$objBanco->query($consultaRegistros);

for($i=1; $regs = $objBanco->fetchArray(); $i++){

    $obj->select($regs[0]);
    $obj->formatarParaExibicao();

    echo $obj->getConsulta();
    echo "<br/>";
    echo "<br/>";
 }
