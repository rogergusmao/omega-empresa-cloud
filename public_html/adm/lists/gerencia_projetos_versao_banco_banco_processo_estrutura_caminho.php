<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao_banco_banco_processo_estrutura_caminho
    * NOME DA CLASSE DAO: DAO_Projetos_versao_banco_banco_processo_estrutura_caminho
    * DATA DE GERAÇÃO:    22.04.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho.php
    * TABELA MYSQL:       projetos_versao_banco_banco_processo_estrutura_caminho
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();
    
    //Parametros de entrada
    //$idPVBBPE
    
    $objPVBBPE = new EXTDAO_Projetos_versao_banco_banco_processo_estrutura();
    $objPVBBPE->select($idPVBBPE);
    $vIndicePartido = $objPVBBPE->getAtual_processo_estrutura_caminho_id_INT();
    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT())){

            $strCondicao[] = "projetos_versao_banco_banco_processo_estrutura_id_INT={$obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT()}";
            $strGET[] = "projetos_versao_banco_banco_processo_estrutura_id_INT={$obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT()}";

        }

         if(!Helper::isNull($obj->getProcesso_estrutura_caminho_id_INT())){

            $strCondicao[] = "processo_estrutura_caminho_id_INT={$obj->getProcesso_estrutura_caminho_id_INT()}";
            $strGET[] = "processo_estrutura_caminho_id_INT={$obj->getProcesso_estrutura_caminho_id_INT()}";

        }

         if(!Helper::isNull($obj->getData_inicio_DATETIME())){

            $strCondicao[] = "data_inicio_DATETIME={$obj->getData_inicio_DATETIME()}";
            $strGET[] = "data_inicio_DATETIME={$obj->getData_inicio_DATETIME()}";

        }

         if(!Helper::isNull($obj->getData_atualizacao_DATETIME())){

            $strCondicao[] = "data_atualizacao_DATETIME={$obj->getData_atualizacao_DATETIME()}";
            $strGET[] = "data_atualizacao_DATETIME={$obj->getData_atualizacao_DATETIME()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM projetos_versao_banco_banco_processo_estrutura_caminho " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM projetos_versao_banco_banco_processo_estrutura_caminho " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Etapas Atuais Do Processo</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="5%" />
			
			
			<col width="20%" />
			<col width="10%" />
			<col width="10%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">
                        <td class="td_list_titulos">Status</td>
			
			
			<td class="td_list_titulos"><?=$obj->label_processo_estrutura_caminho_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_inicio_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_atualizacao_DATETIME ?></td>
			<td class="td_list_titulos">Avisos</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();
        $vStatus = $obj->getStatus();
    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">
            
            <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <?
                    
                    $vImagem = "";
                    switch ($vStatus) {
                        case EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho::NAO_INICIALIZADO:
                            $vImagem = "processo";
                            break;
                        case EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho::EXECUTADO:
                            $vImagem = "processo_verde";
                            break;
                        case EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho::DESATUALIZADO:
                            $vImagem = "processo_vermelho";
                            break;
                        case EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho::FINALIZADO:
                            $vImagem = "processo_verde";
                            break;
                        default:
                            break;
                    }
                    ?>
                    <img border="0" src="imgs/<?=$vImagem;?>.png">
                </td>
    		


                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProcesso_estrutura_caminho_id_INT())){
                
                        $obj->getFkObjProcesso_estrutura_caminho()->select($obj->getProcesso_estrutura_caminho_id_INT());
                        
                    ?>
                        <?=$obj->getFkObjProcesso_estrutura_caminho()->getDescricao();?>
                        

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_inicio_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_atualizacao_DATETIME() ?>
    		</td>
                       
			<td class="td_list_conteudo" style="text-align: center;">
                            
                        <?
                        if ($vIndicePartido == $obj->getId()  ) {
                        ?>    
                        
                            
                            <a href="index.php?tipo=pages&page=executa_processo&pvbb_processo_estrutura_caminho=<?=$obj->getId()?>" target="_self" >
                                    Continuar ciclo
                            </a>
                        <?
                        } else if((!strlen($vIndicePartido) && $i == 1)){
                        ?>      
                            <a href="index.php?tipo=pages&page=executa_processo&pvbb_processo_estrutura_caminho=<?=$obj->getId()?>" target="_self" >
                                    Iniciar ciclo
                            </a>
                            <?
                        }
                        ?>
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=projetos_versao_banco_banco_processo_estrutura_caminho&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
