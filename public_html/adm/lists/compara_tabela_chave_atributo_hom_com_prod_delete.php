<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       tabela_chave_atributo
 * NOME DA CLASSE DAO: DAO_Tabela_chave_atributo
 * DATA DE GERAÇÃO:    06.04.2013
 * ARQUIVO:            EXTDAO_Tabela_chave_atributo.php
 * TABELA MYSQL:       tabela_chave_atributo
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
$acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
$acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
$acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

$registrosPesquisa = 1;

$obj = new EXTDAO_Tabela_chave_atributo();


$strCondicao = array();
$strGET = array();



$strCondicao[] = "tabela_chave_id_INT={$pIdTabelaChave}";
$strGET[] = "tabela_chave_id_INT={$pIdTabelaChave}";



$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++) {

    if ($i == 0)
        $consulta .= "WHERE " . $strCondicao[$i];
    else
        $consulta .= " AND " . $strCondicao[$i];
}

$consultaNumero = "SELECT COUNT(id) FROM tabela_chave_atributo " . $consulta;

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$consultaRegistros = "SELECT id FROM tabela_chave_atributo " . $consulta . " ORDER BY id";

$objBanco->query($consultaRegistros);
$objTCATCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();
$objArg->isDisabled = true;
$objArg->disabled = true;
?>



<fieldset class="fieldset_list">

    <table class="tabela_list">
        <colgroup>
            <col width="31%" />
            <col width="31%" />
            <col width="31%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">

                <td class="td_list_titulos"><?= $objTCATCA->label_homologacao_tabela_chave_atributo_id_INT ?></td>
                <td class="td_list_titulos"><?= $objTCATCA->label_producao_tabela_chave_atributo_id_INT ?></td>

                <td class="td_list_titulos">Avisos</td>

            </tr>
        </thead>
        <tbody>

            <?
            for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

                $obj->select($regs[0]);
                $obj->formatarParaExibicao();

                $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par"
                ?>

                <tr class="<?= $classTr ?>">
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                    </td>
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getAtributo_id_INT())) {

                            $obj->getFkObjAtributo()->select($obj->getAtributo_id_INT());
                            $obj->getFkObjAtributo()->formatarParaExibicao();
                            ?>

                            <?= $obj->getFkObjAtributo()->valorCampoLabel() ?>

                        <? } ?>

                    </td>



                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?
                        
                        $vIdAA = EXTDAO_Atributo_atributo::getIdAtributoAtributo($idPVBB, null, $obj->getAtributo_id_INT());
                        if(strlen($vIdAA)){
                            ?>
                            <a href="index.php?tipo=forms&page=compara_atributo_hom_atributo_prod&<?= $varGET ?>&id1=<?= $vIdAA ?>" target="_self" >
                                Relacionamento entre atributo de homologação com produção
                            </a>
                            <?
                        }
                        
                        ?>
                    </td>



                </tr>

            <? } ?>

        </tbody>
    </table>

</fieldset>

<br/>
<br/>

<?
