<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       atributo_atributo
    * NOME DA CLASSE DAO: DAO_Atributo_atributo
    * DATA DE GERA��O:    30.01.2013
    * ARQUIVO:            EXTDAO_Atributo_atributo.php
    * TABELA MYSQL:       atributo_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $idTT = Helper::POSTGET(Param_Get::TABELA_TABELA);
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    $varGET .= "&".Param_Get::TABELA_TABELA."=".$idTT;
    
    
    
    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Atributo_atributo();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();
    
    $objTabelaTabela = new EXTDAO_Tabela_tabela();
    $objTabelaTabela->select($idTT);
    $objTabelaHomologacao = $objTabelaTabela->getObjTabelaHomologacao();
    if($objTabelaHomologacao == null)
        $objTabelaHomologacao = new EXTDAO_Tabela();
    $objTabelaProducao = $objTabelaTabela->getObjTabelaProducao();
    if($objTabelaProducao== null)
        $objTabelaProducao= new EXTDAO_Tabela();
    $strCondicao = array();
    $strGET = array();
    
        
    if(strlen(Helper::GET(Param_Get::ID_PRODUCAO_TABELA_ID_INT))){
        $varGET .= "&" . Param_Get::ID_PRODUCAO_TABELA_ID_INT."=".Helper::GET(Param_Get::ID_PRODUCAO_TABELA_ID_INT);
    }

     if(!Helper::isNull($obj->getHomologacao_atributo_id_INT())){

        $strCondicao[] = "homologacao_atributo_id_INT={$obj->getHomologacao_atributo_id_INT()}";
        $strGET[] = "homologacao_atributo_id_INT={$obj->getHomologacao_atributo_id_INT()}";

    }

     if(!Helper::isNull($obj->getProducao_atributo_id_INT())){

        $strCondicao[] = "producao_atributo_id_INT={$obj->getProducao_atributo_id_INT()}";
        $strGET[] = "producao_atributo_id_INT={$obj->getProducao_atributo_id_INT()}";

    }


     if(!Helper::isNull($obj->getTipo_operacao_atualizacao_banco_id_INT())){

        $strCondicao[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";
        $strGET[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";

    }

     if(!Helper::isNull($obj->getStatus_verificacao_BOOLEAN())){

        $strCondicao[] = "status_verificacao_BOOLEAN={$obj->getStatus_verificacao_BOOLEAN()}";
        $strGET[] = "status_verificacao_BOOLEAN={$obj->getStatus_verificacao_BOOLEAN()}";

    }

     if(!Helper::isNull($obj->getProjetos_versao_id_INT())){

        $strCondicao[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";
        $strGET[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";

    }

    $strCondicao[] = "tabela_tabela_id_INT={$idTT}";
    
    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE aa." . $strCondicao[$i];
        else
            $consulta .= " AND aa." . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];
    }
    
    $consultaFrom = " FROM atributo_atributo aa ";
    

    $objBanco = new Database();

    $consultaRegistros = "SELECT  DISTINCT aa.id ". $consultaFrom . $consulta . " ORDER BY aa.tipo_operacao_atualizacao_banco_id_INT ";
    
    
    
    $class = $obj->nomeClasse;
    $action = "salvaListaVerificada";
    $postar = "actions.php";
    ?>
    
    <?=$obj->getCabecalhoFormulario($postar); ?>

    <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    <input type="hidden" name="class" id="class" value="<?=$class; ?>">
    <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    <input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>gerencia_tabela_tabela">
    
    <input type="hidden" name="<?=  Param_Get::TABELA_TABELA;?>" id="<?=  Param_Get::TABELA_TABELA;?>" value="<?=$idTT;?>">
    <?=  EXTDAO_Projetos_versao::imprimiCabecalhoIdentificador();?>
    
    <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Relacionamentos Entre As Tabelas De Homologa��o e Produ��o</legend>

   <table class="tabela_list">
   		<colgroup>
                        <col width="5%" />
                        <col width="45%" />
			<col width="45%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">
                        <td class="td_list_titulos">Id</td>
                        <td class="td_list_titulos">Homologa��o</td>
			<td class="td_list_titulos">Produ��o</td>
		</tr>
		</thead>
                
    	<tbody>
            <tr>
                <td><?=$idTT;?></td>
                <td><?
                if(strlen($objTabelaHomologacao->getId()))
                    echo $objTabelaHomologacao->getId() ." - ". $objTabelaHomologacao->getNome();
                    
                    ?></td>
                <td><?
                    if(strlen($objTabelaProducao->getId()))
                    echo $objTabelaProducao->getId() ." - ".$objTabelaProducao->getNome();
                    
                    ?></td>
                </tr>
        </tbody>
   </table>
    </fieldset>
    
   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Relacionamentos Entre Atributos Do Banco De Homologa��o E Produ��o</legend>

   <table class="tabela_list">
   		<colgroup>
                        <col width="5%" />
			<col width="5%" />
			<col width="30%" />
			<col width="30%" />
			<col width="10%" />
			<col width="15%" />
			<col width="5%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">
                        <td class="td_list_titulos">Foi verificada?</td>
			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_homologacao_atributo_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_producao_atributo_id_INT ?></td>
			
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao_atualizacao_banco_id_INT ?></td>
			
			<td class="td_list_titulos">Avisos</td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>
            
    <? 
    $objAtributo = new EXTDAO_Atributo();
    $objTipoOperacaoBanco = new EXTDAO_Tipo_operacao_atualizacao_banco();
    $objBanco->query($consultaRegistros);
    for($i=1; $regs = $objBanco->fetchArray(); $i++){
        
    	$obj->select($regs[0]);
       
        $obj->formatarParaExibicao();

    	$classTr = "";
        $vChecked = "";
        
        if(strlen($obj->getStatus_verificacao_BOOLEAN())
                && $obj->getStatus_verificacao_BOOLEAN() == "1")
            $vChecked = "checked";
        
        
        
        if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT){
            $classTr = ($i%2)?"tr_list_conteudo_insert_impar":"tr_list_conteudo_insert_par";
        }else if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
            $classTr = ($i%2)?"tr_list_conteudo_edit_impar":"tr_list_conteudo_edit_par";
        }else if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE){
            $classTr = ($i%2)?"tr_list_conteudo_delete_impar":"tr_list_conteudo_delete_par";
        }else if($obj->getTipo_operacao_atualizacao_banco_id_INT() == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION){
            $classTr = ($i%2)?"tr_list_conteudo_no_modification_impar":"tr_list_conteudo_no_modification_par";
        }else{
            $classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
        }
            
    ?>

    	<tr class="<?=$classTr ?>">
            <input type="hidden" name="id<?=$i ?>" id="id<?=$i ?>" value="<?=$obj->getId(); ?>">
                <td class="td_list_conteudo" style="text-align: center; padding-left: 5px;">
                    <input type="checkbox" <?=$vChecked;?> name="status_verificacao_BOOLEAN<?=$i;?>" id="status_verificacao_BOOLEAN<?=$i;?>" value="1">
                </td>
    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="index.php?tipo=forms&page=compara_atributo_hom_atributo_prod&<?=$varGET?>&id1=<?=$obj->getId()?>" target="_self" >
                            <?=$obj->getId() ?>
                    </a>
    			
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getHomologacao_atributo_id_INT())){
                
                        $objAtributo->select($obj->getHomologacao_atributo_id_INT());
                        $objAtributo->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$objAtributo->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProducao_atributo_id_INT())){
                
                        $objAtributo->select($obj->getProducao_atributo_id_INT());
                        $objAtributo->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$objAtributo->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>


                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_operacao_atualizacao_banco_id_INT())){
                
                        $objTipoOperacaoBanco->select($obj->getTipo_operacao_atualizacao_banco_id_INT());
                        $objTipoOperacaoBanco->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$objTipoOperacaoBanco->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
                             <?
                                if(strlen($obj->getHomologacao_atributo_id_INT())){
                                    ?>
                                    <img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=gerencia_atributo_atributo&id1=<?=$obj->getId(); ?><?=$varGET ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                                    <?
                                }
                                ?>
				
			</td>


    
		</tr>

    <? } ?>

    </tbody>
     <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(false, true, false, "Marcar como verificada"); ?>

        	</td>
        </tr>
    </table>

    </fieldset>

    <br/>
    <br/>
	