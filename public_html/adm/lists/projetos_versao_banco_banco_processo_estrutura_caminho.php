<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao_banco_banco_processo_estrutura_caminho
    * NOME DA CLASSE DAO: DAO_Projetos_versao_banco_banco_processo_estrutura_caminho
    * DATA DE GERAÇÃO:    23.04.2013
    * ARQUIVO:            EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho.php
    * TABELA MYSQL:       projetos_versao_banco_banco_processo_estrutura_caminho
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/projetos_versao_banco_banco_processo_estrutura_caminho.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT())){

            $strCondicao[] = "projetos_versao_banco_banco_processo_estrutura_id_INT={$obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT()}";
            $strGET[] = "projetos_versao_banco_banco_processo_estrutura_id_INT={$obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT()}";

        }

         if(!Helper::isNull($obj->getProcesso_estrutura_caminho_id_INT())){

            $strCondicao[] = "processo_estrutura_caminho_id_INT={$obj->getProcesso_estrutura_caminho_id_INT()}";
            $strGET[] = "processo_estrutura_caminho_id_INT={$obj->getProcesso_estrutura_caminho_id_INT()}";

        }

         if(!Helper::isNull($obj->getData_inicio_DATETIME())){

            $strCondicao[] = "data_inicio_DATETIME={$obj->getData_inicio_DATETIME()}";
            $strGET[] = "data_inicio_DATETIME={$obj->getData_inicio_DATETIME()}";

        }

         if(!Helper::isNull($obj->getData_fim_DATETIME())){

            $strCondicao[] = "data_fim_DATETIME={$obj->getData_fim_DATETIME()}";
            $strGET[] = "data_fim_DATETIME={$obj->getData_fim_DATETIME()}";

        }

         if(!Helper::isNull($obj->getData_atualizacao_DATETIME())){

            $strCondicao[] = "data_atualizacao_DATETIME={$obj->getData_atualizacao_DATETIME()}";
            $strGET[] = "data_atualizacao_DATETIME={$obj->getData_atualizacao_DATETIME()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM projetos_versao_banco_banco_processo_estrutura_caminho " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM projetos_versao_banco_banco_processo_estrutura_caminho " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Etapas Atuais Do Processo</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_versao_banco_banco_processo_estrutura_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_processo_estrutura_caminho_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_inicio_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_fim_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_atualizacao_DATETIME ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT())){
                
                        $obj->getFkObjProjetos_versao_banco_banco_processo_estrutura()->select($obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT());
                        $obj->getFkObjProjetos_versao_banco_banco_processo_estrutura()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos_versao_banco_banco_processo_estrutura()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProcesso_estrutura_caminho_id_INT())){
                
                        $obj->getFkObjProcesso_estrutura_caminho()->select($obj->getProcesso_estrutura_caminho_id_INT());
                        $obj->getFkObjProcesso_estrutura_caminho()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProcesso_estrutura_caminho()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_inicio_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_fim_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_atualizacao_DATETIME() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=projetos_versao_banco_banco_processo_estrutura_caminho&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=projetos_versao_banco_banco_processo_estrutura_caminho&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=projetos_versao_banco_banco_processo_estrutura_caminho&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
