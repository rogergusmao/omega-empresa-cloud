<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       processo_estrutura_tipo_banco
    * NOME DA CLASSE DAO: DAO_Processo_estrutura_tipo_banco
    * DATA DE GERAÇÃO:    11.06.2013
    * ARQUIVO:            EXTDAO_Processo_estrutura_tipo_banco.php
    * TABELA MYSQL:       processo_estrutura_tipo_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/processo_estrutura_tipo_banco.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Processo_estrutura_tipo_banco();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getId())){

            $strCondicao[] = "id LIKE '%{$obj->getId()}%'";
            $strGET[] = "id={$obj->getId()}";

        }

         if(!Helper::isNull($obj->getProcesso_estrutura_id_INT())){

            $strCondicao[] = "processo_estrutura_id_INT={$obj->getProcesso_estrutura_id_INT()}";
            $strGET[] = "processo_estrutura_id_INT={$obj->getProcesso_estrutura_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_banco_id_INT())){

            $strCondicao[] = "tipo_banco_id_INT={$obj->getTipo_banco_id_INT()}";
            $strGET[] = "tipo_banco_id_INT={$obj->getTipo_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_analise_projeto_id_INT())){

            $strCondicao[] = "tipo_analise_projeto_id_INT={$obj->getTipo_analise_projeto_id_INT()}";
            $strGET[] = "tipo_analise_projeto_id_INT={$obj->getTipo_analise_projeto_id_INT()}";

        }

         if(!Helper::isNull($obj->getSeq_INT())){

            $strCondicao[] = "seq_INT={$obj->getSeq_INT()}";
            $strGET[] = "seq_INT={$obj->getSeq_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM processo_estrutura_tipo_banco " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM processo_estrutura_tipo_banco " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Processos Relacionados Ao Tipo De Banco</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_processo_estrutura_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_analise_projeto_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_seq_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProcesso_estrutura_id_INT())){
                
                        $obj->getFkObjProcesso_estrutura()->select($obj->getProcesso_estrutura_id_INT());
                        $obj->getFkObjProcesso_estrutura()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProcesso_estrutura()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_banco_id_INT())){
                
                        $obj->getFkObjTipo_banco()->select($obj->getTipo_banco_id_INT());
                        $obj->getFkObjTipo_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_analise_projeto_id_INT())){
                
                        $obj->getFkObjTipo_analise_projeto()->select($obj->getTipo_analise_projeto_id_INT());
                        $obj->getFkObjTipo_analise_projeto()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_analise_projeto()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getSeq_INT() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=processo_estrutura_tipo_banco&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=processo_estrutura_tipo_banco&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Processo_estrutura_tipo_banco&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=processo_estrutura_tipo_banco&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
