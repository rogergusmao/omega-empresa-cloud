<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       sistema_produto_log_erro
    * NOME DA CLASSE DAO: DAO_Sistema_produto_log_erro
    * DATA DE GERAÇÃO:    25.05.2013
    * ARQUIVO:            EXTDAO_Sistema_produto_log_erro.php
    * TABELA MYSQL:       sistema_produto_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $idSLI = Helper::GET(Param_Get::ID1);

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $objSPLE = new EXTDAO_Sistema_produto_log_erro();
    $objSPLE->setByGet($registrosPesquisa);
    $objSPLE->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($objSPLE->getClasse())){

            $strCondicao[] = "classe LIKE '%{$objSPLE->getClasse()}%'";
            $strGET[] = "classe={$objSPLE->getClasse()}";

        }

         if(!Helper::isNull($objSPLE->getFuncao())){

            $strCondicao[] = "funcao LIKE '%{$objSPLE->getFuncao()}%'";
            $strGET[] = "funcao={$objSPLE->getFuncao()}";

        }

         if(!Helper::isNull($objSPLE->getLinha_INT())){

            $strCondicao[] = "linha_INT={$objSPLE->getLinha_INT()}";
            $strGET[] = "linha_INT={$objSPLE->getLinha_INT()}";

        }

         if(!Helper::isNull($objSPLE->getNome_arquivo())){

            $strCondicao[] = "nome_arquivo LIKE '%{$objSPLE->getNome_arquivo()}%'";
            $strGET[] = "nome_arquivo={$objSPLE->getNome_arquivo()}";

        }

         if(!Helper::isNull($objSPLE->getIdentificador_erro())){

            $strCondicao[] = "identificador_erro LIKE '%{$objSPLE->getIdentificador_erro()}%'";
            $strGET[] = "identificador_erro={$objSPLE->getIdentificador_erro()}";

        }

         if(!Helper::isNull($objSPLE->getDescricao())){

            $strCondicao[] = "descricao LIKE '%{$objSPLE->getDescricao()}%'";
            $strGET[] = "descricao={$objSPLE->getDescricao()}";

        }

         if(!Helper::isNull($objSPLE->getStacktrace())){

            $strCondicao[] = "stacktrace LIKE '%{$objSPLE->getStacktrace()}%'";
            $strGET[] = "stacktrace={$objSPLE->getStacktrace()}";

        }

         if(!Helper::isNull($objSPLE->getData_visualizacao_DATETIME())){

            $strCondicao[] = "data_visualizacao_DATETIME={$objSPLE->getData_visualizacao_DATETIME()}";
            $strGET[] = "data_visualizacao_DATETIME={$objSPLE->getData_visualizacao_DATETIME()}";

        }

         if(!Helper::isNull($objSPLE->getData_ocorrida_DATETIME())){

            $strCondicao[] = "data_ocorrida_DATETIME={$objSPLE->getData_ocorrida_DATETIME()}";
            $strGET[] = "data_ocorrida_DATETIME={$objSPLE->getData_ocorrida_DATETIME()}";

        }

         if(!Helper::isNull($objSPLE->getSistema_tipo_log_erro_id_INT())){

            $strCondicao[] = "sistema_tipo_log_erro_id_INT={$objSPLE->getSistema_tipo_log_erro_id_INT()}";
            $strGET[] = "sistema_tipo_log_erro_id_INT={$objSPLE->getSistema_tipo_log_erro_id_INT()}";

        }

         if(!Helper::isNull($objSPLE->getSistema_projetos_versao_produto_id_INT())){

            $strCondicao[] = "getSistema_projetos_versao_produto_id_INT={$objSPLE->getSistema_projetos_versao_produto_id_INT()}";
            $strGET[] = "getSistema_projetos_versao_produto_id_INT={$objSPLE->getSistema_projetos_versao_produto_id_INT()}";

        }

         if(!Helper::isNull($objSPLE->getId_usuario_INT())){

            $strCondicao[] = "id_usuario_INT={$objSPLE->getId_usuario_INT()}";
            $strGET[] = "id_usuario_INT={$objSPLE->getId_usuario_INT()}";

        }

         if(!Helper::isNull($objSPLE->getId_corporacao_INT())){

            $strCondicao[] = "id_corporacao_INT={$objSPLE->getId_corporacao_INT()}";
            $strGET[] = "id_corporacao_INT={$objSPLE->getId_corporacao_INT()}";

        }

         if(!Helper::isNull($objSPLE->getSistema_log_identificador_id_INT())){

            $strCondicao[] = "sistema_log_identificador_id_INT={$objSPLE->getSistema_log_identificador_id_INT()}";
            $strGET[] = "sistema_log_identificador_id_INT={$objSPLE->getSistema_log_identificador_id_INT()}";

        }
$strCondicao[] = "sistema_log_identificador_id_INT={$idSLI}";
        $strGET[] = "sistema_log_identificador_id_INT={$idSLI}";

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(sple.id) 
        FROM sistema_produto_log_erro sple " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id 
        FROM sistema_produto_log_erro 
        " . $consulta . " 
        ORDER BY data_visualizacao_DATETIME DESC
        LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Log De Erros Ocorridos Nas Versões Do Produto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="100%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos">Exceções</td>
			

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$objSPLE->select($regs[0]);
    	$objSPLE->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?
                        $msg = "<table>";
                        $msg .= "<tr>";
                        $msg .= "<td>";
                        $msg .= "<b class='b_titulo'>{$objSPLE->label_id}:</b>";
                        $msg .= "</td>";
                        $msg .= "<td>";
                        $msg .= "<i class='i_conteudo'>{$objSPLE->getId()}</i>";
                        $msg .= "</td>";
                        $msg .= "</tr>";
                       
                        if(strlen($objSPLE->getData_visualizacao_DATETIME())){
                            $msg .= "<tr>";
                            $msg .= "<td>";
                            $msg .= "<b class='b_titulo'>{$objSPLE->label_data_visualizacao_DATETIME}:";
                            $msg .= "</td>";
                            $msg .= "<td>";
                            $msg .= "<i class='i_conteudo'>{$objSPLE->getData_visualizacao_DATETIME()}</i>";
                            $msg .= "</td>";
                            $msg .= "</tr>";
                        }
                        
                        $msg .= "<tr>";
                        $msg .= "<td>";
                        $msg .= "<b class='b_titulo'>{$objSPLE->label_data_ocorrida_DATETIME}:</b>";
                        $msg .= "</td>";
                        $msg .= "<td>";
                        $msg .= "<i class='i_conteudo'>{$objSPLE->getData_ocorrida_DATETIME()}</i>";
                        $msg .= "</td>";
                        $msg .= "</tr>";
                        
                        $tipoLogErro = null;
                        if(strlen($objSPLE->getSistema_tipo_log_erro_id_INT())){
                            $objSPLE->getFkObjSistema_tipo_log_erro()->select($objSPLE->getSistema_tipo_log_erro_id_INT());
                            $objSPLE->getFkObjSistema_tipo_log_erro()->formatarParaExibicao();
                            $tipoLogErro = $objSPLE->getFkObjSistema_tipo_log_erro()->valorCampoLabel() ;
                        } 
                        if(strlen($tipoLogErro)){
                            $msg .= "<tr>";
                            $msg .= "<td>";
                            $msg .= "<b class='b_titulo'>{$objSPLE->label_sistema_tipo_log_erro_id_INT}:</b>";
                            $msg .= "</td>";
                            $msg .= "<td>";
                            $msg .= "<i class='i_conteudo'>{$tipoLogErro}</i>";
                            $msg .= "</td>";
                            $msg .= "</tr>";
                        }
                        
                        $msg .= "<tr>";
                        $msg .= "<td>";
                        $msg .= "<b class='b_titulo'>{$objSPLE->label_sistema_projetos_versao_produto_id_INT}:</b>";
                        $msg .= "</td>";
                        $msg .= "<td>";
                        $msg .= "<i class='i_conteudo'>{$objSPLE->getFkObjSistema_projetos_versao_produto()->valorCampoLabel()}</i>";
                        $msg .= "</td>";
                        $msg .= "</tr>";
                         
                        $msg .= "</table>";
                        
                        
                        echo $msg;
                        $msgDesc = "";
                        $msgDesc .= "<b class='b_titulo'>{$objSPLE->label_identificador_erro}:</b>";                        
                        $msgDesc .= "</br><i class='i_conteudo'>{$objSPLE->getIdentificador_erro()}</i>";
                        $msgDesc .= "</br></br><b class='b_titulo'>{$objSPLE->label_descricao}:</b></br><i class='i_conteudo'>{$objSPLE->getDescricao()}</i>";
                        $msgDesc .= "</br></br><b class='b_titulo'>{$objSPLE->label_stacktrace}:</b><i class='i_conteudo'>{$objSPLE->getStacktrace()}</i>";
                        
                        $estadoSPLE = $objSPLE->getEstado();
                        $tipoMensagem = null;
                        switch ($estadoSPLE) {
                            case EXTDAO_Sistema_produto_log_erro::ESTADO_CORRIGIDA:
                                $tipoMensagem = MENSAGEM_OK;
                                break;
                            case EXTDAO_Sistema_produto_log_erro::ESTADO_ERRO:
                                $tipoMensagem = MENSAGEM_ERRO;
                                break;
                            case EXTDAO_Sistema_produto_log_erro::ESTADO_NAO_VISUALIZADO:
                                $tipoMensagem = MENSAGEM_WARNING;
                                break;
                            default:
                                break;
                        }
                        Helper::imprimirMensagem($msgDesc, $tipoMensagem);
                        ?>
    		</td>

    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=sistema_produto_log_erro&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
