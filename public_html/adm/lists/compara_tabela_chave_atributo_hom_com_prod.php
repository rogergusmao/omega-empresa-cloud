<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       tabela_chave_atributo_tabela_chave_atributo
 * NOME DA CLASSE DAO: DAO_Tabela_chave_atributo_tabela_chave_atributo
 * DATA DE GERAÇÃO:    20.04.2013
 * ARQUIVO:            EXTDAO_Tabela_chave_atributo_tabela_chave_atributo.php
 * TABELA MYSQL:       tabela_chave_atributo_tabela_chave_atributo
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
$acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
$acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
$acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$objTCATCA = new EXTDAO_Tabela_chave_atributo_tabela_chave_atributo();


$idTT = Helper::POSTGET(Param_Get::TABELA_TABELA);
$idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
$idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);


$varGET = Param_Get::getIdentificadorProjetosVersao();
$varGET .= "&".Param_Get::TABELA_TABELA."=".$idTT;

$strCondicao = array();
$strGET = array();

$consultaNumero = "SELECT COUNT(id) 
    FROM tabela_chave_atributo_tabela_chave_atributo 
    WHERE tabela_chave_tabela_chave_id_INT = $pIdTCTC 
        ORDER BY id";
$objBanco = new Database();
$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT id 
    FROM tabela_chave_atributo_tabela_chave_atributo 
    WHERE tabela_chave_tabela_chave_id_INT = $pIdTCTC 
        ORDER BY id";

$objBanco->query($consultaRegistros);

$objArg->isDisabled = true;
$objArg->disabled = true;
?>


<?
$validade=  false;
for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {
    $validade=  true;
?>

    
    
    <?
    if($i == 1){
    ?>
    <fieldset class="fieldset_list">
    <table class="tabela_list">
        <colgroup>

            <col width="31%" />
            <col width="31%" />
            <col width="31%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">


                <td class="td_list_titulos"><?= $objTCATCA->label_homologacao_tabela_chave_atributo_id_INT ?></td>
                <td class="td_list_titulos"><?= $objTCATCA->label_producao_tabela_chave_atributo_id_INT ?></td>

                <td class="td_list_titulos">Avisos</td>

            </tr>
        </thead>
        <tbody>

            <?
    }  

            $objTCATCA->select($regs[0]);
            $objTCATCA->formatarParaExibicao();

            $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par"
            ?>

            <tr class="<?= $classTr ?>">


                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                    <?
                    if (strlen($objTCATCA->getHomologacao_tabela_chave_atributo_id_INT())) {

                        $objTCATCA->getFkObjHomologacao_tabela_chave_atributo()->select($objTCATCA->getHomologacao_tabela_chave_atributo_id_INT());
                        $objTCATCA->getFkObjHomologacao_tabela_chave_atributo()->formatarParaExibicao();
                        ?>

                        <?= $objTCATCA->getFkObjHomologacao_tabela_chave_atributo()->valorCampoLabel() ?>

                    <? } ?>

                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                    <?
                    if (strlen($objTCATCA->getProducao_tabela_chave_atributo_id_INT())) {

                        $objTCATCA->getFkObjProducao_tabela_chave_atributo()->select($objTCATCA->getProducao_tabela_chave_atributo_id_INT());
                        $objTCATCA->getFkObjProducao_tabela_chave_atributo()->formatarParaExibicao();
                        ?>

                        <?= $objTCATCA->getFkObjProducao_tabela_chave_atributo()->valorCampoLabel() ?>

                    <? } ?>

                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <?
                    $vIdAA = null;
                    $vIdAA = $objTCATCA->getIdAtributoAtributo($idPVBB);
                    if(strlen($vIdAA)){
                        ?>
                        <a href="index.php?tipo=forms&page=compara_atributo_hom_atributo_prod&<?= $varGET ?>&id1=<?= $vIdAA ?>" target="_self" >
                            Relacionamento entre atributo de homologação com produção
                        </a>
                        <?
                    }

                    ?>

                </td>
            </tr>

<?
}

if($validade){
?>


        </tbody>
    </table>

</fieldset>
<?
}
