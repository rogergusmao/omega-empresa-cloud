<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       tabela
    * NOME DA CLASSE DAO: DAO_Tabela
    * DATA DE GERAÇÃO:    05.02.2013
    * ARQUIVO:            EXTDAO_Tabela.php
    * TABELA MYSQL:       tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $obj = new EXTDAO_Tabela();

    
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    
    $objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
    $cicloFinalizado = false;

    $idPVPE = EXTDAO_Projetos_versao_processo_estrutura::getIdDoProcessoEstrutura($idPVBB, EXTDAO_Processo_estrutura::CARREGA_ESTRUTURA_BANCO_DE_HOM_PARA_PROD);

    EXTDAO_Tabela::corrigeFrequenciaDasTabelas($objPVBB->getProjetos_versao_id_INT(), $objPVBB->getFkObjBanco_banco()->getHomologacao_banco_id_INT());
    $objPV = new EXTDAO_Projetos_versao();
    $objPV->select($idPV);
    $idBB =  $objPVBB->getBanco_banco_id_INT();
    $objBB = new EXTDAO_Banco_banco();
    $objBB->select($idBB);
    
    Helper::imprimirMensagem(
        "Tipo Análise: ({$objPV->getFkObjTipo_analise_projeto()->getId()})" . $objPV->getFkObjTipo_analise_projeto()->getNome() . "<br/>"  
       . " Projetos versão: ($idPV){$objPV->getNome()}</br> "
       . " Projetos versão banco banco: ({$objPVBB->getId()}){$objPVBB->getNome()}</br> " 
       . " Banco Homologação: ({$objBB->getFkObjHomologacao_banco()->getId()}){$objBB->getFkObjHomologacao_banco()->getNome()}</br> "
       . " Banco Produção: ({$objBB->getFkObjProducao_banco()->getId()}){$objBB->getFkObjProducao_banco()->getNome()}</br> "

       );

    $objTabelaTabela = new EXTDAO_Tabela_tabela();
    $objBanco = new Database();

     $consultaNumeroRegistros = "SELECT COUNT(DISTINCT(t.id))
FROM tabela t JOIN tabela_tabela tt ON t.id = tt.homologacao_tabela_id_INT
WHERE tt.projetos_versao_banco_banco_id_INT = ".$idPVBB;
    
     $objBanco->query($consultaNumeroRegistros);
     $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);
     
    $consultaRegistros = "SELECT DISTINCT(t.id)
FROM tabela t JOIN tabela_tabela tt ON t.id = tt.homologacao_tabela_id_INT
WHERE tt.projetos_versao_banco_banco_id_INT = ".$idPVBB;
    
    $objBanco->query($consultaRegistros);
    $class = $obj->nomeClasse;
    $postar = "actions.php";
    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <?  EXTDAO_Projetos_versao::imprimiCabecalhoIdentificador();?>
        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="EXTDAO_Projetos_versao">
        <input type="hidden" name="action" id="action" value="atualizaSistemaTabelaDoBancoDeHomologacao">
        
        
    	<input type="hidden" name="origin_action" id="origin_action" value="">


   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Tabelas Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
                        <col width="5%" />
			<col width="10%" />
			<col width="20%" />
			<col width="10%" />
			<col width="10%" />
                        <col width="10%" />
                        <col width="25%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">
                        <td class="td_list_titulos">Id Tabela Homologação</td>
                        <td class="td_list_titulos"><?=$objTabelaTabela->label_status_verificacao_BOOLEAN?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
                        
			<td class="td_list_titulos"><?=$obj->label_transmissao_web_para_mobile_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_transmissao_mobile_para_web_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_frequencia_sincronizador_INT ?></td>
                        
                        <td class="td_list_titulos">Ações</td>
                        
		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){
        
        $obj->select($regs[0]);
    	$obj->formatarParaExibicao();
        $objTabelaTabela = EXTDAO_Tabela_tabela::getObjDaTabelaHomETabelaProd($idPVBB, $regs[0], null, false);
    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">
                <input type="hidden" name="id<?=$i ?>" id="id<?=$i ?>" value="<?=$obj->getId(); ?>">
                <input type="hidden" name="nome<?=$i ?>" id="nome<?=$i ?>" value="<?=$obj->getNome(); ?>">
                <input type="hidden" name="banco_id_INT<?=$i ?>" id="banco_id_INT<?=$i ?>" value="<?=$obj->getBanco_id_INT(); ?>">
                <input type="hidden" name="projetos_versao_id_INT<?=$i ?>" id="projetos_versao_id_INT<?=$i ?>" value="<?=$obj->getProjetos_versao_id_INT(); ?>">
                
                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $i;
                $objArg->label = $objTabelaTabela->label_status_verificacao_BOOLEAN;
                $objArg->labelTrue = "Ativo";
                $objArg->labelFalse = "Inativo";
                $objArg->valor = $objTabelaTabela->getStatus_verificacao_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;

                ?>
    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_form_campo"><?=$objTabelaTabela->imprimirCampoStatus_verificacao_BOOLEAN($objArg); ?></td>


    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>


    		<?

                    $objArg->numeroDoRegistro = $i;
                    $objArg->label = $obj->label_transmissao_web_para_mobile_BOOLEAN;
                    $objArg->labelTrue = "Sim";
                    $objArg->labelFalse = "Não";
                    $objArg->valor = $obj->getTransmissao_web_para_mobile_BOOLEAN() ;
                    $objArg->classeCss = "input_text";
                    $objArg->classeCssFocus = "focus_text";
                    $objArg->obrigatorio = false;
                    $objArg->largura = 80;

                    ?>


                    <td class="td_form_campo"><?=$obj->imprimirCampoTransmissao_web_para_mobile_BOOLEAN($objArg); ?></td>

            <?

                    $objArg->numeroDoRegistro = $i;
                    $objArg->label = $obj->label_transmissao_mobile_para_web_BOOLEAN;
                    $objArg->labelTrue = "Sim";
                    $objArg->labelFalse = "Não";
                    $objArg->valor = $obj->getTransmissao_mobile_para_web_BOOLEAN();
                    $objArg->classeCss = "input_text";
                    $objArg->classeCssFocus = "focus_text";
                    $objArg->obrigatorio = false;
                    $objArg->largura = 80;

                    ?>

                    <td class="td_form_campo"><?=$obj->imprimirCampoTransmissao_mobile_para_web_BOOLEAN($objArg); ?></td>


    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <?  $strSelectUma= "";
                        $strSelectZero= "";
                        $strSelectSempre= "";
                        $vFrequencia = $obj->getFrequencia_sincronizador_INT();
                        if($vFrequencia != null)
                        switch ($vFrequencia) {
                            case -1:
                                $strSelectSempre = "selected";
                                break;
                            case 0:
                                $strSelectZero = "selected";
                                break;
                            case 1:
                                $strSelectUma = "selected";
                                break;
                            default:
                                break;
                        }
                    ?>
                    <select <?="name=\"frequencia_sincronizador_INT$i\""?> <?="id=\"frequencia_sincronizador_INT$i\""?> >
                        <option value="0" <?=$strSelectZero?>>Zero</option>
                        <option value="1" <?=$strSelectUma?>>Uma vez</option>
                        <option value="-1" <?=$strSelectSempre?>>Sempre</option>
                    </select>
    		</td>


<td class="td_list_conteudo" style="text-align: center;">
				
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=lists&page=gerenciar_sincronizacao_hierarquia_banco&id1=<?=$obj->getId(); ?>&<?=$varGET;?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				
			</td>
    
		</tr>

    <? } ?>
        
                 <tr class="tr_form_rodape1">
            <td colspan="7">

                            <div id="div_barra_de_acoes">
                                    <table class="table_botoes_form">
                                            <tbody>
                                                <tr class="tr_botoes_form">
                                                    <td class="td_botoes_form">
                                                            
                                                            
                                                        <input class="botoes_form" type="submit" value="Salvar Dados de Sincronização" >
                                                            
                                                    </td>
                                                </tr>
                                            </tbody>
                                    </table>
                            </div>
            </td>
        </tr>
         
              
            
    </tbody>
   
    </table>

    </fieldset>

    <br/>
    <br/>
    <?=$obj->getRodapeFormulario(); ?>
    <?
    
	
