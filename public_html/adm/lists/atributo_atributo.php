<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       atributo_atributo
    * NOME DA CLASSE DAO: DAO_Atributo_atributo
    * DATA DE GERAÇÃO:    30.01.2013
    * ARQUIVO:            EXTDAO_Atributo_atributo.php
    * TABELA MYSQL:       atributo_atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    include("filters/atributo_atributo.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Atributo_atributo();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getHomologacao_atributo_id_INT())){

            $strCondicao[] = "homologacao_atributo_id_INT={$obj->getHomologacao_atributo_id_INT()}";
            $strGET[] = "homologacao_atributo_id_INT={$obj->getHomologacao_atributo_id_INT()}";

        }

         if(!Helper::isNull($obj->getProducao_atributo_id_INT())){

            $strCondicao[] = "producao_atributo_id_INT={$obj->getProducao_atributo_id_INT()}";
            $strGET[] = "producao_atributo_id_INT={$obj->getProducao_atributo_id_INT()}";

        }

         if(!Helper::isNull($obj->getTipo_operacao_atualizacao_banco_id_INT())){

            $strCondicao[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";
            $strGET[] = "tipo_operacao_atualizacao_banco_id_INT={$obj->getTipo_operacao_atualizacao_banco_id_INT()}";

        }

         if(!Helper::isNull($obj->getStatus_verificacao_BOOLEAN())){

            $strCondicao[] = "status_verificacao_BOOLEAN={$obj->getStatus_verificacao_BOOLEAN()}";
            $strGET[] = "status_verificacao_BOOLEAN={$obj->getStatus_verificacao_BOOLEAN()}";

        }

         if(!Helper::isNull($obj->getProjetos_versao_id_INT())){

            $strCondicao[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";
            $strGET[] = "projetos_versao_id_INT={$obj->getProjetos_versao_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM atributo_atributo " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM atributo_atributo " . $consulta . " ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Relacionamentos Entre Atributos Do Banco De Homologação E Produção</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_homologacao_atributo_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_producao_atributo_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_tipo_operacao_atualizacao_banco_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_status_verificacao_BOOLEAN ?></td>
			<td class="td_list_titulos"><?=$obj->label_projetos_versao_id_INT ?></td>
			<td class="td_list_titulos">Ações</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getHomologacao_atributo_id_INT())){
                
                        $obj->getFkObjHomologacao_atributo()->select($obj->getHomologacao_atributo_id_INT());
                        $obj->getFkObjHomologacao_atributo()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjHomologacao_atributo()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProducao_atributo_id_INT())){
                
                        $obj->getFkObjProducao_atributo()->select($obj->getProducao_atributo_id_INT());
                        $obj->getFkObjProducao_atributo()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProducao_atributo()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getTipo_operacao_atualizacao_banco_id_INT())){
                
                        $obj->getFkObjTipo_operacao_atualizacao_banco()->select($obj->getTipo_operacao_atualizacao_banco_id_INT());
                        $obj->getFkObjTipo_operacao_atualizacao_banco()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjTipo_operacao_atualizacao_banco()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getStatus_verificacao_BOOLEAN()?"Ativo":"Inativo" ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getProjetos_versao_id_INT())){
                
                        $obj->getFkObjProjetos_versao()->select($obj->getProjetos_versao_id_INT());
                        $obj->getFkObjProjetos_versao()->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->getFkObjProjetos_versao()->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=atributo_atributo&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=atributo_atributo&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Atributo_atributo&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Paginação</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=atributo_atributo&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
