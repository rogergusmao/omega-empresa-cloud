<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: projetos_versao_log_erro
    * DATA DE GERAÇÃO:    01.05.2013
    * ARQUIVO:            projetos_versao_log_erro.php
    * TABELA MYSQL:       projetos_versao_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Projetos_versao_log_erro();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_projetos_versao_log_erro">

    	<?
        $objArg->isDisabled= true;
        $objArg->disabled= true;
    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar log de erro ocorrido na versão de projeto";

            }
            else{

            	$legend = "Cadastrar log de erro ocorrido na versão de projeto";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_classe;
    			$objArg->valor = $obj->getClasse();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoClasse($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_funcao;
    			$objArg->valor = $obj->getFuncao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoFuncao($objArg); ?></td>
			</tr>
			<tr class="tr_form">



    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_ocorrida_DATETIME;
    			$objArg->valor = $obj->getData_ocorrida_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_ocorrida_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_usuario_id_INT;
    			$objArg->valor = $obj->getUsuario_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("usuario_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllUsuario($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_projetos_tipo_log_erro_id_INT;
    			$objArg->valor = $obj->getProjetos_tipo_log_erro_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("projetos_tipo_log_erro_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos_tipo_log_erro($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_projetos_versao_banco_banco_id_INT;
    			$objArg->valor = $obj->getProjetos_versao_banco_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("projetos_versao_banco_banco_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos_versao_banco_banco($objArg); ?>
    			</td>


    			

            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>
                         <tr class="tr_form">
                            
    			<?
                        $objSCB = new EXTDAO_Script_comando_banco();
                        $consultaSQL = EXTDAO_Projetos_versao_log_erro_script::getConsultaSQL($obj->getId());
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $objSCB->label_consulta;
    			$objArg->valor = $consultaSQL;
    			$objArg->obrigatorio = false;
    			$objArg->largura = "98%";
    			$objArg->altura = 330;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
                        <td class="td_form_campo" colspan="3">

    			<?

    			Helper::includePHP(1, "recursos/libs/fckeditor/fckeditor.php");

    			$editor = new FCKeditor("descricao_HTML" . $objArg->numeroDoRegistro); //Nomeia a área de texto
                $editor-> BasePath = "../recursos/libs/fckeditor/";  //Informa a pasta do FKC Editor
                $editor-> ToolbarSet = "Basic";
                $editor-> Value = html_entity_decode($objArg->valor); //Informa o valor inicial do campo, no exemplo está vazio
                $editor-> Width = $objArg->largura;          //informa a largura do editor
                $editor-> Height = $objArg->altura;         //informa a altura do editor
                $editor-> Create();               // Cria o editor

                ?>

    			</td>
                        
                        </tr>
                        <tr class="tr_form">
                            
    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_descricao_HTML;
    			$objArg->valor = $obj->getDescricao_HTML();
    			$objArg->obrigatorio = false;
    			$objArg->largura = "98%";
    			$objArg->altura = 330;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
                        <td class="td_form_campo" colspan="3">

    			<?

    			Helper::includePHP(1, "recursos/libs/fckeditor/fckeditor.php");

    			$editor = new FCKeditor("descricao_HTML" . $objArg->numeroDoRegistro); //Nomeia a área de texto
                $editor-> BasePath = "../recursos/libs/fckeditor/";  //Informa a pasta do FKC Editor
                $editor-> ToolbarSet = "Basic";
                $editor-> Value = html_entity_decode($objArg->valor); //Informa o valor inicial do campo, no exemplo está vazio
                $editor-> Width = $objArg->largura;          //informa a largura do editor
                $editor-> Height = $objArg->altura;         //informa a altura do editor
                $editor-> Create();               // Cria o editor

                ?>

    			</td>
                        
                        </tr>

     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

