<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: permissao_sistema_mobile
    * DATA DE GERAÇÃO:    19.10.2013
    * ARQUIVO:            permissao_sistema_mobile.php
    * TABELA MYSQL:       permissao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Permissao_sistema_mobile();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_permissao_sistema_mobile"=>"Adicionar nova permissão do sistema mobile",
    					 "list_permissao_sistema_mobile"=>"Listar permissões do sistema mobile");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_permissao_sistema_mobile">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar permissão do sistema mobile";

            }
            else{

            	$legend = "Cadastrar permissão do sistema mobile";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        	<input type="hidden" name="projetos_versao_banco_banco_id_INT" value="{$obj->getProjetos_versao_banco_banco_id_INT()}" />
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoNome($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tag;
    			$objArg->valor = $obj->getTag();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoTag($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_pai_permissao_id_INT;
                            $objArg->valor = $obj->getPai_permissao_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = false;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("pai_permissao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllPai_permissao($objArg); ?>
                            </td>


                            

                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_tipo_permissao_id_INT;
                            $objArg->valor = $obj->getTipo_permissao_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = false;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("tipo_permissao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllTipo_permissao($objArg); ?>
                            </td>


                            			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_icone_32_ARQUIVO;
    			$objArg->valor = $obj->getIcone_32_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoIcone_32_ARQUIVO($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_icone_64_ARQUIVO;
    			$objArg->valor = $obj->getIcone_64_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoIcone_64_ARQUIVO($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_icone_128_ARQUIVO;
    			$objArg->valor = $obj->getIcone_128_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoIcone_128_ARQUIVO($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_icone_256_ARQUIVO;
    			$objArg->valor = $obj->getIcone_256_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoIcone_256_ARQUIVO($objArg); ?>
                            
                        </td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

