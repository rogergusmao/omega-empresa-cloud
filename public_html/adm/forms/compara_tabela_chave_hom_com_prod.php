<?php
$objArg = new Generic_Argument();

/*
 *
 * -------------------------------------------------------
 * NOME DO FORMULÁRIO: tabela_chave
 * DATA DE GERAÇÃO:    07.04.2013
 * ARQUIVO:            tabela_chave.php
 * TABELA MYSQL:       tabela_chave
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Tabela_chave_tabela_chave();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = (Helper::GET("id1") ? "edit" : "add");
$postar = "actions.php";

if ($objHomologacao == null && $objProducao == null) {

    $objHomologacao = new EXTDAO_Tabela_chave();
    $objProducao = new EXTDAO_Tabela_chave();
    $objHomologacao->select(Helper::GET("id_homologacao"));
    $objProducao->select(Helper::GET("id_producao"));
}

$idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
$vIdTCTC = EXTDAO_Tabela_chave_tabela_chave::getIdTabelaChaveTabelaChave(
            $idPVBB, $objHomologacao->getId(), $objProducao->getId()
);


if (strlen($vIdTCTC)) {
    $obj->select($vIdTCTC);
}

$objCompara = $obj->comparaChaveHomologacaoComProducao();
$fatoresDif = $objCompara["fatores"];
$fatoresDifTipoOperacao = $objCompara["fatores_tipo_operacao"];
$classTr = "";
$classTrPar = null;
$classTrImpar = null;
$tipoModificacao = $objCompara["tipo_modificacao"];
if ($tipoModificacao != EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT) {
    $classTrPar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr($tipoModificacao, 0);
    $classTrImpar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr($tipoModificacao, 1);
} else {
    $classTrPar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION, 0);
    $classTrImpar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION, 1);
}

//Desabilitando as caixas de texto e o checkbox
$objArg->isDisabled = true;
$objArg->disabled = true;
?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_tabela_chave">

<?
$objArg->isDisabled = true;
$objArg->disabled = true;
for ($cont = 1; $cont <= $numeroRegistros; $cont++) {

    $obj->formatarParaExibicao();
    ?>

    <fieldset class="fieldset_form">
        <?
        $classLengend = EXTDAO_Tipo_operacao_atualizacao_banco::getClassLegend(
                        $obj->atualizaTipoOperacaoAtualizacaoBanco());
        ?>

        <legend class="<?= $classLengend; ?>">Chave <?= $obj->getId(); ?></legend>

        <table class="tabela_form">

            <colgroup>
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
            </colgroup>
            <thead>
                <tr class="tr_list_titulos">

                    <td class="td_list_titulos">Propriedade</td>
                    <td class="td_list_titulos">Homologação</td>
                    <td class="td_list_titulos">Produção</td>
                    <td class="td_list_titulos">Avisos</td>

                </tr>
    <?
    if (in_array(EXTDAO_Tabela_chave_tabela_chave::FATOR_NOME, $fatoresDif)) {
        $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                        $fatoresDifTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_NOME], 0);
    } else
        $classTr = $classTrPar;
    ?>
                <tr class="tr_form">


                <?
                $objArg->numeroDoRegistro = 1;
                $objArg->label = $objHomologacao->label_nome;
                $objArg->valor = $objHomologacao->getNome();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;
                ?>

                    <td class="td_list_titulos"><?= $objArg->getLabel() ?></td>
                    <td class="td_form_campo"><?= $objHomologacao->imprimirCampoNome($objArg); ?></td>
                    <?
                    $objArg->numeroDoRegistro = 2;
                    $objArg->label = $objProducao->label_nome;
                    $objArg->valor = $objProducao->getNome();
                    $objArg->classeCss = "input_text";
                    $objArg->classeCssFocus = "focus_text";
                    $objArg->obrigatorio = true;
                    $objArg->largura = 200;
                    ?>

                    <td class="td_form_campo"><?= $objProducao->imprimirCampoNome($objArg); ?></td>
                    <td class="td_form_campo">O nome não é critério de diferença de chave</td>
                </tr>
    <?
    if (in_array(EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_TYPE, $fatoresDif)) {
        $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                        $fatoresDifTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_TYPE], 1);
    } else
        $classTr = $classTrImpar;
    ?>
                <tr class="<?= $classTr; ?>">
                <?
                $objArg->numeroDoRegistro = 1;
                $objArg->label = $objHomologacao->label_index_type;
                $objArg->valor = $objHomologacao->getIndex_type();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                    <td class="td_list_titulos"><?= $objArg->getLabel() ?></td>
                    <td class="td_form_campo"><?= $objHomologacao->imprimirCampoIndex_type($objArg); ?></td>

    <?
    $objArg->numeroDoRegistro = 2;
    $objArg->label = $objProducao->label_index_type;
    $objArg->valor = $objProducao->getIndex_type();
    $objArg->classeCss = "input_text";
    $objArg->classeCssFocus = "focus_text";
    $objArg->obrigatorio = false;
    $objArg->largura = 200;
    ?>


                    <td class="td_form_campo"><?= $objProducao->imprimirCampoIndex_type($objArg); ?></td>
                    <td class="td_form_campo"></td>
                </tr>
    <?
    if (in_array(EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_METHOD, $fatoresDif)) {
        $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                        $fatoresDifTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_INDEX_METHOD], 0);
    } else
        $classTr = $classTrPar;
    ?>
                <tr class="<?= $classTr; ?>">

                <?
                $objArg->numeroDoRegistro = 1;
                $objArg->label = $objHomologacao->label_index_method;
                $objArg->valor = $objHomologacao->getIndex_method();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                    <td class="td_list_titulos"><?= $objArg->getLabel() ?></td>
                    <td class="td_form_campo"><?= $objHomologacao->imprimirCampoIndex_method($objArg); ?></td>
                    <?
                    $objArg->numeroDoRegistro = 2;
                    $objArg->label = $objProducao->label_index_method;
                    $objArg->valor = $objProducao->getIndex_method();
                    $objArg->classeCss = "input_text";
                    $objArg->classeCssFocus = "focus_text";
                    $objArg->obrigatorio = false;
                    $objArg->largura = 200;
                    ?>


                    <td class="td_form_campo"><?= $objProducao->imprimirCampoIndex_method($objArg); ?></td>
                    <td class="td_form_campo"></td>
                </tr>

    <?
    if (in_array(EXTDAO_Tabela_chave_tabela_chave::FATOR_NON_UNIQUE_BOOLEAN, $fatoresDif)) {
        $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
        $fatoresDifTipoOperacao[EXTDAO_Tabela_chave_tabela_chave::FATOR_NON_UNIQUE_BOOLEAN], 1);
    } else
        $classTr = $classTrImpar;
    ?>
                <tr class="<?= $classTr; ?>">


                <?
                $objArg->numeroDoRegistro = 1;
                $objArg->label = $objHomologacao->label_non_unique_BOOLEAN;
                $objArg->labelTrue = "Sim";
                $objArg->labelFalse = "Não";
                $objArg->valor = $objHomologacao->getNon_unique_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;
                ?>

                    <td class="td_list_titulos"><?= $objArg->getLabel() ?></td>
                    <td class="td_form_campo"><?= $objHomologacao->imprimirCampoNon_unique_BOOLEAN($objArg); ?></td>

    <?
    $objArg->numeroDoRegistro = 2;
    $objArg->label = $objProducao->label_non_unique_BOOLEAN;
    $objArg->labelTrue = "Sim";
    $objArg->labelFalse = "Não";
    $objArg->valor = $objProducao->getNon_unique_BOOLEAN();
    $objArg->classeCss = "input_text";
    $objArg->classeCssFocus = "focus_text";
    $objArg->obrigatorio = false;
    $objArg->largura = 80;
    ?>


                    <td class="td_form_campo"><?= $objProducao->imprimirCampoNon_unique_BOOLEAN($objArg); ?></td>
                    <td class="td_form_campo"></td>

                </tr>


<? } ?>


    </table>

</fieldset>

<?
$pIdTCTC = $vIdTCTC;
$tipoOperacaoAtu = $obj->atualizaTipoOperacaoAtualizacaoBanco();
if ($tipoOperacaoAtu == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT
        || $tipoOperacaoAtu == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION)
    include("lists/compara_tabela_chave_atributo_hom_com_prod.php");
else {
    if ($tipoOperacaoAtu == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_INSERT) {
        $pIdTabelaChave = $obj->getHomologacao_tabela_chave_id_INT();
        include("lists/compara_tabela_chave_atributo_hom_com_prod_insert.php");
    } else if ($tipoOperacaoAtu == EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE) {
        $pIdTabelaChave = $obj->getProducao_tabela_chave_id_INT();
        include("lists/compara_tabela_chave_atributo_hom_com_prod_delete.php");
    }
}
?>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>

