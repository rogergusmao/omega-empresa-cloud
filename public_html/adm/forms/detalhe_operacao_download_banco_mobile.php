<?php
$objArg = new Generic_Argument();

/*
 *
 * -------------------------------------------------------
 * NOME DO FORMULÁRIO: operacao_download_banco_mobile
 * DATA DE GERAÇÃO:    14.05.2013
 * ARQUIVO:            operacao_download_banco_mobile.php
 * TABELA MYSQL:       operacao_download_banco_mobile
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

$objODBM = new EXTDAO_Operacao_download_banco_mobile();

$objArg = new Generic_Argument();
$objArg->disabled = false;
$numeroRegistros = 1;
$class = $objODBM->nomeClasse;
$action = "add";
$idMI = Helper::GET(Param_Get::ID_MOBILE_IDENTIFICADOR);
$postar = "actions.php";
$raiz = Helper::acharRaiz();
?>

<?= $objODBM->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_operacao_download_banco_mobile">

<?

    $idOSM = Helper::GET("id1");
    
    $idODBM = EXTDAO_Operacao_download_banco_mobile::getIdOperacaoSistemaMobile($idOSM);

    $objODBM->select($idODBM);

    $objODBM->formatarParaExibicao();
    ?>

    <input type="hidden" name="id<?= $cont ?>" id="id<?=$cont ?>" value="<?=$objODBM->getId(); ?>">
    <input type="hidden" name="<?=  Param_Get::ID_MOBILE_IDENTIFICADOR;?>" id="<?= Param_Get::ID_MOBILE_IDENTIFICADOR;?>" value="<?= $idMI; ?>">
    <fieldset class="fieldset_form">
        <legend class="legend_form"><?= $legend; ?></legend>

        <table class="tabela_form">

           
            <tr class="tr_form">


                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $objODBM->label_drop_tabela_BOOLEAN;
                $objArg->labelTrue = "Sim";
                $objArg->labelFalse = "Não";
                $objArg->valor = $objODBM->getDrop_tabela_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $objODBM->imprimirCampoDrop_tabela_BOOLEAN($objArg); ?></td>




                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $objODBM->label_popular_tabela_BOOLEAN;
                $objArg->labelTrue = "Sim";
                $objArg->labelFalse = "Não";
                $objArg->valor = $objODBM->getPopular_tabela_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $objODBM->imprimirCampoPopular_tabela_BOOLEAN($objArg); ?></td>
            </tr>
           
            <tr class="tr_form">

                    <?
                    $objArg->numeroDoRegistro = $cont;
                    $objArg->label = $objODBM->label_observacao;
                    $objArg->valor = $objODBM->getObservacao();
                    $objArg->classeCss = "input_text";
                    $objArg->classeCssFocus = "focus_text";
                    $objArg->obrigatorio = false;
                    $objArg->largura = 200;
                    $url = "actions.php?class=EXTDAO_Operacao_download_banco_mobile&action=downloadScript&";
                    $url .= Param_Get::ID_OPERACAO_DOWNLOAD_BANCO_MOBILE."=$idODBM";
                    $url .= "&".Param_Get::ID_OPERACAO_SISTEMA_MOBILE."=".$idOsm;
                    
                    $pathArq = $raiz.
                        EXTDAO_Operacao_download_banco_mobile::getPathDiretorio(
                            $objODBM->getOperacao_sistema_mobile_id_INT()).
                        $nomeArquivo;
                    ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $objODBM->imprimirCampoObservacao($objArg); ?></td>
                
                <td class="td_form_label">Script SQL</td>
                <td class="td_form_campo"><a target="_self" href="<?=$url;?>">Download arquivo</a></td>
            </tr>



    </table>

</fieldset>

                <?= $objODBM->getInformacoesDeValidacaoDosCampos(); ?>

<?= $objODBM->getRodapeFormulario(); ?>

