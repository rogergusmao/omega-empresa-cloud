<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: processo_estrutura
    * DATA DE GERAÇÃO:    17.06.2013
    * ARQUIVO:            processo_estrutura.php
    * TABELA MYSQL:       processo_estrutura
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Processo_estrutura();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_processo_estrutura"=>"Adicionar nova processo de análise da estrutura do banco",
    					 "list_processo_estrutura"=>"Listar processo de análise da estrutura do banco");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_processo_estrutura">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar processo de análise da estrutura do banco";

            }
            else{

            	$legend = "Cadastrar processo de análise da estrutura do banco";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_utiliza_copia_projetos_versao_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getUtiliza_copia_projetos_versao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoUtiliza_copia_projetos_versao_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_fluxo_independente_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getFluxo_independente_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoFluxo_independente_BOOLEAN($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Etapas Do Processo De Análise</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM processo_estrutura_caminho
                                    WHERE processo_estrutura_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/processo_estrutura_caminho_relacionamento_processo_estrutura.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("processo_estrutura_caminho", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "processo_estrutura_caminho_relacionamento_processo_estrutura", "div#processo_estrutura_caminho", "Adicionar Etapa Do Processo De Análise") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

