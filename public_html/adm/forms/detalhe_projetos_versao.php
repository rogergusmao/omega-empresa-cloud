<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: projetos_versao
    * DATA DE GERAÇÃO:    02.05.2013
    * ARQUIVO:            projetos_versao.php
    * TABELA MYSQL:       projetos_versao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Projetos_versao();

    $objArg = new Generic_Argument();
    $objArg->disabled= true;
    $objArg->isDisabled = true;
    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = "edit";
    $postar = "actions.php";

    $nextActions = array("edit_gerencia_projetos_versao"=>"Configurar versão do projeto");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_projetos_versao">

    	<?
        $cont=1;
    	

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET(Param_Get::ID_PROJETOS_VERSAO)){

                $id = Helper::GET(Param_Get::ID_PROJETOS_VERSAO);

                $obj->select($id);
                $legend = "Versão do Projeto";

            } else if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Versão do Projeto";
            }
            

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">
            <tr class="tr_form">
                
    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_id;
    			$objArg->valor = $obj->getId();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"> Identificador</td>
    			<td class="td_form_campo"><?=$obj->getId(); ?></td>


            </tr>
        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_projetos_id_INT;
    			$objArg->valor = $obj->getProjetos_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("projetos_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_homologacao_DATETIME;
    			$objArg->valor = $obj->getData_homologacao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_homologacao_DATETIME($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_producao_DATETIME;
    			$objArg->valor = $obj->getData_producao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_producao_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">

    			<td class="td_form_label"></td>
    			<td class="td_form_campo">
    			    
    			</td>


    			

            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>




	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            
            <?
            include("lists/detalhe_projetos_versao_banco_banco.php");
            ?>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

