<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: sistema_projetos_versao_produto
    * DATA DE GERAÇÃO:    24.06.2013
    * ARQUIVO:            sistema_projetos_versao_produto.php
    * TABELA MYSQL:       sistema_projetos_versao_produto
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_projetos_versao_produto();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_sistema_projetos_versao_produto"=>"Adicionar nova versão de projeto do produto",
    					 "list_sistema_projetos_versao_produto"=>"Listar versões dos projetos do produto");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_sistema_projetos_versao_produto">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar versão de projeto do produto";

            }
            else{

            	$legend = "Cadastrar versão de projeto do produto";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        	<input type="hidden" name="data_homologacao_DATETIME" value="<?=$obj->getData_homologacao_DATETIME(); ?>" />
	<input type="hidden" name="data_producao_DATETIME" value="<?=$obj->getData_producao_DATETIME(); ?>" />
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sistema_projetos_versao_id_INT;
    			$objArg->valor = $obj->getSistema_projetos_versao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sistema_projetos_versao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_projetos_versao($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sistema_produto_id_INT;
    			$objArg->valor = $obj->getSistema_produto_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sistema_produto_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_produto($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_prototipo_projetos_versao_banco_banco_id_INT;
    			$objArg->valor = $obj->getPrototipo_projetos_versao_banco_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("prototipo_projetos_versao_banco_banco_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllPrototipo_projetos_versao_banco_banco($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_servidor_projetos_versao_banco_banco_id_INT;
    			$objArg->valor = $obj->getServidor_projetos_versao_banco_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("servidor_projetos_versao_banco_banco_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllServidor_projetos_versao_banco_banco($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_script_estrutura_banco_hom_para_prod_ARQUIVO;
    			$objArg->valor = $obj->getScript_estrutura_banco_hom_para_prod_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoScript_estrutura_banco_hom_para_prod_ARQUIVO($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_script_registros_banco_hom_para_prod_ARQUIVO;
    			$objArg->valor = $obj->getScript_registros_banco_hom_para_prod_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoScript_registros_banco_hom_para_prod_ARQUIVO($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_xml_estrutura_banco_hom_para_prod_ARQUIVO;
    			$objArg->valor = $obj->getXml_estrutura_banco_hom_para_prod_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoXml_estrutura_banco_hom_para_prod_ARQUIVO($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_xml_registros_banco_hom_para_prod_ARQUIVO;
    			$objArg->valor = $obj->getXml_registros_banco_hom_para_prod_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoXml_registros_banco_hom_para_prod_ARQUIVO($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_script_estrutura_banco_hom_ARQUIVO;
    			$objArg->valor = $obj->getScript_estrutura_banco_hom_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoScript_estrutura_banco_hom_ARQUIVO($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_script_estrutura_banco_prod_ARQUIVO;
    			$objArg->valor = $obj->getScript_estrutura_banco_prod_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoScript_estrutura_banco_prod_ARQUIVO($objArg); ?></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Versões Dos Produtos Mobile Do Sistema</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM sistema_projetos_versao_sistema_produto_mobile
                                    WHERE sistema_projetos_versao_produto_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/sistema_projetos_versao_sistema_produto_mobile_relacionamento_sistema_projetos_versao_produto.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("sistema_projetos_versao_sistema_produto_mobile", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "sistema_projetos_versao_sistema_produto_mobile_relacionamento_sistema_projetos_versao_produto", "div#sistema_projetos_versao_sistema_produto_mobile", "Adicionar Versão Do Produto Mobile Do Sistema") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

