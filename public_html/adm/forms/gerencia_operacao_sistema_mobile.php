<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: operacao_sistema_mobile
    * DATA DE GERAÇÃO:    14.05.2013
    * ARQUIVO:            operacao_sistema_mobile.php
    * TABELA MYSQL:       operacao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Operacao_sistema_mobile();

    $objArg = new Generic_Argument();
    $objArg->isDisabled = true;

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_operacao_sistema_mobile"=>"Adicionar nova operação do sistema do telefone",
    					 "list_operacao_sistema_mobile"=>"Listar operações do sistema do telefone");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_operacao_sistema_mobile">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar operação do sistema do telefone";

            }
            else{

            	$legend = "Cadastrar operação do sistema do telefone";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tipo_operacao_sistema_id_INT;
    			$objArg->valor = $obj->getTipo_operacao_sistema_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;
                        
    			$obj->addInfoCampos("tipo_operacao_sistema_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_sistema($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_estado_operacao_sistema_mobile_id_INT;
    			$objArg->valor = $obj->getEstado_operacao_sistema_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("estado_operacao_sistema_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllEstado_operacao_sistema_mobile($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_mobile_identificador_id_INT;
    			$objArg->valor = $obj->getMobile_identificador_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("mobile_identificador_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMobile_identificador($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_abertura_DATETIME;
    			$objArg->valor = $obj->getData_abertura_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_abertura_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_processamento_DATETIME;
    			$objArg->valor = $obj->getData_processamento_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_processamento_DATETIME($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_conclusao_DATETIME;
    			$objArg->valor = $obj->getData_conclusao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_conclusao_DATETIME($objArg); ?></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     
      
            <?
            $idTOS = $obj->getTipo_operacao_sistema_id_INT();
            new EXTDAO_Tipo_operacao_sistema();
            switch ($idTOS) {
                case EXTDAO_Tipo_operacao_sistema::TRANSFERIR_ARQUIVO_BANCO_SQLITE_DO_SERVIDOR_PARA_MOBILE:
                    include('forms/detalhe_operacao_download_banco_mobile.php');
                    break;

                default:
                    break;
            }
            ?>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

