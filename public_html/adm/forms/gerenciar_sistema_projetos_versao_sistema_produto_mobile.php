<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: sistema_projetos_versao_sistema_produto_mobile
    * DATA DE GERAÇÃO:    17.06.2013
    * ARQUIVO:            sistema_projetos_versao_sistema_produto_mobile.php
    * TABELA MYSQL:       sistema_projetos_versao_sistema_produto_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO_MOBILE)?"edit": "add");
    if(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO_MOBILE)){
        $id = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getIdDaUltimaVersao(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO_MOBILE));
    } else if(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO)){
        $idSP = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO);
        $idSPVP = EXTDAO_Sistema_produto::getIdDaUltimaVersao(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO));
        $idSPM = EXTDAO_Sistema_produto_mobile::getSistemaProdutoMobile($idSP);
        $id = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getIdDaUltimaVersao($idSPM);
    }
    $postar = "actions.php";

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_sistema_projetos_versao_sistema_produto_mobile">

    	<?
            $cont=1; 
            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if($id){

                $obj->select($id);
                $legend = "Atualizar versão do produto mobile do sistema";

            }
            else{

            	$legend = "Cadastrar versão do produto mobile do sistema";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

            <tr class="tr_form">


            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_sistema_produto_mobile_id_INT;
            $objArg->valor = $obj->getSistema_produto_mobile_id_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            $obj->addInfoCampos("sistema_produto_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo">
                <?=$obj->getComboBoxAllSistema_produto_mobile($objArg); ?>
            </td>




            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_sistema_projetos_versao_id_INT;
            $objArg->valor = $obj->getSistema_projetos_versao_id_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            $obj->addInfoCampos("sistema_projetos_versao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo">
                <?=$obj->getComboBoxAllSistema_projetos_versao($objArg); ?>
            </td>


                                    </tr>
            <tr class="tr_form">


            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_arquivo_programa;
            $objArg->valor = $obj->getArquivo_programa();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo"><?=$obj->imprimirCampoArquivo_programa($objArg); ?></td>


            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_sistema_projetos_versao_produto_id_INT;
            $objArg->valor = $obj->getSistema_projetos_versao_produto_id_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            $obj->addInfoCampos("sistema_projetos_versao_produto_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo">
                <?=$obj->getComboBoxAllSistema_projetos_versao_produto($objArg); ?>
            </td>


                                    </tr>
            <tr class="tr_form">


            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_version_code;
            $objArg->valor = $obj->getVersion_code();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo"><?=$obj->imprimirCampoVersion_code($objArg); ?></td>


            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_version_name;
            $objArg->valor = $obj->getVersion_name();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo"><?=$obj->imprimirCampoVersion_name($objArg); ?></td>
            </tr>


         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

