<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: mobile_identificador
    * DATA DE GERAÇÃO:    21.10.2013
    * ARQUIVO:            mobile_identificador.php
    * TABELA MYSQL:       mobile_identificador
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Mobile_identificador();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_mobile_identificador"=>"Adicionar novo identificador do telefone",
    					 "list_mobile_identificador"=>"Listar identificadores dos telefones");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_mobile_identificador">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar identificador do telefone";

            }
            else{

            	$legend = "Cadastrar identificador do telefone";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        	<input type="hidden" name="requisicao_modo_suporte_DATETIME" value="<?=$obj->getRequisicao_modo_suporte_DATETIME(); ?>" />
	<input type="hidden" name="requisicao_usuario_id_INT" value="<?=$obj->getRequisicao_usuario_id_INT(); ?>" />
	<input type="hidden" name="mobile_resposta_suporte_DATETIME" value="<?=$obj->getMobile_resposta_suporte_DATETIME(); ?>" />
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_mobile_id_INT;
    			$objArg->valor = $obj->getMobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMobile($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sistema_projetos_versao_sistema_produto_mobile_id_INT;
    			$objArg->valor = $obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sistema_projetos_versao_sistema_produto_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_projetos_versao_sistema_produto_mobile($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_cadastro_DATETIME;
    			$objArg->valor = $obj->getData_cadastro_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_cadastro_DATETIME($objArg); ?></td>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_is_hospedeiro_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getIs_hospedeiro_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoIs_hospedeiro_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_reinstalar_a_versao_id_INT;
    			$objArg->valor = $obj->getReinstalar_a_versao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("reinstalar_a_versao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllReinstalar_a_versao($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_atualizacao_versao_ativa_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getAtualizacao_versao_ativa_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAtualizacao_versao_ativa_BOOLEAN($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Telefones Conectados</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM mobile_conectado
                                    WHERE mobile_identificador_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/mobile_conectado_relacionamento_mobile_identificador.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("mobile_conectado", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "mobile_conectado_relacionamento_mobile_identificador", "div#mobile_conectado", "Adicionar Telefone Conectado") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Operações Do Sistema Do Telefone</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM operacao_sistema_mobile
                                    WHERE mobile_identificador_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/operacao_sistema_mobile_relacionamento_mobile_identificador.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("operacao_sistema_mobile", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "operacao_sistema_mobile_relacionamento_mobile_identificador", "div#operacao_sistema_mobile", "Adicionar Operação Do Sistema Do Telefone") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

