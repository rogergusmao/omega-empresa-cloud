<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: operacao_sistema_mobile
    * DATA DE GERAÇÃO:    14.05.2013
    * ARQUIVO:            operacao_sistema_mobile.php
    * TABELA MYSQL:       operacao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Operacao_sistema_mobile();

    $objArg = new Generic_Argument();
    $objArg->isDisabled = true;
    $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);
    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = "addOperacaoMonitorarTela";
    $postar = "actions.php";
//
//    $sql = "SELECT osm.id"
//            . " FROM operacao_sistema_mobile osm JOIN mobile_identificador mi ON osm.mobile_identificador_id_INT = mi.id"
//            . " WHERE mi.id = $idMI "
//            . " AND osm.tipo_operacao_sistema_id_INT = ".EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_MONITORAR_TELA." "
//            . " AND osm.estado_operacao_sistema_mobile_id_INT in (".EXTDAO_Estado_operacao_sistema_mobile::AGUARDANDO.", " .EXTDAO_Estado_operacao_sistema_mobile::PROCESSANDO.")"
//            . " LIMIT 0,1 ";
//    $db = new Database();
//    $db->query($sql);
//    $idOSM = $db->getPrimeiraTuplaDoResultSet(0);
//    if(strlen($idOSM)){
//        include("pages/monitorar_tela.php");
//        
//    } else{
//    
    
        ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_operacao_sistema_mobile">
        <input type="hidden" name="<?=  Param_Get::ID_MOBILE_IDENTIFICADOR;?>" id="<?=  Param_Get::ID_MOBILE_IDENTIFICADOR;?>" value="<?=$idMI; ?>">
    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            $legend = "Monitorar Tela do Telefone";
            
            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tipo_operacao_sistema_id_INT;
    			$objArg->valor = EXTDAO_Tipo_operacao_sistema_mobile::OPERACAO_MONITORAR_TELA;
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;
                        $objArg->disabled = true;
    			$obj->addInfoCampos("tipo_operacao_sistema_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_sistema($objArg); ?>
    			</td>





    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_mobile_identificador_id_INT;
    			$objArg->valor = $idMI;
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;
                        $objArg->disabled = true;
    			$obj->addInfoCampos("mobile_identificador_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMobile_identificador($objArg); ?>
    			</td>


    			

			</tr>
			


     	 <? } ?>

        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     
      
        

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); 
        
//        }
        
        ?>

