<?php
$objArg = new Generic_Argument();

/*
 *
 * -------------------------------------------------------
 * NOME DO FORMULÁRIO: operacao_download_banco_mobile
 * DATA DE GERAÇ?O:    14.05.2013
 * ARQUIVO:            operacao_download_banco_mobile.php
 * TABELA MYSQL:       operacao_download_banco_mobile
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Mobile_identificador();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = "reinstalarVersaoAppMobile";
$idMI = Helper::GET(Param_Get::ID_MOBILE_IDENTIFICADOR);
$postar = "actions.php";
?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="reinstalarVersaoAppMobile">

<?


    if (Helper::SESSION("erro")) {

        unset($_SESSION["erro"]);

        $obj->setBySession();
    }

    

    $obj->select($idMI);
    $legend = "Reinstalar Versão";

    $obj->formatarParaExibicao();
    ?>

    
    <input type="hidden" name="<?=  Param_Get::ID_MOBILE_IDENTIFICADOR;?>" id="<?= Param_Get::ID_MOBILE_IDENTIFICADOR;?>" value="<?= $idMI; ?>">
    <fieldset class="fieldset_form">
        <legend class="legend_form"><?= $legend; ?></legend>

        <table class="tabela_form">

           
            <tr class="tr_form">

    			<?
                        $objSPVSPM = new EXTDAO_Sistema_projetos_versao_sistema_produto_mobile();
                        $idSPVSPM = $obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT();
                        $objSPVSPM->select($idSPVSPM);
                        $versoes = EXTDAO_Sistema_projetos_versao_sistema_produto_mobile::getListaVersao($objSPVSPM->getSistema_projetos_versao_produto_id_INT(), null);
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_reinstalar_a_versao_id_INT;
    			$objArg->valor = $obj->getReinstalar_a_versao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
                        $objArg->onChange = "onChangeVersaoParaReinstalar();";
    			$obj->addInfoCampos("reinstalar_versao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>
                        <td class="td_form_label">Vers?o atual</td>
                        <td class="td_form_campo"><?=$idSPVSPM;?></td>
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo" colspan="3">
    			    <?=$obj->getComboBoxAllReinstalar_a_versao($objArg); ?>
                            &nbsp;
                        <a id="link_download_apk" style="visibility: hidden" href="actions.php?class=Servico_mobile&action=downloadApkAndroid&sistema_projetos_versao_sistema_produto_mobile=8">
                    Download Apk
                    </a>
                        </td>

            
            </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

<?= Helper::getBarraDeBotoesDoFormulario(false, true,true, "Reinstalar", false ); ?>

            </td>
        </tr>
    </table>

</fieldset>

                <?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>

