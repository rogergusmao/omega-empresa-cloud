<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: tabela
    * DATA DE GERAÇÃO:    05.02.2013
    * ARQUIVO:            tabela.php
    * TABELA MYSQL:       tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tabela_tabela();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    
    
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    
    

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_tabela">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar tabela do projeto";

            }
            else{

            	$legend = "Cadastrar tabela do projeto";

            }

            $obj->formatarParaExibicao();
            
            $objHomologacao = $obj->getObjTabelaHomologacao();
            if($objHomologacao == null)
                $objHomologacao = new EXTDAO_Tabela();
            
            $objProducao = $obj->getObjTabelaProducao();
            if($objProducao == null)
                $objProducao = new EXTDAO_Tabela();
            $objCompara = $obj->comparaTabelaHomologacaoComProducao();
            $fatoresDif = $objCompara["fatores"];
            $fatoresDifTipoOperacao = $objCompara["fatores_tipo_operacao"];
            $classTr = "";
            $classTrPar = null;
            $classTrImpar = null;
            $tipoModificacao = $objCompara["tipo_modificacao"];
            if($tipoModificacao != EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
                $classTrPar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr($tipoModificacao, 0);
                $classTrImpar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr($tipoModificacao, 1);
            }
            else{
                $classTrPar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION, 0);
                $classTrImpar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION, 1);
            }
            
            //Desabilitando as caixas de texto e o checkbox
            $objArg->isDisabled = true;
            $objArg->disabled = true;
    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form">Comparação Tabela de Homologação Relacionada a de Produção</legend>

        <table class="tabela_form">
                
                <colgroup>
                        <col width="25%" />
                        <col width="25%" />
                        <col width="25%" />
                        <col width="25%" />
                </colgroup>
                <thead>
                <tr class="tr_list_titulos">

                    <td class="td_list_titulos">Propriedade</td>
                    <td class="td_list_titulos">Homologação</td>
                    <td class="td_list_titulos">Produção</td>
                    <td class="td_list_titulos">Avisos</td>

                </tr>
                     
                <?
                if(in_array(EXTDAO_Tabela_tabela::FATOR_NOME, $fatoresDif)){
                    $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                            $fatoresDifTipoOperacao[EXTDAO_Tabela_tabela::FATOR_NOME], 
                            0);
                } else $classTr = $classTrPar;
                ?>
        	<tr class="<?=$classTr;?>">
                        

    			<?
                        
                
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $objHomologacao->label_nome;
    			$objArg->valor = $objHomologacao->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $objProducao->label_nome;
    			$objArg->valor = $objProducao->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_campo"><?=$objProducao->imprimirCampoNome($objArg); ?></td>
                        <td class="td_form_campo"></td>
                    </tr>
                    


     	 <? } ?>

        
	</table>

     </fieldset>
        <?
        
        $pObjTabelaHomologacao= $obj->getObjTabelaHomologacao();
        $pObjTabelaProducao = $obj->getObjTabelaProducao();
        
        
        $chavesDeletar = EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveRemovida($idPVBB, $pObjTabelaProducao);
        $chavesInserir = EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveInserida($idPVBB, $pObjTabelaHomologacao) ;
        
        $chavesIguais = EXTDAO_Tabela_chave_tabela_chave::getListaIdChaveIgual($idPVBB, $pObjTabelaHomologacao, $pObjTabelaProducao) ;
        $objHomologacao = new EXTDAO_Tabela_chave();
        $objProducao = new EXTDAO_Tabela_chave();
        $tipoAtualizacao = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION;
//        echo "passou 1<br/>";
        if($chavesDeletar != null)
        foreach ($chavesDeletar as $prod) {
            $objProducao->select($prod);
            include("forms/compara_tabela_chave_hom_com_prod.php");
        }
//        echo "passou 2<br/>";
        $objHomologacao = new EXTDAO_Tabela_chave();
        $objProducao = new EXTDAO_Tabela_chave();
        if($chavesInserir != null)
        foreach ($chavesInserir as $hom ) {
            $objHomologacao->select($hom);
            include("forms/compara_tabela_chave_hom_com_prod.php");
        }
//        echo "passou 3<br/>";
        $objHomologacao = new EXTDAO_Tabela_chave();
        $objProducao = new EXTDAO_Tabela_chave();
        if($chavesIguais != null)
        foreach ($chavesIguais as $hom => $prod) {
            $objHomologacao->select($hom);
            $objProducao->select($prod);
            include("forms/compara_tabela_chave_hom_com_prod.php");
        }
        
        
        
        
        ?>
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

