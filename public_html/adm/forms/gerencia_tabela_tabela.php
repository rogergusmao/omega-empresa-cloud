<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: tabela_tabela
    * DATA DE GERAÇÃO:    30.01.2013
    * ARQUIVO:            tabela_tabela.php
    * TABELA MYSQL:       tabela_tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tabela_tabela();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";
    
    $idTT = Helper::POSTGET(Param_Get::TABELA_TABELA);
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    
    
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    $varGET .= "&".Param_Get::TABELA_TABELA."=".$idTT;
    
    $nextActions = array("list_gerencia_tabela_tabela"=>"Listar relacionamentos entre tabelas do banco de homologação e produção");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>gerencia_tabela_tabela">
        
        <input type="hidden" name="tabela_tabela_id_INT" id="tabela_tabela_id_INT" value="<?=$idTT;?>">
        <?=EXTDAO_Projetos_versao::imprimiCabecalhoIdentificador();?>
        

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar relacionamento entre tabela do banco de homologação e produção";

            }
            else{

            	$legend = "Cadastrar relacionamento entre tabela do banco de homologação e produção";

            }

            $obj->formatarParaExibicao();

    	?>
        <input type="hidden" name="projetos_versao_id_INT<?=$cont ?>" id="projetos_versao_id_INT<?=$cont ?>" value="<?=$obj->getProjetos_versao_id_INT();?>">
    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">
        <?=  EXTDAO_Projetos_versao::imprimiCabecalhoIdentificador();?>
    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_homologacao_tabela_id_INT;
    			$objArg->valor = $obj->getHomologacao_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("homologacao_tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllHomologacao_tabela($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_producao_tabela_id_INT;
    			$objArg->valor = $obj->getProducao_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("producao_tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProducao_tabela($objArg); ?>
    			</td>


    			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tipo_operacao_atualizacao_banco_id_INT;
    			$objArg->valor = $obj->getTipo_operacao_atualizacao_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("tipo_operacao_atualizacao_banco_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_atualizacao_banco($objArg); ?>
    			</td>



    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_status_verificacao_BOOLEAN;
    			$objArg->labelTrue = "Ativo";
    			$objArg->labelFalse = "Inativo";
    			$objArg->valor = $obj->getStatus_verificacao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoStatus_verificacao_BOOLEAN($objArg); ?></td>


    			


    			</tr>
			


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

