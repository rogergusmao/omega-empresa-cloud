<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: atributo
    * DATA DE GERAÇÃO:    31.01.2013
    * ARQUIVO:            atributo.php
    * TABELA MYSQL:       atributo
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Atributo_atributo();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = Helper::GET("id1");
    $postar = "actions.php";
    
    $idTabelaTabela = Helper::POSTGET(Param_Get::TABELA_TABELA);
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    
    
    $varGET = Param_Get::getIdentificadorProjetosVersao();
    
    
    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_atributo">

    	<?
        $tipoAtuBancoAssocFator = EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT;
    	for($cont=1; $cont <= $numeroRegistros; $cont++){
            $i = $cont;
            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar atributo do projeto";

            }
            else{

            	$legend = "Cadastrar atributo do projeto";

            }
//             return array("tipo_modificacao" => $vTipoModificacao,
//                "fatores" => $vFatores,
//                "atributo_atributo_fk" => $vAtributoAtributoFK);
            $objCompara = $obj->comparaAtributoHomologacaoComProducao($idPVBB);
            $fatoresDif = $objCompara["fatores"];
            $fatoresDifTipoOperacao = $objCompara["fatores_tipo_operacao"];
            $objAAFK = null;
            if(strlen($objCompara["atributo_atributo_fk"])){
                $objAAFK = new EXTDAO_Atributo_atributo();
                $objAAFK->select($objCompara["atributo_atributo_fk"]);
            }
            $classTrPar = null;
            $classTrImpar = null;
            $tipoModificacao = $objCompara["tipo_modificacao"];
            if($tipoModificacao != EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_EDIT){
                $classTrPar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr($tipoModificacao, 0);
                $classTrImpar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr($tipoModificacao, 1);
            }
            else{
                $classTrPar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION, 0);
                $classTrImpar = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_NO_MODIFICATION, 1);
            }
            $classTr = "";
            
            
            $objHomologacao = new EXTDAO_Atributo();
            if(strlen($obj->getHomologacao_atributo_id_INT()))
                $objHomologacao->select($obj->getHomologacao_atributo_id_INT());
            
            $objProducao = new EXTDAO_Atributo();
            if(strlen($obj->getProducao_atributo_id_INT()))
                $objProducao->select($obj->getProducao_atributo_id_INT());
            
            $obj->formatarParaExibicao();
            
            $objArg->isDisabled = true;
            $objArg->disabled = true;
            
    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form">Comparação Atributo de Homologação Relacionado ao de Produção</legend>

                <table class="tabela_form">
                        <colgroup>
                                <col width="20%" />
                                <col width="20%" />
                                <col width="20%" />
                                <col width="20%" />
                                <col width="20%" />
                        </colgroup>
                <thead>
                        <tr class="tr_list_titulos">

                            <td class="td_list_titulos">Propriedade</td>
                            <td class="td_list_titulos">Homologação</td>
                            <td class="td_list_titulos">Produção</td>
                            <td class="td_list_titulos">Avisos</td>

                        </tr>
                        
                        
                        <?
                //Desabilitando as caixas de texto e o checkbox
             
                
                $classTr = $classTrImpar;
    			?>
    			
                <tr class="td_list_conteudo">
    			<td class="td_list_titulos"><?=$objHomologacao->label_id ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->getId(); ?></td>
                        <td class="td_form_campo"><?=$objProducao->getId(); ?></td>
                        <td ></td>
                  </tr>
                <tr class="td_form_campo">


    			<?
                        
    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_tabela_id_INT;
    			$objArg->valor = $objHomologacao->getTabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objHomologacao->getComboBoxAllTabela($objArg); ?>
    			</td>
                        
                        

    			<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_tabela_id_INT;
    			$objArg->valor = $objProducao->getTabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			
    			<td class="td_form_campo">
    			    <?=$objProducao->getComboBoxAllTabela($objArg); ?>
    			</td>
                        <td class="td_form_campo">O nome da tabela não é relevante como alteração</td>
                </tr>
                        
    			<?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_NOME, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NOME], 
                                    0);
                        } else $classTr = $classTrPar;
    			?>
    			
                <tr class="<?=$classTr;?>">


    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_nome;
    			$objArg->valor = $objHomologacao->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
                        $objArg->disabled = true;
    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoNome($objArg); ?></td>



    			<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_nome;
    			$objArg->valor = $objProducao->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoNome($objArg); ?></td>
                        <td ></td>
                  </tr>
                        <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_TIPO_SQL, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TIPO_SQL], 
                                    1);
                        } else $classTr = $classTrImpar;
    			
                        ?>
                  <tr class="<?=$classTr;?>">
    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_tipo_sql;
    			$objArg->valor = $objHomologacao->getTipo_sql();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>
    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoTipo_sql($objArg); ?></td>
                        
                        <?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_tipo_sql;
    			$objArg->valor = $objProducao->getTipo_sql();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoTipo_sql($objArg); ?></td>
                     <td ></td>
                     </tr>
                        <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_TAMANHO, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_TAMANHO], 
                                    0);
                        } else $classTr = $classTrPar;
                        ?>
                        <tr class="<?=$classTr;?>">
    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_tamanho_INT;
    			$objArg->valor = $objHomologacao->getTamanho_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>
                        
    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoTamanho_INT($objArg); ?></td>
                        
			<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_tamanho_INT;
    			$objArg->valor = $objProducao->getTamanho_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>
                        
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoTamanho_INT($objArg); ?></td>
			<td ></td>
                    </tr>
                        <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_DECIMAL , $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DECIMAL], 
                                    1);
                        } else $classTr = $classTrImpar;

                        ?>
                    <tr class="<?=$classTr;?>">


    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_decimal_INT;
    			$objArg->valor = $objHomologacao->getDecimal_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoDecimal_INT($objArg); ?></td>

    			<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_decimal_INT;
    			$objArg->valor = $objProducao->getDecimal_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoDecimal_INT($objArg); ?></td>
                        <td ></td>
                    </tr>
                        <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_NAO_NULO , $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_NAO_NULO], 
                                    0);
                        } else $classTr = $classTrPar;

                        ?>
                    <tr class="<?=$classTr;?>">
    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_not_null_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $objHomologacao->getNot_null_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>
                        
    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoNot_null_BOOLEAN($objArg); ?></td>
                        
                        <?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_not_null_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $objProducao->getNot_null_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>
                        
    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoNot_null_BOOLEAN($objArg); ?></td>
                        
                        <td ></td>
                        
                    </tr>
                        
			<?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_CHAVE_PRIMARIA, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_PRIMARIA], 
                                    1);
                        } else $classTr = $classTrImpar;

                        ?>
                    <tr class="<?=$classTr;?>">


    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_primary_key_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $objHomologacao->getPrimary_key_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoPrimary_key_BOOLEAN($objArg); ?></td>
                        
<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_primary_key_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $objProducao->getPrimary_key_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoPrimary_key_BOOLEAN($objArg); ?></td>
                        <td></td>
                    </tr>
                        <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_AUTO_INCREMENTO, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_AUTO_INCREMENTO], 
                                    0);
                        } else $classTr = $classTrPar;

                        ?>
                    <tr class="<?=$classTr;?>">
    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_auto_increment_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $objHomologacao->getAuto_increment_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>
                        
    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoAuto_increment_BOOLEAN($objArg); ?></td>
                        
                        <?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_auto_increment_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $objProducao->getAuto_increment_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>
                        
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoAuto_increment_BOOLEAN($objArg); ?></td>
                        <td ></td>
                    </tr>
			<?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_DEFAULT, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_DEFAULT], 
                                    1);
                        } else $classTr = $classTrImpar;

                        ?>
                    <tr class="<?=$classTr;?>">


    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_valor_default;
    			$objArg->valor = $objHomologacao->getValor_default();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoValor_default($objArg); ?></td>
                        
                        
    			<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_valor_default;
    			$objArg->valor = $objProducao->getValor_default();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoValor_default($objArg); ?></td>
                        <td ></td>
                    </tr>
                        <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA], 
                                    0);
                        } else $classTr = $classTrPar;

                        ?>
                    <tr class="<?=$classTr;?>">


    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_fk_nome;
    			$objArg->valor = $objHomologacao->getFk_nome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
                        <td class="td_form_campo"><?=$objHomologacao->imprimirCampoFk_nome($objArg); ?></td>
                        
                        
    			<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_fk_nome;
    			$objArg->valor = $objProducao->getFk_nome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoFk_nome($objArg); ?></td>
                        <td ></td>
                    </tr>
                    <tr class="<?=$classTr;?>">

    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_fk_tabela_id_INT;
    			$objArg->valor = $objHomologacao->getFk_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$objHomologacao->addInfoCampos("fk_tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>
                       
    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                            Id:&nbsp;<?=$objHomologacao->getFk_tabela_id_INT(); ?>&nbsp
    			    <?=$objHomologacao->getComboBoxAllFk_tabela($objArg); ?>
    			</td>
                        
                        
<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_fk_tabela_id_INT;
    			$objArg->valor = $objProducao->getFk_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$objProducao->addInfoCampos("fk_tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>
                       
    			
    			<td class="td_form_campo">
                            Id:&nbsp;<?=$objProducao->getFk_tabela_id_INT(); ?>&nbsp
    			    <?=$objProducao->getComboBoxAllFk_tabela($objArg); ?>
    			</td>
                        <td >
                            <?
                            if($objAAFK != null){
                            ?>    
                            
                            <a href="index.php?tipo=forms&page=compara_tabela_hom_tabela_prod&<?=$varGET?>&id1=<?=$objAAFK->getTabela_tabela_id_INT()?>" target="_self" >
                                Comparação Tabela Homologação e Produção
                                </a>
                            <?
                            }
                            ?>
                            
                        </td>
                    </tr>
			 <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_CHAVE_EXTRANGEIRA], 
                                    1);
                        } else $classTr = $classTrImpar;

                        
                        ?>
                    
                    <tr class="<?=$classTr;?>">


    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_fk_atributo_id_INT;
    			$objArg->valor = $objHomologacao->getFk_atributo_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$objHomologacao->addInfoCampos("fk_atributo_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                            Id:&nbsp;<?=$objHomologacao->getFk_atributo_id_INT(); ?>&nbsp
    			    <?=$objHomologacao->getComboBoxAllFk_atributo($objArg); ?>
    			</td>
                        
<?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_fk_atributo_id_INT;
    			$objArg->valor = $objProducao->getFk_atributo_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$objProducao->addInfoCampos("fk_atributo_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			
    			<td class="td_form_campo">
                            Id:&nbsp;<?=$objProducao->getFk_atributo_id_INT(); ?>&nbsp
    			    <?=$objProducao->getComboBoxAllFk_atributo($objArg); ?>
    			</td>
                        <td >
                            
                            <?
                            if($objAAFK != null){
                            ?>    
                            
                                <a href="index.php?tipo=forms&page=compara_atributo_hom_atributo_prod&<?=$varGET?>&id1=<?=$objAAFK->getId()?>" target="_self" >
                                Comparação Atributo Homologação e Produção
                                </a>
                            <?
                            }
                            ?>
                            
                        </td>
                    </tr>
                         <?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_ON_UPDATE, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_UPDATE], 
                                    0);
                        } else $classTr = $classTrPar;

                        ?>
                    <tr class="<?=$classTr;?>">

    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_update_tipo_fk;
    			$objArg->valor = $objHomologacao->getUpdate_tipo_fk();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>
                        
    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoUpdate_tipo_fk($objArg); ?></td>
                        
                        <?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_update_tipo_fk;
    			$objArg->valor = $objProducao->getUpdate_tipo_fk();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>
                        
    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoUpdate_tipo_fk($objArg); ?></td>
                        
                        <td ></td>
                    </tr>
			<?
                        if(in_array(EXTDAO_Atributo_atributo::FATOR_ON_DELETE, $fatoresDif)){
                            $classTr = EXTDAO_Tipo_operacao_atualizacao_banco::getClassTr(
                                    $fatoresDifTipoOperacao[EXTDAO_Atributo_atributo::FATOR_ON_DELETE], 
                                    1);
                        } else $classTr = $classTrImpar;

                        ?>
                    <tr class="<?=$classTr;?>">



    			<?

    			$objArg->numeroDoRegistro= 1;
    			$objArg->label = $objHomologacao->label_delete_tipo_fk;
    			$objArg->valor = $objHomologacao->getDelete_tipo_fk();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_list_titulos"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$objHomologacao->imprimirCampoDelete_tipo_fk($objArg); ?></td>
                        
                        <?

    			$objArg->numeroDoRegistro= 2;
    			$objArg->label = $objProducao->label_delete_tipo_fk;
    			$objArg->valor = $objProducao->getDelete_tipo_fk();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			
    			<td class="td_form_campo"><?=$objProducao->imprimirCampoDelete_tipo_fk($objArg); ?></td>
                        <td ></td>
                    </tr>
                       
		
                
     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

