<?php //@@NAO_MODIFICAR
$objArg = new Generic_Argument();

    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: uf
    * DATA DE GERAÇÃO:    20.10.2009
    * ARQUIVO:            uf.php
    * TABELA MYSQL:       uf
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    * GERADOR DE FORMULÁRIOS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Uf();
    
    $objArg = new Generic_Argument();
    
    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";    
    
    $nextActions = array("add_uf"=>"Adicionar novo estado", 
    					 "list_uf"=>"Listar estados");

    ?>
    
    <?=$obj->getCabecalhoFormulario($postar); ?>
    
        <input type="hidden" name="junk" id="junk" value="junk">
		<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_uf">
        
    	<? 
    	
    	for($cont=1; $cont <= $numeroRegistros; $cont++){ 
    	
            if(Helper::SESSION("erro")){
        
                unset($_SESSION["erro"]);
            
               $obj->setBySession();
                
            }
            
            if(Helper::GET("id{$cont}")){
                
                $id = Helper::GET("id{$cont}");
                
                $obj->select($id);
                
            }

            $obj->formatarParaExibicao();
    	
    	?>
    	
    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">
    	
        <table class="tabela_form">
        
            <tr class="tr_form">

        		
                <?

                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_nome;
                $objArg->valor = $obj->getNome();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;

                ?>

                <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                <td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


                <?

                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_sigla;
                $objArg->valor = $obj->getSigla();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;

                ?>

                <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                <td class="td_form_campo"><?=$obj->imprimirCampoSigla($objArg); ?></td>
            </tr>

     
     	 <? } ?> 
     
         <tr class="tr_form_rodape1">
        	<td colspan="4">
        	
        	    <?=Helper::getBarraDaNextAction($nextActions); ?>
        
        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >
        		
        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>
        
        	</td>
        </tr>
	</table>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    
