<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: sistema_log_identificador
    * DATA DE GERAÇÃO:    25.05.2013
    * ARQUIVO:            sistema_log_identificador.php
    * TABELA MYSQL:       sistema_log_identificador
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_log_identificador();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = "edit";
    $postar = "actions.php";

    $nextActions = array("edit_gerencia_sistema_log_identificador"=>"Detalhes log");
    $cont = 1;
    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_sistema_log_identificador">

    	<?

    	

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            $id = Helper::POSTGET("id1");
            $obj->select($id);
            $obj->formatarParaExibicao();
    	?>

    	<input type="hidden" name="id1" id="id1" value="<?=$obj->getId(); ?>">
        <input type="hidden" name="id" id="id" value="<?=$obj->getId(); ?>">

        <?
        $corrigida = $obj->getCorrigida_BOOLEAN() ? true : false;
        if($corrigida){
        ?>
        
    	<fieldset class="fieldset_form">
            <legend class="legend_form">Dados Correção Efetuada</legend>
            

        <table class="tabela_form">

                        
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_corrigida_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getCorrigida_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><div><?=$obj->getCorrigida_BOOLEAN()?"SIM" : "NÃO"; ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_correcao_DATETIME;
    			$objArg->valor = $obj->getData_correcao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_correcao_DATETIME($objArg); ?></td>
			</tr>
                        <tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_corrigida_pelo_usuario_id_INT;
    			$objArg->valor = $obj->getCorrigida_pelo_usuario_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("corrigida_pelo_usuario_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllCorrigida_pelo_usuario($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sistema_projetos_versao_id_INT;
    			$objArg->valor = $obj->getSistema_projetos_versao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sistema_projetos_versao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_projetos_versao($objArg); ?>
    			</td>


                        </tr>
                        </table>
            </fieldset>
        <?
        }
        ?>
            <fieldset class="fieldset_form">
            <legend class="legend_form">Log</legend>

                <table class="tabela_form">
			
                        <tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_identificador_erro;
    			
    			$valor = $obj->getIdentificador_erro();

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
                        <td class="td_form_campo" colspan="3"><?="<div >$valor</div>"; ?></td>
                        </tr>
			<tr class="tr_form">

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_descricao;
    			$valor = $obj->getDescricao();
    			
                        
                        
    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo" colspan="3"><?="<div >$valor</div>"; ?></td>
			</tr>

     	 <? 
            
         ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>
    <fieldset class="fieldset_form">
        
        <?
        include("lists/gerencia_sistema_produto_log_erro_item.php");
        ?>
        
    </fieldset>
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

        
        <?
        
        EXTDAO_Sistema_log_identificador::atualizaEstadoParaVisualizada($id);
        ?>

