<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: cidade
    * DATA DE GERAÇÃO:    07.08.2011
    * ARQUIVO:            cidade.php
    * TABELA MYSQL:       cidade
    * BANCO DE DADOS:     websaude
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Cidade();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_cidade"=>"Adicionar novo cidade",
    					 "list_cidade"=>"Listar cidades");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_cidade">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar cidade";

            }
            else{

            	$legend = "Cadastrar cidade";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_uf_id_INT;
    			$objArg->valor = $obj->getUf_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("uf_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllUf($objArg); ?>
    			</td>


    						</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Bairros</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM bairro
                                    WHERE cidade_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/bairro_relacionamento_cidade.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("bairro", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "bairro_relacionamento_cidade", "div#bairro", "Adicionar Bairro") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Ups</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM ups
                                    WHERE cidade_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/ups_relacionamento_cidade.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("ups", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "ups_relacionamento_cidade", "div#ups", "Adicionar Ups") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

