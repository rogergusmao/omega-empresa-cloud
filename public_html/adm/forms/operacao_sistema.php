<?php
$objArg = new Generic_Argument();

    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: operacao_sistema
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            operacao_sistema.php
    * TABELA MYSQL:       operacao_sistema
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    * GERENCIADOR DE FORMULÁRIOS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Operacao_sistema();
    
    $objArg = new Generic_Argument();
    
    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";    
    
    $nextActions = array("add_operacao_sistema"=>"Adicionar nova operação no sistema", 
    					 "list_operacao_sistema"=>"Listar operações no sistema");

    ?>
    
    <?=$obj->getCabecalhoFormulario($postar); ?>
    
        <input type="hidden" name="junk" id="junk" value="junk">
		<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_operacao_sistema">
        
    	<? 
    	
    	for($cont=1; $cont <= $numeroRegistros; $cont++){ 
    	
            if(Helper::SESSION("erro")){
        
                unset($_SESSION["erro"]);
            
               $obj->setBySession();
                
            }
            
            if(Helper::GET("id{$cont}")){
                
                $id = Helper::GET("id{$cont}");
                
                $obj->select($id);
                
            }

            $obj->formatarParaExibicao();
    	
    	?>
    	
    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">
    	
        <table class="tabela_form">
        
        			<tr class="tr_form">

        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_usuario_id_INT;
    			$objArg->valor = $obj->getUsuario_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;
    			
    			$obj->addInfoCampos("usuario_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
	
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllUsuario($objArg); ?>
    			</td>


    			
        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tipo_operacao;
    			$objArg->valor = $obj->getTipo_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTipo_operacao($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_pagina_operacao;
    			$objArg->valor = $obj->getPagina_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoPagina_operacao($objArg); ?></td>

        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_entidade_operacao;
    			$objArg->valor = $obj->getEntidade_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoEntidade_operacao($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_chave_registro_operacao_INT;
    			$objArg->valor = $obj->getChave_registro_operacao_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoChave_registro_operacao_INT($objArg); ?></td>

        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_descricao_operacao;
    			$objArg->valor = $obj->getDescricao_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoDescricao_operacao($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_operacao_DATETIME;
    			$objArg->valor = $obj->getData_operacao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_operacao_DATETIME($objArg); ?></td>

                
            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

     
     	 <? } ?> 
     
         <tr class="tr_form_rodape1">
        	<td colspan="4">
        	
        	    <?=Helper::getBarraDaNextAction($nextActions); ?>
        
        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >
        		
        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>
        
        	</td>
        </tr>
	</table>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    
