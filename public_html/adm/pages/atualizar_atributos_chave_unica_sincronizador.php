<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of atualizar_atributos_chave_unica_sincronizador
 *
 * @author home
 */


$idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

$varGET= Param_Get::getIdentificadorProjetosVersao();
$db = new Database();
$objPVBB = new EXTDAO_Projetos_versao_banco_banco($db);
$objPVBB->select($idPVBB);
$idPV= $objPVBB->getProjetos_versao_id_INT();
$objPV = new EXTDAO_Projetos_versao($db);
$objPV->select($idPV);
$idProjeto = $objPV->getProjetos_id_INT();
$database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBB, $db);
$idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBB, $db);

$ret = EXTDAO_Trunk::getTipoAnalisePorDadosVersaoDoTrunk(
    $objPV->getProjetos_id_INT(),
   EXTDAO_Tipo_conjunto_analise::Projeto_Android_Web_Sincronizado,
   EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB,
    EXTDAO_Tipo_banco::$TIPO_BANCO_WEB,
    $db);

$dbSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($ret[1], $db);

$database->query("SHOW TABLES");
$tabelas = Helper::getResultSetToArrayDeUmCampo($database->result);

for($i = 0 ;$i < count($tabelas); $i++){
    $objTabela = new EXTDAO_Tabela($db);
    $table = $tabelas[$i];
    $idTabela = EXTDAO_Tabela::existeTabela($table, $idBanco, $idPV, $db);
    if(!strlen($idTabela)){
        Helper::imprimirMensagem ("[$table] Tabela inexistente no banco de dados, atualize os dados do projeto no Biblioteca Nuvem", MENSAGEM_ERRO);
    }
    $objTabela->select($idTabela);
    
    $vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($objTabela->getId(), $db);
    $idTabelaChave = EXTDAO_Tabela_chave::getIdDaChaveUnicaDaTabela($idTabela, $db);
    
    if(strlen($idTabelaChave))
        $vetorAtributoChaveUnica = EXTDAO_Tabela_chave_atributo::getListaIdOrdenadoAtributoDaChave($idTabelaChave, $db);
    
     $strChaveUnica = "";
    if(count($vetorAtributoChaveUnica) > 0){
        
        //public void setUniqueKey(String pVetorAttributeName[])
        $objAtributo = new EXTDAO_Atributo($db);
        for($j = 0 ; $j < count($vetorAtributoChaveUnica); $j++){
            $objAtributo->select($vetorAtributoChaveUnica[$j]);
            $nomeAtributo = $objAtributo->getNome();
            

            if($j > 0)$strChaveUnica .= ", ";
            $strChaveUnica .= $nomeAtributo;
        }
    }
    
    if(strlen($strChaveUnica)){
        $q = "UPDATE sistema_tabela SET atributos_chave_unica = '$strChaveUnica' WHERE nome LIKE '$table'";
        $dbSincronizadorWeb->query($q);
        echo $q;
        Helper::imprimirMensagem("[$table] Chaves atualizadas com sucesso", MENSAGEM_OK);
    } else{
        Helper::imprimirMensagem("[$table] A tabela n�o possui chaves unicas, isso � um problema para a solucao de duplicaidades durante a sincronizacao.", 
            MENSAGEM_WARNING);
    }
}

