<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    
    if(empty($vetorId)){

       $vVetor = $objPVBB->getVetorTabelaTabela();
       $vetorId = $vVetor;
       $indiceAtual = 0;
    }

    //$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
    $objPE = $objPVPE->getObjProcessoEstrutura();
    $json = $objPE->getJson_constante();
    $idTipoBanco = null;
    $idTipoScriptBanco = null;
    if(strlen($json)){
        $objJson = json_decode($json);    
        $idTipoScriptBanco =  $objJson->idTipoScriptBanco;
    } else {
        $idTipoScriptBanco = EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD;
    }
    
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB, 
        $idTipoScriptBanco);
    $prefixo = "";
    if($objPVBB->getFkObjProjetos_versao()->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB)
        $prefixo = "__";
    $tipoBanco = null;
    
    if($objPE->getId() == EXTDAO_Processo_estrutura::GERAR_SCRIPT_MYSQL_DE_ATUALIZACAO_DA_ESTRUTURA_DO_BANCO_DE_PRODUCAO)
        $tipoBanco = EXTDAO_Tipo_banco::$TIPO_BANCO_WEB;
    
    if($indiceAtual < count($vetorId)){
        for($i = 0 ; $i < 20; $i++){

            if($indiceAtual < count($vetorId)){
                $vTabelaTabela = $vetorId[$indiceAtual];
                if(strlen($vTabelaTabela)){
                    $objGSBB->geraScriptAtualizaEstruturaTabela($vTabelaTabela, $prefixo, $tipoBanco);
                }
            }

            if($indiceAtual >= count($vetorId)){

                break;
            } else{
                $indiceAtual += 1;
            }
        }
    }

    if($indiceAtual >= count($vetorId)){
        $arqXML = $objGSBB->arquivarXML();

        $arqScript = $objGSBB->arquivarScriptSQL();
        
        $objPVBB->setScript_estrutura_banco_hom_para_prod_ARQUIVO($arqScript);
        $objPVBB->setXml_estrutura_banco_hom_para_prod_ARQUIVO($arqXML);

        $objPVBB->formatarParaSQL();
        $objPVBB->update($objPVBB->getId());
    } 

    Helper::imprimirMensagem($indiceAtual." relacionamento(s) entre tabelas mapeados até o momento.");
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	
