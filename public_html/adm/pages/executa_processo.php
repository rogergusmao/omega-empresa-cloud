<?php

/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos
 * NOME DA CLASSE DAO: DAO_Projetos
 * DATA DE GERA��O:    09.01.2013
 * ARQUIVO:            EXTDAO_Projetos.php
 * TABELA MYSQL:       projetos
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */



if(strlen(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO))){
    $idSP = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO);
    
    
    $idSPVP = EXTDAO_Sistema_projetos_versao_produto::getIdDaUltimaVersao($idSP);
    
    //$objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP->select($idSPVP);
    $idPVBB = $objSPVP->getServidor_projetos_versao_banco_banco_id_INT();
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $idPV = $objPVBB->getProjetos_versao_id_INT();
    
} else{
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);    
}



$idPVPE = Helper::GET(Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA);
$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
$objPVPE->select($idPVPE);

$objPE = new EXTDAO_Processo_estrutura();
$objPE = $objPVPE->getObjProcessoEstrutura();

$iniciandoEstrututa = true;
if(strlen(Helper::GET(Param_get::PROCESSO_INICIADO)))
    $iniciandoEstrututa = false;        

$objPVC = new EXTDAO_Projetos_versao_caminho();
$idPVCaminhoAtual = null;
$idPVCaminhoAtual = $objPVPE->getId_atual_projetos_versao_caminho_INT();
if(strlen(Helper::GET(Param_get::ID_CAMINHO_CICLO_REINICIADO))){
    $idPVCaminhoAtual = Helper::GET(Param_get::ID_CAMINHO_CICLO_REINICIADO);
    EXTDAO_Projetos_versao_processo_estrutura::reinicializaCicloNaEtapa($idPVPE, $idPVCaminhoAtual);
    
    
}else{
    
    //Caso nao possua nenhum processo atualmente
    if (!strlen($idPVCaminhoAtual)) {
        $idPVCaminhoAtual = EXTDAO_Projetos_versao_processo_estrutura::inicializaCiclo($idPVPE);    
    }
}

$objPVC->select($idPVCaminhoAtual);

$idTPE = $objPVC->getTipoProcessoEstrutura();
$objTPE = new EXTDAO_Tipo_processo_estrutura();
$objTPE->select($idTPE);

$strVetorId = $objPVPE->getGet_vetor_id();
//echo "strVetorId: ".$objPVPE->getId();
//print_r("$strVetorId");
$vetorId = array();
if (strlen($strVetorId)){
//    echo "Helper::explode";
    $vetorId = Helper::explode('[,]', $strVetorId);
    
//    echo "STR: $strVetorId<br/>";
    
    if(empty($vetorId)){
        $vetorId = $strVetorId;
    }
}
$indiceAtual = $objPVPE->getCiclo_atual_INT();
if($indiceAtual == null || !strlen($indiceAtual))
    $indiceAtual = 0;

$msgPorcentagem = "";
if(is_numeric($vetorId)){
    $porcentagem = ($indiceAtual / $vetorId) * 100;
    $msgPorcentagem = "</br>EXECUTADO:\t\t $porcentagem %";
}
else if (count($vetorId)) {
    $porcentagem = ($indiceAtual / count($vetorId)) * 100;
    $msgPorcentagem = "</br>EXECUTADO:\t\t $porcentagem %";
} else{
    $msgPorcentagem = "</br>EXECUTADO:\t\t 0 %";
}$vIndiceFinal = $objPVPE->getTotalEtapas() - 1;

if($objTPE->getProcesso_manual_BOOLEAN() != "1" || $iniciandoEstrututa){
    Helper::imprimirMensagem("PROCESSO:\t {$objPVC->getNome()} </br> ETAPAS: {$objPVC->getOrdemNaListaDeEtapas()} / {$vIndiceFinal}{$msgPorcentagem}", MENSAGEM_INFO);
}

$cicloPagina = true;
if(! $iniciandoEstrututa){
    $outProcessoConcluido = false;
    $outCicloAtual = 0;
    
    $pagina = $objPVC->getPagina();
    if(strlen($pagina)){

        $objPVBBTeste = new EXTDAO_Projetos_versao_banco_banco;
        $objPVTeste = new EXTDAO_Projetos_versao();
        $idPVBBTeste = null;
        
        
        if($objPE->getUtiliza_copia_projetos_versao_BOOLEAN() == "1"){
            if(!strlen($objPVPE->getCopia_projetos_versao_banco_banco_id_INT())){
                //inicializa a copia a ser utilizada no processo
                EXTDAO_Projetos_versao::cadastraCopiaProjetosVersao($idPVBB, $objPVPE);
                $objPVPE->select($idPVPE);
            }
            $idPVBBTeste = $objPVPE->getCopia_projetos_versao_banco_banco_id_INT();
        } else
            $idPVBBTeste = $idPVBB;
        
              
        $objPVBBTeste->select($idPVBBTeste);
        $idPVTeste = $objPVBBTeste->getProjetos_versao_id_INT();
        $objPVTeste->select($idPVTeste);
        Helper::imprimirMensagem("P�gina: ".$pagina, MENSAGEM_INFO);
        include($pagina);
        $statusProcesso = executa_pagina_processo($objPVPE, $objPVTeste, $objPVBBTeste, $vetorId, $indiceAtual, $idPVCaminhoAtual);
//        print_r($statusProcesso);
//        exit();
        if($objTPE->getProcesso_manual_BOOLEAN() != "1"){
            if( $statusProcesso->outProcessoConcluido != true){
                 if(isset($statusProcesso->tipo) && $statusProcesso->tipo == "TIPO_ITERACAO"){
                    $vetorId = $statusProcesso->totalIteracao;
                    $indiceAtual = $statusProcesso->indiceAtual;
                    $outProcessoConcluido = false;
                    $outCicloAtual = $indiceAtual;
                } else {
                    $vetorId = $statusProcesso->vetorId;
                    $indiceAtual = $statusProcesso->indiceAtual;
                    if($indiceAtual >= count($vetorId)){
                        $outProcessoConcluido = true;
                    }
                    $outCicloAtual = $indiceAtual;
                } 

            } else $outProcessoConcluido = true;
            $strVetor = Helper::vetorToString($vetorId, ",");
            $objPVPE->registraCiclo($outCicloAtual, $strVetor, null);

            if($outProcessoConcluido){
                if($objPVPE->existeProximaEtapa())
                    EXTDAO_Projetos_versao_processo_estrutura::proximaEtapa($objPVPE->getId());
                else{
                    $objPVC->finalizaEtapa();
                    EXTDAO_Projetos_versao_processo_estrutura::finalizaCiclo($objPVPE->getId());
                    $cicloPagina = false;
                }
            }
        } else{
            $cicloPagina = false;
        }
    }
        
}
if($objTPE->getProcesso_manual_BOOLEAN() != "1" || $iniciandoEstrututa){
    if ($cicloPagina) {
        //gravando o estado atual no variavel GET

        $url = EXTDAO_Projetos_versao_processo_estrutura::getUrlDeExecucaoDaEtapa($idPVPE);
        Helper::mudarLocation($url);
        //return array("location: index.php?page=compara_banco_hom_prod&tipo=pages&".$varGET);
    } else if($objPE->getUtiliza_copia_projetos_versao_BOOLEAN() != "1"){
        $url = EXTDAO_Projetos_versao_processo_estrutura::getUrlDeFinalizacaoDoProcessoEstrutura($idPVPE);

        sleep(4);
        Helper::mudarLocation($url);
    }
}
