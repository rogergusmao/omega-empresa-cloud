<?php




    $objArvore = new Arvore_view();
    $vetorNodo = array();

    $idNodo = Helper::GET("id");
    
    $idNodo= urldecode($idNodo);
    $idArvoreView = Helper::GET(Param_Get::ID_ARVORE_VIEW);

    if ($idNodo != null && strlen($idNodo) && $idNodo != "0") {
        $pos = strpos($idNodo, '&');
        
        $objIdNodo = Nodo_view::formataGETIdNodo($idNodo);
        $idNodoAux = $objIdNodo[0];
        $varGET = $objIdNodo[1];
        
        $vetorNodo = $objArvore->getFilhos($idNodoAux, $varGET);
    } else {
        $idNodo = 0;

        $vetorNodo = $objArvore->getListaNodoRaiz();
    }
    $xml = "";
//Imprime somente a subarvore do contexto
    $xml = "<tree id='" . urlencode($idNodo) . "'>";
    for ($i = 0; $i < count($vetorNodo); $i++) {
        $nodo = $vetorNodo[$i];
        $xml .= $nodo->getXML();
    }
    $xml .= "</tree>";
    //Arvore_view::persisteXML($idArvoreView, $idNodo, $xml);

    print($xml);
