<?php

function pagina_informacao_mobile_identificador($idMI){
$objMI = new EXTDAO_Mobile_identificador();
$objMI->select( $idMI);

$strConectado = "";
$strConectado = EXTDAO_Mobile::isMobileConectado($objMI->getMobile_id_INT()) ? "Conectado" : "Desconectado" ;



$tipoBanco =EXTDAO_Mobile_identificador::getTipoBanco($idMI);

$objStatus = $objMI->getMensagemStatusSuporte();

$raiz = Helper::acharRaiz();
?>
<table width="350" align="center" class="tabela_popup">
    <tr>
        <?
        $img = "";
        switch ($tipoBanco) {
            case EXTDAO_Tipo_banco::$TIPO_BANCO_ANDROID:
                $img = "<img src=\"{$raiz}/adm/imgs/mobile_android.png\" height=\"32\" width=\"32\">";
                break;
            case EXTDAO_Tipo_banco::$TIPO_BANCO_IPHONE:
                $img = "<img src=\"{$raiz}/adm/imgs/mobile_iphone.png\" height=\"32\" width=\"32\">";
                break;
            case EXTDAO_Tipo_banco::$TIPO_BANCO_WINDOWS_PHONE:
                $img = "<img src=\"{$raiz}/adm/imgs/mobile_windows_phone.png\" height=\"32\" width=\"32\">";
                break;
            default:
                break;
        }
        ?>
    
        <td rowspan="4" style="padding-top: 5px; color: #333333;">
            <?=$img;?>
        </td>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">
            <?=$objMI->getIdentificador();?>
        </td>
    </tr>
    <tr>
        <td>
            Conexao
        </td>   
        <td>
            <?=$strConectado;?>
            </td>
        </tr>
        <tr>
            <td>
                Suporte
            </td>   
            <td>
            <?=$objStatus->mensagem;?>
            </td>
        </tr>
        <tr>
            <td>
                Ultima Operacao De Sistema Executada
            </td>   
            <td>
            <?=$objStatus->mensagem;?>
            </td>
        </tr>
</table>
<?   
}
