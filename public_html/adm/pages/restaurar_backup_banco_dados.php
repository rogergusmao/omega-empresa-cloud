<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       banco_dados
    * NOME DA CLASSE DAO: DAO_Banco_dados
    * DATA DE GERAÇÃO:    12.12.2009
    * ARQUIVO:            EXTDAO_Banco_dados.php
    * TABELA MYSQL:       banco_dados
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    */

    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar o campo de detalhes deste backup";
    $acoes["tooltip_restauracao"] = "Clique aqui para fazer a restauração deste backup.<br /><br />Um backup automático será feito antes da restauração.";
    $acoes["tooltip_analise"] = "Clique aqui para utilizar este backup do banco de dados para fins de análise";

    if(Helper::GET("analise_retroativa")){

        $objBanco = new Database(NOME_BANCO_DE_DADOS_BACKUP);

        $objBanco->query("SELECT COUNT(id) FROM analise WHERE id=1");

        $numeroLinhas = $objBanco->getPrimeiraTuplaDoResultSet(0);

        if($numeroLinhas == "1"){

            $objAnalise = new EXTDAO_Analise();

            $objAnalise->select(1);
            $objAnalise->formatarParaExibicao();

        }

    ?>

	<fieldset class="fieldset_filtro">
    	<legend class="legend_filtro">Análise Retroativa</legend>

		<table class="tabela_list" style="background-color: #FFFFFF;">

			<? if($numeroLinhas == "1" && !AnaliseRetroativa::getAnaliseNaSessaoCorrente()){ ?>

			<tr class="tr_form">

    			<td class="td_form_campo">

    				&bull; <a href="actions.php?class=AnaliseRetroativa&action=continuarUltimaAnaliseRetroativa" class="link_padrao">Continuar com a última análise (<?=$objAnalise->getDescricao_HTML() ?>)</a>

    			</td>

    		</tr>

    		<? } ?>

    		<? if(!AnaliseRetroativa::getAnaliseNaSessaoCorrente()){ ?>

    			<tr class="tr_form">

        			<td class="td_form_campo">

        				&bull; <a href="actions.php?class=AnaliseRetroativa&action=fazerAnaliseAPartirDoBancoDeDadosAtual" class="link_padrao">Iniciar nova análise a partir do banco de dados atual</a>

        			</td>

        		</tr>

        		<tr class="tr_form">

        			<td class="td_form_campo">

        				&bull; <a href="index.php?page=analise_a_partir_da_importacao&tipo=pages" class="link_padrao">Iniciar nova análise a partir da importação de dados</a>

        			</td>

        		</tr>

        		<tr class="tr_form">

        			<td class="td_form_campo">

        				&bull; <a href="index.php?page=ponto_de_interesse_analise&tipo=lists" class="link_padrao">Visualizar pontos de interesse existentes</a>

        			</td>

        		</tr>

			<? } ?>

    		<? if(AnaliseRetroativa::getAnaliseNaSessaoCorrente()){ ?>

			<tr class="tr_form">

				<td class="td_form_campo">

					&bull; <a href="index.php?page=ponto_de_interesse_analise&tipo=forms" class="link_padrao">Criar/atualizar ponto de interesse com os dados atuais desta análise</a>

				</td>

			</tr>

			<tr class="tr_form">

				<td class="td_form_campo">

					&bull; <a href="actions.php?class=AnaliseRetroativa&action=sairDoModoDeAnalise" class="link_padrao">Sair do Modo de Análise</a>

				</td>

			</tr>

    		<? } ?>

		</table>

    </fieldset>

    <br />

    <? Helper::imprimirMensagem("Os backups abaixo listados podem ser utilizados para fins de análise.\nBasta selecionar um deles clicando no ícone <img  src='imgs/icone_analise.png' />.", MENSAGEM_INFO); ?>

	<br />

    <?

    }

    include("filters/banco_dados.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Banco_dados();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

         if(!Helper::isNull($obj->getDescricao())){

            $strCondicao[] = "descricao LIKE '%{$obj->getDescricao()}%'";
            $strGET[] = "descricao={$obj->getDescricao()}";

        }

         if(!Helper::isNull($obj->getData_criacao_DATETIME())){

            $strCondicao[] = "data_criacao LIKE '%{$obj->getData_criacao_DATETIME()}%'";
            $strGET[] = "data_criacao={$obj->getData_criacao_DATETIME()}";

        }

         if(!Helper::isNull($obj->getData_ultima_restauracao_DATETIME())){

            $strCondicao[] = "data_ultima_restauracao LIKE '%{$obj->getData_ultima_restauracao_DATETIME()}%'";
            $strGET[] = "data_ultima_restauracao={$obj->getData_ultima_restauracao_DATETIME()}";

        }

        if(!Helper::isNull(Helper::GET("analise_retroativa"))){

        	$strCondicao[] = "1=1";
            $strGET[] = "analise_retroativa=" . Helper::GET("analise_retroativa");

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM banco_dados " . $consulta;

    $objBanco = new Database(NOME_BANCO_DE_DADOS_BACKUP);

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM banco_dados {$consulta} ORDER BY data_criacao_DATETIME DESC LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

?>

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Backups Do Banco De Dados</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="50%" />
			<col width="17%" />
			<col width="17%" />
			<col width="16%" />
		</colgroup>
        <thead>
			<tr class="tr_list_titulos">

				<td class="td_list_titulos"><?=$obj->label_descricao ?></td>
				<td class="td_list_titulos"><?=$obj->label_data_criacao ?></td>
				<td class="td_list_titulos"><?=$obj->label_data_ultima_restauracao ?></td>
				<td class="td_list_titulos" style="text-align: center;">Ações</td>

			</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"

    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getDescricao() ?>
    		</td>

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getData_criacao_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getData_ultima_restauracao_DATETIME()?$obj->getData_ultima_restauracao_DATETIME():"Este backup nunca foi restaurado" ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">

			<? if(!Helper::GET("analise_retroativa")){ ?>

				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=banco_dados&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_restauracao.png" onclick="javascript:location.href='actions.php?class=EXTDAO_Banco_dados&action=__actionRestaurar&id=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_restauracao'] ?>')" onmouseout="javascript:notip()">&nbsp;

			<? }else{ ?>

				<img border="0" src="imgs/icone_analise.png" onclick="javascript:location.href='actions.php?class=AnaliseRetroativa&action=carregarOpcaoEscolhidaPeloUsuario&id_backup=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_analise'] ?>')" onmouseout="javascript:notip()">&nbsp;

		   	<? } ?>

		   	</td>

		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Paginação

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

	    <fieldset class="fieldset_paginacao">
	            <legend class="legend_paginacao">Paginação</legend>

		<table class="table_paginacao">
			<tr class="tr_paginacao">

		<?

		for($i=1; $i <= $numeroPaginas; $i++){

			$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

		?>

			<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=pages&page=restaurar_backup_banco_dados&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

		<? } ?>

		    </tr>
		</table>

		</fieldset>

	<? } ?>

