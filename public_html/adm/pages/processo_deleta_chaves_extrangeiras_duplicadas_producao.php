<?php


function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
     $trunk = Trunk::factory();
    $idPVBB = $trunk->getPVBBDoBancoDeDadosDeProducao($objPVBB->getId());

    if(empty($vetorId)){
      $objBB = $objPVBB->getObjBancoBanco($idPVBB);
       
      $vetor = array();

      $dbHmgOuProd = $objBB->getObjBancoProducao();
       if($dbHmgOuProd != null){
           $tabelas = $dbHmgOuProd->getListaDosNomesDasTabelas();
           if(is_array($tabelas)){
               for($i = 0; $i < count($tabelas); $i++){
                    $vetor[$i] = "P{$tabelas[$i]}";
               }
           }
       }
       
       $vetorId = $vetor;
       $indiceAtual = 0;
    }
//    print_r($vetorId);
    //$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
    $objPE = $objPVPE->getObjProcessoEstrutura();
    $json = $objPE->getJson_constante();
    
    $idTipoScriptBanco = null;
    if(strlen($json)){
        $objJson = json_decode($json);    
        $idTipoScriptBanco =  $objJson->idTipoScriptBanco;
    } else {
        $idTipoScriptBanco = EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD;
    }
    
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB, 
        $idTipoScriptBanco);
    $prefixo = "";
    if($objPVBB->getFkObjProjetos_versao()->getTipo_analise_projeto_id_INT() == EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB)
        $prefixo = "__";
    $tipoBanco = null;
    if($objPE->getId() == EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_HOMOLOGACAO
        || $objPE->getId() == EXTDAO_Processo_estrutura::APAGAR_DADOS_DO_BANCO_DE_PRODUCAO)
        $tipoBanco = EXTDAO_Tipo_banco::$TIPO_BANCO_WEB;
    
    if($indiceAtual < count($vetorId)){
        for($i = 0 ; $i < 20; $i++){
            if($indiceAtual < count($vetorId)){
                $strId = $vetorId[$indiceAtual];
                $tabela = substr($strId, 1);
                if(Helper::startsWith($strId, "H")) {
                    //homologacao
                    $dbProdOuHmg = $objGSBB->getDatabaseHomologacao();
                    $objBancoProdOuHmg = $objGSBB->getObjBancoHomologacao();
                    $isProd = false;
                } else {
                    //producao
                    $dbProdOuHmg = $objGSBB->getDatabaseProducao();
                    $objBancoProdOuHmg = $objGSBB->getObjBancoProducao();
                    $isProd = true;
                }
                
                $objGSBB->removerForeignKeyDuplicada(
                    $dbProdOuHmg,
                     $objBancoProdOuHmg,
                    $tabela,
                    $isProd,
                    $prefixo, 
                    $tipoBanco);
            }
            if($indiceAtual >= count($vetorId)){
                break;
            } else{
                $indiceAtual += 1;
            }
        }
    }
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    Helper::imprimirMensagem($indiceAtual." relacionamento(s) entre tabelas mapeados até o momento.");
    if($indiceAtual >= count($vetorId)){
       $status->outProcessoConcluido = true;
    } else {
        $status->outProcessoConcluido = false;
    }
    return $status;
    
}
?>	
