<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $status = new stdClass();
    $totalTabelaPorCiclo = 10;

    $idBB = $objPVBB->getBanco_banco_id_INT();
    $objBancoBanco = new EXTDAO_Banco_banco();
    $objBancoBanco->select($idBB);
    
    if(empty($vetorId)){
        //carregando a estrutura de atributos das tabelas
        $objBancoBanco = new EXTDAO_Banco_banco();    
        $objBancoBanco->select($idBB);

        //$objPV = new EXTDAO_Projetos_versao();
        switch ($objPV->getTipo_analise_projeto_id_INT()) {
            case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
                $objBancoBanco->setPrefixoNomeTabelaProducao("__");
                break;
            default:
                break;
        } 

        
        $vetorId = EXTDAO_Tabela_tabela::getVetorIdTabelaTabela($objPVBB->getId(), $idBB);
        $indiceAtual = 0;
    }
    
    
    for($i = 0 ; $i < $totalTabelaPorCiclo; $i++){
        $objTabelaTabela = new EXTDAO_Tabela_tabela();
        $db = new Database();
        if($indiceAtual < count($vetorId)){
            $idTT = $vetorId[$indiceAtual];
            if(strlen($idTT)){
                $objTabelaTabela->select($idTT);
                $objBancoBanco->inicializaEstruturaDeAtributo($objPVBB->getId(), $objTabelaTabela, $db);    
            }
        }
        if($indiceAtual >= count($vetorId)){

            break;
        } else{
            $indiceAtual += 1;
        }
    }


    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." relacionamento(s) entre tabelas mapeada(s) at� o momento.");
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	