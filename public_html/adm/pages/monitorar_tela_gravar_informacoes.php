<?php

$idOSM = Helper::POSTGET('idOSM');
$tipoDeObjeto = Helper::POSTGET('shape');
$objMTWM = new EXTDAO_Monitora_tela_web_para_mobile();
$objMTWM->setOperacao_sistema_mobile_id_INT($idOSM);
if($tipoDeObjeto == 'rect'){
    
    $x = Helper::POSTGET('x');
    $y = Helper::POSTGET('y');
    $largura = Helper::POSTGET('w');
    $altura = Helper::POSTGET('h');
    $cor  = Helper::POSTGET('color');
    $objMTWM->setCoordenada_x_INT($x);
    $objMTWM->setCoordenada_y_INT($y);
    $objMTWM->setLargura_quadrado_INT($largura);
    $objMTWM->setAltura_quadrado_INT($altura);
    $objMTWM->setCor_INT($cor);
    $objMTWM->setTipo_operacao_monitora_tela_id_INT(EXTDAO_Tipo_operacao_monitora_tela::desenha_retangulo_na_tela_do_mobile);
    
    
}
elseif($tipoDeObjeto == 'circle'){
    
    $x = Helper::POSTGET('x');
    $y = Helper::POSTGET('y');
    $raio = Helper::POSTGET('radius');
    $cor  = Helper::POSTGET('color');
    $objMTWM->setCoordenada_x_INT($x);
    $objMTWM->setCoordenada_y_INT($y);
    
    $objMTWM->setRaio_circulo_INT($raio);
    $objMTWM->setCor_INT($cor);
    $objMTWM->setTipo_operacao_monitora_tela_id_INT(EXTDAO_Tipo_operacao_monitora_tela::desenha_circulo_na_tela_do_mobile);
}
elseif($tipoDeObjeto == 'line'){
    
    $x = Helper::POSTGET('x');
    $y = Helper::POSTGET('y');
    $deslocamentoX = Helper::POSTGET('w');
    $deslocamentoY = Helper::POSTGET('h');
    $cor  = Helper::POSTGET('color');
    $objMTWM->setCoordenada_x_INT($x);
    $objMTWM->setCoordenada_y_INT($y);
    $objMTWM->setLargura_quadrado_INT($deslocamentoX);
    $objMTWM->setAltura_quadrado_INT($deslocamentoY);
    $objMTWM->setCor_INT($cor);
    $objMTWM->setTipo_operacao_monitora_tela_id_INT(EXTDAO_Tipo_operacao_monitora_tela::desenha_reta_na_tela_do_mobile);
    
}
elseif($tipoDeObjeto == 'text'){
    
    $x = Helper::POSTGET('x');
    $y = Helper::POSTGET('y');
    $texto = Helper::POSTGET('text');
    $tamanhoFonte = Helper::POSTGET('size');
    
    $objMTWM->setCoordenada_x_INT($x);
    $objMTWM->setCoordenada_y_INT($y);
    $objMTWM->setMensagem($texto);
    $objMTWM->setTipo_operacao_monitora_tela_id_INT(EXTDAO_Tipo_operacao_monitora_tela::mensagem_toast);
        
}

$objMTWM->formatarParaSQL();
$objMTWM->insert();

?>
