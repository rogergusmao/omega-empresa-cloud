<?php
$raiz = Helper::acharRaiz();
$idMI = Helper::GET(Param_Get::ID_MOBILE_IDENTIFICADOR);

$obj = new EXTDAO_Mobile_identificador();
$obj->select($idMI);
$obj->setRequisicao_modo_suporte_DATETIME(Helper::getDiaEHoraAtualSQL());
$obj->setRequisicao_usuario_id_INT(Seguranca::getId());
$obj->formatarParaSQL();
$obj->update($idMI);

?>

<script type="text/javascript">
    function startTrigger(){
      return window.setInterval(function(){processa()}, 1000) ;
    };
    
    
    window.onload = function(){
        this.trigger = startTrigger();
    };
    
    function processa(){
        var msg = carregarValorRemoto(
            "EXTDAO_Mobile_identificador", 
            "imprimirMensagemStatus('" + '<?=$idMI;?>' +  "')", 
            '<?=$raiz;?>', 
            null);
        
        var objJson = JSON.parse(msg);
        if(objJson.processando == false ){
            //desliga a rotina
            window.clearInterval(this.trigger);  
        } 
        var containerHTML = document.getElementById("div_mensagem_processo");
        
        var msgDialogo = unescape(objJson.mensagem);
        
        containerHTML.innerHTML = msgDialogo;
        
    }
    

    
</script>

<div id="div_mensagem_processo">
    
</div>
