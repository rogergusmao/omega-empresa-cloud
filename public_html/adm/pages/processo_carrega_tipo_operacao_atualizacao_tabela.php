<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){

    if(count($vetorId) == 0){
        $vetorId = $objPVBB->getVetorTabelaTabela();
        $indiceAtual = 0;
    }

    for($i = 0 ; $i < 80; $i++){
        if($indiceAtual < count($vetorId)){
            $vTabelaTabela = $vetorId[$indiceAtual];
            if(strlen($vTabelaTabela)){
                $objTT = new EXTDAO_Tabela_tabela();
                $objTT->select($vTabelaTabela);
                $vTipoOperacao = $objTT->atualizaTipoOperacaoAtualizacaoBanco();
                
                
                $objTT->setTipo_operacao_atualizacao_banco_id_INT($vTipoOperacao);
                $objTT->formatarParaSQL();
                $objTT->update($vTabelaTabela);
            }


        }

        if($indiceAtual >= count($vetorId)){

            break;
        } else{
            $indiceAtual += 1;
        }
    }
    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." relacionamento(s) entre tabelas at� o momento.");
$status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	