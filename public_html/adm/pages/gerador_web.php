<?php


echo Helper::carregarArquivoCss(false, "adm/css/", "padrao_gerador");
    
$varGET = Param_Get::getIdentificadorProjetosVersao();
$url = "index.php?tipo=pages&page=gerador_web&$varGET";
$dbBibliotecaNuvem = new Database();
$condicaoUnderline = "";
$condicaoChave="";
$idPVBB = Helper::GET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
$objPVBB = new EXTDAO_Projetos_versao_banco_banco();
$objPVBB->select($idPVBB);

//if($objPVBB->getTipo_plataforma_operacional_id_INT() != EXTDAO_Tipo_plataforma_operacional::WEB_PHP){
//    Helper::imprimirMensagem ("O protótipo não é do tipo Web.", MENSAGEM_ERRO);
//    exit();
//}
$idPV = $objPVBB->getProjetos_versao_id_INT();

$objPV = new EXTDAO_Projetos_versao();
$objPV->select($idPV);  
$idProjetos = $objPV->getProjetos_id_INT();
$database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBB);

$dbProjeto= EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBB);

$idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBB);
    
$vetorTabelaBancoOrdenada = EXTDAO_Tabela::getHierarquiaTabelaBanco($idPV, $idBanco);
if (!isset($_REQUEST["f"]) || $_REQUEST["f"] == "") {
    
    
//    $objPVBB->get
    ?>

    <html>
        <head>
            
            <title>GERADOR DE PROJETOS OMEGA SOFTWARE</title>
            <script language="javascript">
                
                function automatico(valor, condicaoChave, condicaoUnderLine){

                    var posicao = 0

                    var valorAnt = "";

                    var underline = "_";

                    if(condicaoUnderLine == 1)
                        underline = "_";

                    if(condicaoChave == 1)
                        valorAnt = valor + "_";

                    while(posicao != -1){

                        posicao = valor.indexOf("_", posicao+1);

                        if(posicao != -1)
                            valor = valor.substring(0, posicao) + underline + valor.substring(posicao+1,posicao+2) + valor.substring(posicao+2,valor.length);
                            //valor = valor.substring(0, posicao) + underline + valor.substring(posicao+1,posicao+2).toUpperCase() + valor.substring(posicao+2,valor.length);

                    }

                    var nomeClasse = "DAO" + valor.substring(0, 1).toUpperCase() + valor.substr(1);

                    var nomeExt = "EXTDAO" + valor.substring(0, 1).toUpperCase() + valor.substr(1);

                    var nomeCampo = valorAnt + "id";

                    var nomeLabel = valorAnt + "nome";

                    document.getElementById("classname").value = nomeClasse;

                    //document.getElementById("keyname").value = nomeCampo;

                    //document.getElementById("labelname").value = nomeLabel;

                    document.getElementById("extname").value = nomeExt;

                }

                function carregarTabelas(){

                    var projeto = document.getElementById("projeto").value;

                    if(projeto != ""){

                        document.location = "<?=$url;?>";

                    }

                }

            </script>

        </head>

        <body>

            <h3>
                GERADOR DE PROJETOS OMEGA SOFTWARE
            </h3>

            <form action="<?=$url;?>" method="POST" name="FORMGEN"> 

                <p>1) Projeto:</p> 
                <?
                    $tabelaSel = Helper::GET("tabela");
                    
                    $database->query("SHOW TABLES");
                    $tabelas = Helper::getResultSetToArrayDeUmCampo($database->result);
                    
                    ?>

                    <p>2.1) Escolha a Tabela:</p>

                        <select class="input_text" name="tablename" 
                                id="tablename" 
                                onchange="javascript:location.href='<?= $url; ?>&tabela=' + this.value + '&condicaoUnderline=<?= $condicaoUnderline ?>&condicaoChave=<?= $condicaoChave ?>'">

                        <option value=""></option>

                        <?
                        foreach ($tabelas as $t) {
                            print "<option " . ($tabelaSel == $t ? "selected=\"selected\"" : "") . " value=\"$t\">$t</option>\n";
                        }
                        ?>

                        </select>
                    <br>
                    <br>

                    <? if (!Helper::GET("tabela")) { ?>
                    
                        <? 
                        
                        if(strlen(Helper::GET("acao")) && Helper::GET("acao") == "corrigir_estrutura_tabelas"){

                            $database->query("SHOW TABLES FROM {$database->DBName}");

                            while ($row = $database->fetchArray()) {
                                
                                $tabelaCorrigida = false;
                                
                                $nomeTabela = $row[0];
                               
				$tabelaCorrigida = true;
                                
                                if($tabelaCorrigida){
                                    
                                    print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Tabela <b>$nomeTabela</b> corrigida com sucesso</b>.
                                    </p>";
                                    
                                }
                                                                
                            }
                    

                        } elseif(Helper::GET("acao") == "popula_sistema_atributo"){
                        
                            $idProjeto = Helper::GET("projeto");
                            
                            include("pages/popula_sistema_atributo.php");
                            populaSistemaAtributo();

                            print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Tabela populada com sucesso
                                    </p>";
                            
                        }
                        elseif(strlen(Helper::GET("acao")) && Helper::GET("acao") == "criar_chaves_estrangeiras"){
                        
                            $idProjeto = Helper::GET("projeto");
                            
                            $objPadronizador = new Padronizador_Database($database);
//                            print_r($database);
//                            exit();
                            $objPadronizador->padronizeDatabaseSQL();
                            
                            print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Chaves estrangeiras criadas com sucesso.
                                    </p>";
                            
                        }
                        elseif(Helper::GET("acao") == "atualizar_sistema_tabela"){
                        
                            $idProjeto = Helper::GET("projeto");
                            
                            $objPadronizador = new Padronizador_Database($database);
                            $objPadronizador->atualizaSistemTabela();
                            
                            print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Tabela 'sistema_tabela' atualizada com sucesso.
                                    </p>";
                            
                        }elseif(Helper::GET("acao") == "gerar_codigo_web_banco_de_dados"){
                        
                            $idProjeto = Helper::GET("projeto");
                            
                            include("pages/gerador_web_banco.php");
                            $objGWB = new Gerador_web_banco();
                            $objGWB->gerarBancoWeb("1");

                            print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Arquivo do banco de dados foi gerado com sucesso
                                    </p>";
                         
                        }
                        elseif(Helper::GET("acao") == "gerar_codigo_menu_android"){
                        
                            $idProjeto = Helper::GET("projeto");
                            
                            include("lists/gerencia_permissao_sistema_mobile.php");
                            return;

                         
                        }
                        
                        ?>
                    
                        <p>2.2) Gerar classe para todas as tabelas deste projeto que tenham configurações salvas</p>

                        <input type="hidden" value="ok" name="todas">

                        <input type="checkbox" name="gerarDAO" value="1" checked> Gerar DAO &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarEXT" value="1"> Gerar EXTDAO &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarForm" value="1" checked> Gerar Formulário &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarList" value="1" checked> Gerar List &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarFiltro" value="1" checked> Gerar Filtro &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarAjaxList" value="1" checked> Gerar Ajax List &nbsp;&nbsp;
                        <br /><br />

                        <input class="botoes_form" type="submit" value="Gerar">
                        <input type="hidden" name="f" value="formshowed">

                    </form>
            
                    <a class="link_padrao" href="<?=$url; ?>&acao=corrigir_estrutura_tabelas">Corrigir Estrutura das Tabelas</a>
                    <br />
                    <a class="link_padrao" href="<?=$url; ?>&acao=criar_chaves_estrangeiras">Criar Chaves Estrangeiras</a>
                    <br />
                    <a class="link_padrao" href="<?=$url; ?>&acao=atualizar_sistema_tabela">Atualizar Sistema Tabela</a>
                    
                    
                    <br />
                    <a class="link_padrao" href="<?=$url; ?>&acao=gerar_codigo_web_banco_de_dados">Gerar Codigo Web Banco De Dados</a>
                    
                    <br />
                    <a class="link_padrao" href="index.php?tipo=pages&page=gerar_codigo_menu_android&<?=$varGET;?>">Gerar Codigo do Menu Android</a>
                    <br />
                    <a class="link_padrao" href="index.php?tipo=lists&page=gerencia_permissao_sistema_mobile&<?=$varGET;?>">Gerenciar Permissões Do Menu Android</a>
                    <br/>
                    <a class="link_padrao" href="index.php?tipo=pages&page=popula_sistema_atributo&<?=$varGET;?>">Popula Sistema Atributo</a>
                <? } ?>



            <? if (strlen(Helper::GET("tabela"))) { ?>

                <?
                
                
                $tabela = Helper::GET("tabela");

                $databaseAux = new Database();
                $databaseAux->query("SELECT valores_campos_texto 
                    FROM configs_salvas 
                    WHERE projetos_id_INT={$idProjetos} 
                        AND tabela_nome='{$tabela}'");

                $arrCampos = array("camposfiltro" => array(),
                                    "camposlista" => array(),
                                    "acoeslista" => array(),
                                    "camposajax" => array(),
                                    "camposajaxmestres" => array(),
                                    "camposajaxget" => array(),
                                    "obrigatorioxcampo" => array());

                $loadBanco = false;

                if ($databaseAux->rows() > 0) {

                    $arrCampos = unserialize(html_entity_decode(Database::mysqli_result($databaseAux->result, 0, 0)));
                    $loadBanco = true;
                }

                if (!array_key_exists("camposobrigatorios", $arrCampos))
                    $arrCampos["camposobrigatorios"] = array();

                if (!array_key_exists("camposfiltro", $arrCampos))
                    $arrCampos["camposfiltro"] = array();

                if (!array_key_exists("camposlista", $arrCampos))
                    $arrCampos["camposlista"] = array();

                if (!array_key_exists("camposajax", $arrCampos))
                    $arrCampos["camposajax"] = array();

                if (!array_key_exists("camposajaxmestres", $arrCampos))
                    $arrCampos["camposajaxmestres"] = array();

                if (!array_key_exists("camposajaxget", $arrCampos))
                    $arrCampos["camposajaxget"] = array();

                if (!array_key_exists("acoeslista", $arrCampos))
                    $arrCampos["acoeslista"] = array();
                ?>

                <p>2.2) Opção de apagar todos os arquivos gerados nesta tabela:</p>

                <input class="botoes_form" type="button" id="apagar" name="apagar" size="50" value="Apagar Arquivos" onclick="javascript:location.href='<?=$url; ?>' + document.getElementById('projeto').value + '&opcao=apagarArquivos&tabela=' + document.getElementById('tablename').value + '&condicaoUnderline=<?= $condicaoUnderline ?>&condicaoChave=<?= $condicaoChave ?>'">
                <br>

                <p>3.1) Escreva o nome da classe ("ex: DAO_MinhaClasse"):</p>

                <input class="input_text" type="text" id="classname" name="classname" size="50" value="<?= $arrCampos["classname"] ?>">
                <br>

                <p>3.2) Escreva o nome da classe herdeira da classe DAO:</p>

                <input class="input_text" type="text" id="extname" name="extname" value="<?= $arrCampos["extname"] ?>" size="50">

                <p>4.1) Escreva o nome da entidade representada pela tabela no singular:</p>

                <font size=1 >
                <i>Todas os caracteres em minúsculo</i>
                </font>
                <br>
                <input class="input_text" type="text" id="entidade_singular" name="entidade_singular" value="<?= $arrCampos["entidade_singular"] ?>" size="50" value="">
                <br>
                <p>4.2) Escreva o nome da entidade representada pela tabela no plural:</p>

                <font size=1 >
                <i>Todas os caracteres em minúsculo</i>
                </font>
                <br>
                <input class="input_text" type="text" id="entidade_plural" name="entidade_plural" value="<?= $arrCampos["entidade_plural"] ?>" size="50" value="">
                <br>

                <p>4.3) Gênero da entidade:</p>

                <?
                $genero = $arrCampos["genero_entidade"];
                ?>

                <select class="input_text" name="genero_entidade" id="genero_entidade">
                    <option value="F" <?= $genero == "F" ? "selected=\"selected\"" : "" ?>>Feminino</option>
                    <option value="M" <?= $genero == "M" ? "selected=\"selected\"" : "" ?>>Masculino</option>
                </select>
                <br>

                <p>4.4) Defina o nome e obrigatoriedade de preenchimento dos campos contidos na tabela:</p>

                <table class="tabela_list">
                    <tr class="tr_list_titulos">
                        <td class="td_list_titulos">Campo</td>
                        <td class="td_list_titulos">Nome</td>
                        <td class="td_list_titulos">Obrigatório ?</td>
                    </tr>

                    <?
                    $tabela = Helper::GET("tabela");

                    //PROCURANDO COLUNAS NOT NULL
                    if (!$loadBanco) {

                        $database->query("SHOW COLUMNS FROM $tabela WHERE `Null` = 'NO';");

                        $result = $database->result;

                        $achouColuna = false;

                        while ($colunas = $database->fetchArray()) {

                            $arrCampos["camposobrigatorios"][] = $colunas[0];
                        }
                    }

                    $database->query("SHOW COLUMNS FROM $tabela");

                    $result = $database->result;

                    $achouColuna = false;

                    for ($y = 0; $colunas = $database->fetchArray(); $y++) {

                        $cssClassTr = ($y % 2 == 1) ? "tr_list_conteudo_par" : "tr_list_conteudo_impar";

                        $nomeColuna = $colunas[0];

                        if ($nomeColuna == "dataCadastro")
                            $nomePadrao = "data de cadastro";
                        elseif ($nomeColuna == "dataEdicao")
                            $nomePadrao = "data de edição";
                        else
                            $nomePadrao = "";
                        ?>
                        <tr class="<?= $cssClassTr ?>">
                            <td><?= $nomeColuna ?></td>
                            <td>
                                <input class="input_text" type="text" style="width: 300px;" name="nomexcampo_<?= $nomeColuna ?>" value="<?= $loadBanco ? $arrCampos["nomexcampo_" . $nomeColuna] : $nomePadrao ?>" />
                            </td>
                            <td align="center">
                                <input class="input_text" type="checkbox" name="camposobrigatorios[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposobrigatorios"]) ? "checked=\"checked\"" : "" ?> />
                            </td>
                        </tr>


                    <? } ?>

                </table>

                <br>

                <p>5) Escreva o nome do campo que contém a chave primária da tabela:</p>

                <select class="input_text" name="keyname" id="keyname">

                    <?
                    $tabela = Helper::GET("tabela");

                    $database->query("SHOW COLUMNS FROM $tabela WHERE `Key` = 'PRI';");

                    $result = $database->result;

                    $achouColuna = false;

                    while ($colunas = $database->fetchArray()) {


                        $nomeColuna = $colunas[0];

                        if ($arrCampos["keyname"] == $nomeColuna) {

                            $selected = "selected=\"selected\"";
                        } else {

                            $selected = "";
                        }
                        ?>

                        <option value="<?= $nomeColuna ?>" <?= $selected ?>><?= $nomeColuna ?></option>

                    <? } ?>

                </select>

                <br>
                <font size=1 >
                <i>Nome do índice da tabela, campo numérico com auto-incremento</i>
                </font>
                <br>

                <p>6) Escreva o nome do campo que contém o campo principal da tabela:</p>

                <select class="input_text" name="labelname" id="labelname">

                    <?
                    $tabela = Helper::GET("tabela");

                    $database->query("SHOW COLUMNS FROM $tabela");

                    $result = $database->result;

                    while ($colunas = $database->fetchArray()) {

                        $nomeColuna = $colunas[0];

                        if (isset($arrCampos["labelname"])) {

                            if ($arrCampos["labelname"] == $nomeColuna) {

                                $selected = "selected=\"selected\"";
                            } else {

                                $selected = "";
                            }
                        } else {

                            if (!$achouColuna && substr_count($nomeColuna, "nome") > 0) {

                                $selected = "selected=\"selected\"";
                                $achouColuna = true;
                            } else {

                                $selected = "";
                            }
                        }
                        ?>

                        <option value="<?= $nomeColuna ?>" <?= $selected ?>><?= $nomeColuna ?></option>

                    <? } ?>

                </select>
                <br>
                <br>

                <p>6.1) Defina os campos que serão escondidos (input type='hidden') no formulário:</p>

                <?
                $tabela = Helper::GET("tabela");

                $database->query("SHOW COLUMNS FROM $tabela  WHERE `Key` <> 'PRI';");

                $result = $database->result;

                while ($colunas = $database->fetchArray()) {

                    $nomeColuna = $colunas[0];
                    ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposformulariohidden"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposformulariohidden" name="camposformulariohidden[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

                <? } ?>

                <br>
                
                <p>6.2) Defina os campos com preenchimento do tipo Autocompletar:</p>

                <?
                $tabela = Helper::GET("tabela");

                $database->query("SHOW COLUMNS FROM $tabela  WHERE `Key` <> 'PRI';");

                $result = $database->result;

                while ($colunas = $database->fetchArray()) {

                    $nomeColuna = $colunas[0];
                    
                    if(!substr_count($nomeColuna, "_HTML") && 
                            !substr_count($nomeColuna, "_TEXTO") && 
                            !substr_count($nomeColuna, "_BOOLEAN") && 
                            !substr_count($nomeColuna, "_id_INT") &&
                            $nomeColuna != "dataCadastro" &&
                            $nomeColuna != "dataEdicao" ){
                    
                    ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposformularioautocompletar"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposformularioautocompletar" name="camposformularioautocompletar[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

                <? 
                
                    }
                
                } 
                
                ?>

                <br>
                
                <p>6.3) Defina os campos de relacionamento com preenchimento do tipo Autocompletar:</p>

                <?
                $tabela = Helper::GET("tabela");

                $database->query("SHOW COLUMNS FROM $tabela  WHERE `Key` <> 'PRI';");

                $result = $database->result;

                while ($colunas = $database->fetchArray()) {

                    $nomeColuna = $colunas[0];
                    
                    if(substr_count($nomeColuna, "_id_INT")){
                    
                    ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposformularioautocompletarrel"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposformularioautocompletarrel" name="camposformularioautocompletarrel[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

                <? 
                
                    }
                
                } 
                
                ?>

                <br>
                
                <p>6.4) Defina as entidades que serão cadastradas junto a entidade corrente:</p>

                <?
                $database->query("SHOW TABLES");

                while ($tabelas = $database->fetchArray()) {

                    $tabela = $tabelas[0];
                    
                    if($tabela != $tabelaSel){

                        $dbProjeto->query("SHOW COLUMNS FROM $tabela");

                        while ($colunas = $dbProjeto->fetchArray()) {

                            $nomeColuna = $colunas[0];

                            if($nomeColuna == "{$tabelaSel}_id_INT"){

                            ?>

                            <input class="input_text" <?=in_array($tabela, $arrCampos["tabelasrelacionamento"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="tabelasrelacionamento" name="tabelasrelacionamento[]" value="<?=$tabela ?>"><?=$tabela ?><br/>

                         <? } ?>

                    <? } ?>
                            
                <? } ?>        

            <? } ?>



        <br>

        <p>7) Defina os campos que irão compor o filtro da pesquisa:</p>

        <?
        $tabela = Helper::GET("tabela");

        $database->query("SHOW COLUMNS FROM $tabela");

        $result = $database->result;

        while ($colunas = $database->fetchArray()) {

            $nomeColuna = $colunas[0];
            ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposfiltro"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposfiltro" name="camposfiltro[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

        <? } ?>

                <br>
                <br>
                <p>8) Defina os campos que irão compor a lista de registros:</p>

        <?
        $tabela = Helper::GET("tabela");

        $database->query("SHOW COLUMNS FROM $tabela");

        $result = $database->result;

        while ($colunas = $database->fetchArray()) {

            $nomeColuna = $colunas[0];
            ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposlista"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposlista" name="camposlista[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

        <? } ?>

                <br>
                <br>
                <p>9.1) Defina os campos que irão compor a lista de cadastro/atualização multipla (Ajax)</p>

                <table class="tabela_list">
                    <tr class="tr_list_titulos">
                        <td class="td_list_titulos">Campo</td>
                        <td class="td_list_titulos" width="150px" align="center">Listagem</td>
                        <td class="td_list_titulos" width="150px" align="center">Mestre</td>
                        <td class="td_list_titulos" width="150px" align="center">Mestre $_GET</td>
                    </tr>

        <?
        $tabela = Helper::GET("tabela");

        $database->query("SHOW COLUMNS FROM $tabela");

        $result = $database->result;

        $achouColuna = false;

        for ($y = 0; $colunas = $database->fetchArray(); $y++) {

            $cssClassTr = ($y % 2 == 1) ? "tr_list_conteudo_par" : "tr_list_conteudo_impar";

            $nomeColuna = $colunas[0];
            ?>
                        <tr class="<?= $cssClassTr ?>">
                            <td><?= $nomeColuna ?></td>
                            <td align="center"><input class="input_text" type="checkbox" name="camposajax[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposajax"]) ? "checked=\"checked\"" : "" ?> /></td>
                            <td align="center"><input class="input_text" type="checkbox" name="camposajaxmestres[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposajaxmestres"]) ? "checked=\"checked\"" : "" ?> /></td>
                            <td align="center"><input class="input_text" type="checkbox" name="camposajaxget[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposajaxget"]) ? "checked=\"checked\"" : "" ?> /></td>
                        </tr>

        <? } ?>

                </table>

                <br>
                <br>

                <p>9.2) Defina o número de cadastros simultâneos do formulário ajax [0 (zero) gera ComboBox p/ o usuário selecionar]:</p>

                <input class="input_text" type="text" id="numero_cadastros_ajax" name="numero_cadastros_ajax" value="<?= $arrCampos["numero_cadastros_ajax"] ? $arrCampos["numero_cadastros_ajax"] : "0" ?>" size="2">
                <br>
                <br>

                <p>9.3) Defina o número limite no ComboBox de quantidade de registros:</p>

                <input class="input_text" type="text" id="numero_maximo_ajax" name="numero_maximo_ajax" value="<?= $arrCampos["numero_maximo_ajax"] ? $arrCampos["numero_maximo_ajax"] : "0" ?>" size="2">
                <br>
                <br>

                <p>10) Defina as ações disponíveis na lista:</p>

                <input <?= $loadBanco ? in_array("editar", $arrCampos["acoeslista"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" id="acoeslista" name="acoeslista[]" value="editar">Editar&nbsp;&nbsp;
                <input <?= $loadBanco ? in_array("visualizar", $arrCampos["acoeslista"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" id="acoeslista" name="acoeslista[]" value="visualizar">Visualizar&nbsp;&nbsp;
                <input <?= $loadBanco ? in_array("excluir", $arrCampos["acoeslista"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" id="acoeslista" name="acoeslista[]" value="excluir">Excluir&nbsp;&nbsp;

                <br>
                <br>

                <p>11) Defina o número de cadastros simultâneos do formulário:</p>

                <input class="input_text" type="text" id="numeroCadastros" name="numeroCadastros" value="<?= $arrCampos["numeroCadastros"] ? $arrCampos["numeroCadastros"] : "1" ?>" size="2">

                <br>
                <br>

                <p>12) Defina o que será gerado:</p>

                <input <?= $loadBanco ? isset($arrCampos["gerarDAO"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarDAO" value="1"> Gerar DAO <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarEXT"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarEXT" value="1"> Gerar EXTDAO <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarForm"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarForm" value="1"> Gerar Formulário <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarList"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarList" value="1"> Gerar List <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarFiltro"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarFiltro" value="1"> Gerar Filtro <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarAjaxList"]) ? "checked=\"checked\"" : ""  : "" ?> type="checkbox" name="gerarAjaxList" value="1"> Gerar Ajax List <br />
                <br>
                <br>
                <input type="checkbox" checked="checked" name="overwriteDAO" value="1"> Sobrescrever DAO <br />
                <input type="checkbox" checked="checked" name="overwriteEXT" value="1"> Sobrescrever EXTDAO <br />
                <input type="checkbox" checked="checked" name="overwriteForm" value="1"> Sobrescrever Formulário <br />
                <input type="checkbox" checked="checked" name="overwriteList" value="1"> Sobrescrever List <br />
                <input type="checkbox" checked="checked" name="overwriteFiltro" value="1"> Sobrescrever Filtro <br />
                <input type="checkbox" checked="checked" name="overwriteAjaxList" value="1"> Sobrescrever Ajax List <br />
                <br>
                <br>

                <input class="botoes_form" type="submit" name="s1" value="Gerar Arquivos & Salvar Dados">
                <input type="hidden" name="f" value="formshowed">

            </form>

        </body>

        <script language="javascript">

            automatico("<?= Helper::GET("tabela") ?>", "<?= Helper::GET("condicaoChave") ?>", "<?= Helper::GET("condicaoUnderline") ?>");

        </script>

        </html>

    <? } ?>

    <?
} elseif (isset($_REQUEST["todas"]) && $_REQUEST["todas"] != "") {

    echo "<html>
             <head>
                  <link type=\"text/css\" rel=\"stylesheet\" href=\"padrao.css\" />
              <title>GERADOR DE PROJETOS OMEGA SOFTWARE - TODAS AS TABELAS</title>
             </head>
    ";

    

    
    
    $dbBibliotecaNuvem->query("SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT=$idProjetos");
//    $valores = $dbBibliotecaNuvem->getPrimeiraTuplaDoResultSet(0);
    
    $arrModificacoes = $_POST;
    if(!empty($valores )) $arrModificacoes = $valores ;
    $dbProjeto->query("SHOW TABLES");
    $tabelaBancoProjeto = Helper::getResultSetToArrayDeUmCampo($dbProjeto->result);
    $tabelasEncontradas = array();
    
    while ($row = mysqli_fetch_array($dbBibliotecaNuvem->result)) {

        $arrCampos = unserialize(html_entity_decode($row[0]));

        $_POST = $arrCampos;

        $tabela = $_POST["tablename"];
        if(!in_array($tabela, $tabelaBancoProjeto)) 
                continue;
        else 
            $tabelasEncontradas[count ($tabelasEncontradas)] = $tabela;
        $class = $_POST["classname"];
        $key = $_POST["keyname"];
        $label = $_POST["labelname"];
        $ext = $_POST["extname"];
        $numeroRegs = $_POST["numeroCadastros"];

        if ($arrModificacoes["gerarDAO"] == "1"){
//            echo "CHAVE: ";
//            print_r($key);
//            echo "<br/>";
//            
//            echo "LABEL: ";
//            print_r($label);
//            echo "<br/>";
//            
//            echo "EXT: ";
//            print_r($ext);
//            echo "<br/>";
//            exit();
            $objGWD = new Gerador_web_dao();
            $objGWD->gerarDAOWeb(
                $vetorTabelaBancoOrdenada, $tabela, $class, $key, $label, $ext, "1");
        }
        if ($arrModificacoes["gerarEXT"] == "1"){
            $objGWE = new Gerador_web_extdao();
            $objGWE->gerarEXTDAOWeb($tabela, $class, $key, $label, $ext, "0");
        }
        if ($arrModificacoes["gerarForm"] == "1"){
         
            if(is_array($_POST["tabelasrelacionamento"])){
                
                gerarFormMultiplo($tabela, $class, $key, $label, $ext, "1", $numeroRegs);
                
            }
            else{
            
                gerarForm($tabela, $class, $key, $label, $ext, "1", $numeroRegs);

            }            
            
        }
            
        if ($arrModificacoes["gerarFiltro"] == "1")
            gerarFiltro($tabela, $class, $key, $label, $ext, "1", $_POST["camposfiltro"]);

        if ($arrModificacoes["gerarList"] == "1")
            gerarList($tabela, $class, $key, $label, $ext, "1", $_POST["camposlista"], $_POST["acoeslista"], $_POST["camposfiltro"]);


        if ($arrModificacoes["gerarAjaxList"] == "1") {

            gerarAjaxForm($tabela, $class, $key, $label, $ext, "1", $numeroRegs);

            gerarAjaxPage($tabela, $class, $key, $label, $ext, "1", $numeroRegs);
        }

        echo "<br /><br />";
    }
    for($k = 0 ; $k < count($tabelaBancoProjeto); $k++)
    if(!in_array($tabelaBancoProjeto[$k], $tabelasEncontradas)) 
        Helper::imprimirMensagem ("Arquivo não foi gerado, pois a tabela não foi configurada: ".$tabelaBancoProjeto[$k], MENSAGEM_ERRO);
} else {

    
    $tabela = $_REQUEST["tablename"];
    $class = $_REQUEST["classname"];
    $key = $_REQUEST["keyname"];
    $label = $_REQUEST["labelname"];
    $ext = $_REQUEST["extname"];
    $numeroRegs = $_REQUEST["numeroCadastros"];

    $todosCampos = htmlentities(serialize($_POST));

    
    $dbBibliotecaNuvem->query("SELECT id FROM configs_salvas WHERE projetos_id_INT=$idProjetos AND tabela_nome='$tabela'");

    if ($dbBibliotecaNuvem->rows() > 0) {

        $idConfig = Database::mysqli_result($dbBibliotecaNuvem->result, 0, 0);

        $dbBibliotecaNuvem->query("UPDATE configs_salvas SET valores_campos_texto='$todosCampos' WHERE id=$idConfig");
    } else {

        $dbBibliotecaNuvem->query("INSERT INTO configs_salvas VALUES (null, $idProjetos, '$tabela', '$todosCampos')");
    }

    echo "<html>
             <head>
                      <link type=\"text/css\" rel=\"stylesheet\" href=\"padrao.css\" />
                      <title>GERADOR DE PROJETOS OMEGA SOFTWARE</title>
             </head>
	";

    $sobrescreverDAO = $_POST["overwriteDAO"];
    $sobrescreverEXT = $_POST["overwriteEXT"];
    $sobrescreverForm = $_POST["overwriteForm"];
    $sobrescreverFiltro = $_POST["overwriteFiltro"];
    $sobrescreverAjaxList = $_POST["overwriteAjaxList"];

    if ($_POST["gerarDAO"] == "1"){
        $objGerador = new Gerador_web_dao();
        $objGerador->gerarDAOWeb($vetorTabelaBancoOrdenada, $tabela, $class, $key, $label, $ext, $sobrescreverDAO);
    }
    if ($_POST["gerarEXT"] == "1")
        gerarEXTDAO($tabela, $class, $key, $label, $ext, $sobrescreverEXT);

    if ($_POST["gerarForm"] == "1"){
     
        if(is_array($_POST["tabelasrelacionamento"])){
                
            gerarFormMultiplo($tabela, $class, $key, $label, $ext, $sobrescreverForm, $numeroRegs);

        }
        else{

            gerarForm($tabela, $class, $key, $label, $ext, $sobrescreverForm, $numeroRegs);

        } 
      
    }
        
    if ($_POST["gerarAjaxList"] == "1") {

        gerarAjaxPage($tabela, $class, $key, $label, $ext, $sobrescreverAjaxList, $numeroRegs);
        gerarAjaxForm($tabela, $class, $key, $label, $ext, $sobrescreverAjaxList, $numeroRegs);
    }

    if ($_POST["gerarFiltro"] == "1")
        gerarFiltro($tabela, $class, $key, $label, $ext, $sobrescreverFiltro, $_POST["camposfiltro"]);

    if ($_POST["gerarList"] == "1")
        gerarList($tabela, $class, $key, $label, $ext, $sobrescreverFiltro, $_POST["camposlista"], $_POST["acoeslista"], $_POST["camposfiltro"]);
    ?>

    <center><input class="botoes_form" type="button" onclick="javascript:history.back()" value="Voltar"></center>

<? } ?>

