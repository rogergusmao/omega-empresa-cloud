<?php

function pagina_informacao_mobile($idM){

    $objM = new EXTDAO_Mobile();
    $objM->select($idM);

    $identificadores = EXTDAO_Mobile::getInformacoesMobileIdentificadorDoMobile($idM);

?>

<table class="tabela_list">
    <tr>

        <?
        $raiz = Helper::acharRaiz();
        $img = EXTDAO_Tipo_banco::getImg($raiz, $identificadores[0]["tipoBanco"]);
        ?>

        <td rowspan="<?=(count($identificadores) + 1);?>" style="padding-top: 5px; color: #333333;">
            <?=$img;?>
        </td>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">
            <?=$objM->getIdentificador();?>
        </td>
    </tr>
    <tr>
        <td>
            Id Mobile
        </td>
        <td>
            <?=$idM;?>
        </td>
    </tr>
    <tr>
        <td>
            <?php

                for($i = 0 ;$i < count($identificadores); $i++){
                    $informacoesIdentificador = $identificadores[$i];
                    $getMobileIdentificador = Param_Get::ID_MOBILE_IDENTIFICADOR."=".$informacoesIdentificador["id"]
                        ."&id_mobile=".$informacoesIdentificador["idMobile"];

            ?>
                <table width="350" align="center" class="tabela_popup">

                     <tr>
                        <td>
                            Id Mobile Identificador
                        </td>
                        <td>
                            <?=$informacoesIdentificador["id"];?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Produto
                        </td>
                        <td>
                            <?=$informacoesIdentificador["produto"];?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vers�o
                        </td>
                        <td>
                            <?=$informacoesIdentificador["versao"];?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Conexao
                        </td>
                        <td>
                            <?=$informacoesIdentificador["softwareEmExecucao"];?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Suporte
                        </td>
                        <td>
                        <?=$informacoesIdentificador["processandoOperacaoSistema"] ? "Ativo" : "Inativo";?>
                            <?php
                            if(!$informacoesIdentificador["processandoOperacaoSistema"]){
                                ?>
                            &nbsp;
                                <a target="_self" href="javascript:abrirNovaJanela('pages', 'status_inicia_suporte', '<?=$getMobileIdentificador."&".Param_Get::ID_TIPO_OPERACAO_SISTEMA."=4";?>');">
                                    Ativar suporte
                                </a>
                                    <?php
                            }
                            ?>
                        </td>
                    </tr>
                        <tr>
                            <td>
                                Ultima Operacao De Sistema Executada
                            </td>
                            <td>
                            <?=$informacoesIdentificador["statusSuporte"];?>
                            </td>
                        </tr>
                        <tr><td>
                                <a target="_self" href="javascript:abrirNovaJanela('forms', 'gerencia_operacao_sistema_mobile_filho', '<?=$getMobileIdentificador."&".Param_Get::ID_TIPO_OPERACAO_SISTEMA."=".EXTDAO_Tipo_operacao_sistema::DOWNLOAD_BANCO_SQLITE_MOBILE;?>');">
                                    Download banco de dados sqlite
                                </a>
                            </td>
                            <td>
                                <a target="_self" href="javascript:abrirNovaJanela('forms', 'gerencia_operacao_download_banco_mobile', '<?=$getMobileIdentificador;?>');">
                                    Download banco de dados como sql
                                </a>
                            </td>
                            <td>
                                <a target="_self" href="javascript:abrirNovaJanela('lists', 'gerencia_sistema_produto_log_erro_mobile_identificador', '<?=$getMobileIdentificador;?>');">
                                    Log de erro
                                </a>

                            </td>
                            <td>
                                <a target="_self" href="javascript:abrirNovaJanela('forms', 'gerencia_monitorar_tela', '<?=$getMobileIdentificador;?>');">
                                    Monitorar tela
                                </a>

                            </td>
                            <td>
                                <a target="_self" href="javascript:abrirNovaJanela('forms', 'gerencia_operacao_sistema_mobile_filho', '<?=$getMobileIdentificador."&".Param_Get::ID_TIPO_OPERACAO_SISTEMA."=4";?>');">
                                    Sincronizar Dados do Telefone
                                </a>
                            </td>
                            <td>
                                <a target="_self" href="javascript:abrirNovaJanela('forms', 'reinstalar_versao_app_mobile', '<?=$getMobileIdentificador."&".Param_Get::ID_TIPO_OPERACAO_SISTEMA."=4";?>');">
                                    Reinstalar Vers�o
                                </a>
                            </td>
                        </tr>

                </table>

                <?
                }
                ?>

        </td>
    </tr>
</table>

<?php

}

?>
