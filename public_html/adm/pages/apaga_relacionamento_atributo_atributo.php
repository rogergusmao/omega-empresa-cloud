
    
    
    <?php

/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos
 * NOME DA CLASSE DAO: DAO_Projetos
 * DATA DE GERA��O:    09.01.2013
 * ARQUIVO:            EXTDAO_Projetos.php
 * TABELA MYSQL:       projetos
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */
$PROCESSO_INICIALIZA = 0;
$PROCESSO_ATUALIZA_CHAVE_DAS_TABELAS = 1;
$PROCESSO_FINALIZA = 2;


$idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
$idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);


$varGET = Param_Get::getIdentificadorProjetosVersao();



$strVetorId = Helper::GET(Param_Get::VETOR_ID);
$vetorId = array();
if (strlen($strVetorId))
    $vetorId = Helper::explode("[,]", $strVetorId);
$idPVCaminhoAtual = Helper::GET(Param_Get::ID_PROCESSO_ATUAL);
if (!strlen($idPVCaminhoAtual)) {
    $idPVCaminhoAtual = $PROCESSO_INICIALIZA;
}


$indiceAtual = Helper::GET(Param_Get::INDICE_ATUAL);
$vIdBancoBanco = Helper::GET(Param_Get::BANCO_BANCO);

$msgPorcentagem = "";
if (count($vetorId)) {
    $porcentagem = ($indiceAtual / count($vetorId)) * 100;
    $msgPorcentagem = "</br>ANDAMENTO:\t\t $porcentagem %";
}
Helper::imprimirMensagem("PROCESSO:\t {$idPVCaminhoAtual} / {$PROCESSO_FINALIZA}{$msgPorcentagem}", MENSAGEM_INFO);


$cicloPagina = true;
switch ($idPVCaminhoAtual) {
    case $PROCESSO_INICIALIZA:


        $idPVCaminhoAtual += 1;
        break;
    case $PROCESSO_ATUALIZA_CHAVE_DAS_TABELAS:
        if(empty($vetorId)){
            //carregando a estrutura de atributos das tabelas
            
            $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
            $objPVBB->select($idPVBB);
            $vetorId = $objPVBB->getVetorAtributoAtributo();
            $indiceAtual = 0;
        }
        $obj = new EXTDAO_Atributo_atributo();
        for($i = 0 ; $i < 5; $i++){
            if($vetorId != null)
            foreach ($vetorId as $id) {
                $obj->delete($id);
            }
         if($indiceAtual >= count($vetorId)){
                $indiceAtual = null;
                $vetorId = null;
                $idPVCaminhoAtual += 1;
                break;
            } else{
                $indiceAtual += 1;
            }
        }

        break;


    case $PROCESSO_FINALIZA:

        $cicloPagina = false;
        Helper::imprimirMensagem("Estrutra de atributo apagada.");

        break;

    default:
        $idPVCaminhoAtual += 1;
        break;
}

if ($cicloPagina) {
    //gravando o estado atual no variavel GET
    $strVetor = Helper::vetorToString($vetorId, ",");

    $varGET = Helper::formataGET(
                    array(
                Param_Get::VETOR_ID,
                Param_Get::INDICE_ATUAL,
                Param_Get::ID_PROCESSO_ATUAL,
                        Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                        Param_Get::ID_PROJETOS_VERSAO
                    ), array(
                $strVetor,
                $indiceAtual,
                $idPVCaminhoAtual,
                    Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO),
                    Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO)
                    )
    );

    sleep(1);
    Helper::mudarLocation("index.php?page=apaga_relacionamento_atributo_atributo&tipo=pages&" . $varGET);
    //return array("location: index.php?page=compara_banco_hom_prod&tipo=pages&".$varGET);
} 
?>	
?>	