<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){

    $objPE = $objPVPE->getObjProcessoEstrutura();
    $json = $objPE->getJson_constante();
    $idTipoScriptBanco = null;
    if(strlen($json)){
        $objJson = json_decode($json);    
        $idTipoScriptBanco =  $objJson->idTipoScriptBanco;
    } else {
        $idTipoScriptBanco = EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD;
    }
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB,
        $idTipoScriptBanco);
    if(empty($vetorId)){
       $vetorId = EXTDAO_Script_tabela_tabela::getListaId($objGSBB->objScriptProjetosVersaoBancoBanco->getId());
       $indiceAtual = 0;
    }


    $objSTT = new EXTDAO_Script_tabela_tabela();
    for($i = 0 ; $i < 80; $i++){

       if($indiceAtual < count($vetorId)){
           $idSTT = $vetorId[$indiceAtual];
           if(strlen($idSTT))
            $objSTT->delete($idSTT);
       }

       if($indiceAtual >= count($vetorId)){
           break;
       } else{
           $indiceAtual += 1;
       }

    }

    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." comandos apagados ate o momento.");
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	