<?php

/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos
 * NOME DA CLASSE DAO: DAO_Projetos
 * DATA DE GERA��O:    09.01.2013
 * ARQUIVO:            EXTDAO_Projetos.php
 * TABELA MYSQL:       projetos
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */



if(strlen(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO))){
    $idSP = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO);
    
    
    $idSPVP = EXTDAO_Sistema_projetos_versao_produto::getIdDaUltimaVersao($idSP);
    
    //$objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP->select($idSPVP);
    $idPVBB = $objSPVP->getServidor_projetos_versao_banco_banco_id_INT();
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $idPV = $objPVBB->getProjetos_versao_id_INT();
    
} else{
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);    
}

$iniciandoEstrututa = true;
if(strlen(Helper::GET(Param_get::PROCESSO_INICIADO)))
    $iniciandoEstrututa = false;        



$isNumerico = "false";

$strVetorId = Helper::GET(Param_Get::VETOR_ID);
if(strlen($strVetorId)){
    if(!strlen(Helper::GET(Param_Get::NUMERICO) || Helper::GET(Param_Get::NUMERICO) == "false")){
        $vetorId = array();
        if (strlen($strVetorId)){
            $vetorId = Helper::explode("[,]", $strVetorId);
            $indiceAtual = Helper::GET(Param_Get::CICLO_ATUAL);
            if($indiceAtual == null || !strlen($indiceAtual))
                $indiceAtual = 0;
        }
    } else{
        $vetorId = $strVetorId;
        $isNumerico = "true"; 
    }
    
}

$indiceAtual = Helper::GET(Param_Get::INDICE_ATUAL);


$msgPorcentagem = "";
if (count($vetorId)) {
    if(is_numeric($vetorId)){
        $porcentagem = ($indiceAtual / $vetorId) * 100;
    }
    else
        $porcentagem = ($indiceAtual / count($vetorId)) * 100;
    $msgPorcentagem = "</br>EXECUTADO:\t\t $porcentagem %";
} else{
    $msgPorcentagem = "</br>EXECUTADO:\t\t 0 %";
}



$cicloPagina = true;
if(! $iniciandoEstrututa){
    $outProcessoConcluido = false;
    $outCicloAtual = 0;
    
    $paginaEnc = Helper::GET(Param_Get::PAGINA);
    $pagina = Helper::decodeURLGet($paginaEnc);
    if(strlen($pagina)){

        $objPVBBTeste = new EXTDAO_Projetos_versao_banco_banco;
        $objPVTeste = new EXTDAO_Projetos_versao();
        
        $idPVBBTeste = $idPVBB;
        
        if(strlen($idPVBBTeste)){
            $objPVBBTeste->select($idPVBBTeste);
            $idPVTeste = $objPVBBTeste->getProjetos_versao_id_INT();
            $objPVTeste->select($idPVTeste);
        }
        
        
        include($pagina);
        $statusProcesso = executa_pagina_processo($objPVPE, $objPVTeste, $objPVBBTeste, $vetorId, $indiceAtual, $idPVCaminhoAtual);
        
        if( $statusProcesso->outProcessoConcluido != true){
            $vetorId = $statusProcesso->vetorId;
            $indiceAtual = $statusProcesso->indiceAtual;
            if(is_numeric($vetorId)){
                $isNumerico = "true"; 
                if($indiceAtual >= $vetorId){
                    $outProcessoConcluido = true;
                }
            } else{
                if($indiceAtual >= count($vetorId)){
                    $outProcessoConcluido = true;
                }
            }
            $outCicloAtual = $indiceAtual;
        } else $outProcessoConcluido = true;
        
        if(is_numeric($vetorId))
            $strVetor = $vetorId;
        else 
            $strVetor = Helper::vetorToString($vetorId, ",");
        
        if($outProcessoConcluido){
            $cicloPagina = false;
        }
        
    } else{
        Helper::imprimirMensagem("Pagina vazia no parametro GET 'pagina'.");
        $cicloPagina = false;
    }
        
}

if ($cicloPagina) {
    //gravando o estado atual no variavel GET


    
    $varGET = Helper::formataGET(
              array(
                    Param_get::PROCESSO_INICIADO,
                    Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                    Param_get::ID_PROJETOS_VERSAO,
                    Param_Get::PAGINA,
                    Param_Get::VETOR_ID,
                    Param_Get::PROXIMA_PAGINA,
                    Param_Get::NUMERICO,
                    Param_Get::INDICE_ATUAL
              ), array(
                    "true",
                    Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO),
                    Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO),
                    Helper::GET(Param_Get::PAGINA),
                    $strVetor,
                    Helper::GET(Param_Get::PROXIMA_PAGINA),
                    $isNumerico,
                  $indiceAtual
              )
        );
    $url = "index.php?page=executa_processo_pagina&tipo=pages&" . $varGET;
    Helper::mudarLocation($url);
    //return array("location: index.php?page=compara_banco_hom_prod&tipo=pages&".$varGET);
} else{

    $varGET = Helper::formataGET(
              array(
                Param_get::PROCESSO_INICIADO,
                Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
                Param_get::ID_PROJETOS_VERSAO
              ), array(
                "true",
                Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO),
                Helper::POSTGET(Param_get::ID_PROJETOS_VERSAO)
              )
        );
    $proximaPagina = Helper::GET(Param_Get::PROXIMA_PAGINA);
    $proximaPagina = Helper::decodeURLGet($proximaPagina );
    $url = "$proximaPagina&" . $varGET;
    
    if(strlen($proximaPagina)){
        sleep(4);
        Helper::mudarLocation($url);
    }
    
}

?>	