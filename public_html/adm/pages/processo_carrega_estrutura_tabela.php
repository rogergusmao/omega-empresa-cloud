<?php


function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $idBB = $objPVBB->getBanco_banco_id_INT();
    $objBB = new EXTDAO_Banco_banco();
    $objBB->select($idBB);

    //$objPV = new EXTDAO_Projetos_versao();
    switch ($objPV->getTipo_analise_projeto_id_INT()) {
        case EXTDAO_Tipo_analise_projeto::ATUALIZANDO_O_BANCO_DO_SINCRONIZADOR_WEB:
            //setando o prefixo das tabelas do Banco Sincronizador Web referentes as tabelas do Banco Web
            $objBB->setPrefixoNomeTabelaProducao("__");
            break;
        default:
            break;
    } 
    
    //carregando o estado dos atributos
    if ($vetorId == null || empty($vetorId)) {
        $vetorId = array(0, 1, 2, 3);
        $indiceAtual = 0;
    }
    $vMensagem = "";
    for ($i = 0; $i < 1; $i++) {
        if ($indiceAtual < count($vetorId)) {

            //carregando a estrutura da tabela
            switch ($indiceAtual) {
                case 0:
                    $objBB->populaTabelaDoProjetoVersaoDoBancoDeProducao($objPV->getId());
                    $vMensagem = "Tabelas do banco de produ��o carregadas";
                    break;
                case 1:

    //TODO responsavel por popular as tabela 'tabela' relativo ao banco de homologacao do respectivo tipo e projeto
                    //nesse caso � realizada a copia da estrutura do banco de produ��o para de homologa��o
                    $objBB->populaTabelaDoProjetoVersaoDoBancoDeHomologacao($objPV->getId());
                    $vMensagem = "Tabelas do banco de homologa��o carregadas";
                    break;
                case 2:
                    $objBB->populaTabelaTabelaDoProjetoVersao($objPV->getId());
                    $vMensagem = "Mapeando relacionamento entre as tabelas do banco de homologa��o e produ��o.";
                    break;
                case 3:
                    //Retira as tabelas inexistentes no banco e que estao cadastradas 
                    //na tabela 'Tabela' da BibliotecaNuvem
                    //=> toda tupla tabela_tabela tb eh retirada
                    $objBB->retiraTabelasInexistentes($objPV->getId());
                    $vMensagem = "Checando dados.";
                    break;

                default:
                    $vMensagem = "Estado n�o mapeado";
                    break;
            }
        }
        if ($indiceAtual >= count($vetorId)) {

            break;
        } else {
            $indiceAtual += 1;
        }
    }

    Helper::imprimirMensagem($vMensagem);
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	