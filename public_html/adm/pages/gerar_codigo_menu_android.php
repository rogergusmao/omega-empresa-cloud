<?php
    
    $sobrescrever = true;
    set_time_limit(40 * 60);    
    
    $class = "MenuDoPrograma";
    
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    
    $varGET= Param_Get::getIdentificadorProjetosVersao();
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $idPV= $objPVBB->getProjetos_versao_id_INT();
    $objPV = new EXTDAO_Projetos_versao();
    $objPV->select($idPV);
    
    
    
    
    $objDiretorio = new EXTDAO_Diretorio_android();
    $objDiretorio->select(EXTDAO_Diretorio_android::getIdDiretorio($idPVBB));
    $raizSrc = $objDiretorio->getRaizSrc();

    $packageRaiz = str_replace("/", ".", Helper::getPathSemBarra($raizSrc));
    $packageAppConfiguration = "package app.omegasoftware.pontoeletronico.appconfiguration;";
    
    $raizDatabase  = Helper::getPathSemBarra($raizDatabase);
    $packageDatabase = str_replace("/", ".", $raizDatabase);
    
    $diretorio = "app/omegasoftware/pontoeletronico/appconfiguration/";
    $packageDAO = str_replace("/", ".", Helper::getPathSemBarra($diretorio));
    
   
    $dir = dirname(__FILE__);

    $filename = Helper::getPathComBarra($raizSrc) .Helper::getPathComBarra($diretorio) . $class . ".java";


    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
        Helper::imprimirMensagem("Sobreescrevendo o arquivo já é exitente: ".$filename );
        
    }
    
    
    if(!file_exists($filename)){
$db = new Database();

//Seleciona todos os identificadores que são menus pais
    $q = "SELECT psm.pai_permissao_id_INT, psm.id, psm.tag
        FROM permissao_sistema_mobile psm
        WHERE psm.tipo_permissao_id_INT IN (
            ".EXTDAO_Tipo_permissao::$MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA.", 
            ".EXTDAO_Tipo_permissao::$MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS.",
            ".EXTDAO_Tipo_permissao::$ACESSO_A_TELA."
        )
        ORDER BY psm.pai_permissao_id_INT
    ";
    
    $db->query($q);
    
    
    $dadosArvore = Helper::getResultSetToMatriz($db->result);
    $ultimoPai = null;
    
    $strFilhos = "";
    for($i = 0 ; $i < count($dadosArvore); $i++){
        $item = $dadosArvore[$i];
        $idPai = $item[0];
        
        if($ultimoPai == null && $i == 0)
            $ultimoPai = $idPai;
        if( $ultimoPai == $idPai){
            if(strlen($strFilhos))
                $strFilhos .= ", 
                    ";
            $strFilhos .= "PERMISSAO.".$item[2];
        } else if ($ultimoPai != $idPai || $i == count($dadosArvore) - 1){
            $strFilhos =  "new PERMISSAO [] {".$strFilhos."}";
            if(!strlen($ultimoPai)) $ultimoPai="ConstantesMenuPrograma.ID_RAIZ";
            $strArvore .= "
                singletonArvoreIdPermissoes.put(".$ultimoPai.", $strFilhos);
                    ";  
            $strFilhos = "PERMISSAO.".$item[2];
            $ultimoPai = $idPai;
        }
         
    }
//    
//    
//     $strArvore = "";
//        $str = "";
//        for($i = 0 ; $i < count($matrix); $i++){
//            $item = $matrix[$i];
//            $enumNome = $item[2];
//            if(strlen($enumNome)){
//                $str = "singletonArvoreIdPermissoes.put(";
//                $idPai = "";
//                if(!strlen($item[3])){
//                    $idPai = "ID_RAIZ";
//                } else {
//                    //id
//                    $idPai = $item[0];
//                }
//                $str .= "$idPai, new PERMISSAO [] {PERMISSAO.$enumNome } );
//                        ";
//                $strArvore .= $str;
//            }
//            
//        }
//        
        
        
    
    
    
        $db->query("SELECT id, nome, tag, pai_permissao_id_INT, tipo_permissao_id_INT
            FROM permissao_sistema_mobile 
            WHERE projetos_versao_banco_banco_id_INT = $idPVBB
            ORDER BY pai_permissao_id_INT ");
        $matrix = Helper::getResultSetToMatriz($db->result, false);
        
        $strEnumPermissao = "
	public enum PERMISSAO {";
        
        $str = "";
        for($i = 0 ; $i < count($matrix); $i++){
            $item = $matrix[$i];
            $enumNome = $item[2];
            
            
            if(strlen($enumNome)){
                
                switch ($item[4]){
                    case EXTDAO_Tipo_permissao::$ACESSO_A_FUNCIONALIDADE_DO_SISTEMA:
                           $str .= " 
            $enumNome (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    $item[0],
                    null,
                    \"$item[1]\",
                    null,
                    \"$item[2]\",
                    null )";
                        break;
                    case EXTDAO_Tipo_permissao::$ACESSO_A_TELA:
                        $str .= " 
            $enumNome (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    $item[0],
                    null,
                    \"$item[1]\",
                    $item[2].class,
                    \"$item[2]\",
                    null )";
                        break;
                    case EXTDAO_Tipo_permissao::$MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA:
                      
                $str .= " 
            $enumNome  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA,
                    $item[0],
                    null,
                    \"$item[1]\",
                    null,
                    \"$item[2]\",
                    null )";
                        break;
                    case EXTDAO_Tipo_permissao::$MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS:

                $str .= "
            $enumNome  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    $item[0],
                    null,
                    \"$item[1]\",
                    null,
                    \"$item[2]\",
                    null )";
                        break;
                    default:
                        break;
                }
                    if($i < count($matrix) - 1) $str .= ", ";

                    $str .= "
                        ";
            }
        }
        $strEnumPermissao .= "
                $str ;
                final public ItemMenu item;
		private PERMISSAO(
				ConstantesMenuPrograma.TIPO_PERMISSAO tipoPermissao,
				int id,
				int drawable[],
				String nome,
				Class<?> classe,
				String tag,
				Integer rIdStringNome) {
			item = new ItemMenu(
					tipoPermissao,
					id,
					drawable,
					nome,
					classe,
					tag,
					rIdStringNome);
			
		}
	 
	 
	}";
       
        
    // open file in insert mode
    $file = fopen($filename, "w+");
    $filedate = date("d.m.Y");

    $c = "";

    $c = "

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  $class
    * DATA DE GERAÇÃO: $filedate
    * ARQUIVO:         $class.java
    * -------------------------------------------------------
    *
    */
    
    $packageAppConfiguration

    import java.util.HashMap;
    
";
        
        
    $c .= "
        
       
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

	
    
    public class MenuDoPrograma {

    ";
    $c.= $strEnumPermissao;
    $c .= "    
    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************
    
        
	//Armazena a arvore de permissoes
	static HashMap<Integer, PERMISSAO[]> singletonArvoreIdPermissoes;

        

	public static HashMap<Integer, PERMISSAO[]> getArvoreMenu(){
        if(singletonArvoreIdPermissoes != null) return singletonArvoreIdPermissoes;
        singletonArvoreIdPermissoes = new HashMap<Integer, PERMISSAO[]>();
                $strArvore
		return singletonArvoreIdPermissoes;
	}
    ";
   
    
    $c.= "

    } // classe: fim
    

    ";
    fwrite($file, $c);
    fclose($file);

	
	$strGerouExt = "<p class=\"mensagem_retorno\">
		&bull;&nbsp;&nbsp;Classe <b>$class</b> gerada com sucesso no arquivo <b>$class.php</b>.
                     &nbsp;Path: $filename
		</p>";

    print $strGerouExt;


    }



?>
