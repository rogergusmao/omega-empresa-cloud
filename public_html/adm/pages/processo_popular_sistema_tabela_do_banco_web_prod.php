<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    
    
    $db = new Database();
    //$objPVBB->getFkObjBanco_banco()->atualizaSistemaTabelaDoBancoDeProducao($objPVBB->getProjetos_versao_id_INT());
    $idPVBBAndroid = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoAndroidDoProjetosVersao(
        $objPVBB->getProjetos_versao_id_INT(), 
        $db);
    $objPVBBAndroid = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBAndroid->select($idPVBBAndroid);
    $idPVAndroid = $objPVBBAndroid->getProjetos_versao_id_INT();
    //$objPVBB->select($idPVBBAndroid);
    $objBancoAndroid = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao(
        $idPVBBAndroid, $db);

    $idPVBBWeb = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoWebDoProjetosVersao(
        $objPVBB->getProjetos_versao_id_INT(), 
        $db);
    
    $objBancoWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBWeb, $db);
    $objPVBBWeb = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBWeb->select($idPVBBWeb);
    $idPVWeb =  $objPVBBWeb->getProjetos_versao_id_INT();
    //Atualizando o sistema_tabela do banco sincronizador_web
    $idPVBBSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getIdPVBBSincronizadorWebHmgParaProd($objPVBB->getProjetos_versao_id_INT(), $db);
    $objBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao($idPVBBSincronizadorWeb);
    
    
    EXTDAO_Sistema_tabela::atualizaSistemaTabelaDoBanco(
        $objBancoWeb, 
        $objBancoAndroid, 
        $objBancoSincronizadorWeb, 
        $idPVWeb,
        $idPVAndroid,
        $db);
    
            
    

    $varGET= Param_Get::getIdentificadorProjetosVersao();
    
    
    
    sleep(5);
    
    Helper::imprimirMensagem("Database.php gerado com sucesso");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
