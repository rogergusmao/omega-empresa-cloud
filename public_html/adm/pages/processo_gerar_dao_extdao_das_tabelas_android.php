<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){

   
    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: sistema_projetos_versao_produto
    * DATA DE GERA��O:    17.06.2013
    * ARQUIVO:            sistema_projetos_versao_produto.php
    * TABELA MYSQL:       sistema_projetos_versao_produto
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
    //Aten��o o tipo de an�lise que chamou esse projeto � a sincroniza��o(2)
    //nesse caso o relacionamento ocorre 
    //hmg->prd
    //banco web hmg -> banco android hmg
    
    $db = new Database();
    $trunk = Trunk::factory();
    
    $idPVBBPrototipoAndroid = $trunk->getPVBBDoBancoDeDadosDeProducao($objPVBB->getId());
//    echo "{$objPVBB->getId()}:: $idPVBBPrototipoAndroid";
//    exit();
    
    $objPVBBPrototipoAndroid = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBBPrototipoAndroid->select($idPVBBPrototipoAndroid);
    
    $objPVPrototipoAndroid = new EXTDAO_Projetos_versao();
    $objPVPrototipoAndroid->select($objPVBBPrototipoAndroid->getProjetos_versao_id_INT());
    
    $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($objPVBBPrototipoAndroid->getId());
    $vetorTabelaBancoOrdenada = EXTDAO_Tabela::getHierarquiaTabelaBanco(
        $objPVPrototipoAndroid->getId(), $idBanco);
    
    $objGerador = new Gerador_android_dao();
    $objGAE = new Gerador_android_extdao();
//    print_r($vetorTabelaBancoOrdenada);
//    exit();
    for($i = 0 ; $i < count($vetorTabelaBancoOrdenada); $i++){
        $nomeClasseDAO = "DAO".str_replace("_", "", $vetorTabelaBancoOrdenada[$i]);
        $nomeClasseEXTDAO = "EXTDAO".str_replace("_", "", $vetorTabelaBancoOrdenada[$i]);
        
        $retDAO = $objGerador->gerarDAO(
            $vetorTabelaBancoOrdenada, 
            $vetorTabelaBancoOrdenada[$i], 
            $nomeClasseDAO, 
            "id", 
            "nome", 
            $nomeClasseEXTDAO, 
            true,
            $idPVBBPrototipoAndroid);
        
        $retEXTDAO = $objGAE->gerarEXTDAO (
            $vetorTabelaBancoOrdenada[$i], 
            $nomeClasseEXTDAO, 
            "id", 
            "nome", 
            $nomeClasseEXTDAO, 
            false,
            $idPVBBPrototipoAndroid);
        
        Helper::imprimirMensagem(print_r($retDAO, true) , MENSAGEM_INFO);
        Helper::imprimirMensagem(print_r($retEXTDAO, true) , MENSAGEM_INFO);
    }
    
    sleep(5);
    
    Helper::imprimirMensagem("As classes DAO's do projeto android foram geradas com sucesso");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
    

}

?>