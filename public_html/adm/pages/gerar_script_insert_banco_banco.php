<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos
    * NOME DA CLASSE DAO: DAO_Projetos
    * DATA DE GERA��O:    09.01.2013
    * ARQUIVO:            EXTDAO_Projetos.php
    * TABELA MYSQL:       projetos
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
   $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
   
   if(!strlen($idPVBB)){
       Helper::imprimirMensagem ("Falta o parametro projetos_versao_banco_banco", MENSAGEM_ERRO);
       return;
   }
   $vIsAtualizacao = "1";
   if(strlen(Helper::GET("atualizacao")))
       $vIsAtualizacao = Helper::GET("atualizacao");
   $vObjPVBB = new EXTDAO_Projetos_versao_banco_banco;
   $vObjPVBB->select($idPVBB);
   
   $vObj = new Gerador_script_banco_banco($vObjPVBB, $vIsAtualizacao);
   $vObj->geraScriptAtualizaTuplas();
?>	