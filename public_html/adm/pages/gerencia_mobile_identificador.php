<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: mobile_identificador
    * DATA DE GERA��O:    02.04.2013
    * ARQUIVO:            mobile_identificador.php
    * TABELA MYSQL:       mobile_identificador
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Mobile_identificador();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = "edit";
    $postar = "actions.php";
    $cont = 1;
    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_mobile_identificador">

    	<?


            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            $id = Helper::GET(Param_Get::ID_MOBILE_IDENTIFICADOR);

            $obj->select($id);
            $legend = "Dados da apk Android";


            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_mobile_id_INT;
    			$objArg->valor = $obj->getMobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMobile($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sistema_projetos_versao_sistema_produto_mobile_id_INT;
    			$objArg->valor = $obj->getSistema_projetos_versao_sistema_produto_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sistema_projetos_versao_sistema_produto_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_projetos_versao_sistema_produto_mobile($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_cadastro_DATETIME;
    			$objArg->valor = $obj->getData_cadastro_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_cadastro_DATETIME($objArg); ?></td>

            </tr>
        </table>

     </fieldset>
        
        <fieldset class="fieldset_form">
            <legend class="legend_form">Suporte</legend>

        <table class="tabela_form">

            <tr class="tr_form">

            	
    		<td class="td_form_campo">
                    <?
                    $estado = $obj->getEstadoConexaoMobileNoModoSuporte();
                    switch ($estado) {
                        case EXTDAO_Mobile_identificador::ESTADO_MOBILE_OFFLINE:
                            Helper::imprimirMensagem(
                                    "N�o � poss�vel ativar o suporte, pois o telefone est� desconectado.", 
                                    MENSAGEM_ERRO);
                            
                            break;
                        case EXTDAO_Mobile_identificador::ESTADO_CONECTANDO:
                            
                            $totMin = $this->getTotalSegundoRestanteProximaVerificacaoDoMobileDeSuporte() / 60;
                            Helper::imprimirMensagem(
                                    "Tentativa de conex�o em $totMin min.", 
                                    MENSAGEM_WARNING);
                            break;
                        case EXTDAO_Mobile_identificador::ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB:
                            Helper::imprimirMensagem(
                                    "N�o � poss�vel ativar o suporte, pois o software n�o est� sendo executado no telefone.", 
                                    MENSAGEM_ERRO);
                            
                            break;
                        case ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB:
                            Helper::imprimirMensagem(
                                "<a target=\"_self\" href=\"javascript:abrirNovaJanela('pages', 'status_inicia_suporte', '".Param_Get::ID_MOBILE_IDENTIFICADOR."=$id;');\">
                                    Ativar Suporte   
                                </a>", 
                                MENSAGEM_ERRO);
                            break;
                        case EXTDAO_Mobile_identificador::ESTADO_CONECTADO:
                            Helper::imprimirMensagem(
                                    "Suporte ativo.", 
                                    MENSAGEM_OK);
                            break;
                        case EXTDAO_Mobile_identificador::ESTADO_FALHA_CONEXAO :
                            Helper::imprimirMensagem(
                                "Falha na tentativa de conex�o. Se persistir o problema, comunique no nosso suporte. 
                                <a target=\"_self\" href=\"javascript:abrirNovaJanela('pages', 'status_inicia_suporte', '".Param_Get::ID_MOBILE_IDENTIFICADOR."=$id;');\">
                                    Clique aqui para tentar novamente.
                                </a>", 
                                MENSAGEM_ERRO);
                            break;
                        default:
//                        case EXTDAO_Mobile_identificador::ESTADO_SUPORTE_NAO_REQUISITADO_NA_WEB :
                            Helper::imprimirMensagem(
                                "<a target=\"_self\" href=\"javascript:abrirNovaJanela('pages', 'status_inicia_suporte', '".Param_Get::ID_MOBILE_IDENTIFICADOR."=$id;');\">
                                    Ativar Suporte   
                                </a>", 
                                MENSAGEM_ERRO);
                            break;

                        

                ?>

                    <?}?>
                </td>
            </tr>
        </table>

     </fieldset>


	

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

