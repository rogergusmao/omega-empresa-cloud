<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos_versao
    * NOME DA CLASSE DAO: DAO_Projetos_versao
    * DATA DE GERA��O:    29.01.2013
    * ARQUIVO:            EXTDAO_Projetos_versao.php
    * TABELA MYSQL:       projetos_versao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_publicar"] = "Clique aqui para publicar a vers�o";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Projetos_versao();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $idP = Helper::POSTGET(Param_Get::ID_PROJETOS);
    $objBanco = new Database();

    $consultaRegistros = "SELECT id 
        FROM projetos_versao 
        WHERE projetos_id_INT = {$idP}
           AND copia_do_projetos_versao_id_INT IS NULL
        ORDER BY nome";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Vers�es Do Projeto</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="17%" />
			<col width="17%" />
			
			<col width="17%" />
			<col width="17%" />
			<col width="17%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			
			<td class="td_list_titulos"><?=$obj->label_data_homologacao_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_producao_DATETIME ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
        $varGET = Param_Get::ID_PROJETOS_VERSAO."=".$regs[0]."&".Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO."=".$obj->getId();
        $url = "index.php?tipo=pages&page=seleciona_banco_banco&{$varGET}";
    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="<?=$url;?>" target="_self"><?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_homologacao_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_producao_DATETIME() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
                            <?
                            
                           if(!strlen($obj->getData_producao_DATETIME())){
                                ?>
                                <img border="0" src="imgs/icone_validar.png" onclick="javascript:confirmarExclusao('index.php?tipo=pages&page=publicar&<?=$varGET; ?>','<?=$acoes['mensagem_publicar'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_publicar'] ?>')" onmouseout="javascript:notip()">&nbsp;

                                &nbsp;
                                <?
                            }
                            ?>
                                <img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=gerencia_projetos_versao&id1=<?=$obj->getId(); ?>&<?=$varGET;?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                                &nbsp;
                            <img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Projetos_versao&action=remove&id=<?=$obj->getId(); ?>&<?=$varGET;?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?
	