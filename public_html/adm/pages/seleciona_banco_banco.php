<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       banco_banco
 * NOME DA CLASSE DAO: DAO_Banco_banco
 * DATA DE GERA��O:    29.01.2013
 * ARQUIVO:            EXTDAO_Banco_banco.php
 * TABELA MYSQL:       banco_banco
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

$idPV = Helper::GET(Param_Get::ID_PROJETOS_VERSAO);
//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
$acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
$acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
$acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";




$varGET = Helper::formataGET(
                array(Param_Get::ID_PROJETOS_VERSAO), array($idPV)
);
$objBanco = new Database();


$consultaRegistros = "SELECT tb.id id_tipo_banco, 
        bb.id,  
        tb.nome nome_tipo_banco, 
        b1.nome nome_banco_homologacao, 
        b2.nome nome_banco_producao, 
        pvbb.id 
                        FROM banco_banco bb JOIN projetos_versao_banco_banco pvbb ON bb.id = pvbb.banco_banco_id_INT
                                        LEFT JOIN banco b1 ON bb.homologacao_banco_id_INT = b1.id 
                                        LEFT JOIN banco b2 ON bb.producao_banco_id_INT = b2.id
                                        LEFT JOIN tipo_banco tb ON bb.tipo_banco_id_INT = tb.id
                        WHERE pvbb.projetos_versao_id_INT = " . $idPV . "
                        GROUP BY tb.id";

$objBanco->query($consultaRegistros);
?>



<fieldset class="fieldset_list">
    <legend class="legend_list">Lista de Bancos Do Projeto</legend>

    <table class="tabela_list">
        <colgroup>
            <col width="5%" />
            <col width="20%" />
            <col width="20%" />
            <col width="55%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">


                <td class="td_list_titulos">Tipo de Banco</td>
                <td class="td_list_titulos">Nome Banco Homologa��o</td>
                <td class="td_list_titulos">Nome Banco Produ��o</td>
                <td class="td_list_titulos">A��es</td>


            </tr>
        </thead>
        <tbody>

<?
for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

    $idPVBB = $regs[5];

    $varGETLocal = $varGET . Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO . "=" . $idPVBB;

    $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";
    ?>

                <tr class="<?= $classTr ?>">

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <a href="index.php?tipo=lists&page=gerencia_tabela_tabela&<?= $varGETLocal; ?>" target="_self" ><?= $regs[1] ?></a>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

    <?= $regs[2] ?>

                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $regs[3] ?>
                    </td>
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        </br>
                        <a href="index.php?tipo=lists&page=gerencia_projetos_versao_processo_estrutura&<?= $varGETLocal; ?>" target="_self" >Detalhes Processo</a>
                        </br>
                        <a href="index.php?tipo=pages&page=menu_banco_banco&<?= $varGETLocal ?>" target="_self" >Executar Processos</a>
                        </br>
                        <a href="index.php?tipo=pages&page=gerar_script_insert_banco_banco&<?= $varGETLocal; ?>" target="_self" >Gerar Script Atualiza Registro Hom Para Prod</a>

                        </br>
                        <a href="index.php?tipo=pages&page=arquivar_script_banco_banco&<?= $varGETLocal; ?>" target="_self" >Arquivar Script</a>
                        </br>
                        <a href="index.php?tipo=pages&page=carrega_sistema_tabela_do_banco_de_producao&<?= $varGETLocal; ?>" target="_self" >Carrega Tabelas Para Sincroniza��o</a>
                        </br>
                        <a href="index.php?tipo=pages&page=apaga_relacionamento_atributo_atributo&<?= $varGETLocal; ?>" target="_self" >Apaga Relacionamento Entre Atributo de Homologa��o e Produ��o</a>
                        </br>
                        <a href="index.php?tipo=pages&page=apaga_relacionamento_tabela_tabela&<?= $varGETLocal; ?>" target="_self" >Apaga Relacionamento Entre Tabela de Homologa��o e Produ��o</a>
                        </br>
                    </td>
                </tr>

<? } ?>

        </tbody>
    </table>

</fieldset>

<br/>
<br/>

    <?


	