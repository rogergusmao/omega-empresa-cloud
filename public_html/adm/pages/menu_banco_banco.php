<?php
$objPVBB = new EXTDAO_Projetos_versao_banco_banco();
if(strlen(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO))){
    $idSP = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO);
    
    
    $idSPVP = EXTDAO_Sistema_produto::getIdDaUltimaVersao($idSP);
    
    //$objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP->select($idSPVP);
    $idPVBB = $objSPVP->getServidor_projetos_versao_banco_banco_id_INT();
    
    $objPVBB->select($idPVBB);
    
    
    $idPV = $objPVBB->getProjetos_versao_id_INT();
    
} else{
    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    
    $objPVBB->select($idPVBB);
    $idPV = $objPVBB->getProjetos_versao_id_INT();
}
$varGET = Param_Get::getIdentificadorProjetosVersao(null, $idPV, $idPVBB);



if( !EXTDAO_Projetos_versao_processo_estrutura::validaListaProcessoEstrutura($idPVBB)){
    Helper::imprimirMensagem("O processo de atualização ainda não foi iniciado, <a target=\"_self\" href=\"".EXTDAO_Projetos_versao_processo_estrutura::getUrlDeInicializacaoDeProcesso($idPV, $idPVBB)."\" >clique aqui</a> para iniciar.", MENSAGEM_WARNING);
    exit();
}

if($objPVBB->isMobile()){
   $idMI = EXTDAO_Mobile_identificador::getMobileIdentificadorHospedeiro($idPVBB);
          Helper::imprimirMensagem("<p><a href=\"javascript:windowOpen('index.php?tipo=pages&page=gerar_arquivos_assets_do_projeto_android_para_versao_mobile&$varGET')\" target=\"_self\">Clique aqui</a> para gerar o arquivo de configurações do aplicativo para a nova versão de homologação.
               Após gerar o arquivo, gere a APK utilizando o eclipse, e execute o aplicativo no celular que será utilizado para os testes de homologação durante o processo de análise.</p>", MENSAGEM_INFO);
   if(!strlen($idMI)){
       Helper::imprimirMensagem("<a href=\"javascript:windowOpen('index.php?tipo=pages&page=seleciona_mobile_identificador&$varGET')\" target=\"_self\">Clique aqui</a> para selecionar um telefone para análise.", MENSAGEM_ERRO);
       

       exit();
   } else{
       include("pages/informacao_mobile_identificador.php");    
       pagina_informacao_mobile_identificador($idMI);
   }
}
$objPV = new EXTDAO_Projetos_versao();
$objPV->select($idPV);
$objBB = new EXTDAO_Banco_banco();
$objBB->select($objPVBB->getBanco_banco_id_INT());

Helper::imprimirMensagem(
 "Tipo Análise: ({$objPV->getFkObjTipo_analise_projeto()->getId()})" . $objPV->getFkObjTipo_analise_projeto()->getNome() . "<br/>"  
. " Projetos versão: ($idPV){$objPV->getNome()}</br> "
. " Projetos versão banco banco: ({$objPVBB->getId()}){$objPVBB->getNome()}</br> " 
. " Banco Homologação: ({$objBB->getFkObjHomologacao_banco()->getId()}){$objBB->getFkObjHomologacao_banco()->getNome()}</br> "
. " Banco Produção: ({$objBB->getFkObjProducao_banco()->getId()}){$objBB->getFkObjProducao_banco()->getNome()}</br> "

);


$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
$vetorProcessoEstrutura = EXTDAO_Projetos_versao_processo_estrutura::getListaIdPE($idPVBB);

//$vetorProcessoEstrutura = array(EXTDAO_Processo_estrutura::CARREGA_ESTRUTURA_BANCO_DE_HOM_PARA_PROD,
//        EXTDAO_Processo_estrutura::GERA_SCRIPT_ESTRUTURA_DO_BANCO_DE_PROD_COM_HOM,
//        EXTDAO_Processo_estrutura::ATUALIZA_ESTRUTURA_BANCO_DE_PROD,
//        EXTDAO_Processo_estrutura::COMPARAR_ESTRUTURA_BANCO_DE_PROD_COM_HOM
//    );

$cicloFinalizado = false;
$idPVPEAntigo = null;
$statusPEAnterior = null;
for ($h = 0 ; $h < count($vetorProcessoEstrutura); $h++){
    
    $retPE =  $vetorProcessoEstrutura[$h];
    $idPE = $retPE[0];
    $seq = $retPE[1];
    
    
    $idPVPE = EXTDAO_Projetos_versao_processo_estrutura::getIdDoProcessoEstrutura($idPVBB, $idPE);
    
    $objPVPE->select($idPVPE);
    
    if($statusPEAnterior == EXTDAO_Projetos_versao_caminho::EXECUTADO
        && $idPVPEAntigo != null 
        && $statusPEAnterior != EXTDAO_Projetos_versao_caminho::DESATUALIZADO){
        
        if($objPVPE->desatualizadoComparado($idPVPEAntigo))
            $statusPEAnterior = EXTDAO_Projetos_versao_caminho::DESATUALIZADO;
    } 
    
    $idPVPEAntigo = $idPVPE;
    //$idPVBBPE
    include("lists/gerencia_projetos_versao_caminho.php");    
    $statusAux = $objPVPE->getStatus();
    
    if($statusAux < $statusPEAnterior || $statusPEAnterior == null)
        $statusPEAnterior = $statusAux;
    if(!$cicloFinalizado)
        $cicloFinalizado = $objPVPE->isFaseFinalizada($statusPEAnterior);
    
}
?>


