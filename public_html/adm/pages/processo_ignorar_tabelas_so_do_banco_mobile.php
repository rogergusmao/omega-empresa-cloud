<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    
    
    $listaTTSoMobile = EXTDAO_Tabela_tabela::getVetorIdTabelaTabelaSoProducaoComTipoOperacaoBanco(
            $objPVBB->getId(), 
            $objPVBB->getBanco_banco_id_INT(), 
            array(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_DELETE));
    
    for($i = 0 ; $i < count($listaTTSoMobile); $i++){
        $idTTSoMobile = $listaTTSoMobile[$i];
        if(strlen($idTTSoMobile)){
            $objTT = new EXTDAO_Tabela_tabela();
            $objTT->select($idTTSoMobile);
            $objTT->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE);
            $objTT->formatarParaSQL();
            $objTT->update($idTTSoMobile);
        }
    }
    
    Helper::imprimirMensagem("Os relacionamentos entre tabelas do banco mobile que n�o ser�o sincronizadas foram ignorados da an�lise.");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
?>	