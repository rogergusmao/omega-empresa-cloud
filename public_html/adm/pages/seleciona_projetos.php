<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos
    * NOME DA CLASSE DAO: DAO_Projetos
    * DATA DE GERA��O:    09.01.2013
    * ARQUIVO:            EXTDAO_Projetos.php
    * TABELA MYSQL:       projetos
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Projetos();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $objBanco = new Database();

    $consultaRegistros = "SELECT id FROM projetos  ORDER BY nome ";

    $objBanco->query($consultaRegistros);

    ?>

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Projetos</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="10%" />
			<col width="90%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";

        $url = "index.php?tipo=pages&page=seleciona_projetos_versao&".Param_Get::ID_PROJETOS."=".$obj->getId();
    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <a href="<?=$url;?>" target="_self"><?=$obj->getNome(); ?></a>
    			
    		</td>

		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

