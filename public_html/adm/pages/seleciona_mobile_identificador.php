<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       projetos
    * NOME DA CLASSE DAO: DAO_Projetos
    * DATA DE GERA��O:    09.01.2013
    * ARQUIVO:            EXTDAO_Projetos.php
    * TABELA MYSQL:       projetos
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = "Tem certeza que deseja excluir este registro?";
    $acoes["tooltip_exclusao"] = "Clique aqui para excluir este registro";
    $acoes["tooltip_edicao"] = "Clique aqui para editar este registro";
    $acoes["tooltip_visualizacao"] = "Clique aqui para visualizar este registro";

    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
    $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
    $varGET = Param_Get::getIdentificadorProjetosVersao();

    $obj = new EXTDAO_Mobile_identificador();
            
    $vetorIdMobileIdentificador = EXTDAO_Mobile_identificador::getListaMobileIdentificadorDoPVBB($idPVBB);
                        
//    $vetorIdMobileIdentificador = EXTDAO_Mobile_identificador::getListaIdentificadorNo($GET[Param_Get::ID_MOBILE_IDENTIFICADOR]);
//
//    $vetorIdMobileIdentificador = EXTDAO_Mobile_identificador::getListaIdentificadorNoProdutos($GET[Param_Get::ID_MOBILE]);
//    
    ?>

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Telefones</legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="10%" />
			<col width="90%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=0; $i < count($vetorIdMobileIdentificador); $i++){
        $idMI = $vetorIdMobileIdentificador[$i];
        
    	$obj->select($idMI);

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";

        $url = "actions.php?class=EXTDAO_Mobile_identificador&action=ativaHospedeiro&".$varGET."&".Param_Get::ID_MOBILE_IDENTIFICADOR."=$idMI";
    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <a href="<?=$url;?>" target="_self"><?=$idMI;?></a>
    		</td>
                
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <?=$obj->getIdentificador();?>    
                </td>
    		

        </tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

