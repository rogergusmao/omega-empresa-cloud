<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: operacao_download_banco_mobile
 * DATA DE GERA��O:    14.05.2013
 * ARQUIVO:            operacao_download_banco_mobile.php
 * TABELA MYSQL:       operacao_download_banco_mobile
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Operacao_download_banco_mobile();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = "add";
$idMI = Helper::GET(Param_Get::ID_MOBILE_IDENTIFICADOR);
$postar = "actions.php";
?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_operacao_download_banco_mobile">

<?
for ($cont = 1; $cont <= $numeroRegistros; $cont++) {

    if (Helper::SESSION("erro")) {

        unset($_SESSION["erro"]);

        $obj->setBySession();
    }

    if (Helper::GET("id{$cont}")) {

        $id = Helper::GET("id{$cont}");

        $obj->select($id);
        $legend = "Atualizar opera��o de sistema do telefone";
    } else {

        $legend = "Cadastrar opera��o de sistema do telefone";
    }

    $obj->formatarParaExibicao();
    ?>

    <input type="hidden" name="id<?= $cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">
    <input type="hidden" name="<?=  Param_Get::ID_MOBILE_IDENTIFICADOR;?>" id="<?= Param_Get::ID_MOBILE_IDENTIFICADOR;?>" value="<?= $idMI; ?>">
    <fieldset class="fieldset_form">
        <legend class="legend_form"><?= $legend; ?></legend>

        <table class="tabela_form">

           
            <tr class="tr_form">


                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_drop_tabela_BOOLEAN;
                $objArg->labelTrue = "Sim";
                $objArg->labelFalse = "N�o";
                $objArg->valor = $obj->getDrop_tabela_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoDrop_tabela_BOOLEAN($objArg); ?></td>




                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_popular_tabela_BOOLEAN;
                $objArg->labelTrue = "Sim";
                $objArg->labelFalse = "N�o";
                $objArg->valor = $obj->getPopular_tabela_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 80;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoPopular_tabela_BOOLEAN($objArg); ?></td>
            </tr>
           
            <tr class="tr_form">

                    <?
                    $objArg->numeroDoRegistro = $cont;
                    $objArg->label = $obj->label_observacao;
                    $objArg->valor = $obj->getObservacao();
                    $objArg->classeCss = "input_text";
                    $objArg->classeCssFocus = "focus_text";
                    $objArg->obrigatorio = false;
                    $objArg->largura = 200;
                    ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoObservacao($objArg); ?></td>
            </tr>

            <? } ?>

        <tr class="tr_form_rodape1">
            <td colspan="4">

            <?= Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

<?= Helper::getBarraDeBotoesDoFormulario(true, true, $action == "edit" ? true : false); ?>

            </td>
        </tr>
    </table>

</fieldset>

                <?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>

