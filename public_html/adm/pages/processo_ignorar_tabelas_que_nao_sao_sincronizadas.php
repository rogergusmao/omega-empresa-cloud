<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    
    
    $idBancoWeb = $objPVBB->getFkObjBanco_banco()->getHomologacao_banco_id_INT();
    $idBancoMobile = $objPVBB->getFkObjBanco_banco()->getProducao_banco_id_INT();
    $db = new Database();
    $trunk = Trunk::factory();
    //Nesse caso teremos o banco de homologação que corresponde ao prototipo web
    //na analise de sincronizacao
    $idPVTabelaHomologacao = $trunk->getPVDoBancoDeDadosDeHomologacao($objPV->getId());
    
    ////$pvBancos = $trunk->getProjetosVersaoDoBancoDeDados($objPV->getId(), $db);
//    $idPVTabelaHomologacao = $pvBancos['idPVTabelaHomologacao'];


    $q = "SELECT id, nome
        FROM tabela
        WHERE (transmissao_web_para_mobile_BOOLEAN = '0'
            OR transmissao_web_para_mobile_BOOLEAN IS NULL)
            AND (transmissao_mobile_para_web_BOOLEAN = '0'
            OR transmissao_mobile_para_web_BOOLEAN IS NULL)
                AND projetos_versao_id_INT = {$idPVTabelaHomologacao}
                AND banco_id_INT = $idBancoWeb";
    $db->query($q);
    //Lista de tabela que não são sincronizados para o telefone
    $vetorObjSistemaTabela = Helper::getResultSetToMatriz($db->result);
    for($i = 0 ; $i < count($vetorObjSistemaTabela); $i++){
        $objSistemaTabela = $vetorObjSistemaTabela[$i];
        $idTabelaWeb = $objSistemaTabela[0];
        if(strlen($idTabelaWeb)){
            $idTabelaTabela = EXTDAO_Tabela_tabela::getIdTabelaTabelaDaTabelaHomologacao(
                $objPVBB->getId(), $idTabelaWeb, $db);
            if(strlen($idTabelaTabela)){
                $objTT = new EXTDAO_Tabela_tabela($db);
                $objTT->select($idTabelaTabela);
                if($objTT->getProducao_tabela_id_INT() == null
                    || $objTT->getHomologacao_tabela_id_INT() == null){
                    $objTT->ignorarOperacaoDeAtualizacaoDoBancoHmgParaProd(true);
                }
            }
            
        }
    }
    
    Helper::imprimirMensagem("Os relacionamentos entre tabelas do banco mobile que não serão sincronizadas foram ignorados da análise.");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
?>	
