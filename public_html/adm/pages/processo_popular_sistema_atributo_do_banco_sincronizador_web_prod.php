<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $db = new Database();
    
    $idPVBBWeb = EXTDAO_Projetos_versao_banco_banco::getIdPVBBPrototipoWebDoProjetosVersao(
        $objPVBB->getProjetos_versao_id_INT(), 
        $db);
    $objBancoWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoHomologacao(
        $idPVBBWeb, $db);

    //Atualizando o sistema_tabela do banco sincronizador_web
    $idPVBBSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getVersaoSinc(
        $idPVBBWeb, 
        EXTDAO_Tipo_analise_projeto::Atualiza_banco_de_dados_de_producao_do_Sincronizador_Web, 
        $db);
    
    $objBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getObjBancoProducao(
        $idPVBBSincronizadorWeb, $db);

    EXTDAO_Sistema_atributo::atualizaSistemaAtributoDoBancoSincronizadorWeb(
        $objBancoWeb, 
        $objBancoSincronizadorWeb, 
        $db);
    $varGET= Param_Get::getIdentificadorProjetosVersao();
    
    sleep(5);
    
    Helper::imprimirMensagem("Atributos do banco {$objBancoSincronizadorWeb->getNome()} atualizado");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
?>	