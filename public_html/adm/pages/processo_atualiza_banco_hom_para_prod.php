<?php

function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    $objPE = $objPVPE->getObjProcessoEstrutura();
    $json = $objPE->getJson_constante();
    
    $idTipoScriptBanco = null;
    if(strlen($json)){
        $objJson = json_decode($json);    
        $idTipoScriptBanco =  $objJson->idTipoScriptBanco;
    } else {
        $idTipoScriptBanco = EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD;
    }
    //carregando o estado das tabelas
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB, 
        $idTipoScriptBanco);
    
    
    if(empty($vetorId)){
        $vetorId = $objGSBB->getListaIdScriptComandoBancoEstrutura();
        $indiceAtual = 0;
    }
    
    if($indiceAtual < count($vetorId)){


        $objGSBB->conectaBancoProducao();
        $objSCB = new EXTDAO_Script_comando_banco();

        for($i = 0 ; $i < 40; $i++){

            if($indiceAtual < count($vetorId)){
                $idSCB = $vetorId[$indiceAtual];
                if(strlen($idSCB)){
                    $objSCB->select($idSCB);
                    $objGSBB->executaComandoNoBancoDeProducao($idSCB, $objSCB->getConsulta());
                }
            }
            if($indiceAtual >= count($vetorId)){
                break;
            } else{
                $indiceAtual += 1;
            }
        }
    }

    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." consulta(s) executada(s) at� o momento.");
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	