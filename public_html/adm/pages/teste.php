<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos
 * NOME DA CLASSE DAO: DAO_Projetos
 * DATA DE GERA��O:    09.01.2013
 * ARQUIVO:            EXTDAO_Projetos.php
 * TABELA MYSQL:       projetos
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */
?>

<fieldset class="fieldset_list">
    <legend class="legend_list">Lista de Testes</legend>

    <table class="tabela_list">
        <colgroup>
            <col width="10%" />
            <col width="90%" />
        </colgroup>
        <thead>
            
        </thead>
        <tbody>

            <tr class="<?= $classTr ?>">

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Arvore
                </td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="arvore.php?tipo=pages&page=teste&<?=  Param_Get::ID_PROJETOS."=34";?>" target="_self">Arvore View</a>

                </td>

            </tr>
           
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Servi�os Mobile
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="actions.php?class=Servico_mobile&action=modoSuporte&id_mobile_identificador=137&usuario=null&corporacao=null" target="_self">Suporte</a>
                </td>
            </tr>
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Inicia Modo Suporte No Telefone
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="popup.php?tipo=pages&page=status_inicia_suporte&<?=Param_Get::ID_MOBILE_IDENTIFICADOR;?>=137" target="_self">Conecta</a>
                </td>
            </tr>
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Consulta Opera��o Sistema Mobile
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="actions.php?class=Servico_mobile&action=consultaOperacaoSistemaMobile&id_mobile_identificador=137&id_mobile_conectado=9" target="_self">Teste</a>
                    
                </td>
            </tr>
            
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Atualiza Estado Opera��o Sistema
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="actions.php?class=Servico_mobile&action=atualizaEstadoOperacaoSistemaMobile&id_mobile_identificador=137&id_mobile_conectado=9&id_estado_operacao_sistema_mobile=3&id_operacao_sistema_mobile=67" target="_self">Teste</a>
                    
                </td>
            </tr>
            
             <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Android Enviando Log de Erros Para Web
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="actions.php?class=Servico_mobile&action=logErroProdutoMobile&id_mobile_identificador=137&id_mobile_conectado=9&id_estado_operacao_sistema_mobile=3&id_operacao_sistema_mobile=67" target="_self">Teste</a>
                    
                </td>
            </tr>
             <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Download Script Banco
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="http://192.168.254.1/BibliotecaNuvem/public_html/adm/actions.php?class=Servico_mobile&action=transfereScriptBancoAndroidParaWeb&operacao_sistema_mobile=84" target="_self">Teste</a>
                    
                </td>
            </tr>       
            
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Teste Temporario - Teste atual -> execu��o do shell_exec est� falhando. 
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="popup.php?tipo=pages&page=teste_pagina&<?=Param_Get::ID_MOBILE_IDENTIFICADOR;?>=137" target="_self">Teste</a>
                </td>
            </tr>
            
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Gerador C�digo Android
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="popup.php?tipo=pages&page=gerador_android&<?=Param_Get::ID_PROJETOS_VERSAO;?>=2&<?=  Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO?>=1" target="_self">Teste</a>
                </td>
            </tr>
           
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Gerador C�digo Web
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="popup.php?tipo=pages&page=gerador_web&<?=Param_Get::ID_PROJETOS_VERSAO;?>=2&<?=  Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO?>=2" target="_self">Teste</a>
                </td>
            </tr>
            
            <tr class="<?= $classTr ?>">
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    Atualiza Chaves Unicas na tabela 'sistema_tabela' do banco do Sincronizador Web
                </td>
                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    <a href="popup.php?tipo=pages&page=atualizar_atributos_chave_unica_sincronizador&<?=  Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO?>=9" target="_self">Teste</a>
                </td>
            </tr>
        </tbody>
    </table>

</fieldset>

<br/>
<br/>

