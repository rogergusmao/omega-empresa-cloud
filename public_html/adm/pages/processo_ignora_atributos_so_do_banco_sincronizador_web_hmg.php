<?php
function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){
    //banco sincronizador web =(equivalente a) banco de produ��o
    
//    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
//    $databaseSincronizadorWeb = $objPVBB->getObjDatabaseDoBancoProducao();
    $idBancoSincronizadorWeb = EXTDAO_Projetos_versao_banco_banco::getIdBancoProducao($objPVBB->getId());
    $idsTabelasSoDoSincronizadorWeb = EXTDAO_Tabela::getTabelasComPrefixo(
        $idBancoSincronizadorWeb, $objPVBB->getProjetos_versao_id_INT(), "__", $objPVBB->database);
    
    if(count($idsTabelasSoDoSincronizadorWeb)){
        $objAA = new EXTDAO_Atributo_atributo();
        for($i = 0 ; $i < count($idsTabelasSoDoSincronizadorWeb); $i++){
            $idTabelaEspelho = $idsTabelasSoDoSincronizadorWeb[$i][0];
            $idAtributoCrudMobile = EXTDAO_Atributo::getIdAtributo("crud_mobile_id_INT", $idTabelaEspelho, $objAA->database);
            if(strlen($idAtributoCrudMobile)){
                $idAA = EXTDAO_Atributo_atributo::getIdAtributoAtributoProd($objPVBB->getId(), $idAtributoCrudMobile, $objAA->database);
                if(strlen($idAA)){
                    $objAA->select($idAA);
                    $objAA->setTipo_operacao_atualizacao_banco_id_INT(EXTDAO_Tipo_operacao_atualizacao_banco::$TIPO_IGNORE);
                    $objAA->formatarParaSQL();
                    $objAA->update($idAA);
                }
            }
        }
    }
    
    Helper::imprimirMensagem("Os relacionamentos entre tabelas do banco mobile que n�o ser�o sincronizadas foram ignorados da an�lise.");
    $status = new stdClass();
    $status->outProcessoConcluido = true;
    return $status;
}
?>	