<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: projetos_versao
    * DATA DE GERA��O:    02.05.2013
    * ARQUIVO:            projetos_versao.php
    * TABELA MYSQL:       projetos_versao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Projetos_versao();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = "edit";
    $postar = "actions.php";
    
    

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_projetos_versao">

    	<?
        $cont=1;
    	

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            $id = Helper::GET(Param_Get::ID_PROJETOS_VERSAO);

            $obj->select($id);
            $idProjetos = $obj->getProjetos_id_INT();
            $legend = "Publicar Vers�o do Projeto";

            $idPVNovo = EXTDAO_Projetos_versao::publica($id);
            
            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

            <tr class="tr_form">
                <td class="td_form_label">
<?
                Helper::imprimirMensagem("
                    Vers�o publicada com sucesso, agora voc� pode atualizar as vers�es dos sistemas.
                    <a target='_self' href='index.php?tipo=forms&page=gerencia_projetos_versao&".
                        Param_Get::ID_PROJETOS_VERSAO."=".$idPVNovo.
                        "&".Param_Get::ID_PROJETOS."=$idProjetos'>Clique aqui para finalizar</a>");
?>
</td>
            </tr>
            
	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Bancos Da Vers�o Do Projeto</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM projetos_versao_banco_banco
                                    WHERE projetos_versao_id_INT={$id} ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/gerencia_projetos_versao_banco_banco_relacionamento_projetos_versao.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("projetos_versao_banco_banco", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "gerencia_projetos_versao_banco_banco_relacionamento_projetos_versao", "div#projetos_versao_banco_banco", "Adicionar Banco Da Vers�o Do Projeto") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

