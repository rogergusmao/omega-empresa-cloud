<?php

function executa_pagina_processo($objPVPE, $objPV, $objPVBB, $vetorId, $indiceAtual, $idPVC){

    //carregando o estado das tabelas
    $objPE = $objPVPE->getObjProcessoEstrutura();
    $json = $objPE->getJson_constante();
    
    $idTipoScriptBanco = null;
    if(strlen($json)){
        $objJson = json_decode($json);    
        $idTipoScriptBanco =  $objJson->idTipoScriptBanco;
    } else {
        $idTipoScriptBanco = EXTDAO_Tipo_script_banco::SCRIPT_DE_ATUALIZACAO_DO_BANCO_DE_HMG_PARA_PRD;
    }
    
    $objGSBB = new Gerador_script_banco_banco(
        $objPVBB, 
        $idTipoScriptBanco
    );
    $objLog = new EXTDAO_Projetos_versao_log_erro_script();
    if(empty($vetorId)){
        $vetorId = EXTDAO_Projetos_versao_log_erro_script::getListaIdPVLE(
            $objPVBB->getId(), 
            $idTipoScriptBanco);
        $indiceAtual = 0;
    }
    if($indiceAtual < count($vetorId)){
        $objPVLE = new EXTDAO_Projetos_versao_log_erro();
        for($i = 0 ; $i < 80; $i++){

            if($indiceAtual < count($vetorId)){
                $idPVLE= $vetorId[$indiceAtual];

                if(strlen($idPVLE)){
                    $objPVLE->delete($idPVLE);
                }
            }

            if($indiceAtual >= count($vetorId)){

                break;
            } else{
                $indiceAtual += 1;
            }
        }

    }

    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." log(s) apagados at� o momento.");
    $status = new stdClass();
     $status->outProcessoConcluido = true;
    return $status;
}
?>	