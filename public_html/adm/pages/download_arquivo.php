<?php
$id = Helper::POSTGET(Param_Get::ID1);
$idSTDA = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO_LOG_ERRO);
$nomeTabela = Helper::POSTGET(Param_Get::NOME_TABELA);

$nomeArquivo = Helper::POSTGET(Param_Get::NOME_ARQUIVO);

$path = GerenciadorArquivo::getPathDoTipo(
                $id,
                $nomeTabela,
                $idSTDA);
        
$raiz = Helper::acharRaiz();

$pathDiretorio = $raiz.$path;
if(is_dir($pathDiretorio)){
    $pathArquivo = $pathDiretorio.$nomeArquivo;
    $objDownload = new Download($pathArquivo);
    print $objDownload->ds_download();
}

?>
