<?php

/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos
 * NOME DA CLASSE DAO: DAO_Projetos
 * DATA DE GERA��O:    09.01.2013
 * ARQUIVO:            EXTDAO_Projetos.php
 * TABELA MYSQL:       projetos
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */






$idPVPE = Helper::GET(Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA);
$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
$objPVPE->select($idPVPE);

$objPE = new EXTDAO_Processo_estrutura();
$objPE = $objPVPE->getObjProcessoEstrutura();

$iniciandoEstrututa = true;
if(strlen(Helper::GET(Param_get::PROCESSO_INICIADO)))
    $iniciandoEstrututa = false;        

$objPVC = new EXTDAO_Projetos_versao_caminho();
$idPVCaminhoAtual = null;
$idPVCaminhoAtual = $objPVPE->getId_atual_projetos_versao_caminho_INT();
if(strlen(Helper::GET(Param_get::ID_CAMINHO_CICLO_REINICIADO))){
    $idPVCaminhoAtual = Helper::GET(Param_get::ID_CAMINHO_CICLO_REINICIADO);
    EXTDAO_Projetos_versao_processo_estrutura::reinicializaCicloNaEtapa($idPVPE, $idPVCaminhoAtual);
    
}else{
    
    //Caso nao possua nenhum processo atualmente
    if (!strlen($idPVCaminhoAtual)) {
        $idPVCaminhoAtual = EXTDAO_Projetos_versao_processo_estrutura::inicializaCiclo($idPVPE);    
    }
}

$objPVC->select($idPVCaminhoAtual);

$strVetorId = $objPVPE->getGet_vetor_id();
$vetorId = array();
if (strlen($strVetorId)){
    $vetorId = Helper::explode("[,]", $strVetorId);
    $indiceAtual = $objPVPE->getCiclo_atual_INT();
    if($indiceAtual == null || !strlen($indiceAtual))
        $indiceAtual = 0;
}


$msgPorcentagem = "";
if (count($vetorId)) {
    $porcentagem = ($indiceAtual / count($vetorId)) * 100;
    $msgPorcentagem = "</br>EXECUTADO:\t\t $porcentagem %";
} else{
    $msgPorcentagem = "</br>EXECUTADO:\t\t 0 %";
}$vIndiceFinal = $objPVPE->getTotalEtapas() - 1;
Helper::imprimirMensagem("PROCESSO:\t {$objPVC->getNome()} </br> ETAPAS: {$objPVC->getOrdemNaListaDeEtapas()} / {$vIndiceFinal}{$msgPorcentagem}", MENSAGEM_INFO);
$cicloPagina = true;
if(! $iniciandoEstrututa){
    $outProcessoConcluido = false;
    $outCicloAtual = 0;
    
    $pagina = $objPVC->getPagina();
    if(strlen($pagina)){

//        //carregando o estado das tabelas
//        $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
//        $idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);
//
//        $objPVBB = new EXTDAO_Projetos_versao_banco_banco;
//        $objPVBB->select($idPVBB);
//
//        if(empty($vetorId)){
//            $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
//            $objPVBB->select($idPVBB);
//            $vetorId = $objPVBB->getVetorTabelaTabela();
//
//            $indiceAtual = 0;
//        }

        
        //carregando o estado das tabelas
        $idPVBB = Helper::GET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

        //Utilizado para autocomplete
        //$objPE = new EXTDAO_Processo_estrutura();
        //$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
        $objPVBBTeste = new EXTDAO_Projetos_versao_banco_banco;
        $objPVTeste = new EXTDAO_Projetos_versao();
        $idPVBBTeste = null;
        
        
        if($objPE->getUtiliza_copia_projetos_versao_BOOLEAN() == "1"){
            if(!strlen($objPVPE->getCopia_projetos_versao_banco_banco_id_INT())){
                //inicializa a copia a ser utilizada no processo
                EXTDAO_Projetos_versao::cadastraCopiaProjetosVersao($idPVBB, $objPVPE);
                $objPVPE->select($idPVPE);
            }
            $idPVBBTeste = $objPVPE->getCopia_projetos_versao_banco_banco_id_INT();
        } else
            $idPVBBTeste = $idPVBB;
        
        $objPVBBTeste->select($idPVBBTeste);
        $idPVTeste = $objPVBBTeste->getProjetos_versao_id_INT();
        $objPVTeste->select($idPVTeste);
        include($pagina);
        
        
        $statusProcesso = imprime_pagina_processo($objPVPE, $objPVTeste, $objPVBBTeste, $vetorId, $indiceAtual);
        

//        
//        if($statusProcesso->outProcessoConcluido != true){
//            $vetorId = $statusProcesso->vetorId;
//            $indiceAtual = $statusProcesso->indiceAtual;
//            if($indiceAtual >= count($vetorId)){
//                $outProcessoConcluido = true;
//            }
//            $outCicloAtual = $indiceAtual;
//        } else $outProcessoConcluido = true;
//        $vStrVetor = Helper::vetorToString($vetorId, ",");
//        $objPVPE->registraCiclo($outCicloAtual, $vStrVetor, null);
//        
//        if($outProcessoConcluido){
//            if($objPVPE->existeProximaEtapa())
//                EXTDAO_Projetos_versao_processo_estrutura::proximaEtapa($objPVPE->getId());
//            else{
//                $objPVC->finalizaEtapa();
//                EXTDAO_Projetos_versao_processo_estrutura::finalizaCiclo($objPVPE->getId());
//                $cicloPagina = false;
//            }
//        }
    }
        
}
//
//if ($cicloPagina) {
//    //gravando o estado atual no variavel GET
//    
//    
//    $varGET = Helper::formataGET(
//                    array(
//                Param_get::PROCESSO_INICIADO,
//                Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA,
//                Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
//                Param_get::ID_PROJETOS_VERSAO
//          ), array(
//                "true",
//                $idPVPE,
//                Helper::GET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO),
//                Helper::GET(Param_get::ID_PROJETOS_VERSAO)
//          )
//    );
//
//    
//    Helper::mudarLocation("index.php?page=executa_processo&tipo=pages&" . $varGET);
//    //return array("location: index.php?page=compara_banco_hom_prod&tipo=pages&".$varGET);
//} else{
//    $varGET = Helper::formataGET(
//              array(
//                Param_get::PROJETOS_VERSAO_PROCESSO_ESTRUTURA,
//                Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO,
//                Param_get::ID_PROJETOS_VERSAO
//              ), array(
//                $idPVPE,
//                Helper::GET(Param_get::ID_PROJETOS_VERSAO_BANCO_BANCO),
//                Helper::GET(Param_get::ID_PROJETOS_VERSAO)  
//              )
//    );
//
//    sleep(4);
//    Helper::mudarLocation("index.php?page=menu_banco_banco&tipo=pages&" . $varGET);
//}
?>	