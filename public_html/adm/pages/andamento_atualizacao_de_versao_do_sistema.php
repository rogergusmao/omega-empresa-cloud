<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos_versao_processo_estrutura
 * NOME DA CLASSE DAO: DAO_Projetos_versao_processo_estrutura
 * DATA DE GERA��O:    27.04.2013
 * ARQUIVO:            EXTDAO_Projetos_versao_processo_estrutura.php
 * TABELA MYSQL:       projetos_versao_processo_estrutura
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */


$idSistema = Helper::POSTGET(Param_Get::ID_SISTEMA);
$idSPV = EXTDAO_Sistema_projetos_versao::consultaUltimaVersaoDoSistema($idSistema);
$listaNoSP = EXTDAO_Sistema_produto::getListaIdentificadorNo($idSistema);





$varGET = Param_Get::getIdentificadorProjetosVersao();
$obj = new EXTDAO_Projetos_versao_banco_banco();
$objBanco = new Database();


?>
<fieldset class="fieldset_list">
    <legend class="legend_list">Gerenciar atualiza��o dos aplicativos do sistema</legend>
    <table class="tabela_list">
        <colgroup>
            <col width="80%" />
            <col width="20%" />
        </colgroup>
        <tr>
            <td>
                <table class="tabela_list">
                    <colgroup>
                        <col width="33%" />
                        <col width="33%" />
                        <col width="33%" />
                    </colgroup>
                    <thead>
                        <tr class="tr_list_titulos">
                            <td class="td_list_titulos"><?= $obj->label_id ?></td>
                            <td class="td_list_titulos">Prot�tipo</td>
                            <td class="td_list_titulos">Status</td>
                        </tr>
                    </thead>
                    <tbody>


<?
$statusPredominante = null;
$idPVPEAntigo = null;

for ($h = 0; $h < count($listaNoSP); $h++) {
    $noSP= $listaNoSP[$h];
    $idSP= $noSP[0];
    $objSP = new EXTDAO_Sistema_produto();
    $objSP->select($idSP);
    
    $idSPVP = EXTDAO_Sistema_projetos_versao_produto::getIdSPVPDaUltimaVersaoDoProduto($idSP);
    if(!strlen($idSPVP)){
     
        continue;
    }
    $objSPVP = new EXTDAO_Sistema_projetos_versao_produto();
    $objSPVP->select($idSPVP);
    
    $idPVBB = $objSPVP->getServidor_projetos_versao_banco_banco_id_INT();
    $obj->select($idPVBB);

    $status = EXTDAO_Projetos_versao_processo_estrutura::getStatusPVBB($idPVBB);
    if ($statusPredominante == null)
        $statusPredominante = $status;
    else if ($statusPredominante < $status)
        $statusPredominante = $status;


    switch ($status) {
        case EXTDAO_Projetos_versao_caminho::NAO_INICIALIZADO:
            $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";
            break;
        case EXTDAO_Projetos_versao_caminho::EXECUTADO:
            $classTr = ($i % 2) ? "tr_list_conteudo_insert_impar" : "tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Projetos_versao_caminho::EXECUTANDO:
            $classTr = ($i % 2) ? "tr_list_conteudo_edit_impar" : "tr_list_conteudo_edit_par";
            break;
        case EXTDAO_Projetos_versao_caminho::FINALIZADO:
            $classTr = ($i % 2) ? "tr_list_conteudo_insert_impar" : "tr_list_conteudo_insert_par";
            break;
        case EXTDAO_Projetos_versao_caminho::DESATUALIZADO:
            $classTr = ($i % 2) ? "tr_list_conteudo_delete_impar" : "tr_list_conteudo_delete_par";
            break;
        default:


            break;
    }
    ?>

                            <tr class="<?= $classTr ?>">

                                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $obj->getId() ?>
                                </td>

                                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= $obj->getNome(); ?>

                                </td>

                                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    <?= EXTDAO_Projetos_versao_caminho::statusToString($status); ?>

                                </td>





                            </tr>


<? }
?>


                    </tbody>
                </table>
            </td>

            <td>
<?
if ($statusPredominante == EXTDAO_Projetos_versao_caminho::FINALIZADO ||
        $statusPredominante == EXTDAO_Projetos_versao_caminho::EXECUTADO) {
    ?>
                    <a target="_self" href="index.php?tipo=pages&page=publicar&<?= Param_Get::ID_PROJETOS_VERSAO . "=" . $idPV; ?>">Publicar</a>
                    <?
                } else {
                    ?>
                    N�o � poss�vel publicar uma nova vers�o, enquanto os prot�tipos n�o forem processados.
                    <?
                }
                ?>
            </td>
        </tr>
    </table>


</fieldset>

<br/>
<br/>

