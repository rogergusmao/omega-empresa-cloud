<?php

/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       projetos
 * NOME DA CLASSE DAO: DAO_Projetos
 * DATA DE GERA��O:    09.01.2013
 * ARQUIVO:            EXTDAO_Projetos.php
 * TABELA MYSQL:       projetos
 * BANCO DE DADOS:     biblioteca_nuvem
 * -------------------------------------------------------
 *
 */


$idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);
$idPV = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO);

$varGET = Param_Get::getIdentificadorProjetosVersao();

if(strlen(Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO))){
    $idSP = Helper::POSTGET(Param_Get::ID_SISTEMA_PRODUTO);    
    $idSPVP = EXTDAO_Sistema_produto::getIdDaUltimaVersao($idSP);
}

$listaObjPE = EXTDAO_Projetos_versao_processo_estrutura::getListaObjProcessoEstrutura($idPVBB);

$vetorDeletar = $listaObjPE["deletarPVPE"];
$vetorInserir = $listaObjPE["inserirPE"];

$objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
if($vetorDeletar != null)
foreach ($vetorDeletar as $idPVPE) {
    $objPVPE->delete($idPVPE);
}
if($vetorInserir != null)
foreach ($vetorInserir as $idPE) {
    $objPVPE = new EXTDAO_Projetos_versao_processo_estrutura();
    $objPVPE->setProcesso_estrutura_id_INT($idPE);
    $objPVPE->setProjetos_versao_banco_banco_id_INT($idPVBB);
    if(strlen($idSPVP)){
        $objPVPE->setUtiliza_sistema_projetos_versao_produto_id_INT($idSPVP);
    }
    $objPVPE->formatarParaSQL();
    $objPVPE->insert();
}

$listaIdPVPE = EXTDAO_Projetos_versao_processo_estrutura::getListaIdPVPE($idPVBB);
for($i = 0 ; $i < count($listaIdPVPE); $i++){
    $idPVPE = $listaIdPVPE[$i];
    $listaObjPEC = EXTDAO_Projetos_versao_processo_estrutura::getListaObjProcessoEstruturaCaminho($idPVBB, $idPVPE);
    $vetorDeletar = $listaObjPEC["deletarPVC"];
    $vetorInserir = $listaObjPEC["inserirPEC"];
    

    $objPVC = new EXTDAO_Projetos_versao_caminho();
    if($vetorDeletar != null)
    foreach ($vetorDeletar as $idPVC) {
        $objPVC->delete($idPVC);
    }

    for($j = 0 ; $j < count($vetorInserir); $j++){

        $idPC =$vetorInserir[$j];
        
        $objPVC = new EXTDAO_Projetos_versao_caminho();
        $objPVC->setProcesso_estrutura_caminho_id_INT($idPC);
        $objPVC->setProjetos_versao_processo_estrutura_id_INT($idPVPE);
        $objPVC->formatarParaSQL();
        $objPVC->insert();
    }
}

    Helper::imprimirMensagem("Estrutura atualizada com sucesso");

?>	