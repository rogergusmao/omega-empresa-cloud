<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    $idPVBB = Helper::POSTGET(Param_Get::ID_PROJETOS_VERSAO_BANCO_BANCO);

    $varGET= Param_Get::getIdentificadorProjetosVersao();
    $objPVBB = new EXTDAO_Projetos_versao_banco_banco();
    $objPVBB->select($idPVBB);
    $idPV= $objPVBB->getProjetos_versao_id_INT();
    $objPV = new EXTDAO_Projetos_versao();
    $objPV->select($idPV);
    $idProjeto = $objPV->getProjetos_id_INT();
    $database = EXTDAO_Projetos_versao_banco_banco::getObjDatabaseDoBancoHomologacao($idPVBB);
    $idBanco = EXTDAO_Projetos_versao_banco_banco::getIdBancoHomologacao($idPVBB);
    
    $database->query("SHOW TABLES");
    $tabelas = Helper::getResultSetToArrayDeUmCampo($database->result);

    

    for($i = 0 ;$i < count($tabelas); $i++){
        $objTabela = new EXTDAO_Tabela();
        $table = $tabelas[$i];
        $database->query("SELECT id FROM sistema_tabela WHERE nome LIKE '".$table."'");
        
        $idSistemaTabela = $database->getPrimeiraTuplaDoResultSet(0);
        $idTabela = EXTDAO_Tabela::existeTabela($table, $idBanco, $idPV);
        if(!strlen($idTabela)){
            Helper::imprimirMensagem ("[$table] Tabela inexistente no mapeamento da Biblioteca Nuvem, atualize os dados do projeto no Biblioteca Nuvem", MENSAGEM_ERRO);
            continue;
        } else if($idSistemaTabela == null || !strlen($idSistemaTabela)){
            Helper::imprimirMensagem ("[$table] Tabela inexistente no mapeamento da sistema_tabela do banco: $nomeBanco.", MENSAGEM_ERRO);
            continue;
        }
        $objTabela->select($idTabela);
        $vetorIdAtributo = EXTDAO_Tabela::getVetorIdAtributo($objTabela->getId());
        for($j = 0 ; $j < count($vetorIdAtributo); $j++){
            $idAtributo = $vetorIdAtributo[$j];
            $objAtributo = new EXTDAO_Atributo();
            $objAtributo->select($idAtributo);
            
            $nomeAtributo = $objAtributo->getNome();
            if(!strlen($nomeAtributo)){
                continue;
            }
            
            $database->query("SELECT id "
                . " FROM sistema_atributo "
                . " WHERE nome LIKE '".$nomeAtributo."' "
                . " AND sistema_tabela_id_INT = $idSistemaTabela");
            
            $idSistemaAtributo = $database->getPrimeiraTuplaDoResultSet(0);
            $objSistemaAtributo = new EXTDAO_Sistema_atributo(0, 0, true, $database);
            
            if(strlen($idSistemaAtributo)){
                
                $objSistemaAtributo->select($idSistemaAtributo);
            }
            
            $objSistemaAtributo->setSistema_tabela_id_INT($idSistemaTabela);    
            $objSistemaAtributo->setNome($nomeAtributo);
            $objSistemaAtributo->setNot_null_BOOLEAN($objAtributo->getNot_null_BOOLEAN());
            $objSistemaAtributo->setValor_default($objAtributo->getValor_default());
            $objSistemaAtributo->setPrimary_key_BOOLEAN($objAtributo->getPrimary_key_BOOLEAN());
            $objSistemaAtributo->setTamanho_INT($objAtributo->getTamanho_INT());
            $objSistemaAtributo->setAuto_increment_BOOLEAN($objAtributo->getAuto_increment_BOOLEAN());
            $objSistemaAtributo->setLabel($objAtributo->getLabel());
            $objSistemaAtributo->setUnique_BOOLEAN($objAtributo->getUnique_BOOLEAN());
            $objSistemaAtributo->setUnique_nome($objAtributo->getUnique_nome());


            
            // se for chave extrangeira
            $idFKAtributo = $objAtributo->getFk_atributo_id_INT();
            if(strlen($idFKAtributo)){
                $objFKAtributo = $objAtributo->getObjFKAtributo();
                $nomeAtributoFK = $objFKAtributo->getNome();
                $varAtributoFK = strtoupper($nomeAtributoFK);

                $objFKTabela = $objAtributo->getObjFKTabela();
                $tabelaFK = $objFKTabela->getNome();
                $database->query("SELECT id FROM sistema_tabela WHERE nome LIKE '".$tabelaFK."'");
                $idSistemaTabelaFk = $database->getPrimeiraTuplaDoResultSet(0);
                
                $objSistemaAtributo->setFk_nome($objAtributo->getFk_nome());
                $objSistemaAtributo->setAtributo_fk($nomeAtributoFK);
                $objSistemaAtributo->setFk_sistema_tabela_id_INT($idSistemaTabelaFk);
                $objSistemaAtributo->setDelete_tipo_fk($objAtributo->getDelete_tipo_fk());
                $objSistemaAtributo->setUpdate_tipo_fk($objAtributo->getUpdate_tipo_fk());
                

            } else{
                $objSistemaAtributo->setFk_nome(null);
                $objSistemaAtributo->setAtributo_fk(null);
                $objSistemaAtributo->setFk_sistema_tabela_id_INT(null);
                $objSistemaAtributo->setDelete_tipo_fk(null);
                $objSistemaAtributo->setUpdate_tipo_fk(null);
                
            }
            $objSistemaAtributo->formatarParaSQL();
            if(strlen($idSistemaAtributo)){
                $objSistemaAtributo->update($idSistemaAtributo);
            } else{
                $objSistemaAtributo->insert();
            }

        }
    }


?>