<?php
function executa_pagina_processo(
    EXTDAO_Projetos_versao_processo_estrutura $objPVPE, 
    EXTDAO_Projetos_versao $objPV, 
    EXTDAO_Projetos_versao_banco_banco $objPVBB, 
    $vetorId, 
    $indiceAtual, 
    $idPVC){
    /*
        *
        * -------------------------------------------------------
        * NOME DA LIST:       projetos
        * NOME DA CLASSE DAO: DAO_Projetos
        * DATA DE GERA��O:    09.01.2013
        * ARQUIVO:            EXTDAO_Projetos.php
        * TABELA MYSQL:       projetos
        * BANCO DE DADOS:     biblioteca_nuvem
        * -------------------------------------------------------
        *
        */
 
    if(empty($vetorId)){

       $vVetor = $objPVBB->getVetorTabelaTabela();
      
       $vetorId = $vVetor;
       $indiceAtual = 0;
    }

    $objTT = new EXTDAO_Tabela_tabela();
    for($i = 0 ; $i < 80; $i++){

       if($indiceAtual < count($vetorId)){
           $vTabelaTabela = $vetorId[$indiceAtual];
           if(strlen($vTabelaTabela)){
               $objTT->select($vTabelaTabela);
               $objTT->atualizaChaves($objPVBB->getId());
           }

       }

       if($indiceAtual >= count($vetorId)){

           break;
       } else{
           $indiceAtual += 1;
       }

    }

    Helper::imprimirMensagem($indiceAtual."/".count($vetorId)." relacionamento(s) entre tabelas do banco de produ��o e homologa��o mapeadas at� o momento ");
    
    $status = new stdClass();
    $status->indiceAtual = $indiceAtual;
    $status->vetorId = $vetorId;
    $status->outProcessoConcluido = false;
    return $status;
}
?>	