<?php

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../recursos/php/funcoes.php';

function includePHPDirectory($dir){
    $arqs=Helper::getListaArquivoDoDiretorio($dir);
    foreach ($arqs as $arq ){
        include_once($arq);
    }
}
$workspaceRaiz = acharRaizWorkspace();

includePHPDirectory($workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/');
includePHPDirectory($workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/');

Helper::includePHP(false, 'adm/imports/instancias.php');
Helper::includePHP(false, 'protocolo/in/Protocolo_empresa_hosting.php');
Helper::includePHP(false, 'recursos/classes/class/LockSincronizador.php');

includePHPDirectory('recursos/classes/class/');
includePHPDirectory('recursos/classes/DAO/');
includePHPDirectory('recursos/classes/EXTDAO/');
includePHPDirectory('recursos/classes/BO/entidade/');
includePHPDirectory('recursos/classes/BO/util/');

try{
    //executavel.php [corporacao] [class] [action] [GET]
    if(php_sapi_name() != 'cli'){
        echo "Esse arquivo so pode ser chamando por linha de comando";
        exit();
    }
    if($argc < 4){
        echo "Chamada inválida. Ex de chamada correta: executavel.php [class] [action] [GET]*. Sendo GET os parametros (opcionais) separados com ','";
        exit();
    }
    $strGet = "";
    $get = array();
    if(count($argv) > 4){
        $strGet = $argv[5];
        $get = explode(",", $strGet);
        if(count($get) == 1
        && strlen($get[0]) == 0 )
            $get = null;
    }

    $classe = $argv[2];
    $action = $argv[4];

    $obj = call_user_func_array(array($classe, "factory"), array());


    //---------------------------------
    //*
    //*
    // INICIAR TRANSACAO
    //*
    //*
    //---------------------------------

    $retorno = null;
    //chama a funcao de nome "$action"
    if(!empty($get) )
        $retorno = call_user_func(array($obj, $action, $get));
    else
        $retorno = call_user_func(array($obj, $action));
//        echo "ACTION: $action<br/>";
//        print_r($retorno);

} catch (Exception $ex) {
    $retorno = new Mensagem(null, null, $ex);
}
if($retorno != null){
    $log= Registry::get('HelperLog');
    $log->gravarLogEmCasoErro($retorno);

    $strRet = Helper::jsonEncode ($retorno);
    echo $strRet;
}
        
