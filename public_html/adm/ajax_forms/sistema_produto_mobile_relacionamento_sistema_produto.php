<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: sistema_produto_mobile
    * DATA DE GERAÇÃO:    30.03.2013
    * ARQUIVO:            sistema_produto_mobile.php
    * TABELA MYSQL:       sistema_produto_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
                
    if(isset($_GET["contador"])){

        $numeroRegistroInterno = Helper::GET("contador");

    }

    if(isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento)){

        $objSistema_produto_mobile = new EXTDAO_Sistema_produto_mobile();
        $objSistema_produto_mobile->select($identificadorRelacionamento);

    }
    else{

        $objSistema_produto_mobile = new EXTDAO_Sistema_produto_mobile();

    }

        $objArgSistema_produto_mobile = new Generic_Argument();
        $objSistema_produto_mobile->formatarParaExibicao();

    	?>

    	<input type="hidden" name="sistema_produto_mobile_id_<?=$numeroRegistroInterno ?>" id="sistema_produto_mobile_id_<?=$numeroRegistroInterno ?>" value="<?=$objSistema_produto_mobile->getId(); ?>">

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?
                
                        $objArgSistema_produto_mobile = new Generic_Argument();

    			$objArgSistema_produto_mobile->numeroDoRegistro = "";
    			$objArgSistema_produto_mobile->label = $objSistema_produto_mobile->label_diretorio_android_id_INT;
    			$objArgSistema_produto_mobile->valor = $objSistema_produto_mobile->getDiretorio_android_id_INT();
    			$objArgSistema_produto_mobile->classeCss = "input_text";
    			$objArgSistema_produto_mobile->classeCssFocus = "focus_text";
    			$objArgSistema_produto_mobile->obrigatorio = false;
    			$objArgSistema_produto_mobile->largura = 200;
                        $objArgSistema_produto_mobile->nome = "sistema_produto_mobile_diretorio_android_id_INT_{$numeroRegistroInterno}";
                        $objArgSistema_produto_mobile->id = "sistema_produto_mobile_diretorio_android_id_INT_{$numeroRegistroInterno}";

    			$objSistema_produto_mobile->addInfoCampos("diretorio_android_id_INT", $objArgSistema_produto_mobile->label, "TEXTO", $objArgSistema_produto_mobile->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgSistema_produto_mobile->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objSistema_produto_mobile->getFkObjDiretorio_android()->getComboBox($objArgSistema_produto_mobile); ?>
    			</td>


                    

    			<?
                
                        $objArgSistema_produto_mobile = new Generic_Argument();

    			$objArgSistema_produto_mobile->numeroDoRegistro = "";
    			$objArgSistema_produto_mobile->label = $objSistema_produto_mobile->label_diretorio_iphone_id_INT;
    			$objArgSistema_produto_mobile->valor = $objSistema_produto_mobile->getDiretorio_iphone_id_INT();
    			$objArgSistema_produto_mobile->classeCss = "input_text";
    			$objArgSistema_produto_mobile->classeCssFocus = "focus_text";
    			$objArgSistema_produto_mobile->obrigatorio = false;
    			$objArgSistema_produto_mobile->largura = 200;
                        $objArgSistema_produto_mobile->nome = "sistema_produto_mobile_diretorio_iphone_id_INT_{$numeroRegistroInterno}";
                        $objArgSistema_produto_mobile->id = "sistema_produto_mobile_diretorio_iphone_id_INT_{$numeroRegistroInterno}";

    			$objSistema_produto_mobile->addInfoCampos("diretorio_iphone_id_INT", $objArgSistema_produto_mobile->label, "TEXTO", $objArgSistema_produto_mobile->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgSistema_produto_mobile->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objSistema_produto_mobile->getFkObjDiretorio_iphone()->getComboBox($objArgSistema_produto_mobile); ?>
    			</td>


                    			</tr>
			<tr class="tr_form">


    			<?
                
                        $objArgSistema_produto_mobile = new Generic_Argument();

    			$objArgSistema_produto_mobile->numeroDoRegistro = "";
    			$objArgSistema_produto_mobile->label = $objSistema_produto_mobile->label_diretorio_windows_mobile_id_INT;
    			$objArgSistema_produto_mobile->valor = $objSistema_produto_mobile->getDiretorio_windows_mobile_id_INT();
    			$objArgSistema_produto_mobile->classeCss = "input_text";
    			$objArgSistema_produto_mobile->classeCssFocus = "focus_text";
    			$objArgSistema_produto_mobile->obrigatorio = false;
    			$objArgSistema_produto_mobile->largura = 200;
                        $objArgSistema_produto_mobile->nome = "sistema_produto_mobile_diretorio_windows_mobile_id_INT_{$numeroRegistroInterno}";
                        $objArgSistema_produto_mobile->id = "sistema_produto_mobile_diretorio_windows_mobile_id_INT_{$numeroRegistroInterno}";

    			$objSistema_produto_mobile->addInfoCampos("diretorio_windows_mobile_id_INT", $objArgSistema_produto_mobile->label, "TEXTO", $objArgSistema_produto_mobile->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgSistema_produto_mobile->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objSistema_produto_mobile->getFkObjDiretorio_windows_mobile()->getComboBox($objArgSistema_produto_mobile); ?>
    			</td>


                    

    			<?
                
                        $objArgSistema_produto_mobile = new Generic_Argument();

    			$objArgSistema_produto_mobile->numeroDoRegistro = "";
    			$objArgSistema_produto_mobile->label = $objSistema_produto_mobile->label_sistema_tipo_mobile_id_INT;
    			$objArgSistema_produto_mobile->valor = $objSistema_produto_mobile->getSistema_tipo_mobile_id_INT();
    			$objArgSistema_produto_mobile->classeCss = "input_text";
    			$objArgSistema_produto_mobile->classeCssFocus = "focus_text";
    			$objArgSistema_produto_mobile->obrigatorio = false;
    			$objArgSistema_produto_mobile->largura = 200;
                        $objArgSistema_produto_mobile->nome = "sistema_produto_mobile_sistema_tipo_mobile_id_INT_{$numeroRegistroInterno}";
                        $objArgSistema_produto_mobile->id = "sistema_produto_mobile_sistema_tipo_mobile_id_INT_{$numeroRegistroInterno}";

    			$objSistema_produto_mobile->addInfoCampos("sistema_tipo_mobile_id_INT", $objArgSistema_produto_mobile->label, "TEXTO", $objArgSistema_produto_mobile->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgSistema_produto_mobile->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objSistema_produto_mobile->getFkObjSistema_tipo_mobile()->getComboBox($objArgSistema_produto_mobile); ?>
    			</td>


                    			</tr>
	<tr><td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button" value="Remover Produto Para Telefone" onclick="javascript:removerDivAjaxEmLista(this);"></td></tr>	</table><br />

