<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: banco
    * DATA DE GERAÇÃO:    28.01.2013
    * ARQUIVO:            banco.php
    * TABELA MYSQL:       banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
                
    if(isset($_GET["contador"])){

        $numeroRegistroInterno = Helper::GET("contador");

    }

    if(isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento)){

        $objBanco = new EXTDAO_Banco();
        $objBanco->select($identificadorRelacionamento);

    }
    else{

        $objBanco = new EXTDAO_Banco();

    }

        $objArgBanco = new Generic_Argument();
        $objBanco->formatarParaExibicao();

    	?>

    	<input type="hidden" name="banco_id_<?=$numeroRegistroInterno ?>" id="banco_id_<?=$numeroRegistroInterno ?>" value="<?=$objBanco->getId(); ?>">

        <table class="tabela_form">

        			<tr class="tr_form">


                    <?

                    $objArgBanco = new Generic_Argument();

                    $objArgBanco->numeroDoRegistro = "";
                    $objArgBanco->label = $objBanco->label_nome;
                    $objArgBanco->valor = $objBanco->getNome();
                    $objArgBanco->classeCss = "input_text";
                    $objArgBanco->classeCssFocus = "focus_text";
                    $objArgBanco->obrigatorio = true;
                    $objArgBanco->largura = 200;
                    $objArgBanco->nome = "banco_nome_{$numeroRegistroInterno}";
                    $objArgBanco->id = "banco_nome_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgBanco->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objBanco->campoTexto($objArgBanco); ?></td>


    			<?
                
                        $objArgBanco = new Generic_Argument();

    			$objArgBanco->numeroDoRegistro = "";
    			$objArgBanco->label = $objBanco->label_tipo_banco_id_INT;
    			$objArgBanco->valor = $objBanco->getTipo_banco_id_INT();
    			$objArgBanco->classeCss = "input_text";
    			$objArgBanco->classeCssFocus = "focus_text";
    			$objArgBanco->obrigatorio = true;
    			$objArgBanco->largura = 200;
                        $objArgBanco->nome = "banco_tipo_banco_id_INT_{$numeroRegistroInterno}";
                        $objArgBanco->id = "banco_tipo_banco_id_INT_{$numeroRegistroInterno}";

    			$objBanco->addInfoCampos("tipo_banco_id_INT", $objArgBanco->label, "TEXTO", $objArgBanco->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgBanco->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objBanco->getFkObjTipo_banco()->getComboBox($objArgBanco); ?>
    			</td>


                    			</tr>
			<tr class="tr_form">


    			<?
                
                        $objArgBanco = new Generic_Argument();

    			$objArgBanco->numeroDoRegistro = "";
    			$objArgBanco->label = $objBanco->label_conexoes_id_INT;
    			$objArgBanco->valor = $objBanco->getConexoes_id_INT();
    			$objArgBanco->classeCss = "input_text";
    			$objArgBanco->classeCssFocus = "focus_text";
    			$objArgBanco->obrigatorio = true;
    			$objArgBanco->largura = 200;
                        $objArgBanco->nome = "banco_conexoes_id_INT_{$numeroRegistroInterno}";
                        $objArgBanco->id = "banco_conexoes_id_INT_{$numeroRegistroInterno}";

    			$objBanco->addInfoCampos("conexoes_id_INT", $objArgBanco->label, "TEXTO", $objArgBanco->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgBanco->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objBanco->getFkObjConexoes()->getComboBox($objArgBanco); ?>
    			</td>


                    

            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>
	<tr><td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button" value="Remover Banco" onclick="javascript:removerDivAjaxEmLista(this);"></td></tr>	</table><br />

