<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: projetos_versao_banco_banco
    * DATA DE GERAÇÃO:    27.05.2013
    * ARQUIVO:            projetos_versao_banco_banco.php
    * TABELA MYSQL:       projetos_versao_banco_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
                
    if(isset($_GET["contador"])){

        $numeroRegistroInterno = Helper::GET("contador");

    }

    if(isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento)){

        $objProjetos_versao_banco_banco = new EXTDAO_Projetos_versao_banco_banco();
        $objProjetos_versao_banco_banco->select($identificadorRelacionamento);

    }
    else{

        $objProjetos_versao_banco_banco = new EXTDAO_Projetos_versao_banco_banco();

    }

        $objArgProjetos_versao_banco_banco = new Generic_Argument();
        $objProjetos_versao_banco_banco->formatarParaExibicao();

    	?>

    	<input type="hidden" name="projetos_versao_banco_banco_id_<?=$numeroRegistroInterno ?>" id="projetos_versao_banco_banco_id_<?=$numeroRegistroInterno ?>" value="<?=$objProjetos_versao_banco_banco->getId(); ?>">

        <table class="tabela_form">

        			<tr class="tr_form">


                    <?

                    $objArgProjetos_versao_banco_banco = new Generic_Argument();

                    $objArgProjetos_versao_banco_banco->numeroDoRegistro = "";
                    $objArgProjetos_versao_banco_banco->label = $objProjetos_versao_banco_banco->label_nome;
                    $objArgProjetos_versao_banco_banco->valor = $objProjetos_versao_banco_banco->getNome();
                    $objArgProjetos_versao_banco_banco->classeCss = "input_text";
                    $objArgProjetos_versao_banco_banco->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_banco_banco->obrigatorio = false;
                    $objArgProjetos_versao_banco_banco->largura = 200;
                    $objArgProjetos_versao_banco_banco->nome = "projetos_versao_banco_banco_nome_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_banco_banco->id = "projetos_versao_banco_banco_nome_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_banco_banco->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_banco_banco->campoTexto($objArgProjetos_versao_banco_banco); ?></td>


    			<?
                
                        $objArgProjetos_versao_banco_banco = new Generic_Argument();

    			$objArgProjetos_versao_banco_banco->numeroDoRegistro = "";
    			$objArgProjetos_versao_banco_banco->label = $objProjetos_versao_banco_banco->label_banco_banco_id_INT;
    			$objArgProjetos_versao_banco_banco->valor = $objProjetos_versao_banco_banco->getBanco_banco_id_INT();
    			$objArgProjetos_versao_banco_banco->classeCss = "input_text";
    			$objArgProjetos_versao_banco_banco->classeCssFocus = "focus_text";
    			$objArgProjetos_versao_banco_banco->obrigatorio = false;
    			$objArgProjetos_versao_banco_banco->largura = 200;
                        $objArgProjetos_versao_banco_banco->nome = "projetos_versao_banco_banco_banco_banco_id_INT_{$numeroRegistroInterno}";
                        $objArgProjetos_versao_banco_banco->id = "projetos_versao_banco_banco_banco_banco_id_INT_{$numeroRegistroInterno}";

    			$objProjetos_versao_banco_banco->addInfoCampos("banco_banco_id_INT", $objArgProjetos_versao_banco_banco->label, "TEXTO", $objArgProjetos_versao_banco_banco->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgProjetos_versao_banco_banco->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objProjetos_versao_banco_banco->getFkObjBanco_banco()->getComboBox($objArgProjetos_versao_banco_banco); ?>
    			</td>


                    			</tr>
			<tr class="tr_form">


                    <?

                    $objArgProjetos_versao_banco_banco = new Generic_Argument();

                    $objArgProjetos_versao_banco_banco->numeroDoRegistro = "";
                    $objArgProjetos_versao_banco_banco->label = $objProjetos_versao_banco_banco->label_script_sql_homologacao_para_producao;
                    $objArgProjetos_versao_banco_banco->valor = $objProjetos_versao_banco_banco->getScript_sql_homologacao_para_producao();
                    $objArgProjetos_versao_banco_banco->classeCss = "input_text";
                    $objArgProjetos_versao_banco_banco->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_banco_banco->obrigatorio = false;
                    $objArgProjetos_versao_banco_banco->largura = 200;
                    $objArgProjetos_versao_banco_banco->nome = "projetos_versao_banco_banco_script_sql_homologacao_para_producao_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_banco_banco->id = "projetos_versao_banco_banco_script_sql_homologacao_para_producao_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_banco_banco->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_banco_banco->campoTexto($objArgProjetos_versao_banco_banco); ?></td>


                    <?

                    $objArgProjetos_versao_banco_banco = new Generic_Argument();

                    $objArgProjetos_versao_banco_banco->numeroDoRegistro = "";
                    $objArgProjetos_versao_banco_banco->label = $objProjetos_versao_banco_banco->label_script_sql_producao_para_homologacao;
                    $objArgProjetos_versao_banco_banco->valor = $objProjetos_versao_banco_banco->getScript_sql_producao_para_homologacao();
                    $objArgProjetos_versao_banco_banco->classeCss = "input_text";
                    $objArgProjetos_versao_banco_banco->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_banco_banco->obrigatorio = false;
                    $objArgProjetos_versao_banco_banco->largura = 200;
                    $objArgProjetos_versao_banco_banco->nome = "projetos_versao_banco_banco_script_sql_producao_para_homologacao_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_banco_banco->id = "projetos_versao_banco_banco_script_sql_producao_para_homologacao_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_banco_banco->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_banco_banco->campoTexto($objArgProjetos_versao_banco_banco); ?></td>
			</tr>
			<tr class="tr_form">


    			<?
                
                        $objArgProjetos_versao_banco_banco = new Generic_Argument();

    			$objArgProjetos_versao_banco_banco->numeroDoRegistro = "";
    			$objArgProjetos_versao_banco_banco->label = $objProjetos_versao_banco_banco->label_copia_do_projetos_versao_banco_banco_id_INT;
    			$objArgProjetos_versao_banco_banco->valor = $objProjetos_versao_banco_banco->getCopia_do_projetos_versao_banco_banco_id_INT();
    			$objArgProjetos_versao_banco_banco->classeCss = "input_text";
    			$objArgProjetos_versao_banco_banco->classeCssFocus = "focus_text";
    			$objArgProjetos_versao_banco_banco->obrigatorio = false;
    			$objArgProjetos_versao_banco_banco->largura = 200;
                        $objArgProjetos_versao_banco_banco->nome = "projetos_versao_banco_banco_copia_do_projetos_versao_banco_banco_id_INT_{$numeroRegistroInterno}";
                        $objArgProjetos_versao_banco_banco->id = "projetos_versao_banco_banco_copia_do_projetos_versao_banco_banco_id_INT_{$numeroRegistroInterno}";

    			$objProjetos_versao_banco_banco->addInfoCampos("copia_do_projetos_versao_banco_banco_id_INT", $objArgProjetos_versao_banco_banco->label, "TEXTO", $objArgProjetos_versao_banco_banco->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgProjetos_versao_banco_banco->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objProjetos_versao_banco_banco->getFkObjCopia_do_projetos_versao_banco_banco()->getComboBox($objArgProjetos_versao_banco_banco); ?>
    			</td>


                    

            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>
	<tr><td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button" value="Remover Banco Da Versão Do Projeto" onclick="javascript:removerDivAjaxEmLista(this);"></td></tr>	</table><br />

