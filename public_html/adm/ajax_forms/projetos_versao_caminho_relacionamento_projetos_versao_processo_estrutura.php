<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: projetos_versao_caminho
    * DATA DE GERAÇÃO:    18.06.2013
    * ARQUIVO:            projetos_versao_caminho.php
    * TABELA MYSQL:       projetos_versao_caminho
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
                
    if(isset($_GET["contador"])){

        $numeroRegistroInterno = Helper::GET("contador");

    }

    if(isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento)){

        $objProjetos_versao_caminho = new EXTDAO_Projetos_versao_caminho();
        $objProjetos_versao_caminho->select($identificadorRelacionamento);

    }
    else{

        $objProjetos_versao_caminho = new EXTDAO_Projetos_versao_caminho();

    }

        $objArgProjetos_versao_caminho = new Generic_Argument();
        $objProjetos_versao_caminho->formatarParaExibicao();

    	?>

    	<input type="hidden" name="projetos_versao_caminho_id_<?=$numeroRegistroInterno ?>" id="projetos_versao_caminho_id_<?=$numeroRegistroInterno ?>" value="<?=$objProjetos_versao_caminho->getId(); ?>">

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?
                
                        $objArgProjetos_versao_caminho = new Generic_Argument();

    			$objArgProjetos_versao_caminho->numeroDoRegistro = "";
    			$objArgProjetos_versao_caminho->label = $objProjetos_versao_caminho->label_processo_estrutura_caminho_id_INT;
    			$objArgProjetos_versao_caminho->valor = $objProjetos_versao_caminho->getProcesso_estrutura_caminho_id_INT();
    			$objArgProjetos_versao_caminho->classeCss = "input_text";
    			$objArgProjetos_versao_caminho->classeCssFocus = "focus_text";
    			$objArgProjetos_versao_caminho->obrigatorio = false;
    			$objArgProjetos_versao_caminho->largura = 200;
                        $objArgProjetos_versao_caminho->nome = "projetos_versao_caminho_processo_estrutura_caminho_id_INT_{$numeroRegistroInterno}";
                        $objArgProjetos_versao_caminho->id = "projetos_versao_caminho_processo_estrutura_caminho_id_INT_{$numeroRegistroInterno}";

    			$objProjetos_versao_caminho->addInfoCampos("processo_estrutura_caminho_id_INT", $objArgProjetos_versao_caminho->label, "TEXTO", $objArgProjetos_versao_caminho->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgProjetos_versao_caminho->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objProjetos_versao_caminho->getFkObjProcesso_estrutura_caminho()->getComboBox($objArgProjetos_versao_caminho); ?>
    			</td>


                    

                    <?

                    $objArgProjetos_versao_caminho = new Generic_Argument();

                    $objArgProjetos_versao_caminho->numeroDoRegistro = "";
                    $objArgProjetos_versao_caminho->label = $objProjetos_versao_caminho->label_data_inicio_DATETIME;
                    $objArgProjetos_versao_caminho->valor = $objProjetos_versao_caminho->getData_inicio_DATETIME();
                    $objArgProjetos_versao_caminho->classeCss = "input_text";
                    $objArgProjetos_versao_caminho->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_caminho->obrigatorio = false;
                    $objArgProjetos_versao_caminho->largura = 200;
                    $objArgProjetos_versao_caminho->nome = "projetos_versao_caminho_data_inicio_DATETIME_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_caminho->id = "projetos_versao_caminho_data_inicio_DATETIME_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_caminho->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_caminho->campoData($objArgProjetos_versao_caminho); ?></td>
			</tr>
			<tr class="tr_form">


                    <?

                    $objArgProjetos_versao_caminho = new Generic_Argument();

                    $objArgProjetos_versao_caminho->numeroDoRegistro = "";
                    $objArgProjetos_versao_caminho->label = $objProjetos_versao_caminho->label_data_fim_DATETIME;
                    $objArgProjetos_versao_caminho->valor = $objProjetos_versao_caminho->getData_fim_DATETIME();
                    $objArgProjetos_versao_caminho->classeCss = "input_text";
                    $objArgProjetos_versao_caminho->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_caminho->obrigatorio = false;
                    $objArgProjetos_versao_caminho->largura = 200;
                    $objArgProjetos_versao_caminho->nome = "projetos_versao_caminho_data_fim_DATETIME_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_caminho->id = "projetos_versao_caminho_data_fim_DATETIME_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_caminho->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_caminho->campoData($objArgProjetos_versao_caminho); ?></td>


                    <?

                    $objArgProjetos_versao_caminho = new Generic_Argument();

                    $objArgProjetos_versao_caminho->numeroDoRegistro = "";
                    $objArgProjetos_versao_caminho->label = $objProjetos_versao_caminho->label_data_atualizacao_DATETIME;
                    $objArgProjetos_versao_caminho->valor = $objProjetos_versao_caminho->getData_atualizacao_DATETIME();
                    $objArgProjetos_versao_caminho->classeCss = "input_text";
                    $objArgProjetos_versao_caminho->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_caminho->obrigatorio = false;
                    $objArgProjetos_versao_caminho->largura = 200;
                    $objArgProjetos_versao_caminho->nome = "projetos_versao_caminho_data_atualizacao_DATETIME_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_caminho->id = "projetos_versao_caminho_data_atualizacao_DATETIME_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_caminho->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_caminho->campoData($objArgProjetos_versao_caminho); ?></td>
			</tr>
			<tr class="tr_form">


                    <?

                    $objArgProjetos_versao_caminho = new Generic_Argument();

                    $objArgProjetos_versao_caminho->numeroDoRegistro = "";
                    $objArgProjetos_versao_caminho->label = $objProjetos_versao_caminho->label_tempo_total_gasto_seg_INT;
                    $objArgProjetos_versao_caminho->valor = $objProjetos_versao_caminho->getTempo_total_gasto_seg_INT();
                    $objArgProjetos_versao_caminho->classeCss = "input_text";
                    $objArgProjetos_versao_caminho->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_caminho->obrigatorio = false;
                    $objArgProjetos_versao_caminho->largura = 200;
                    $objArgProjetos_versao_caminho->nome = "projetos_versao_caminho_tempo_total_gasto_seg_INT_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_caminho->id = "projetos_versao_caminho_tempo_total_gasto_seg_INT_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_caminho->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_caminho->campoInteiro($objArgProjetos_versao_caminho); ?></td>


                    <?

                    $objArgProjetos_versao_caminho = new Generic_Argument();

                    $objArgProjetos_versao_caminho->numeroDoRegistro = "";
                    $objArgProjetos_versao_caminho->label = $objProjetos_versao_caminho->label_total_execucao_INT;
                    $objArgProjetos_versao_caminho->valor = $objProjetos_versao_caminho->getTotal_execucao_INT();
                    $objArgProjetos_versao_caminho->classeCss = "input_text";
                    $objArgProjetos_versao_caminho->classeCssFocus = "focus_text";
                    $objArgProjetos_versao_caminho->obrigatorio = false;
                    $objArgProjetos_versao_caminho->largura = 200;
                    $objArgProjetos_versao_caminho->nome = "projetos_versao_caminho_total_execucao_INT_{$numeroRegistroInterno}";
                    $objArgProjetos_versao_caminho->id = "projetos_versao_caminho_total_execucao_INT_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgProjetos_versao_caminho->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objProjetos_versao_caminho->campoInteiro($objArgProjetos_versao_caminho); ?></td>
			</tr>
	<tr><td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button" value="Remover Etapa Do Processo De Análise" onclick="javascript:removerDivAjaxEmLista(this);"></td></tr>	</table><br />

