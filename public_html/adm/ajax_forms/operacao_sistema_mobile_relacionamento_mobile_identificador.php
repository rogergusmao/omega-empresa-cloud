<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMULÁRIO: operacao_sistema_mobile
    * DATA DE GERAÇÃO:    21.10.2013
    * ARQUIVO:            operacao_sistema_mobile.php
    * TABELA MYSQL:       operacao_sistema_mobile
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */
                
    if(isset($_GET["contador"])){

        $numeroRegistroInterno = Helper::GET("contador");

    }

    if(isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento)){

        $objOperacao_sistema_mobile = new EXTDAO_Operacao_sistema_mobile();
        $objOperacao_sistema_mobile->select($identificadorRelacionamento);

    }
    else{

        $objOperacao_sistema_mobile = new EXTDAO_Operacao_sistema_mobile();

    }

        $objArgOperacao_sistema_mobile = new Generic_Argument();
        $objOperacao_sistema_mobile->formatarParaExibicao();

    	?>

    	<input type="hidden" name="operacao_sistema_mobile_id_<?=$numeroRegistroInterno ?>" id="operacao_sistema_mobile_id_<?=$numeroRegistroInterno ?>" value="<?=$objOperacao_sistema_mobile->getId(); ?>">

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?
                
                        $objArgOperacao_sistema_mobile = new Generic_Argument();

    			$objArgOperacao_sistema_mobile->numeroDoRegistro = "";
    			$objArgOperacao_sistema_mobile->label = $objOperacao_sistema_mobile->label_tipo_operacao_sistema_id_INT;
    			$objArgOperacao_sistema_mobile->valor = $objOperacao_sistema_mobile->getTipo_operacao_sistema_id_INT();
    			$objArgOperacao_sistema_mobile->classeCss = "input_text";
    			$objArgOperacao_sistema_mobile->classeCssFocus = "focus_text";
    			$objArgOperacao_sistema_mobile->obrigatorio = true;
    			$objArgOperacao_sistema_mobile->largura = 200;
                        $objArgOperacao_sistema_mobile->nome = "operacao_sistema_mobile_tipo_operacao_sistema_id_INT_{$numeroRegistroInterno}";
                        $objArgOperacao_sistema_mobile->id = "operacao_sistema_mobile_tipo_operacao_sistema_id_INT_{$numeroRegistroInterno}";

    			$objOperacao_sistema_mobile->addInfoCampos("tipo_operacao_sistema_id_INT", $objArgOperacao_sistema_mobile->label, "TEXTO", $objArgOperacao_sistema_mobile->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgOperacao_sistema_mobile->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objOperacao_sistema_mobile->getFkObjTipo_operacao_sistema()->getComboBox($objArgOperacao_sistema_mobile); ?>
    			</td>


                    

    			<?
                
                        $objArgOperacao_sistema_mobile = new Generic_Argument();

    			$objArgOperacao_sistema_mobile->numeroDoRegistro = "";
    			$objArgOperacao_sistema_mobile->label = $objOperacao_sistema_mobile->label_estado_operacao_sistema_mobile_id_INT;
    			$objArgOperacao_sistema_mobile->valor = $objOperacao_sistema_mobile->getEstado_operacao_sistema_mobile_id_INT();
    			$objArgOperacao_sistema_mobile->classeCss = "input_text";
    			$objArgOperacao_sistema_mobile->classeCssFocus = "focus_text";
    			$objArgOperacao_sistema_mobile->obrigatorio = false;
    			$objArgOperacao_sistema_mobile->largura = 200;
                        $objArgOperacao_sistema_mobile->nome = "operacao_sistema_mobile_estado_operacao_sistema_mobile_id_INT_{$numeroRegistroInterno}";
                        $objArgOperacao_sistema_mobile->id = "operacao_sistema_mobile_estado_operacao_sistema_mobile_id_INT_{$numeroRegistroInterno}";

    			$objOperacao_sistema_mobile->addInfoCampos("estado_operacao_sistema_mobile_id_INT", $objArgOperacao_sistema_mobile->label, "TEXTO", $objArgOperacao_sistema_mobile->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArgOperacao_sistema_mobile->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$objOperacao_sistema_mobile->getFkObjEstado_operacao_sistema_mobile()->getComboBox($objArgOperacao_sistema_mobile); ?>
    			</td>


                    			</tr>
			<tr class="tr_form">


                    <?

                    $objArgOperacao_sistema_mobile = new Generic_Argument();

                    $objArgOperacao_sistema_mobile->numeroDoRegistro = "";
                    $objArgOperacao_sistema_mobile->label = $objOperacao_sistema_mobile->label_data_abertura_DATETIME;
                    $objArgOperacao_sistema_mobile->valor = $objOperacao_sistema_mobile->getData_abertura_DATETIME();
                    $objArgOperacao_sistema_mobile->classeCss = "input_text";
                    $objArgOperacao_sistema_mobile->classeCssFocus = "focus_text";
                    $objArgOperacao_sistema_mobile->obrigatorio = true;
                    $objArgOperacao_sistema_mobile->largura = 200;
                    $objArgOperacao_sistema_mobile->nome = "operacao_sistema_mobile_data_abertura_DATETIME_{$numeroRegistroInterno}";
                    $objArgOperacao_sistema_mobile->id = "operacao_sistema_mobile_data_abertura_DATETIME_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgOperacao_sistema_mobile->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objOperacao_sistema_mobile->campoData($objArgOperacao_sistema_mobile); ?></td>


                    <?

                    $objArgOperacao_sistema_mobile = new Generic_Argument();

                    $objArgOperacao_sistema_mobile->numeroDoRegistro = "";
                    $objArgOperacao_sistema_mobile->label = $objOperacao_sistema_mobile->label_data_processamento_DATETIME;
                    $objArgOperacao_sistema_mobile->valor = $objOperacao_sistema_mobile->getData_processamento_DATETIME();
                    $objArgOperacao_sistema_mobile->classeCss = "input_text";
                    $objArgOperacao_sistema_mobile->classeCssFocus = "focus_text";
                    $objArgOperacao_sistema_mobile->obrigatorio = false;
                    $objArgOperacao_sistema_mobile->largura = 200;
                    $objArgOperacao_sistema_mobile->nome = "operacao_sistema_mobile_data_processamento_DATETIME_{$numeroRegistroInterno}";
                    $objArgOperacao_sistema_mobile->id = "operacao_sistema_mobile_data_processamento_DATETIME_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgOperacao_sistema_mobile->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objOperacao_sistema_mobile->campoData($objArgOperacao_sistema_mobile); ?></td>
			</tr>
			<tr class="tr_form">


                    <?

                    $objArgOperacao_sistema_mobile = new Generic_Argument();

                    $objArgOperacao_sistema_mobile->numeroDoRegistro = "";
                    $objArgOperacao_sistema_mobile->label = $objOperacao_sistema_mobile->label_data_conclusao_DATETIME;
                    $objArgOperacao_sistema_mobile->valor = $objOperacao_sistema_mobile->getData_conclusao_DATETIME();
                    $objArgOperacao_sistema_mobile->classeCss = "input_text";
                    $objArgOperacao_sistema_mobile->classeCssFocus = "focus_text";
                    $objArgOperacao_sistema_mobile->obrigatorio = false;
                    $objArgOperacao_sistema_mobile->largura = 200;
                    $objArgOperacao_sistema_mobile->nome = "operacao_sistema_mobile_data_conclusao_DATETIME_{$numeroRegistroInterno}";
                    $objArgOperacao_sistema_mobile->id = "operacao_sistema_mobile_data_conclusao_DATETIME_{$numeroRegistroInterno}";

                    ?>

                    <td class="td_form_label"><?=$objArgOperacao_sistema_mobile->getLabel() ?></td>
                    <td class="td_form_campo"><?=$objOperacao_sistema_mobile->campoData($objArgOperacao_sistema_mobile); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>
	<tr><td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button" value="Remover Operação Do Sistema Do Telefone" onclick="javascript:removerDivAjaxEmLista(this);"></td></tr>	</table><br />

