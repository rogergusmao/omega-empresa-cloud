<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema_produto_log_erro
    * DATA DE GERAÇÃO:    04.07.2013
    * ARQUIVO:            sistema_produto_log_erro.php
    * TABELA MYSQL:       sistema_produto_log_erro
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_produto_log_erro();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sistema_produto_log_erro"=>"Adicionar nova log de erro ocorrido na versão do produto",
    					 "list_sistema_produto_log_erro"=>"Listar log de erros ocorridos nas versões do produto");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sistema_produto_log_erro">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Log De Erros Ocorridos Nas Versões Do Produto</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_classe;
    			$objArg->valor = $obj->getClasse();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoClasse($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_funcao;
    			$objArg->valor = $obj->getFuncao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoFuncao($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_linha_INT;
    			$objArg->valor = $obj->getLinha_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoLinha_INT($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_nome_arquivo;
    			$objArg->valor = $obj->getNome_arquivo();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome_arquivo($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_identificador_erro;
    			$objArg->valor = $obj->getIdentificador_erro();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoIdentificador_erro($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_descricao;
    			$objArg->valor = $obj->getDescricao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoDescricao($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_stacktrace;
    			$objArg->valor = $obj->getStacktrace();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoStacktrace($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_data_visualizacao_DATETIME;
    			$objArg->valor = $obj->getData_visualizacao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_visualizacao_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_data_ocorrida_DATETIME;
    			$objArg->valor = $obj->getData_ocorrida_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_ocorrida_DATETIME($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_sistema_tipo_log_erro_id_INT;
    			$objArg->valor = $obj->getSistema_tipo_log_erro_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_tipo_log_erro($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_sistema_projetos_versao_produto_id_INT;
    			$objArg->valor = $obj->getSistema_projetos_versao_produto_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_projetos_versao_produto($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_id_usuario_INT;
    			$objArg->valor = $obj->getId_usuario_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_usuario_INT($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_id_corporacao_INT;
    			$objArg->valor = $obj->getId_corporacao_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_corporacao_INT($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_sistema_log_identificador_id_INT;
    			$objArg->valor = $obj->getSistema_log_identificador_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_log_identificador($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_sistema_projetos_versao_id_INT;
    			$objArg->valor = $obj->getSistema_projetos_versao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_projetos_versao($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_mobile_conectado_id_INT;
    			$objArg->valor = $obj->getMobile_conectado_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMobile_conectado($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_operacao_sistema_mobile_id_INT;
    			$objArg->valor = $obj->getOperacao_sistema_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllOperacao_sistema_mobile($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_script_comando_banco_id_INT;
    			$objArg->valor = $obj->getScript_comando_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllScript_comando_banco($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_total_ocorrencia_INT;
    			$objArg->valor = $obj->getTotal_ocorrencia_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTotal_ocorrencia_INT($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

