<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     corporacao
    * DATA DE GERAÇÃO:    02.03.2017
    * ARQUIVO:            corporacao.php5
    * TABELA MYSQL:       corporacao
    * BANCO DE DADOS:     biblioteca_nuvem_corporacao
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Corporacao();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php5";

    $nextActions = array("add_corporacao"=>"Adicionar nova Corporacao",
    					 "list_corporacao"=>"Listar Corporações");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="corporacao">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Corporações</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

