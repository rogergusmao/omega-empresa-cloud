<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     tipo_analise_banco
    * DATA DE GERAÇÃO:    10.06.2013
    * ARQUIVO:            tipo_analise_banco.php
    * TABELA MYSQL:       tipo_analise_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tipo_analise_banco();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_tipo_analise_banco"=>"Adicionar  tipo de análise de banco",
    					 "list_tipo_analise_banco"=>"Listar tipos de análise de banco");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="tipo_analise_banco">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Tipos De Análise De Banco</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

