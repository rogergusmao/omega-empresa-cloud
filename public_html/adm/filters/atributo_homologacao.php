<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     atributo_homologacao
    * DATA DE GERAÇÃO:    09.01.2013
    * ARQUIVO:            atributo_homologacao.php
    * TABELA MYSQL:       atributo_homologacao
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Atributo_homologacao();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_atributo_homologacao"=>"Adicionar  atributo do banco de homologação",
    					 "list_atributo_homologacao"=>"Listar atributos do banco de homologação");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="atributo_homologacao">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Atributos Do Banco De Homologação</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_tabela_homologacao_id_INT;
    			$objArg->valor = $obj->getTabela_homologacao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTabela_homologacao($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_tipo_sql;
    			$objArg->valor = $obj->getTipo_sql();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTipo_sql($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_fk_tabela_homologacao_id_INT;
    			$objArg->valor = $obj->getFk_tabela_homologacao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllFk_tabela_homologacao($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_banco_id_INT;
    			$objArg->valor = $obj->getBanco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllBanco($objArg); ?>
    			</td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

