<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     tabela_chave_tabela_chave
    * DATA DE GERAÇÃO:    09.02.2015
    * ARQUIVO:            tabela_chave_tabela_chave.php
    * TABELA MYSQL:       tabela_chave_tabela_chave
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tabela_chave_tabela_chave();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_tabela_chave_tabela_chave"=>"Adicionar nova relacionamento entre atributo da chave da tabela do banco de homologação e produção",
    					 "list_tabela_chave_tabela_chave"=>"Listar relacionamentos entre atributos da chave da tabela do banco de homologação e produção");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="tabela_chave_tabela_chave">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Relacionamentos Entre Atributos Da Chave Da Tabela Do Banco De Homologação E Produção</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_homologacao_tabela_chave_id_INT;
    			$objArg->valor = $obj->getHomologacao_tabela_chave_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllHomologacao_tabela_chave($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_producao_tabela_chave_id_INT;
    			$objArg->valor = $obj->getProducao_tabela_chave_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProducao_tabela_chave($objArg); ?>
    			</td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

