<?php
$objArg = new Generic_Argument();

    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     usuario
    * DATA DE GERAÇÃO:    16.01.2010
    * ARQUIVO:            usuario.php
    * TABELA MYSQL:       usuario
    * BANCO DE DADOS:     dep_pesquisas
    * -------------------------------------------------------
    *
    * GERENCIADOR DE FILTROS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Usuario();
    
    $objArg = new Generic_Argument();
    
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";    
    
    $nextActions = array("add_usuario"=>"Adicionar  usuário", 
    					 "list_usuario"=>"Listar usuários");
    
    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);
    
       $obj->setBySession();
        
    }
    
    $obj->setByGet("1");
        
    $obj->formatarParaExibicao();
    					 
    ?>
    
    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="usuario">
    	
        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Usuários</legend>
        
        <table class="tabela_form">
        
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>

        		
    			<?
    
    			$objArg->label = $obj->label_email;
    			$objArg->valor = $obj->getEmail();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoEmail($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_usuario_tipo_id_INT;
    			$objArg->valor = $obj->getUsuario_tipo_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllUsuario_tipo($objArg); ?>
    			</td>

                
            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">
        		
        		<?=Helper::imprimirBotoesList(true, true); ?>
        
        	</td>
        </tr>
	</table>
     
     </fieldset>
    
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    
