<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     projetos_versao_banco_banco_script
    * DATA DE GERAÇÃO:    04.04.2013
    * ARQUIVO:            projetos_versao_banco_banco_script.php
    * TABELA MYSQL:       projetos_versao_banco_banco_script
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Projetos_versao_banco_banco_script();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_projetos_versao_banco_banco_script"=>"Adicionar  script de atualização do banco de homologação",
    					 "list_projetos_versao_banco_banco_script"=>"Listar scripts de atualização do banco de homologação");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="projetos_versao_banco_banco_script">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Scripts De Atualização Do Banco De Homologação</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_consulta;
    			$objArg->valor = $obj->getConsulta();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoConsulta($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_is_atualizacao_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getIs_atualizacao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoIs_atualizacao_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_tipo_operacao_atualizacao_banco_id_INT;
    			$objArg->valor = $obj->getTipo_operacao_atualizacao_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_atualizacao_banco($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_atributo_atributo_id_INT;
    			$objArg->valor = $obj->getAtributo_atributo_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllAtributo_atributo($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_tabela_tabela_id_INT;
    			$objArg->valor = $obj->getTabela_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTabela_tabela($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_projetos_versao_banco_banco_id_INT;
    			$objArg->valor = $obj->getProjetos_versao_banco_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos_versao_banco_banco($objArg); ?>
    			</td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

