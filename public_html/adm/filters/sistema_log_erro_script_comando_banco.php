<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema_log_erro_script_comando_banco
    * DATA DE GERAÇÃO:    05.04.2013
    * ARQUIVO:            sistema_log_erro_script_comando_banco.php
    * TABELA MYSQL:       sistema_log_erro_script_comando_banco
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_log_erro_script_comando_banco();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sistema_log_erro_script_comando_banco"=>"Adicionar  erro ocorrido no comando de banco",
    					 "list_sistema_log_erro_script_comando_banco"=>"Listar erro ocorrido no comando de banco");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sistema_log_erro_script_comando_banco">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Erro Ocorrido No Comando De Banco</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_script_comando_banco_id_INT;
    			$objArg->valor = $obj->getScript_comando_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllScript_comando_banco($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_sistema_log_erro_mobile_id_INT;
    			$objArg->valor = $obj->getSistema_log_erro_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_log_erro_mobile($objArg); ?>
    			</td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

