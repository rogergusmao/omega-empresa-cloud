<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     tipo_plataforma_operacional
    * DATA DE GERAÇÃO:    11.06.2013
    * ARQUIVO:            tipo_plataforma_operacional.php
    * TABELA MYSQL:       tipo_plataforma_operacional
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tipo_plataforma_operacional();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_tipo_plataforma_operacional"=>"Adicionar nova plataforma operacional",
    					 "list_tipo_plataforma_operacional"=>"Listar plataformas operacionais");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="tipo_plataforma_operacional">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Plataformas Operacionais</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

