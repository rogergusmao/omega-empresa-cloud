<?php
$objArg = new Generic_Argument();


/*
*
* -------------------------------------------------------
* NOME DO FILTRO:     script_projetos_versao_banco_banco
* DATA DE GERA??O:    05.04.2013
* ARQUIVO:            script_projetos_versao_banco_banco.php
* TABELA MYSQL:       script_projetos_versao_banco_banco
* BANCO DE DADOS:     biblioteca_nuvem
* -------------------------------------------------------
*
*/

$obj = new EXTDAO_Script_projetos_versao_banco_banco();

$objArg = new Generic_Argument();

$class = $obj->nomeClasse;
$action = (Helper::GET("id")?"edit": "add");
$postar = "index.php";

$nextActions = array("add_script_projetos_versao_banco_banco"=>"Adicionar  script de atualiza??o do relacionamento entre bancos da vers?o do projeto",
	"list_script_projetos_versao_banco_banco"=>"Listar scripts de atualiza??o do relacionamento entre bancos da vers?o do projeto");

if(Helper::SESSION("erro")){

	unset($_SESSION["erro"]);

	$obj->setBySession();

}

$obj->setByGet("1");

$obj->formatarParaExibicao();

?>

<?=$obj->getCabecalhoFiltro($postar); ?>

	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
	<input type="hidden" name="tipo" id="tipo" value="lists">
	<input type="hidden" name="page" id="tipo" value="script_projetos_versao_banco_banco">
<?=EXTDAO_Projetos_versao::imprimiCabecalhoIdentificador();?>
	<fieldset class="fieldset_filtro">
		<legend class="legend_filtro">Pesquisar Scripts De Atualiza??o Do Relacionamento Entre Bancos Da Vers?o Do Projeto</legend>

		<table class="tabela_form">

			<tr class="tr_form">


				<?

				$objArg->label = $obj->label_is_atualizacao_BOOLEAN;
				$objArg->labelTrue = "Sim";
				$objArg->labelFalse = "N?o";
				$objArg->valor = $obj->getIs_atualizacao_BOOLEAN();
				$objArg->classeCss = "input_text";
				$objArg->classeCssFocus = "focus_text";
				$objArg->obrigatorio = false;
				$objArg->largura = 80;

				?>

				<td class="td_form_label"><?=$objArg->getLabel() ?></td>
				<td class="td_form_campo"><?=$obj->imprimirCampoIs_atualizacao_BOOLEAN($objArg); ?></td>


				<?

				$objArg->label = $obj->label_projetos_versao_banco_banco_id_INT;
				$objArg->valor = $obj->getProjetos_versao_banco_banco_id_INT();
				$objArg->classeCss = "input_text";
				$objArg->classeCssFocus = "focus_text";
				$objArg->obrigatorio = false;
				$objArg->largura = 200;

				?>

				<td class="td_form_label"><?=$objArg->getLabel() ?></td>
				<td class="td_form_campo">
					<?=$obj->getComboBoxAllProjetos_versao_banco_banco($objArg); ?>
				</td>
			</tr>
			<tr class="tr_form">


				<?

				$objArg->label = $obj->label_data_criacao_DATETIME;
				$objArg->valor = $obj->getData_criacao_DATETIME();
				$objArg->classeCss = "input_text";
				$objArg->classeCssFocus = "focus_text";
				$objArg->obrigatorio = false;
				$objArg->largura = 200;

				?>

				<td class="td_form_label"><?=$objArg->getLabel() ?></td>
				<td class="td_form_campo"><?=$obj->imprimirCampoData_criacao_DATETIME($objArg); ?></td>


				<td class="td_form_label"></td>
				<td class="td_form_campo"></td>
			</tr>

			<tr class="tr_form_rodape2">
				<td colspan="4">

					<?=Helper::imprimirBotoesList(true, true); ?>

				</td>
			</tr>
		</table>

	</fieldset>

<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

<?=$obj->getRodapeFormulario(); ?>
