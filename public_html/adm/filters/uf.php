<?php
$objArg = new Generic_Argument();

    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     uf
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            uf.php
    * TABELA MYSQL:       uf
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    * GERENCIADOR DE FILTROS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Uf();
    
    $objArg = new Generic_Argument();
    
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";    
    
    $nextActions = array("add_uf"=>"Adicionar  estado", 
    					 "list_uf"=>"Listar estados");
    
    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);
    
       $obj->setBySession();
        
    }
    
    $obj->setByGet("1");
    
    
    $obj->formatarParaExibicao();
    					 
    ?>
    
    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="uf">
    	
        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Estados</legend>
        
        <table class="tabela_form">
        
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>

        		
    			<?
    
    			$objArg->label = $obj->label_sigla;
    			$objArg->valor = $obj->getSigla();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoSigla($objArg); ?></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">
        		
        		<?=Helper::imprimirBotoesList(true, true); ?>
        
        	</td>
        </tr>
	</table>
     
     </fieldset>
    
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    
