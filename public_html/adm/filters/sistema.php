<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema
    * DATA DE GERAÇÃO:    18.06.2013
    * ARQUIVO:            sistema.php
    * TABELA MYSQL:       sistema
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sistema"=>"Adicionar  sistema",
    					 "list_sistema"=>"Listar sistemas");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sistema">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Sistemas</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_projetos_id_INT;
    			$objArg->valor = $obj->getProjetos_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_manutencao_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getManutencao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoManutencao_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_prototipo_sistema_id_INT;
    			$objArg->valor = $obj->getPrototipo_sistema_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllPrototipo_sistema($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_prototipo_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getPrototipo_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoPrototipo_BOOLEAN($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

