<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     tabela_tabela
    * DATA DE GERAÇÃO:    08.04.2013
    * ARQUIVO:            tabela_tabela.php
    * TABELA MYSQL:       tabela_tabela
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tabela_tabela();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_tabela_tabela"=>"Adicionar  relacionamento entre tabela do banco de homologação e produção",
    					 "list_tabela_tabela"=>"Listar relacionamentos entre tabelas do banco de homologação e produção");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="tabela_tabela">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Relacionamentos Entre Tabelas Do Banco De Homologação E Produção</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_homologacao_tabela_id_INT;
    			$objArg->valor = $obj->getHomologacao_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllHomologacao_tabela($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_producao_tabela_id_INT;
    			$objArg->valor = $obj->getProducao_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProducao_tabela($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_tipo_operacao_atualizacao_banco_id_INT;
    			$objArg->valor = $obj->getTipo_operacao_atualizacao_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_atualizacao_banco($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_projetos_versao_id_INT;
    			$objArg->valor = $obj->getProjetos_versao_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos_versao($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_status_verificacao_BOOLEAN;
    			$objArg->labelTrue = "Ativo";
    			$objArg->labelFalse = "Inativo";
    			$objArg->valor = $obj->getStatus_verificacao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoStatus_verificacao_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_inserir_tuplas_na_homologacao_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getInserir_tuplas_na_homologacao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoInserir_tuplas_na_homologacao_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_remover_tuplas_na_homologacao_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getRemover_tuplas_na_homologacao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoRemover_tuplas_na_homologacao_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_editar_tuplas_na_homologacao_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getEditar_tuplas_na_homologacao_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoEditar_tuplas_na_homologacao_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_tabela_sistema_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "Não";
    			$objArg->valor = $obj->getTabela_sistema_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTabela_sistema_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_projetos_versao_banco_banco_id_INT;
    			$objArg->valor = $obj->getProjetos_versao_banco_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos_versao_banco_banco($objArg); ?>
    			</td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

