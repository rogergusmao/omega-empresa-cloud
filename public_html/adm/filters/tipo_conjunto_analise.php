<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     tipo_conjunto_analise
    * DATA DE GERAÇÃO:    07.02.2015
    * ARQUIVO:            tipo_conjunto_analise.php
    * TABELA MYSQL:       tipo_conjunto_analise
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tipo_conjunto_analise();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_tipo_conjunto_analise"=>"Adicionar  Tipo conjunto análise",
    					 "list_tipo_conjunto_analise"=>"Listar Tipos conjunto análise");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="tipo_conjunto_analise">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Tipos Conjunto Análise</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

