<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     monitora_tela_mobile_para_web
    * DATA DE GERAÇÃO:    26.10.2013
    * ARQUIVO:            monitora_tela_mobile_para_web.php
    * TABELA MYSQL:       monitora_tela_mobile_para_web
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Monitora_tela_mobile_para_web();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_monitora_tela_mobile_para_web"=>"Adicionar nova operação de monitoramento mobile para web",
    					 "list_monitora_tela_mobile_para_web"=>"Listar operações de monitoramento mobile para web");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="monitora_tela_mobile_para_web">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Operações De Monitoramento Mobile Para Web</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_operacao_sistema_mobile_id_INT;
    			$objArg->valor = $obj->getOperacao_sistema_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllOperacao_sistema_mobile($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_mobile_identificador_id_INT;
    			$objArg->valor = $obj->getMobile_identificador_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMobile_identificador($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_sequencia_operacao_INT;
    			$objArg->valor = $obj->getSequencia_operacao_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoSequencia_operacao_INT($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_tipo_operacao_monitora_tela_id_INT;
    			$objArg->valor = $obj->getTipo_operacao_monitora_tela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_monitora_tela($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_monitora_tela_web_para_mobile_id_INT;
    			$objArg->valor = $obj->getMonitora_tela_web_para_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllMonitora_tela_web_para_mobile($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_data_ocorrencia_DATETIME;
    			$objArg->valor = $obj->getData_ocorrencia_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_ocorrencia_DATETIME($objArg); ?></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

