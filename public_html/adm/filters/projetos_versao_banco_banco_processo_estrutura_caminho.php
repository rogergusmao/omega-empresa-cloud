<?php
$objArg = new Generic_Argument();


    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     projetos_versao_banco_banco_processo_estrutura_caminho
    * DATA DE GERAÇÃO:    23.04.2013
    * ARQUIVO:            projetos_versao_banco_banco_processo_estrutura_caminho.php
    * TABELA MYSQL:       projetos_versao_banco_banco_processo_estrutura_caminho
    * BANCO DE DADOS:     biblioteca_nuvem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Projetos_versao_banco_banco_processo_estrutura_caminho();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_projetos_versao_banco_banco_processo_estrutura_caminho"=>"Adicionar nova etapa atual do processo",
    					 "list_projetos_versao_banco_banco_processo_estrutura_caminho"=>"Listar etapas atuais do processo");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="projetos_versao_banco_banco_processo_estrutura_caminho">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Etapas Atuais Do Processo</legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_projetos_versao_banco_banco_processo_estrutura_id_INT;
    			$objArg->valor = $obj->getProjetos_versao_banco_banco_processo_estrutura_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProjetos_versao_banco_banco_processo_estrutura($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_processo_estrutura_caminho_id_INT;
    			$objArg->valor = $obj->getProcesso_estrutura_caminho_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllProcesso_estrutura_caminho($objArg); ?>
    			</td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_data_inicio_DATETIME;
    			$objArg->valor = $obj->getData_inicio_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_inicio_DATETIME($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_data_fim_DATETIME;
    			$objArg->valor = $obj->getData_fim_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_fim_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_data_atualizacao_DATETIME;
    			$objArg->valor = $obj->getData_atualizacao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_atualizacao_DATETIME($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

