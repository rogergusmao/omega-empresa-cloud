<?php

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../recursos/php/funcoes.php';

echo Javascript::importarBibliotecaTree();

$idArvoreView = Helper::GET(Param_Get::ID_ARVORE_VIEW);
if(!strlen($idArvoreView)){
    $idArvoreView = Arvore_view::persisteNovaArvore();
}
$script = new ArvorePadrao(
    
    new ArvoreEstruturaPadrao(
        Param_Get::ID_ARVORE_VIEW."=".$idArvoreView."&".Param_Get::ID_PROJETOS."=".Helper::GET(Param_Get::ID_PROJETOS),
        Helper::acharRaiz().PATH_RELATIVO_PROJETO."imgs/icones_arvore/biblioteca_nuvem/"));

$script->render();
