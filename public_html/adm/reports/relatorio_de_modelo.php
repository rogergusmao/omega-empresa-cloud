    <?php

    $objBanco2 = new Database();
    $objBanco3 = new Database();
    $objBanco4 = new Database();

    $objEstruturaOrg = new Estrutura_Organizacional();
    $objSPV = new EXTDAO_Spv();

    if(!$funcaoAssociada){

        $funcaoAssociada = $objEstruturaOrg->getRaizDaEstruturaOrganizacional();

    }

    $nivelVertical = $objEstruturaOrg->getNivelVerticalDoNodo($funcaoAssociada);

    if($nivelVertical <= 2){

    ?>

	<table class="tabela_form" width="100%">

    	<colgroup>
    		<col width="20%" />
    		<col width="10%" />
    		<col width="10%" />
    		<col width="10%" />
    		<col width="0.5%" />
    		<col width="11%" />
    		<col width="13%" />
    		<col width="13%" />
    		<col width="13%" />
    	</colgroup>
    	<tr class="tr_list_titulos_vermelha">
    		<td class="td_list_titulos" style="text-align: center;" colspan="9">RESUMO DO PROCESSO DE MOBILIZAÇÃO</td>
    	</tr>
    	<tr class="tr_list_titulos">
    		<td class="td_list_titulos" rowspan="3">Área</td>
    		<td class="td_list_titulos" rowspan="3" style="text-align: center;">Total de Vagas</td>
    		<td class="td_list_titulos" rowspan="3" style="text-align: center;">Total de Pessoas Contratadas</td>
    		<td class="td_list_titulos" rowspan="3" style="text-align: center;">% Contratado</td>
    		<td class="td_espacamento"  rowspan="3">&nbsp;</td>
    		<td class="td_list_titulos" colspan="3" style="text-align: center;">PEDIDOS CADASTRADOS</td>
    		<td class="td_list_titulos" colspan="1" style="text-align: center;">PEDIDOS NÃO CADASTRADOS</td>
    	</tr>
    	<tr class="tr_list_titulos">
    		<td class="td_list_titulos" colspan="2" style="text-align: center;">EM ANDAMENTO</td>
    		<td class="td_list_titulos" colspan="1" rowspan="2" style="text-align: center;">DEMANDA FUTURA</td>
    		<td class="td_list_titulos" colspan="1" rowspan="2" style="text-align: center;">EM ATRASO</td>
    	</tr>
    	<tr class="tr_list_titulos">

    		<td class="td_list_titulos" style="text-align: center;"><?="&le; " . DIAS_SPV_NO_PRAZO . " dias" ?></td>
    		<td class="td_list_titulos" style="text-align: center;"><?="> " . DIAS_SPV_NO_PRAZO . " dias" ?></td>
    	</tr>

    <?

    $niveisVerticais = array();
    $niveis = array();
    $somenteFuncoesDoNivelArr = array();
    $css = array();

    for($i=$nivelVertical; $i <= 2; $i++){

        if($i == 1){

            $objBanco3->query("SELECT id, nivel_estrutura_INT FROM nodo_nivel_estrutura WHERE nivel_estrutura_INT=1 ORDER BY numero_ordem_INT");

            while($dados = $objBanco3->fetchArray()){

                $niveisVerticais[] = $dados[1];
                $niveis[] = $dados[0];
                $somenteFuncoesDoNivelArr[] = true;
                $css[] = "td_relatorio_cinza";

            }

        }
        elseif($i == 2){

            $objBanco3->query("SELECT id, nivel_estrutura_INT, nome
            				   FROM nodo_nivel_estrutura
            				   WHERE nivel_estrutura_INT=2
                               AND is_funcao_BOOLEAN <> 1
                               ORDER BY numero_ordem_INT");

            while($dados = $objBanco3->fetchArray()){

                $niveisVerticais[] = $dados[1];
                $niveis[] = $dados[0];
                $somenteFuncoesDoNivelArr[] = true;
                $css[] = "td_relatorio_branca";

                $objBanco2->query("SELECT id, nivel_estrutura_INT, nome
                				   FROM nodo_nivel_estrutura
                				   WHERE nivel_superior_id_INT={$dados[0]}
                				   AND is_funcao_BOOLEAN <> 1
                				   ORDER BY numero_ordem_INT");

                while($dados2 = $objBanco2->fetchArray()){

                    $niveisVerticais[] = $dados2[1];
                    $niveis[] = $dados2[0];
                    $somenteFuncoesDoNivelArr[] = false;
                    $css[] = "td_relatorio_branca";

                }

            }

        }

    }

    for($i=0; $i < count($niveis); $i++){

        $nivelVertical = $niveisVerticais[$i];
        $nivelPai = $niveis[$i];
        $somenteFuncoesDoNivel = $somenteFuncoesDoNivelArr[$i];
        $nomePai = $objEstruturaOrg->getNomeDoNodo($nivelPai);

        if($nivelVertical == 2){

            $nomeGerenciaGeral = $nomePai;

        }

        $objBanco2 = new Database();

        $arrComplemento = $objEstruturaOrg->getArrayDePartesParaConsultaSQLRecursiva($nivelPai, "v.nodo_nivel_estrutura_id_INT", false, false, $somenteFuncoesDoNivel);

        //totais
        $objBanco2->query("SELECT COUNT(DISTINCT v.id)
        				  FROM {$arrComplemento["stringFrom"]}
        				  vaga AS v
        				  WHERE 1=1
                          {$arrComplemento["stringWhere"]}
                          ");

        $vagasTotais = $objBanco2->getPrimeiraTuplaDoResultSet(0);

        //contratados
        $objBanco2->query("SELECT COUNT(DISTINCT v.id)
        				  FROM {$arrComplemento["stringFrom"]}
        				  vaga AS v
        				  WHERE 1=1
        				  AND v.id IN (SELECT vaga_id_INT FROM associacao_pessoa_vaga WHERE data_desassociacao_DATE IS NULL)
                          {$arrComplemento["stringWhere"]}
                          ");

        $vagasContratadas = $objBanco2->getPrimeiraTuplaDoResultSet(0);

        $statusSPV = $objSPV->getStatusDasSPVs($nivelPai, false, false, false, $somenteFuncoesDoNivel);

        $porcentagem = Helper::getPorcentagem($vagasContratadas, $vagasTotais);

        $coluna1 = $nomePai;
        $coluna2 = $vagasTotais;
        $coluna3 = $vagasContratadas;
        $coluna4 = $porcentagem;

        //SPVs
        $colunaSPV1 = $statusSPV[0][STATUS_SPV_ABERTA];
        $colunaSPV2 = $statusSPV[0][STATUS_SPV_ABERTA_E_ATRASADA];
        $colunaSPV3 = $statusSPV[0][STATUS_SPV_NAO_ESTA_ABERTA_E_NAO_DEVERIA_ESTAR];
        $colunaSPV4 = $statusSPV[0][STATUS_SPV_DEVERIA_ESTAR_ABERTA];

        $coluna2Total += $coluna2;
        $coluna3Total += $coluna3;

        $colunaSPV1Total += $colunaSPV1;
        $colunaSPV2Total += $colunaSPV2;
        $colunaSPV3Total += $colunaSPV3;
        $colunaSPV4Total += $colunaSPV4;

        $coluna2Acc += $coluna2;
        $coluna3Acc += $coluna3;

        $colunaSPV1Acc += $colunaSPV1;
        $colunaSPV2Acc += $colunaSPV2;
        $colunaSPV3Acc += $colunaSPV3;
        $colunaSPV4Acc += $colunaSPV4;

        if($nivelVertical == 3)
            $fontWeight = "font-weight: normal;";
        else
            $fontWeight = "";

        ?>

    		<tr class="tr_form" style="height: 30px;">
        		<td class="<?=$css[$i] ?>" style="text-align: left; <?=$fontWeight ?>"><?=$coluna1 ?></td>
        		<td class="<?=$css[$i] ?>" style="text-align: center; font-weight: bolder;"><?=$coluna2 ?></td>
        		<td class="<?=$css[$i] ?>" style="text-align: center; font-weight: bolder;"><?=$coluna3 ?></td>
        		<td class="<?=$css[$i] ?>" style="text-align: center;"><?=$coluna4 ?></td>
        		<td class="td_espacamento">&nbsp;</td>
        		<td class="<?=$css[$i] ?>" style="text-align: center;"><?=$colunaSPV1 ?></td>
        		<td class="<?=$css[$i] ?>" style="text-align: center;"><?=$colunaSPV2 ?></td>
        		<td class="<?=$css[$i] ?>" style="text-align: center;"><?=$colunaSPV3 ?></td>
        		<td class="<?=$css[$i] ?>" style="text-align: center;"><?=$colunaSPV4 ?></td>
        	</tr>

    <?

        if(($niveisVerticais[$i+1] == 2 && $nivelVertical != 1) || $i == count($niveisVerticais) -1){

            $coluna4Acc = Helper::getPorcentagem($coluna3Acc, $coluna2Acc);

            $cssParcial = "td_relatorio_cinza";

        ?>

        	<tr class="tr_form" style="height: 30px;">
        		<td class="<?=$cssParcial ?>" style="text-align: left;  "><?="Total {$nomeGerenciaGeral}" ?></td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$coluna2Acc ?></td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$coluna3Acc ?></td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$coluna4Acc ?></td>
        		<td class="td_espacamento">&nbsp;</td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$colunaSPV1Acc ?></td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$colunaSPV2Acc ?></td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$colunaSPV3Acc ?></td>
        		<td class="<?=$cssParcial ?>" style="text-align: center;"><?=$colunaSPV4Acc ?></td>
        	</tr>
        	<tr>
        		<td class="td_espacamento" colspan="9">&nbsp;</td>
        	</tr>


        <?

            $coluna2Acc = 0;
            $coluna3Acc = 0;

            $colunaSPV1Acc = 0;
            $colunaSPV2Acc = 0;
            $colunaSPV3Acc = 0;
            $colunaSPV4Acc = 0;

        }

        if($i == count($niveisVerticais) -1){

            $coluna4Total = Helper::getPorcentagem($coluna3Total, $coluna2Total);

            $cssTotal = "td_relatorio_cinza";

        ?>

        	<tr class="tr_form" style="height: 30px;">
        		<td class="<?=$cssTotal ?>" style="text-align: left;  "><?="Total" ?></td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$coluna2Total ?></td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$coluna3Total ?></td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$coluna4Total ?></td>
        		<td class="td_espacamento">&nbsp;</td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$colunaSPV1Total ?></td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$colunaSPV2Total ?></td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$colunaSPV3Total ?></td>
        		<td class="<?=$cssTotal ?>" style="text-align: center;"><?=$colunaSPV4Total ?></td>
        	</tr>



        <? } ?>

    <? } ?>

        </table>
    <br />

    <? } ?>
