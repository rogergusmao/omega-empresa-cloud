ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_5`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_3`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_6`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_7`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_1`
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_5 FOREIGN KEY (`forma_pagamento_id_INT`)
	REFERENCES `forma_pagamento` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_3 FOREIGN KEY (`usuario_id_INT`)
	REFERENCES `usuario` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_6 FOREIGN KEY (`minha_empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_7 FOREIGN KEY (`outra_empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_1 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    ALTER TABLE `empresa_compra_parcela` DROP FOREIGN KEY `sdhgfgh`
                    ALTER TABLE `empresa_compra_parcela` DROP FOREIGN KEY `empresa_compra_parcela_ibfk_2`
                    ALTER TABLE empresa_compra_parcela
	ADD CONSTRAINT sdhgfgh FOREIGN KEY (`empresa_compra_id_INT`)
	REFERENCES `empresa_compra` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    ALTER TABLE empresa_compra_parcela
	ADD CONSTRAINT empresa_compra_parcela_ibfk_2 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL
                    