ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_5`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_3`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_6`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_7`
                    ALTER TABLE `empresa_compra` DROP FOREIGN KEY `empresa_compra_ibfk_1`
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_5 FOREIGN KEY (`forma_pagamento_id_INT`)
	REFERENCES `forma_pagamento` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_3 FOREIGN KEY (`usuario_id_INT`)
	REFERENCES `usuario` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_6 FOREIGN KEY (`minha_empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_7 FOREIGN KEY (`outra_empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_ibfk_1 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE `empresa_compra_parcela` DROP FOREIGN KEY `sdhgfgh`
                    ALTER TABLE `empresa_compra_parcela` DROP FOREIGN KEY `empresa_compra_parcela_ibfk_2`
                    ALTER TABLE empresa_compra_parcela  MODIFY COLUMN valor_FLOAT  double NOT NULL 
                    ALTER TABLE empresa_compra_parcela
	ADD CONSTRAINT sdhgfgh FOREIGN KEY (`empresa_compra_id_INT`)
	REFERENCES `empresa_compra` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE empresa_compra_parcela
	ADD CONSTRAINT empresa_compra_parcela_ibfk_2 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL
                    ALTER TABLE empresa_tipo_venda_parcela  MODIFY COLUMN porcentagem_FLOAT  double NOT NULL 
                    ALTER TABLE empresa_venda_parcela  MODIFY COLUMN valor_FLOAT  double NOT NULL 
                    ALTER TABLE `permissao_categoria_permissao` DROP FOREIGN KEY `permissao_categoria_permissao_FK_254364013`
                    ALTER TABLE `permissao_categoria_permissao` DROP FOREIGN KEY `permissao_categoria_permissao_ibfk_10`
                    ALTER TABLE `permissao_categoria_permissao` DROP FOREIGN KEY `permissao_categoria_permissao_ibfk_3`
                    ALTER TABLE permissao_categoria_permissao
	DROP KEY permissao_id_INT

                    ALTER TABLE permissao_categoria_permissao
	DROP KEY permissao_categoria_permissao_FK_924652100

                    ALTER TABLE permissao_categoria_permissao
	DROP KEY permissao_categoria_permissao_FK_616943359

                    ALTER TABLE permissao_categoria_permissao  MODIFY COLUMN permissao_id_INT  int(11)  NOT NULL 
                    ALTER TABLE permissao_categoria_permissao  MODIFY COLUMN categoria_permissao_id_INT  int(11)  NOT NULL 
                    ALTER TABLE permissao_categoria_permissao  MODIFY COLUMN corporacao_id_INT  int(11)  NOT NULL 
                    ALTER TABLE permissao_categoria_permissao ADD INDEX `categoria_permissao_id_INT` (categoria_permissao_id_INT) USING BTREE
                    ALTER TABLE permissao_categoria_permissao ADD INDEX `corporacao_id_INT` (corporacao_id_INT) USING BTREE
                    ALTER TABLE permissao_categoria_permissao ADD INDEX `permissao_id_INT` (permissao_id_INT) USING BTREE
                    ALTER TABLE permissao_categoria_permissao
	ADD CONSTRAINT permissao_categoria_permissao_ibfk_9 FOREIGN KEY (`permissao_id_INT`)
	REFERENCES `permissao` (`id`)	ON UPDATE CASCADE	ON DELETE CASCADE
                    ALTER TABLE permissao_categoria_permissao
	ADD CONSTRAINT permissao_categoria_permissao_ibfk_4 FOREIGN KEY (`categoria_permissao_id_INT`)
	REFERENCES `categoria_permissao` (`id`)	ON UPDATE CASCADE	ON DELETE CASCADE
                    ALTER TABLE permissao_categoria_permissao
	ADD CONSTRAINT permissao_categoria_permissao_ibfk_7 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE CASCADE
                    