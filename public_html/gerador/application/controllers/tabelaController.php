<?php
class tabelaController extends Controller {    

    protected function init(){    
        $this->db = new Database();
//        $this->db = new MySqlDataAdapter(
//            $this->cfg['db']['hostname'], $this->cfg['db']['username'], 
//            $this->cfg['db']['password'], $this->cfg['db']['database'],
//            true);
        
    }
    
    public function read($id = false){
        $json = "";
        if(!$id){
            $data = $this->_model->read();
            
        } else {
            $data = $this->_model->read($id);
            
        }
        if(MyHelpers::isAjax()){
            header('Content-type: application/json');
            echo $data;
            exit();
        } else {
            print_r($data);
            exit();
        }
    }
    
    public function index(){
        $tipoCampoModel = new tipoCampoModel($this->db);
        $tipoCampoItens = $tipoCampoModel->read();
        if(MyHelpers::isAjax()){
            throw new Exception("N�o implementado");
        }else{
            $this->view->set('tipoCampoItens',$tipoCampoItens);
            return $this->view();
        }  
    }
}