<?php
class segurancaController extends Controller {    

    protected function init(){    
//        $this->db = new MySqlDataAdapter(
//            $this->cfg['db']['hostname'], $this->cfg['db']['username'], 
//            $this->cfg['db']['password'], $this->cfg['db']['database'],
//            true);
        $this->db = new Database();
    }
    
    public function read(){
        //CHECKAUTH
        $identificador = Helper::POSTGET("idClienteCookie");
        
        $objSeguranca = new Seguranca();
        $ret = $objSeguranca->validaIdentificadorUsuario($identificador);


        if(MyHelpers::isAjax()){
            header('Content-type: application/json');
            echo json_encode($ret);

            exit();
        } else {
            throw new Exception("FALTA IMPLEMENTAR");
        }

        return $this->view();
    }

    public function login(){
        
        
        if($_SERVER["REQUEST_METHOD"]=='POST'){
            //TODO continuar salvando o objeto
            $obj = Helper::getObjPostMVC();    
            $this->_model->set($obj );
            
            $objSeguranca = new Seguranca();
            $ret = $objSeguranca->procedimentoValidarLoginSICOB(
                $this->_model->email, 
                $this->_model->senha);
            
            if(MyHelpers::isAjax()){
                header('Content-type: application/json');
                echo json_encode($ret);
                
                exit();
            } else {
                throw new Exception("FALTA IMPLEMENTAR");
            }
        }
        return $this->view();
    }
   
    
    
}