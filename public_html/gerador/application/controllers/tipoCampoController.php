<?php
class tipoCampoController extends Controller {    

    protected function init(){    
//        $this->db = new MySqlDataAdapter(
//            $this->cfg['db']['hostname'], $this->cfg['db']['username'], 
//            $this->cfg['db']['password'], $this->cfg['db']['database'],
//            true);
        $this->db = new Database();
    }
    
    public function read($id = false){
    
        $json = "";
        if(!$id){
            $data = $this->_model->read();
            
        } else {
            $data = $this->_model->read($id);
            
        }
        if(MyHelpers::isAjax()){
            header('Content-type: application/json');
            echo $data;
            exit();
        } else {
            print_r($data);
            exit();
        }
        
    }
}