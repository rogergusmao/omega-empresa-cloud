<?php 

$this->layout = '~/views/shared/_defaultLayout.php';


if(!strlen($idTabela)){
    $idTabela = Helper::POSTGET("id_tabela");
}
if(strlen($idTabela)){
    $idsAtributo = EXTDAO_Tabela::getVetorIdAtributoFk($idTabela);    
} else {
    $idsAtributo = array();
}

$objAtributo = new EXTDAO_Atributo();
?>


<div class='row'>
    <div class='col-sm-12'>
      <div class='box bordered-box purple-border' style='margin-bottom:0;'>
        <div class='box-header purple-background'>
          <div class='title'>Atributos</div>
          <div class='actions'>
              <a class="btn btn-success btn-lg" 
                    role="button" 
                    onclick="inicializaFormularioCadastro()">Novo</a>

            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
            </a>
          </div>
        </div>
        <div class='box-content box-no-padding'>
          <table class='table table-striped' style='margin-bottom:0;'>
            <thead>
              <tr>
                <th>
                  Nome
                </th>
                <th>
                  Identificador(es)
                </th>
                <th>
                  Tipo
                </th>
                <th>
                  A��es
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbodyAtributos">
                 <tr class="tr-atributo">
                    <td>id</td>
                    <td>Inteiro</td>
                    <td></td>
                  </tr>
                <?
                for($i = 0 ; $i < count($idsAtributo); $i++){
                    $objAtributo->select($idsAtributo[$i]);
                    $objAtributo->formatarParaExibicao();
                    $nome = $objAtributo->getNome();
                    $tipoSql = $objAtributo->getTipo_sql();
                    $tipoSqlFicticio = $objAtributo->getTipo_sql_ficticio();
                    
                    ?>
                    <tr class="tr-atributo" id="<?=$objAtributo->getId(); ?>">
                        <td class="nome-atributo" ><?=$nome; ?></td>
                        
                        <td class="tipo-ficticio-atributo" ><?=$tipoSqlFicticio; ?></td>

                        <td>
                          <div class='text-right'>
                            <a class='btn btn-success btn-xs' href='#'>
                              <i class='icon-ok'></i>
                            </a>
                            <a class='btn btn-danger btn-xs' href='#'>
                              <i class='icon-remove'></i>
                            </a>
                          </div>
                        </td>
                      </tr>
                    <?
                }
                ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

        <? include 'formularios/atributo.php';; ?>
     
<script>
    var CRUD_REMOCAO = 1;
    var CRUD_INSERCAO = 2;
    var CRUD_EDICAO = 3;
    var CRUD_SEM_MODIFICACAO = 4;
    var MODO_EDICAO = 1;
    var MODO_INSERCAO = 2;
    
    var modo = MODO_EDICAO;
    var cont = 0;
    
    function Atributo(
            id,
            nomeExibicao,
            descricao,
            tipoFicticio,
            labelTipoFicticio,
            tamanho,
            casasDecimais,
            tabela,
            cardinalidade,
            crud){ 
        this.id = id;
        this.nomeExibicao = nomeExibicao;
        this.nome = formatarNomeSql( nomeExibicao);
        this.descricao = descricao;
        this.tipoFicticio = tipoFicticio;
        this.tamanho = tamanho;
        this.casasDecimais = casasDecimais;
        this.tabela = tabela;
        this.cardinalidade = cardinalidade;
        this.crud = crud;
        this.labelTipoFicticio = labelTipoFicticio;
     this.getIdHtml = function (prefixo ){
            var id = (prefixo != null ? prefixo + "_" : "") 
                    + (this.id == null ? "null" : this.id);
            return id;
        };
    }
    
    
    function onChangeTipoFicticio(){
        $('.categoria-atributo').css('display', 'none');
        
        var slTipoFicticio = $('#slTipoFicticio').val();
        $('.categoria-atributo ' ).not('.' + slTipoFicticio).find('.form-control').attr('data-rule-required', 'false');
        $('.categoria-atributo ' ).filter('.' + slTipoFicticio).css('display', 'block');
        $('.categoria-atributo ' ).filter('.' + slTipoFicticio).find('.form-control').attr('data-rule-required', 'true');
        
    }
   
     
    function adicionarAtributo(){
       var slTipoFicticio = $('#slTipoFicticio').val();
        if(slTipoFicticio != null && slTipoFicticio.length > 0){
            $('.categoria-atributo ' ).not('.' + slTipoFicticio).find('.form-control').attr('data-rule-required', 'false');    
        }
       // var valido  = $('#formAtributo').validate();
        var validator  = $("#formAtributo").validate({
                            rules: {
                              field: {
                                required: true,
                                number: true
                              }
                            }
                          });
       
        validator.form();
        if(!$("#formAtributo").valid()){
            return;
        } else {
            if(tabela !== 'undefined'){
                var atributo = factoryAtributo(null);
                adicionarAtributoNaLista(atributo);
                limparFormularioAtributo();
            }
            //$('#myModal').modal('toggle')
            //$('#myModal').modal('show')
            //TODO descomentar a linha abaixo
            $('#modal-formulario-atributo').modal('hide');
        }
    }
    function factoryAtributo(id){
        var idTemp = id;
        var crud = CRUD_EDICAO;
        if(id == null){
            idTemp = "temp_" + cont;
            cont ++;   
            crud = CRUD_INSERCAO;
        }
         
        var atributo = new Atributo(
            idTemp,
            $('#inNomeExibicao').val(),
            $('#inDescricao').val(),
            $('#slTipoFicticio').val(),
            $("#slTipoFicticio option:selected").text(),
            $('#inTamanho').val(),
            $('#inCasasDecimais').val(),
            $('#slTabela').val(),
            $('#slCardinalidade').val(),
            crud);
        return atributo;
    }
    
    function limparFormularioAtributo(){
        $('#formAtributo').trigger("reset");
        $('.categoria-atributo').css('display', 'none');
        var validator = $( "#formAtributo" ).validate();
        validator.resetForm();
    }
    
    function inicializaFormularioCadastro(){
        //Se o modo atual � edi��o
        if(modo == MODO_EDICAO){
            limparFormularioAtributo();
        }
        
        modo = MODO_INSERCAO;
        $('#aSalvarAlteracao').css('display', 'none');
        $('#aAdicionarAtributo').css('display', 'inline-block');
        $('#aSalvarAtributo').css('display', 'none');
        $('#hTitulo').val('Adicionar atributo');
        $('#inNomeExibicao').keyup();
        
        $('#modal-formulario-atributo').modal('show');
        
    }
    
    
    function preencheFormularioAtributo(objAtributo){
        modo = MODO_EDICAO;
        $('#hTitulo').val('Editar atributo');
        $('#inNomeExibicao').val(objAtributo.nome);
        $('#inNomeExibicao').keyup();
        
        $('#inDescricao').val(objAtributo.descricao);
        $('#slTipoFicticio').val(objAtributo.tipoFicticio);
        onChangeTipoFicticio();
        $('#inTamanho').val(objAtributo.tamanho);
        $('#inCasasDecimais').val(objAtributo.casasDecimais);
        $('#slTabela').val(objAtributo.tabela);
        $('#slCardinalidade').val(objAtributo.cardinalidade);
        $('#inId').val(objAtributo.getIdHtml());
        $('#aAdicionarAtributo').css('display', 'none');
        $('#aSalvarAlteracao').css('display', 'inline-block');
        
        
    }
    
    
    function salvarAlteracaoAtributo(){
        var id= $('#inId').val();
        var novoAtributo = factoryAtributo(id);
        atualizaAtributo(id, novoAtributo);    
        $('#modal-formulario-atributo').modal('hide');
    }
    
    $(document).ready(function(){
        $('.categoria-atributo').css('display', 'none');
        jQuery.validator.addMethod("nomeduplicado", function(value, element) {
            value = formatarNomeSql(value);
            var attr = getObjAtributoPeloNome(value);
            
            if(attr == null)
                return true;
            else return false;
        }, "J� existe um atributo com esse nome");
        jQuery.validator.addMethod("idreservado", function(value, element) {

          return !( value == "id");
        }, "O nome 'id' � reservado, escolha outro.");
        
        $('#inNomeExibicao').keyup(function(){
             var str = $(this).val();
             
             str = formatarNomeSql(str);
            $('#inNome').val(str);
            
        });
    });
</script>