<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir='ltr' xmlns='http://www.w3.org/1999/xhtml' data-framework="backbonejs">
    <head>
        <title><?php echo $cfg['site']['title']; ?></title>
        <?=Helper::carregarCssFlatty(); ?>
        <?=Javascript::importarBibliotecasFlattyBase(); ?>
        <?=Helper::imprimirComandoJavascript("var dialogo = false"); ?>
        
        <script>
                var isMobile = function() {
                      return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
                   };

                $(document).ready(function() {
                        if(!isMobile()){		
                                $('#main').css('display', 'none');			
                                $('#main').fadeIn(1000);		
                        }	
                });
        </script>
        
        <?php $this->renderSection('head');?>
        
    </head>
    
        <?
        
        if(Seguranca::autenticado()){
            
        ?>
        <body id="content-app" class='content-auth contrast-red without-footer'>
            
            <?php Javascript::imprimirCorpoDoTooltip(); ?>
            
            <?php include(MyHelpers::UrlContent('~/views/shared/_mensagens.php')); ?>
            <?php Ajax::imprimirCorpoDaDivDeRetornoAjax(); ?>
            <?php include(MyHelpers::UrlContent('~/views/shared/_topoPagina.php')); ?>
            <?php include(MyHelpers::UrlContent('~/views/shared/_modalCarregando.php')); ?>
              <div id="divAlterarSenha">
              </div>
              
              <div id='wrapper'>
                <?php include(MyHelpers::UrlContent('~/views/shared/_menuPart.php')); ?>
                <section id='content'>
                    <div class='container'>
                        <div class='row' id='content-wrapper'>
                          <div class='col-xs-12' id="content-body">
                           <?php $this->renderBody();?>
                          </div>
                        </div>
                        <?php include(MyHelpers::UrlContent('~/views/shared/_rodape.php')); ?>

                      </div>           
                </section>
              </div>
          </body>
    
              <?
              
          } else {
              
          ?>
            <body id="content-app"  class='contrast-red login contrast-background'>
                <div class='middle-container'>
                  <div class='middle-row'>
                    <div id="content-body"  class='middle-wrapper'>
                     

                    </div>
                  </div>
                </div>
          </body>
          <?    
          }
          ?>
    
          <?=Javascript::importarBibliotecasFlatty(); ?>
    
          <?=Javascript::importarBibliotecasBackbone(); ?>
        
    
</html>
