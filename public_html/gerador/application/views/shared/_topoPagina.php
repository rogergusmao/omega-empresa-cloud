<?php

?>
<header id="headerView" class="header">

      <nav class='navbar navbar-default'>

        <a class='navbar-brand' href='index.php'>
          <img width="81" height="21" class="logo" alt="Flatty" src="css/flatty/assets/images/logo.svg" />
          <img width="21" height="21" class="logo-xs" alt="Flatty" src="css/flatty/assets/images/logo_xs.svg" />
        </a>

        <a class='toggle-nav btn pull-left' href='javascript: void(0);'>
          <i class='icon-reorder'></i>
        </a>

        <ul class="user-info pull-left pull-right-xs pull-none-xsm">

              <!-- Raw Notifications -->
              <li class="notifications dropdown open">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <i class="entypo-attention"></i>
                  </a>

              </li>

              <!-- Message Notifications -->
              <li class="notifications dropdown">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <i class="entypo-mail"></i>
                  </a>

              </li>

              <!-- Task Notifications -->
              <li class="notifications dropdown">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <i class="entypo-list"></i>
                  </a>

              </li>

        </ul>

        <ul class='nav'>
         
          <li class='dropdown dark user-menu'>

            <? if($idClienteLogado){ ?>
                    
            <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                
              <span class='user-name'><?=$_SESSION["cliente_email"] ?></span>
              <b class='caret'></b>

            </a>
            <ul class='dropdown-menu'>
                
              <li>
                  
                  <script type="text/javascript">

                      function verClientesDaAssinatura(){

                          var url = 'ajax_modal.php?tipo=paginas&page=alterar_senha&titulo_modal=Alterar%20Senha';
                          carregarModalComLoading('#divAlterarSenha', 'html', url, 'alterarSenha', true);

                      }

                  </script>
                  <a onclick="verClientesDaAssinatura()">
                  <i class='icon-lock'></i>
                  Alterar senha
                </a>
              </li>
              <li>
                  <a href='index.php?tipo=formularios&page=cliente&tipo_posterior=formularios&page_posterior=cliente&id1=<?=  Seguranca_Pagamento::getIdDoClienteLogado();?>'>
                  <i class='icon-user'></i>
                  Meus dados
                </a>
              </li>
              
              <li class='divider'></li>
              <li>
                <a href='actions.php?class=Seguranca_Pagamento&action=efetuarLogout'>
                  <i class='icon-signout'></i>
                  Sair
                </a>
              </li>
            </ul>
                <? } ?>
            
          </li>
        </ul>
        
      </nav>
</header>


