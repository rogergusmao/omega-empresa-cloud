<?php

    if(Helper::GET("msgSucesso")){

        $mensagem = Helper::GET("msgSucesso");

     }
     elseif(Helper::GET("msgErro")){

        $mensagem = Helper::GET("msgErro");

     }

    $ultimoReferer = $_SERVER["HTTP_REFERER"];

     if(strlen(trim($mensagem))){

         if($_SESSION["ultima_mensagem"] == $mensagem && $_SESSION["ultimo_referer"] == $ultimoReferer){

             $esconder = false;

         }
         else{

             $_SESSION["ultima_mensagem"] = $mensagem;
             $_SESSION["ultimo_referer"] = $_SERVER["HTTP_REFERER"];

         }

     }
     else{

        $_SESSION["ultima_mensagem"] = "";
     	$esconder = true;

     }

   
     
     
    ?>
<div class="bootbox modal fade in" id="caixaDeMensagemSucesso" 
     tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog "  >
        <div class="modal-content  ">
            <div class="modal-body alert-success">
                <button type="button" 
                        class="bootbox-close-button close" 
                        style="margin-top: -10px;"
                        onclick="javascript:document.getElementById('caixaDeMensagemSucesso').style.display='none'">×</button>
                <div class="bootbox-body"><?=$mensagem;?>
                </div>

            </div>
            <div class="modal-footer ">
                <button id="botao_sucesso_ok"
                        data-bb-handler="ok" 
                        type="button" 
                        class="btn btn-primary" 
                        onclick="javascript:document.getElementById('caixaDeMensagemSucesso').style.display='none'">
                    OK</button>

            </div>
        </div>
    </div>
</div>
    	

<div class="bootbox modal fade in" id="caixaDeMensagemErro" 
     tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog "  >
        <div class="modal-content  ">
            <div class="modal-body alert-danger">
                <button type="button" 
                        class="bootbox-close-button close" 
                        style="margin-top: -10px;"
                        onclick="javascript:document.getElementById('caixaDeMensagemErro').style.display='none'">×</button>
                <div class="bootbox-body"><?=$mensagem;?>
                </div>

            </div>
            <div class="modal-footer ">
                <button data-bb-handler="ok" 
                        type="button" 
                        class="btn btn-primary" 
                        onclick="javascript:document.getElementById('caixaDeMensagemErro').style.display='none'">
                    OK</button>

            </div>
        </div>
    </div>
</div>
    


<div class="bootbox modal fade in" id="caixaDeMensagemDialogo" 
     tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog "  >
        <div class="modal-content  ">
            <div class="modal-body alert-danger">
                <button type="button" 
                        class="bootbox-close-button close" 
                        style="margin-top: -10px;"
                        onclick="javascript:document.getElementById('caixaDeMensagemDialogo').style.display='none'">×</button>
                <div class="bootbox-body"><?=$mensagem;?>
                </div>

            </div>
            <div class="modal-footer ">
                <button data-bb-handler="ok" 
                        type="button" 
                        class="btn btn-primary" 
                        onclick="javascript:document.getElementById('caixaDeMensagemDialogo').style.display='none'">
                    OK</button>

            </div>
        </div>
    </div>
</div>

        <? if(strlen(trim(Helper::GET("msgSucesso"))) > 0 && !strlen($esconder)){ ?>

            <script language="javascript">

            	document.getElementById("caixaDeMensagemSucesso").style.display = "block";
                document.getElementById("botao_sucesso_ok").focus();

            </script>

          <? } ?>

          <? if(strlen(trim(Helper::GET("msgErro"))) > 0 && !strlen($esconder)){ ?>

            <script language="javascript">

            	document.getElementById("caixaDeMensagemErro").style.display = "block";
                var botaoOk = document.getElementById("botao_erro_ok");
                if(botaoOk != null)
                botaoOk.focus();

            </script>

          <? } ?>
