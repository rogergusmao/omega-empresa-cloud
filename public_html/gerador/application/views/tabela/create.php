<?php 
$this->layout = '~/views/shared/_defaultLayout.php';
?>

<div class='row'>
<div class='col-sm-12'>
  <div class='box'>
    <div class='box-header red-background'>
      <div class='title'>
        <div class='icon-edit'></div>
        Nova tabela
      </div>
      <div class='actions'>
        <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
        </a>
      </div>
    </div>
    <div class='box-content'>
      <form action="/gerador/tabela/create" method="post">
        <input name="authenticity_token" type="hidden" /><div class='form-group'>
        <label for='inputText'>Nome</label>
        <input class='form-control' id='inNome' name='inNome' placeholder='Nome' type='text'>
      </div>
      <div class='form-group'>
        <label for='inputTextArea'>Descrição</label>
        <textarea class='form-control' id='inDescricao' name="inDescricao" placeholder='Descrição' rows='3'></textarea>
      </div>
      <div class='form-group'>
        <label>Banco de dados</label>
        <label class='checkbox-inline'>
            <input id="ckAndroid" name="ckAndroid" type='checkbox' value='on'>
            Android
          </label>
        <label class='checkbox-inline'>
            <input id='ckWeb' name='ckWeb' type='checkbox' value='on'>
            Web
        </label>
      </div>
          </form>
      <div class='form-group'>
          <?php include(MyHelpers::UrlContent('~/views/atributo/index.php')); ?>
      </div>
      <div class='form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg' style='margin-bottom: 0;'>
          
          <div class='btn btn-primary btn-lg' a="aSalvar" onclick="tabela.salvar()">
          <i class='icon-save'></i>
          Save
        </div>
      </div>
      

    </div>
  </div>
</div>
</div>


<script type="text/javascript">
    function Tabela(){
        
        this.nome = "";
        this.descricao = "";
        this.isAndroid = false;
        this.isWeb = false;
        
        this.atributos = [];
        
        this.salvar = function(){

            try{
                var post = this;
                var idImg = 'imgSalvarTabela';
                var idBotao = 'aSalvar'
                $('#' + idImg).css('display', 'inline-block');
                $('#' + idBotao).css('display', 'none');
                
                var funcaoASerChamada = function () {
                    try{
                        var retorno = arguments[0];
                        var codRetorno = retorno.mCodRetorno;

                        if (codRetorno == OPERACAO_REALIZADA_COM_SUCESSO) {
                            bootbox.alert({
                                message: "Dados atualizados com sucesso",
                            });
                           return true;
                        }
                        else {
                            bootbox.alert({
                                message: "Ocorreu um erro durante a atualização entre em contato com nossa central.",
                            });
                            return false;
                        }
                    }catch(err){
                        bootbox.alert({
                            message: err.stack,
                        });
                        return false;
                    }finally{
                        
                        $('#' + idImg).css('display', 'none');
                        $('#' + idBotao).css('display', 'inline-block');
                    }
                }
                carregarValorRemotoAsyncPost(
                    'webservice.php?class=EXTDAO_Lista&action=salvar',
                    null,
                    funcaoASerChamada, 
                    post);

            }catch(err){
                $('#imgLogando').css('display', 'none');
                $('#aLogin').css('display', 'inline-block');
                bootbox.alert({
                        message: err.stack,
                    });
                    return false;
            }finally{

            }
            return false;
        }
    }
    
    function atualizaTabelaJs(){
        tabela.nome = $('#inNome').val();
        tabela.isAndroid = $('#ckAndroid').val() == "on" ? true : false;
        tabela.isWeb = $('#ckWeb').val() == "on" ? true : false;
        tabela.descricao = $('#inDescricao').val();
    }
    
    
    
    var tabela = new Tabela();
</script>
