/*global define */
define([
	'underscore',
	'backbone',
	'models/tipo_campo',
        'common'
], function (_, Backbone, TipoCampo, Common) {
	'use strict';
        
	var TipoCampoCollection = Backbone.Collection.extend({
            // Reference to this collection's model.
            model: TipoCampo,
            url: Common.PATH_PROJETO + '/tipoCampo/read/',
            constructor: function(){
                Backbone.Collection.apply(this, [jsonTipoCampoItens], {});
                
                
                
                
            }

	});

	return TipoCampoCollection;
});
