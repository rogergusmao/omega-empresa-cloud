/*global define */
define([
	'underscore',
	'backbone',
	'models/atributo'
], function (_, Backbone, Atributo) {
	'use strict';

	var AtributoCollection = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: Atributo,
	});

	return new AtributoCollection();
});
