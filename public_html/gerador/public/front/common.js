/*global define*/
'use strict';

define([
    "jquery",
    'underscore',
    'backbone',
], function ( $, _, Backbone) {
    
     var Common = {
        root : "gerador/",                     // The root path to run the application through.
        URL : "http://gerador.local:8080",                      // Base application URL
        API : "gerador/",                   // Base API URL (used by models & collections)
        
        Dominio: function() { return "http://geral.local:8080/gerador/"},
        
        DEBUG: true,
        // Show alert classes and hide after specified timeout
        showAlert: function(title, text, klass) {
            $("#header-alert").removeClass("alert-danger alert-warning alert-success alert-info");
            $("#header-alert").addClass(klass);
            $("#header-alert").html('<button class="close" data-dismiss="alert">�</button><strong>' + title + '</strong> ' + text);
            $("#header-alert").show('fast');
            setTimeout(function() {
                $("#header-alert").hide();
            }, 7000 );
        },
        // What is the enter key constant?
        ENTER_KEY: 13,
        ESCAPE_KEY: 27,
        PATH_PROJETO: '/gerador',
        
        mySyncFunction: function(method, model, options){
            
            if(method=='GET'){
              options.url = '/gerador/' + model.name ; 
            }else{
               options.url = '/gerador/' + model.name +'/' + method; 
            }
            return Backbone.sync(method, model, options);
        },
        
        webservice: function(method, model, options) {
            var type =method;
            if(method=='GET'){
              options.url = '/gerador/' + model.name ; 
            }else{
               options.url = '/gerador/' + model.name +'/' + method; 
            }
            
            
            _.defaults(options || (options = {}), 
                {emulateJSON: true, type: 'POST'});
            
            // Default options, unless specified.
            _.defaults(options || (options = {}), {
              emulateHTTP: Backbone.emulateHTTP,
              emulateJSON: Backbone.emulateJSON
            });

            // Default JSON-request options.
            var params = {type: type, dataType: 'json'};

            // Ensure that we have a URL.
            if (!options.url) {
              params.url = _.result(model, 'url');
            }

            // Ensure that we have the appropriate request data.
//            if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
              params.contentType = 'application/json';
              params.data = JSON.stringify(options.attrs || model.toJSON(options));
//            }

            // For older servers, emulate JSON by encoding the request into an HTML-form.
//            if (options.emulateJSON) {
//              params.contentType = 'application/x-www-form-urlencoded';
//              params.data = params.data ? {model: params.data} : {};
//            }

            // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
            // And an `X-HTTP-Method-Override` header.
            if (options.emulateHTTP && (type === 'PUT' || type === 'DELETE' || type === 'PATCH')) {
              params.type = 'POST';
              if (options.emulateJSON) params.data._method = type;
              var beforeSend = options.beforeSend;
              options.beforeSend = function(xhr) {
                xhr.setRequestHeader('X-HTTP-Method-Override', type);
                if (beforeSend) return beforeSend.apply(this, arguments);
              };
            }

            // Don't process data on a non-GET request.
            if (params.type !== 'GET' && !options.emulateJSON) {
              params.processData = false;
            }

            // If we're sending a `PATCH` request, and we're in an old Internet Explorer
            // that still has ActiveX enabled by default, override jQuery to use that
            // for XHR instead. Remove this line when jQuery supports `PATCH` on IE8.
            if (params.type === 'PATCH' && noXhrPatch) {
              params.xhr = function() {
                return new ActiveXObject("Microsoft.XMLHTTP");
              };
            }

            // Make the request, allowing the user to override any Ajax options.
            var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
            model.trigger('request', model, xhr, options);
            return xhr;
        },
        
        setFormInModel: function(seletorForm, model){
             var formData = this.getFormData(seletorForm);
             //this.model = new Atributo( formData );
             this.resetModel(model, formData);
         },
         
         resetModel: function(model, novosAtributos){
             var attrs = _.keys(model.attributes);
             
             for(var i = 0 ; i < attrs.length; i++){
                 var attr = attrs[i];
                 model.set(attr, null, {silent: true});
             }
             for(var i = 0 ; i < attrs.length; i++){
                 var attr = attrs[i];
                 model.set(attr, novosAtributos[attr], {silent: true});
             }
             model.trigger('change');
         },
         
         clearFormData: function(seletorForm){
            $( seletorForm ).find( 'select' ).each( function( i, el ) {
                $( el ).val('');
            });

            $( seletorForm ).find( 'input' ).each( function( i, el ) {
                if( $( el ).val() != '' )
                {
                    $( el ).val('');
                }
            });
         },
         
        formatarNomeSql: function (str){
            str = removerAcentosDaString(str);
            str = str.replace(/[^A-Za-z0-9\_]/g, '_');
            return str;
        },
        getFormData: function(seletorForm, pFormData) {
            var formData = null;
            if(pFormData == null)
                formData = {};
            else 
                formData =  pFormData;


            $( seletorForm ).find( 'select' ).each( function( i, el ) {
                var valorEl = $( el ).val();
                if(!_.isNumber(valorEl)){
                    var objJson = JSON.parse(valorEl);
                    formData[ el.id ] = objJson.id;
                }else{
                    formData[ el.id ] = valorEl;
                }
            });

            $( seletorForm ).find( 'input' ).each( function( i, el ) {
                formData[ el.id ] = $( el ).val();
            });
            return formData;

        },

    };

    $.ajaxSetup({ cache: false });          // force ajax call on all browsers


    // Global event aggregator
    Common.eventAggregator = _.extend({}, Backbone.Events);

    return Common;
    
});
