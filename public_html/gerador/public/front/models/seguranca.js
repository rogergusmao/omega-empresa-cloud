/*global define*/
define([
	'underscore',
	'backbone',
        'common'
], function (_, Backbone, Common) {
	'use strict';
        var Seguranca = Backbone.Model.extend({
            name: 'seguranca',
            sync: Common.mySyncFunction,
            // Default attributes for the todo
            // and ensure that each todo created has `title` and `completed` keys.
             defaults: function(){
                return {
                    email: false,
                    senha: null,
                };
            },
	});

	return Seguranca;
});
