/*global define*/
define([
	'underscore',
	'backbone'
], function (_, Backbone) {
	'use strict';

	var Atributo = Backbone.Model.extend({
                name: 'campo',
		// Default attributes for the todo
		// and ensure that each todo created has `title` and `completed` keys.
		 defaults: function(){
                    return {
                        nomeExibicao: '',
                        nome : '',
                        descricao : '',
                        tipoFicticio : '',
                        tamanho : '',
                        casasDecimais : '',
                        tabela : '',
                        cardinalidade : '',
                        crud : '',
                        labelTipoFicticio: ''
                    };
                },
                
                


	});

	return Atributo;
});
