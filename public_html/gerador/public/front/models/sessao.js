/*global define*/
define([
	'underscore',
	'backbone'
], function (_, Backbone) {
	'use strict';

	var Sessao = Backbone.Model.extend({
            name: 'sessao',
            // Default attributes for the todo
            // and ensure that each todo created has `title` and `completed` keys.
             defaults: function(){
                return {
                    autenticado: false,
                    usuario: null,
                    senha: null,
                    identificadorUsuario_sessao: null,
                    corporacao: null,
                    projetos_versao: null,

                };
            },
	});

	return Sessao;
});
