/*global define*/
define([
	'underscore',
	'backbone',
        'common',
], function ( _, Backbone, Common) {
	'use strict';

	var Tabela = Backbone.Model.extend({
            name: 'lista',
            sync: Common.mySyncFunction,
//            urlRoot: Common.PATH_PROJETO + '/lista/',
            // Default attributes for the todo
            // and ensure that each todo created has `title` and `completed` keys.
            defaults: function(){
                return {
                    nome: '',
                    descricao: '',
                    isAndroid: false,
                    isWeb: false,
//                    atributos: new Backbone.Collection.extend({
//                        model: Atributo
//                    })
                };
            },
           
            validate: function (attrs) {
              var errors = {};
              
              if (_.isNaN(attrs.nome)) errors.nome = "O nome � obrigat�rio.";
              
              if (!_.isEmpty(errors)) {
                return errors;
              }
            }

	});

	return Tabela;
});
