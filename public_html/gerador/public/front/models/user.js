/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "jquery",
    'underscore',
    "common",
    "backbone"
], function($, _, Common, Backbone){

    var User = Backbone.Model.extend({

        name: 'usuario',

        initialize: function(){
//            _.bindAll(this);
        },

        defaults: {
            email: '',
            nome: ''
        },

        url: function(){
            return Common.API + '/user';
        }

    });
    
    return User ;
});