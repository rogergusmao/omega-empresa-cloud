/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "jquery",
    'underscore',
    "common",
    "models/user",
    "backbone",
    'jquerycookie',
    'protocolo_sistema',
    
], function($, _, Common, User, Backbone, Cookie, ProtocoloSistema){

    var Session = Backbone.Model.extend({
        name: 'seguranca',
        sync: Common.mySyncFunction,
        // Initialize with negative/empty defaults
        // These will be overriden after the initial checkAuth
        defaults: {
            email: null,
            senha: null,
            autenticado: false,
            identificadorUsuario: ''
        },

        initialize: function(){
//            _.bindAll(this);

            // Singleton user object
            // Access or listen on this throughout any module with Common.session.user
            this.user = new User({});
        },


        url: function(){
            return Common.API + '/login';
        },

        // Fxn to update user attributes after recieving API response
        updateSessionUser: function( userData ){
            this.user.set(_.pick(userData, _.keys(this.user.defaults)));
        },
        /*
         * Check for session from API 
         * The API will parse client cookies using its secret token
         * and return a user object if authenticated
         */
        checkAuth: function(callback, args) {
            var self = this;
            var idClienteCookie = $.cookie('IdCliente');
            var data = null;
            if(idClienteCookie != null){
                data = {idClienteCookie: idClienteCookie};
                this.fetch({ 
                    data: data,
                    success: function(mod, res){
                        if(res.mCodRetorno == ProtocoloSistema.OPERACAO_REALIZADA_COM_SUCESSO){
                            self.updateSessionUser(res.usuario);
                            self.set({ autenticado : true });
                            if('success' in callback) callback.success(mod, res);    
                        } else {
                            self.set({ autenticado : false });
                            if('error' in callback) callback.error(mod, res);    
                        }
                    }, error:function(mod, res){
                        self.set({ autenticado : false });
                        if('error' in callback) callback.error(mod, res);    
                    }
                }).complete( function(){
                    if('complete' in callback) callback.complete();
                });
            } else {
                if('complete' in callback) callback.complete();
            }
            
        },


        /*
         * Abstracted fxn to make a POST request to the auth endpoint
         * This takes care of the CSRF header for security, as well as
         * updating the user and session after receiving an API response
         */
        postAuth: function(opts, callback, args){
            var self = this;
            var postData = _.omit(opts, 'method');
            if(DEBUG) console.log(postData);
            $.ajax({
                url: this.url() + '/' + opts.method,
                contentType: 'application/json',
                dataType: 'json',
                type: 'POST',
                beforeSend: function(xhr) {
                    // Set the CSRF Token in the header for security
                    var token = $('meta[name="csrf-token"]').attr('content');
                    if (token) xhr.setRequestHeader('X-CSRF-Token', token);
                },
                data:  JSON.stringify( _.omit(opts, 'method') ),
                success: function(res){

                    if( !res.error ){
                        if(_.indexOf(['login', 'signup'], opts.method) !== -1){

                            self.updateSessionUser( res.user || {} );
                            self.set({ identificadorUsuario: res.user.id, autenticado: true });
                        } else {
                            self.set({ autenticado: false });
                        }

                        if(callback && 'success' in callback) callback.success(res);
                    } else {
                        if(callback && 'error' in callback) callback.error(res);
                    }
                },
                error: function(mod, res){
                    if(callback && 'error' in callback) callback.error(res);
                }
            }).complete( function(){
                if(callback && 'complete' in callback) callback.complete(res);
            });
        },


        login: function(opts, callback, args){
//            this.postAuth(_.extend(opts, { method: 'login' }), callback);
            var options = {};
            var contexto = this;
            Common.setFormInModel('#loginForm', this);
            options.success = function(ret) {
               if(ret.mCodRetorno == ProtocoloSistema.OPERACAO_REALIZADA_COM_SUCESSO){
                   var obj = ret.mObj;
                   contexto.updateSessionUser( obj.usuario || {} );
                   $.cookie('IdCliente', obj.identificadorUsuario, { expires: 7 });
                   contexto.set({ identificadorUsuario: obj.identificadorUsuario, autenticado: true });
               } else {
                    contexto.set({ autenticado: false });
               }
             };
            Common.webservice("login", this, options) ;
        },

        logout: function(opts, callback, args){
            this.postAuth(_.extend(opts, { method: 'logout' }), callback);
        },

        signup: function(opts, callback, args){
            this.postAuth(_.extend(opts, { method: 'signup' }), callback);
        },

        removeAccount: function(opts, callback, args){
            this.postAuth(_.extend(opts, { method: 'remove_account' }), callback);
        }
    });
    
    return Session;
});
