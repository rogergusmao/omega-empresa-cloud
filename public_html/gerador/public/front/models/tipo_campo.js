/*global define*/
define([
        'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	'use strict';

	var TipoCampo = Backbone.Model.extend({
            name: 'TipoCampo',
            
            // Default attributes for the todo
            // and ensure that each todo created has `title` and `completed` keys.
            defaults: function(){
                return {
                    nome : '',
                    descricao : '',
                    seq_INT: 0
                };
            },

	});
//        // Set the default implementation of `Backbone.ajax` to proxy through to `$`.
//        // Override this if you'd like to use a different library.
//        Backbone.ajax = function() {
//               $.ajax({
//                url: arguments.url,
//                type: arguments.type,
//                data: arguments.data,
//                dataType: 'json',
//                success: function (data) {
//                    
//                    var k = 0;
//                    return data;
//                }
//            }).done(function () {
//                var arg = arguments[0];
//                funcaoASerChamada(arg);
//            });
//          //return Backbone.$.ajax.apply(Backbone.$, arguments);
//          
//        };
        
	return TipoCampo;
});
