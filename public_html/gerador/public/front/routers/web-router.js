define([
    "jquery",
    'underscore',
    "common",
    "models/session",
    "models/user",
    "views/header-view",
    "views/login",
    "views/form-tabela",
    "views/app",
    "backbone"
], function($, _, Common, Session, User, 
            HeaderView, 
            LoginView,
            FormTabelaView, 
            AppView,
            Backbone){

    var WebRouter = Backbone.Router.extend({

        initialize: function(){
            //What _.bindAll does is documented here, but it is essentially built exactly for this purpose. If you want to have this properly set in all functions of the object, you can call _.bindAll(this) with no list of functions. I tend to have this global bind function in most of my views.

            
        },

        routes: {
            "" : "index",
            "gerador/index" : "index",
            "gerador/" : "index",
            "gerador/AuthLogin" : "carregaPaginaAutenticada",
            "gerador/tabela": "tabela"
        },

        desviaParaPaginaSeguraSeNecessario: function(a, b, c){
            if(Common.session.get('autenticado') 
                    && $('.content-auth').length == 0){
//                this.navigate("gerador/index", {replace: true});
                
                location.href = Common.URL +"/" + Common.API;
            }
        },

        show: function(view, options){

            // Every page view in the router should need a header.
            // Instead of creating a base parent view, just assign the view to this
            // so we can create it if it doesn't yet exist
            if(!this.headerView && Common.session.get('autenticado') == true ){
                this.headerView = new HeaderView({});
                this.headerView.setElement($(".header")).render();
            }

            // Close and unbind any existing page view
            if(this.currentView && _.isFunction(this.currentView.close)) this.currentView.close();

            // Establish the requested view into scope
            this.currentView = view;

            // Need to be authenticated before rendering view.
            // For cases like a user's settings page where we need to double check against the server.
            if (typeof options !== 'undefined' && options.requiresAuth){        
                var self = this;
                Common.session.checkAuth({
                    success: function(res){
                        // If auth successful, render inside the page wrapper
                        $('#content').html( self.currentView.render().$el);
                    }, error: function(res){
                        self.navigate("/", { trigger: true, replace: true });
                    }
                });

            } else {
                // Render inside the page wrapper
                this.currentView.render();
                $('#content').html(this.currentView.$el);
                //this.currentView.delegateEvents(this.currentView.events);        // Re-delegate events (unbound when closed)
            }

        },

//        carregaPaginaAutenticada: function(){
//          alert("entrou")  ;
//        },
        
        tabela: function() {
            this.show(new FormTabelaView({}));
        },

        

        index: function() {
            // Fix for non-pushState routing (IE9 and below)
            
            
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) {
                this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            }
            else {
                if(!Common.session.get('autenticado')){
                    var view = new LoginView({});
                    Common.session.off("change:autenticado", this.desviaParaPaginaSeguraSeNecessario, this);
                    Common.session.on("change:autenticado", this.desviaParaPaginaSeguraSeNecessario, this);
                    
                    this.show(view);
                }
                else
                    this.show(new AppView({}));
            }
        }

    });
//    _.bindAll(WebRouter, 'desviaParaPaginaSeguraSeNecessario');
    return WebRouter;

});