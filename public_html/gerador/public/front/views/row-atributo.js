/*global define*/
define([
	'jquery',
	'underscore',
	'backbone',
        'models/atributo',
	'text!templates/row-atributo.html',
	'common',
        'views/modal-form-atributo'
], function (
        $, 
        _, 
        Backbone, 
        Atributo, 
        rowAtributoTemplate, 
        Common) {
	'use strict';

	var RowArtibutoView = Backbone.View.extend({
            
            tagName: 'tr',

            className: '.row-atributo',

            // The DOM events specific to an item.
           // The DOM events specific to an item.
            events:{
               'click .delete-atributo': 'deletarAtributo',
               "click .edit-atributo":   "editarAtribuito",
            },
            
            initialize: function(model, $viewModalFormAtributoView) {
                this.model = model;
                this.$viewModalFormAtributoView = $viewModalFormAtributoView;  
            },
            
            render: function() {
                //this.el is what we defined in tagName. use $el to get access to jQuery html() function
                var htmlTemplate = _.template(rowAtributoTemplate)(this.model.attributes);
                this.$el.html( htmlTemplate );
                
                return this;
            },
            
            deletarAtributo: function() {
                //Delete model
                this.model.destroy();

                //Delete view
                this.remove();
            },
            
            editarAtribuito: function(e){
                e.preventDefault();
                
                this.$viewModalFormAtributoView.model = this.model;
                this.$viewModalFormAtributoView.render();
//                this.$el.append(this.$viewModalFormAtributoView.render().el);
                this.$viewModalFormAtributoView.show();
            }
	});

	return RowArtibutoView;
});
