define([
    "jquery",
    'underscore',
    "common",
    "backbone"
], function($, _, Common, Backbone){

    var HeaderView = Backbone.View.extend({
        el: "#headerView",
        
        
        
        initialize: function () {
//            _.bindAll(this);

            // Listen for session autenticado state changes and re-render
            Common.session.on("change:autenticado", this.onLoginStatusChange);
        },
        
        events: {
            "click #logout-link"         : "onLogoutClick",
            "click #remove-account-link" : "onRemoveAccountClick"
        },
      

        onLogoutClick: function(evt) {
            evt.preventDefault();
            Common.session.logout({});  // No callbacks needed b/c of session event listening
        },

        onRemoveAccountClick: function(evt){
            evt.preventDefault();
            Common.session.removeAccount({});
        },


        render: function () {
            if(Common.DEBUG) console.log("RENDER::", Common.session.user.toJSON(), Common.session.toJSON());
//            //TODO descomentar
//            this.$el.html(this.template({ 
//                autenticado: Common.session.get("autenticado"),
//                user: Common.session.user.toJSON() 
//            }));
            return this;
        },
        
        
        onLoginStatusChange: function(evt, callback, contexto){

            contexto.render();
            if(Common.session.get("autenticado")) Common.showAlert("Success!", "Logged in as "+Common.session.user.get("username"), "alert-success");
            else Common.showAlert("See ya!", "Logged out successfully", "alert-success");

            
        },

    });

    return HeaderView;
});