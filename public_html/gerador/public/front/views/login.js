define([
    "jquery",
    'underscore',
    "common",
    "text!templates/login.html",
    "backbone"
], function($, _, Common, loginTemplate, Backbone){

    var LoginView = Backbone.View.extend({
        el: '#content-app',
        initialize: function () {
            //_.bindAll(this);

            // Listen for session autenticado state changes and re-render
//            Common.session.on("change:autenticado", this.render);
//            Common.session.on("change:autenticado", this.reloadPage);
            
        },

        events: {
            'click #login-btn'                      : 'onLoginAttempt',
            'click #signup-btn'                     : 'onSignupAttempt',
            'keyup #senha'                          : 'onPasswordKeyup',
            'keyup #signup-password-confirm-input'  : 'onConfirmPasswordKeyup'
        },

        // Allow enter press to trigger login
        onPasswordKeyup: function(evt){
            var k = evt.keyCode || evt.which;

            if (k == 13 && $('#senha').val() === ''){
                evt.preventDefault();    // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onLoginAttempt();
                return false;
            }
        },

        // Allow enter press to trigger signup
        onConfirmPasswordKeyup: function(evt){
            var k = evt.keyCode || evt.which;

            if (k == 13 && $('#confirm-password-input').val() === ''){
                evt.preventDefault();   // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onSignupAttempt();
                return false;
            }
        },

        onLoginAttempt: function(evt){
            if(evt) evt.preventDefault();

            //if(this.$("#login-form").parsley('validate')){
            var data = {
                    email: this.$("#email").val(),
                    senha: this.$("#senha").val()
                };
            Common.session.login(data, {
                success: function(mod, res){
                    
                    if(Common.DEBUG) console.log("SUCCESS", mod, res);
                },
                error: function(err){
                    if(Common.DEBUG) console.log("ERROR", err);
                    Common.showAlert('Bummer dude!', err.error, 'alert-danger'); 
                }
            });
//            } else {
//                // Invalid clientside validations thru parsley
//                if(Common.DEBUG) console.log("Did not pass clientside validation");
//
//            }
        },
        

        onSignupAttempt: function(evt){
            if(evt) evt.preventDefault();
            if(this.$("#signup-form").parsley('validate')){
                Common.session.signup({
                    username: this.$("#signup-username-input").val(),
                    password: this.$("#signup-password-input").val(),
                    name: this.$("#signup-name-input").val()
                }, {
                    success: function(mod, res){
                        if(Common.DEBUG) console.log("SUCCESS", mod, res);

                    },
                    error: function(err){
                        if(Common.DEBUG) console.log("ERROR", err);
                        Common.showAlert('Uh oh!', err.error, 'alert-danger'); 
                    }
                });
            } else {
                // Invalid clientside validations thru parsley
                if(Common.DEBUG) console.log("Did not pass clientside validation");

            }
        },
        close: function(){
            
        },
        

        render:function (evt, callback, context) {
            if(Common.session.get('autenticado') == true){
                //TODO - se tiver um template padr�o da area logada, esse dever� ser padronizado aqui
                //se o conteudo autenticado j� foi carregado
//                if(this.$el != null && this.$el.find('.content-autenticado')){
////                    this.loginView = new AppView();
////                    this.$el
////                    WebRouter.navigate('views/app', {trigger: true}); 
//                    Backbone.history.navigate(Common.API, {trigger: true});
//                } else {
//                    Backbone.history.navigate(Common.API, {trigger: true});
//                    
//                }
                //Recarrega a p�gina
                //Se a pagina n�o j� tiver sido recarregada
//                if($('.content-autenticado').length == 0){
////                    Backbone.history.navigate(Common.API+ "/AuthLogin", {trigger: true});    
//                    location.href = location.href;
//                }
                
            } else {
                this.template = _.template(loginTemplate);
                    //            else this.template = _.template(loginTemplate); 
                var html = this.template({ user: Common.session.user.toJSON() });
                this.$el.html(html);
                return this;
                
            }

        }

    });
//    LoginView.recarregarPagina = _.throttle(function(){
//        location.href = location.href;
//    }, 2000) ;
    return LoginView;
});