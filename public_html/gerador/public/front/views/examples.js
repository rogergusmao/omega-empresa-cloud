///* 
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//
///**
//     * View for the overall application. We need this because backbone can only
//     * bind events for children of 'el'.
//     *
//     * In our template our modal is inside #app, so this class handles
//     * interaction at the application level rather than strictly with a
//     * collection of Passwords (that's the job of the PasswordListView).
//     */
//    var PasswordPanelView = Backbone.View.extend({
//        el: '#passwordPanel',
//        events: {
//            "click #passwordForm :submit": "handleModal",
//            "keydown #passwordForm": "handleModalOnEnter",
//            "hidden #passwordModal": "prepareForm"
//        },
//
//        initialize: function() {
//            this.passwordList = new PasswordListView({app: this});
//        },
//
//        displayError: function(model, response) {
//            var that = this;
//            if (response.status == 403) {
//                alert("You don't have permission to edit that data");
//            }
//            else {
//                alert("Unable to create or edit that data. Please make sure you entered valid data.");
//            }
//        },
//
//        render: function() {
//            this.$el.find('table').append(this.passwordList.render().el);
//        },
//
//        /**
//         * Allows users to update an existing password
//         *
//         * @param Password password: A Password Model of the password to edit.
//         */
//        editPassword: function(password) {
//            this.prepareForm(password.toJSON());
//            // store the password ID as data on the modal itself
//            $('#passwordModal').data('passwordId', password.get('id'));
//            $('#passwordModal').modal('show');
//        },
//
//        /**
//         * Sets up the password form.
//         *
//         * @param object passwordData: An object containing data to use for the
//         * form values. Any fields not present will be set to defaults.
//         */
//        prepareForm: function(passwordData) {
//            passwordData = passwordData || {};
//            
//            var data = {
//                'title': '',
//                'username': '',
//                'password': '',
//                'url': '',
//                'notes': ''
//            };
//
//            $.extend(data, passwordData);
//
//            var form = $('#passwordForm');
//            $(form).find('#id_title').val(data.title);
//            $(form).find('#id_username').val(data.username);
//            $(form).find('#id_password').val(data.password);
//            $(form).find('#id_url').val(data.url);
//            $(form).find('#id_notes').val(data.notes);
//
//            // create an array of the selected shares
//            var shares = _.map(data.shares, function(elem){
//                return elem.id;
//            });
//
//            var value;
//            $(form).find(':checkbox').each(function(){
//                value = $(this).attr('value');
//                // for each checkbox, see if the value is in the 'shares' array
//                for (var i in shares)
//                {
//                    if (value == shares[i])
//                    {
//                        $(this).prop('checked', true);
//                        return;
//                    }
//                }
//
//                // otherwise uncheck it
//                $(this).prop('checked', false);
//            });
//
//            // clear any previous references to passwordId in case the user
//            // clicked the cancel button
//            $('#passwordModal').data('passwordId', '');
//        },
//
//        handleModal: function(event) {
//            event.preventDefault();
//            event.stopImmediatePropagation();
//            var form = $('#passwordForm');
//
//            var passwordData = {
//                title: $(form).find('#id_title').val(),
//                username: $(form).find('#id_username').val(),
//                password: $(form).find('#id_password').val(),
//                url: $(form).find('#id_url').val(),
//                notes: $(form).find('#id_notes').val(),
//                shares: $(form).find(':checked').map(function() {
//                    return $(this).attr('value');
//                }).get()
//            };
//
//            if ($('#passwordModal').data('passwordId'))
//            {
//                passwordData.id = $('#passwordModal').data('passwordId');
//                this.passwordList.updatePassword(passwordData, { error: this.displayError });
//            }
//            else
//            {
//                // add or update the password
//                this.passwordList.addNew(passwordData, { error: this.displayError });
//            }
//
//            // hide the modal
//            $('#passwordModal').modal('hide');
//
//            return this;
//        },
//
//        handleModalOnEnter: function(event) {
//            // process the modal if the user pressed the ENTER key
//            if (event.keyCode == 13)
//            {
//                return this.handleModal(event);
//            }
//        }
//    });
