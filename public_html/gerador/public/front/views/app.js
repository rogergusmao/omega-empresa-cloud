/*global define*/
define([
	'jquery',
	'underscore',
	'backbone',
        'models/sessao',
	'models/tabela',
        'views/login',
	'views/form-tabela',
        'collections/tipo_campo',
	'common'
], function ($, _, Backbone, Sessao, Tabela, LoginView, FormTabelaView, TipoCampoCollection, Common) {
	'use strict';

	// Our overall **AppView** is the top-level piece of UI.
	var AppView = Backbone.View.extend({

		// Instead of generating a new element, bind to the existing skeleton of
		// the App already present in the HTML.
		el: '#gerador-app',

		// Delegated events for creating new items, and clearing completed ones.
		events: {
		},

		// At initialization we bind to the relevant events on the `Todos`
		// collection, when items are added or changed. Kick things off by
		// loading any preexisting todos that might be saved in *localStorage*.
		initialize: function () {
                    
                    
                    this.$footer= this.$('#footer');
                    this.$contentBody= this.$('#content-body');
                    
//                  var view = new FormTabelaView();
//                    this.model = new Sessao();
//                    this.model.on('change:usuario', this.iniciarSessaoDoUsuario);
                    
//                    var view = new LoginView();
//                    view.setSessaoModel(this.model);
//                    this.$contentBody.append(view.render().el);
                },
                
		// Re-rendering the App just means refreshing the statistics -- the rest
		// of the app doesn't change.
		render: function () {
                    this.$footer.show();
                    return this;
		},
                
                iniciarSessaoDoUsuario: function() {
                    var view = new TabelaView();
                }

	});

	return AppView;
});
