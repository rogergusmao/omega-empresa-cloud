/*global define*/
define([
	'jquery',
        'jqueryui',
	'underscore',
	'backbone',
        'models/atributo',
        'collections/tipo_campo',
        'collections/atributo',
	'text!templates/modal-form-atributo.html',
	'common'
], function ($, 
            jqueryui, 
            _, 
            Backbone, 
            Atributo, 
            TipoCampoCollection, 
            AtributoCollection, 
            modalFormAtributoTemplate, 
            Common) {
	'use strict';

	var ModalFormAtributoView = Backbone.View.extend({

            tagName: 'div',

            className: '.modal-form-atributo',

            template: _.template(modalFormAtributoTemplate),
            
            // The DOM events specific to an item.
            events:{
               'keyup #nomeExibicao': 'atualizarNome',
               'change #tipoFicticio': 'onChangeTipoFicticio',
               'click #adicionar-atributo': 'adicionarAtributo'
            },
            
            adicionarAtributo: function(e){
                e.preventDefault();
                if(!this.validate()){
                    return;
                }
                var formData = Common.getFormData('#formAtributo');
                var novoAtributo = new Atributo( formData );
                this.atributoCollection.add(novoAtributo);
                
                this.clear();
            },
            
            
            
            atualizarNome: function(e){
                var $obj = $(e.currentTarget);
                var str = $obj.val();
                str = Common.formatarNomeSql(str);
                this.$el.find('#nome').val(str);  
            },
            
            initialize: function ($parentView, atributoCollection) {
                var contexto = this;
                contexto.Common = Common;
                this.$parentView = $parentView;
                if(this.model == null){
                    this.model = new Atributo();
                    this.action = "insert";
                } else {
                    this.action = "update";
                }
                this.atributoCollection = atributoCollection;
                
                this.atributoCollection =  AtributoCollection;
                this.tipoCampoCollection = new TipoCampoCollection();
                
//                this.listenTo(
//                    this.tipoCampoCollection, 
//                    'add', 
//                    this.renderTipoCampo);
                    
                this.listenTo(
                    this.model, 
                    'change', 
                    this.renderTipoFicticio);
                
//                this.tipoCampoCollection.fetch({
//                    success: function(){
////                        contexto.$el.find('#tipoFicticio').append(new Option("Selecione", ""));
//                        _.each(contexto.tipoCampoCollection.models, contexto.renderTipoCampo, contexto);
//                    }
//                });
                
                $.validator.addMethod("nomeduplicado", function(value, element) {
                    
                    value = contexto.Common.formatarNomeSql(value);
                    var atributosDuplicados = contexto.atributoCollection.where({nome: value});
                    
                    if(atributosDuplicados.length > 0 )
                        return false;
                    else return true;
                }, "J� existe um atributo com esse nome");
                $.validator.addMethod("idreservado", function(value, element) {
                  return !( value == "id");
                }, "O nome 'id' � reservado para uso do sistema.");

                $.validator.addMethod("testevalid", function(value, element) {
                  return !( value == "id");
                }, "O nome 'id' � reservado para uso do sistema.");

                
            },
            
            renderTipoFicticio: function(){
                this.$el.find('.categoria-atributo').css('display', 'none');
//                var slTipoFicticio = this.$el.find('#tipoFicticio').val();
                //str = str.replace(/find/gi,?replace?);
                var json = $("#tipoFicticio option:selected").val();
                if(!_.isUndefined(json) && json.length > 0){
                    var valor = JSON.parse(json);
                    var nome = valor.nome;
                    this.$el.find('.categoria-atributo ' ).not('.' + nome).find('.form-control').attr('data-rule-required', 'false');
                    this.$el.find('.categoria-atributo ' ).filter('.' + nome).css('display', 'block');
                    this.$el.find('.categoria-atributo ' ).filter('.' + nome).find('.form-control').attr('data-rule-required', 'true');
                } 
            },
            
            onChangeTipoFicticio: function(){
                
//                var slTipoFicticio = this.$el.find('#tipoFicticio').val();
                //str = str.replace(/find/gi,?replace?);
                var json = $("#tipoFicticio option:selected").val();
                var valor = JSON.parse(json);
                var id = valor.id;
                
                this.model.set('tipoFicticio', id);
            },
            
            renderTipoCampo: function(model,collection,ajaxRequest){
                
                var descricao = model.get('descricao');
                var id = model.get('id');
                var nome = model.get('nome');
                var valor = {id: id, nome: nome};
                var json = JSON.stringify(valor);
                var option = new Option(descricao, json);
                this.$el.find('#tipoFicticio').append(option);
                if(this.model.get('id') == id){
                    //seleciona o valor
                    this.$el.find('#tipoFicticio').val(valor);
                }
            },
            
            render: function() {
//                //re-rendering
//                if(this.$el.html() != ''){
//                    var atributos = _.keys(this.model.attributes);
//                    _.each(
//                        atributos, 
//                        function(attrName) {
//                            //var attrName = attr.name;
//                            var attrValue = this.model.get(attrName);
//                            
//                            this.$el.find('#' + attrName).each(function(){
//                                if($(this).is("select")){
//                                    $(this).find('option').each(function(){
//                                        //select
//                                        var igual = false;
//                                        if(!_.isNumber($(this).val())){
//                                            if($(this).val().indexOf( "\"" + attrValue + "\"") >= 0){
//                                                igual = true;
//                                            }
//                                        } else if($(this).val() == attrValue){
//                                            igual = true;
//                                        }
//
//                                        if(igual){
//                                            var $select = $(this).parent();        
//                                            $select.val($(this).val());
//                                            
//                                        }
//                                    });
//                                    
//                                } else {
//                                    $(this).val(attrValue);
//                                }
//                                
//                            });
//                        }, 
//                        this
//                   );
//                    
//                    this.$el.html
//                    
//                    
//                } else {
                    //se for a se
            this.$el.html( this.template( this.model.attributes ) );
            this.$modal = $(this.$el.find('.modal'));
            this.renderTipoFicticio();
            this.$el.find('#tipoFicticio').append(new Option("Selecione", ""));
            _.each(this.tipoCampoCollection.models, this.renderTipoCampo, this);
            if(this.$parentView != null){
                this.$parentView.html(this.$el);
            }  
//                }

                return this;
            },
            
            
            renderizada: function(){
              if(this.$el.html().length > 0)  
                  return true;
              else return false;
            },
            
            show: function(){
                this.$modal.modal('show');
            },
            
            hide: function(){
                this.$modal.modal('hide');
            },
            
            clear: function(){
                Common.clearFormData('#formAtributo');
                
                this.model.clear();
                this.model = new Atributo();
            },
            
            validate: function(){
                
                var validator  = $("#formAtributo").validate({
                            rules: {
                              field: {
                                required: true,
                                number: true
                              }
                            }
                          });
       
                validator.form();
                return $("#formAtributo").valid();
           } ,   
	});

	return ModalFormAtributoView;
});
