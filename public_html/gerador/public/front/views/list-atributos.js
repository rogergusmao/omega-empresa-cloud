/*global define*/
define([
	'jquery',
	'underscore',
	'backbone',
        'collections/atributo',
        'views/row-atributo',
        'views/modal-form-atributo',
	'text!templates/list-atributos.html',
	'common'
], function (
    $, 
    _, 
    Backbone, 
    AtributosCollection, 
    RowAtributoView, 
    ModalFormAtributoView, 
    listAtributosTemplate, 
    Common) {

	'use strict';

	var ListAtributosView = Backbone.View.extend({
            tagName: 'div',
            className: 'list-atributos',

            template: _.template(listAtributosTemplate),

           // The DOM events specific to an item.
           // The DOM events specific to an item.
           events:{
               'click #add-atributo':'abrirFormularioAtributo'
           },


            // The TodoView listens for changes to its model, re-rendering. Since there's
            // a one-to-one correspondence between a **Todo** and a **TodoView** in this
            // app, we set a direct reference on the model for convenience.
            initialize: function (atributos) {
                
                if(atributos != null)
                    this.collection = atributos;
                else 
                    this.collection = AtributosCollection;
                
                this.listenTo( this.collection, 'add', this.renderAtributo );
                
            },

// Re-render the titles of the todo item.
//            render: function () {
//                    this.$el.html(this.template());
//                    return this;
//            },

            // render library by rendering each book in its collection
            render: function() {
                var html = this.template();
                this.$el.html( html );
                this.collection.each(function( item ) {
                    this.renderAtributo( item );
                }, this );
                
                this.$modalFormAtributo = this.$el.find('#list-atributos-modal-form-atributo');
                this.$viewModalFormAtributoView = new ModalFormAtributoView(this.$modalFormAtributo, this.collection);
                this.$viewModalFormAtributoView.render();
//                this.$modalFormAtributo.html(this.$viewModalFormAtributoView.render().el);
                this.$tbodyAtributos = this.$el.find("#tbodyAtributos");
                return this;
            },

            // render a book by creating a BookView and appending the
            // element it renders to the library's element
            renderAtributo: function( item ) {
                var rowAtributoView = new RowAtributoView(item, this.$viewModalFormAtributoView);
                
                this.$tbodyAtributos.append( rowAtributoView.render().el );
            },
            // Remove the item, destroy the model from *localStorage* and delete its view.
            clear: function () {
                    this.model.destroy();
            },
            
            abrirFormularioAtributo: function(e){
              if( ! _.isUndefined(this.$viewModalFormAtributoView.model.get('id')) ){
                  this.$viewModalFormAtributoView.clear();
              }
              this.$viewModalFormAtributoView.show();
              //to cancel action of link(a)
              return false;
            },
            
            addAtributo: function( e ) {
                e.preventDefault();
                
                var formData = {};
                //recupera todos os atributos do formulario
                $( '#addAtributo div' ).children( 'input' ).each( function( i, el ) {
                    if( $( el ).val() != '' )
                    {
                        formData[ el.id ] = $( el ).val();
                    }
                });

                this.collection.add( new Atributo( formData ) );
            },
	});

	return ListAtributosView;
});
