///*global define*/
//define([
//	'jquery',
//        'jqueryui',
//	'underscore',
//	'backbone',
//        'models/atributo',
//        'models/seguranca',
//        'collections/tipo_campo',
//        'collections/atributo',
//	'text!templates/login.html',
//        'protocolo_sistema',
//        'helper',
//	'common',
//        'bootbox'
//], function ($, 
//            jqueryui, 
//            _, 
//            Backbone, 
//            Atributo, 
//            Seguranca,
//            TipoCampoCollection, 
//            AtributoCollection, 
//            loginTemplate, 
//            ProtocoloSistema,
//            Helper,
//            Common,
//            bootbox) {
//	'use strict';
//
//	var LoginView = Backbone.View.extend({
//
//
//            template: _.template(loginTemplate),
//            
//            // The DOM events specific to an item.
////            events:{
////               'click #formularioLogin': 'login'
////            },
//            events: {
//                "click .loginBt": "login"
//            },
//            
//            initialize: function () {
//                this.model = new Seguranca();
//                
//            },
//            setSessaoModel: function(sessaoModel){
//                this.sessaoModel = sessaoModel;
//            },
//            
//            render: function() {
//                //se for a se
//                this.$el.html( this.template( this.model.attributes ) );
//                
//                return this;
//            },
//            
//            validate: function(){
//                
//                var validator  = $("#loginForm").validate({
//                            rules: {
//                                
//                              field: {
//                                required: true,
//                                number: true
//                              }
//                            }
//                          });
//                validator.form();
//                return $("#loginForm").valid();
//           },
//            
//            login: function(e){
//                e.preventDefault();
//                if(!this.validate()){
//                    return;
//                }
//                Helper.setFormInModel('#loginForm',this.model);
////                this.atributoCollection.add(novoAtributo);
//                var options = {emulateJSON: true, type: 'POST'};
//                var contexto = this;
//                options.success = function(ret) {
//                   if(ret.codRetorno == ProtocoloSistema.OPERACAO_REALIZADA_COM_SUCESSO){
//                       contexto.salvarSessao(
//                            contexto.model.get('email'), 
//                            contexto.model.get('senha'));
//                   } else {
//                        bootbox.alert(ret.mMensagem);   
//                   }
//                 };
//                Common.webservice("login", this.model, options) ;
//            },
//            
//            salvarSessao: function(usuario, senha){
//                
//                this.sessaoModel.set('autenticado', true);
//                this.sessaoModel.set('senha', senha);
//                //trigger novo usu�rio no App.js
//                this.sessaoModel.set('usuario', usuario);
//            }
//	});
//
//	return LoginView;
//});
