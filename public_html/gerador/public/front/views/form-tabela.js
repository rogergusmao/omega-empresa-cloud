/*global define*/
define([
	'jquery',
        'jqueryui',
	'underscore',
	'backbone',
        'models/tabela',
        'views/list-atributos',
        'text!templates/form-tabela.html',
        'common'
], function (
        $, 
        jqueryui, 
        _, 
        Backbone, 
        Tabela, 
        ListAtributosView, 
        formTabelaTemplate, 
        Common) {
	'use strict';

	var FormTabelaView = Backbone.View.extend({

            tagName: 'div',

            // The DOM events specific to an item.
            events:{
               'click #adicionar-tabela': 'adicionarTabela'
            },
            
            // Compile our stats template
            template: _.template(formTabelaTemplate),

            initialize: function () {

                if(this.model == null){
                    this.model = new Tabela();
                    this.action = "insert";
                } else {
                    this.action = "update";
                }
                 
            },

            // Re-render the titles of the todo item.
            render: function () {
                var json = this.model.toJSON();
                var html = this.template(json);
                this.$el.html(html);
                var view = new ListAtributosView();
                this.$atributosList = this.$(".tabela-list-atributos");
                this.$atributosList.append(view.render().el);

//                    if(Atributos.length){
//                        
//                    } else {
//                        
//                    }
                return this;
            },

            validate: function(){
                
                var validator  = $("#formTabela").validate({
                            rules: {
                                
                              field: {
                                required: true,
                                number: true
                              }
                            }
                          });
                validator.form();
                return $("#formTabela").valid();
           },
               
            showErrors: function (note, errors) {
              this.$el.find('.error').removeClass('error');
              this.$el.find('.alert').html(_.values(errors).join('<br>')).show();
              // highlight the fields with errors
              _.each(_.keys(errors), _.bind(function (key) {
                this.$el.find('*[name=' + key + ']').parent().addClass('error');
              }, this));
            },
               
               
             adicionarTabela: function(e){
                e.preventDefault();
                if(!this.validate()){
                    return;
                }
                Common.setFormInModel('#formTabela',this.model);
//                this.atributoCollection.add(novoAtributo);
                this.model.save();
//                this.clear();
             },
             
             close: function(){
                 
             }
		
	});

	return FormTabelaView;
});
