/*global require*/
'use strict';

// Require.js allows us to configure shortcut alias
require.config({
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		underscore: {
                    exports: '_'
		},
		backbone: {
                    deps: [
                        'underscore',
                        'jquery'
                    ],
                    exports: 'Backbone'
		},
		backboneLocalstorage: {
                    deps: ['backbone'],
                    exports: 'Store'
		},
                
                jquerymigrate: ['jquery'],
                jquerymobilecustom: ['jquery'],
                jqueryui: ['jquery'],
                jqueryuitouchpunch: ['jquery'],
                jquerycookie: ['jquery'],
                jqueryvalidator: ['jquery'],
                additionalmethods: ['jqueryvalidator'],
                helper: ['jquery'],
                bootstrap: [
                    'jquery', 
                    'jquerymigrate', 
                    'jquerymobilecustom',
                    'jqueryui',
                    'jqueryuitouchpunch',
                    //'jquerycookie'
                ],
                bootbox: {
                    deps: ["jquery","bootstrap"],
                    exports: 'bootbox'
		},
               
	},
	paths: {
		jquery: '../node_modules/jquery/jquery.min',
                jquerymobilecustom: '../node_modules/jquery/jquery.mobile.custom.min',
                jquerymigrate: '../node_modules/jquery/jquery-migrate.min',
                jqueryui: '../node_modules/jquery/jquery-ui.min',
                jqueryuitouchpunch: '../node_modules/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min',
                jquerycookie: '../node_modules/plugins/jquery.cookie.js/jquery.cookie',
                bootstrap: '../node_modules/bootstrap/bootstrap',
                bootbox: '../node_modules/plugins/bootbox/bootbox.min',
		underscore: '../node_modules/underscore/underscore',
		backbone: '../node_modules/backbone/backbone',
		backboneLocalstorage: '../node_modules/backbone.localStorage/backbone.localStorage',
		text: '../node_modules/requirejs-text/text',
                jqueryvalidator: '../node_modules/plugins/validate/jquery.validate.min',
                additionalmethods: '../node_modules/plugins/validate/additional-methods',
                
	}
});

require([
        "jquery",
        'underscore',
        'jquerymigrate',
        'jquerymobilecustom',
        'jqueryui',
        'jqueryuitouchpunch',
        'jqueryvalidator',
        'additionalmethods',
        'bootstrap',
	'backbone',
        'models/session',
        'common',
	'views/app',
	'routers/web-router',
        'bootbox',
        "jquerycookie"
], function ($, _, jQueryMigrate, 
jQueryMobileCustom, jQueryUI,
jQueryUITouchPunch, jQueryValidator,
additionalMethods, Bootstrap,
Backbone, Session, Common, AppView, 
WebRouter, bootbox, Cookie) {
    
      // Just use GET and POST to support all browsers
    Backbone.emulateHTTP = true;

    Common.router = new WebRouter();

    // Create a new session model and scope it to the app global
    // This will be a singleton, which other modules can access
    Common.session = new Session({});

    // Check the auth status upon initialization,
    // before rendering anything or matching routes
    Common.session.checkAuth({

    
        // Start the backbone routing once we have captured a user's auth status
        complete: function(){

            // HTML5 pushState for URLs without hashbangs
            var hasPushstate = !!(window.history && history.pushState);
            if(hasPushstate) Backbone.history.start({ pushState: true, root: '/' });
            else Backbone.history.start();

        }
    });


    // All navigation that is relative should be passed through the navigate
    // method, to be processed by the router. If the link has a `data-bypass`
    // attribute, bypass the delegation completely.
    $('#content-app').on("click", 'a:not([data-bypass]):not([reload-page])', function(evt) {
        evt.preventDefault();
        var href = $(this).attr("href");
        Common.router.navigate(href, { trigger : true, replace : false });

    });
    
    $('#content-app').on("click", "a[reload-page]", function(evt) {
        evt.preventDefault();
        var href = $(this).attr("href");
//        location.href = Common.URL;
        if(! _.isUndefined(href) ){
            if(href.indexOf("http") >= 0)
                location.href = href;    
            else
                location.href = Common.URL + "/"+ href;
        }
    });
    
    
    
//	/*jshint nonew:false*/
//	// Initialize routing and start Backbone.history()
//	new Workspace();
//	Backbone.history.start();
//
//	// Initialize the application view
//	new AppView();
});
