/*global define*/
'use strict';

define([], function () {
    var ProtocoloSistema =  {
        inicializa : function(){
            this.FALHA_DURANTE_CHAMADA_CURL = 0;
            this.OPERACAO_REALIZADA_COM_SUCESSO = -1;
            this.ERRO_COM_SERVIDOR = 1;
            this.ERRO_VERSAO_DE_SISTEMA_ANTIGA = 2;
            this.EMAIL_DO_USUARIO_INEXISTENTE = 4;
            this.SENHA_DO_USUARIO_INCORRETA = 5;
            this.USUARIO_NAO_PERTENCE_A_CORPORACAO = 6;
            this.ERRO_SEM_SER_EXCECAO = 7;
            this.USUARIO_INATIVO_NA_CORPORACAO = 8;
            this.SISTEMA_EM_MANUTENCAO = 9;
            this.MOBILE_DESCONECTADO  = 10;
            this.RESULTADO_VAZIO = 11;
            this.ERRO_PARAMETRO_INVALIDO = 12;
            this.ARQUIVO_INEXISTENTE = 13;
            this.ARQUIVO_INVALIDO = 14;
            this.REINSTALAR_VERSAO = 15;
            this.ATUALIZACAO_DE_VERSAO_BLOQUEADA = 16;
            this.BLOQUEADA = 17;
            this.NAO_BLOQUEADA = 18;
            this.ACESSO_NEGADO = 19;
            this.SIM = 20;
            this.NAO = 21;
        }
        
    };
    ProtocoloSistema.inicializa();
    return ProtocoloSistema;
});

