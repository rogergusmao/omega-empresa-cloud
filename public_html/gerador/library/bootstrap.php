<?php 
// Ensure we have session
if(session_id() === ""){
    session_start();
}

$path = ROOT . DS . 'config' . DS . 'config.php';

// include the config settings
require_once ($path);

// Autoload any classes that are required
spl_autoload_register(function($className) {

    //$className = strtolower($className);
    $rootPath = ROOT . DS;
    $valid = false;
   
    // check root directory of library
    $valid = file_exists($classFile = $rootPath . 'library' . DS . $className . '.class.php');
    
    // if we cannot find any, then find library/core directory
    if(!$valid){
        $valid = file_exists($classFile = $rootPath . 'library' . DS . 'core' . DS . $className . '.class.php');   	
    }
    // if we cannot find any, then find library/mvc directory
    if(!$valid){
        $valid = file_exists($classFile = $rootPath . 'library' . DS . 'mvc' . DS . $className . '.class.php');
    }     
    // if we cannot find any, then find application/controllers directory
    if(!$valid){
        $valid = file_exists($classFile = $rootPath . 'application' . DS . 'controllers' . DS . $className . '.php');
//        TODO importante local para verificar se o router est� chegando at� o arquivo.
        //echo $rootPath . 'application' . DS . 'controllers' . DS . $className . '.php<br/>';

    } 
    // if we cannot find any, then find application/models directory
    if(!$valid){
        $valid = file_exists($classFile = $rootPath . 'application' . DS . 'models' . DS . $className . '.php');
    }  
  
    // if we have valid fild, then include it
    if($valid){
       require_once($classFile); 
    }else{
        /* Error Generation Code Here */
    }    
});

include_once '../../recursos/php/constants.php';

include_once '../../recursos/php/database_config.php';

include_once '../../gerador/application/imports/header.php';

include_once '../../recursos/php/funcoes.php';
include_once '../../gerador/application/imports/instancias.php';

include_once '../../adm/imports/lingua.php';

include_once '../../gerador/application/imports/sessao.php';

include_once '../../recursos/classes/class/Javascript.php';

include_once '../../recursos/classes/class/Ajax.php';

include_once '../../recursos/classes/class/AES.php';
include_once '../../recursos/classes/class/Crypt.php';

include_once '../../recursos/classes/class/Generic_Argument.php';

include_once '../../recursos/classes/EXTDAO/EXTDAO_Acesso_cliente.php';




// remove the magic quotes
MyHelpers::removeMagicQuotes();

// unregister globals
MyHelpers::unregisterGlobals();

// register route
$router = new Router($_route);

// finaly we dispatch the output
$router->dispatch();

// close session to speed up the concurrent connections
// http://php.net/manual/en/function.session-write-close.php
session_write_close();