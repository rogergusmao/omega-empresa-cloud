<?php
    class Protocolo_conecta_telefone extends Interface_protocolo{
        
        public $mobile;
        public $mobileConectado;
        public $mobileIdentificador;
        public $usuario;
        public $corporacao;
        
        public static function constroi($pVetorValor){
            if(is_array($pVetorValor)){
                $vObj = new Protocolo_conecta_telefone();
                $vObj->mobile = $pVetorValor[0];   
                $vObj->mobileConectado = $pVetorValor[1];
                $vObj->mobileIdentificador = $pVetorValor[2];
                $vObj->usuario = $pVetorValor[3];
                $vObj->corporacao = $pVetorValor[4];
                
                return $vObj;
            }
            return null;
        }

    public function factory($objJson) {
        
    }
        
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
