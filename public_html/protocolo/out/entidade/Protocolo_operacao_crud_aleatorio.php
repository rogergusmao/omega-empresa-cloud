<?php
    class Protocolo_operacao_crud_aleatorio extends Interface_protocolo_entidade{
        public $id;
        
        public $operacao_sistema_mobile_id_INT;
        public $total_insercao_INT;
        
        public $porcentagem_remocao_FLOAT;
        public $porcentagem_edicao_FLOAT;
        public $sincronizar_BOOLEAN;
        
        public function __construct($idOSMFilho){
            
            $this->id = $idOSMFilho;
            $obj = new EXTDAO_Operacao_crud_aleatorio();            
            $obj->select($idOSMFilho);
            
            $this->operacao_sistema_mobile_id_INT = $obj->getOperacao_sistema_mobile_id_INT();
            $this->total_insercao_INT = $obj->getTotal_insercao_INT();

            $this->porcentagem_remocao_FLOAT = $obj->getPorcentagem_remocao_FLOAT();
            $this->porcentagem_edicao_FLOAT = $obj->getPorcentagem_edicao_FLOAT();
            $this->sincronizar_BOOLEAN = $obj->getSincronizar_BOOLEAN();

        }

    public function factory($objJson) {
        
    }
        
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
