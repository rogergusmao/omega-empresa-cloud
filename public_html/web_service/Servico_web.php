<?php
    class Servico_web{
        
        public function __construct() {

        }
        
        public function factory() {

            return new Servico_web();
        }
        
        
        
        public static function consultaUltimoProjetosVersaoDoSistema(){
            
            $idSistema = Helper::POST("sistema");
            $vObjSPV = new BO_Sistema_projetos_versao();
            $vRet = $vObjSPV->consultaUltimaVersaoDoSistema(
                    $idSistema);
            
            return $vRet;
        }
        
        public static function getArvoreBancoOmegaEmpresa(){
            set_time_limit(0);
            $idSistema = Helper::POSTGET("id_sistema");
            $msg = BO_Sistema::getArvoreBancoOmegaEmpresa($idSistema);
            return $msg;
        }
    }
