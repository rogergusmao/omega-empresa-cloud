<?php

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of HelperMensagem
     *
     * @author rogerfsg
     */
    class HelperMensagem
    {
        //put your code here
        const GERAL_ID_VAZIO = "Identificador vazio.";
        const ENTIDADE_MOBILE_PROJETOS_VERSAO = "Vers�o de projeto do telefone";
        const ENTIDADE_MOBILE = "Telefone";
        const ENTIDADE_MOBILE_CONECTADO = "Telefone conectado";
        const MOBILE_IMEI_VAZIO = "O imei n�o foi passado no par�metro de entrada";
        const MOBILE_JA_ESTA_DESCONECTADO = "O telefone j� est� desconectado.";
        const SUPORTE_INATIVO = "Suporte inativo.";
        const PROTOCOLO_SISTEMA_MOBILE_DESCONECTADO = "O telefone esta desconectado.";
        const SISTEMA_TIPO_DOWNLOAD_ARQUIVO_ARQUIVO_INVALIDO = "Arquivo inv�lido.";
        const SISTEMA_TIPO_DOWNLOAD_ARQUIVO_LOG_FILE_OBRIGATORIO = "O par�metro log_file deve estar setado.";
        const SISTEMA_TIPO_DOWNLOAD_FALHA_UPLOAD_ARQUIVO = "Falha durante o upload do arquivo.";
        const SISTEMA_TIPO_DOWNLOAD_ARQUIVO_ARQUIVO_NAO_DELETADO = "O arquivo n�o foi deletado.";

        public static function GERAL_FALHA_CADASTRO($nomeEntidade, $vetorParam)
        {
            $ret = "Falha no cadastro da entidade: $nomeEntidade. Par�metros: ";
            $strParam = "";
            if ($vetorParam != null)
            {
                foreach ($vetorParam as $param)
                {
                    if (strlen($strParam) == 0)
                    {
                        $strParam .= $param;
                    }
                    else
                    {
                        $strParam .= ", {$param}";
                    }
                }
            }

            return $ret . $strParam;
        }

        public static function GERAL_ARQUIVO_INEXISTENTE($pathArquivo)
        {
            return "Arquivo inexistente: $pathArquivo.";
        }

        public static function GERAL_PARAMETRO_INVALIDO($nomeWebService, $vetorParam)
        {
            $ret = "Par�metro inv�lido na chamada do webservice: $nomeWebService. Par�metros: ";
            $strParam = "";
            if ($vetorParam != null)
            {
                foreach ($vetorParam as $param)
                {
                    if (strlen($strParam) == 0)
                    {
                        $strParam .= $param;
                    }
                    else
                    {
                        $strParam .= ", {$param}";
                    }
                }
            }

            return $ret . $strParam;
        }

        public static function GERAL_FALHA_REMOCAO($nomeEntidade, $vetorParam)
        {
            $ret = "Falha na edi��o da entidade: $nomeEntidade. Par�metros: ";
            $strParam = "";
            if ($vetorParam != null)
            {
                foreach ($vetorParam as $param)
                {
                    if (strlen($strParam) == 0)
                    {
                        $strParam .= $param;
                    }
                    else
                    {
                        $strParam .= ", {$param}";
                    }
                }
            }

            return $ret . $strParam;
        }

        public static function GERAL_FALHA_CONSULTA($nomeEntidade, $vetorParam)
        {
            $ret = "Retorno vazio na consulta da entidade: $nomeEntidade. Par�metros: ";
            $strParam = "";
            if ($vetorParam != null)
            {
                foreach ($vetorParam as $param)
                {
                    if (strlen($strParam) == 0)
                    {
                        $strParam .= $param;
                    }
                    else
                    {
                        $strParam .= ", {$param}";
                    }
                }
            }

            return $ret . $strParam;
        }
    }

?>
