<?php
    class Servico_mobile{
        
         
        
        public static function logErroProdutoMobile() {

            $strObjJson = Helper::POSTGET(Param_Get::ID_OBJ_JSON);
//---------------------------------------------------
//  Teste
            //$strObjJson =   "{\"mCodRetorno\":0,\"mVetorObj\":[{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id . Mensagem Especifica: no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 13:56:32\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id . Mensagem Especifica: no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 13:56:32\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id . Mensagem Especifica: no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:04:48\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id . Mensagem Especifica: no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:05:44\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id . Mensagem Especifica: no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:06:25\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id . Mensagem Especifica: no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:06:25\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id . Mensagem Especifica: no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:11:02\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id . Mensagem Especifica: no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:11:02\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id . Mensagem Especifica: no such table: usuario: , while compiling: SELECT * FROM usuario ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:12:07\"},{\"sistema_projetos_versao_sistema_produto_mobile_id_INT\":\"1\",\"funcao\":\"getListTable\",\"classe\":\"Table\",\"sistema_tipo_log_erro_id_INT\":\"7\",\"descricao\":\"no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id . Mensagem Especifica: no such table: corporacao: , while compiling: SELECT * FROM corporacao ORDER BY id \",\"data_ocorrida_DATETIME\":\"2011-01-05 14:12:07\"}]}";
//---------------------------------------------------
            $mensagemRet = BO_Sistema_log_erro_produto::logErroProdutoMobile($strObjJson);
            return $mensagemRet;    
            
        }
        
        public function __construct() {
            
        }

        public static function factory() {

            return new Servico_mobile();
        }
        
        public static function isServidorOnline(){
            echo "TRUE";
        }
        
        public static function downloadApkAndroid() {
            //$id = Helper::POSTGET("sistema_projetos_versao_sistema_produto_mobile");
            $id = Helper::POSTGET("sistema_projetos_versao_sistema_produto_mobile");
            
            $mensagemRet = BO_Sistema_projetos_versao_sistema_produto_mobile::donwloadApkAndroid($id);
            
            return $mensagemRet;
        }
    
        public static function transfereArquivoMobileParaWeb() {
            $pIdNaTabela = Helper::POSTGET("id_na_tabela");
            $pNomeTabela = Helper::POSTGET("nome_tabela");
            $pIdSistemaTipoDownloadArquivo = Helper::POSTGET("id_sistema_tipo_download_arquivo");
            
            if(strlen($pIdNaTabela) 
                    && strlen($pNomeTabela) 
                    && strlen($pIdSistemaTipoDownloadArquivo) ){
                $mensagemRet = BO_Sistema_tipo_download_arquivo::tranfereArquivoMobileParaWeb($pIdNaTabela, $pNomeTabela, $pIdSistemaTipoDownloadArquivo);
                return $mensagemRet;
            }
            else{
                $mensagemRet =  new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "transfereArquivoZipadoMobileParaWeb", 
                                array(
                                    $pIdNaTabela ,
                                    $pNomeTabela ,
                                    $pIdSistemaTipoDownloadArquivo 
                                )
                            )
                        );
                return $mensagemRet;
            }
        }
        public static function falhaPrintScreenMobileParaWeb(){
            $motivoErro = Helper::POSTGET("motivo_erro");
            $idOperacaoSistemaMobile  = Helper::POSTGET("id_operacao_sistema_mobile");
            $idMobileIdentificador  = Helper::POSTGET("id_mobile_identificador");
            $seq = Helper::POSTGET("seq");
            $programaAberto = Helper::POSTGET("programa_aberto");
            $tecladoAtivo = Helper::POSTGET("teclado_ativo");
            
            if(strlen($motivoErro)){
                $ret = BO_Monitora_tela_mobile_para_web::falhaPrintScreen(
                    $idOperacaoSistemaMobile, 
                    $idMobileIdentificador, 
                    $seq,
                    $programaAberto,
                    $tecladoAtivo,
                    $motivoErro);
                echo json_encode($ret);
            } else{
                 $mensagemRet =  new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "falhaPrintScreenMobileParaWeb", 
                                array(
                                    $idOperacaoSistemaMobile, 
                                    $idMobileIdentificador, 
                                    $seq,
                                    $programaAberto,
                                    $tecladoAtivo,
                                    $motivoErro
                                )
                            )
                        );
                return $mensagemRet;
            }
        }
        
        public static function transferePrintScreenMobileParaWeb() {
            $pIdNaTabela = Helper::POSTGET("id_na_tabela");
            $pNomeTabela = Helper::POSTGET("nome_tabela");
            $pIdSistemaTipoDownloadArquivo = Helper::POSTGET("id_sistema_tipo_download_arquivo");
            $idOperacaoSistemaMobile  = Helper::POSTGET("id_operacao_sistema_mobile");
            $idMobileIdentificador  = Helper::POSTGET("id_mobile_identificador");
            $seq = Helper::POSTGET("seq");
            $programaAberto = Helper::POSTGET("programa_aberto");
            $tecladoAtivo = Helper::POSTGET("teclado_ativo");
            if(strlen($pIdNaTabela) 
                    && strlen($pNomeTabela) 
                    && strlen($pIdSistemaTipoDownloadArquivo) ){
                $mensagemRet = BO_Monitora_tela_mobile_para_web::printScreenMobileParaWeb(
                        $idOperacaoSistemaMobile, 
            $idMobileIdentificador, 
            $seq,
            $programaAberto,
            $tecladoAtivo,
            $pIdNaTabela, 
            $pNomeTabela, 
            $pIdSistemaTipoDownloadArquivo);
                return $mensagemRet;
            }
            else{
                $mensagemRet =  new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "transferePrintScreenMobileParaWeb", 
                                array(
                                    $pIdNaTabela ,
                                    $pNomeTabela ,
                                    $pIdSistemaTipoDownloadArquivo 
                                )
                            )
                        );
                return $mensagemRet;
            }
        }
        
        
        public static function transfereArquivoWebParaMobile() {
            
            $pIdNaTabela = Helper::POSTGET("id_na_tabela");
            $pNomeTabela = Helper::POSTGET("nome_tabela");
            $pIdSistemaTipoDownloadArquivo = Helper::POSTGET("id_sistema_tipo_download_arquivo");
            $pNomeArquivo = Helper::POSTGET("nome_arquivo");
            if(strlen($pIdNaTabela) 
                    && strlen($pNomeTabela) 
                    && strlen($pIdSistemaTipoDownloadArquivo) 
                    && strlen($pNomeArquivo)){
                
                $mensagemRet = BO_Sistema_tipo_download_arquivo::transfereArquivoWebParaMobile($pIdNaTabela, $pNomeTabela, $pIdSistemaTipoDownloadArquivo, $pNomeArquivo);
                if ($mensagemRet->codRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                    return $mensagemRet;
            }
            else{
                $mensagemRet= new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "transfereArquivoWebParaMobile", 
                                array(
                                    $pIdNaTabela ,
                                    $pNomeTabela ,
                                    $pIdSistemaTipoDownloadArquivo ,
                                    $pNomeArquivo
                                )
                            )
                        );
                return $mensagemRet;
            }
        }
        
        public static function transfereScriptBancoAndroidParaWeb(){
            $pOperacaoSistemaMobile = Helper::POSTGET("operacao_sistema_mobile");

            $mensagemRet = null;
            if(strlen($pOperacaoSistemaMobile)){
                $vObj = new BO_Operacao_download_banco_mobile(
                        $pOperacaoSistemaMobile, 
                        "operacao_sistema_mobile", 
                        GerenciadorArquivo::ID_SCRIPT);
                $mensagemRet = $vObj->executa();
                return $mensagemRet;
            }
            else{
                $mensagemRet = new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "transfereScriptBancoAndroidParaWeb", 
                                array(
                                    $pOperacaoSistemaMobile
                                )
                            )
                        );
            
                return $mensagemRet;
            }
        }
        
        public static function transfereArquivoZipadoMobileParaWeb() {
            $pIdNaTabela = Helper::POSTGET("id_na_tabela");
            $pNomeTabela = Helper::POSTGET("nome_tabela");
            $pIdSistemaTipoDownloadArquivo = Helper::POSTGET("id_sistema_tipo_download_arquivo");
            
            if(strlen($pIdNaTabela) 
                    && strlen($pNomeTabela) 
                    && strlen($pIdSistemaTipoDownloadArquivo) ){
                $mensagemRet = BO_Sistema_tipo_download_arquivo::transfereArquivoZipadoMobileParaWeb($pIdNaTabela, $pNomeTabela, $pIdSistemaTipoDownloadArquivo);
                return $mensagemRet;
            }
            else{
                $mensagemRet =  new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "transfereArquivoZipadoMobileParaWeb", 
                                array(
                                    $pIdNaTabela ,
                                    $pNomeTabela ,
                                    $pIdSistemaTipoDownloadArquivo 
                                )
                            )
                        );
                return $mensagemRet;
            }
        }
        
        
        
        //conecta o celular ao servidor
        public static function conectaTelefone(){
            try {
                $idUsuario = Helper::POSTGET("usuario");
                $idCorporacao = Helper::POSTGET("corporacao");
                $nomeCorporacao = Helper::POSTGET("nome_corporacao");
                $identificador = Helper::POSTGET("identificador");
                $imei = Helper::POSTGET("imei");
                $idSPVSPMobile = Helper::POSTGET("sistema_projetos_versao_sistema_produto_mobile");

                $marca = Helper::POSTGET("marca");
                $modelo = Helper::POSTGET("modelo");
                $cpu = Helper::POSTGET("cpu");

                $vObjMC = new BO_Controlador_mobile();
                $mensagemRet = $vObjMC->conectaTelefone(
                        $imei, 
                        $identificador, 
                        $idSPVSPMobile,
                        $idUsuario,
                        $idCorporacao,
                        $marca,
                        $modelo,
                        $cpu,
                        $nomeCorporacao);
                return $mensagemRet;
            } catch (Exception $exc) {
                $msg = "Stack: ".$exc->getTraceAsString().". Msg: ".$exc->getMessage();
                $mensagemRet = new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, $msg);
                return $mensagemRet;
            }

           
        }
        
        //conecta o celular ao servidor
        public static function modoSuporte(){
            
            $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);
                 
            $vObjMC = new BO_Mobile_identificador();
            
            $mensagemRet = $vObjMC->modoSuporte($idMI);
            return $mensagemRet;
        }
        
        
        
        public static function registraUsuarioMensagem(){
            $json = Helper::POSTGET("objeto_usuario_mensagem_json");
            json_decode($json, true);
        }
        
        public static function transfereProgramaMobileWebParaAndroid(){
            $id = Helper::POSTGET("sistema_produto_mobile");
            $vObjSPV = new BO_Sistema_projetos_versao_sistema_produto_mobile();
            $mensagemRet = $vObjSPV->donwloadApkAndroid($id);
            //So imprime o retorno se algo de errado tive acontcido
            return $mensagemRet;
        }
        
        //sistema_projetos_versao_sistema_produto_mobile
        public static function consultaListaIdSistemaProjetosVersaoSistemaProdutoMobile(){
            
            $idSPM = Helper::POSTGET("sistema_produto_mobile");
            $idSPVSPM = Helper::POSTGET("sistema_projetos_versao_sistema_produto_mobile");
            $idMI = Helper::POSTGET("mobile_identificador");
            if(strlen($idMI)){
                $objMI = new BO_Mobile_identificador();
                $mensagemRet = $objMI->verificaReinstalacaoDeVersao($idMI);
                if($mensagemRet->codRetorno != PROTOCOLO_SISTEMA::RESULTADO_VAZIO){
                    return $mensagemRet;
                    return;
                }
            }
            
            if(!strlen($idSPVSPM)) $idSPVSPM = -1;
            $vObjSPV = new BO_Sistema_projetos_versao_sistema_produto_mobile();
            $mensagemRet = $vObjSPV->getListaVersao($idSPM, $idSPVSPM);
            return $mensagemRet;
        }
        
        public static function consultaUltimoIdSistemaProjetosVersaoSistemaProdutoMobile(){
            
            $idSPM = Helper::POSTGET("sistema_produto_mobile");
            $idSPVSPM = Helper::POSTGET("sistema_projetos_versao_sistema_produto_mobile");
            if(!strlen($idSPVSPM)) $idSPVSPM = -1;
            $vObjSPV = new BO_Sistema_projetos_versao_sistema_produto_mobile();
            $mensagemRet = $vObjSPV->getUltimaVersao($idSPM, $idSPVSPM);
            return $mensagemRet;
        }
        
        public static function registraLogErroMobile(){
            
            $json = Helper::POSTGET("erro_log_erro_mobile_json");
            json_decode($json, true);
            //Basicamente chegara a lista de tuplas a serem adicionadas
        }
        
        
         //sistema_projetos_versao_sistema_produto_mobile
        public static function consultaOperacaoSistemaMobile(){
            
            $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);
            $idMC = Helper::POSTGET(Param_Get::ID_MOBILE_CONECTADO);
            $obj = new BO_Operacao_sistema_mobile(null);
            if(strlen($idMC)){
                $vObjMC = new BO_Mobile_conectado();
                $vObjMC->atualizaConexao($idMC);
            }
            
            $mensagemRet = $obj->consultaOperacaoAguardando($idMI);
            return $mensagemRet;
        }
        
         public static function atualizaEstadoOperacaoSistemaMobile(){
            
            $idMI = Helper::POSTGET(Param_Get::ID_MOBILE_IDENTIFICADOR);
            $idMC = Helper::POSTGET(Param_Get::ID_MOBILE_CONECTADO);
            $idEOSM = Helper::POSTGET(Param_Get::ID_ESTADO_OPERACAO_SISTEMA_MOBILE);
            $idOSM =  Helper::POSTGET(Param_Get::ID_OPERACAO_SISTEMA_MOBILE);
            $obj = new BO_Operacao_sistema_mobile();
            if(strlen($idMC)){
                $vObjMC = new BO_Mobile_conectado();
                $vObjMC->atualizaConexao($idMC);
            }
            
            $mensagemRet = $obj->atualizaEstadoOperacaoSistemaMobile($idOSM, $idEOSM);
            
            return $mensagemRet;
        }
        
        
        
        public static function registraLogErroAtualizacaoBancoMobile(){
            
            $json = Helper::POSTGET("erro_log_erro_atualizacao_banco_mobile_json");
            json_decode($json, true);
            //Basicamente chegara a lista de tuplas a serem adicionadas
        }
        
        public static function atualizaConexao(){
            $idMobileIdentificador = Helper::POSTGET("mobile_identificador");
                    
            $vObjMC = new BO_Mobile_conectado();
            $mensagemRet = $vObjMC->atualizaConexao(
                    $idMobileIdentificador);
            return $mensagemRet;
        }
        
        //desconecta o celular do servidor
        public static function desconectaTelefone(){
            
            $idMobileConectado = Helper::POSTGET("mobile_conectado");
                    
            $vObjMC = new BO_Mobile_conectado();
            $mensagemRet = $vObjMC->desconectaTelefone(
                    $idMobileConectado);
            return $mensagemRet;
        }
        
        public static function consultaMonitoraTelaWebParaMobile(){
            try{
                $idOSM = Helper::POSTGET(Param_Get::ID_OPERACAO_SISTEMA_MOBILE);
                $mensagemRet = null;
                if(strlen($idOSM)){
                    $mensagemRet = BO_Monitora_tela_web_para_mobile::consultaListaProtocoloDaOSM($idOSM);
                } else {
                    $mensagemRet =  new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, 
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "consultaMonitoraTelaWebParaMobile", 
                                array(
                                    $idOSM
                                )
                            )
                        );
                }

                
                return $mensagemRet;
            } catch (Exception $exc) {
                $msg = "Stack: ".$exc->getTraceAsString().". Msg: ".$exc->getMessage();
                $mensagemRet = new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, $msg);
                return $mensagemRet;
            }
        }
        
        
        public static function criaOperacaoSistemaMobileDownloadBancoMobileAvulso(){
            $idMI = Helper::POSTGET("id_mobile_identificador");
            $nomeArquivo = Helper::POSTGET("nome_arquivo");
            $msg = EXTDAO_Operacao_download_banco_mobile::criaAvulsoDownloadBancoMobile($idMI, $nomeArquivo);
            return $msg;
        }
        
        
        public static function criaOperacaoSistemaMobileDownloadBancoSQLiteMobileAvulso(){
            $idMI = Helper::POSTGET("id_mobile_identificador");
            $nomeArquivo = Helper::POSTGET("nome_arquivo");
            $msg = EXTDAO_Operacao_download_banco_mobile::criaAvulsoDownloadBancoSQLiteMobile($idMI, $nomeArquivo);
            return $msg;
        }

        public static function logArquivoErroProdutoMobile(){
            $idMI = Helper::POSTGET("id_mobile_identificador");
            if(!is_numeric($idMI))
                $idMI = "vazio";
            return BO_Sistema_log_erro_produto::logArquivoErroProdutoMobile($idMI);


        }

        
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
