/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : omega_empresa_web_hmg

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-02-20 06:24:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acesso`
-- ----------------------------
DROP TABLE IF EXISTS `acesso`;
CREATE TABLE `acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `data_login_SEC` int(10) DEFAULT NULL,
  `data_login_OFFSEC` int(6) DEFAULT NULL,
  `data_logout_SEC` int(10) DEFAULT NULL,
  `data_logout_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acesso_FK_257202148` (`usuario_id_INT`),
  CONSTRAINT `acesso_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of acesso
-- ----------------------------

-- ----------------------------
-- Table structure for `api`
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of api
-- ----------------------------

-- ----------------------------
-- Table structure for `api_id`
-- ----------------------------
DROP TABLE IF EXISTS `api_id`;
CREATE TABLE `api_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `chave` varchar(255) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL COMMENT 'email facebook, id conta pag seguro',
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `api_id_FK_814331055` (`pessoa_id_INT`),
  KEY `api_id_FK_701324463` (`usuario_id_INT`),
  KEY `api_id_FK_867980957` (`empresa_id_INT`),
  KEY `api_id_FK_830413819` (`corporacao_id_INT`),
  CONSTRAINT `api_id_FK_701324463` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `api_id_FK_814331055` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `api_id_FK_830413819` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `api_id_FK_867980957` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of api_id
-- ----------------------------

-- ----------------------------
-- Table structure for `area_menu`
-- ----------------------------
DROP TABLE IF EXISTS `area_menu`;
CREATE TABLE `area_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(252) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of area_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade`
-- ----------------------------
DROP TABLE IF EXISTS `atividade`;
CREATE TABLE `atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `atividade_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_horas_INT` int(3) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_FK_389770508` (`empresa_id_INT`),
  KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`),
  KEY `atividade_FK_219207763` (`atividade_unidade_medida_id_INT`),
  KEY `atividade_FK_411010742` (`relatorio_id_INT`),
  CONSTRAINT `atividade_FK_219207763` FOREIGN KEY (`atividade_unidade_medida_id_INT`) REFERENCES `atividade_unidade_medida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_FK_411010742` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `atividade_tipo`;
CREATE TABLE `atividade_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `nome_normalizado` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_098787` (`nome`,`corporacao_id_INT`) USING BTREE,
  KEY `empresa_servico_tipo_FK_604431152` (`corporacao_id_INT`),
  CONSTRAINT `atividade_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade_tipos`
-- ----------------------------
DROP TABLE IF EXISTS `atividade_tipos`;
CREATE TABLE `atividade_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atividade_tipo_id_INT` int(11) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `atividade_tipos_FK_423400879` (`atividade_tipo_id_INT`),
  KEY `atividade_tipos_FK_996185303` (`atividade_id_INT`),
  KEY `atividade_tipos_FK_789062500` (`corporacao_id_INT`),
  CONSTRAINT `atividade_tipos_FK_423400879` FOREIGN KEY (`atividade_tipo_id_INT`) REFERENCES `atividade_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_tipos_FK_789062500` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_tipos_FK_996185303` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade_tipos
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `atividade_unidade_medida`;
CREATE TABLE `atividade_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `atividade_unidade_medida_FK_833953858` (`corporacao_id_INT`),
  CONSTRAINT `atividade_unidade_medida_FK_833953858` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `bairro`
-- ----------------------------
DROP TABLE IF EXISTS `bairro`;
CREATE TABLE `bairro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome234234` (`nome_normalizado`,`cidade_id_INT`,`corporacao_id_INT`) USING BTREE,
  KEY `b_cidade_FK` (`cidade_id_INT`),
  KEY `bairro_FK_545776367` (`corporacao_id_INT`),
  CONSTRAINT `bairro_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `bairro_ibfk_2` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bairro
-- ----------------------------

-- ----------------------------
-- Table structure for `categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `categoria_permissao`;
CREATE TABLE `categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_Aasd` (`nome_normalizado`,`corporacao_id_INT`) USING BTREE,
  KEY `categoria_permissao_FK_780273438` (`corporacao_id_INT`),
  CONSTRAINT `categoria_permissao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categoria_permissao
-- ----------------------------
INSERT INTO `categoria_permissao` VALUES ('1', 'USUÁRIO PADRÃO', 'USUARIO PADRAO', '1');
INSERT INTO `categoria_permissao` VALUES ('2', 'MOTORISTA', 'MOTORISTA', '1');
INSERT INTO `categoria_permissao` VALUES ('3', 'SUPERVISOR', 'SUPERVISOR', '1');
INSERT INTO `categoria_permissao` VALUES ('4', 'ANALISTA', 'ANALISTA', '1');

-- ----------------------------
-- Table structure for `cidade`
-- ----------------------------
DROP TABLE IF EXISTS `cidade`;
CREATE TABLE `cidade` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `uf_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_736` (`nome_normalizado`,`uf_id_INT`,`corporacao_id_INT`) USING BTREE,
  KEY `uf_id_INT` (`uf_id_INT`),
  KEY `cidade_FK_928863526` (`corporacao_id_INT`),
  CONSTRAINT `cidade_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `cidade_ibfk_2` FOREIGN KEY (`uf_id_INT`) REFERENCES `uf` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cidade
-- ----------------------------

-- ----------------------------
-- Table structure for `corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `corporacao`;
CREATE TABLE `corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `usuario_dropbox` varchar(255) DEFAULT NULL,
  `senha_dropbox` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporacao_nome_UNIQUE` (`nome_normalizado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of corporacao
-- ----------------------------
INSERT INTO `corporacao` VALUES ('1', 'FACTU', 'FACTU', null, null);
INSERT INTO `corporacao` VALUES ('3', 'TESTE', 'TESTE', null, null);
INSERT INTO `corporacao` VALUES ('4', 'TESTEEMPRESA', 'TESTEEMPRESA', null, null);
INSERT INTO `corporacao` VALUES ('5', 'TESTETTT', 'TESTETTT', null, null);

-- ----------------------------
-- Table structure for `despesa`
-- ----------------------------
DROP TABLE IF EXISTS `despesa`;
CREATE TABLE `despesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_FLOAT` double DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `despesa_cotidiano_id_INT` int(11) DEFAULT NULL,
  `valor_pagamento_FLOAT` double DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `pagamento_usuario_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `despesa_FK_685638428` (`empresa_id_INT`),
  KEY `despesa_FK_939453125` (`despesa_cotidiano_id_INT`),
  KEY `despesa_FK_777130127` (`cadastro_usuario_id_INT`),
  KEY `despesa_FK_818298340` (`pagamento_usuario_id_INT`),
  KEY `despesa_FK_352508545` (`corporacao_id_INT`),
  CONSTRAINT `despesa_FK_352508545` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_685638428` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_777130127` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_818298340` FOREIGN KEY (`pagamento_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_939453125` FOREIGN KEY (`despesa_cotidiano_id_INT`) REFERENCES `despesa_cotidiano` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of despesa
-- ----------------------------

-- ----------------------------
-- Table structure for `despesa_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `despesa_cotidiano`;
CREATE TABLE `despesa_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `despesa_da_empresa_id_INT` int(11) DEFAULT NULL,
  `despesa_do_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `id_tipo_despesa_cotidiano_INT` int(3) DEFAULT NULL,
  `parametro_INT` int(11) DEFAULT NULL,
  `parametro_OFFSEC` int(10) DEFAULT NULL,
  `parametro_SEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `despesa_cotidiano_FK_33874511` (`despesa_da_empresa_id_INT`),
  KEY `despesa_cotidiano_FK_475311279` (`despesa_do_usuario_id_INT`),
  KEY `despesa_cotidiano_FK_201293945` (`cadastro_usuario_id_INT`),
  KEY `despesa_cotidiano_FK_755279541` (`corporacao_id_INT`),
  CONSTRAINT `despesa_cotidiano_FK_201293945` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_cotidiano_FK_33874511` FOREIGN KEY (`despesa_da_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_cotidiano_FK_475311279` FOREIGN KEY (`despesa_do_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_cotidiano_FK_755279541` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of despesa_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa`
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `telefone1` varchar(30) DEFAULT NULL,
  `telefone2` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `operadora_id_INT` int(11) DEFAULT NULL,
  `celular_sms` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tipo_documento_id_INT` int(11) DEFAULT NULL,
  `numero_documento` varchar(30) DEFAULT NULL,
  `tipo_empresa_id_INT` int(11) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro_id_INT` int(11) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `ind_email_valido_BOOLEAN` int(1) DEFAULT NULL,
  `ind_celular_valido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`corporacao_id_INT`) USING BTREE,
  KEY `u_bairro_FK` (`bairro_id_INT`),
  KEY `u_cidade_FK` (`cidade_id_INT`),
  KEY `empresa_FK_222015381` (`corporacao_id_INT`),
  KEY `empresa_FK_261840820` (`tipo_documento_id_INT`),
  KEY `empresa_FK_354888916` (`tipo_empresa_id_INT`),
  KEY `empresa_FK_967346192` (`operadora_id_INT`),
  KEY `key_empr766968` (`id`) USING BTREE,
  KEY `empresa_FK_690795899` (`relatorio_id_INT`),
  CONSTRAINT `empresa_FK_690795899` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_2` FOREIGN KEY (`operadora_id_INT`) REFERENCES `operadora` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_3` FOREIGN KEY (`tipo_documento_id_INT`) REFERENCES `tipo_documento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_4` FOREIGN KEY (`tipo_empresa_id_INT`) REFERENCES `tipo_empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_5` FOREIGN KEY (`bairro_id_INT`) REFERENCES `bairro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_6` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_atividade`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_atividade`;
CREATE TABLE `empresa_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_horas_INT` int(3) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `preco_venda_FLOAT` double DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_FK_389770508` (`empresa_id_INT`),
  KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`),
  KEY `empresa_atividade_FK_124084472` (`atividade_id_INT`),
  CONSTRAINT `empresa_atividade_FK_124084472` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_atividade
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_atividade_compra`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_atividade_compra`;
CREATE TABLE `empresa_atividade_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_compra_FK_892700196` (`empresa_compra_id_INT`),
  KEY `empresa_servico_compra_FK_118560791` (`corporacao_id_INT`),
  KEY `empresa_atividade_compra_FK_668334961` (`atividade_id_INT`),
  KEY `empresa_atividade_compra_FK_537750244` (`empresa_id_INT`),
  CONSTRAINT `empresa_atividade_compra_FK_537750244` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_compra_FK_668334961` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_compra_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_compra_ibfk_3` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_atividade_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_atividade_venda`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_atividade_venda`;
CREATE TABLE `empresa_atividade_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_venda_FK_937438965` (`empresa_venda_id_INT`),
  KEY `empresa_servico_venda_FK_338836670` (`corporacao_id_INT`),
  KEY `empresa_atividade_venda_FK_500762939` (`atividade_id_INT`),
  KEY `empresa_atividade_venda_FK_194274902` (`empresa_id_INT`),
  CONSTRAINT `empresa_atividade_venda_FK_194274902` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_venda_FK_500762939` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_venda_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_venda_ibfk_3` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_atividade_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_compra`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_compra`;
CREATE TABLE `empresa_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `minha_empresa_id_INT` int(11) DEFAULT NULL,
  `outra_empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `fechamento_SEC` int(10) DEFAULT NULL,
  `fechamento_OFFSEC` int(6) DEFAULT NULL,
  `valor_pago_FLOAT` double DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_compra_FK_903930664` (`forma_pagamento_id_INT`),
  KEY `empresa_compra_FK_592681885` (`usuario_id_INT`),
  KEY `empresa_compra_FK_180664062` (`corporacao_id_INT`),
  KEY `minha_empresa_id_INT` (`minha_empresa_id_INT`),
  KEY `outra_empresa_id_INT` (`outra_empresa_id_INT`),
  KEY `empresa_compra_FK_584411621` (`relatorio_id_INT`),
  KEY `empresa_compra_FK_608856201` (`registro_estado_id_INT`),
  KEY `empresa_compra_FK_382690430` (`registro_estado_corporacao_id_INT`),
  CONSTRAINT `empresa_compra_FK_382690430` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_FK_584411621` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_FK_608856201` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_3` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_5` FOREIGN KEY (`forma_pagamento_id_INT`) REFERENCES `forma_pagamento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_6` FOREIGN KEY (`minha_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_7` FOREIGN KEY (`outra_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_compra_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_compra_parcela`;
CREATE TABLE `empresa_compra_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `valor_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_compra_parcela_FK_238128662` (`corporacao_id_INT`),
  KEY `sdhgfgh` (`empresa_compra_id_INT`),
  CONSTRAINT `empresa_compra_parcela_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sdhgfgh` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_compra_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_equipe`;
CREATE TABLE `empresa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_equipe_FK_652496338` (`empresa_id_INT`),
  KEY `empresa_equipe_FK_545715332` (`corporacao_id_INT`),
  CONSTRAINT `empresa_equipe_FK_545715332` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_equipe_FK_652496338` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_perfil`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_perfil`;
CREATE TABLE `empresa_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `perfil_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `empresa_id_INT` (`empresa_id_INT`,`perfil_id_INT`,`corporacao_id_INT`),
  KEY `empresa_perfil_FK_569122315` (`empresa_id_INT`),
  KEY `empresa_perfil_FK_645446777` (`perfil_id_INT`),
  KEY `empresa_perfil_FK_11688232` (`corporacao_id_INT`),
  CONSTRAINT `empresa_perfil_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_perfil_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_perfil_ibfk_3` FOREIGN KEY (`perfil_id_INT`) REFERENCES `perfil` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_perfil
-- ----------------------------
INSERT INTO `empresa_perfil` VALUES ('21', null, '1', '1');
INSERT INTO `empresa_perfil` VALUES ('45', null, '1', '1');
INSERT INTO `empresa_perfil` VALUES ('18', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('19', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('20', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('22', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('23', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('24', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('25', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('26', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('27', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('28', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('29', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('30', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('31', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('32', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('33', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('34', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('35', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('36', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('37', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('39', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('40', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('41', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('42', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('43', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('44', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('46', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('47', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('48', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('49', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('50', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('51', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('52', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('53', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('54', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('55', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('56', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('57', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('58', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('59', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('60', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('61', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('62', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('63', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('64', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('65', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('66', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('67', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('68', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('69', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('70', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('71', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('72', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('73', null, '2', '1');
INSERT INTO `empresa_perfil` VALUES ('4', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('74', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('75', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('76', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('77', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('78', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('79', null, '4', '1');
INSERT INTO `empresa_perfil` VALUES ('80', null, '4', '1');

-- ----------------------------
-- Table structure for `empresa_produto`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_produto`;
CREATE TABLE `empresa_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `preco_venda_FLOAT` double DEFAULT NULL,
  `estoque_atual_INT` int(11) DEFAULT NULL,
  `estoque_minimo_INT` int(11) DEFAULT NULL,
  `prazo_reposicao_estoque_dias_INT` int(6) DEFAULT NULL,
  `codigo_barra` char(13) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_FK_453643799` (`empresa_id_INT`),
  KEY `empresa_produto_FK_183288574` (`corporacao_id_INT`),
  KEY `empresa_produto_FK_384552002` (`produto_id_INT`),
  CONSTRAINT `empresa_produto_FK_384552002` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_ibfk_4` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_produto
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_produto_compra`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_produto_compra`;
CREATE TABLE `empresa_produto_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_compra_FK_163604736` (`empresa_compra_id_INT`),
  KEY `empresa_produto_compra_FK_69030761` (`corporacao_id_INT`),
  KEY `empresa_produto_compra_FK_968383790` (`produto_id_INT`),
  KEY `empresa_produto_compra_FK_618072510` (`empresa_id_INT`),
  CONSTRAINT `empresa_produto_compra_FK_618072510` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_compra_FK_968383790` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_compra_ibfk_1` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_compra_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_produto_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_produto_venda`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_produto_venda`;
CREATE TABLE `empresa_produto_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_venda_FK_890716553` (`empresa_venda_id_INT`),
  KEY `empresa_produto_venda_FK_292968750` (`corporacao_id_INT`),
  KEY `empresa_produto_venda_FK_420928955` (`produto_id_INT`),
  KEY `empresa_produto_venda_FK_367370605` (`empresa_id_INT`),
  CONSTRAINT `empresa_produto_venda_FK_367370605` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_venda_FK_420928955` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_venda_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_venda_ibfk_3` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_produto_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_venda`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_venda`;
CREATE TABLE `empresa_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `cliente_empresa_id_INT` int(11) DEFAULT NULL,
  `fornecedor_empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `acrescimo_FLOAT` double DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `fechamento_SEC` int(10) DEFAULT NULL,
  `fechamento_OFFSEC` int(6) DEFAULT NULL,
  `valor_pago_FLOAT` double DEFAULT NULL,
  `mesa_id_INT` int(11) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_empr83221` (`forma_pagamento_id_INT`) USING BTREE,
  KEY `key_empr213501` (`usuario_id_INT`) USING BTREE,
  KEY `key_empr835846` (`cliente_empresa_id_INT`) USING BTREE,
  KEY `key_empr655823` (`fornecedor_empresa_id_INT`) USING BTREE,
  KEY `key_empr330170` (`pessoa_id_INT`) USING BTREE,
  KEY `key_empr193359` (`corporacao_id_INT`) USING BTREE,
  KEY `empresa_venda_FK_985656739` (`registro_estado_id_INT`),
  KEY `empresa_venda_FK_407073975` (`registro_estado_corporacao_id_INT`),
  KEY `empresa_venda_FK_402862549` (`relatorio_id_INT`),
  KEY `empresa_venda_FK_648132324` (`mesa_id_INT`),
  CONSTRAINT `empresa_venda_FK_402862549` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_FK_407073975` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_FK_648132324` FOREIGN KEY (`mesa_id_INT`) REFERENCES `mesa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_FK_985656739` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_1` FOREIGN KEY (`forma_pagamento_id_INT`) REFERENCES `forma_pagamento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_3` FOREIGN KEY (`cliente_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_5` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_6` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_7` FOREIGN KEY (`fornecedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_venda_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_venda_parcela`;
CREATE TABLE `empresa_venda_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `valor_FLOAT` double NOT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `seq_INT` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_venda_parcela_FK_200561523` (`empresa_venda_id_INT`),
  KEY `empresa_venda_parcela_FK_50445556` (`corporacao_id_INT`),
  CONSTRAINT `empresa_venda_parcela_ibfk_1` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_parcela_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_venda_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_venda_template`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_venda_template`;
CREATE TABLE `empresa_venda_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `template_json` varchar(512) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_venda_template_FK_418518066` (`produto_id_INT`),
  KEY `empresa_venda_template_FK_963958741` (`empresa_id_INT`),
  KEY `empresa_venda_template_FK_776367188` (`corporacao_id_INT`),
  CONSTRAINT `empresa_venda_template_FK_418518066` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_template_FK_776367188` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_template_FK_963958741` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_venda_template
-- ----------------------------

-- ----------------------------
-- Table structure for `estado_civil`
-- ----------------------------
DROP TABLE IF EXISTS `estado_civil`;
CREATE TABLE `estado_civil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_63545` (`nome`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_civil
-- ----------------------------
INSERT INTO `estado_civil` VALUES ('2', 'casado');
INSERT INTO `estado_civil` VALUES ('3', 'divorciado');
INSERT INTO `estado_civil` VALUES ('1', 'solteiro');
INSERT INTO `estado_civil` VALUES ('4', 'viúvo');

-- ----------------------------
-- Table structure for `forma_pagamento`
-- ----------------------------
DROP TABLE IF EXISTS `forma_pagamento`;
CREATE TABLE `forma_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `forma_pagamento_FK_734954834` (`corporacao_id_INT`),
  CONSTRAINT `forma_pagamento_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of forma_pagamento
-- ----------------------------

-- ----------------------------
-- Table structure for `mensagem`
-- ----------------------------
DROP TABLE IF EXISTS `mensagem`;
CREATE TABLE `mensagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `mensagem` varchar(512) DEFAULT NULL,
  `remetente_usuario_id_INT` int(11) DEFAULT NULL,
  `tipo_mensagem_id_INT` int(11) DEFAULT NULL,
  `destinatario_usuario_id_INT` int(11) DEFAULT NULL,
  `destinatario_pessoa_id_INT` int(11) DEFAULT NULL,
  `destinatario_empresa_id_INT` int(11) DEFAULT NULL,
  `data_min_envio_SEC` int(10) DEFAULT NULL,
  `data_min_envio_OFFSEC` int(6) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `empresa_para_cliente_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mensagem_FK_646789551` (`remetente_usuario_id_INT`),
  KEY `mensagem_FK_970550538` (`tipo_mensagem_id_INT`),
  KEY `mensagem_FK_119873046` (`destinatario_usuario_id_INT`),
  KEY `mensagem_FK_67901611` (`destinatario_pessoa_id_INT`),
  KEY `mensagem_FK_637390137` (`destinatario_empresa_id_INT`),
  KEY `mensagem_FK_258514404` (`registro_estado_id_INT`),
  KEY `mensagem_FK_570434570` (`corporacao_id_INT`),
  CONSTRAINT `mensagem_FK_119873046` FOREIGN KEY (`destinatario_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_258514404` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_570434570` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_637390137` FOREIGN KEY (`destinatario_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_646789551` FOREIGN KEY (`remetente_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_67901611` FOREIGN KEY (`destinatario_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_970550538` FOREIGN KEY (`tipo_mensagem_id_INT`) REFERENCES `mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for `mensagem_envio`
-- ----------------------------
DROP TABLE IF EXISTS `mensagem_envio`;
CREATE TABLE `mensagem_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_canal_envio_id_INT` int(11) DEFAULT NULL,
  `mensagem_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `obs` varchar(100) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `tentativa_INT` int(2) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mensagem_envio_FK_703460694` (`tipo_canal_envio_id_INT`),
  KEY `mensagem_envio_FK_757080078` (`mensagem_id_INT`),
  KEY `mensagem_envio_FK_273468017` (`registro_estado_id_INT`),
  KEY `mensagem_envio_FK_794128418` (`corporacao_id_INT`),
  CONSTRAINT `mensagem_envio_FK_273468017` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_envio_FK_703460694` FOREIGN KEY (`tipo_canal_envio_id_INT`) REFERENCES `tipo_canal_envio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_envio_FK_757080078` FOREIGN KEY (`mensagem_id_INT`) REFERENCES `mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_envio_FK_794128418` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mensagem_envio
-- ----------------------------

-- ----------------------------
-- Table structure for `mesa`
-- ----------------------------
DROP TABLE IF EXISTS `mesa`;
CREATE TABLE `mesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `template_json` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mesa_FK_462615967` (`empresa_id_INT`),
  KEY `mesa_FK_317504883` (`corporacao_id_INT`),
  CONSTRAINT `mesa_FK_317504883` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_FK_462615967` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mesa
-- ----------------------------

-- ----------------------------
-- Table structure for `mesa_reserva`
-- ----------------------------
DROP TABLE IF EXISTS `mesa_reserva`;
CREATE TABLE `mesa_reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mesa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `reserva_SEC` int(10) DEFAULT NULL,
  `reserva_OFFSEC` int(6) DEFAULT NULL,
  `confimado_SEC` int(10) DEFAULT NULL,
  `confirmado_OFFSEC` int(6) DEFAULT NULL,
  `confirmador_usuario_id_INT` int(11) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mesa_reserva_FK_235290527` (`mesa_id_INT`),
  KEY `mesa_reserva_FK_304656982` (`pessoa_id_INT`),
  KEY `mesa_reserva_FK_974975586` (`confirmador_usuario_id_INT`),
  KEY `mesa_reserva_FK_961578370` (`corporacao_id_INT`),
  CONSTRAINT `mesa_reserva_FK_235290527` FOREIGN KEY (`mesa_id_INT`) REFERENCES `mesa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_reserva_FK_304656982` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_reserva_FK_961578370` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_reserva_FK_974975586` FOREIGN KEY (`confirmador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mesa_reserva
-- ----------------------------

-- ----------------------------
-- Table structure for `modelo`
-- ----------------------------
DROP TABLE IF EXISTS `modelo`;
CREATE TABLE `modelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `ano_INT` int(4) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome_normalizado`,`ano_INT`,`corporacao_id_INT`) USING BTREE,
  KEY `modelo_ibfk_1` (`corporacao_id_INT`),
  CONSTRAINT `modelo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modelo
-- ----------------------------

-- ----------------------------
-- Table structure for `negociacao_divida`
-- ----------------------------
DROP TABLE IF EXISTS `negociacao_divida`;
CREATE TABLE `negociacao_divida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `devedor_empresa_id_INT` int(11) DEFAULT NULL,
  `devedor_pessoa_id_INT` int(11) DEFAULT NULL,
  `negociador_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `resultado_empresa_venda_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `negociacao_divida_FK_620697022` (`devedor_empresa_id_INT`),
  KEY `negociacao_divida_FK_110839843` (`devedor_pessoa_id_INT`),
  KEY `negociacao_divida_FK_708282471` (`negociador_usuario_id_INT`),
  KEY `negociacao_divida_FK_563903809` (`resultado_empresa_venda_id_INT`),
  KEY `negociacao_divida_FK_873504639` (`relatorio_id_INT`),
  KEY `negociacao_divida_FK_729370117` (`registro_estado_id_INT`),
  KEY `negociacao_divida_FK_940582276` (`corporacao_id_INT`),
  CONSTRAINT `negociacao_divida_FK_110839843` FOREIGN KEY (`devedor_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_563903809` FOREIGN KEY (`resultado_empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_620697022` FOREIGN KEY (`devedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_708282471` FOREIGN KEY (`negociador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_729370117` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_873504639` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_940582276` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of negociacao_divida
-- ----------------------------

-- ----------------------------
-- Table structure for `negociacao_divida_empresa_venda`
-- ----------------------------
DROP TABLE IF EXISTS `negociacao_divida_empresa_venda`;
CREATE TABLE `negociacao_divida_empresa_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_protocolo_INT` bigint(11) DEFAULT NULL,
  `negociacao_divida_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `negociacao_divida_empresa_venda_FK_449615478` (`negociacao_divida_id_INT`),
  KEY `negociacao_divida_empresa_venda_FK_306701660` (`corporacao_id_INT`),
  CONSTRAINT `negociacao_divida_empresa_venda_FK_306701660` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_empresa_venda_FK_449615478` FOREIGN KEY (`negociacao_divida_id_INT`) REFERENCES `negociacao_divida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of negociacao_divida_empresa_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `nota`
-- ----------------------------
DROP TABLE IF EXISTS `nota`;
CREATE TABLE `nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nota_FK_922271729` (`empresa_id_INT`),
  KEY `nota_FK_826232910` (`pessoa_id_INT`),
  KEY `nota_FK_654876709` (`usuario_id_INT`),
  KEY `nota_FK_336425781` (`veiculo_id_INT`),
  KEY `nota_FK_797149659` (`relatorio_id_INT`),
  KEY `nota_FK_750427246` (`corporacao_id_INT`),
  CONSTRAINT `nota_FK_336425781` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_654876709` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_750427246` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_797149659` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_826232910` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_922271729` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nota
-- ----------------------------

-- ----------------------------
-- Table structure for `nota_tipo_nota`
-- ----------------------------
DROP TABLE IF EXISTS `nota_tipo_nota`;
CREATE TABLE `nota_tipo_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota_id_INT` int(11) DEFAULT NULL,
  `tipo_nota_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nota_id_INT` (`nota_id_INT`,`tipo_nota_id_INT`,`corporacao_id_INT`),
  KEY `nota_tipo_nota_FK_277954101` (`tipo_nota_id_INT`),
  KEY `nota_tipo_nota_FK_861236573` (`corporacao_id_INT`),
  CONSTRAINT `nota_tipo_nota_FK_277954101` FOREIGN KEY (`tipo_nota_id_INT`) REFERENCES `tipo_nota` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_tipo_nota_FK_861236573` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_tipo_nota_FK_92987060` FOREIGN KEY (`nota_id_INT`) REFERENCES `nota` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nota_tipo_nota
-- ----------------------------

-- ----------------------------
-- Table structure for `operadora`
-- ----------------------------
DROP TABLE IF EXISTS `operadora`;
CREATE TABLE `operadora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `nome_normalizado` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_operadora_UNIQUE` (`nome_normalizado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operadora
-- ----------------------------
INSERT INTO `operadora` VALUES ('1', 'NEXTEL', 'NEXTEL');
INSERT INTO `operadora` VALUES ('2', 'VIVO', 'VIVO');
INSERT INTO `operadora` VALUES ('3', 'OI', 'OI');
INSERT INTO `operadora` VALUES ('4', 'CLARO', 'CLARO');
INSERT INTO `operadora` VALUES ('5', 'TIM', 'TIM');

-- ----------------------------
-- Table structure for `pais`
-- ----------------------------
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_83746` (`nome_normalizado`,`corporacao_id_INT`) USING BTREE,
  KEY `pais_FK_330535889` (`corporacao_id_INT`),
  CONSTRAINT `pais_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pais
-- ----------------------------

-- ----------------------------
-- Table structure for `perfil`
-- ----------------------------
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_perfil_UNIQUE` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of perfil
-- ----------------------------
INSERT INTO `perfil` VALUES ('2', 'Cliente');
INSERT INTO `perfil` VALUES ('3', 'Fornecedor');
INSERT INTO `perfil` VALUES ('4', 'Minha Empresa');
INSERT INTO `perfil` VALUES ('1', 'Parceiro');

-- ----------------------------
-- Table structure for `permissao`
-- ----------------------------
DROP TABLE IF EXISTS `permissao`;
CREATE TABLE `permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `pai_permissao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`),
  KEY `permissao_fk` (`pai_permissao_id_INT`),
  CONSTRAINT `permissao_ibfk_1` FOREIGN KEY (`pai_permissao_id_INT`) REFERENCES `permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissao
-- ----------------------------
INSERT INTO `permissao` VALUES ('1', 'Ponto Eletrônico', 'ServicePontoEletronicoActivityMobile', null);
INSERT INTO `permissao` VALUES ('2', 'Rastreamento', 'ServiceRastrearActivityMobile', null);
INSERT INTO `permissao` VALUES ('3', 'Cadastros', 'ServiceCadastroActivityMobile', null);
INSERT INTO `permissao` VALUES ('4', 'Catálogos', 'ServiceCatalogoActivityMobile', null);
INSERT INTO `permissao` VALUES ('5', 'Monitoramento Remoto', 'ServiceMonitoramentoRemotoActivityMobile', null);
INSERT INTO `permissao` VALUES ('7', 'Serviços Online', 'ServiceOnlineActivityMobile', null);
INSERT INTO `permissao` VALUES ('8', 'Permissões', 'ServicePermissaoActivityMobile', null);
INSERT INTO `permissao` VALUES ('9', 'Configuração', 'ConfigurationActivityMobile', null);
INSERT INTO `permissao` VALUES ('10', 'Sobre', 'AboutActivityMobile', null);
INSERT INTO `permissao` VALUES ('11', 'Teclado', 'SelectEmpresaPontoEletronicoActivityMobile', '1');
INSERT INTO `permissao` VALUES ('12', 'Leitor QR Code', 'PontoEletronicoActivityMobile', '1');
INSERT INTO `permissao` VALUES ('13', 'Histórico de Rota', 'SelectUsuarioPosicaoActivityMobile', '2');
INSERT INTO `permissao` VALUES ('14', 'Monitorar', 'SelectVeiculoUsuarioActivityMobile', '2');
INSERT INTO `permissao` VALUES ('15', 'Cadastro Empresa', 'FormEmpresaActivityMobile', '3');
INSERT INTO `permissao` VALUES ('16', 'Cadastro Pessoa', 'FormPessoaActivityMobile', '3');
INSERT INTO `permissao` VALUES ('17', 'Cadastro Tarefa', 'FormTarefaActivityMobile', '3');
INSERT INTO `permissao` VALUES ('18', 'Cadastro Veículo', 'FormVeiculoActivityMobile', '3');
INSERT INTO `permissao` VALUES ('19', 'Cadastro Usuário do Veículo', 'FormVeiculoUsuarioActivityMobile', '3');
INSERT INTO `permissao` VALUES ('20', 'Cadastro Usuário', 'FormUsuarioActivityMobile', '3');
INSERT INTO `permissao` VALUES ('21', 'Catálogo Empresa', 'FilterEmpresaActivityMobile', '4');
INSERT INTO `permissao` VALUES ('22', 'Catálogo Pessoa', 'FilterPessoaActivityMobile', '4');
INSERT INTO `permissao` VALUES ('23', 'Catálogo Tarefa', 'FilterTarefaActivityMobile', '4');
INSERT INTO `permissao` VALUES ('24', 'Catálogo Veículo', 'FilterVeiculoActivityMobile', '4');
INSERT INTO `permissao` VALUES ('25', 'Catálogo Usuário do Veículo', 'FilterVeiculoUsuarioActivityMobile', '4');
INSERT INTO `permissao` VALUES ('26', 'Catálogo Usuário', 'FilterUsuarioActivityMobile', '4');
INSERT INTO `permissao` VALUES ('27', 'Monitorar Usuário', 'SelectServicoMonitoramentoRemotoUsuarioActivityMobile', '5');
INSERT INTO `permissao` VALUES ('29', 'Cadastrar Categoria Permissão', 'FormCategoriaPermissaoActivityMobile', '8');
INSERT INTO `permissao` VALUES ('30', 'Gerenciar Categoria Permissão', 'SelectCategoriaPermissaoActivityMobile', '8');
INSERT INTO `permissao` VALUES ('31', 'Cadastro Estado', 'FormEstadoActivityMobile', '74');
INSERT INTO `permissao` VALUES ('32', 'Catálogo Estado', 'FilterEstadoActivityMobile', '75');
INSERT INTO `permissao` VALUES ('33', 'Cadastro Cidade', 'FormCidadeActivityMobile', '74');
INSERT INTO `permissao` VALUES ('34', 'Catálogo Cidade', 'FilterCidadeActivityMobile', '75');
INSERT INTO `permissao` VALUES ('35', 'Cadastro Bairro', 'FormBairroActivityMobile', '74');
INSERT INTO `permissao` VALUES ('36', 'Catálogo Bairro', 'FilterBairroActivityMobile', '75');
INSERT INTO `permissao` VALUES ('37', 'Alterar Senha', 'AlterarSenhaUsuarioActivityMobile', '9');
INSERT INTO `permissao` VALUES ('45', 'Editar Empresa', 'EditEmpresa', '21');
INSERT INTO `permissao` VALUES ('47', 'Editar Tarefa', 'EditTarefa', '23');
INSERT INTO `permissao` VALUES ('48', 'Editar Veículo', 'EditVeiculo', '24');
INSERT INTO `permissao` VALUES ('49', 'Editar Usuário do Veículo', 'EditVeiculoUsuario', '25');
INSERT INTO `permissao` VALUES ('50', 'Editar Usuário', 'EditUsuario', '26');
INSERT INTO `permissao` VALUES ('51', 'Editar Estado', 'EditUf', '32');
INSERT INTO `permissao` VALUES ('52', 'Editar Cidade', 'EditCidade', '34');
INSERT INTO `permissao` VALUES ('53', 'Editar Bairro', 'EditBairro', '36');
INSERT INTO `permissao` VALUES ('54', 'Remover Empresa', 'RemoveEmpresa', '21');
INSERT INTO `permissao` VALUES ('56', 'Remover Tarefa', 'RemoveTarefa', '23');
INSERT INTO `permissao` VALUES ('57', 'Remover Veículo', 'RemoveVeiculo', '24');
INSERT INTO `permissao` VALUES ('58', 'Remover Usuário do Veículo', 'RemoveVeiculoUsuario', '25');
INSERT INTO `permissao` VALUES ('59', 'Remover Usuário', 'RemoveUsuario', '26');
INSERT INTO `permissao` VALUES ('60', 'Remover Estado', 'RemoveUf', '32');
INSERT INTO `permissao` VALUES ('61', 'Remover Cidade', 'RemoveCidade', '34');
INSERT INTO `permissao` VALUES ('62', 'Remover Bairro', 'RemoveBairro', '36');
INSERT INTO `permissao` VALUES ('68', 'Sincronizar Dados', 'SynchronizeReceiveActivityMobile', '9');
INSERT INTO `permissao` VALUES ('69', 'Histórico Monitoramento Usuário', 'SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile', '5');
INSERT INTO `permissao` VALUES ('70', 'Catálogo País', 'FilterPaisActivityMobile', '75');
INSERT INTO `permissao` VALUES ('71', 'Cadastro País', 'FormPaisActivityMobile', '74');
INSERT INTO `permissao` VALUES ('72', 'Editar País', 'EditPais', '70');
INSERT INTO `permissao` VALUES ('73', 'Remover País', 'RemovePais', '70');
INSERT INTO `permissao` VALUES ('74', 'Cadastro Geral', 'ServiceCadastroGeralActivityMobile', '3');
INSERT INTO `permissao` VALUES ('75', 'Catálogo Geral', 'ServiceCatalogoGeralActivityMobile', '4');
INSERT INTO `permissao` VALUES ('76', 'Cadastro Corporação', 'FormCorporacaoActivityMobile', '9');
INSERT INTO `permissao` VALUES ('77', 'Catálogo Corporação', 'FilterCorporacaoActivityMobile', '9');
INSERT INTO `permissao` VALUES ('78', 'Meus Serviços', 'MenuServicoOnlineUsuarioActivityMobile', '7');
INSERT INTO `permissao` VALUES ('79', 'Serviço Online Usuário', 'SelectServicoOnlineGrupoUsuarioActivityMobile', '7');
INSERT INTO `permissao` VALUES ('80', 'Dirigir Veículo', 'DirigirVeiculoActivityMobile', '2');
INSERT INTO `permissao` VALUES ('85', 'Cadastro Profissão', 'FormProfissaoActivityMobile', '74');
INSERT INTO `permissao` VALUES ('86', 'Cadastro Tipo Documento', 'FormTipoDocumentoActivityMobile', '74');
INSERT INTO `permissao` VALUES ('87', 'Catálogo Profissão', 'FilterProfissaoActivityMobile', '75');
INSERT INTO `permissao` VALUES ('88', 'Catálogo Tipo Documento', 'FilterTipoDocumentoActivityMobile', '75');
INSERT INTO `permissao` VALUES ('89', 'Remover Profissão', 'RemoveProfissao', '87');
INSERT INTO `permissao` VALUES ('90', 'Remover Tipo Documento', 'RemoveTipoDocumento', '88');
INSERT INTO `permissao` VALUES ('91', 'Editar Profissão', 'EditProfissao', '87');
INSERT INTO `permissao` VALUES ('92', 'Editar Tipo Documento', 'EditTipoDocumento', '88');
INSERT INTO `permissao` VALUES ('93', 'Catálogo Tipo Empresa', 'FilterTipoEmpresaActivityMobile', '75');
INSERT INTO `permissao` VALUES ('94', 'Cadastro Tipo Empresa', 'FormTipoEmpresaActivityMobile', '74');
INSERT INTO `permissao` VALUES ('95', 'Remover Tipo Empresa', 'RemoveTipoEmpresa', '93');
INSERT INTO `permissao` VALUES ('96', 'Editar Tipo Empresa', 'EditTipoEmpresa', '93');
INSERT INTO `permissao` VALUES ('99', 'Cadastro Modelo', 'FormModeloActivityMobile', '74');
INSERT INTO `permissao` VALUES ('100', 'Catálogo Modelo', 'FilterModeloActivityMobile', '75');
INSERT INTO `permissao` VALUES ('101', 'Catálogo Id Pessoa Empresa', 'FilterPessoaEmpresaActivityMobile', '1');
INSERT INTO `permissao` VALUES ('102', 'Sincronização Ponto', 'SincronizacaoPontoActivityMobile', '1');
INSERT INTO `permissao` VALUES ('103', 'Tutorial Ponto Eletrônico', 'TutorialPontoEletronicoActivityMobile', '1');
INSERT INTO `permissao` VALUES ('104', 'Minhas Tarefas', 'FilterMinhasTarefasActivityMobile', null);
INSERT INTO `permissao` VALUES ('105', 'Catálogo Rede', 'FilterRedeActivityMobile', '4');
INSERT INTO `permissao` VALUES ('106', 'Cadastro Rede', 'FormRedeActivityMobile', '3');
INSERT INTO `permissao` VALUES ('107', 'Minha Rotina', 'ListPessoaEmpresaRotinaActivityMobile', '1');
INSERT INTO `permissao` VALUES ('108', 'Meus Pontos Batidos', 'ListMeuPontoActivityMobile', '1');
INSERT INTO `permissao` VALUES ('109', 'Catálogo de Pontos Batidos', 'FilterPontoActivityMobile', '1');
INSERT INTO `permissao` VALUES ('110', 'Relatório', 'ServiceRelatorioActivityMobile', null);
INSERT INTO `permissao` VALUES ('111', 'Catálogo Relatório', 'FilterRelatorioActivityMobile', '110');
INSERT INTO `permissao` VALUES ('112', 'Cadastro Relatório', 'FormRelatorioActivityMobile', '110');
INSERT INTO `permissao` VALUES ('113', 'Minha Rotina', 'DetailRotinaActivityMobile', '1');

-- ----------------------------
-- Table structure for `permissao_categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `permissao_categoria_permissao`;
CREATE TABLE `permissao_categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissao_id_INT` int(11) NOT NULL,
  `categoria_permissao_id_INT` int(11) NOT NULL,
  `corporacao_id_INT` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria_permissao_id_INT` (`categoria_permissao_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `permissao_id_INT` (`permissao_id_INT`),
  CONSTRAINT `permissao_categoria_permissao_ibfk_4` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissao_categoria_permissao_ibfk_7` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissao_categoria_permissao_ibfk_9` FOREIGN KEY (`permissao_id_INT`) REFERENCES `permissao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissao_categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa`;
CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `tipo_documento_id_INT` int(11) DEFAULT NULL,
  `numero_documento` varchar(30) DEFAULT NULL,
  `is_consumidor_BOOLEAN` int(1) DEFAULT NULL,
  `sexo_id_INT` int(11) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `operadora_id_INT` int(11) DEFAULT NULL,
  `celular_sms` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(30) DEFAULT '',
  `complemento` varchar(100) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `bairro_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_nascimento_SEC` int(10) DEFAULT NULL,
  `data_nascimento_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `ind_email_valido_BOOLEAN` int(1) DEFAULT NULL,
  `ind_celular_valido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_95867` (`email`,`corporacao_id_INT`) USING BTREE,
  KEY `e_bairro_FK` (`bairro_id_INT`),
  KEY `e_cidade_FK` (`cidade_id_INT`),
  KEY `funcionario_sexo_FK` (`sexo_id_INT`),
  KEY `pessoa_FK_169738769` (`corporacao_id_INT`),
  KEY `pessoa_FK_224273681` (`tipo_documento_id_INT`),
  KEY `pessoa_FK_5187988` (`operadora_id_INT`),
  KEY `pessoa_FK_594970703` (`relatorio_id_INT`),
  CONSTRAINT `pessoa_FK_594970703` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_2` FOREIGN KEY (`tipo_documento_id_INT`) REFERENCES `tipo_documento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_3` FOREIGN KEY (`sexo_id_INT`) REFERENCES `sexo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_4` FOREIGN KEY (`operadora_id_INT`) REFERENCES `operadora` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_5` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_6` FOREIGN KEY (`bairro_id_INT`) REFERENCES `bairro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_empresa`;
CREATE TABLE `pessoa_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `profissao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pessoa_empresa_UNIQUE` (`pessoa_id_INT`,`empresa_id_INT`,`profissao_id_INT`,`corporacao_id_INT`),
  KEY `empresa_id_INT` (`empresa_id_INT`),
  KEY `fe_profissao_FK` (`profissao_id_INT`),
  KEY `pessoa_empresa_FK_41381835` (`pessoa_id_INT`),
  KEY `pessoa_empresa_FK_199859619` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_ibfk_2` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_ibfk_3` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_ibfk_4` FOREIGN KEY (`profissao_id_INT`) REFERENCES `profissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_empresa_rotina`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_empresa_rotina`;
CREATE TABLE `pessoa_empresa_rotina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_empresa_id_INT` int(11) DEFAULT NULL,
  `semana_INT` int(4) NOT NULL,
  `dia_semana_INT` int(4) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pessoa_empresa_rotina_FK_188354492` (`pessoa_empresa_id_INT`),
  KEY `pessoa_empresa_rotina_FK_969879151` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_empresa_rotina_ibfk_1` FOREIGN KEY (`pessoa_empresa_id_INT`) REFERENCES `pessoa_empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_rotina_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_empresa_rotina
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_equipe`;
CREATE TABLE `pessoa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pessoa_equipe_FK_929412842` (`empresa_equipe_id_INT`),
  KEY `pessoa_equipe_FK_198364258` (`usuario_id_INT`),
  KEY `pessoa_equipe_FK_935943604` (`pessoa_id_INT`),
  KEY `pessoa_equipe_FK_316467285` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_equipe_FK_198364258` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_equipe_FK_316467285` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_equipe_FK_929412842` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_equipe_FK_935943604` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_usuario`;
CREATE TABLE `pessoa_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pessoa_usuario_UNIQUE` (`pessoa_id_INT`,`usuario_id_INT`,`corporacao_id_INT`),
  KEY `pessoa_usuario_FK_347076416` (`pessoa_id_INT`),
  KEY `pessoa_usuario_FK_629943848` (`usuario_id_INT`),
  KEY `pessoa_usuario_FK_759368897` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_usuario_ibfk_1` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_usuario_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_usuario_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `ponto`
-- ----------------------------
DROP TABLE IF EXISTS `ponto`;
CREATE TABLE `ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `profissao_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `is_entrada_BOOLEAN` int(1) NOT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `tipo_ponto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ponto_FK_228240967` (`corporacao_id_INT`),
  KEY `ponto_FK_329772949` (`empresa_id_INT`),
  KEY `ponto_FK_160339355` (`profissao_id_INT`),
  KEY `ponto_FK_509979248` (`usuario_id_INT`),
  KEY `ponto_FK_1234567` (`tipo_ponto_id_INT`),
  KEY `pessoa_id_INT` (`pessoa_id_INT`),
  CONSTRAINT `ponto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_3` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_4` FOREIGN KEY (`profissao_id_INT`) REFERENCES `profissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_5` FOREIGN KEY (`tipo_ponto_id_INT`) REFERENCES `tipo_ponto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_6` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ponto
-- ----------------------------

-- ----------------------------
-- Table structure for `produto`
-- ----------------------------
DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `produto_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `prazo_reposicao_estoque_dias_INT` int(6) DEFAULT NULL,
  `codigo_barra` char(13) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_FK_183288574` (`corporacao_id_INT`),
  KEY `produto_FK_485992432` (`produto_unidade_medida_id_INT`),
  CONSTRAINT `produto_FK_485992432` FOREIGN KEY (`produto_unidade_medida_id_INT`) REFERENCES `produto_unidade_medida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `produto_tipo`;
CREATE TABLE `produto_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_98765` (`nome`,`corporacao_id_INT`) USING BTREE,
  KEY `empresa_produto_tipo_FK_926544190` (`corporacao_id_INT`),
  CONSTRAINT `produto_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_tipos`
-- ----------------------------
DROP TABLE IF EXISTS `produto_tipos`;
CREATE TABLE `produto_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_tipo_id_INT` int(11) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_tipos_FK_90423584` (`produto_tipo_id_INT`),
  KEY `produto_tipos_FK_811035157` (`produto_id_INT`),
  KEY `produto_tipos_FK_529571533` (`corporacao_id_INT`),
  CONSTRAINT `produto_tipos_FK_529571533` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `produto_tipos_FK_811035157` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `produto_tipos_FK_90423584` FOREIGN KEY (`produto_tipo_id_INT`) REFERENCES `produto_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto_tipos
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `produto_unidade_medida`;
CREATE TABLE `produto_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `abreviacao` varchar(10) NOT NULL,
  `is_float_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `profissao`
-- ----------------------------
DROP TABLE IF EXISTS `profissao`;
CREATE TABLE `profissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_34677654` (`nome_normalizado`,`corporacao_id_INT`) USING BTREE,
  KEY `profissao_FK_779541016` (`corporacao_id_INT`),
  CONSTRAINT `profissao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of profissao
-- ----------------------------

-- ----------------------------
-- Table structure for `recebivel`
-- ----------------------------
DROP TABLE IF EXISTS `recebivel`;
CREATE TABLE `recebivel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_FLOAT` double DEFAULT NULL,
  `devedor_empresa_id_INT` int(11) DEFAULT NULL,
  `devedor_pessoa_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `valor_pagamento_FLOAT` double DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `recebedor_usuario_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recebivel_FK_239410400` (`devedor_empresa_id_INT`),
  KEY `recebivel_FK_19348144` (`devedor_pessoa_id_INT`),
  KEY `recebivel_FK_829437256` (`cadastro_usuario_id_INT`),
  KEY `recebivel_FK_988525391` (`recebedor_usuario_id_INT`),
  KEY `recebivel_FK_1007080` (`relatorio_id_INT`),
  KEY `recebivel_FK_595886231` (`corporacao_id_INT`),
  CONSTRAINT `recebivel_FK_1007080` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_19348144` FOREIGN KEY (`devedor_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_239410400` FOREIGN KEY (`devedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_595886231` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_829437256` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_988525391` FOREIGN KEY (`recebedor_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recebivel
-- ----------------------------

-- ----------------------------
-- Table structure for `recebivel_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `recebivel_cotidiano`;
CREATE TABLE `recebivel_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recebedor_empresa_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `id_tipo_despesa_recebivel_INT` int(3) DEFAULT NULL,
  `parametro_INT` int(11) DEFAULT NULL,
  `parametro_OFFSEC` int(10) DEFAULT NULL,
  `parametro_SEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recebivel_cotidiano_FK_447082519` (`recebedor_empresa_id_INT`),
  KEY `recebivel_cotidiano_FK_452484131` (`cadastro_usuario_id_INT`),
  KEY `recebivel_cotidiano_FK_900634766` (`corporacao_id_INT`),
  CONSTRAINT `recebivel_cotidiano_FK_447082519` FOREIGN KEY (`recebedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_cotidiano_FK_452484131` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_cotidiano_FK_900634766` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recebivel_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `rede`
-- ----------------------------
DROP TABLE IF EXISTS `rede`;
CREATE TABLE `rede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`,`corporacao_id_INT`),
  KEY `rede_FK_554260254` (`corporacao_id_INT`),
  KEY `rede_FK_704559326` (`relatorio_id_INT`),
  CONSTRAINT `rede_FK_704559326` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rede_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rede
-- ----------------------------

-- ----------------------------
-- Table structure for `rede_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `rede_empresa`;
CREATE TABLE `rede_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rede_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rede_empresa_UNIQUE` (`rede_id_INT`,`empresa_id_INT`),
  KEY `rede_empresa_FK_472778320` (`corporacao_id_INT`),
  KEY `key_rede788727` (`empresa_id_INT`) USING BTREE,
  KEY `rede_id_INT` (`rede_id_INT`),
  CONSTRAINT `rede_empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rede_empresa_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rede_empresa_ibfk_3` FOREIGN KEY (`rede_id_INT`) REFERENCES `rede` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rede_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `registro`
-- ----------------------------
DROP TABLE IF EXISTS `registro`;
CREATE TABLE `registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocolo_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `id_origem_chamada_INT` int(3) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_FK_991607667` (`responsavel_usuario_id_INT`),
  KEY `registro_FK_883850098` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_FK_435150146` (`tipo_registro_id_INT`),
  KEY `registro_FK_886230469` (`registro_estado_id_INT`),
  KEY `registro_FK_225860595` (`registro_estado_corporacao_id_INT`),
  KEY `registro_FK_979919434` (`corporacao_id_INT`),
  CONSTRAINT `registro_FK_225860595` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_435150146` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_883850098` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_886230469` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_979919434` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_991607667` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro
-- ----------------------------

-- ----------------------------
-- Table structure for `registro_estado`
-- ----------------------------
DROP TABLE IF EXISTS `registro_estado`;
CREATE TABLE `registro_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `prazo_dias_INT` int(3) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_estado_FK_634155274` (`tipo_registro_id_INT`),
  CONSTRAINT `registro_estado_FK_634155274` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro_estado
-- ----------------------------
INSERT INTO `registro_estado` VALUES ('1', 'aguardando_inicializacao', null, '1');
INSERT INTO `registro_estado` VALUES ('2', 'em_execucao', null, '1');
INSERT INTO `registro_estado` VALUES ('3', 'finalizada', null, '1');
INSERT INTO `registro_estado` VALUES ('4', 'cancelada', null, '1');
INSERT INTO `registro_estado` VALUES ('5', 'pausada', null, '1');

-- ----------------------------
-- Table structure for `registro_estado_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `registro_estado_corporacao`;
CREATE TABLE `registro_estado_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `prazo_dias_INT` int(3) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_estado_corporacao_FK_488037109` (`responsavel_usuario_id_INT`),
  KEY `registro_estado_corporacao_FK_697784424` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_estado_corporacao_FK_794616700` (`tipo_registro_id_INT`),
  KEY `registro_estado_corporacao_FK_896209717` (`corporacao_id_INT`),
  CONSTRAINT `registro_estado_corporacao_FK_488037109` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_corporacao_FK_697784424` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_corporacao_FK_794616700` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_corporacao_FK_896209717` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro_estado_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `registro_fluxo_estado_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `registro_fluxo_estado_corporacao`;
CREATE TABLE `registro_fluxo_estado_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origem_registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `origem_registro_estado_id_INT` int(11) DEFAULT NULL,
  `destino_registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `destino_registro_estado_id_INT` int(11) DEFAULT NULL,
  `template_JSON` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_fluxo_estado_corporacao_FK_25970459` (`origem_registro_estado_corporacao_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_785644532` (`origem_registro_estado_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_761993408` (`destino_registro_estado_corporacao_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_918395997` (`destino_registro_estado_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_13153076` (`corporacao_id_INT`),
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_13153076` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_25970459` FOREIGN KEY (`origem_registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_761993408` FOREIGN KEY (`destino_registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_785644532` FOREIGN KEY (`origem_registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_918395997` FOREIGN KEY (`destino_registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro_fluxo_estado_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio`;
CREATE TABLE `relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `tipo_relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relatorio_FK_445983887` (`usuario_id_INT`),
  KEY `relatorio_FK_395233154` (`corporacao_id_INT`),
  KEY `relatorio_FK_984588624` (`tipo_relatorio_id_INT`),
  CONSTRAINT `relatorio_FK_984588624` FOREIGN KEY (`tipo_relatorio_id_INT`) REFERENCES `tipo_relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `relatorio_anexo`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio_anexo`;
CREATE TABLE `relatorio_anexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `arquivo` varchar(50) DEFAULT NULL,
  `tipo_anexo_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relatorio_anexo_FK_268829345` (`relatorio_id_INT`),
  KEY `relatorio_anexo_FK_663513184` (`tipo_anexo_id_INT`),
  KEY `relatorio_anexo_FK_730926514` (`corporacao_id_INT`),
  CONSTRAINT `relatorio_anexo_ibfk_1` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_anexo_ibfk_2` FOREIGN KEY (`tipo_anexo_id_INT`) REFERENCES `tipo_anexo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_anexo_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio_anexo
-- ----------------------------

-- ----------------------------
-- Table structure for `rotina`
-- ----------------------------
DROP TABLE IF EXISTS `rotina`;
CREATE TABLE `rotina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ultima_dia_contagem_ciclo_DATE` date NOT NULL,
  `ultima_dia_contagem_ciclo_data_SEC` int(10) DEFAULT NULL,
  `ultima_dia_contagem_ciclo_data_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rotina_FK_259857177` (`corporacao_id_INT`),
  CONSTRAINT `rotina_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rotina
-- ----------------------------

-- ----------------------------
-- Table structure for `servico`
-- ----------------------------
DROP TABLE IF EXISTS `servico`;
CREATE TABLE `servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of servico
-- ----------------------------
INSERT INTO `servico` VALUES ('1', 'Tirar Foto Interna', 'ServiceTiraFoto');
INSERT INTO `servico` VALUES ('2', 'Rastrear Posição', 'ServiceMyPosition');
INSERT INTO `servico` VALUES ('3', 'Tirar Foto Externa', 'ServiceTiraFoto');

-- ----------------------------
-- Table structure for `sexo`
-- ----------------------------
DROP TABLE IF EXISTS `sexo`;
CREATE TABLE `sexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sexo
-- ----------------------------
INSERT INTO `sexo` VALUES ('1', 'Masculino');
INSERT INTO `sexo` VALUES ('2', 'Feminino');

-- ----------------------------
-- Table structure for `sistema_atributo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_atributo`;
CREATE TABLE `sistema_atributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) DEFAULT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `tipo_sql` varchar(15) DEFAULT NULL,
  `tipo_sql_ficticio` varchar(50) DEFAULT NULL,
  `tamanho_INT` int(11) DEFAULT NULL,
  `decimal_INT` int(11) DEFAULT NULL,
  `not_null_BOOLEAN` int(1) DEFAULT NULL,
  `primary_key_BOOLEAN` int(1) DEFAULT NULL,
  `auto_increment_BOOLEAN` int(1) DEFAULT NULL,
  `valor_default` varchar(100) DEFAULT NULL,
  `fk_sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `atributo_fk` varchar(100) DEFAULT NULL,
  `fk_nome` varchar(255) DEFAULT NULL,
  `update_tipo_fk` varchar(30) DEFAULT NULL,
  `delete_tipo_fk` varchar(30) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `unique_BOOLEAN` int(1) DEFAULT NULL,
  `unique_nome` varchar(255) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_irtu` (`nome`,`sistema_tabela_id_INT`) USING BTREE,
  KEY `sa_sistema_tabela_FK` (`sistema_tabela_id_INT`),
  KEY `sistema_atributo_FK_622131348` (`fk_sistema_tabela_id_INT`),
  CONSTRAINT `sa_sistema_tabela_FK` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_atributo_FK_622131348` FOREIGN KEY (`fk_sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1314 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_atributo
-- ----------------------------
INSERT INTO `sistema_atributo` VALUES ('1', 'id', null, '466', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('2', 'data_login_DATETIME', null, '466', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('3', 'data_logout_DATETIME', null, '466', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('4', 'usuario_id_INT', null, '466', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'acesso_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('5', 'id', null, '1', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('6', 'nome', null, '1', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('7', 'nome_normalizado', null, '1', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome234234', null, '0');
INSERT INTO `sistema_atributo` VALUES ('8', 'cidade_id_INT', null, '1', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'bairro_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'nome234234', null, '0');
INSERT INTO `sistema_atributo` VALUES ('9', 'corporacao_id_INT', null, '1', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'bairro_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome234234', null, '0');
INSERT INTO `sistema_atributo` VALUES ('10', 'id', null, '2', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('11', 'nome', null, '2', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('12', 'nome_normalizado', null, '2', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_Aasd', null, '0');
INSERT INTO `sistema_atributo` VALUES ('13', 'corporacao_id_INT', null, '2', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'categoria_permissao_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_Aasd', null, '0');
INSERT INTO `sistema_atributo` VALUES ('14', 'id', null, '4', '', null, '3', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('15', 'nome', null, '4', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('16', 'nome_normalizado', null, '4', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_736', null, '0');
INSERT INTO `sistema_atributo` VALUES ('17', 'corporacao_id_INT', null, '4', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'cidade_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_736', null, '0');
INSERT INTO `sistema_atributo` VALUES ('18', 'uf_id_INT', null, '4', '', null, '11', null, '0', '0', '0', '', '13', 'id', 'cidade_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'nome_736', null, '0');
INSERT INTO `sistema_atributo` VALUES ('19', 'id', null, '49', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('20', 'nome', null, '49', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('21', 'nome_normalizado', null, '49', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'corporacao_nome_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('22', 'usuario_dropbox', null, '49', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('23', 'senha_dropbox', null, '49', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('24', 'id', null, '5', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('25', 'nome', null, '5', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('26', 'nome_normalizado', null, '5', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('27', 'telefone1', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('28', 'telefone2', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('29', 'fax', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('30', 'celular', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('31', 'operadora_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '77', 'id', 'empresa_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('32', 'celular_sms', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('33', 'email', null, '5', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'email', null, '0');
INSERT INTO `sistema_atributo` VALUES ('34', 'tipo_documento_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '23', 'id', 'empresa_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('35', 'numero_documento', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('36', 'tipo_empresa_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '12', 'id', 'empresa_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('37', 'logradouro', null, '5', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('38', 'logradouro_normalizado', null, '5', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('39', 'numero', null, '5', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('40', 'complemento', null, '5', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('41', 'complemento_normalizado', null, '5', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('42', 'bairro_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '1', 'id', 'empresa_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('43', 'cidade_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'empresa_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('44', 'latitude_INT', null, '5', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('45', 'longitude_INT', null, '5', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('46', 'foto', null, '5', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('47', 'corporacao_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'email', null, '0');
INSERT INTO `sistema_atributo` VALUES ('48', 'id', null, '54', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('49', 'forma_pagamento_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '143', 'id', 'empresa_compra_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('50', 'valor_total_FLOAT', null, '54', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('51', 'data_DATE', null, '54', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('52', 'hora_TIME', null, '54', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('53', 'minha_empresa_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_compra_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('54', 'outra_empresa_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_compra_ibfk_7', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('55', 'foto', null, '54', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('56', 'corporacao_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_compra_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('57', 'usuario_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'empresa_compra_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('58', 'id', null, '55', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('59', 'empresa_compra_id_INT', null, '55', '', null, '11', null, '0', '0', '0', '', '54', 'id', 'sdhgfgh', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('60', 'data_DATE', null, '55', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('61', 'valor_FLOAT', null, '55', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('62', 'corporacao_id_INT', null, '55', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_compra_parcela_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('63', 'id', null, '24', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('64', 'empresa_id_INT', null, '24', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_perfil_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'empresa_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('65', 'perfil_id_INT', null, '24', '', null, '11', null, '0', '0', '0', '', '30', 'id', 'empresa_perfil_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'empresa_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('66', 'corporacao_id_INT', null, '24', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_perfil_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'empresa_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('67', 'id', null, '56', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('68', 'identificador', null, '56', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('69', 'nome', null, '56', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('70', 'nome_normalizado', null, '56', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('71', 'descricao', null, '56', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('72', 'empresa_produto_tipo_id_INT', null, '56', '', null, '11', null, '0', '0', '0', '', '71', 'id', 'empresa_produto_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('73', 'empresa_produto_unidade_medida_id_INT', null, '56', '', null, '11', null, '0', '0', '0', '', '59', 'id', 'empresa_produto_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('74', 'empresa_id_INT', null, '56', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_produto_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('75', 'video', null, '56', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('76', 'corporacao_id_INT', null, '56', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_produto_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('77', 'id', null, '57', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('78', 'empresa_produto_id_INT', null, '57', '', null, '11', null, '0', '0', '0', '', '56', 'id', 'empresa_produto_compra_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('79', 'empresa_compra_id_INT', null, '57', '', null, '11', null, '0', '0', '0', '', '54', 'id', 'empresa_produto_compra_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('80', 'quantidade_FLOAT', null, '57', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('81', 'valor_total_FLOAT', null, '57', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('82', 'corporacao_id_INT', null, '57', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_produto_compra_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('83', 'id', null, '58', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('84', 'empresa_produto_id_INT', null, '58', '', null, '11', null, '0', '0', '0', '', '56', 'id', 'empresa_produto_foto_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('85', 'foto', null, '58', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('86', 'corporacao_id_INT', null, '58', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_produto_foto_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('87', 'id', null, '71', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('88', 'nome', null, '71', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_98765', null, '1');
INSERT INTO `sistema_atributo` VALUES ('89', 'nome_normalizado', null, '71', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('90', 'corporacao_id_INT', null, '71', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_produto_tipo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_98765', null, '1');
INSERT INTO `sistema_atributo` VALUES ('91', 'id', null, '59', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('92', 'nome', null, '59', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('93', 'abreviacao', null, '59', '', null, '10', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('94', 'is_float_BOOLEAN', null, '59', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('95', 'id', null, '60', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('96', 'empresa_produto_id_INT', null, '60', '', null, '11', null, '0', '0', '0', '', '56', 'id', 'empresa_produto_venda_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('97', 'empresa_venda_id_INT', null, '60', '', null, '11', null, '0', '0', '0', '', '68', 'id', 'empresa_produto_venda_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('98', 'quantidade_FLOAT', null, '60', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('99', 'valor_total_FLOAT', null, '60', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('100', 'corporacao_id_INT', null, '60', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_produto_venda_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('101', 'id', null, '61', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('102', 'identificador', null, '61', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('103', 'nome', null, '61', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('104', 'nome_normalizado', null, '61', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('105', 'descricao', null, '61', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('106', 'empresa_servico_tipo_id_INT', null, '61', '', null, '11', null, '0', '0', '0', '', '72', 'id', 'empresa_servico_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('107', 'empresa_id_INT', null, '61', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_servico_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('108', 'empresa_servico_unidade_medida_id_INT', null, '61', '', null, '11', null, '0', '0', '0', '', '64', 'id', 'empresa_servico_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('109', 'prazo_entrega_dia_INT', null, '61', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('110', 'duracao_meses_INT', null, '61', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('111', 'video', null, '61', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('112', 'corporacao_id_INT', null, '61', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_servico_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('113', 'id', null, '62', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('114', 'empresa_servico_id_INT', null, '62', '', null, '11', null, '0', '0', '0', '', '61', 'id', 'empresa_servico_compra_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('115', 'empresa_compra_id_INT', null, '62', '', null, '11', null, '0', '0', '0', '', '54', 'id', 'empresa_servico_compra_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('116', 'quantidade_FLOAT', null, '62', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('117', 'valor_total_FLOAT', null, '62', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('118', 'corporacao_id_INT', null, '62', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_servico_compra_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('119', 'id', null, '63', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('120', 'empresa_servico_id_INT', null, '63', '', null, '11', null, '0', '0', '0', '', '61', 'id', 'empresa_servico_foto_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('121', 'foto', null, '63', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('122', 'corporacao_id_INT', null, '63', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_servico_foto_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('123', 'id', null, '72', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('124', 'nome', null, '72', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_098787', null, '1');
INSERT INTO `sistema_atributo` VALUES ('125', 'nome_normalizado', null, '72', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('126', 'corporacao_id_INT', null, '72', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_servico_tipo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_098787', null, '1');
INSERT INTO `sistema_atributo` VALUES ('127', 'id', null, '64', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('128', 'nome', null, '64', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('129', 'id', null, '65', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('130', 'empresa_servico_id_INT', null, '65', '', null, '11', null, '0', '0', '0', '', '61', 'id', 'empresa_servico_venda_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('131', 'empresa_venda_id_INT', null, '65', '', null, '11', null, '0', '0', '0', '', '68', 'id', 'empresa_servico_venda_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('132', 'quantidade_FLOAT', null, '65', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('133', 'valor_total_FLOAT', null, '65', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('134', 'corporacao_id_INT', null, '65', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_servico_venda_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('135', 'id', null, '66', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('136', 'empresa_produto_id_INT', null, '66', '', null, '11', null, '0', '0', '0', '', '56', 'id', 'empresa_tipo_venda_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('137', 'empresa_servico_id_INT', null, '66', '', null, '11', null, '0', '0', '0', '', '61', 'id', 'empresa_tipo_venda_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('138', 'valor_total_FLOAT', null, '66', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('139', 'corporacao_id_INT', null, '66', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_tipo_venda_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('140', 'id', null, '67', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('141', 'empresa_tipo_venda_id_INT', null, '67', '', null, '11', null, '0', '0', '0', '', '66', 'id', 'empresa_tipo_venda_parcela_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('142', 'porcentagem_FLOAT', null, '67', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('143', 'corporacao_id_INT', null, '67', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_tipo_venda_parcela_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('144', 'id', null, '68', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('145', 'forma_pagamento_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '143', 'id', 'empresa_venda_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('146', 'valor_total_FLOAT', null, '68', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('147', 'data_DATE', null, '68', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('148', 'hora_TIME', null, '68', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('149', 'cliente_empresa_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_venda_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('150', 'fornecedor_empresa_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_venda_ibfk_7', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('151', 'pessoa_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'empresa_venda_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('152', 'foto', null, '68', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('153', 'corporacao_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_venda_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('154', 'usuario_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'empresa_venda_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('155', 'id', null, '69', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('156', 'empresa_venda_id_INT', null, '69', '', null, '11', null, '0', '0', '0', '', '68', 'id', 'empresa_venda_parcela_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('157', 'data_DATE', null, '69', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('158', 'valor_FLOAT', null, '69', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('159', 'corporacao_id_INT', null, '69', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_venda_parcela_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('160', 'id', null, '126', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('161', 'nome', null, '126', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_63545', null, '0');
INSERT INTO `sistema_atributo` VALUES ('162', 'id', null, '143', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('163', 'nome', null, '143', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('164', 'nome_normalizado', null, '143', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('165', 'corporacao_id_INT', null, '143', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'forma_pagamento_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('166', 'id', null, '441', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('167', 'hora_inicio_TIME', null, '441', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('168', 'hora_fim_TIME', null, '441', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('169', 'pessoa_empresa_id_INT', null, '441', '', null, '11', null, '0', '0', '0', '', '7', 'id', 'horario_trabalho_pessoa_empresa_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('170', 'data_DATE', null, '441', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('171', 'corporacao_id_INT', null, '441', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'horario_trabalho_pessoa_empresa_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('172', 'id', null, '8', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('173', 'nome', null, '8', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('174', 'nome_normalizado', null, '8', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('175', 'ano_INT', null, '8', '', null, '4', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('176', 'corporacao_id_INT', null, '8', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'modelo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('177', 'id', null, '467', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('178', 'tipo_operacao', null, '467', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('179', 'pagina_operacao', null, '467', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('180', 'entidade_operacao', null, '467', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('181', 'chave_registro_operacao_INT', null, '467', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('182', 'descricao_operacao', null, '467', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('183', 'data_operacao_DATETIME', null, '467', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('184', 'url_completa', null, '467', '', null, '1000', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('185', 'usuario_id_INT', null, '467', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'operacao_sistema_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('186', 'id', null, '77', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('187', 'nome', null, '77', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('188', 'nome_normalizado', null, '77', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_operadora_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('189', 'id', null, '22', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('190', 'nome', null, '22', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('191', 'nome_normalizado', null, '22', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_83746', null, '0');
INSERT INTO `sistema_atributo` VALUES ('192', 'corporacao_id_INT', null, '22', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'pais_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_83746', null, '0');
INSERT INTO `sistema_atributo` VALUES ('193', 'id', null, '30', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('194', 'nome', null, '30', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_perfil_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('195', 'id', null, '46', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('196', 'nome', null, '46', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('197', 'tag', null, '46', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'tag', null, '0');
INSERT INTO `sistema_atributo` VALUES ('198', 'pai_permissao_id_INT', null, '46', '', null, '11', null, '0', '0', '0', '', '46', 'id', 'permissao_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('199', 'id', null, '3', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('200', 'permissao_id_INT', null, '3', '', null, '11', null, '1', '0', '0', '', '46', 'id', 'permissao_categoria_permissao_ibfk_9', 'CASCADE', 'CASCADE', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('201', 'categoria_permissao_id_INT', null, '3', '', null, '11', null, '1', '0', '0', '', '2', 'id', 'permissao_categoria_permissao_ibfk_4', 'CASCADE', 'CASCADE', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('202', 'corporacao_id_INT', null, '3', '', null, '11', null, '1', '0', '0', '', '49', 'id', 'permissao_categoria_permissao_ibfk_7', 'CASCADE', 'CASCADE', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('203', 'id', null, '6', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('204', 'identificador', null, '6', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('205', 'nome', null, '6', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('206', 'nome_normalizado', null, '6', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('207', 'tipo_documento_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '23', 'id', 'pessoa_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('208', 'numero_documento', null, '6', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('209', 'is_consumidor_BOOLEAN', null, '6', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('210', 'sexo_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '39', 'id', 'pessoa_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('211', 'telefone', null, '6', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('212', 'celular', null, '6', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('213', 'operadora_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '77', 'id', 'pessoa_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('214', 'celular_sms', null, '6', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('215', 'email', null, '6', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_95867', null, '0');
INSERT INTO `sistema_atributo` VALUES ('216', 'logradouro', null, '6', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('217', 'logradouro_normalizado', null, '6', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('218', 'numero', null, '6', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('219', 'complemento', null, '6', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('220', 'complemento_normalizado', null, '6', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('221', 'cidade_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'pessoa_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('222', 'bairro_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '1', 'id', 'pessoa_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('223', 'latitude_INT', null, '6', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('224', 'longitude_INT', null, '6', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('225', 'foto', null, '6', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('226', 'corporacao_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'pessoa_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_95867', null, '0');
INSERT INTO `sistema_atributo` VALUES ('227', 'id', null, '7', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('228', 'pessoa_id_INT', null, '7', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'pessoa_empresa_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('229', 'empresa_id_INT', null, '7', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'pessoa_empresa_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('230', 'profissao_id_INT', null, '7', '', null, '11', null, '0', '0', '0', '', '10', 'id', 'pessoa_empresa_ibfk_4', 'CASCADE', 'SET NULL', '', '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('231', 'corporacao_id_INT', null, '7', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'pessoa_empresa_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('232', 'id', null, '85', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('233', 'pessoa_empresa_id_INT', null, '85', '', null, '11', null, '0', '0', '0', '', '7', 'id', 'pessoa_empresa_rotina_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('234', 'semana_INT', null, '85', '', null, '4', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('235', 'dia_semana_INT', null, '85', '', null, '4', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('236', 'corporacao_id_INT', null, '85', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'pessoa_empresa_rotina_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('237', 'id', null, '87', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('238', 'pessoa_id_INT', null, '87', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'pessoa_usuario_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'pessoa_usuario_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('239', 'corporacao_id_INT', null, '87', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'pessoa_usuario_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'pessoa_usuario_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('240', 'usuario_id_INT', null, '87', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'pessoa_usuario_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'pessoa_usuario_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('241', 'id', null, '9', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('242', 'pessoa_id_INT', null, '9', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'ponto_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('243', 'hora_TIME', null, '9', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('244', 'dia_DATE', null, '9', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('245', 'foto', null, '9', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('246', 'empresa_id_INT', null, '9', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'ponto_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('247', 'profissao_id_INT', null, '9', '', null, '11', null, '0', '0', '0', '', '10', 'id', 'ponto_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('248', 'is_entrada_BOOLEAN', null, '9', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('249', 'latitude_INT', null, '9', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('250', 'longitude_INT', null, '9', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('251', 'tipo_ponto_id_INT', null, '9', '', null, '11', null, '0', '0', '0', '', '82', 'id', 'ponto_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('252', 'corporacao_id_INT', null, '9', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'ponto_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('253', 'usuario_id_INT', null, '9', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'ponto_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('254', 'id', null, '468', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('255', 'ponto_id_INT', null, '468', '', null, '11', null, '0', '0', '0', '', '9', 'id', 'ponto_endereco_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('256', 'endereco_completo', null, '468', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('257', 'id', null, '10', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('258', 'nome', null, '10', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('259', 'nome_normalizado', null, '10', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_34677654', null, '0');
INSERT INTO `sistema_atributo` VALUES ('260', 'corporacao_id_INT', null, '10', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'profissao_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_34677654', null, '0');
INSERT INTO `sistema_atributo` VALUES ('261', 'id', null, '83', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('262', 'nome', null, '83', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('263', 'nome_normalizado', null, '83', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('264', 'corporacao_id_INT', null, '83', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'rede_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('265', 'id', null, '84', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('266', 'rede_id_INT', null, '84', '', null, '11', null, '0', '0', '0', '', '83', 'id', 'rede_empresa_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'rede_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('267', 'empresa_id_INT', null, '84', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'rede_empresa_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'rede_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('268', 'corporacao_id_INT', null, '84', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'rede_empresa_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('269', 'id', null, '89', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('270', 'titulo', null, '89', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('271', 'titulo_normalizado', null, '89', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('272', 'descricao', null, '89', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('273', 'descricao_normalizado', null, '89', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('274', 'pessoa_id_INT', null, '89', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'relatorio_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('275', 'empresa_id_INT', null, '89', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'relatorio_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('276', 'hora_TIME', null, '89', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('277', 'dia_DATE', null, '89', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('278', 'corporacao_id_INT', null, '89', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'relatorio_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('279', 'usuario_id_INT', null, '89', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'relatorio_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('280', 'id', null, '91', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('281', 'relatorio_id_INT', null, '91', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'relatorio_anexo_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('282', 'arquivo', null, '91', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('283', 'tipo_anexo_id_INT', null, '91', '', null, '11', null, '0', '0', '0', '', '90', 'id', 'relatorio_anexo_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('284', 'corporacao_id_INT', null, '91', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'relatorio_anexo_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('285', 'id', null, '86', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('286', 'ultima_dia_contagem_ciclo_DATE', null, '86', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('287', 'corporacao_id_INT', null, '86', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'rotina_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('288', 'id', null, '31', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('289', 'nome', null, '31', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'tag', null, '0');
INSERT INTO `sistema_atributo` VALUES ('290', 'tag', null, '31', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('291', 'id', null, '39', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('292', 'nome', null, '39', '', null, '30', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('293', 'id', null, '445', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('294', 'nome', null, '445', '', null, '128', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_irtu', null, '0');
INSERT INTO `sistema_atributo` VALUES ('295', 'sistema_tabela_id_INT', null, '445', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sa_sistema_tabela_FK', 'CASCADE', 'SET NULL', '', '1', 'nome_irtu', null, '0');
INSERT INTO `sistema_atributo` VALUES ('296', 'tipo_sql', null, '445', '', null, '15', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('297', 'tamanho_INT', null, '445', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('298', 'decimal_INT', null, '445', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('299', 'not_null_BOOLEAN', null, '445', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('300', 'primary_key_BOOLEAN', null, '445', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('301', 'auto_increment_BOOLEAN', null, '445', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('302', 'valor_default', null, '445', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('303', 'fk_sistema_tabela_id_INT', null, '445', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_atributo_FK_622131348', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('304', 'atributo_fk', null, '445', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('305', 'fk_nome', null, '445', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('306', 'update_tipo_fk', null, '445', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('307', 'delete_tipo_fk', null, '445', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('308', 'label', null, '445', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('309', 'unique_BOOLEAN', null, '445', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('310', 'unique_nome', null, '445', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('311', 'seq_INT', null, '445', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('312', 'excluido_BOOLEAN', null, '445', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('313', 'id', null, '448', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('314', 'nome', null, '448', '', null, '250', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('315', 'valor_string', null, '448', '', null, '250', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('316', 'valor_INT', null, '448', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('317', 'valor_DATETIME', null, '448', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('318', 'valor_DATE', null, '448', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('319', 'valor_FLOAT', null, '448', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('320', 'valor_BOOLEAN', null, '448', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('321', 'id', null, '469', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('322', 'projetos_versao_INT', null, '469', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('323', 'data_homologacao_DATETIME', null, '469', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('324', 'data_producao_DATETIME', null, '469', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('325', 'id', null, '470', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('326', 'sistema_tabela_id_INT', null, '470', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_registro_sincronizador_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('327', 'id_tabela_INT', null, '470', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('328', 'tipo_operacao_banco_id_INT', null, '470', '', null, '2', null, '0', '0', '0', '', '171', 'id', 'sistema_registro_sincronizador_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('329', 'corporacao_id_INT', null, '470', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_registro_sincronizador_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('330', 'is_from_android_BOOLEAN', null, '470', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('331', 'imei', null, '470', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('332', 'sistema_produto_INT', null, '470', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('333', 'sistema_projetos_versao_INT', null, '470', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('334', 'usuario_id_INT', null, '470', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'sistema_registro_sincronizador_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('335', 'id', null, '139', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('336', 'nome', null, '139', '', null, '128', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('337', 'data_modificacao_estrutura_DATETIME', null, '139', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('338', 'frequencia_sincronizador_INT', null, '139', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('339', 'banco_mobile_BOOLEAN', null, '139', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('340', 'banco_web_BOOLEAN', null, '139', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('341', 'transmissao_web_para_mobile_BOOLEAN', null, '139', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('342', 'transmissao_mobile_para_web_BOOLEAN', null, '139', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('343', 'tabela_sistema_BOOLEAN', null, '139', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('344', 'excluida_BOOLEAN', null, '139', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('345', 'atributos_chave_unica', null, '139', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('346', 'id', null, '343', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('347', 'nome', null, '343', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'sistema_tipo_download_arquivo_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('348', 'path', null, '343', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('349', 'id', null, '171', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('350', 'nome', null, '171', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_84756', null, '0');
INSERT INTO `sistema_atributo` VALUES ('351', 'id', null, '11', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('352', 'categoria_permissao_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '2', 'id', 'tarefa_ibfk_7', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('353', 'origem_pessoa_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'tarefa_ibfk_8', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('354', 'origem_empresa_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'tarefa_ibfk_15', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('355', 'origem_logradouro', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('356', 'origem_logradouro_normalizado', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('357', 'origem_numero', null, '11', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('358', 'origem_complemento', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('359', 'origem_complemento_normalizado', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('360', 'origem_cidade_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'tarefa_ibfk_14', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('361', 'origem_bairro_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '1', 'id', 'tarefa_ibfk_9', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('362', 'origem_latitude_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('363', 'origem_longitude_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('364', 'origem_latitude_real_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('365', 'origem_longitude_real_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('366', 'destino_pessoa_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'tarefa_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('367', 'destino_empresa_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'tarefa_ibfk_12', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('368', 'destino_logradouro', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('369', 'destino_logradouro_normalizado', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('370', 'destino_numero', null, '11', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('371', 'destino_complemento', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('372', 'destino_complemento_normalizado', null, '11', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('373', 'destino_cidade_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'tarefa_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('374', 'destino_bairro_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '1', 'id', 'tarefa_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('375', 'destino_latitude_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('376', 'destino_longitude_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('377', 'destino_latitude_real_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('378', 'destino_longitude_real_INT', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('379', 'tempo_estimado_carro_INT', null, '11', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('380', 'tempo_estimado_a_pe_INT', null, '11', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('381', 'distancia_estimada_carro_INT', null, '11', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('382', 'distancia_estimada_a_pe_INT', null, '11', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('383', 'is_realizada_a_pe_BOOLEAN', null, '11', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('384', 'inicio_data_programada_DATE', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('385', 'inicio_hora_programada_TIME', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('386', 'data_exibir_DATE', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('387', 'inicio_data_DATE', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('388', 'inicio_hora_TIME', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('389', 'fim_data_DATE', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('390', 'fim_hora_TIME', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('391', 'data_cadastro_DATE', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('392', 'hora_cadastro_TIME', null, '11', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('393', 'titulo', null, '11', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('394', 'titulo_normalizado', null, '11', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('395', 'descricao', null, '11', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('396', 'descricao_normalizado', null, '11', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('397', 'corporacao_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tarefa_ibfk_11', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('398', 'criado_pelo_usuario_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('399', 'veiculo_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '20', 'id', 'tarefa_ibfk_10', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('400', 'usuario_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_ibfk_13', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('401', 'veiculo_usuario_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '21', 'id', 'tarefa_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('402', 'id', null, '90', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('403', 'nome', null, '90', '', null, '30', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('404', 'id', null, '23', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('405', 'nome', null, '23', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('406', 'nome_normalizado', null, '23', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('407', 'corporacao_id_INT', null, '23', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tipo_documento_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('408', 'id', null, '12', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('409', 'nome', null, '12', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_93486ui', null, '0');
INSERT INTO `sistema_atributo` VALUES ('410', 'nome_normalizado', null, '12', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('411', 'corporacao_id_INT', null, '12', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tipo_empresa_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_93486ui', null, '0');
INSERT INTO `sistema_atributo` VALUES ('412', 'id', null, '82', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('413', 'nome', null, '82', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('414', 'id', null, '13', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('415', 'nome', null, '13', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('416', 'nome_normalizado', null, '13', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome2', null, '0');
INSERT INTO `sistema_atributo` VALUES ('417', 'sigla', null, '13', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('418', 'pais_id_INT', null, '13', '', null, '11', null, '0', '0', '0', '', '22', 'id', 'uf_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'nome2', null, '0');
INSERT INTO `sistema_atributo` VALUES ('419', 'corporacao_id_INT', null, '13', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'uf_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome2', null, '0');
INSERT INTO `sistema_atributo` VALUES ('420', 'id', null, '14', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('421', 'nome', null, '14', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('422', 'nome_normalizado', null, '14', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('423', 'email', null, '14', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'email', null, '0');
INSERT INTO `sistema_atributo` VALUES ('424', 'senha', null, '14', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('425', 'status_BOOLEAN', null, '14', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('426', 'pagina_inicial', null, '14', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('427', 'id', null, '15', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('428', 'usuario_id_INT', null, '15', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_categoria_permissao_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('429', 'categoria_permissao_id_INT', null, '15', '', null, '11', null, '0', '0', '0', '', '2', 'id', 'usuario_categoria_permissao_ibfk_8', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('430', 'corporacao_id_INT', null, '15', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_categoria_permissao_ibfk_9', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('431', 'id', null, '16', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('432', 'usuario_id_INT', null, '16', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_corporacao_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'usuario_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('433', 'corporacao_id_INT', null, '16', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_corporacao_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'usuario_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('434', 'status_BOOLEAN', null, '16', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('435', 'is_adm_BOOLEAN', null, '16', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('436', 'id', null, '471', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('437', 'usuario_id_INT', null, '471', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_empresa_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('438', 'empresa_id_INT', null, '471', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'usuario_empresa_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('439', 'corporacao_id_INT', null, '471', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_empresa_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('440', 'id', null, '17', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('441', 'data_DATE', null, '17', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('442', 'hora_TIME', null, '17', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('443', 'foto_interna', null, '17', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('444', 'foto_externa', null, '17', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('445', 'usuario_id_INT', null, '17', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_foto_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('446', 'corporacao_id_INT', null, '17', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_foto_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('447', 'id', null, '168', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('448', 'is_camera_interna_BOOLEAN', null, '168', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('449', 'periodo_segundo_INT', null, '168', '', null, '7', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('450', 'usuario_id_INT', null, '168', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_foto_configuracao_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('451', 'corporacao_id_INT', null, '168', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_foto_configuracao_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('452', 'id', null, '472', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('453', 'usuario_id_INT', null, '472', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_menu_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('454', 'area_menu', null, '472', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('455', 'corporacao_id_INT', null, '472', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_menu_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('456', 'id', null, '18', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('457', 'data_DATE', null, '18', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('458', 'hora_TIME', null, '18', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('459', 'veiculo_usuario_id_INT', null, '18', '', null, '11', null, '0', '0', '0', '', '21', 'id', 'usuario_posicao_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('460', 'usuario_id_INT', null, '18', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_posicao_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('461', 'latitude_INT', null, '18', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('462', 'longitude_INT', null, '18', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('463', 'velocidade_INT', null, '18', '', null, '5', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('464', 'foto_interna', null, '18', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('465', 'foto_externa', null, '18', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('466', 'corporacao_id_INT', null, '18', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_posicao_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('467', 'id', null, '473', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('468', 'usuario_id_INT', null, '473', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_privilegio_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('469', 'identificador_funcionalidade', null, '473', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('470', 'corporacao_id_INT', null, '473', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_privilegio_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('471', 'id', null, '19', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('472', 'status_BOOLEAN', null, '19', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('473', 'servico_id_INT', null, '19', '', null, '11', null, '0', '0', '0', '', '31', 'id', 'usuario_servico_FK_777465821', 'CASCADE', 'SET NULL', '', '1', 'servico_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('474', 'usuario_id_INT', null, '19', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_servico_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'servico_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('475', 'corporacao_id_INT', null, '19', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_servico_ibfk_9', 'CASCADE', 'SET NULL', '', '1', 'servico_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('476', 'id', null, '73', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('477', 'nome', null, '73', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'usuario_tipo_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('478', 'nome_visivel', null, '73', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('479', 'status_BOOLEAN', null, '73', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('480', 'pagina_inicial', null, '73', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('481', 'corporacao_id_INT', null, '73', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_tipo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'usuario_tipo_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('482', 'id', null, '75', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('483', 'usuario_id_INT', null, '75', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_tipo_corporacao_ibfk_2', 'CASCADE', 'SET NULL', '', '1', 'usuario_tipo_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('484', 'usuario_tipo_id_INT', null, '75', '', null, '11', null, '0', '0', '0', '', '73', 'id', 'usuario_tipo_corporacao_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('485', 'corporacao_id_INT', null, '75', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_tipo_corporacao_ibfk_3', 'CASCADE', 'SET NULL', '', '1', 'usuario_tipo_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('486', 'id', null, '474', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('487', 'usuario_tipo_id_INT', null, '474', '', null, '11', null, '0', '0', '0', '', '73', 'id', 'usuario_tipo_menu_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('488', 'area_menu', null, '474', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('489', 'corporacao_id_INT', null, '474', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_tipo_menu_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('490', 'id', null, '475', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('491', 'usuario_tipo_id_INT', null, '475', '', null, '11', null, '0', '0', '0', '', '73', 'id', 'usuario_tipo_privilegio_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('492', 'identificador_funcionalidade', null, '475', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('493', 'corporacao_id_INT', null, '475', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_tipo_privilegio_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('494', 'id', null, '20', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('495', 'placa', null, '20', '', null, '20', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'placa', null, '0');
INSERT INTO `sistema_atributo` VALUES ('496', 'modelo_id_INT', null, '20', '', null, '11', null, '0', '0', '0', '', '8', 'id', 'veiculo_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('497', 'corporacao_id_INT', null, '20', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'veiculo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'placa', null, '0');
INSERT INTO `sistema_atributo` VALUES ('498', 'id', null, '446', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('499', 'entrada_BOOLEAN', null, '446', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('500', 'data_DATETIME', null, '446', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('501', 'latitude_INT', null, '446', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('502', 'longitude_INT', null, '446', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('503', 'corporacao_id_INT', null, '446', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'veiculo_registro_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('504', 'veiculo_usuario_id_INT', null, '446', '', null, '11', null, '0', '0', '0', '', '21', 'id', 'veiculo_registro_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('505', 'id', null, '21', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('506', 'usuario_id_INT', null, '21', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'veiculo_usuario_ibfk_5', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('507', 'data_DATE', null, '21', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('508', 'hora_TIME', null, '21', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('509', 'veiculo_id_INT', null, '21', '', null, '11', null, '0', '0', '0', '', '20', 'id', 'veiculo_usuario_ibfk_4', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('510', 'is_ativo_BOOLEAN', null, '21', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('511', 'corporacao_id_INT', null, '21', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'veiculo_usuario_ibfk_6', 'CASCADE', 'SET NULL', '', '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('512', 'id', null, '464', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('513', 'network_id', null, '464', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('514', 'rssi', null, '464', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('515', 'ssid', null, '464', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('516', 'usuario', null, '464', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('517', 'senha', null, '464', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('518', 'ip', null, '464', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('519', 'link_speed', null, '464', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('520', 'mac_address', null, '464', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('521', 'frequency', null, '464', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('522', 'bssid', null, '464', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('523', 'empresa_id_INT', null, '464', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'wifi_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('524', 'pessoa_id_INT', null, '464', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'wifi_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('525', 'data_DATETIME', null, '464', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('526', 'contador_verificacao_INT', null, '464', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('527', 'corporacao_id_INT', null, '464', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'wifi_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('528', 'id', null, '465', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('529', 'wifi_id_INT', null, '465', '', null, '11', null, '0', '0', '0', '', '464', 'id', 'wifi_registro_FK_898468018', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('530', 'data_inicio_DATETIME', null, '465', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('531', 'data_fim_DATETIME', null, '465', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('532', 'corporacao_id_INT', null, '465', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'wifi_registro_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('533', 'usuario_id_INT', null, '465', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'wifi_registro_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('534', 'id', null, '450', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('535', 'last_id_sincronizador_php_INT', null, '450', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('536', 'corporacao_id_INT', null, '450', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'corporacao_sincronizador_php_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('537', 'nome_exibicao', null, '139', '', null, '128', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('538', 'id', null, '451', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('539', 'data_DATE', null, '451', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('540', 'hora_TIME', null, '451', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('541', 'veiculo_usuario_id_INT', null, '451', '', null, '11', null, '0', '0', '0', '', '21', 'id', 'usuario_posicao_rastreamento_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('542', 'usuario_id_INT', null, '451', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_posicao_rastreamento_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('543', 'latitude_INT', null, '451', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('544', 'longitude_INT', null, '451', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('545', 'velocidade_INT', null, '451', '', null, '5', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('546', 'foto_interna', null, '451', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('547', 'foto_externa', null, '451', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('548', 'corporacao_id_INT', null, '451', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_posicao_rastreamento_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('549', 'id', null, '81', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('550', 'nome', null, '81', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('551', 'nome_normalizado', null, '81', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('552', 'data_lancamento_DATE', null, '81', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('553', 'link_download', null, '81', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('554', 'total_dia_trial_INT', null, '81', '', null, '5', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('555', 'desconto_FLOAT', null, '57', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('556', 'desconto_FLOAT', null, '68', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('557', 'id', null, '489', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('558', 'nome', null, '489', '', null, '256', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('559', 'descricao', null, '489', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('560', 'seq_INT', null, '489', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('561', 'id', null, '484', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('562', 'nome', null, '484', '', null, '252', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('563', 'id', null, '462', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('564', 'sistema_tabela_id_INT', null, '462', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('565', 'id_tabela_INT', null, '462', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('566', 'sistema_tipo_operacao_id_INT', null, '462', '', null, '11', null, '0', '0', '0', '', '171', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('567', 'usuario_id_INT', null, '462', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('568', 'corporacao_id_INT', null, '462', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('569', 'imei', null, '462', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('570', 'sistema_produto_INT', null, '462', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('571', 'sistema_projetos_versao_INT', null, '462', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('572', 'id_tabela_web_INT', null, '462', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('573', 'locale', null, '444', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('574', 'desconto_FLOAT', null, '65', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('575', 'dia_semana_INT', null, '441', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('576', 'nome_exibicao', null, '445', '', null, '128', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('577', 'tipo_sql_ficticio', null, '445', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('578', 'id', null, '485', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('579', 'nome', null, '485', '', null, '256', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('580', 'descricao', null, '485', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('581', 'sistema_lista_id_INT', null, '485', '', null, '11', null, '0', '0', '0', '', '488', 'id', 'sistema_campo_FK_927398682', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('582', 'sistema_tipo_campo_id_INT', null, '485', '', null, '11', null, '0', '0', '0', '', '489', 'id', 'sistema_campo_FK_760070801', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('583', 'id', null, '486', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('584', 'sistema_campo_id_INT', null, '486', '', null, '11', null, '0', '0', '0', '', '485', 'id', 'sistema_campo_atributo_FK_405761719', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('585', 'sistema_atributo_id_INT', null, '486', '', null, '11', null, '0', '0', '0', '', '445', 'id', 'sistema_campo_atributo_FK_854766846', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('586', 'seq_INT', null, '486', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('587', 'id', null, '487', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('588', 'nome', null, '487', '', null, '215', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('589', 'id', null, '488', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('590', 'nome', null, '488', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('591', 'descricao', null, '488', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('592', 'sistema_tabela_id_INT', null, '488', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_lista_FK_318206787', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('593', 'sistema_estado_lista_id_INT', null, '488', '', null, '11', null, '0', '0', '0', '', '487', 'id', 'sistema_lista_FK_14404296', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('594', 'ultima_alteracao_usuario_id_INT', null, '488', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'sistema_lista_FK_571807861', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('595', 'ultima_alteracao_DATETIME', null, '488', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('596', 'criacao_DATETIME', null, '488', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('597', 'criador_usuario_id_INT', null, '488', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'sistema_lista_FK_563171387', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('598', 'id', null, '495', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('599', 'tabela', null, '495', '', null, '128', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('600', 'id_ultimo_INT', null, '495', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('601', 'id', null, '477', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('602', 'nome', null, '477', '', null, '150', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('603', 'tela_estado_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '491', 'id', 'tela_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('604', 'tela_tipo_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '493', 'id', 'tela_FK_119079589', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('605', 'is_android_BOOLEAN', null, '477', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('606', 'is_web_BOOLEAN', null, '477', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('607', 'configuracao_json', null, '477', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('608', 'data_criacao_DATETIME', null, '477', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('609', 'data_modificacao_DATETIME', null, '477', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('610', 'modificacao_usuario_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tela_FK_465026855', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('611', 'cricacao_usuario_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tela_FK_720916748', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('612', 'ativa_BOOLEAN', null, '477', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('613', 'permissao_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '46', 'id', 'tela_FK_137664795', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('614', 'area_menu_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '484', 'id', 'tela_FK_272827148', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('615', 'corporacao_id_INT', null, '477', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tela_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('616', 'id', null, '490', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('617', 'nome', null, '490', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('618', 'configuracao_json', null, '490', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('619', 'sistema_campo_id_INT', null, '490', '', null, '11', null, '0', '0', '0', '', '485', 'id', 'tela_componente_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('620', 'tela_id_INT', null, '490', '', null, '11', null, '0', '0', '0', '', '477', 'id', 'campo_FK_222290039', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('621', 'tela_sistema_lista_id_INT', null, '490', '', null, '11', null, '0', '0', '0', '', '492', 'id', 'tela_componente_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('622', 'seq_INT', null, '490', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('623', 'tela_tipo_componente_id_INT', null, '490', '', null, '11', null, '0', '0', '0', '', '494', 'id', 'tela_componente_FK_168670654', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('624', 'corporacao_id_INT', null, '490', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tela_componente_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('625', 'id', null, '491', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('626', 'nome', null, '491', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('627', 'id', null, '492', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('628', 'tela_id_INT', null, '492', '', null, '11', null, '0', '0', '0', '', '477', 'id', 'tela_sistema_lista_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('629', 'sistema_lista_id_INT', null, '492', '', null, '11', null, '0', '0', '0', '', '488', 'id', 'tela_sistema_lista_FK_907806397', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('630', 'is_principal_BOOLEAN', null, '492', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('631', 'tipo_cardinalidade_id_INT', null, '492', '', null, '11', null, '0', '0', '0', '', '482', 'id', 'tela_sistema_lista_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('632', 'corporacao_id_INT', null, '492', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tela_sistema_lista_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('633', 'id', null, '493', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('634', 'nome', null, '493', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('635', 'seq_INT', null, '493', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('636', 'id', null, '494', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('637', 'nome', null, '494', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('638', 'seq_INT', null, '494', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('639', 'id', null, '482', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('640', 'nome', null, '482', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('641', 'seq_INT', null, '482', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('642', 'area_menu_id_INT', null, '474', '', null, '11', null, '0', '0', '0', '', '484', 'id', 'usuario_tipo_menu_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('643', 'quilometragem_INT', null, '446', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('644', 'observacao', null, '446', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('645', 'desconto_FLOAT', null, '60', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('646', 'desconto_FLOAT', null, '62', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('647', 'id', null, '459', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('648', 'pai_sistema_grafo_hierarquia_banco_id_INT', null, '459', '', null, '11', null, '0', '0', '0', '', '453', 'id', 'sistema_aresta_grafo_hierarquia_banco_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('649', 'filho_sistema_grafo_hierarquia_banco_id_INT', null, '459', '', null, '11', null, '0', '0', '0', '', '453', 'id', 'sistema_aresta_grafo_hierarquia_banco_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('650', 'id', null, '238', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('651', 'nome', null, '238', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('652', 'data_inicio_estrutura_DATETIME', null, '238', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('653', 'data_fim_estrutura_DATETIME', null, '238', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('654', 'id', null, '458', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('655', 'dia_DATE', null, '458', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('656', 'corporacao_id_INT', null, '458', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_corporacao_sincronizador_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('657', 'id', null, '461', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('658', 'dia_DATE', null, '461', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('659', 'sistema_corporacao_sincronizador_id_INT', null, '461', '', null, '11', null, '0', '0', '0', '', '458', 'id', 'sistema_corporacao_sincronizador_tabela_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('660', 'sistema_tabela_id_INT', null, '461', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_corporacao_sincronizador_tabela_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('661', 'id', null, '496', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('662', 'id_crud_INT', null, '496', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('663', 'id_crud_mobile_INT', null, '496', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('664', 'id_estado_crud_INT', null, '496', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('665', 'id_estado_crud_mobile_INT', null, '496', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('666', 'sistema_tipo_operacao_banco_id_INT', null, '496', '', null, '11', null, '0', '0', '0', '', '171', 'id', 'sistema_crud_estado_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('667', 'corporacao_id_INT', null, '496', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_crud_estado_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('668', 'id', null, '447', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('669', 'nome', null, '447', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('670', 'path', null, '447', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('671', 'id', null, '453', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('672', 'grau_INT', null, '453', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('673', 'numero_dependentes_INT', null, '453', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('674', 'sistema_tabela_id_INT', null, '453', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_grafo_hierarquia_banco_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('675', 'id', null, '454', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('676', 'sistema_tabela_id_INT', null, '454', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_historico_sincronizador_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('677', 'id_tabela_INT', null, '454', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('678', 'sistema_tipo_operacao_id_INT', null, '454', '', null, '11', null, '0', '0', '0', '', '171', 'id', 'sistema_historico_sincronizador_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('679', 'data_DATETIME', null, '454', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('680', 'usuario_INT', null, '454', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('681', 'corporacao_id_INT', null, '454', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_historico_sincronizador_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('682', 'id', null, '455', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('683', 'operacao_sistema_mobile_id_INT', null, '455', '', null, '11', null, '0', '0', '0', '', '457', 'id', 'sistema_operacao_crud_aleatorio_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('684', 'total_insercao_INT', null, '455', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('685', 'porcentagem_remocao_FLOAT', null, '455', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('686', 'porcentagem_edicao_FLOAT', null, '455', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('687', 'sincronizar_BOOLEAN', null, '455', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('688', 'id', null, '460', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('689', 'path_script_sql_banco', null, '460', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('690', 'observacao', null, '460', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('691', 'operacao_sistema_mobile_id_INT', null, '460', '', null, '11', null, '0', '0', '0', '', '457', 'id', 'sistema_operacao_download_banco_mobile_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('692', 'popular_tabela_BOOLEAN', null, '460', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('693', 'drop_tabela_BOOLEAN', null, '460', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('694', 'destino_banco_id_INT', null, '460', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('695', 'id', null, '456', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('696', 'xml_script_sql_banco_ARQUIVO', null, '456', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('697', 'operacao_sistema_mobile_id_INT', null, '456', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('698', 'id', null, '457', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('699', 'tipo_operacao_sistema_id_INT', null, '457', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('700', 'estado_operacao_sistema_mobile_id_INT', null, '457', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('701', 'mobile_identificador_id_INT', null, '457', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('702', 'data_abertura_DATETIME', null, '457', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('703', 'data_processamento_DATETIME', null, '457', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('704', 'data_conclusao_DATETIME', null, '457', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('705', 'id', null, '449', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('706', 'classe', null, '449', '', null, '300', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('707', 'funcao', null, '449', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('708', 'linha_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('709', 'nome_arquivo', null, '449', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('710', 'identificador_erro', null, '449', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('711', 'descricao', null, '449', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('712', 'stacktrace', null, '449', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('713', 'data_visualizacao_DATETIME', null, '449', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('714', 'data_ocorrida_DATETIME', null, '449', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('715', 'sistema_tipo_log_erro_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('716', 'sistema_projetos_versao_produto_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('717', 'id_usuario_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('718', 'id_corporacao_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('719', 'sistema_log_identificador_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('720', 'sistema_projetos_versao_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('721', 'mobile_conectado_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('722', 'operacao_sistema_mobile_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('723', 'script_comando_banco_id_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('724', 'total_ocorrencia_INT', null, '449', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('725', 'sincronizado_BOOLEAN', null, '449', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('726', 'id', null, '497', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('727', 'crud', null, '497', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('728', 'sistema_registro_sincronizador_id_INT', null, '497', '', null, '11', null, '0', '0', '0', '', '462', 'id', 'sistema_registro_historico_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('729', 'sistema_tabela_id_INT', null, '497', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_registro_historico_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('730', 'id_tabela_INT', null, '497', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('731', 'corporacao_id_INT', null, '497', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_registro_historico_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('732', 'id', null, '463', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('733', 'sistema_tabela_id_INT', null, '463', '', null, '11', null, '0', '0', '0', '', '139', 'id', 'sistema_registro_sincronizador_web_para_android_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('734', 'id_tabela_INT', null, '463', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('735', 'sistema_tipo_operacao_id_INT', null, '463', '', null, '11', null, '0', '0', '0', '', '171', 'id', 'sistema_registro_sincronizador_web_para_android_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('736', 'corporacao_id_INT', null, '463', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_registro_sincronizador_web_para_android_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('737', 'imei', null, '463', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('738', 'sistema_produto_INT', null, '463', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('739', 'sistema_projetos_versao_INT', null, '463', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('740', 'id', null, '452', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('741', 'arquivo', null, '452', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('742', 'is_zip_BOOLEAN', null, '452', '', null, '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('743', 'corporacao_id_INT', null, '452', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_sincronizador_arquivo_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('744', 'id', null, '317', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('745', 'nome', null, '317', '', null, '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('746', 'id', null, '344', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('747', 'nome', null, '344', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('748', 'id', null, '337', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('749', 'tag', null, '337', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('750', 'mensagem', null, '337', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('751', 'sistema_tipo_usuario_mensagem_id_INT', null, '337', '', null, '11', null, '0', '0', '0', '', '344', 'id', 'sistema_usuario_mensagem_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('752', 'usuario_id_INT', null, '337', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'sistema_usuario_mensagem_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('753', 'corporacao_id_INT', null, '337', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'sistema_usuario_mensagem_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('754', 'id', null, '442', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('755', 'tarefa_id_INT', null, '442', '', null, '11', null, '0', '0', '0', '', '11', 'id', 'tarefa_acao_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('756', 'hora_TIME', null, '442', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('757', 'dia_DATE', null, '442', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('758', 'usuario_id_INT', null, '442', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_acao_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('759', 'is_entrada_BOOLEAN', null, '442', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('760', 'latitude_FLOAT', null, '442', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('761', 'longitude_FLOAT', null, '442', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('762', 'corporacao_id_INT', null, '442', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tarefa_acao_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('763', 'id', null, '498', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('764', 'nome', null, '498', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('765', 'empresa_id_INT', null, '498', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_equipe_FK_652496338', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('766', 'corporacao_id_INT', null, '498', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_equipe_FK_545715332', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('767', 'id_omega_INT', null, '56', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('768', 'id_omega_INT', null, '61', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('769', 'acrescimo_FLOAT', null, '68', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('770', 'id', null, '499', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('771', 'empresa_equipe_id_INT', null, '499', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('772', 'pessoa_empresa_id_INT', null, '499', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('773', 'corporacao_id_INT', null, '499', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('774', 'empresa_equipe_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '498', 'id', 'tarefa_FK_603546143', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('775', 'data_login_SEC', null, '466', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('776', 'data_login_OFFSEC', null, '466', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('777', 'data_logout_SEC', null, '466', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('778', 'data_logout_OFFSEC', null, '466', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('779', 'cadastro_SEC', null, '1', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('780', 'cadastro_OFFSEC', null, '1', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('781', 'cadastro_SEC', null, '5', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('782', 'cadastro_OFFSEC', null, '5', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('783', 'data_SEC', null, '54', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('784', 'data_OFFSEC', null, '54', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('785', 'data_SEC', null, '55', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('786', 'data_OFFSEC', null, '55', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('787', 'cadastro_SEC', null, '498', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('788', 'cadastro_OFFSEC', null, '498', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('789', 'cadastro_SEC', null, '56', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('790', 'cadastro_OFFSEC', null, '56', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('791', 'cadastro_SEC', null, '61', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('792', 'cadastro_OFFSEC', null, '61', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('793', 'data_SEC', null, '68', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('794', 'data_OFFSEC', null, '68', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('795', 'data_SEC', null, '69', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('796', 'data_OFFSEC', null, '69', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('797', 'data_SEC', null, '441', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('798', 'data_OFFSEC', null, '441', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('799', 'cadastro_SEC', null, '6', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('800', 'cadastro_OFFSEC', null, '6', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('801', 'data_SEC', null, '9', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('802', 'data_OFFSEC', null, '9', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('803', 'data_SEC', null, '89', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('804', 'data_OFFSEC', null, '89', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('805', 'ultima_dia_contagem_ciclo_data_SEC', null, '86', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('806', 'ultima_dia_contagem_ciclo_data_OFFSEC', null, '86', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('807', 'ultima_alteracao_SEC', null, '488', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('808', 'ultima_alteracao_OFFSEC', null, '488', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('809', 'criacao_SEC', null, '488', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('810', 'criacao_OFFSEC', null, '488', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('811', 'valor_SEC', null, '448', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('812', 'valor_OFFSEC', null, '448', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('813', 'id', null, '597', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('814', 'nome', null, '597', '', null, '250', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('815', 'valor_string', null, '597', '', null, '250', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('816', 'valor_INT', null, '597', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('817', 'valor_DATETIME', null, '597', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('818', 'valor_SEC', null, '597', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('819', 'valor_OFFSEC', null, '597', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('820', 'valor_DATE', null, '597', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('821', 'valor_FLOAT', null, '597', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('822', 'valor_BOOLEAN', null, '597', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('823', 'corporacao_id_INT', null, '597', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'spgc_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, null);
INSERT INTO `sistema_atributo` VALUES ('824', 'inicio_hora_programada_SEC', null, '11', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('825', 'inicio_hora_programada_OFFSEC', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('826', 'data_exibir_SEC', null, '11', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('827', 'data_exibir_OFFSEC', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('828', 'inicio_SEC', null, '11', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('829', 'inicio_OFFSEC', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('830', 'fim_SEC', null, '11', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('831', 'fim_OFFSEC', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('832', 'cadastro_SEC', null, '11', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('833', 'cadastro_OFFSEC', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('834', 'data_criacao_SEC', null, '477', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('835', 'data_criacao_OFFSEC', null, '477', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('836', 'data_modificacao_SEC', null, '477', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('837', 'data_modificacao_OFFSEC', null, '477', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('838', 'cadastro_SEC', null, '14', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('839', 'cadastro_OFFSEC', null, '14', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('840', 'cadastro_SEC', null, '16', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('841', 'cadastro_OFFSEC', null, '16', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('842', 'cadastro_SEC', null, '17', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('843', 'cadastro_OFFSEC', null, '17', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('844', 'cadastro_SEC', null, '18', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('845', 'cadastro_OFFSEC', null, '18', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('846', 'fonte_informacao_INT', null, '18', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('847', 'cadastro_SEC', null, '20', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('848', 'cadastro_OFFSEC', null, '20', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('849', 'data_SEC', null, '446', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('850', 'data_OFFSEC', null, '446', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('851', 'cadastro_SEC', null, '21', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('852', 'cadastro_OFFSEC', null, '21', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('853', 'data_SEC', null, '464', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('854', 'data_OFFSEC', null, '464', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('855', 'data_inicio_SEC', null, '465', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('856', 'data_inicio_OFFSEC', null, '465', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('857', 'data_fim_SEC', null, '465', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('858', 'data_fim_OFFSEC', null, '465', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('859', 'id_sincronizador_mobile_INT', null, '496', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('860', 'id_tabela_mobile_INT', null, '454', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('861', 'id', null, '598', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('862', 'operacao_sistema_mobile_id_INT', null, '598', '', null, '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('863', 'url_json_operacoes_web', null, '598', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('864', 'sincronizar_BOOLEAN', null, '598', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('865', 'id_tabela_mobile_INT', null, '497', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('866', 'id_tabela_mobile_INT', null, '462', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('867', 'criado_em_DATETIME', null, '462', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('868', 'sincronizando_BOOLEAN', null, '462', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('869', 'id', null, '599', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('870', 'nome', null, '599', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('871', 'id', null, '600', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('872', 'pessoa_id_INT', null, '600', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'api_id_FK_814331055', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('873', 'usuario_id_INT', null, '600', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'api_id_FK_701324463', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('874', 'empresa_id_INT', null, '600', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'api_id_FK_867980957', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('875', 'chave', null, '600', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('876', 'identificador', null, '600', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('877', 'cadastro_SEC', null, '600', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('878', 'cadastro_OFFSEC', null, '600', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('879', 'corporacao_id_INT', null, '600', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'api_id_FK_830413819', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('880', 'id', null, '601', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('881', 'identificador', null, '601', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('882', 'nome', null, '601', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('883', 'nome_normalizado', null, '601', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('884', 'descricao', null, '601', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('885', 'empresa_id_INT', null, '601', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'atividade_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('886', 'atividade_unidade_medida_id_INT', null, '601', '', null, '11', null, '0', '0', '0', '', '604', 'id', 'atividade_FK_219207763', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('887', 'prazo_entrega_dia_INT', null, '601', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('888', 'duracao_horas_INT', null, '601', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('889', 'id_omega_INT', null, '601', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('890', 'cadastro_SEC', null, '601', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('891', 'cadastro_OFFSEC', null, '601', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('892', 'corporacao_id_INT', null, '601', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'atividade_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('893', 'relatorio_id_INT', null, '601', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'atividade_FK_411010742', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('894', 'id', null, '602', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('895', 'nome', null, '602', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_098787', null, '0');
INSERT INTO `sistema_atributo` VALUES ('896', 'nome_normalizado', null, '602', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('897', 'corporacao_id_INT', null, '602', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'atividade_tipo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_098787', null, '0');
INSERT INTO `sistema_atributo` VALUES ('898', 'id', null, '603', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('899', 'atividade_tipo_id_INT', null, '603', '', null, '11', null, '0', '0', '0', '', '602', 'id', 'atividade_tipos_FK_423400879', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('900', 'atividade_id_INT', null, '603', '', null, '11', null, '0', '0', '0', '', '601', 'id', 'atividade_tipos_FK_996185303', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('901', 'corporacao_id_INT', null, '603', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'atividade_tipos_FK_789062500', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('902', 'id', null, '604', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('903', 'nome', null, '604', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('904', 'corporacao_id_INT', null, '604', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'atividade_unidade_medida_FK_833953858', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('905', 'id', null, '605', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('906', 'valor_FLOAT', null, '605', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('907', 'empresa_id_INT', null, '605', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'despesa_FK_685638428', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('908', 'vencimento_SEC', null, '605', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('909', 'vencimento_OFFSEC', null, '605', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('910', 'pagamento_SEC', null, '605', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('911', 'pagamento_OFFSEC', null, '605', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('912', 'despesa_cotidiano_id_INT', null, '605', '', null, '11', null, '0', '0', '0', '', '606', 'id', 'despesa_FK_939453125', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('913', 'valor_pagamento_FLOAT', null, '605', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('914', 'cadastro_usuario_id_INT', null, '605', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'despesa_FK_777130127', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('915', 'pagamento_usuario_id_INT', null, '605', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'despesa_FK_818298340', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('916', 'protocolo_INT', null, '605', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('917', 'corporacao_id_INT', null, '605', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'despesa_FK_352508545', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('918', 'id', null, '606', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('919', 'despesa_da_empresa_id_INT', null, '606', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'despesa_cotidiano_FK_33874511', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('920', 'despesa_do_usuario_id_INT', null, '606', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'despesa_cotidiano_FK_475311279', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('921', 'cadastro_usuario_id_INT', null, '606', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'despesa_cotidiano_FK_201293945', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('922', 'id_tipo_despesa_cotidiano_INT', null, '606', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('923', 'parametro_INT', null, '606', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('924', 'parametro_OFFSEC', null, '606', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('925', 'parametro_SEC', null, '606', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('926', 'parametro_json', null, '606', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('927', 'valor_FLOAT', null, '606', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('928', 'data_limite_cotidiano_SEC', null, '606', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('929', 'data_limite_cotidiano_OFFSEC', null, '606', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('930', 'corporacao_id_INT', null, '606', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'despesa_cotidiano_FK_755279541', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('931', 'relatorio_id_INT', null, '5', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'empresa_FK_690795899', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('932', 'ind_email_valido_BOOLEAN', null, '5', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('933', 'ind_celular_valido_BOOLEAN', null, '5', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('934', 'id', null, '607', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('935', 'empresa_id_INT', null, '607', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_atividade_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('936', 'prazo_entrega_dia_INT', null, '607', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('937', 'duracao_horas_INT', null, '607', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('938', 'cadastro_SEC', null, '607', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('939', 'cadastro_OFFSEC', null, '607', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('940', 'preco_custo_FLOAT', null, '607', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('941', 'preco_venda_FLOAT', null, '607', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('942', 'atividade_id_INT', null, '607', '', null, '11', null, '0', '0', '0', '', '601', 'id', 'empresa_atividade_FK_124084472', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('943', 'corporacao_id_INT', null, '607', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_atividade_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('944', 'id', null, '608', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('945', 'empresa_compra_id_INT', null, '608', '', null, '11', null, '0', '0', '0', '', '54', 'id', 'empresa_atividade_compra_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('946', 'quantidade_FLOAT', null, '608', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('947', 'valor_total_FLOAT', null, '608', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('948', 'desconto_FLOAT', null, '608', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('949', 'descricao', null, '608', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('950', 'atividade_id_INT', null, '608', '', null, '11', null, '0', '0', '0', '', '601', 'id', 'empresa_atividade_compra_FK_668334961', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('951', 'empresa_id_INT', null, '608', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_atividade_compra_FK_537750244', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('952', 'corporacao_id_INT', null, '608', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_atividade_compra_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('953', 'id', null, '609', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('954', 'empresa_venda_id_INT', null, '609', '', null, '11', null, '0', '0', '0', '', '68', 'id', 'empresa_atividade_venda_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('955', 'quantidade_FLOAT', null, '609', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('956', 'valor_total_FLOAT', null, '609', '', null, null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('957', 'desconto_FLOAT', null, '609', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('958', 'corporacao_id_INT', null, '609', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_atividade_venda_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('959', 'cadastro_SEC', null, '609', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('960', 'cadastro_OFFSEC', null, '609', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('961', 'atividade_id_INT', null, '609', '', null, '11', null, '0', '0', '0', '', '601', 'id', 'empresa_atividade_venda_FK_500762939', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('962', 'empresa_id_INT', null, '609', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_atividade_venda_FK_194274902', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('963', 'vencimento_SEC', null, '54', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('964', 'vencimento_OFFSEC', null, '54', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('965', 'relatorio_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'empresa_compra_FK_584411621', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('966', 'registro_estado_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'empresa_compra_FK_608856201', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('967', 'registro_estado_corporacao_id_INT', null, '54', '', null, '11', null, '0', '0', '0', '', '628', 'id', 'empresa_compra_FK_382690430', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('968', 'fechamento_SEC', null, '54', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('969', 'fechamento_OFFSEC', null, '54', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('970', 'valor_pago_FLOAT', null, '54', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('971', 'desconto_FLOAT', null, '54', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('972', 'protocolo_INT', null, '54', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('973', 'pagamento_SEC', null, '55', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('974', 'pagamento_OFFSEC', null, '55', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('975', 'preco_custo_FLOAT', null, '56', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('976', 'preco_venda_FLOAT', null, '56', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('977', 'estoque_atual_INT', null, '56', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('978', 'estoque_minimo_INT', null, '56', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('979', 'prazo_reposicao_estoque_dias_INT', null, '56', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('980', 'codigo_barra', null, '56', '', null, '13', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('981', 'produto_id_INT', null, '56', '', null, '11', null, '0', '0', '0', '', '620', 'id', 'empresa_produto_FK_384552002', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('982', 'produto_id_INT', null, '57', '', null, '11', null, '0', '0', '0', '', '620', 'id', 'empresa_produto_compra_FK_968383790', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('983', 'empresa_id_INT', null, '57', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_produto_compra_FK_618072510', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('984', 'descricao', null, '57', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('985', 'produto_id_INT', null, '60', '', null, '11', null, '0', '0', '0', '', '620', 'id', 'empresa_produto_venda_FK_420928955', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('986', 'descricao', null, '60', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('987', 'cadastro_SEC', null, '60', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('988', 'cadastro_OFFSEC', null, '60', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('989', 'empresa_id_INT', null, '60', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_produto_venda_FK_367370605', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('990', 'registro_estado_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'empresa_venda_FK_985656739', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('991', 'registro_estado_corporacao_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '628', 'id', 'empresa_venda_FK_407073975', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('992', 'relatorio_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'empresa_venda_FK_402862549', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('993', 'descricao', null, '68', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('994', 'vencimento_SEC', null, '68', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('995', 'vencimento_OFFSEC', null, '68', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('996', 'fechamento_SEC', null, '68', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('997', 'fechamento_OFFSEC', null, '68', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('998', 'valor_pago_FLOAT', null, '68', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('999', 'mesa_id_INT', null, '68', '', null, '11', null, '0', '0', '0', '', '613', 'id', 'empresa_venda_FK_648132324', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1000', 'identificador', null, '68', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1001', 'protocolo_INT', null, '68', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1002', 'pagamento_SEC', null, '69', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1003', 'pagamento_OFFSEC', null, '69', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1004', 'seq_INT', null, '69', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1005', 'id', null, '610', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1006', 'titulo', null, '610', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1007', 'template_json', null, '610', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1008', 'produto_id_INT', null, '610', '', null, '11', null, '0', '0', '0', '', '620', 'id', 'empresa_venda_template_FK_418518066', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1009', 'empresa_id_INT', null, '610', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'empresa_venda_template_FK_963958741', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1010', 'corporacao_id_INT', null, '610', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'empresa_venda_template_FK_776367188', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1011', 'id', null, '611', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1012', 'titulo', null, '611', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1013', 'mensagem', null, '611', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1014', 'remetente_usuario_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'mensagem_FK_646789551', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1015', 'tipo_mensagem_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '611', 'id', 'mensagem_FK_970550538', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1016', 'destinatario_usuario_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'mensagem_FK_119873046', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1017', 'destinatario_pessoa_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'mensagem_FK_67901611', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1018', 'destinatario_empresa_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'mensagem_FK_637390137', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1019', 'data_min_envio_SEC', null, '611', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1020', 'data_min_envio_OFFSEC', null, '611', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1021', 'protocolo_INT', null, '611', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1022', 'registro_estado_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'mensagem_FK_258514404', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1023', 'empresa_para_cliente_BOOLEAN', null, '611', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1024', 'corporacao_id_INT', null, '611', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'mensagem_FK_570434570', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1025', 'id', null, '612', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1026', 'tipo_canal_envio_id_INT', null, '612', '', null, '11', null, '0', '0', '0', '', '632', 'id', 'mensagem_envio_FK_703460694', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1027', 'mensagem_id_INT', null, '612', '', null, '11', null, '0', '0', '0', '', '611', 'id', 'mensagem_envio_FK_757080078', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1028', 'cadastro_SEC', null, '612', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1029', 'cadastro_OFFSEC', null, '612', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1030', 'obs', null, '612', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1031', 'identificador', null, '612', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1032', 'tentativa_INT', null, '612', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1033', 'registro_estado_id_INT', null, '612', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'mensagem_envio_FK_273468017', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1034', 'corporacao_id_INT', null, '612', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'mensagem_envio_FK_794128418', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1035', 'id', null, '613', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1036', 'descricao', null, '613', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1037', 'empresa_id_INT', null, '613', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'mesa_FK_462615967', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1038', 'template_json', null, '613', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1039', 'cadastro_SEC', null, '613', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1040', 'cadastro_OFFSEC', null, '613', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1041', 'corporacao_id_INT', null, '613', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'mesa_FK_317504883', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1042', 'id', null, '614', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1043', 'mesa_id_INT', null, '614', '', null, '11', null, '0', '0', '0', '', '613', 'id', 'mesa_reserva_FK_235290527', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1044', 'cadastro_SEC', null, '614', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1045', 'cadastro_OFFSEC', null, '614', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1046', 'pessoa_id_INT', null, '614', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'mesa_reserva_FK_304656982', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1047', 'reserva_SEC', null, '614', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1048', 'reserva_OFFSEC', null, '614', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1049', 'confimado_SEC', null, '614', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1050', 'confirmado_OFFSEC', null, '614', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1051', 'confirmador_usuario_id_INT', null, '614', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'mesa_reserva_FK_974975586', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1052', 'celular', null, '614', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1053', 'protocolo_INT', null, '614', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1054', 'corporacao_id_INT', null, '614', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'mesa_reserva_FK_961578370', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1055', 'id', null, '615', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1056', 'devedor_empresa_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'negociacao_divida_FK_620697022', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1057', 'devedor_pessoa_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'negociacao_divida_FK_110839843', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1058', 'negociador_usuario_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'negociacao_divida_FK_708282471', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1059', 'cadastro_SEC', null, '615', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1060', 'cadastro_OFFSEC', null, '615', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1061', 'resultado_empresa_venda_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '68', 'id', 'negociacao_divida_FK_563903809', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1062', 'relatorio_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'negociacao_divida_FK_873504639', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1063', 'registro_estado_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'negociacao_divida_FK_729370117', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1064', 'protocolo_INT', null, '615', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1065', 'corporacao_id_INT', null, '615', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'negociacao_divida_FK_940582276', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1066', 'id', null, '616', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1067', 'empresa_venda_protocolo_INT', null, '616', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1068', 'negociacao_divida_id_INT', null, '616', '', null, '11', null, '0', '0', '0', '', '615', 'id', 'negociacao_divida_empresa_venda_FK_449615478', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1069', 'corporacao_id_INT', null, '616', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'negociacao_divida_empresa_venda_FK_306701660', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1070', 'id', null, '617', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1071', 'empresa_id_INT', null, '617', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'nota_FK_922271729', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1072', 'pessoa_id_INT', null, '617', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'nota_FK_826232910', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1073', 'usuario_id_INT', null, '617', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'nota_FK_654876709', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1074', 'veiculo_id_INT', null, '617', '', null, '11', null, '0', '0', '0', '', '20', 'id', 'nota_FK_336425781', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1075', 'relatorio_id_INT', null, '617', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'nota_FK_797149659', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1076', 'protocolo_INT', null, '617', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1077', 'corporacao_id_INT', null, '617', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'nota_FK_750427246', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1078', 'id', null, '618', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1079', 'nota_id_INT', null, '618', '', null, '11', null, '0', '0', '0', '', '617', 'id', 'nota_tipo_nota_FK_92987060', 'CASCADE', 'SET NULL', '', '1', 'nota_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1080', 'tipo_nota_id_INT', null, '618', '', null, '11', null, '0', '0', '0', '', '633', 'id', 'nota_tipo_nota_FK_277954101', 'CASCADE', 'SET NULL', '', '1', 'nota_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1081', 'corporacao_id_INT', null, '618', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'nota_tipo_nota_FK_861236573', 'CASCADE', 'SET NULL', '', '1', 'nota_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1082', 'data_nascimento_SEC', null, '6', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1083', 'data_nascimento_OFFSEC', null, '6', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1084', 'relatorio_id_INT', null, '6', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'pessoa_FK_594970703', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1085', 'ind_email_valido_BOOLEAN', null, '6', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1086', 'ind_celular_valido_BOOLEAN', null, '6', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1087', 'id', null, '619', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1088', 'empresa_equipe_id_INT', null, '619', '', null, '11', null, '0', '0', '0', '', '498', 'id', 'pessoa_equipe_FK_929412842', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1089', 'usuario_id_INT', null, '619', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'pessoa_equipe_FK_198364258', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1090', 'pessoa_id_INT', null, '619', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'pessoa_equipe_FK_935943604', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1091', 'corporacao_id_INT', null, '619', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'pessoa_equipe_FK_316467285', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1092', 'descricao', null, '9', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1093', 'endereco', null, '9', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1094', 'id', null, '620', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1095', 'identificador', null, '620', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1096', 'nome', null, '620', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1097', 'nome_normalizado', null, '620', '', null, '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1098', 'descricao', null, '620', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1099', 'produto_unidade_medida_id_INT', null, '620', '', null, '11', null, '0', '0', '0', '', '623', 'id', 'produto_FK_485992432', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1100', 'id_omega_INT', null, '620', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1101', 'cadastro_SEC', null, '620', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1102', 'cadastro_OFFSEC', null, '620', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1103', 'preco_custo_FLOAT', null, '620', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1104', 'prazo_reposicao_estoque_dias_INT', null, '620', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1105', 'codigo_barra', null, '620', '', null, '13', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1106', 'corporacao_id_INT', null, '620', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'produto_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1107', 'id', null, '621', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1108', 'nome', null, '621', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_98765', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1109', 'nome_normalizado', null, '621', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1110', 'corporacao_id_INT', null, '621', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'produto_tipo_ibfk_1', 'CASCADE', 'SET NULL', '', '1', 'nome_98765', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1111', 'id', null, '622', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1112', 'produto_tipo_id_INT', null, '622', '', null, '11', null, '0', '0', '0', '', '621', 'id', 'produto_tipos_FK_90423584', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1113', 'produto_id_INT', null, '622', '', null, '11', null, '0', '0', '0', '', '620', 'id', 'produto_tipos_FK_811035157', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1114', 'corporacao_id_INT', null, '622', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'produto_tipos_FK_529571533', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1115', 'id', null, '623', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1116', 'nome', null, '623', '', null, '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1117', 'abreviacao', null, '623', '', null, '10', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1118', 'is_float_BOOLEAN', null, '623', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1119', 'id', null, '624', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1120', 'valor_FLOAT', null, '624', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1121', 'devedor_empresa_id_INT', null, '624', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'recebivel_FK_239410400', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1122', 'devedor_pessoa_id_INT', null, '624', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'recebivel_FK_19348144', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1123', 'cadastro_usuario_id_INT', null, '624', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'recebivel_FK_829437256', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1124', 'cadastro_SEC', null, '624', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1125', 'cadastro_OFFSEC', null, '624', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1126', 'valor_pagamento_FLOAT', null, '624', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1127', 'pagamento_SEC', null, '624', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1128', 'pagamento_OFFSEC', null, '624', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1129', 'recebedor_usuario_id_INT', null, '624', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'recebivel_FK_988525391', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1130', 'relatorio_id_INT', null, '624', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'recebivel_FK_1007080', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1131', 'protocolo_INT', null, '624', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1132', 'corporacao_id_INT', null, '624', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'recebivel_FK_595886231', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1133', 'id', null, '625', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1134', 'recebedor_empresa_id_INT', null, '625', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'recebivel_cotidiano_FK_447082519', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1135', 'cadastro_usuario_id_INT', null, '625', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'recebivel_cotidiano_FK_452484131', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1136', 'id_tipo_despesa_recebivel_INT', null, '625', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1137', 'parametro_INT', null, '625', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1138', 'parametro_OFFSEC', null, '625', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1139', 'parametro_SEC', null, '625', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1140', 'parametro_json', null, '625', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1141', 'valor_FLOAT', null, '625', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1142', 'data_limite_cotidiano_SEC', null, '625', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1143', 'data_limite_cotidiano_OFFSEC', null, '625', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1144', 'corporacao_id_INT', null, '625', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'recebivel_cotidiano_FK_900634766', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1145', 'cadastro_SEC', null, '83', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1146', 'cadastro_OFFSEC', null, '83', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1147', 'relatorio_id_INT', null, '83', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'rede_FK_704559326', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1148', 'cadastro_SEC', null, '84', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1149', 'cadastro_OFFSEC', null, '84', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1150', 'id', null, '626', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1151', 'protocolo_INT', null, '626', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1152', 'cadastro_SEC', null, '626', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1153', 'cadastro_OFFSEC', null, '626', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1154', 'responsavel_usuario_id_INT', null, '626', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'registro_FK_991607667', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1155', 'responsavel_categoria_permissao_id_INT', null, '626', '', null, '11', null, '0', '0', '0', '', '2', 'id', 'registro_FK_883850098', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1156', 'descricao', null, '626', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1157', 'tipo_registro_id_INT', null, '626', '', null, '11', null, '0', '0', '0', '', '634', 'id', 'registro_FK_435150146', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1158', 'registro_estado_id_INT', null, '626', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'registro_FK_886230469', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1159', 'registro_estado_corporacao_id_INT', null, '626', '', null, '11', null, '0', '0', '0', '', '628', 'id', 'registro_FK_225860595', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1160', 'id_origem_chamada_INT', null, '626', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1161', 'corporacao_id_INT', null, '626', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'registro_FK_979919434', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1162', 'id', null, '627', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1163', 'nome', null, '627', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1164', 'responsavel_usuario_id_INT', null, '627', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'registro_estado_FK_27282714', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1167', 'tipo_registro_id_INT', null, '627', '', null, '11', null, '0', '0', '0', '', '634', 'id', 'registro_estado_FK_634155274', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1168', 'id', null, '628', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1169', 'nome', null, '628', '', null, '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1170', 'responsavel_usuario_id_INT', null, '628', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'registro_estado_corporacao_FK_488037109', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1171', 'responsavel_categoria_permissao_id_INT', null, '628', '', null, '11', null, '0', '0', '0', '', '2', 'id', 'registro_estado_corporacao_FK_697784424', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1172', 'prazo_dias_INT', null, '628', '', null, '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1173', 'tipo_registro_id_INT', null, '628', '', null, '11', null, '0', '0', '0', '', '634', 'id', 'registro_estado_corporacao_FK_794616700', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1174', 'corporacao_id_INT', null, '628', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'registro_estado_corporacao_FK_896209717', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1175', 'id', null, '629', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1176', 'origem_registro_estado_corporacao_id_INT', null, '629', '', null, '11', null, '0', '0', '0', '', '628', 'id', 'registro_fluxo_estado_corporacao_FK_25970459', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1177', 'origem_registro_estado_id_INT', null, '629', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'registro_fluxo_estado_corporacao_FK_785644532', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1178', 'destino_registro_estado_corporacao_id_INT', null, '629', '', null, '11', null, '0', '0', '0', '', '628', 'id', 'registro_fluxo_estado_corporacao_FK_761993408', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1179', 'destino_registro_estado_id_INT', null, '629', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'registro_fluxo_estado_corporacao_FK_918395997', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1180', 'template_JSON', null, '629', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1181', 'corporacao_id_INT', null, '629', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'registro_fluxo_estado_corporacao_FK_13153076', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1182', 'tipo_relatorio_id_INT', null, '89', '', null, '11', null, '0', '0', '0', '', '635', 'id', 'relatorio_FK_984588624', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1183', 'descricao', null, '91', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1184', 'empresa_atividade_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '607', 'id', 'tarefa_FK_189971924', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1185', 'tipo_tarefa_id_INT', null, '11', '', null, '4', null, '0', '0', '0', '', '636', 'id', 'tarefa_FK_751190186', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1186', 'empresa_venda_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '68', 'id', 'tarefa_FK_443054199', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1187', 'id_prioridade_INT', null, '11', '', null, '4', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1188', 'registro_estado_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '627', 'id', 'tarefa_FK_972564698', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1189', 'registro_estado_corporacao_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '628', 'id', 'tarefa_FK_558166504', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1190', 'id', null, '630', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1191', 'criado_pelo_usuario_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_cotidiano_ibfk_8', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1192', 'categoria_permissao_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '2', 'id', 'tarefa_cotidiano_ibfk_10', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1193', 'veiculo_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '20', 'id', 'tarefa_cotidiano_ibfk_1', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1194', 'usuario_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_cotidiano_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1195', 'empresa_equipe_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '498', 'id', 'tarefa_cotidiano_FK_718597412', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1196', 'origem_pessoa_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'tarefa_cotidiano_ibfk_11', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1197', 'origem_empresa_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'tarefa_cotidiano_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1198', 'origem_logradouro', null, '630', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1199', 'origem_numero', null, '630', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1200', 'origem_cidade_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'tarefa_cotidiano_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1201', 'origem_latitude_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1202', 'origem_longitude_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1203', 'origem_latitude_real_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1204', 'origem_longitude_real_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1205', 'destino_pessoa_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '6', 'id', 'tarefa_cotidiano_ibfk_7', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1206', 'destino_empresa_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'tarefa_cotidiano_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1207', 'destino_logradouro', null, '630', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1208', 'destino_numero', null, '630', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1209', 'destino_cidade_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '4', 'id', 'tarefa_cotidiano_ibfk_9', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1210', 'destino_latitude_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1211', 'destino_longitude_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1212', 'destino_latitude_real_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1213', 'destino_longitude_real_INT', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1214', 'tempo_estimado_carro_INT', null, '630', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1215', 'tempo_estimado_a_pe_INT', null, '630', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1216', 'distancia_estimada_carro_INT', null, '630', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1217', 'distancia_estimada_a_pe_INT', null, '630', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1218', 'is_realizada_a_pe_BOOLEAN', null, '630', '', null, '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1219', 'inicio_hora_programada_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1220', 'inicio_hora_programada_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1221', 'inicio_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1222', 'inicio_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1223', 'fim_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1224', 'fim_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1225', 'cadastro_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1226', 'cadastro_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1227', 'titulo', null, '630', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1228', 'titulo_normalizado', null, '630', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1229', 'descricao', null, '630', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1230', 'corporacao_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tarefa_cotidiano_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1231', 'servico_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '31', 'id', 'tarefa_cotidiano_FK_656982422', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('1232', 'id_estado_INT', null, '630', '', null, '4', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1233', 'venda_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('1234', 'id_prioridade_INT', null, '630', '', null, '4', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1235', 'parametro_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1236', 'parametro_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1237', 'parametro_json', null, '630', '', null, '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1238', 'data_limite_cotidiano_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1239', 'data_limite_cotidiano_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1240', 'id', null, '631', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1241', 'descricao', null, '631', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1242', 'data_conclusao_SEC', null, '631', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1243', 'data_conclusao_OFFSEC', null, '631', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1244', 'tarefa_id_INT', null, '631', '', null, '11', null, '0', '0', '0', '', '11', 'id', 'tarefa_item_FK_979400635', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1245', 'cadastro_SEC', null, '631', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1246', 'cadastro_OFFSEC', null, '631', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1247', 'criado_pelo_usuario_id_INT', null, '631', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_item_FK_803283692', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1248', 'executada_pelo_usuario_id_INT', null, '631', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_item_FK_903411866', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1249', 'corporacao_id_INT', null, '631', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tarefa_item_FK_176757812', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1250', 'id', null, '632', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1251', 'nome', null, '632', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1252', 'id', null, '633', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1253', 'nome', null, '633', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1254', 'corporacao_id_INT', null, '633', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tipo_nota_FK_962524415', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1255', 'id', null, '634', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1256', 'nome', null, '634', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1257', 'id', null, '635', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1258', 'nome', null, '635', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1259', 'id', null, '636', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1260', 'nome', null, '636', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1261', 'corporacao_id_INT', null, '636', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tipo_tarefa_FK_143341064', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('1262', 'id', null, '637', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1263', 'nome', null, '637', '', null, '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1264', 'corporacao_id_INT', null, '637', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tipo_veiculo_registro_FK_76385498', 'CASCADE', 'SET NULL', '', '0', '', null, '1');
INSERT INTO `sistema_atributo` VALUES ('1265', 'id', null, '638', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1266', 'hora_inicio_TIME', null, '638', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1267', 'hora_fim_TIME', null, '638', '', null, null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1268', 'data_SEC', null, '638', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1269', 'data_OFFSEC', null, '638', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1270', 'dia_semana_INT', null, '638', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1271', 'empresa_id_INT', null, '638', '', null, '11', null, '0', '0', '0', '', '5', 'id', 'usuario_horario_trabalho_FK_788391114', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1272', 'usuario_id_INT', null, '638', '', null, '11', null, '0', '0', '0', '', '14', 'id', 'usuario_horario_trabalho_FK_905120850', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1273', 'template_JSON', null, '638', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1274', 'corporacao_id_INT', null, '638', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'usuario_horario_trabalho_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1275', 'tipo_veiculo_registro_id_INT', null, '446', '', null, '11', null, '0', '0', '0', '', '637', 'id', 'veiculo_registro_FK_612823486', 'CASCADE', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1276', 'protocolo_INT', null, '609', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1277', 'id', null, '639', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1278', 'nome', null, '639', '', null, '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1279', 'corporacao_id_INT', null, '639', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1280', 'percentual_completo_INT', null, '11', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1281', 'prazo_SEC', null, '11', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1282', 'prazo_OFFSEC', null, '11', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1283', 'atividade_id_INT', null, '11', '', null, '11', null, '0', '0', '0', '', '601', 'id', 'tarefa_FK_575897217', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1284', 'protocolo_empresa_venda_INT', null, '11', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1285', 'protocolo_empresa_atividade_venda_INT', null, '11', '', null, '20', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1286', 'atividade_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '601', 'id', 'tarefa_cotidiano_FK_316833496', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1287', 'id_tipo_cotidiano_INT', null, '630', '', null, '4', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1288', 'prazo_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1289', 'prazo_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1290', 'percentual_completo_INT', null, '630', '', null, '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1291', 'ultima_geracao_SEC', null, '630', '', null, '10', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1292', 'ultima_geracao_OFFSEC', null, '630', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1293', 'relatorio_id_INT', null, '630', '', null, '11', null, '0', '0', '0', '', '89', 'id', 'tarefa_cotidiano_FK_442840576', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1294', 'id', null, '640', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1295', 'descricao', null, '640', '', null, '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1296', 'corporacao_id_INT', null, '640', '', null, '11', null, '0', '0', '0', '', '49', 'id', 'tarefa_cotidiano_item_FK_381195068', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1297', 'seq_INT', null, '640', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1298', 'latitude_INT', null, '640', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1299', 'longitude_INT', null, '640', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1300', 'tarefa_cotidiano_id_INT', null, '640', '', null, '11', null, '0', '0', '0', '', '630', 'id', 'tarefa_cotidiano_item_FK_630126953', 'SET NULL', 'SET NULL', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1301', 'seq_INT', null, '631', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1302', 'latitude_INT', null, '631', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1303', 'longitude_INT', null, '631', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1304', 'latitude_real_INT', null, '631', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1305', 'longitude_real_INT', null, '631', '', null, '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1306', 'id', null, '641', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1307', 'tarefa_id_INT', null, '641', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1308', 'relatorio_id_INT', null, '641', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1309', 'corporacao_id_INT', null, '641', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1310', 'id', null, '642', '', null, '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1311', 'tarefa_id_INT', null, '642', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1312', 'tag_id_INT', null, '642', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1313', 'corporacao_id_INT', null, '642', '', null, '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, '0');

-- ----------------------------
-- Table structure for `sistema_campo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_campo`;
CREATE TABLE `sistema_campo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(256) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `sistema_lista_id_INT` int(11) DEFAULT NULL,
  `sistema_tipo_campo_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_campo_FK_927398682` (`sistema_lista_id_INT`),
  KEY `sistema_campo_FK_760070801` (`sistema_tipo_campo_id_INT`),
  CONSTRAINT `sistema_campo_FK_760070801` FOREIGN KEY (`sistema_tipo_campo_id_INT`) REFERENCES `sistema_tipo_campo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_campo_FK_927398682` FOREIGN KEY (`sistema_lista_id_INT`) REFERENCES `sistema_lista` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_campo
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_campo_atributo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_campo_atributo`;
CREATE TABLE `sistema_campo_atributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Essa tabela contem os atributos gerados por deterimando campo. Ex: campo do tipo data inicio e fim possuem dois atributos datetime. Campo data possui um atributo do tipo date.',
  `sistema_campo_id_INT` int(11) DEFAULT NULL,
  `sistema_atributo_id_INT` int(11) DEFAULT NULL COMMENT 'atributo do prototipo android que foi criado pelo campo',
  `seq_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_campo_atributo_FK_405761719` (`sistema_campo_id_INT`),
  KEY `sistema_campo_atributo_FK_854766846` (`sistema_atributo_id_INT`),
  CONSTRAINT `sistema_campo_atributo_FK_405761719` FOREIGN KEY (`sistema_campo_id_INT`) REFERENCES `sistema_campo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_campo_atributo_FK_854766846` FOREIGN KEY (`sistema_atributo_id_INT`) REFERENCES `sistema_atributo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_campo_atributo
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_estado_lista`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_estado_lista`;
CREATE TABLE `sistema_estado_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(215) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_estado_lista
-- ----------------------------
INSERT INTO `sistema_estado_lista` VALUES ('1', 'DESENVOLVIMENTO');
INSERT INTO `sistema_estado_lista` VALUES ('2', 'INICIAR PROCESSO DE PUBLICAÇÃO');
INSERT INTO `sistema_estado_lista` VALUES ('3', 'PUBLICADA');

-- ----------------------------
-- Table structure for `sistema_lista`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_lista`;
CREATE TABLE `sistema_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL COMMENT 'Tabela que foi gerada a partir da lista',
  `sistema_estado_lista_id_INT` int(11) DEFAULT NULL,
  `ultima_alteracao_usuario_id_INT` int(11) DEFAULT NULL,
  `ultima_alteracao_SEC` int(10) DEFAULT NULL,
  `ultima_alteracao_OFFSEC` int(6) DEFAULT NULL,
  `criacao_SEC` int(10) DEFAULT NULL,
  `criacao_OFFSEC` int(6) DEFAULT NULL,
  `criador_usuario_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_lista_FK_318206787` (`sistema_tabela_id_INT`),
  KEY `sistema_lista_FK_14404296` (`sistema_estado_lista_id_INT`),
  KEY `sistema_lista_FK_571807861` (`ultima_alteracao_usuario_id_INT`),
  KEY `sistema_lista_FK_563171387` (`criador_usuario_id_INT`),
  CONSTRAINT `sistema_lista_FK_14404296` FOREIGN KEY (`sistema_estado_lista_id_INT`) REFERENCES `sistema_estado_lista` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_lista_FK_318206787` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_lista_FK_563171387` FOREIGN KEY (`criador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_lista_FK_571807861` FOREIGN KEY (`ultima_alteracao_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_lista
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_parametro_global`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_parametro_global`;
CREATE TABLE `sistema_parametro_global` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `valor_string` varchar(250) DEFAULT NULL,
  `valor_INT` int(11) DEFAULT NULL,
  `valor_DATETIME` datetime DEFAULT NULL,
  `valor_SEC` int(10) DEFAULT NULL,
  `valor_OFFSEC` int(6) DEFAULT NULL,
  `valor_DATE` date DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `valor_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_parametro_global
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_parametro_global_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_parametro_global_corporacao`;
CREATE TABLE `sistema_parametro_global_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `valor_string` varchar(250) DEFAULT NULL,
  `valor_INT` int(11) DEFAULT NULL,
  `valor_DATETIME` datetime DEFAULT NULL,
  `valor_SEC` int(10) DEFAULT NULL,
  `valor_OFFSEC` int(6) DEFAULT NULL,
  `valor_DATE` date DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `valor_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spgc_FK_66619873` (`corporacao_id_INT`),
  CONSTRAINT `spgc_ibfk_1 ` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_parametro_global_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_projetos_versao`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_projetos_versao`;
CREATE TABLE `sistema_projetos_versao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projetos_versao_INT` int(11) NOT NULL,
  `data_homologacao_DATETIME` datetime DEFAULT NULL,
  `data_producao_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_projetos_versao
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_registro_sincronizador`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_registro_sincronizador`;
CREATE TABLE `sistema_registro_sincronizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `id_tabela_INT` int(11) NOT NULL,
  `tipo_operacao_banco_id_INT` int(2) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `is_from_android_BOOLEAN` int(1) NOT NULL,
  `imei` varchar(30) DEFAULT NULL,
  `sistema_produto_INT` int(11) DEFAULT NULL,
  `sistema_projetos_versao_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sincronizador_FK_801788330` (`tipo_operacao_banco_id_INT`),
  KEY `sincronizador_FK_881042481` (`usuario_id_INT`),
  KEY `sincronizador_FK_66619873` (`corporacao_id_INT`),
  KEY `sistema_registro_sincronizador_FK_110656738` (`sistema_tabela_id_INT`),
  CONSTRAINT `sistema_registro_sincronizador_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_ibfk_2` FOREIGN KEY (`tipo_operacao_banco_id_INT`) REFERENCES `sistema_tipo_operacao_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_ibfk_3` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_ibfk_4` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_registro_sincronizador
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_sequencia`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_sequencia`;
CREATE TABLE `sistema_sequencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabela` varchar(128) DEFAULT NULL,
  `id_ultimo_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_sequencia
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_tabela`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tabela`;
CREATE TABLE `sistema_tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  `data_modificacao_estrutura_DATETIME` date DEFAULT NULL,
  `frequencia_sincronizador_INT` int(2) DEFAULT NULL,
  `banco_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `banco_web_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_web_para_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_mobile_para_web_BOOLEAN` int(1) DEFAULT NULL,
  `tabela_sistema_BOOLEAN` int(1) DEFAULT NULL,
  `excluida_BOOLEAN` int(1) DEFAULT NULL,
  `atributos_chave_unica` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=643 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tabela
-- ----------------------------
INSERT INTO `sistema_tabela` VALUES ('1', 'bairro', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, cidade_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('2', 'categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('3', 'permissao_categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'permissao_id_INT, categoria_permissao_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('4', 'cidade', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, uf_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('5', 'empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('6', 'pessoa', null, null, '-1', '1', '1', '1', '1', null, '0', 'email, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('7', 'pessoa_empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, empresa_id_INT, profissao_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('8', 'modelo', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('9', 'ponto', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('10', 'profissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('11', 'tarefa', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('12', 'tipo_empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('13', 'uf', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT, pais_id_INT');
INSERT INTO `sistema_tabela` VALUES ('14', 'usuario', null, null, '-1', '1', '1', '1', '1', null, '0', 'email');
INSERT INTO `sistema_tabela` VALUES ('15', 'usuario_categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('16', 'usuario_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('17', 'usuario_foto', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('18', 'usuario_posicao', null, null, '0', '1', '1', '0', '0', null, '0', 'usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('19', 'usuario_servico', null, null, '-1', '1', '1', '1', '1', null, '0', 'servico_id_INT, usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('20', 'veiculo', null, null, '-1', '1', '1', '1', '1', null, '0', 'placa, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('21', 'veiculo_usuario', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, veiculo_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('22', 'pais', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('23', 'tipo_documento', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('24', 'empresa_perfil', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('30', 'perfil', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('31', 'servico', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('39', 'sexo', null, null, '1', '1', '1', '1', '0', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('46', 'permissao', null, null, '-1', '1', '1', '1', '1', '1', '0', 'tag');
INSERT INTO `sistema_tabela` VALUES ('49', 'corporacao', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('54', 'empresa_compra', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('55', 'empresa_compra_parcela', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('56', 'empresa_produto', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('57', 'empresa_produto_compra', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('58', 'empresa_produto_foto', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('59', 'empresa_produto_unidade_medida', null, null, '1', '1', '1', '1', '0', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('60', 'empresa_produto_venda', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('61', 'empresa_servico', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('62', 'empresa_servico_compra', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('63', 'empresa_servico_foto', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('64', 'empresa_servico_unidade_medida', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('65', 'empresa_servico_venda', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('66', 'empresa_tipo_venda', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('67', 'empresa_tipo_venda_parcela', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('68', 'empresa_venda', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('69', 'empresa_venda_parcela', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('71', 'empresa_produto_tipo', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('72', 'empresa_servico_tipo', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('73', 'usuario_tipo', null, null, '-1', '1', '1', '1', '0', null, '0', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('75', 'usuario_tipo_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('77', 'operadora', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado');
INSERT INTO `sistema_tabela` VALUES ('81', 'app', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('82', 'tipo_ponto', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('83', 'rede', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('84', 'rede_empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT');
INSERT INTO `sistema_tabela` VALUES ('85', 'pessoa_empresa_rotina', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, empresa_id_INT, profissao_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('86', 'rotina', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT');
INSERT INTO `sistema_tabela` VALUES ('87', 'pessoa_usuario', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('89', 'relatorio', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT');
INSERT INTO `sistema_tabela` VALUES ('90', 'tipo_anexo', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('91', 'relatorio_anexo', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT');
INSERT INTO `sistema_tabela` VALUES ('126', 'estado_civil', null, null, '1', '1', '1', '1', '0', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('139', 'sistema_tabela', null, null, '-1', '1', '1', '1', '0', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('143', 'forma_pagamento', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('168', 'usuario_foto_configuracao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT');
INSERT INTO `sistema_tabela` VALUES ('171', 'sistema_tipo_operacao_banco', null, null, '1', '1', '1', '1', '0', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('238', 'sistema_banco_versao', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('317', 'sistema_tipo_mobile', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('337', 'sistema_usuario_mensagem', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('343', 'sistema_tipo_download_arquivo', null, null, '0', '1', '1', '0', '0', '1', '0', 'nome');
INSERT INTO `sistema_tabela` VALUES ('344', 'sistema_tipo_usuario_mensagem', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('441', 'horario_trabalho_pessoa_empresa', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome');
INSERT INTO `sistema_tabela` VALUES ('442', 'tarefa_acao', null, null, null, '1', '1', null, null, null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('443', 'projetos_versao', null, null, null, null, null, null, null, '1', '1', null);
INSERT INTO `sistema_tabela` VALUES ('444', 'android_metadata', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('445', 'sistema_atributo', null, null, '-1', '1', '1', '1', '1', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('446', 'veiculo_registro', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('447', 'sistema_deletar_arquivo', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('448', 'sistema_parametro_global', null, null, '-1', '1', '1', '1', '1', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('449', 'sistema_produto_log_erro', null, null, null, '1', '1', null, null, '1', '1', null);
INSERT INTO `sistema_tabela` VALUES ('450', 'corporacao_sincronizador_php', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('451', 'usuario_posicao_rastreamento', null, null, null, '1', '1', null, null, null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('452', 'sistema_sincronizador_arquivo', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('453', 'sistema_grafo_hierarquia_banco', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('454', 'sistema_historico_sincronizador', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('455', 'sistema_operacao_crud_aleatorio', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('456', 'sistema_operacao_executa_script', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('457', 'sistema_operacao_sistema_mobile', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('458', 'sistema_corporacao_sincronizador', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('459', 'sistema_aresta_grafo_hierarquia_banco', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('460', 'sistema_operacao_download_banco_mobile', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('461', 'sistema_corporacao_sincronizador_tabela', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('462', 'sistema_registro_sincronizador_android_para_web', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('463', 'sistema_registro_sincronizador_web_para_android', null, null, null, '1', '1', null, null, '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('464', 'wifi', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('465', 'wifi_registro', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('466', 'acesso', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('467', 'operacao_sistema', null, null, '0', '1', '1', '0', '0', '1', '1', null);
INSERT INTO `sistema_tabela` VALUES ('468', 'ponto_endereco', null, null, '0', '1', '1', '0', '0', null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('469', 'sistema_projetos_versao', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('470', 'sistema_registro_sincronizador', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('471', 'usuario_empresa', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('472', 'usuario_menu', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('473', 'usuario_privilegio', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('474', 'usuario_tipo_menu', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('475', 'usuario_tipo_privilegio', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('476', 'campo', null, null, '-1', '1', '1', '1', '0', null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('477', 'tela', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('478', 'tela_categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('479', 'tela_sistema_tabela', null, null, '-1', '1', '1', '1', '0', null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('480', 'tela_usuario_tipo', null, null, '0', '1', '1', '0', '0', null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('481', 'tipo_campo', null, null, '1', '1', '1', '1', '0', '1', '1', null);
INSERT INTO `sistema_tabela` VALUES ('482', 'tipo_cardinalidade', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('483', 'tipo_tela', null, null, '1', '1', '1', '1', '0', '1', '1', null);
INSERT INTO `sistema_tabela` VALUES ('484', 'area_menu', null, null, '-1', '1', '1', '0', '1', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('485', 'sistema_campo', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('486', 'sistema_campo_atributo', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('487', 'sistema_estado_lista', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('488', 'sistema_lista', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('489', 'sistema_tipo_campo', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('490', 'tela_componente', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('491', 'tela_estado', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('492', 'tela_sistema_lista', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('493', 'tela_tipo', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('494', 'tela_tipo_componente', null, null, '0', '1', '1', '0', '0', '1', '0', null);
INSERT INTO `sistema_tabela` VALUES ('495', 'sistema_sequencia', null, null, '0', '1', '1', '0', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('496', 'sistema_crud_estado', null, null, null, '1', '1', null, null, null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('497', 'sistema_registro_historico', null, null, null, '1', '1', null, null, null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('498', 'empresa_equipe', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('499', 'pessoa_empresa_equipe', null, null, '-1', '1', '1', '1', '1', null, '1', null);
INSERT INTO `sistema_tabela` VALUES ('597', 'sistema_parametro_global_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('598', 'sistema_operacao_crud_mobile_baseado_web', null, null, null, '1', '1', null, null, null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('599', 'api', null, null, '-1', '1', '1', '1', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('600', 'api_id', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('601', 'atividade', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('602', 'atividade_tipo', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('603', 'atividade_tipos', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('604', 'atividade_unidade_medida', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('605', 'despesa', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('606', 'despesa_cotidiano', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('607', 'empresa_atividade', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('608', 'empresa_atividade_compra', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('609', 'empresa_atividade_venda', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('610', 'empresa_venda_template', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('611', 'mensagem', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('612', 'mensagem_envio', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('613', 'mesa', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('614', 'mesa_reserva', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('615', 'negociacao_divida', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('616', 'negociacao_divida_empresa_venda', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('617', 'nota', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('618', 'nota_tipo_nota', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('619', 'pessoa_equipe', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('620', 'produto', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('621', 'produto_tipo', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('622', 'produto_tipos', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('623', 'produto_unidade_medida', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('624', 'recebivel', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('625', 'recebivel_cotidiano', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('626', 'registro', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('627', 'registro_estado', null, null, '-1', '1', '1', '0', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('628', 'registro_estado_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('629', 'registro_fluxo_estado_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('630', 'tarefa_cotidiano', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('631', 'tarefa_item', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('632', 'tipo_canal_envio', null, null, '-1', '1', '1', '1', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('633', 'tipo_nota', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('634', 'tipo_registro', null, null, '-1', '1', '1', '1', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('635', 'tipo_relatorio', null, null, '-1', '1', '1', '1', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('636', 'tipo_tarefa', null, null, '-1', '1', '1', '1', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('637', 'tipo_veiculo_registro', null, null, '-1', '1', '1', '1', '0', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('638', 'usuario_horario_trabalho', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('639', 'tag', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('640', 'tarefa_cotidiano_item', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('641', 'tarefa_relatorio', null, null, '-1', '1', '1', '1', '1', null, '0', null);
INSERT INTO `sistema_tabela` VALUES ('642', 'tarefa_tag', null, null, '-1', '1', '1', '1', '1', null, '0', null);

-- ----------------------------
-- Table structure for `sistema_tipo_campo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_campo`;
CREATE TABLE `sistema_tipo_campo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(256) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_campo
-- ----------------------------
INSERT INTO `sistema_tipo_campo` VALUES ('1', 'data_e_hora', 'Data e hora Ex.: 03/02/1988 20:48', null);
INSERT INTO `sistema_tipo_campo` VALUES ('2', 'data', 'Data. Ex.: 03/02/1988', null);
INSERT INTO `sistema_tipo_campo` VALUES ('3', 'hora', 'Hora. Ex.: 20:48 ', null);
INSERT INTO `sistema_tipo_campo` VALUES ('4', 'preco', 'Preço. Ex.: R$ 27,99 ', null);
INSERT INTO `sistema_tipo_campo` VALUES ('5', 'inteiro', 'Número Inteiro. Ex.: 1, 2, 3', null);
INSERT INTO `sistema_tipo_campo` VALUES ('6', 'descricao', 'Descrição (Uma linha)', null);
INSERT INTO `sistema_tipo_campo` VALUES ('7', 'telefone', 'Telefone', null);
INSERT INTO `sistema_tipo_campo` VALUES ('8', 'texto', 'Texto (Múltiplas linhas)', null);
INSERT INTO `sistema_tipo_campo` VALUES ('9', 'flutuante', 'Real, Flutuante. Ex.: 1.2, 1.323', null);
INSERT INTO `sistema_tipo_campo` VALUES ('10', 'consulta', 'Consultar outro registro. Ex.: Venda->Item da Venda', null);
INSERT INTO `sistema_tipo_campo` VALUES ('11', 'data_inicio_e_fim', 'Data inicial e final', null);
INSERT INTO `sistema_tipo_campo` VALUES ('12', 'datetime_inicio_e_fim', 'Data e hora inicial e final', null);
INSERT INTO `sistema_tipo_campo` VALUES ('13', 'time_inicio_e_fim', 'Hora inicial e final', null);
INSERT INTO `sistema_tipo_campo` VALUES ('14', 'posição gps', 'Localização do GPS', null);

-- ----------------------------
-- Table structure for `sistema_tipo_download_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_download_arquivo`;
CREATE TABLE `sistema_tipo_download_arquivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `path` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sistema_tipo_download_arquivo_UNIQUE` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_download_arquivo
-- ----------------------------
INSERT INTO `sistema_tipo_download_arquivo` VALUES ('1', 'fotos', 'fotos');
INSERT INTO `sistema_tipo_download_arquivo` VALUES ('2', 'videos', 'videos');
INSERT INTO `sistema_tipo_download_arquivo` VALUES ('3', 'anexos', 'anexos');
INSERT INTO `sistema_tipo_download_arquivo` VALUES ('4', 'audios', 'audios');
INSERT INTO `sistema_tipo_download_arquivo` VALUES ('5', 'bancos', 'bancos');

-- ----------------------------
-- Table structure for `sistema_tipo_operacao_banco`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_operacao_banco`;
CREATE TABLE `sistema_tipo_operacao_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_84756` (`nome`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_operacao_banco
-- ----------------------------
INSERT INTO `sistema_tipo_operacao_banco` VALUES ('3', 'Editar');
INSERT INTO `sistema_tipo_operacao_banco` VALUES ('2', 'Inserir');
INSERT INTO `sistema_tipo_operacao_banco` VALUES ('1', 'Remover');

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_FK_198974609` (`corporacao_id_INT`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tag
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa`;
CREATE TABLE `tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `origem_pessoa_id_INT` int(11) DEFAULT NULL,
  `origem_empresa_id_INT` int(11) DEFAULT NULL,
  `origem_logradouro` varchar(255) DEFAULT NULL,
  `origem_numero` varchar(30) DEFAULT NULL,
  `origem_cidade_id_INT` int(11) DEFAULT NULL,
  `origem_latitude_INT` int(6) DEFAULT NULL,
  `origem_longitude_INT` int(6) DEFAULT NULL,
  `origem_latitude_real_INT` int(6) DEFAULT NULL,
  `origem_longitude_real_INT` int(6) DEFAULT NULL,
  `destino_pessoa_id_INT` int(11) DEFAULT NULL,
  `destino_empresa_id_INT` int(11) DEFAULT NULL,
  `destino_logradouro` varchar(255) DEFAULT NULL,
  `destino_numero` varchar(30) DEFAULT NULL,
  `destino_cidade_id_INT` int(11) DEFAULT NULL,
  `destino_latitude_INT` int(6) DEFAULT NULL,
  `destino_longitude_INT` int(6) DEFAULT NULL,
  `destino_latitude_real_INT` int(6) DEFAULT NULL,
  `destino_longitude_real_INT` int(6) DEFAULT NULL,
  `tempo_estimado_carro_INT` int(11) DEFAULT NULL,
  `tempo_estimado_a_pe_INT` int(11) DEFAULT NULL,
  `distancia_estimada_carro_INT` int(11) DEFAULT NULL,
  `distancia_estimada_a_pe_INT` int(11) DEFAULT NULL,
  `inicio_hora_programada_SEC` int(10) DEFAULT NULL,
  `inicio_hora_programada_OFFSEC` int(6) DEFAULT NULL,
  `data_exibir_SEC` int(10) DEFAULT NULL,
  `data_exibir_OFFSEC` int(6) DEFAULT NULL,
  `inicio_SEC` int(10) DEFAULT NULL,
  `inicio_OFFSEC` int(6) DEFAULT NULL,
  `fim_SEC` int(10) DEFAULT NULL,
  `fim_OFFSEC` int(6) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `titulo_normalizado` varchar(100) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `empresa_atividade_id_INT` int(11) DEFAULT NULL,
  `tipo_tarefa_id_INT` int(4) DEFAULT NULL,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `id_prioridade_INT` int(4) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `percentual_completo_INT` int(2) DEFAULT NULL,
  `prazo_SEC` int(10) DEFAULT NULL,
  `prazo_OFFSEC` int(6) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `protocolo_empresa_venda_INT` bigint(20) DEFAULT NULL,
  `protocolo_empresa_atividade_venda_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_FK_490081787` (`origem_cidade_id_INT`),
  KEY `tarefa_FK_618682861` (`destino_cidade_id_INT`),
  KEY `tarefa_FK_90545654` (`corporacao_id_INT`),
  KEY `tabela_FK_100` (`usuario_id_INT`),
  KEY `tarefa_FK_529296875` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_FK_70098877` (`categoria_permissao_id_INT`),
  KEY `tarefa_FK_751892090` (`veiculo_id_INT`),
  KEY `tarefa_FK_718139649` (`origem_pessoa_id_INT`),
  KEY `tarefa_FK_466461182` (`origem_empresa_id_INT`),
  KEY `tarefa_FK_517883301` (`destino_pessoa_id_INT`),
  KEY `tarefa_FK_919769288` (`destino_empresa_id_INT`),
  KEY `tarefa_FK_603546143` (`empresa_equipe_id_INT`),
  KEY `tarefa_FK_751190186` (`tipo_tarefa_id_INT`),
  KEY `tarefa_FK_972564698` (`registro_estado_id_INT`),
  KEY `tarefa_FK_558166504` (`registro_estado_corporacao_id_INT`),
  KEY `tarefa_FK_189971924` (`empresa_atividade_id_INT`),
  KEY `tarefa_FK_443054199` (`empresa_venda_id_INT`),
  KEY `tarefa_FK_575897217` (`atividade_id_INT`),
  CONSTRAINT `tarefa_FK_189971924` FOREIGN KEY (`empresa_atividade_id_INT`) REFERENCES `empresa_atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_FK_443054199` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_FK_558166504` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_FK_575897217` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_FK_603546143` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_FK_751190186` FOREIGN KEY (`tipo_tarefa_id_INT`) REFERENCES `tipo_tarefa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_FK_972564698` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_10` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_11` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_12` FOREIGN KEY (`destino_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_13` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_14` FOREIGN KEY (`origem_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_15` FOREIGN KEY (`origem_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_3` FOREIGN KEY (`destino_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_4` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_5` FOREIGN KEY (`destino_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_7` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_8` FOREIGN KEY (`origem_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_cotidiano`;
CREATE TABLE `tarefa_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `origem_pessoa_id_INT` int(11) DEFAULT NULL,
  `origem_empresa_id_INT` int(11) DEFAULT NULL,
  `origem_logradouro` varchar(255) DEFAULT NULL,
  `origem_numero` varchar(30) DEFAULT NULL,
  `origem_cidade_id_INT` int(11) DEFAULT NULL,
  `origem_latitude_INT` int(6) DEFAULT NULL,
  `origem_longitude_INT` int(6) DEFAULT NULL,
  `origem_latitude_real_INT` int(6) DEFAULT NULL,
  `origem_longitude_real_INT` int(6) DEFAULT NULL,
  `destino_pessoa_id_INT` int(11) DEFAULT NULL,
  `destino_empresa_id_INT` int(11) DEFAULT NULL,
  `destino_logradouro` varchar(255) DEFAULT NULL,
  `destino_numero` varchar(30) DEFAULT NULL,
  `destino_cidade_id_INT` int(11) DEFAULT NULL,
  `destino_latitude_INT` int(6) DEFAULT NULL,
  `destino_longitude_INT` int(6) DEFAULT NULL,
  `destino_latitude_real_INT` int(6) DEFAULT NULL,
  `destino_longitude_real_INT` int(6) DEFAULT NULL,
  `tempo_estimado_carro_INT` int(11) DEFAULT NULL,
  `tempo_estimado_a_pe_INT` int(11) DEFAULT NULL,
  `distancia_estimada_carro_INT` int(11) DEFAULT NULL,
  `distancia_estimada_a_pe_INT` int(11) DEFAULT NULL,
  `is_realizada_a_pe_BOOLEAN` int(1) DEFAULT NULL,
  `inicio_hora_programada_SEC` int(10) DEFAULT NULL,
  `inicio_hora_programada_OFFSEC` int(6) DEFAULT NULL,
  `inicio_SEC` int(10) DEFAULT NULL,
  `inicio_OFFSEC` int(6) DEFAULT NULL,
  `fim_SEC` int(10) DEFAULT NULL,
  `fim_OFFSEC` int(6) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `titulo_normalizado` varchar(100) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `id_estado_INT` int(4) DEFAULT NULL,
  `id_prioridade_INT` int(4) DEFAULT NULL,
  `parametro_SEC` int(10) DEFAULT NULL,
  `parametro_OFFSEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `id_tipo_cotidiano_INT` int(4) DEFAULT NULL,
  `prazo_SEC` int(10) DEFAULT NULL,
  `prazo_OFFSEC` int(6) DEFAULT NULL,
  `percentual_completo_INT` int(2) DEFAULT NULL,
  `ultima_geracao_SEC` int(10) DEFAULT NULL,
  `ultima_geracao_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_FK_490081787` (`origem_cidade_id_INT`),
  KEY `tarefa_FK_618682861` (`destino_cidade_id_INT`),
  KEY `tarefa_FK_90545654` (`corporacao_id_INT`),
  KEY `tabela_FK_100` (`usuario_id_INT`),
  KEY `tarefa_FK_529296875` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_FK_70098877` (`categoria_permissao_id_INT`),
  KEY `tarefa_FK_751892090` (`veiculo_id_INT`),
  KEY `tarefa_FK_718139649` (`origem_pessoa_id_INT`),
  KEY `tarefa_FK_466461182` (`origem_empresa_id_INT`),
  KEY `tarefa_FK_517883301` (`destino_pessoa_id_INT`),
  KEY `tarefa_FK_919769288` (`destino_empresa_id_INT`),
  KEY `tarefa_cotidiano_FK_718597412` (`empresa_equipe_id_INT`),
  KEY `tarefa_cotidiano_FK_316833496` (`atividade_id_INT`),
  KEY `tarefa_cotidiano_FK_442840576` (`relatorio_id_INT`),
  CONSTRAINT `tarefa_cotidiano_FK_316833496` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_cotidiano_FK_442840576` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_cotidiano_FK_718597412` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_1` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_10` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_11` FOREIGN KEY (`origem_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_3` FOREIGN KEY (`destino_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_4` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_5` FOREIGN KEY (`origem_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_6` FOREIGN KEY (`origem_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_7` FOREIGN KEY (`destino_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_8` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_9` FOREIGN KEY (`destino_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_cotidiano_item`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_cotidiano_item`;
CREATE TABLE `tarefa_cotidiano_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `tarefa_cotidiano_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_cotidiano_item_FK_381195068` (`corporacao_id_INT`),
  KEY `tarefa_cotidiano_item_FK_630126953` (`tarefa_cotidiano_id_INT`),
  CONSTRAINT `tarefa_cotidiano_item_FK_381195068` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_cotidiano_item_FK_630126953` FOREIGN KEY (`tarefa_cotidiano_id_INT`) REFERENCES `tarefa_cotidiano` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_cotidiano_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_item`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_item`;
CREATE TABLE `tarefa_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `data_conclusao_SEC` int(10) DEFAULT NULL,
  `data_conclusao_OFFSEC` int(6) DEFAULT NULL,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `executada_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `latitude_real_INT` int(6) DEFAULT NULL,
  `longitude_real_INT` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_item_FK_979400635` (`tarefa_id_INT`),
  KEY `tarefa_item_FK_803283692` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_item_FK_903411866` (`executada_pelo_usuario_id_INT`),
  KEY `tarefa_item_FK_176757812` (`corporacao_id_INT`),
  CONSTRAINT `tarefa_item_FK_176757812` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_item_FK_803283692` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_item_FK_903411866` FOREIGN KEY (`executada_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_item_FK_979400635` FOREIGN KEY (`tarefa_id_INT`) REFERENCES `tarefa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_relatorio`;
CREATE TABLE `tarefa_relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_relatorio_FK_286987304` (`tarefa_id_INT`),
  KEY `tarefa_relatorio_FK_220855713` (`relatorio_id_INT`),
  KEY `tarefa_relatorio_FK_496887207` (`corporacao_id_INT`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_tag`;
CREATE TABLE `tarefa_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `tag_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_tag_FK_665161133` (`tarefa_id_INT`),
  KEY `tarefa_tag_FK_816802979` (`tag_id_INT`),
  KEY `tarefa_tag_FK_330139160` (`corporacao_id_INT`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `tela`
-- ----------------------------
DROP TABLE IF EXISTS `tela`;
CREATE TABLE `tela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) DEFAULT NULL,
  `tela_estado_id_INT` int(11) DEFAULT NULL,
  `tela_tipo_id_INT` int(11) DEFAULT NULL,
  `is_android_BOOLEAN` int(1) DEFAULT NULL,
  `is_web_BOOLEAN` int(1) DEFAULT NULL,
  `configuracao_json` tinytext,
  `data_criacao_SEC` int(10) DEFAULT NULL,
  `data_criacao_OFFSEC` int(6) DEFAULT NULL,
  `data_modificacao_SEC` int(10) DEFAULT NULL,
  `data_modificacao_OFFSEC` int(6) DEFAULT NULL,
  `modificacao_usuario_id_INT` int(11) DEFAULT NULL,
  `cricacao_usuario_id_INT` int(11) DEFAULT NULL,
  `ativa_BOOLEAN` int(1) DEFAULT NULL,
  `permissao_id_INT` int(11) DEFAULT NULL COMMENT 'Permissão do sistema android que corresponde a tela',
  `area_menu_id_INT` int(11) DEFAULT NULL COMMENT 'Área do menu web correspondente a tela',
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tela_FK_465026855` (`modificacao_usuario_id_INT`),
  KEY `tela_FK_720916748` (`cricacao_usuario_id_INT`),
  KEY `tela_estado_id_INT` (`tela_estado_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `tela_FK_119079589` (`tela_tipo_id_INT`),
  KEY `tela_FK_137664795` (`permissao_id_INT`),
  KEY `tela_FK_272827148` (`area_menu_id_INT`),
  CONSTRAINT `tela_FK_119079589` FOREIGN KEY (`tela_tipo_id_INT`) REFERENCES `tela_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_FK_137664795` FOREIGN KEY (`permissao_id_INT`) REFERENCES `permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_FK_272827148` FOREIGN KEY (`area_menu_id_INT`) REFERENCES `area_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_FK_465026855` FOREIGN KEY (`modificacao_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_FK_720916748` FOREIGN KEY (`cricacao_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_ibfk_1` FOREIGN KEY (`tela_estado_id_INT`) REFERENCES `tela_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tela
-- ----------------------------

-- ----------------------------
-- Table structure for `tela_componente`
-- ----------------------------
DROP TABLE IF EXISTS `tela_componente`;
CREATE TABLE `tela_componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `configuracao_json` tinytext,
  `sistema_campo_id_INT` int(11) DEFAULT NULL,
  `tela_id_INT` int(11) DEFAULT NULL,
  `tela_sistema_lista_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `tela_tipo_componente_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campo_FK_222290039` (`tela_id_INT`),
  KEY `sistema_campo_id_INT` (`sistema_campo_id_INT`),
  KEY `tela_sistema_lista_id_INT` (`tela_sistema_lista_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `tela_componente_FK_168670654` (`tela_tipo_componente_id_INT`),
  CONSTRAINT `campo_FK_222290039` FOREIGN KEY (`tela_id_INT`) REFERENCES `tela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_componente_FK_168670654` FOREIGN KEY (`tela_tipo_componente_id_INT`) REFERENCES `tela_tipo_componente` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_componente_ibfk_1` FOREIGN KEY (`sistema_campo_id_INT`) REFERENCES `sistema_campo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_componente_ibfk_2` FOREIGN KEY (`tela_sistema_lista_id_INT`) REFERENCES `tela_sistema_lista` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_componente_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tela_componente
-- ----------------------------

-- ----------------------------
-- Table structure for `tela_estado`
-- ----------------------------
DROP TABLE IF EXISTS `tela_estado`;
CREATE TABLE `tela_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tela_estado
-- ----------------------------
INSERT INTO `tela_estado` VALUES ('1', 'Construção');
INSERT INTO `tela_estado` VALUES ('2', 'Homologação');
INSERT INTO `tela_estado` VALUES ('3', 'Produção');

-- ----------------------------
-- Table structure for `tela_sistema_lista`
-- ----------------------------
DROP TABLE IF EXISTS `tela_sistema_lista`;
CREATE TABLE `tela_sistema_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tela_id_INT` int(11) DEFAULT NULL,
  `sistema_lista_id_INT` int(11) DEFAULT NULL,
  `is_principal_BOOLEAN` int(1) DEFAULT NULL,
  `tipo_cardinalidade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tela_id_INT` (`tela_id_INT`),
  KEY `tipo_cardinalidade_id_INT` (`tipo_cardinalidade_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `tela_sistema_lista_FK_907806397` (`sistema_lista_id_INT`),
  CONSTRAINT `tela_sistema_lista_FK_907806397` FOREIGN KEY (`sistema_lista_id_INT`) REFERENCES `sistema_lista` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_sistema_lista_ibfk_1` FOREIGN KEY (`tela_id_INT`) REFERENCES `tela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_sistema_lista_ibfk_3` FOREIGN KEY (`tipo_cardinalidade_id_INT`) REFERENCES `tipo_cardinalidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tela_sistema_lista_ibfk_4` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tela_sistema_lista
-- ----------------------------

-- ----------------------------
-- Table structure for `tela_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `tela_tipo`;
CREATE TABLE `tela_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tela_tipo
-- ----------------------------
INSERT INTO `tela_tipo` VALUES ('1', 'formulario', null);
INSERT INTO `tela_tipo` VALUES ('2', 'lista', null);
INSERT INTO `tela_tipo` VALUES ('3', 'filtro', null);

-- ----------------------------
-- Table structure for `tela_tipo_componente`
-- ----------------------------
DROP TABLE IF EXISTS `tela_tipo_componente`;
CREATE TABLE `tela_tipo_componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tela_tipo_componente
-- ----------------------------
INSERT INTO `tela_tipo_componente` VALUES ('1', 'radio button', null);
INSERT INTO `tela_tipo_componente` VALUES ('2', 'drop down', null);
INSERT INTO `tela_tipo_componente` VALUES ('3', 'input text', null);
INSERT INTO `tela_tipo_componente` VALUES ('4', 'check box', null);
INSERT INTO `tela_tipo_componente` VALUES ('5', 'input text numeric', null);
INSERT INTO `tela_tipo_componente` VALUES ('6', 'text area', null);
INSERT INTO `tela_tipo_componente` VALUES ('7', 'html', null);
INSERT INTO `tela_tipo_componente` VALUES ('8', 'datetime select', null);

-- ----------------------------
-- Table structure for `tipo_anexo`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_anexo`;
CREATE TABLE `tipo_anexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_anexo
-- ----------------------------
INSERT INTO `tipo_anexo` VALUES ('1', 'foto');
INSERT INTO `tipo_anexo` VALUES ('2', 'audio');
INSERT INTO `tipo_anexo` VALUES ('3', 'filme');
INSERT INTO `tipo_anexo` VALUES ('4', 'arquivo');

-- ----------------------------
-- Table structure for `tipo_canal_envio`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_canal_envio`;
CREATE TABLE `tipo_canal_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_canal_envio
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_cardinalidade`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_cardinalidade`;
CREATE TABLE `tipo_cardinalidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_cardinalidade
-- ----------------------------
INSERT INTO `tipo_cardinalidade` VALUES ('1', '1 para 1', '1');
INSERT INTO `tipo_cardinalidade` VALUES ('2', '1 para N', '2');
INSERT INTO `tipo_cardinalidade` VALUES ('3', 'M para N', '3');

-- ----------------------------
-- Table structure for `tipo_documento`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_documento_FK_958312989` (`corporacao_id_INT`),
  CONSTRAINT `tipo_documento_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_documento
-- ----------------------------
INSERT INTO `tipo_documento` VALUES ('1', 'CNPJ', 'CNPJ', '1');
INSERT INTO `tipo_documento` VALUES ('2', 'CNPJ', 'CNPJ', '3');
INSERT INTO `tipo_documento` VALUES ('3', 'CPF', 'CPF', '3');
INSERT INTO `tipo_documento` VALUES ('4', 'RG', 'RG', '3');
INSERT INTO `tipo_documento` VALUES ('5', 'CNPJ', 'CNPJ', '4');
INSERT INTO `tipo_documento` VALUES ('6', 'CPF', 'CPF', '4');
INSERT INTO `tipo_documento` VALUES ('7', 'RG', 'RG', '4');
INSERT INTO `tipo_documento` VALUES ('8', 'CNPJ', 'CNPJ', '5');
INSERT INTO `tipo_documento` VALUES ('9', 'CPF', 'CPF', '5');
INSERT INTO `tipo_documento` VALUES ('10', 'RG', 'RG', '5');

-- ----------------------------
-- Table structure for `tipo_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_empresa`;
CREATE TABLE `tipo_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_93486ui` (`nome`,`corporacao_id_INT`) USING BTREE,
  KEY `tipo_empresa_FK_221710205` (`corporacao_id_INT`),
  CONSTRAINT `tipo_empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_empresa
-- ----------------------------
INSERT INTO `tipo_empresa` VALUES ('1', 'MARKETING E PUBLICIDADE', 'MARKETING E PUBLICIDADE', '1');
INSERT INTO `tipo_empresa` VALUES ('2', 'CONSTRUTORA', 'CONSTRUTORA', '1');

-- ----------------------------
-- Table structure for `tipo_nota`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_nota`;
CREATE TABLE `tipo_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_nota_FK_962524415` (`corporacao_id_INT`),
  CONSTRAINT `tipo_nota_FK_962524415` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_nota
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_ponto`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_ponto`;
CREATE TABLE `tipo_ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_ponto
-- ----------------------------
INSERT INTO `tipo_ponto` VALUES ('1', 'Jornada de Trabalho');
INSERT INTO `tipo_ponto` VALUES ('3', 'Intervalo');

-- ----------------------------
-- Table structure for `tipo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_registro`;
CREATE TABLE `tipo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_registro
-- ----------------------------
INSERT INTO `tipo_registro` VALUES ('1', 'tarefa');
INSERT INTO `tipo_registro` VALUES ('2', 'compra');
INSERT INTO `tipo_registro` VALUES ('3', 'venda');
INSERT INTO `tipo_registro` VALUES ('4', 'negociacao_divida');
INSERT INTO `tipo_registro` VALUES ('5', 'recebivel');
INSERT INTO `tipo_registro` VALUES ('6', 'despesa');
INSERT INTO `tipo_registro` VALUES ('7', 'ponto');
INSERT INTO `tipo_registro` VALUES ('8', 'wifi');
INSERT INTO `tipo_registro` VALUES ('9', 'nota');

-- ----------------------------
-- Table structure for `tipo_relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_relatorio`;
CREATE TABLE `tipo_relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_tarefa`;
CREATE TABLE `tipo_tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_tarefa
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_veiculo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_veiculo_registro`;
CREATE TABLE `tipo_veiculo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_veiculo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `uf`
-- ----------------------------
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `nome_normalizado` varchar(255) DEFAULT NULL,
  `sigla` char(2) DEFAULT NULL,
  `pais_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome2` (`nome_normalizado`,`corporacao_id_INT`,`pais_id_INT`),
  KEY `uf_FK_724060059` (`corporacao_id_INT`),
  KEY `pais_id_INT` (`pais_id_INT`),
  CONSTRAINT `uf_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `uf_ibfk_2` FOREIGN KEY (`pais_id_INT`) REFERENCES `pais` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uf
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `status_BOOLEAN` int(1) DEFAULT NULL,
  `pagina_inicial` text,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_categoria_permissao`;
CREATE TABLE `usuario_categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_id_INT` (`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_categoria_permissao_FK_734741211` (`categoria_permissao_id_INT`),
  KEY `usuario_categoria_permissao_FK_776031494` (`corporacao_id_INT`),
  CONSTRAINT `usuario_categoria_permissao_ibfk_3` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_categoria_permissao_ibfk_8` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_categoria_permissao_ibfk_9` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_corporacao`;
CREATE TABLE `usuario_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `is_adm_BOOLEAN` int(1) NOT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_corporacao_UNIQUE` (`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_corporacao_FK_972991944` (`usuario_id_INT`),
  KEY `usuario_corporacao_FK_135986328` (`corporacao_id_INT`),
  CONSTRAINT `usuario_corporacao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_corporacao_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_empresa`;
CREATE TABLE `usuario_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_id_INT` (`usuario_id_INT`,`empresa_id_INT`,`corporacao_id_INT`),
  KEY `key_usua869446` (`usuario_id_INT`) USING BTREE,
  KEY `key_usua240082` (`empresa_id_INT`) USING BTREE,
  KEY `key_usua498779` (`corporacao_id_INT`) USING BTREE,
  CONSTRAINT `usuario_empresa_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_empresa_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_empresa_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_foto`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_foto`;
CREATE TABLE `usuario_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `foto_interna` varchar(50) DEFAULT NULL,
  `foto_externa` varchar(50) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_foto_FK_640136719` (`usuario_id_INT`),
  KEY `usuario_foto_FK_401641846` (`corporacao_id_INT`),
  CONSTRAINT `usuario_foto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_foto_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_foto
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_foto_configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_foto_configuracao`;
CREATE TABLE `usuario_foto_configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_camera_interna_BOOLEAN` int(1) NOT NULL,
  `periodo_segundo_INT` int(7) NOT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_foto_configuracao_FK_765228272` (`usuario_id_INT`),
  KEY `usuario_foto_configuracao_FK_640441895` (`corporacao_id_INT`),
  CONSTRAINT `usuario_foto_configuracao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_foto_configuracao_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_foto_configuracao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_horario_trabalho`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_horario_trabalho`;
CREATE TABLE `usuario_horario_trabalho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio_TIME` time DEFAULT NULL,
  `hora_fim_TIME` time DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `dia_semana_INT` int(2) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `template_JSON` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_hora478455` (`corporacao_id_INT`) USING BTREE,
  KEY `usuario_horario_trabalho_FK_788391114` (`empresa_id_INT`),
  KEY `usuario_horario_trabalho_FK_905120850` (`usuario_id_INT`),
  CONSTRAINT `usuario_horario_trabalho_FK_788391114` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_horario_trabalho_FK_905120850` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_horario_trabalho_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_horario_trabalho
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_menu`;
CREATE TABLE `usuario_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `area_menu` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_menu_FK_522369385` (`usuario_id_INT`),
  KEY `usuario_menu_FK_986877442` (`corporacao_id_INT`),
  CONSTRAINT `usuario_menu_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_menu_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_posicao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_posicao`;
CREATE TABLE `usuario_posicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(11) NOT NULL,
  `longitude_INT` int(11) NOT NULL,
  `velocidade_INT` int(5) DEFAULT NULL,
  `foto_interna` varchar(30) DEFAULT NULL,
  `foto_externa` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `fonte_informacao_INT` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_posicao_FK_244567871` (`corporacao_id_INT`),
  KEY `usuario_posicao_FK_205352783` (`usuario_id_INT`),
  KEY `usuario_posicao_FK_64941406` (`veiculo_usuario_id_INT`),
  CONSTRAINT `usuario_posicao_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_posicao_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_posicao_ibfk_3` FOREIGN KEY (`veiculo_usuario_id_INT`) REFERENCES `veiculo_usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_posicao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_privilegio`;
CREATE TABLE `usuario_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_privilegio_FK_689819336` (`usuario_id_INT`),
  KEY `usuario_privilegio_FK_879547120` (`corporacao_id_INT`),
  CONSTRAINT `usuario_privilegio_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_privilegio_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_servico`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_servico`;
CREATE TABLE `usuario_servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_BOOLEAN` int(1) NOT NULL,
  `servico_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servico_id_INT` (`servico_id_INT`,`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_servico_FK_158416748` (`usuario_id_INT`),
  KEY `usuario_servico_FK_909790039` (`corporacao_id_INT`),
  CONSTRAINT `usuario_servico_FK_777465821` FOREIGN KEY (`servico_id_INT`) REFERENCES `servico` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_servico_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_servico_ibfk_9` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_servico
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo`;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_visivel` varchar(100) DEFAULT NULL,
  `status_BOOLEAN` int(1) DEFAULT NULL,
  `pagina_inicial` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_tipo_UNIQUE` (`nome`,`corporacao_id_INT`),
  KEY `usuario_tipo_FK_915130616` (`corporacao_id_INT`),
  CONSTRAINT `usuario_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_corporacao`;
CREATE TABLE `usuario_tipo_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_tipo_corporacao_UNIQUE` (`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_tipo_corporacao_FK_644226074` (`usuario_id_INT`),
  KEY `usuario_tipo_corporacao_FK_503631592` (`usuario_tipo_id_INT`),
  KEY `usuario_tipo_corporacao_FK_788208008` (`corporacao_id_INT`),
  CONSTRAINT `usuario_tipo_corporacao_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_corporacao_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_corporacao_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_menu`;
CREATE TABLE `usuario_tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `area_menu` varchar(255) NOT NULL,
  `area_menu_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_tipo_menu_FK_356384277` (`usuario_tipo_id_INT`),
  KEY `usuario_tipo_menu_FK_503875732` (`corporacao_id_INT`),
  KEY `area_menu_id_INT` (`area_menu_id_INT`),
  CONSTRAINT `usuario_tipo_menu_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_menu_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_menu_ibfk_3` FOREIGN KEY (`area_menu_id_INT`) REFERENCES `area_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_privilegio`;
CREATE TABLE `usuario_tipo_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_tipo_privilegio_FK_228912353` (`usuario_tipo_id_INT`),
  KEY `usuario_tipo_privilegio_FK_250061035` (`corporacao_id_INT`),
  CONSTRAINT `usuario_tipo_privilegio_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_privilegio_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `veiculo`
-- ----------------------------
DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE `veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placa` varchar(20) NOT NULL,
  `modelo_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `placa` (`placa`,`corporacao_id_INT`),
  KEY `veiculo_FK_363189697` (`corporacao_id_INT`),
  KEY `veiculo_FK_481506348` (`modelo_id_INT`),
  CONSTRAINT `veiculo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_ibfk_2` FOREIGN KEY (`modelo_id_INT`) REFERENCES `modelo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of veiculo
-- ----------------------------

-- ----------------------------
-- Table structure for `veiculo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `veiculo_registro`;
CREATE TABLE `veiculo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entrada_BOOLEAN` int(1) DEFAULT NULL,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `quilometragem_INT` int(11) DEFAULT NULL,
  `observacao` varchar(512) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `tipo_veiculo_registro_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `veiculo_usuario_id_INT` (`veiculo_usuario_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `veiculo_registro_FK_612823486` (`tipo_veiculo_registro_id_INT`),
  CONSTRAINT `veiculo_registro_FK_612823486` FOREIGN KEY (`tipo_veiculo_registro_id_INT`) REFERENCES `tipo_veiculo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_registro_ibfk_1` FOREIGN KEY (`veiculo_usuario_id_INT`) REFERENCES `veiculo_usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_registro_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of veiculo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `veiculo_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `veiculo_usuario`;
CREATE TABLE `veiculo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `is_ativo_BOOLEAN` int(1) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_id_INT` (`usuario_id_INT`,`veiculo_id_INT`,`corporacao_id_INT`),
  KEY `veiculo_usuario_FK_266174316` (`veiculo_id_INT`),
  KEY `veiculo_usuario_FK_358489990` (`corporacao_id_INT`),
  CONSTRAINT `veiculo_usuario_ibfk_4` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_usuario_ibfk_5` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_usuario_ibfk_6` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of veiculo_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `wifi`
-- ----------------------------
DROP TABLE IF EXISTS `wifi`;
CREATE TABLE `wifi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) DEFAULT NULL,
  `rssi` int(11) DEFAULT NULL,
  `ssid` varchar(30) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `senha` varchar(30) DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  `link_speed` int(11) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `bssid` varchar(30) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `contador_verificacao_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_id_INT` (`empresa_id_INT`),
  KEY `pessoa_id_INT` (`pessoa_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  CONSTRAINT `wifi_ibfk_1` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_ibfk_2` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wifi
-- ----------------------------

-- ----------------------------
-- Table structure for `wifi_registro`
-- ----------------------------
DROP TABLE IF EXISTS `wifi_registro`;
CREATE TABLE `wifi_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wifi_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `data_inicio_SEC` int(10) DEFAULT NULL,
  `data_inicio_OFFSEC` int(6) DEFAULT NULL,
  `data_fim_SEC` int(10) DEFAULT NULL,
  `data_fim_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wifi_registro_ibfk_2` (`corporacao_id_INT`),
  KEY `wifi_registro_ibfk_1` (`usuario_id_INT`),
  KEY `wifi_registro_FK_898468018` (`wifi_id_INT`),
  CONSTRAINT `wifi_registro_FK_898468018` FOREIGN KEY (`wifi_id_INT`) REFERENCES `wifi` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_registro_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_registro_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wifi_registro
-- ----------------------------

-- ----------------------------
-- Procedure structure for `initNextval`
-- ----------------------------
DROP PROCEDURE IF EXISTS `initNextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `initNextval`(IN ptabela VARCHAR(128), IN totalRegs int(11), OUT id INT(11) )
begin
	
	declare x int(1);
  
	UPDATE sistema_sequencia SET id_ultimo_INT = LAST_INSERT_ID(id_ultimo_INT + totalRegs) WHERE tabela = ptabela  ;
	set x = row_count();
	SELECT LAST_INSERT_ID() into id;

		if(x=0) THEN
			
			SET @c2 = '';
			SET @t1 = CONCAT( 'SELECT MAX(id) INTO @c2 FROM ', ptabela ); 
			PREPARE stmt3 FROM @t1;
			EXECUTE stmt3 ;
			DEALLOCATE PREPARE stmt3;
			
			if(@c2 is null) then
				INSERT INTO sistema_sequencia (`tabela`, `id_ultimo_INT`) values(ptabela, 0);
				UPDATE sistema_sequencia SET id_ultimo_INT = LAST_INSERT_ID(id_ultimo_INT + totalRegs) WHERE tabela = ptabela  ;
				SELECT LAST_INSERT_ID() into id;
			else 
				INSERT INTO sistema_sequencia (`tabela`, `id_ultimo_INT`) values(ptabela, @c2);
				UPDATE sistema_sequencia SET id_ultimo_INT = LAST_INSERT_ID(id_ultimo_INT + totalRegs) WHERE tabela = ptabela  ;
				SELECT LAST_INSERT_ID() into id;
			end if;
		end if;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `nextval`
-- ----------------------------
DROP FUNCTION IF EXISTS `nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextval`(ptabela VARCHAR(128), totalRegs int(11) ) RETURNS int(11)
begin
	declare id int(11);
	declare x int(1);
	UPDATE sistema_sequencia SET id_ultimo_INT = LAST_INSERT_ID(id_ultimo_INT + totalRegs) WHERE tabela = ptabela  ;
	set x = row_count();
	SELECT LAST_INSERT_ID() into id;
	
	IF(x=0) THEN
		return 0;
	END IF;
	return id;
end
;;
DELIMITER ;
