/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : sincronizador_web_corporacao_hmg

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-02-19 23:43:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acesso`
-- ----------------------------
DROP TABLE IF EXISTS `acesso`;
CREATE TABLE `acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `data_login_DATETIME` datetime NOT NULL,
  `data_logout_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `crud_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acesso_FK_260284424` (`usuario_id_INT`),
  KEY `acesso_FK_107116699` (`crud_id_INT`),
  CONSTRAINT `acesso_ibfk_1` FOREIGN KEY (`crud_id_INT`) REFERENCES `crud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acesso_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of acesso
-- ----------------------------

-- ----------------------------
-- Table structure for `corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `corporacao`;
CREATE TABLE `corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `usuario_dropbox` varchar(255) DEFAULT NULL,
  `senha_dropbox` varchar(255) DEFAULT NULL,
  `ultima_migracao_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporacao_nome_UNIQUE` (`nome_normalizado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `crud`
-- ----------------------------
DROP TABLE IF EXISTS `crud`;
CREATE TABLE `crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) NOT NULL,
  `id_tabela_web_INT` int(11) DEFAULT NULL,
  `tipo_operacao_banco_id_INT` int(2) NOT NULL,
  `crud_origem_id_INT` int(11) DEFAULT NULL,
  `sincronizacao_id_INT` int(11) DEFAULT NULL,
  `id_sistema_registro_sincronizador_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `crud_FK_480834961` (`sistema_tabela_id_INT`),
  KEY `crud_FK_100250244` (`tipo_operacao_banco_id_INT`),
  KEY `crud_FK_100891113` (`crud_origem_id_INT`),
  KEY `crud_FK_967620850` (`sincronizacao_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  CONSTRAINT `crud_ibfk_1` FOREIGN KEY (`tipo_operacao_banco_id_INT`) REFERENCES `tipo_operacao_banco` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_ibfk_2` FOREIGN KEY (`crud_origem_id_INT`) REFERENCES `crud_origem` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_ibfk_3` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_ibfk_4` FOREIGN KEY (`sincronizacao_id_INT`) REFERENCES `sincronizacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_ibfk_5` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crud
-- ----------------------------

-- ----------------------------
-- Table structure for `crud_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `crud_mobile`;
CREATE TABLE `crud_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) NOT NULL,
  `id_tabela_mobile_INT` int(11) DEFAULT NULL,
  `id_tabela_web_INT` int(11) DEFAULT NULL,
  `id_tabela_web_duplicada_INT` int(11) DEFAULT NULL,
  `id_sincronizador_mobile_INT` int(11) DEFAULT NULL,
  `tipo_operacao_banco_id_INT` int(2) NOT NULL,
  `sincronizacao_mobile_id_INT` int(11) DEFAULT NULL,
  `crud_id_INT` int(11) DEFAULT NULL,
  `processada_BOOLEAN` int(1) DEFAULT NULL,
  `data_processamento_DATETIME` datetime DEFAULT NULL,
  `erro_sql_INT` int(5) DEFAULT NULL,
  `corporacao_id_INT` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `crud_mobile_FK_958709717` (`sistema_tabela_id_INT`),
  KEY `crud_mobile_FK_391723633` (`tipo_operacao_banco_id_INT`),
  KEY `crud_mobile_FK_12115478` (`sincronizacao_mobile_id_INT`),
  KEY `crud_mobile_FK_619201660` (`crud_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  CONSTRAINT `crud_mobile_ibfk_1` FOREIGN KEY (`sincronizacao_mobile_id_INT`) REFERENCES `sincronizacao_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_mobile_ibfk_2` FOREIGN KEY (`tipo_operacao_banco_id_INT`) REFERENCES `tipo_operacao_banco` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_mobile_ibfk_3` FOREIGN KEY (`crud_id_INT`) REFERENCES `crud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_mobile_ibfk_4` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crud_mobile_ibfk_5` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crud_mobile
-- ----------------------------

-- ----------------------------
-- Table structure for `crud_mobile_dependente`
-- ----------------------------
DROP TABLE IF EXISTS `crud_mobile_dependente`;
CREATE TABLE `crud_mobile_dependente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `id_sincronizador_mobile_INT` int(11) DEFAULT NULL,
  `atributo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crud_mobile_dependente_FK_628112793` (`crud_mobile_id_INT`),
  CONSTRAINT `crud_mobile_dependente_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crud_mobile_dependente
-- ----------------------------

-- ----------------------------
-- Table structure for `crud_origem`
-- ----------------------------
DROP TABLE IF EXISTS `crud_origem`;
CREATE TABLE `crud_origem` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crud_origem
-- ----------------------------
INSERT INTO `crud_origem` VALUES ('1', 'MOBILE');
INSERT INTO `crud_origem` VALUES ('2', 'WEB');

-- ----------------------------
-- Table structure for `erro`
-- ----------------------------
DROP TABLE IF EXISTS `erro`;
CREATE TABLE `erro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `codigo_erro` int(11) NOT NULL,
  `mensagem_erro` text NOT NULL,
  `url` text,
  `arquivo_erro` varchar(255) DEFAULT NULL,
  `linha_erro` varchar(80) DEFAULT NULL,
  `get` text,
  `post` text,
  `session` text,
  `stacktrace` mediumtext,
  `datahora_DATETIME` datetime NOT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erro_FK_300537109` (`usuario_id_INT`),
  CONSTRAINT `erro_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of erro
-- ----------------------------

-- ----------------------------
-- Table structure for `estado_sincronizacao`
-- ----------------------------
DROP TABLE IF EXISTS `estado_sincronizacao`;
CREATE TABLE `estado_sincronizacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_sincronizacao
-- ----------------------------
INSERT INTO `estado_sincronizacao` VALUES ('1', 'INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO');
INSERT INTO `estado_sincronizacao` VALUES ('2', 'PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB');
INSERT INTO `estado_sincronizacao` VALUES ('3', 'ERRO_SINCRONIZACAO_MOBILE');
INSERT INTO `estado_sincronizacao` VALUES ('4', 'ERRO_SINCRONIZACAO');
INSERT INTO `estado_sincronizacao` VALUES ('5', 'INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO');
INSERT INTO `estado_sincronizacao` VALUES ('6', 'GERANDO ARQUIVO DE DADOS WEB PARA CELULAR');
INSERT INTO `estado_sincronizacao` VALUES ('7', 'FINALIZADO');
INSERT INTO `estado_sincronizacao` VALUES ('8', 'RESETADO');

-- ----------------------------
-- Table structure for `estado_sincronizacao_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `estado_sincronizacao_mobile`;
CREATE TABLE `estado_sincronizacao_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_sincronizacao_mobile
-- ----------------------------
INSERT INTO `estado_sincronizacao_mobile` VALUES ('-3', 'OCORREU_UM_ERRO_FATAL');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('1', 'FILA_ESPERA_PARA_SINCRONIZACAO');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('2', 'INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('3', 'PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('4', 'OCORREU_UM_ERRO_LEVE_QUE_SO_ENVIARA_OS_DADOS_LOCAIS_QUANDO_RESETAR');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('5', 'AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('6', 'AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('7', 'FINALIZADO');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('8', 'RESETADO');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('9', 'CANCELADO');
INSERT INTO `estado_sincronizacao_mobile` VALUES ('10', 'MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO');

-- ----------------------------
-- Table structure for `operacao_sistema`
-- ----------------------------
DROP TABLE IF EXISTS `operacao_sistema`;
CREATE TABLE `operacao_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `tipo_operacao` varchar(50) DEFAULT NULL,
  `pagina_operacao` varchar(255) DEFAULT NULL,
  `entidade_operacao` varchar(50) DEFAULT NULL,
  `chave_registro_operacao_INT` int(11) DEFAULT NULL,
  `descricao_operacao` varchar(255) DEFAULT NULL,
  `data_operacao_DATETIME` datetime NOT NULL,
  `url_completa` varchar(1000) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario_id_INT`),
  CONSTRAINT `operacao_sistema_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operacao_sistema
-- ----------------------------

-- ----------------------------
-- Table structure for `sincronizacao`
-- ----------------------------
DROP TABLE IF EXISTS `sincronizacao`;
CREATE TABLE `sincronizacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_inicial_DATETIME` datetime DEFAULT NULL,
  `data_final_DATETIME` datetime DEFAULT NULL,
  `limite_mobile_INT` int(11) DEFAULT NULL,
  `estado_sincronizacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_ARQUIVO` varchar(255) DEFAULT NULL,
  `erro` varchar(255) DEFAULT NULL,
  `id_ultimo_sistema_registro_sincronizador_INT` int(11) DEFAULT NULL,
  `quantidade_crud_processado_INT` int(11) DEFAULT NULL,
  `processando_BOOLEAN` int(1) DEFAULT NULL,
  `data_ultimo_processamento_DATETIME` datetime DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sincronizacao_FK_598144531` (`estado_sincronizacao_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  CONSTRAINT `sincronizacao_ibfk_1` FOREIGN KEY (`estado_sincronizacao_id_INT`) REFERENCES `estado_sincronizacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sincronizacao_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sincronizacao
-- ----------------------------

-- ----------------------------
-- Table structure for `sincronizacao_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `sincronizacao_mobile`;
CREATE TABLE `sincronizacao_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_identificador_INT` int(11) DEFAULT NULL,
  `mobile_conectado_INT` int(11) DEFAULT NULL,
  `estado_sincronizacao_mobile_id_INT` int(11) DEFAULT NULL,
  `data_inicial_DATETIME` datetime DEFAULT NULL,
  `data_final_DATETIME` datetime DEFAULT NULL,
  `resetando_BOOLEAN` int(1) DEFAULT NULL,
  `sincronizacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL COMMENT 'posicao do mobile na sincronizacao',
  `descricao` varchar(512) DEFAULT NULL,
  `crud_mobile_ARQUIVO` varchar(50) DEFAULT NULL,
  `crud_web_para_mobile_ARQUIVO` varchar(50) DEFAULT NULL,
  `ultimo_crud_id_INT` int(11) DEFAULT NULL COMMENT 'O ├║ltimo id do crud que foi realizada a ultima sincroniza├º├úo do celular',
  `ultima_verificacao_DATETIME` datetime DEFAULT NULL,
  `erro` varchar(512) DEFAULT NULL,
  `erro_sincronizacao_id_INT` int(11) DEFAULT NULL,
  `primeira_vez_BOOLEAN` int(1) DEFAULT NULL,
  `id_primeiro_sinc_android_web_INT` int(11) DEFAULT NULL,
  `id_ultimo_sinc_android_web_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sincronizacao_mobile_FK_35949707` (`estado_sincronizacao_mobile_id_INT`) USING BTREE,
  KEY `sincronizacao_mobile_FK_139007568` (`sincronizacao_id_INT`) USING BTREE,
  KEY `sincronizacao_mobile_FK_231689453` (`ultimo_crud_id_INT`) USING BTREE,
  KEY `sincronizacao_mobile_FK_5889892` (`erro_sincronizacao_id_INT`) USING BTREE,
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  CONSTRAINT `sincronizacao_mobile_ibfk_1` FOREIGN KEY (`sincronizacao_id_INT`) REFERENCES `sincronizacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sincronizacao_mobile_ibfk_2` FOREIGN KEY (`ultimo_crud_id_INT`) REFERENCES `crud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sincronizacao_mobile_ibfk_3` FOREIGN KEY (`estado_sincronizacao_mobile_id_INT`) REFERENCES `estado_sincronizacao_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sincronizacao_mobile_ibfk_4` FOREIGN KEY (`erro_sincronizacao_id_INT`) REFERENCES `sincronizacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sincronizacao_mobile_ibfk_5` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sincronizacao_mobile
-- ----------------------------

-- ----------------------------
-- Table structure for `sincronizacao_mobile_iteracao`
-- ----------------------------
DROP TABLE IF EXISTS `sincronizacao_mobile_iteracao`;
CREATE TABLE `sincronizacao_mobile_iteracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sincronizacao_mobile_id_INT` int(11) DEFAULT NULL,
  `estado_sincronizacao_mobile_id_INT` int(11) DEFAULT NULL,
  `ocorrencia_DATETIME` datetime DEFAULT NULL,
  `enum_origem_chamada` char(1) DEFAULT NULL COMMENT 'Tipo enum',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sincronizacao_mobile_iteracao
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_atributo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_atributo`;
CREATE TABLE `sistema_atributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) DEFAULT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `tipo_sql` varchar(15) DEFAULT NULL,
  `tipo_sql_ficticio` varchar(50) DEFAULT NULL,
  `tamanho_INT` int(11) DEFAULT NULL,
  `decimal_INT` int(11) DEFAULT NULL,
  `not_null_BOOLEAN` int(1) DEFAULT NULL,
  `primary_key_BOOLEAN` int(1) DEFAULT NULL,
  `auto_increment_BOOLEAN` int(1) DEFAULT NULL,
  `valor_default` varchar(100) DEFAULT NULL,
  `fk_sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `atributo_fk` varchar(100) DEFAULT NULL,
  `fk_nome` varchar(255) DEFAULT NULL,
  `update_tipo_fk` varchar(30) DEFAULT NULL,
  `delete_tipo_fk` varchar(30) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `unique_BOOLEAN` int(1) DEFAULT NULL,
  `unique_nome` varchar(255) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`,`sistema_tabela_id_INT`) USING BTREE,
  KEY `sa_sistema_tabela_FK` (`sistema_tabela_id_INT`) USING BTREE,
  CONSTRAINT `sistema_atributo_ibfk_1` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1314 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_atributo
-- ----------------------------
INSERT INTO `sistema_atributo` VALUES ('1', 'id', null, '466', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('2', 'data_login_DATETIME', null, '466', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('3', 'data_logout_DATETIME', null, '466', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('4', 'usuario_id_INT', null, '466', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'acesso_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('5', 'id', null, '1', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('6', 'nome', null, '1', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('7', 'nome_normalizado', null, '1', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome234234', null, '0');
INSERT INTO `sistema_atributo` VALUES ('8', 'cidade_id_INT', null, '1', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'bairro_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'nome234234', null, '0');
INSERT INTO `sistema_atributo` VALUES ('9', 'corporacao_id_INT', null, '1', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'bairro_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome234234', null, '0');
INSERT INTO `sistema_atributo` VALUES ('10', 'id', null, '2', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('11', 'nome', null, '2', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('12', 'nome_normalizado', null, '2', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_Aasd', null, '0');
INSERT INTO `sistema_atributo` VALUES ('13', 'corporacao_id_INT', null, '2', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'categoria_permissao_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_Aasd', null, '0');
INSERT INTO `sistema_atributo` VALUES ('14', 'id', null, '4', null, null, '3', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('15', 'nome', null, '4', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('16', 'nome_normalizado', null, '4', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_736', null, '0');
INSERT INTO `sistema_atributo` VALUES ('17', 'corporacao_id_INT', null, '4', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'cidade_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_736', null, '0');
INSERT INTO `sistema_atributo` VALUES ('18', 'uf_id_INT', null, '4', null, null, '11', null, '0', '0', '0', null, '13', 'id', 'cidade_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'nome_736', null, '0');
INSERT INTO `sistema_atributo` VALUES ('19', 'id', null, '49', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('20', 'nome', null, '49', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('21', 'nome_normalizado', null, '49', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'corporacao_nome_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('22', 'usuario_dropbox', null, '49', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('23', 'senha_dropbox', null, '49', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('24', 'id', null, '5', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('25', 'nome', null, '5', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('26', 'nome_normalizado', null, '5', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('27', 'telefone1', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('28', 'telefone2', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('29', 'fax', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('30', 'celular', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('31', 'operadora_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '77', 'id', 'empresa_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('32', 'celular_sms', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('33', 'email', null, '5', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'email', null, '0');
INSERT INTO `sistema_atributo` VALUES ('34', 'tipo_documento_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '23', 'id', 'empresa_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('35', 'numero_documento', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('36', 'tipo_empresa_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '12', 'id', 'empresa_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('37', 'logradouro', null, '5', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('38', 'logradouro_normalizado', null, '5', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('39', 'numero', null, '5', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('40', 'complemento', null, '5', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('41', 'complemento_normalizado', null, '5', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('42', 'bairro_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '1', 'id', 'empresa_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('43', 'cidade_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'empresa_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('44', 'latitude_INT', null, '5', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('45', 'longitude_INT', null, '5', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('46', 'foto', null, '5', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('47', 'corporacao_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'email', null, '0');
INSERT INTO `sistema_atributo` VALUES ('48', 'id', null, '54', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('49', 'forma_pagamento_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '143', 'id', 'empresa_compra_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('50', 'valor_total_FLOAT', null, '54', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('51', 'data_DATE', null, '54', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('52', 'hora_TIME', null, '54', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('53', 'minha_empresa_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_compra_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('54', 'outra_empresa_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_compra_ibfk_7', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('55', 'foto', null, '54', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('56', 'corporacao_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_compra_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('57', 'usuario_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'empresa_compra_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('58', 'id', null, '55', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('59', 'empresa_compra_id_INT', null, '55', null, null, '11', null, '0', '0', '0', null, '54', 'id', 'sdhgfgh', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('60', 'data_DATE', null, '55', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('61', 'valor_FLOAT', null, '55', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('62', 'corporacao_id_INT', null, '55', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_compra_parcela_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('63', 'id', null, '24', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('64', 'empresa_id_INT', null, '24', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_perfil_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'empresa_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('65', 'perfil_id_INT', null, '24', null, null, '11', null, '0', '0', '0', null, '30', 'id', 'empresa_perfil_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'empresa_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('66', 'corporacao_id_INT', null, '24', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_perfil_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'empresa_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('67', 'id', null, '56', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('68', 'identificador', null, '56', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('69', 'nome', null, '56', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('70', 'nome_normalizado', null, '56', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('71', 'descricao', null, '56', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('72', 'empresa_produto_tipo_id_INT', null, '56', null, null, '11', null, '0', '0', '0', null, '71', 'id', 'empresa_produto_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('73', 'empresa_produto_unidade_medida_id_INT', null, '56', null, null, '11', null, '0', '0', '0', null, '59', 'id', 'empresa_produto_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('74', 'empresa_id_INT', null, '56', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_produto_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('75', 'video', null, '56', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('76', 'corporacao_id_INT', null, '56', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_produto_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('77', 'id', null, '57', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('78', 'empresa_produto_id_INT', null, '57', null, null, '11', null, '0', '0', '0', null, '56', 'id', 'empresa_produto_compra_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('79', 'empresa_compra_id_INT', null, '57', null, null, '11', null, '0', '0', '0', null, '54', 'id', 'empresa_produto_compra_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('80', 'quantidade_FLOAT', null, '57', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('81', 'valor_total_FLOAT', null, '57', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('82', 'corporacao_id_INT', null, '57', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_produto_compra_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('83', 'id', null, '58', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('84', 'empresa_produto_id_INT', null, '58', null, null, '11', null, '0', '0', '0', null, '56', 'id', 'empresa_produto_foto_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('85', 'foto', null, '58', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('86', 'corporacao_id_INT', null, '58', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_produto_foto_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('87', 'id', null, '71', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('88', 'nome', null, '71', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_98765', null, '1');
INSERT INTO `sistema_atributo` VALUES ('89', 'nome_normalizado', null, '71', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('90', 'corporacao_id_INT', null, '71', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_produto_tipo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_98765', null, '1');
INSERT INTO `sistema_atributo` VALUES ('91', 'id', null, '59', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('92', 'nome', null, '59', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('93', 'abreviacao', null, '59', null, null, '10', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('94', 'is_float_BOOLEAN', null, '59', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('95', 'id', null, '60', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('96', 'empresa_produto_id_INT', null, '60', null, null, '11', null, '0', '0', '0', null, '56', 'id', 'empresa_produto_venda_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('97', 'empresa_venda_id_INT', null, '60', null, null, '11', null, '0', '0', '0', null, '68', 'id', 'empresa_produto_venda_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('98', 'quantidade_FLOAT', null, '60', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('99', 'valor_total_FLOAT', null, '60', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('100', 'corporacao_id_INT', null, '60', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_produto_venda_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('101', 'id', null, '61', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('102', 'identificador', null, '61', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('103', 'nome', null, '61', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('104', 'nome_normalizado', null, '61', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('105', 'descricao', null, '61', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('106', 'empresa_servico_tipo_id_INT', null, '61', null, null, '11', null, '0', '0', '0', null, '72', 'id', 'empresa_servico_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('107', 'empresa_id_INT', null, '61', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_servico_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('108', 'empresa_servico_unidade_medida_id_INT', null, '61', null, null, '11', null, '0', '0', '0', null, '64', 'id', 'empresa_servico_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('109', 'prazo_entrega_dia_INT', null, '61', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('110', 'duracao_meses_INT', null, '61', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('111', 'video', null, '61', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('112', 'corporacao_id_INT', null, '61', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_servico_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('113', 'id', null, '62', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('114', 'empresa_servico_id_INT', null, '62', null, null, '11', null, '0', '0', '0', null, '61', 'id', 'empresa_servico_compra_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('115', 'empresa_compra_id_INT', null, '62', null, null, '11', null, '0', '0', '0', null, '54', 'id', 'empresa_servico_compra_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('116', 'quantidade_FLOAT', null, '62', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('117', 'valor_total_FLOAT', null, '62', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('118', 'corporacao_id_INT', null, '62', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_servico_compra_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('119', 'id', null, '63', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('120', 'empresa_servico_id_INT', null, '63', null, null, '11', null, '0', '0', '0', null, '61', 'id', 'empresa_servico_foto_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('121', 'foto', null, '63', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('122', 'corporacao_id_INT', null, '63', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_servico_foto_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('123', 'id', null, '72', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('124', 'nome', null, '72', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_098787', null, '1');
INSERT INTO `sistema_atributo` VALUES ('125', 'nome_normalizado', null, '72', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('126', 'corporacao_id_INT', null, '72', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_servico_tipo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_098787', null, '1');
INSERT INTO `sistema_atributo` VALUES ('127', 'id', null, '64', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('128', 'nome', null, '64', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('129', 'id', null, '65', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('130', 'empresa_servico_id_INT', null, '65', null, null, '11', null, '0', '0', '0', null, '61', 'id', 'empresa_servico_venda_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('131', 'empresa_venda_id_INT', null, '65', null, null, '11', null, '0', '0', '0', null, '68', 'id', 'empresa_servico_venda_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('132', 'quantidade_FLOAT', null, '65', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('133', 'valor_total_FLOAT', null, '65', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('134', 'corporacao_id_INT', null, '65', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_servico_venda_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('135', 'id', null, '66', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('136', 'empresa_produto_id_INT', null, '66', null, null, '11', null, '0', '0', '0', null, '56', 'id', 'empresa_tipo_venda_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('137', 'empresa_servico_id_INT', null, '66', null, null, '11', null, '0', '0', '0', null, '61', 'id', 'empresa_tipo_venda_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('138', 'valor_total_FLOAT', null, '66', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('139', 'corporacao_id_INT', null, '66', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_tipo_venda_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('140', 'id', null, '67', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('141', 'empresa_tipo_venda_id_INT', null, '67', null, null, '11', null, '0', '0', '0', null, '66', 'id', 'empresa_tipo_venda_parcela_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('142', 'porcentagem_FLOAT', null, '67', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('143', 'corporacao_id_INT', null, '67', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_tipo_venda_parcela_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('144', 'id', null, '68', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('145', 'forma_pagamento_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '143', 'id', 'empresa_venda_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('146', 'valor_total_FLOAT', null, '68', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('147', 'data_DATE', null, '68', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('148', 'hora_TIME', null, '68', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('149', 'cliente_empresa_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_venda_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('150', 'fornecedor_empresa_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_venda_ibfk_7', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('151', 'pessoa_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'empresa_venda_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('152', 'foto', null, '68', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('153', 'corporacao_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_venda_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('154', 'usuario_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'empresa_venda_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('155', 'id', null, '69', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('156', 'empresa_venda_id_INT', null, '69', null, null, '11', null, '0', '0', '0', null, '68', 'id', 'empresa_venda_parcela_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('157', 'data_DATE', null, '69', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('158', 'valor_FLOAT', null, '69', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('159', 'corporacao_id_INT', null, '69', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_venda_parcela_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('160', 'id', null, '126', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('161', 'nome', null, '126', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_63545', null, '0');
INSERT INTO `sistema_atributo` VALUES ('162', 'id', null, '143', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('163', 'nome', null, '143', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('164', 'nome_normalizado', null, '143', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('165', 'corporacao_id_INT', null, '143', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'forma_pagamento_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('166', 'id', null, '441', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('167', 'hora_inicio_TIME', null, '441', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('168', 'hora_fim_TIME', null, '441', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('169', 'pessoa_empresa_id_INT', null, '441', null, null, '11', null, '0', '0', '0', null, '7', 'id', 'horario_trabalho_pessoa_empresa_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('170', 'data_DATE', null, '441', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('171', 'corporacao_id_INT', null, '441', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'horario_trabalho_pessoa_empresa_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('172', 'id', null, '8', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('173', 'nome', null, '8', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('174', 'nome_normalizado', null, '8', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('175', 'ano_INT', null, '8', null, null, '4', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('176', 'corporacao_id_INT', null, '8', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'modelo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('177', 'id', null, '467', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('178', 'tipo_operacao', null, '467', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('179', 'pagina_operacao', null, '467', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('180', 'entidade_operacao', null, '467', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('181', 'chave_registro_operacao_INT', null, '467', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('182', 'descricao_operacao', null, '467', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('183', 'data_operacao_DATETIME', null, '467', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('184', 'url_completa', null, '467', null, null, '1000', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('185', 'usuario_id_INT', null, '467', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'operacao_sistema_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('186', 'id', null, '77', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('187', 'nome', null, '77', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('188', 'nome_normalizado', null, '77', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_operadora_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('189', 'id', null, '22', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('190', 'nome', null, '22', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('191', 'nome_normalizado', null, '22', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_83746', null, '0');
INSERT INTO `sistema_atributo` VALUES ('192', 'corporacao_id_INT', null, '22', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'pais_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_83746', null, '0');
INSERT INTO `sistema_atributo` VALUES ('193', 'id', null, '30', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('194', 'nome', null, '30', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_perfil_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('195', 'id', null, '46', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('196', 'nome', null, '46', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('197', 'tag', null, '46', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'tag', null, '0');
INSERT INTO `sistema_atributo` VALUES ('198', 'pai_permissao_id_INT', null, '46', null, null, '11', null, '0', '0', '0', null, '46', 'id', 'permissao_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('199', 'id', null, '3', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('200', 'permissao_id_INT', null, '3', null, null, '11', null, '1', '0', '0', null, '46', 'id', 'permissao_categoria_permissao_ibfk_9', 'CASCADE', 'CASCADE', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('201', 'categoria_permissao_id_INT', null, '3', null, null, '11', null, '1', '0', '0', null, '2', 'id', 'permissao_categoria_permissao_ibfk_4', 'CASCADE', 'CASCADE', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('202', 'corporacao_id_INT', null, '3', null, null, '11', null, '1', '0', '0', null, '49', 'id', 'permissao_categoria_permissao_ibfk_7', 'CASCADE', 'CASCADE', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('203', 'id', null, '6', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('204', 'identificador', null, '6', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('205', 'nome', null, '6', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('206', 'nome_normalizado', null, '6', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('207', 'tipo_documento_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '23', 'id', 'pessoa_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('208', 'numero_documento', null, '6', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('209', 'is_consumidor_BOOLEAN', null, '6', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('210', 'sexo_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '39', 'id', 'pessoa_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('211', 'telefone', null, '6', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('212', 'celular', null, '6', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('213', 'operadora_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '77', 'id', 'pessoa_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('214', 'celular_sms', null, '6', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('215', 'email', null, '6', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_95867', null, '0');
INSERT INTO `sistema_atributo` VALUES ('216', 'logradouro', null, '6', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('217', 'logradouro_normalizado', null, '6', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('218', 'numero', null, '6', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('219', 'complemento', null, '6', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('220', 'complemento_normalizado', null, '6', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('221', 'cidade_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'pessoa_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('222', 'bairro_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '1', 'id', 'pessoa_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('223', 'latitude_INT', null, '6', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('224', 'longitude_INT', null, '6', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('225', 'foto', null, '6', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('226', 'corporacao_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'pessoa_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_95867', null, '0');
INSERT INTO `sistema_atributo` VALUES ('227', 'id', null, '7', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('228', 'pessoa_id_INT', null, '7', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'pessoa_empresa_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('229', 'empresa_id_INT', null, '7', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'pessoa_empresa_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('230', 'profissao_id_INT', null, '7', null, null, '11', null, '0', '0', '0', null, '10', 'id', 'pessoa_empresa_ibfk_4', 'CASCADE', 'SET NULL', null, '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('231', 'corporacao_id_INT', null, '7', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'pessoa_empresa_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'pessoa_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('232', 'id', null, '85', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('233', 'pessoa_empresa_id_INT', null, '85', null, null, '11', null, '0', '0', '0', null, '7', 'id', 'pessoa_empresa_rotina_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('234', 'semana_INT', null, '85', null, null, '4', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('235', 'dia_semana_INT', null, '85', null, null, '4', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('236', 'corporacao_id_INT', null, '85', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'pessoa_empresa_rotina_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('237', 'id', null, '87', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('238', 'pessoa_id_INT', null, '87', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'pessoa_usuario_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'pessoa_usuario_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('239', 'corporacao_id_INT', null, '87', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'pessoa_usuario_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'pessoa_usuario_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('240', 'usuario_id_INT', null, '87', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'pessoa_usuario_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'pessoa_usuario_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('241', 'id', null, '9', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('242', 'pessoa_id_INT', null, '9', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'ponto_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('243', 'hora_TIME', null, '9', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('244', 'dia_DATE', null, '9', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('245', 'foto', null, '9', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('246', 'empresa_id_INT', null, '9', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'ponto_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('247', 'profissao_id_INT', null, '9', null, null, '11', null, '0', '0', '0', null, '10', 'id', 'ponto_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('248', 'is_entrada_BOOLEAN', null, '9', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('249', 'latitude_INT', null, '9', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('250', 'longitude_INT', null, '9', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('251', 'tipo_ponto_id_INT', null, '9', null, null, '11', null, '0', '0', '0', null, '82', 'id', 'ponto_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('252', 'corporacao_id_INT', null, '9', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'ponto_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('253', 'usuario_id_INT', null, '9', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'ponto_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('254', 'id', null, '468', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('255', 'ponto_id_INT', null, '468', null, null, '11', null, '0', '0', '0', null, '9', 'id', 'ponto_endereco_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('256', 'endereco_completo', null, '468', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('257', 'id', null, '10', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('258', 'nome', null, '10', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('259', 'nome_normalizado', null, '10', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_34677654', null, '0');
INSERT INTO `sistema_atributo` VALUES ('260', 'corporacao_id_INT', null, '10', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'profissao_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_34677654', null, '0');
INSERT INTO `sistema_atributo` VALUES ('261', 'id', null, '83', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('262', 'nome', null, '83', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('263', 'nome_normalizado', null, '83', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('264', 'corporacao_id_INT', null, '83', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'rede_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('265', 'id', null, '84', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('266', 'rede_id_INT', null, '84', null, null, '11', null, '0', '0', '0', null, '83', 'id', 'rede_empresa_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'rede_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('267', 'empresa_id_INT', null, '84', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'rede_empresa_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'rede_empresa_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('268', 'corporacao_id_INT', null, '84', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'rede_empresa_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('269', 'id', null, '89', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('270', 'titulo', null, '89', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('271', 'titulo_normalizado', null, '89', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('272', 'descricao', null, '89', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('273', 'descricao_normalizado', null, '89', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('274', 'pessoa_id_INT', null, '89', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'relatorio_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('275', 'empresa_id_INT', null, '89', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'relatorio_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('276', 'hora_TIME', null, '89', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('277', 'dia_DATE', null, '89', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('278', 'corporacao_id_INT', null, '89', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'relatorio_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('279', 'usuario_id_INT', null, '89', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'relatorio_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('280', 'id', null, '91', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('281', 'relatorio_id_INT', null, '91', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'relatorio_anexo_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('282', 'arquivo', null, '91', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('283', 'tipo_anexo_id_INT', null, '91', null, null, '11', null, '0', '0', '0', null, '90', 'id', 'relatorio_anexo_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('284', 'corporacao_id_INT', null, '91', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'relatorio_anexo_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('285', 'id', null, '86', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('286', 'ultima_dia_contagem_ciclo_DATE', null, '86', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('287', 'corporacao_id_INT', null, '86', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'rotina_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('288', 'id', null, '31', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('289', 'nome', null, '31', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'tag', null, '0');
INSERT INTO `sistema_atributo` VALUES ('290', 'tag', null, '31', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('291', 'id', null, '39', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('292', 'nome', null, '39', null, null, '30', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('293', 'id', null, '445', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('294', 'nome', null, '445', null, null, '128', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_irtu', null, '0');
INSERT INTO `sistema_atributo` VALUES ('295', 'sistema_tabela_id_INT', null, '445', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sa_sistema_tabela_FK', 'CASCADE', 'SET NULL', null, '1', 'nome_irtu', null, '0');
INSERT INTO `sistema_atributo` VALUES ('296', 'tipo_sql', null, '445', null, null, '15', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('297', 'tamanho_INT', null, '445', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('298', 'decimal_INT', null, '445', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('299', 'not_null_BOOLEAN', null, '445', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('300', 'primary_key_BOOLEAN', null, '445', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('301', 'auto_increment_BOOLEAN', null, '445', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('302', 'valor_default', null, '445', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('303', 'fk_sistema_tabela_id_INT', null, '445', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_atributo_FK_622131348', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('304', 'atributo_fk', null, '445', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('305', 'fk_nome', null, '445', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('306', 'update_tipo_fk', null, '445', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('307', 'delete_tipo_fk', null, '445', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('308', 'label', null, '445', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('309', 'unique_BOOLEAN', null, '445', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('310', 'unique_nome', null, '445', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('311', 'seq_INT', null, '445', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('312', 'excluido_BOOLEAN', null, '445', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('313', 'id', null, '448', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('314', 'nome', null, '448', null, null, '250', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('315', 'valor_string', null, '448', null, null, '250', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('316', 'valor_INT', null, '448', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('317', 'valor_DATETIME', null, '448', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('318', 'valor_DATE', null, '448', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('319', 'valor_FLOAT', null, '448', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('320', 'valor_BOOLEAN', null, '448', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('321', 'id', null, '469', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('322', 'projetos_versao_INT', null, '469', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('323', 'data_homologacao_DATETIME', null, '469', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('324', 'data_producao_DATETIME', null, '469', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('325', 'id', null, '470', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('326', 'sistema_tabela_id_INT', null, '470', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_registro_sincronizador_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('327', 'id_tabela_INT', null, '470', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('328', 'tipo_operacao_banco_id_INT', null, '470', null, null, '2', null, '0', '0', '0', null, '171', 'id', 'sistema_registro_sincronizador_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('329', 'corporacao_id_INT', null, '470', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_registro_sincronizador_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('330', 'is_from_android_BOOLEAN', null, '470', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('331', 'imei', null, '470', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('332', 'sistema_produto_INT', null, '470', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('333', 'sistema_projetos_versao_INT', null, '470', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('334', 'usuario_id_INT', null, '470', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'sistema_registro_sincronizador_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('335', 'id', null, '139', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('336', 'nome', null, '139', null, null, '128', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome', null, '0');
INSERT INTO `sistema_atributo` VALUES ('337', 'data_modificacao_estrutura_DATETIME', null, '139', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('338', 'frequencia_sincronizador_INT', null, '139', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('339', 'banco_mobile_BOOLEAN', null, '139', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('340', 'banco_web_BOOLEAN', null, '139', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('341', 'transmissao_web_para_mobile_BOOLEAN', null, '139', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('342', 'transmissao_mobile_para_web_BOOLEAN', null, '139', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('343', 'tabela_sistema_BOOLEAN', null, '139', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('344', 'excluida_BOOLEAN', null, '139', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('345', 'atributos_chave_unica', null, '139', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('346', 'id', null, '343', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('347', 'nome', null, '343', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'sistema_tipo_download_arquivo_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('348', 'path', null, '343', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('349', 'id', null, '171', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('350', 'nome', null, '171', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_84756', null, '0');
INSERT INTO `sistema_atributo` VALUES ('351', 'id', null, '11', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('352', 'categoria_permissao_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '2', 'id', 'tarefa_ibfk_7', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('353', 'origem_pessoa_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'tarefa_ibfk_8', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('354', 'origem_empresa_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'tarefa_ibfk_15', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('355', 'origem_logradouro', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('356', 'origem_logradouro_normalizado', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('357', 'origem_numero', null, '11', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('358', 'origem_complemento', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('359', 'origem_complemento_normalizado', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('360', 'origem_cidade_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'tarefa_ibfk_14', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('361', 'origem_bairro_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '1', 'id', 'tarefa_ibfk_9', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('362', 'origem_latitude_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('363', 'origem_longitude_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('364', 'origem_latitude_real_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('365', 'origem_longitude_real_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('366', 'destino_pessoa_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'tarefa_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('367', 'destino_empresa_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'tarefa_ibfk_12', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('368', 'destino_logradouro', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('369', 'destino_logradouro_normalizado', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('370', 'destino_numero', null, '11', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('371', 'destino_complemento', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('372', 'destino_complemento_normalizado', null, '11', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('373', 'destino_cidade_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'tarefa_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('374', 'destino_bairro_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '1', 'id', 'tarefa_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('375', 'destino_latitude_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('376', 'destino_longitude_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('377', 'destino_latitude_real_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('378', 'destino_longitude_real_INT', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('379', 'tempo_estimado_carro_INT', null, '11', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('380', 'tempo_estimado_a_pe_INT', null, '11', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('381', 'distancia_estimada_carro_INT', null, '11', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('382', 'distancia_estimada_a_pe_INT', null, '11', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('383', 'is_realizada_a_pe_BOOLEAN', null, '11', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('384', 'inicio_data_programada_DATE', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('385', 'inicio_hora_programada_TIME', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('386', 'data_exibir_DATE', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('387', 'inicio_data_DATE', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('388', 'inicio_hora_TIME', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('389', 'fim_data_DATE', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('390', 'fim_hora_TIME', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('391', 'data_cadastro_DATE', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('392', 'hora_cadastro_TIME', null, '11', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('393', 'titulo', null, '11', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('394', 'titulo_normalizado', null, '11', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('395', 'descricao', null, '11', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('396', 'descricao_normalizado', null, '11', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('397', 'corporacao_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tarefa_ibfk_11', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('398', 'criado_pelo_usuario_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('399', 'veiculo_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '20', 'id', 'tarefa_ibfk_10', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('400', 'usuario_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_ibfk_13', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('401', 'veiculo_usuario_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '21', 'id', 'tarefa_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('402', 'id', null, '90', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('403', 'nome', null, '90', null, null, '30', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('404', 'id', null, '23', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('405', 'nome', null, '23', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('406', 'nome_normalizado', null, '23', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('407', 'corporacao_id_INT', null, '23', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tipo_documento_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('408', 'id', null, '12', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('409', 'nome', null, '12', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_93486ui', null, '0');
INSERT INTO `sistema_atributo` VALUES ('410', 'nome_normalizado', null, '12', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('411', 'corporacao_id_INT', null, '12', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tipo_empresa_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_93486ui', null, '0');
INSERT INTO `sistema_atributo` VALUES ('412', 'id', null, '82', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('413', 'nome', null, '82', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('414', 'id', null, '13', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('415', 'nome', null, '13', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('416', 'nome_normalizado', null, '13', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome2', null, '0');
INSERT INTO `sistema_atributo` VALUES ('417', 'sigla', null, '13', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('418', 'pais_id_INT', null, '13', null, null, '11', null, '0', '0', '0', null, '22', 'id', 'uf_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'nome2', null, '0');
INSERT INTO `sistema_atributo` VALUES ('419', 'corporacao_id_INT', null, '13', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'uf_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome2', null, '0');
INSERT INTO `sistema_atributo` VALUES ('420', 'id', null, '14', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('421', 'nome', null, '14', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('422', 'nome_normalizado', null, '14', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('423', 'email', null, '14', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'email', null, '0');
INSERT INTO `sistema_atributo` VALUES ('424', 'senha', null, '14', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('425', 'status_BOOLEAN', null, '14', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('426', 'pagina_inicial', null, '14', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('427', 'id', null, '15', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('428', 'usuario_id_INT', null, '15', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_categoria_permissao_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('429', 'categoria_permissao_id_INT', null, '15', null, null, '11', null, '0', '0', '0', null, '2', 'id', 'usuario_categoria_permissao_ibfk_8', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('430', 'corporacao_id_INT', null, '15', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_categoria_permissao_ibfk_9', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('431', 'id', null, '16', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('432', 'usuario_id_INT', null, '16', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_corporacao_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'usuario_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('433', 'corporacao_id_INT', null, '16', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_corporacao_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'usuario_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('434', 'status_BOOLEAN', null, '16', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('435', 'is_adm_BOOLEAN', null, '16', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('436', 'id', null, '471', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('437', 'usuario_id_INT', null, '471', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_empresa_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('438', 'empresa_id_INT', null, '471', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'usuario_empresa_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('439', 'corporacao_id_INT', null, '471', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_empresa_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('440', 'id', null, '17', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('441', 'data_DATE', null, '17', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('442', 'hora_TIME', null, '17', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('443', 'foto_interna', null, '17', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('444', 'foto_externa', null, '17', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('445', 'usuario_id_INT', null, '17', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_foto_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('446', 'corporacao_id_INT', null, '17', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_foto_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('447', 'id', null, '168', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('448', 'is_camera_interna_BOOLEAN', null, '168', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('449', 'periodo_segundo_INT', null, '168', null, null, '7', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('450', 'usuario_id_INT', null, '168', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_foto_configuracao_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('451', 'corporacao_id_INT', null, '168', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_foto_configuracao_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('452', 'id', null, '472', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('453', 'usuario_id_INT', null, '472', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_menu_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('454', 'area_menu', null, '472', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('455', 'corporacao_id_INT', null, '472', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_menu_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('456', 'id', null, '18', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('457', 'data_DATE', null, '18', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('458', 'hora_TIME', null, '18', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('459', 'veiculo_usuario_id_INT', null, '18', null, null, '11', null, '0', '0', '0', null, '21', 'id', 'usuario_posicao_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('460', 'usuario_id_INT', null, '18', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_posicao_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('461', 'latitude_INT', null, '18', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('462', 'longitude_INT', null, '18', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('463', 'velocidade_INT', null, '18', null, null, '5', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('464', 'foto_interna', null, '18', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('465', 'foto_externa', null, '18', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('466', 'corporacao_id_INT', null, '18', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_posicao_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('467', 'id', null, '473', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('468', 'usuario_id_INT', null, '473', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_privilegio_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('469', 'identificador_funcionalidade', null, '473', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('470', 'corporacao_id_INT', null, '473', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_privilegio_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('471', 'id', null, '19', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('472', 'status_BOOLEAN', null, '19', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('473', 'servico_id_INT', null, '19', null, null, '11', null, '0', '0', '0', null, '31', 'id', 'usuario_servico_FK_777465821', 'CASCADE', 'SET NULL', null, '1', 'servico_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('474', 'usuario_id_INT', null, '19', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_servico_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'servico_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('475', 'corporacao_id_INT', null, '19', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_servico_ibfk_9', 'CASCADE', 'SET NULL', null, '1', 'servico_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('476', 'id', null, '73', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('477', 'nome', null, '73', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'usuario_tipo_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('478', 'nome_visivel', null, '73', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('479', 'status_BOOLEAN', null, '73', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('480', 'pagina_inicial', null, '73', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('481', 'corporacao_id_INT', null, '73', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_tipo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'usuario_tipo_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('482', 'id', null, '75', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('483', 'usuario_id_INT', null, '75', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_tipo_corporacao_ibfk_2', 'CASCADE', 'SET NULL', null, '1', 'usuario_tipo_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('484', 'usuario_tipo_id_INT', null, '75', null, null, '11', null, '0', '0', '0', null, '73', 'id', 'usuario_tipo_corporacao_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('485', 'corporacao_id_INT', null, '75', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_tipo_corporacao_ibfk_3', 'CASCADE', 'SET NULL', null, '1', 'usuario_tipo_corporacao_UNIQUE', null, '0');
INSERT INTO `sistema_atributo` VALUES ('486', 'id', null, '474', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('487', 'usuario_tipo_id_INT', null, '474', null, null, '11', null, '0', '0', '0', null, '73', 'id', 'usuario_tipo_menu_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('488', 'area_menu', null, '474', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('489', 'corporacao_id_INT', null, '474', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_tipo_menu_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('490', 'id', null, '475', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('491', 'usuario_tipo_id_INT', null, '475', null, null, '11', null, '0', '0', '0', null, '73', 'id', 'usuario_tipo_privilegio_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('492', 'identificador_funcionalidade', null, '475', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('493', 'corporacao_id_INT', null, '475', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_tipo_privilegio_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('494', 'id', null, '20', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('495', 'placa', null, '20', null, null, '20', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'placa', null, '0');
INSERT INTO `sistema_atributo` VALUES ('496', 'modelo_id_INT', null, '20', null, null, '11', null, '0', '0', '0', null, '8', 'id', 'veiculo_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('497', 'corporacao_id_INT', null, '20', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'veiculo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'placa', null, '0');
INSERT INTO `sistema_atributo` VALUES ('498', 'id', null, '446', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('499', 'entrada_BOOLEAN', null, '446', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('500', 'data_DATETIME', null, '446', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('501', 'latitude_INT', null, '446', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('502', 'longitude_INT', null, '446', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('503', 'corporacao_id_INT', null, '446', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'veiculo_registro_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('504', 'veiculo_usuario_id_INT', null, '446', null, null, '11', null, '0', '0', '0', null, '21', 'id', 'veiculo_registro_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('505', 'id', null, '21', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('506', 'usuario_id_INT', null, '21', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'veiculo_usuario_ibfk_5', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('507', 'data_DATE', null, '21', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('508', 'hora_TIME', null, '21', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('509', 'veiculo_id_INT', null, '21', null, null, '11', null, '0', '0', '0', null, '20', 'id', 'veiculo_usuario_ibfk_4', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('510', 'is_ativo_BOOLEAN', null, '21', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('511', 'corporacao_id_INT', null, '21', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'veiculo_usuario_ibfk_6', 'CASCADE', 'SET NULL', null, '1', 'usuario_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('512', 'id', null, '464', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('513', 'network_id', null, '464', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('514', 'rssi', null, '464', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('515', 'ssid', null, '464', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('516', 'usuario', null, '464', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('517', 'senha', null, '464', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('518', 'ip', null, '464', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('519', 'link_speed', null, '464', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('520', 'mac_address', null, '464', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('521', 'frequency', null, '464', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('522', 'bssid', null, '464', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('523', 'empresa_id_INT', null, '464', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'wifi_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('524', 'pessoa_id_INT', null, '464', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'wifi_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('525', 'data_DATETIME', null, '464', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('526', 'contador_verificacao_INT', null, '464', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('527', 'corporacao_id_INT', null, '464', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'wifi_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('528', 'id', null, '465', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('529', 'wifi_id_INT', null, '465', null, null, '11', null, '0', '0', '0', null, '464', 'id', 'wifi_registro_FK_898468018', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('530', 'data_inicio_DATETIME', null, '465', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('531', 'data_fim_DATETIME', null, '465', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('532', 'corporacao_id_INT', null, '465', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'wifi_registro_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('533', 'usuario_id_INT', null, '465', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'wifi_registro_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('534', 'id', null, '450', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('535', 'last_id_sincronizador_php_INT', null, '450', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('536', 'corporacao_id_INT', null, '450', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'corporacao_sincronizador_php_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('537', 'nome_exibicao', null, '139', null, null, '128', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('538', 'id', null, '451', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('539', 'data_DATE', null, '451', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('540', 'hora_TIME', null, '451', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('541', 'veiculo_usuario_id_INT', null, '451', null, null, '11', null, '0', '0', '0', null, '21', 'id', 'usuario_posicao_rastreamento_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('542', 'usuario_id_INT', null, '451', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_posicao_rastreamento_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('543', 'latitude_INT', null, '451', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('544', 'longitude_INT', null, '451', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('545', 'velocidade_INT', null, '451', null, null, '5', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('546', 'foto_interna', null, '451', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('547', 'foto_externa', null, '451', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('548', 'corporacao_id_INT', null, '451', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_posicao_rastreamento_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('549', 'id', null, '81', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('550', 'nome', null, '81', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('551', 'nome_normalizado', null, '81', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('552', 'data_lancamento_DATE', null, '81', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('553', 'link_download', null, '81', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('554', 'total_dia_trial_INT', null, '81', null, null, '5', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('555', 'desconto_FLOAT', null, '57', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('556', 'desconto_FLOAT', null, '68', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('557', 'id', null, '489', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('558', 'nome', null, '489', null, null, '256', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('559', 'descricao', null, '489', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('560', 'seq_INT', null, '489', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('561', 'id', null, '484', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('562', 'nome', null, '484', null, null, '252', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('563', 'id', null, '462', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('564', 'sistema_tabela_id_INT', null, '462', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('565', 'id_tabela_INT', null, '462', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('566', 'sistema_tipo_operacao_id_INT', null, '462', null, null, '11', null, '0', '0', '0', null, '171', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('567', 'usuario_id_INT', null, '462', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('568', 'corporacao_id_INT', null, '462', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_registro_sincronizador_android_para_web_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('569', 'imei', null, '462', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('570', 'sistema_produto_INT', null, '462', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('571', 'sistema_projetos_versao_INT', null, '462', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('572', 'id_tabela_web_INT', null, '462', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('573', 'locale', null, '444', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('574', 'desconto_FLOAT', null, '65', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('575', 'dia_semana_INT', null, '441', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('576', 'nome_exibicao', null, '445', null, null, '128', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('577', 'tipo_sql_ficticio', null, '445', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('578', 'id', null, '485', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('579', 'nome', null, '485', null, null, '256', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('580', 'descricao', null, '485', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('581', 'sistema_lista_id_INT', null, '485', null, null, '11', null, '0', '0', '0', null, '488', 'id', 'sistema_campo_FK_927398682', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('582', 'sistema_tipo_campo_id_INT', null, '485', null, null, '11', null, '0', '0', '0', null, '489', 'id', 'sistema_campo_FK_760070801', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('583', 'id', null, '486', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('584', 'sistema_campo_id_INT', null, '486', null, null, '11', null, '0', '0', '0', null, '485', 'id', 'sistema_campo_atributo_FK_405761719', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('585', 'sistema_atributo_id_INT', null, '486', null, null, '11', null, '0', '0', '0', null, '445', 'id', 'sistema_campo_atributo_FK_854766846', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('586', 'seq_INT', null, '486', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('587', 'id', null, '487', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('588', 'nome', null, '487', null, null, '215', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('589', 'id', null, '488', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('590', 'nome', null, '488', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('591', 'descricao', null, '488', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('592', 'sistema_tabela_id_INT', null, '488', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_lista_FK_318206787', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('593', 'sistema_estado_lista_id_INT', null, '488', null, null, '11', null, '0', '0', '0', null, '487', 'id', 'sistema_lista_FK_14404296', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('594', 'ultima_alteracao_usuario_id_INT', null, '488', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'sistema_lista_FK_571807861', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('595', 'ultima_alteracao_DATETIME', null, '488', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('596', 'criacao_DATETIME', null, '488', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('597', 'criador_usuario_id_INT', null, '488', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'sistema_lista_FK_563171387', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('598', 'id', null, '495', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('599', 'tabela', null, '495', null, null, '128', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('600', 'id_ultimo_INT', null, '495', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('601', 'id', null, '477', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('602', 'nome', null, '477', null, null, '150', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('603', 'tela_estado_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '491', 'id', 'tela_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('604', 'tela_tipo_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '493', 'id', 'tela_FK_119079589', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('605', 'is_android_BOOLEAN', null, '477', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('606', 'is_web_BOOLEAN', null, '477', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('607', 'configuracao_json', null, '477', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('608', 'data_criacao_DATETIME', null, '477', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('609', 'data_modificacao_DATETIME', null, '477', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('610', 'modificacao_usuario_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tela_FK_465026855', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('611', 'cricacao_usuario_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tela_FK_720916748', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('612', 'ativa_BOOLEAN', null, '477', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('613', 'permissao_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '46', 'id', 'tela_FK_137664795', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('614', 'area_menu_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '484', 'id', 'tela_FK_272827148', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('615', 'corporacao_id_INT', null, '477', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tela_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('616', 'id', null, '490', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('617', 'nome', null, '490', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('618', 'configuracao_json', null, '490', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('619', 'sistema_campo_id_INT', null, '490', null, null, '11', null, '0', '0', '0', null, '485', 'id', 'tela_componente_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('620', 'tela_id_INT', null, '490', null, null, '11', null, '0', '0', '0', null, '477', 'id', 'campo_FK_222290039', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('621', 'tela_sistema_lista_id_INT', null, '490', null, null, '11', null, '0', '0', '0', null, '492', 'id', 'tela_componente_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('622', 'seq_INT', null, '490', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('623', 'tela_tipo_componente_id_INT', null, '490', null, null, '11', null, '0', '0', '0', null, '494', 'id', 'tela_componente_FK_168670654', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('624', 'corporacao_id_INT', null, '490', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tela_componente_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('625', 'id', null, '491', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('626', 'nome', null, '491', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('627', 'id', null, '492', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('628', 'tela_id_INT', null, '492', null, null, '11', null, '0', '0', '0', null, '477', 'id', 'tela_sistema_lista_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('629', 'sistema_lista_id_INT', null, '492', null, null, '11', null, '0', '0', '0', null, '488', 'id', 'tela_sistema_lista_FK_907806397', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('630', 'is_principal_BOOLEAN', null, '492', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('631', 'tipo_cardinalidade_id_INT', null, '492', null, null, '11', null, '0', '0', '0', null, '482', 'id', 'tela_sistema_lista_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('632', 'corporacao_id_INT', null, '492', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tela_sistema_lista_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('633', 'id', null, '493', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('634', 'nome', null, '493', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('635', 'seq_INT', null, '493', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('636', 'id', null, '494', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('637', 'nome', null, '494', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('638', 'seq_INT', null, '494', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('639', 'id', null, '482', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('640', 'nome', null, '482', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('641', 'seq_INT', null, '482', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('642', 'area_menu_id_INT', null, '474', null, null, '11', null, '0', '0', '0', null, '484', 'id', 'usuario_tipo_menu_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('643', 'quilometragem_INT', null, '446', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('644', 'observacao', null, '446', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('645', 'desconto_FLOAT', null, '60', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('646', 'desconto_FLOAT', null, '62', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('647', 'id', null, '459', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('648', 'pai_sistema_grafo_hierarquia_banco_id_INT', null, '459', null, null, '11', null, '0', '0', '0', null, '453', 'id', 'sistema_aresta_grafo_hierarquia_banco_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('649', 'filho_sistema_grafo_hierarquia_banco_id_INT', null, '459', null, null, '11', null, '0', '0', '0', null, '453', 'id', 'sistema_aresta_grafo_hierarquia_banco_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('650', 'id', null, '238', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('651', 'nome', null, '238', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('652', 'data_inicio_estrutura_DATETIME', null, '238', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('653', 'data_fim_estrutura_DATETIME', null, '238', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('654', 'id', null, '458', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('655', 'dia_DATE', null, '458', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('656', 'corporacao_id_INT', null, '458', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_corporacao_sincronizador_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('657', 'id', null, '461', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('658', 'dia_DATE', null, '461', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('659', 'sistema_corporacao_sincronizador_id_INT', null, '461', null, null, '11', null, '0', '0', '0', null, '458', 'id', 'sistema_corporacao_sincronizador_tabela_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('660', 'sistema_tabela_id_INT', null, '461', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_corporacao_sincronizador_tabela_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('661', 'id', null, '496', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('662', 'id_crud_INT', null, '496', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('663', 'id_crud_mobile_INT', null, '496', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('664', 'id_estado_crud_INT', null, '496', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('665', 'id_estado_crud_mobile_INT', null, '496', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('666', 'sistema_tipo_operacao_banco_id_INT', null, '496', null, null, '11', null, '0', '0', '0', null, '171', 'id', 'sistema_crud_estado_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('667', 'corporacao_id_INT', null, '496', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_crud_estado_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('668', 'id', null, '447', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('669', 'nome', null, '447', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('670', 'path', null, '447', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('671', 'id', null, '453', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('672', 'grau_INT', null, '453', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('673', 'numero_dependentes_INT', null, '453', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('674', 'sistema_tabela_id_INT', null, '453', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_grafo_hierarquia_banco_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('675', 'id', null, '454', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('676', 'sistema_tabela_id_INT', null, '454', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_historico_sincronizador_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('677', 'id_tabela_INT', null, '454', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('678', 'sistema_tipo_operacao_id_INT', null, '454', null, null, '11', null, '0', '0', '0', null, '171', 'id', 'sistema_historico_sincronizador_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('679', 'data_DATETIME', null, '454', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('680', 'usuario_INT', null, '454', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('681', 'corporacao_id_INT', null, '454', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_historico_sincronizador_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('682', 'id', null, '455', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('683', 'operacao_sistema_mobile_id_INT', null, '455', null, null, '11', null, '0', '0', '0', null, '457', 'id', 'sistema_operacao_crud_aleatorio_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('684', 'total_insercao_INT', null, '455', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('685', 'porcentagem_remocao_FLOAT', null, '455', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('686', 'porcentagem_edicao_FLOAT', null, '455', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('687', 'sincronizar_BOOLEAN', null, '455', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('688', 'id', null, '460', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('689', 'path_script_sql_banco', null, '460', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('690', 'observacao', null, '460', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('691', 'operacao_sistema_mobile_id_INT', null, '460', null, null, '11', null, '0', '0', '0', null, '457', 'id', 'sistema_operacao_download_banco_mobile_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('692', 'popular_tabela_BOOLEAN', null, '460', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('693', 'drop_tabela_BOOLEAN', null, '460', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('694', 'destino_banco_id_INT', null, '460', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('695', 'id', null, '456', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('696', 'xml_script_sql_banco_ARQUIVO', null, '456', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('697', 'operacao_sistema_mobile_id_INT', null, '456', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('698', 'id', null, '457', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('699', 'tipo_operacao_sistema_id_INT', null, '457', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('700', 'estado_operacao_sistema_mobile_id_INT', null, '457', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('701', 'mobile_identificador_id_INT', null, '457', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('702', 'data_abertura_DATETIME', null, '457', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('703', 'data_processamento_DATETIME', null, '457', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('704', 'data_conclusao_DATETIME', null, '457', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('705', 'id', null, '449', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('706', 'classe', null, '449', null, null, '300', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('707', 'funcao', null, '449', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('708', 'linha_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('709', 'nome_arquivo', null, '449', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('710', 'identificador_erro', null, '449', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('711', 'descricao', null, '449', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('712', 'stacktrace', null, '449', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('713', 'data_visualizacao_DATETIME', null, '449', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('714', 'data_ocorrida_DATETIME', null, '449', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('715', 'sistema_tipo_log_erro_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('716', 'sistema_projetos_versao_produto_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('717', 'id_usuario_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('718', 'id_corporacao_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('719', 'sistema_log_identificador_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('720', 'sistema_projetos_versao_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('721', 'mobile_conectado_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('722', 'operacao_sistema_mobile_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('723', 'script_comando_banco_id_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('724', 'total_ocorrencia_INT', null, '449', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('725', 'sincronizado_BOOLEAN', null, '449', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('726', 'id', null, '497', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('727', 'crud', null, '497', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('728', 'sistema_registro_sincronizador_id_INT', null, '497', null, null, '11', null, '0', '0', '0', null, '462', 'id', 'sistema_registro_historico_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('729', 'sistema_tabela_id_INT', null, '497', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_registro_historico_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('730', 'id_tabela_INT', null, '497', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('731', 'corporacao_id_INT', null, '497', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_registro_historico_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('732', 'id', null, '463', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('733', 'sistema_tabela_id_INT', null, '463', null, null, '11', null, '0', '0', '0', null, '139', 'id', 'sistema_registro_sincronizador_web_para_android_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('734', 'id_tabela_INT', null, '463', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('735', 'sistema_tipo_operacao_id_INT', null, '463', null, null, '11', null, '0', '0', '0', null, '171', 'id', 'sistema_registro_sincronizador_web_para_android_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('736', 'corporacao_id_INT', null, '463', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_registro_sincronizador_web_para_android_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('737', 'imei', null, '463', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('738', 'sistema_produto_INT', null, '463', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('739', 'sistema_projetos_versao_INT', null, '463', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('740', 'id', null, '452', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('741', 'arquivo', null, '452', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('742', 'is_zip_BOOLEAN', null, '452', null, null, '1', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('743', 'corporacao_id_INT', null, '452', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_sincronizador_arquivo_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('744', 'id', null, '317', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('745', 'nome', null, '317', null, null, '50', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('746', 'id', null, '344', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('747', 'nome', null, '344', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('748', 'id', null, '337', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('749', 'tag', null, '337', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('750', 'mensagem', null, '337', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('751', 'sistema_tipo_usuario_mensagem_id_INT', null, '337', null, null, '11', null, '0', '0', '0', null, '344', 'id', 'sistema_usuario_mensagem_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('752', 'usuario_id_INT', null, '337', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'sistema_usuario_mensagem_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('753', 'corporacao_id_INT', null, '337', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'sistema_usuario_mensagem_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('754', 'id', null, '442', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('755', 'tarefa_id_INT', null, '442', null, null, '11', null, '0', '0', '0', null, '11', 'id', 'tarefa_acao_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('756', 'hora_TIME', null, '442', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('757', 'dia_DATE', null, '442', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('758', 'usuario_id_INT', null, '442', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_acao_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('759', 'is_entrada_BOOLEAN', null, '442', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('760', 'latitude_FLOAT', null, '442', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('761', 'longitude_FLOAT', null, '442', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('762', 'corporacao_id_INT', null, '442', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tarefa_acao_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('763', 'id', null, '498', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('764', 'nome', null, '498', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('765', 'empresa_id_INT', null, '498', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_equipe_FK_652496338', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('766', 'corporacao_id_INT', null, '498', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_equipe_FK_545715332', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('767', 'id_omega_INT', null, '56', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('768', 'id_omega_INT', null, '61', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('769', 'acrescimo_FLOAT', null, '68', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('770', 'id', null, '499', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('771', 'empresa_equipe_id_INT', null, '499', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('772', 'pessoa_empresa_id_INT', null, '499', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('773', 'corporacao_id_INT', null, '499', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('774', 'empresa_equipe_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '498', 'id', 'tarefa_FK_603546143', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('775', 'data_login_SEC', null, '466', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('776', 'data_login_OFFSEC', null, '466', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('777', 'data_logout_SEC', null, '466', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('778', 'data_logout_OFFSEC', null, '466', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('779', 'cadastro_SEC', null, '1', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('780', 'cadastro_OFFSEC', null, '1', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('781', 'cadastro_SEC', null, '5', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('782', 'cadastro_OFFSEC', null, '5', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('783', 'data_SEC', null, '54', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('784', 'data_OFFSEC', null, '54', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('785', 'data_SEC', null, '55', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('786', 'data_OFFSEC', null, '55', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('787', 'cadastro_SEC', null, '498', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('788', 'cadastro_OFFSEC', null, '498', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('789', 'cadastro_SEC', null, '56', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('790', 'cadastro_OFFSEC', null, '56', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('791', 'cadastro_SEC', null, '61', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('792', 'cadastro_OFFSEC', null, '61', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('793', 'data_SEC', null, '68', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('794', 'data_OFFSEC', null, '68', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('795', 'data_SEC', null, '69', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('796', 'data_OFFSEC', null, '69', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('797', 'data_SEC', null, '441', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('798', 'data_OFFSEC', null, '441', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('799', 'cadastro_SEC', null, '6', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('800', 'cadastro_OFFSEC', null, '6', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('801', 'data_SEC', null, '9', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('802', 'data_OFFSEC', null, '9', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('803', 'data_SEC', null, '89', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('804', 'data_OFFSEC', null, '89', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('805', 'ultima_dia_contagem_ciclo_data_SEC', null, '86', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('806', 'ultima_dia_contagem_ciclo_data_OFFSEC', null, '86', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('807', 'ultima_alteracao_SEC', null, '488', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('808', 'ultima_alteracao_OFFSEC', null, '488', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('809', 'criacao_SEC', null, '488', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('810', 'criacao_OFFSEC', null, '488', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('811', 'valor_SEC', null, '448', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('812', 'valor_OFFSEC', null, '448', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('813', 'id', null, '597', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('814', 'nome', null, '597', null, null, '250', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('815', 'valor_string', null, '597', null, null, '250', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('816', 'valor_INT', null, '597', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('817', 'valor_DATETIME', null, '597', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('818', 'valor_SEC', null, '597', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('819', 'valor_OFFSEC', null, '597', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('820', 'valor_DATE', null, '597', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('821', 'valor_FLOAT', null, '597', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('822', 'valor_BOOLEAN', null, '597', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('823', 'corporacao_id_INT', null, '597', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'spgc_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, null);
INSERT INTO `sistema_atributo` VALUES ('824', 'inicio_hora_programada_SEC', null, '11', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('825', 'inicio_hora_programada_OFFSEC', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('826', 'data_exibir_SEC', null, '11', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('827', 'data_exibir_OFFSEC', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('828', 'inicio_SEC', null, '11', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('829', 'inicio_OFFSEC', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('830', 'fim_SEC', null, '11', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('831', 'fim_OFFSEC', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('832', 'cadastro_SEC', null, '11', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('833', 'cadastro_OFFSEC', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('834', 'data_criacao_SEC', null, '477', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('835', 'data_criacao_OFFSEC', null, '477', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('836', 'data_modificacao_SEC', null, '477', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('837', 'data_modificacao_OFFSEC', null, '477', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('838', 'cadastro_SEC', null, '14', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('839', 'cadastro_OFFSEC', null, '14', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('840', 'cadastro_SEC', null, '16', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('841', 'cadastro_OFFSEC', null, '16', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('842', 'cadastro_SEC', null, '17', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('843', 'cadastro_OFFSEC', null, '17', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('844', 'cadastro_SEC', null, '18', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('845', 'cadastro_OFFSEC', null, '18', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('846', 'fonte_informacao_INT', null, '18', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('847', 'cadastro_SEC', null, '20', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('848', 'cadastro_OFFSEC', null, '20', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('849', 'data_SEC', null, '446', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('850', 'data_OFFSEC', null, '446', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('851', 'cadastro_SEC', null, '21', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('852', 'cadastro_OFFSEC', null, '21', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('853', 'data_SEC', null, '464', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('854', 'data_OFFSEC', null, '464', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('855', 'data_inicio_SEC', null, '465', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('856', 'data_inicio_OFFSEC', null, '465', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('857', 'data_fim_SEC', null, '465', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('858', 'data_fim_OFFSEC', null, '465', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('859', 'id_sincronizador_mobile_INT', null, '496', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('860', 'id_tabela_mobile_INT', null, '454', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('861', 'id', null, '598', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('862', 'operacao_sistema_mobile_id_INT', null, '598', null, null, '11', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('863', 'url_json_operacoes_web', null, '598', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('864', 'sincronizar_BOOLEAN', null, '598', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('865', 'id_tabela_mobile_INT', null, '497', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('866', 'id_tabela_mobile_INT', null, '462', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('867', 'criado_em_DATETIME', null, '462', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('868', 'sincronizando_BOOLEAN', null, '462', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('869', 'id', null, '599', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('870', 'nome', null, '599', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('871', 'id', null, '600', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('872', 'pessoa_id_INT', null, '600', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'api_id_FK_814331055', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('873', 'usuario_id_INT', null, '600', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'api_id_FK_701324463', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('874', 'empresa_id_INT', null, '600', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'api_id_FK_867980957', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('875', 'chave', null, '600', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('876', 'identificador', null, '600', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('877', 'cadastro_SEC', null, '600', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('878', 'cadastro_OFFSEC', null, '600', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('879', 'corporacao_id_INT', null, '600', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'api_id_FK_830413819', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('880', 'id', null, '601', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('881', 'identificador', null, '601', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('882', 'nome', null, '601', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('883', 'nome_normalizado', null, '601', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('884', 'descricao', null, '601', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('885', 'empresa_id_INT', null, '601', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'atividade_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('886', 'atividade_unidade_medida_id_INT', null, '601', null, null, '11', null, '0', '0', '0', null, '604', 'id', 'atividade_FK_219207763', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('887', 'prazo_entrega_dia_INT', null, '601', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('888', 'duracao_horas_INT', null, '601', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('889', 'id_omega_INT', null, '601', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('890', 'cadastro_SEC', null, '601', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('891', 'cadastro_OFFSEC', null, '601', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('892', 'corporacao_id_INT', null, '601', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'atividade_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('893', 'relatorio_id_INT', null, '601', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'atividade_FK_411010742', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('894', 'id', null, '602', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('895', 'nome', null, '602', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '1', 'nome_098787', null, '0');
INSERT INTO `sistema_atributo` VALUES ('896', 'nome_normalizado', null, '602', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('897', 'corporacao_id_INT', null, '602', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'atividade_tipo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_098787', null, '0');
INSERT INTO `sistema_atributo` VALUES ('898', 'id', null, '603', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('899', 'atividade_tipo_id_INT', null, '603', null, null, '11', null, '0', '0', '0', null, '602', 'id', 'atividade_tipos_FK_423400879', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('900', 'atividade_id_INT', null, '603', null, null, '11', null, '0', '0', '0', null, '601', 'id', 'atividade_tipos_FK_996185303', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('901', 'corporacao_id_INT', null, '603', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'atividade_tipos_FK_789062500', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('902', 'id', null, '604', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('903', 'nome', null, '604', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('904', 'corporacao_id_INT', null, '604', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'atividade_unidade_medida_FK_833953858', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('905', 'id', null, '605', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('906', 'valor_FLOAT', null, '605', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('907', 'empresa_id_INT', null, '605', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'despesa_FK_685638428', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('908', 'vencimento_SEC', null, '605', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('909', 'vencimento_OFFSEC', null, '605', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('910', 'pagamento_SEC', null, '605', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('911', 'pagamento_OFFSEC', null, '605', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('912', 'despesa_cotidiano_id_INT', null, '605', null, null, '11', null, '0', '0', '0', null, '606', 'id', 'despesa_FK_939453125', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('913', 'valor_pagamento_FLOAT', null, '605', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('914', 'cadastro_usuario_id_INT', null, '605', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'despesa_FK_777130127', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('915', 'pagamento_usuario_id_INT', null, '605', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'despesa_FK_818298340', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('916', 'protocolo_INT', null, '605', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('917', 'corporacao_id_INT', null, '605', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'despesa_FK_352508545', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('918', 'id', null, '606', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('919', 'despesa_da_empresa_id_INT', null, '606', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'despesa_cotidiano_FK_33874511', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('920', 'despesa_do_usuario_id_INT', null, '606', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'despesa_cotidiano_FK_475311279', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('921', 'cadastro_usuario_id_INT', null, '606', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'despesa_cotidiano_FK_201293945', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('922', 'id_tipo_despesa_cotidiano_INT', null, '606', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('923', 'parametro_INT', null, '606', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('924', 'parametro_OFFSEC', null, '606', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('925', 'parametro_SEC', null, '606', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('926', 'parametro_json', null, '606', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('927', 'valor_FLOAT', null, '606', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('928', 'data_limite_cotidiano_SEC', null, '606', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('929', 'data_limite_cotidiano_OFFSEC', null, '606', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('930', 'corporacao_id_INT', null, '606', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'despesa_cotidiano_FK_755279541', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('931', 'relatorio_id_INT', null, '5', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'empresa_FK_690795899', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('932', 'ind_email_valido_BOOLEAN', null, '5', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('933', 'ind_celular_valido_BOOLEAN', null, '5', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('934', 'id', null, '607', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('935', 'empresa_id_INT', null, '607', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_atividade_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('936', 'prazo_entrega_dia_INT', null, '607', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('937', 'duracao_horas_INT', null, '607', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('938', 'cadastro_SEC', null, '607', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('939', 'cadastro_OFFSEC', null, '607', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('940', 'preco_custo_FLOAT', null, '607', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('941', 'preco_venda_FLOAT', null, '607', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('942', 'atividade_id_INT', null, '607', null, null, '11', null, '0', '0', '0', null, '601', 'id', 'empresa_atividade_FK_124084472', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('943', 'corporacao_id_INT', null, '607', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_atividade_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('944', 'id', null, '608', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('945', 'empresa_compra_id_INT', null, '608', null, null, '11', null, '0', '0', '0', null, '54', 'id', 'empresa_atividade_compra_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('946', 'quantidade_FLOAT', null, '608', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('947', 'valor_total_FLOAT', null, '608', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('948', 'desconto_FLOAT', null, '608', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('949', 'descricao', null, '608', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('950', 'atividade_id_INT', null, '608', null, null, '11', null, '0', '0', '0', null, '601', 'id', 'empresa_atividade_compra_FK_668334961', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('951', 'empresa_id_INT', null, '608', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_atividade_compra_FK_537750244', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('952', 'corporacao_id_INT', null, '608', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_atividade_compra_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('953', 'id', null, '609', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('954', 'empresa_venda_id_INT', null, '609', null, null, '11', null, '0', '0', '0', null, '68', 'id', 'empresa_atividade_venda_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('955', 'quantidade_FLOAT', null, '609', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('956', 'valor_total_FLOAT', null, '609', null, null, null, null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('957', 'desconto_FLOAT', null, '609', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('958', 'corporacao_id_INT', null, '609', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_atividade_venda_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('959', 'cadastro_SEC', null, '609', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('960', 'cadastro_OFFSEC', null, '609', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('961', 'atividade_id_INT', null, '609', null, null, '11', null, '0', '0', '0', null, '601', 'id', 'empresa_atividade_venda_FK_500762939', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('962', 'empresa_id_INT', null, '609', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_atividade_venda_FK_194274902', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('963', 'vencimento_SEC', null, '54', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('964', 'vencimento_OFFSEC', null, '54', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('965', 'relatorio_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'empresa_compra_FK_584411621', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('966', 'registro_estado_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'empresa_compra_FK_608856201', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('967', 'registro_estado_corporacao_id_INT', null, '54', null, null, '11', null, '0', '0', '0', null, '628', 'id', 'empresa_compra_FK_382690430', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('968', 'fechamento_SEC', null, '54', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('969', 'fechamento_OFFSEC', null, '54', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('970', 'valor_pago_FLOAT', null, '54', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('971', 'desconto_FLOAT', null, '54', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('972', 'protocolo_INT', null, '54', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('973', 'pagamento_SEC', null, '55', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('974', 'pagamento_OFFSEC', null, '55', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('975', 'preco_custo_FLOAT', null, '56', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('976', 'preco_venda_FLOAT', null, '56', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('977', 'estoque_atual_INT', null, '56', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('978', 'estoque_minimo_INT', null, '56', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('979', 'prazo_reposicao_estoque_dias_INT', null, '56', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('980', 'codigo_barra', null, '56', null, null, '13', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('981', 'produto_id_INT', null, '56', null, null, '11', null, '0', '0', '0', null, '620', 'id', 'empresa_produto_FK_384552002', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('982', 'produto_id_INT', null, '57', null, null, '11', null, '0', '0', '0', null, '620', 'id', 'empresa_produto_compra_FK_968383790', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('983', 'empresa_id_INT', null, '57', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_produto_compra_FK_618072510', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('984', 'descricao', null, '57', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('985', 'produto_id_INT', null, '60', null, null, '11', null, '0', '0', '0', null, '620', 'id', 'empresa_produto_venda_FK_420928955', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('986', 'descricao', null, '60', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('987', 'cadastro_SEC', null, '60', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('988', 'cadastro_OFFSEC', null, '60', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('989', 'empresa_id_INT', null, '60', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_produto_venda_FK_367370605', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('990', 'registro_estado_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'empresa_venda_FK_985656739', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('991', 'registro_estado_corporacao_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '628', 'id', 'empresa_venda_FK_407073975', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('992', 'relatorio_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'empresa_venda_FK_402862549', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('993', 'descricao', null, '68', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('994', 'vencimento_SEC', null, '68', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('995', 'vencimento_OFFSEC', null, '68', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('996', 'fechamento_SEC', null, '68', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('997', 'fechamento_OFFSEC', null, '68', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('998', 'valor_pago_FLOAT', null, '68', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('999', 'mesa_id_INT', null, '68', null, null, '11', null, '0', '0', '0', null, '613', 'id', 'empresa_venda_FK_648132324', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1000', 'identificador', null, '68', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1001', 'protocolo_INT', null, '68', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1002', 'pagamento_SEC', null, '69', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1003', 'pagamento_OFFSEC', null, '69', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1004', 'seq_INT', null, '69', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1005', 'id', null, '610', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1006', 'titulo', null, '610', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1007', 'template_json', null, '610', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1008', 'produto_id_INT', null, '610', null, null, '11', null, '0', '0', '0', null, '620', 'id', 'empresa_venda_template_FK_418518066', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1009', 'empresa_id_INT', null, '610', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'empresa_venda_template_FK_963958741', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1010', 'corporacao_id_INT', null, '610', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'empresa_venda_template_FK_776367188', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1011', 'id', null, '611', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1012', 'titulo', null, '611', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1013', 'mensagem', null, '611', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1014', 'remetente_usuario_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'mensagem_FK_646789551', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1015', 'tipo_mensagem_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '611', 'id', 'mensagem_FK_970550538', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1016', 'destinatario_usuario_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'mensagem_FK_119873046', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1017', 'destinatario_pessoa_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'mensagem_FK_67901611', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1018', 'destinatario_empresa_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'mensagem_FK_637390137', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1019', 'data_min_envio_SEC', null, '611', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1020', 'data_min_envio_OFFSEC', null, '611', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1021', 'protocolo_INT', null, '611', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1022', 'registro_estado_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'mensagem_FK_258514404', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1023', 'empresa_para_cliente_BOOLEAN', null, '611', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1024', 'corporacao_id_INT', null, '611', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'mensagem_FK_570434570', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1025', 'id', null, '612', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1026', 'tipo_canal_envio_id_INT', null, '612', null, null, '11', null, '0', '0', '0', null, '632', 'id', 'mensagem_envio_FK_703460694', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1027', 'mensagem_id_INT', null, '612', null, null, '11', null, '0', '0', '0', null, '611', 'id', 'mensagem_envio_FK_757080078', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1028', 'cadastro_SEC', null, '612', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1029', 'cadastro_OFFSEC', null, '612', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1030', 'obs', null, '612', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1031', 'identificador', null, '612', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1032', 'tentativa_INT', null, '612', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1033', 'registro_estado_id_INT', null, '612', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'mensagem_envio_FK_273468017', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1034', 'corporacao_id_INT', null, '612', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'mensagem_envio_FK_794128418', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1035', 'id', null, '613', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1036', 'descricao', null, '613', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1037', 'empresa_id_INT', null, '613', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'mesa_FK_462615967', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1038', 'template_json', null, '613', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1039', 'cadastro_SEC', null, '613', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1040', 'cadastro_OFFSEC', null, '613', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1041', 'corporacao_id_INT', null, '613', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'mesa_FK_317504883', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1042', 'id', null, '614', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1043', 'mesa_id_INT', null, '614', null, null, '11', null, '0', '0', '0', null, '613', 'id', 'mesa_reserva_FK_235290527', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1044', 'cadastro_SEC', null, '614', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1045', 'cadastro_OFFSEC', null, '614', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1046', 'pessoa_id_INT', null, '614', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'mesa_reserva_FK_304656982', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1047', 'reserva_SEC', null, '614', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1048', 'reserva_OFFSEC', null, '614', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1049', 'confimado_SEC', null, '614', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1050', 'confirmado_OFFSEC', null, '614', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1051', 'confirmador_usuario_id_INT', null, '614', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'mesa_reserva_FK_974975586', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1052', 'celular', null, '614', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1053', 'protocolo_INT', null, '614', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1054', 'corporacao_id_INT', null, '614', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'mesa_reserva_FK_961578370', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1055', 'id', null, '615', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1056', 'devedor_empresa_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'negociacao_divida_FK_620697022', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1057', 'devedor_pessoa_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'negociacao_divida_FK_110839843', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1058', 'negociador_usuario_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'negociacao_divida_FK_708282471', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1059', 'cadastro_SEC', null, '615', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1060', 'cadastro_OFFSEC', null, '615', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1061', 'resultado_empresa_venda_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '68', 'id', 'negociacao_divida_FK_563903809', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1062', 'relatorio_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'negociacao_divida_FK_873504639', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1063', 'registro_estado_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'negociacao_divida_FK_729370117', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1064', 'protocolo_INT', null, '615', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1065', 'corporacao_id_INT', null, '615', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'negociacao_divida_FK_940582276', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1066', 'id', null, '616', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1067', 'empresa_venda_protocolo_INT', null, '616', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1068', 'negociacao_divida_id_INT', null, '616', null, null, '11', null, '0', '0', '0', null, '615', 'id', 'negociacao_divida_empresa_venda_FK_449615478', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1069', 'corporacao_id_INT', null, '616', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'negociacao_divida_empresa_venda_FK_306701660', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1070', 'id', null, '617', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1071', 'empresa_id_INT', null, '617', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'nota_FK_922271729', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1072', 'pessoa_id_INT', null, '617', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'nota_FK_826232910', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1073', 'usuario_id_INT', null, '617', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'nota_FK_654876709', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1074', 'veiculo_id_INT', null, '617', null, null, '11', null, '0', '0', '0', null, '20', 'id', 'nota_FK_336425781', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1075', 'relatorio_id_INT', null, '617', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'nota_FK_797149659', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1076', 'protocolo_INT', null, '617', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1077', 'corporacao_id_INT', null, '617', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'nota_FK_750427246', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1078', 'id', null, '618', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1079', 'nota_id_INT', null, '618', null, null, '11', null, '0', '0', '0', null, '617', 'id', 'nota_tipo_nota_FK_92987060', 'CASCADE', 'SET NULL', null, '1', 'nota_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1080', 'tipo_nota_id_INT', null, '618', null, null, '11', null, '0', '0', '0', null, '633', 'id', 'nota_tipo_nota_FK_277954101', 'CASCADE', 'SET NULL', null, '1', 'nota_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1081', 'corporacao_id_INT', null, '618', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'nota_tipo_nota_FK_861236573', 'CASCADE', 'SET NULL', null, '1', 'nota_id_INT', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1082', 'data_nascimento_SEC', null, '6', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1083', 'data_nascimento_OFFSEC', null, '6', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1084', 'relatorio_id_INT', null, '6', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'pessoa_FK_594970703', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1085', 'ind_email_valido_BOOLEAN', null, '6', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1086', 'ind_celular_valido_BOOLEAN', null, '6', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1087', 'id', null, '619', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1088', 'empresa_equipe_id_INT', null, '619', null, null, '11', null, '0', '0', '0', null, '498', 'id', 'pessoa_equipe_FK_929412842', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1089', 'usuario_id_INT', null, '619', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'pessoa_equipe_FK_198364258', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1090', 'pessoa_id_INT', null, '619', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'pessoa_equipe_FK_935943604', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1091', 'corporacao_id_INT', null, '619', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'pessoa_equipe_FK_316467285', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1092', 'descricao', null, '9', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1093', 'endereco', null, '9', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1094', 'id', null, '620', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1095', 'identificador', null, '620', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1096', 'nome', null, '620', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1097', 'nome_normalizado', null, '620', null, null, '255', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1098', 'descricao', null, '620', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1099', 'produto_unidade_medida_id_INT', null, '620', null, null, '11', null, '0', '0', '0', null, '623', 'id', 'produto_FK_485992432', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1100', 'id_omega_INT', null, '620', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1101', 'cadastro_SEC', null, '620', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1102', 'cadastro_OFFSEC', null, '620', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1103', 'preco_custo_FLOAT', null, '620', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1104', 'prazo_reposicao_estoque_dias_INT', null, '620', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1105', 'codigo_barra', null, '620', null, null, '13', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1106', 'corporacao_id_INT', null, '620', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'produto_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1107', 'id', null, '621', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1108', 'nome', null, '621', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '1', 'nome_98765', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1109', 'nome_normalizado', null, '621', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1110', 'corporacao_id_INT', null, '621', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'produto_tipo_ibfk_1', 'CASCADE', 'SET NULL', null, '1', 'nome_98765', null, '0');
INSERT INTO `sistema_atributo` VALUES ('1111', 'id', null, '622', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1112', 'produto_tipo_id_INT', null, '622', null, null, '11', null, '0', '0', '0', null, '621', 'id', 'produto_tipos_FK_90423584', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1113', 'produto_id_INT', null, '622', null, null, '11', null, '0', '0', '0', null, '620', 'id', 'produto_tipos_FK_811035157', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1114', 'corporacao_id_INT', null, '622', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'produto_tipos_FK_529571533', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1115', 'id', null, '623', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1116', 'nome', null, '623', null, null, '100', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1117', 'abreviacao', null, '623', null, null, '10', null, '1', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1118', 'is_float_BOOLEAN', null, '623', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1119', 'id', null, '624', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1120', 'valor_FLOAT', null, '624', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1121', 'devedor_empresa_id_INT', null, '624', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'recebivel_FK_239410400', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1122', 'devedor_pessoa_id_INT', null, '624', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'recebivel_FK_19348144', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1123', 'cadastro_usuario_id_INT', null, '624', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'recebivel_FK_829437256', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1124', 'cadastro_SEC', null, '624', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1125', 'cadastro_OFFSEC', null, '624', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1126', 'valor_pagamento_FLOAT', null, '624', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1127', 'pagamento_SEC', null, '624', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1128', 'pagamento_OFFSEC', null, '624', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1129', 'recebedor_usuario_id_INT', null, '624', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'recebivel_FK_988525391', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1130', 'relatorio_id_INT', null, '624', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'recebivel_FK_1007080', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1131', 'protocolo_INT', null, '624', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1132', 'corporacao_id_INT', null, '624', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'recebivel_FK_595886231', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1133', 'id', null, '625', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1134', 'recebedor_empresa_id_INT', null, '625', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'recebivel_cotidiano_FK_447082519', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1135', 'cadastro_usuario_id_INT', null, '625', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'recebivel_cotidiano_FK_452484131', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1136', 'id_tipo_despesa_recebivel_INT', null, '625', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1137', 'parametro_INT', null, '625', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1138', 'parametro_OFFSEC', null, '625', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1139', 'parametro_SEC', null, '625', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1140', 'parametro_json', null, '625', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1141', 'valor_FLOAT', null, '625', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1142', 'data_limite_cotidiano_SEC', null, '625', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1143', 'data_limite_cotidiano_OFFSEC', null, '625', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1144', 'corporacao_id_INT', null, '625', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'recebivel_cotidiano_FK_900634766', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1145', 'cadastro_SEC', null, '83', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1146', 'cadastro_OFFSEC', null, '83', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1147', 'relatorio_id_INT', null, '83', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'rede_FK_704559326', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1148', 'cadastro_SEC', null, '84', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1149', 'cadastro_OFFSEC', null, '84', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1150', 'id', null, '626', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1151', 'protocolo_INT', null, '626', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1152', 'cadastro_SEC', null, '626', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1153', 'cadastro_OFFSEC', null, '626', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1154', 'responsavel_usuario_id_INT', null, '626', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'registro_FK_991607667', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1155', 'responsavel_categoria_permissao_id_INT', null, '626', null, null, '11', null, '0', '0', '0', null, '2', 'id', 'registro_FK_883850098', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1156', 'descricao', null, '626', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1157', 'tipo_registro_id_INT', null, '626', null, null, '11', null, '0', '0', '0', null, '634', 'id', 'registro_FK_435150146', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1158', 'registro_estado_id_INT', null, '626', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'registro_FK_886230469', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1159', 'registro_estado_corporacao_id_INT', null, '626', null, null, '11', null, '0', '0', '0', null, '628', 'id', 'registro_FK_225860595', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1160', 'id_origem_chamada_INT', null, '626', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1161', 'corporacao_id_INT', null, '626', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'registro_FK_979919434', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1162', 'id', null, '627', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1163', 'nome', null, '627', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1166', 'prazo_dias_INT', null, '627', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1167', 'tipo_registro_id_INT', null, '627', null, null, '11', null, '0', '0', '0', null, '634', 'id', 'registro_estado_FK_634155274', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1168', 'id', null, '628', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1169', 'nome', null, '628', null, null, '50', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1170', 'responsavel_usuario_id_INT', null, '628', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'registro_estado_corporacao_FK_488037109', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1171', 'responsavel_categoria_permissao_id_INT', null, '628', null, null, '11', null, '0', '0', '0', null, '2', 'id', 'registro_estado_corporacao_FK_697784424', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1172', 'prazo_dias_INT', null, '628', null, null, '3', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1173', 'tipo_registro_id_INT', null, '628', null, null, '11', null, '0', '0', '0', null, '634', 'id', 'registro_estado_corporacao_FK_794616700', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1174', 'corporacao_id_INT', null, '628', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'registro_estado_corporacao_FK_896209717', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1175', 'id', null, '629', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1176', 'origem_registro_estado_corporacao_id_INT', null, '629', null, null, '11', null, '0', '0', '0', null, '628', 'id', 'registro_fluxo_estado_corporacao_FK_25970459', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1177', 'origem_registro_estado_id_INT', null, '629', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'registro_fluxo_estado_corporacao_FK_785644532', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1178', 'destino_registro_estado_corporacao_id_INT', null, '629', null, null, '11', null, '0', '0', '0', null, '628', 'id', 'registro_fluxo_estado_corporacao_FK_761993408', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1179', 'destino_registro_estado_id_INT', null, '629', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'registro_fluxo_estado_corporacao_FK_918395997', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1180', 'template_JSON', null, '629', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1181', 'corporacao_id_INT', null, '629', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'registro_fluxo_estado_corporacao_FK_13153076', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1182', 'tipo_relatorio_id_INT', null, '89', null, null, '11', null, '0', '0', '0', null, '635', 'id', 'relatorio_FK_984588624', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1183', 'descricao', null, '91', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1184', 'empresa_atividade_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '607', 'id', 'tarefa_FK_189971924', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1185', 'tipo_tarefa_id_INT', null, '11', null, null, '4', null, '0', '0', '0', null, '636', 'id', 'tarefa_FK_751190186', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1186', 'empresa_venda_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '68', 'id', 'tarefa_FK_443054199', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1187', 'id_prioridade_INT', null, '11', null, null, '4', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1188', 'registro_estado_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '627', 'id', 'tarefa_FK_972564698', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1189', 'registro_estado_corporacao_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '628', 'id', 'tarefa_FK_558166504', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1190', 'id', null, '630', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1191', 'criado_pelo_usuario_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_cotidiano_ibfk_8', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1192', 'categoria_permissao_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '2', 'id', 'tarefa_cotidiano_ibfk_10', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1193', 'veiculo_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '20', 'id', 'tarefa_cotidiano_ibfk_1', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1194', 'usuario_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_cotidiano_ibfk_4', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1195', 'empresa_equipe_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '498', 'id', 'tarefa_cotidiano_FK_718597412', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1196', 'origem_pessoa_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'tarefa_cotidiano_ibfk_11', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1197', 'origem_empresa_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'tarefa_cotidiano_ibfk_6', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1198', 'origem_logradouro', null, '630', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1199', 'origem_numero', null, '630', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1200', 'origem_cidade_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'tarefa_cotidiano_ibfk_5', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1201', 'origem_latitude_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1202', 'origem_longitude_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1203', 'origem_latitude_real_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1204', 'origem_longitude_real_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1205', 'destino_pessoa_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '6', 'id', 'tarefa_cotidiano_ibfk_7', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1206', 'destino_empresa_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'tarefa_cotidiano_ibfk_3', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1207', 'destino_logradouro', null, '630', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1208', 'destino_numero', null, '630', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1209', 'destino_cidade_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '4', 'id', 'tarefa_cotidiano_ibfk_9', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1210', 'destino_latitude_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1211', 'destino_longitude_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1212', 'destino_latitude_real_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1213', 'destino_longitude_real_INT', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1214', 'tempo_estimado_carro_INT', null, '630', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1215', 'tempo_estimado_a_pe_INT', null, '630', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1216', 'distancia_estimada_carro_INT', null, '630', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1217', 'distancia_estimada_a_pe_INT', null, '630', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1218', 'is_realizada_a_pe_BOOLEAN', null, '630', null, null, '1', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1219', 'inicio_hora_programada_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1220', 'inicio_hora_programada_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1221', 'inicio_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1222', 'inicio_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1223', 'fim_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1224', 'fim_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1225', 'cadastro_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1226', 'cadastro_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1227', 'titulo', null, '630', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1228', 'titulo_normalizado', null, '630', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1229', 'descricao', null, '630', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1230', 'corporacao_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tarefa_cotidiano_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1231', 'servico_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '31', 'id', 'tarefa_cotidiano_FK_656982422', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('1232', 'id_estado_INT', null, '630', null, null, '4', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1233', 'venda_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('1234', 'id_prioridade_INT', null, '630', null, null, '4', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1235', 'parametro_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1236', 'parametro_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1237', 'parametro_json', null, '630', null, null, '512', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1238', 'data_limite_cotidiano_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1239', 'data_limite_cotidiano_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1240', 'id', null, '631', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1241', 'descricao', null, '631', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1242', 'data_conclusao_SEC', null, '631', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1243', 'data_conclusao_OFFSEC', null, '631', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1244', 'tarefa_id_INT', null, '631', null, null, '11', null, '0', '0', '0', null, '11', 'id', 'tarefa_item_FK_979400635', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1245', 'cadastro_SEC', null, '631', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1246', 'cadastro_OFFSEC', null, '631', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1247', 'criado_pelo_usuario_id_INT', null, '631', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_item_FK_803283692', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1248', 'executada_pelo_usuario_id_INT', null, '631', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'tarefa_item_FK_903411866', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1249', 'corporacao_id_INT', null, '631', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tarefa_item_FK_176757812', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1250', 'id', null, '632', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1251', 'nome', null, '632', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1252', 'id', null, '633', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1253', 'nome', null, '633', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1254', 'corporacao_id_INT', null, '633', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tipo_nota_FK_962524415', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1255', 'id', null, '634', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1256', 'nome', null, '634', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1257', 'id', null, '635', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1258', 'nome', null, '635', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1259', 'id', null, '636', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1260', 'nome', null, '636', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1261', 'corporacao_id_INT', null, '636', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tipo_tarefa_FK_143341064', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('1262', 'id', null, '637', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1263', 'nome', null, '637', null, null, '100', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1264', 'corporacao_id_INT', null, '637', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tipo_veiculo_registro_FK_76385498', 'CASCADE', 'SET NULL', null, '0', null, null, '1');
INSERT INTO `sistema_atributo` VALUES ('1265', 'id', null, '638', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1266', 'hora_inicio_TIME', null, '638', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1267', 'hora_fim_TIME', null, '638', null, null, null, null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1268', 'data_SEC', null, '638', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1269', 'data_OFFSEC', null, '638', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1270', 'dia_semana_INT', null, '638', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1271', 'empresa_id_INT', null, '638', null, null, '11', null, '0', '0', '0', null, '5', 'id', 'usuario_horario_trabalho_FK_788391114', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1272', 'usuario_id_INT', null, '638', null, null, '11', null, '0', '0', '0', null, '14', 'id', 'usuario_horario_trabalho_FK_905120850', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1273', 'template_JSON', null, '638', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1274', 'corporacao_id_INT', null, '638', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'usuario_horario_trabalho_ibfk_2', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1275', 'tipo_veiculo_registro_id_INT', null, '446', null, null, '11', null, '0', '0', '0', null, '637', 'id', 'veiculo_registro_FK_612823486', 'CASCADE', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1276', 'protocolo_INT', null, '609', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1277', 'id', null, '639', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1278', 'nome', null, '639', null, null, '30', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1279', 'corporacao_id_INT', null, '639', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1280', 'percentual_completo_INT', null, '11', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1281', 'prazo_SEC', null, '11', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1282', 'prazo_OFFSEC', null, '11', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1283', 'atividade_id_INT', null, '11', null, null, '11', null, '0', '0', '0', null, '601', 'id', 'tarefa_FK_575897217', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1284', 'protocolo_empresa_venda_INT', null, '11', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1285', 'protocolo_empresa_atividade_venda_INT', null, '11', null, null, '20', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1286', 'atividade_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '601', 'id', 'tarefa_cotidiano_FK_316833496', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1287', 'id_tipo_cotidiano_INT', null, '630', null, null, '4', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1288', 'prazo_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1289', 'prazo_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1290', 'percentual_completo_INT', null, '630', null, null, '2', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1291', 'ultima_geracao_SEC', null, '630', null, null, '10', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1292', 'ultima_geracao_OFFSEC', null, '630', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1293', 'relatorio_id_INT', null, '630', null, null, '11', null, '0', '0', '0', null, '89', 'id', 'tarefa_cotidiano_FK_442840576', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1294', 'id', null, '640', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1295', 'descricao', null, '640', null, null, '255', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1296', 'corporacao_id_INT', null, '640', null, null, '11', null, '0', '0', '0', null, '49', 'id', 'tarefa_cotidiano_item_FK_381195068', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1297', 'seq_INT', null, '640', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1298', 'latitude_INT', null, '640', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1299', 'longitude_INT', null, '640', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1300', 'tarefa_cotidiano_id_INT', null, '640', null, null, '11', null, '0', '0', '0', null, '630', 'id', 'tarefa_cotidiano_item_FK_630126953', 'SET NULL', 'SET NULL', null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1301', 'seq_INT', null, '631', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1302', 'latitude_INT', null, '631', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1303', 'longitude_INT', null, '631', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1304', 'latitude_real_INT', null, '631', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1305', 'longitude_real_INT', null, '631', null, null, '6', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1306', 'id', null, '641', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1307', 'tarefa_id_INT', null, '641', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1308', 'relatorio_id_INT', null, '641', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1309', 'corporacao_id_INT', null, '641', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1310', 'id', null, '642', null, null, '11', null, '1', '1', '1', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1311', 'tarefa_id_INT', null, '642', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1312', 'tag_id_INT', null, '642', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');
INSERT INTO `sistema_atributo` VALUES ('1313', 'corporacao_id_INT', null, '642', null, null, '11', null, '0', '0', '0', null, null, null, null, null, null, null, '0', null, null, '0');

-- ----------------------------
-- Table structure for `sistema_tabela`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tabela`;
CREATE TABLE `sistema_tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  `data_modificacao_estrutura_DATETIME` date DEFAULT NULL,
  `frequencia_sincronizador_INT` int(2) DEFAULT NULL,
  `banco_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `banco_web_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_web_para_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_mobile_para_web_BOOLEAN` int(1) DEFAULT NULL,
  `tabela_sistema_BOOLEAN` int(1) DEFAULT NULL,
  `excluida_BOOLEAN` int(1) DEFAULT NULL,
  `atributos_chave_unica` varchar(512) DEFAULT NULL,
  `primeira_vez_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=643 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tabela
-- ----------------------------
INSERT INTO `sistema_tabela` VALUES ('1', 'bairro', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, cidade_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('2', 'categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('3', 'permissao_categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'permissao_id_INT, categoria_permissao_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('4', 'cidade', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, uf_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('5', 'empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('6', 'pessoa', null, null, '-1', '1', '1', '1', '1', null, '0', 'email, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('7', 'pessoa_empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, empresa_id_INT, profissao_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('8', 'modelo', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('9', 'ponto', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('10', 'profissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('11', 'tarefa', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('12', 'tipo_empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('13', 'uf', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT, pais_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('14', 'usuario', null, null, '-1', '1', '1', '1', '1', null, '0', 'email', null);
INSERT INTO `sistema_tabela` VALUES ('15', 'usuario_categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('16', 'usuario_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('17', 'usuario_foto', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('18', 'usuario_posicao', null, null, '0', '1', '1', '0', '0', null, '0', 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('19', 'usuario_servico', null, null, '-1', '1', '1', '1', '1', null, '0', 'servico_id_INT, usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('20', 'veiculo', null, null, '-1', '1', '1', '1', '1', null, '0', 'placa, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('21', 'veiculo_usuario', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, veiculo_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('22', 'pais', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('23', 'tipo_documento', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('24', 'empresa_perfil', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('30', 'perfil', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('31', 'servico', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('39', 'sexo', null, null, '1', '1', '1', '1', '0', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('46', 'permissao', null, null, '-1', '1', '1', '1', '1', '1', '0', 'tag', null);
INSERT INTO `sistema_tabela` VALUES ('49', 'corporacao', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('54', 'empresa_compra', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('55', 'empresa_compra_parcela', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('56', 'empresa_produto', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('57', 'empresa_produto_compra', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('58', 'empresa_produto_foto', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('59', 'empresa_produto_unidade_medida', null, null, '1', '1', '1', '1', '0', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('60', 'empresa_produto_venda', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('61', 'empresa_servico', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('62', 'empresa_servico_compra', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('63', 'empresa_servico_foto', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('64', 'empresa_servico_unidade_medida', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('65', 'empresa_servico_venda', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('66', 'empresa_tipo_venda', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('67', 'empresa_tipo_venda_parcela', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('68', 'empresa_venda', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('69', 'empresa_venda_parcela', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('71', 'empresa_produto_tipo', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('72', 'empresa_servico_tipo', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('73', 'usuario_tipo', null, null, '-1', '1', '1', '1', '0', null, '0', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('75', 'usuario_tipo_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('77', 'operadora', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('81', 'app', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('82', 'tipo_ponto', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('83', 'rede', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('84', 'rede_empresa', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('85', 'pessoa_empresa_rotina', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, empresa_id_INT, profissao_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('86', 'rotina', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('87', 'pessoa_usuario', null, null, '-1', '1', '1', '1', '1', null, '0', 'pessoa_id_INT, usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('89', 'relatorio', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('90', 'tipo_anexo', null, null, '-1', '1', '1', '1', '1', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('91', 'relatorio_anexo', null, null, '-1', '1', '1', '1', '1', null, '0', 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('126', 'estado_civil', null, null, '1', '1', '1', '1', '0', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('139', 'sistema_tabela', null, null, '-1', '1', '1', '1', '0', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('143', 'forma_pagamento', null, null, '-1', '1', '1', '1', '1', null, '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('168', 'usuario_foto_configuracao', null, null, '-1', '1', '1', '1', '1', null, '0', 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('171', 'sistema_tipo_operacao_banco', null, null, '1', '1', '1', '1', '0', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('238', 'sistema_banco_versao', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('317', 'sistema_tipo_mobile', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('337', 'sistema_usuario_mensagem', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('343', 'sistema_tipo_download_arquivo', null, null, '0', '1', '1', '0', '0', '1', '0', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('344', 'sistema_tipo_usuario_mensagem', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('441', 'horario_trabalho_pessoa_empresa', null, null, '-1', '1', '1', '1', '1', null, '1', 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('442', 'tarefa_acao', null, null, null, '1', '1', null, null, null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('443', 'projetos_versao', null, null, null, null, null, null, null, '1', '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('444', 'android_metadata', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('445', 'sistema_atributo', null, null, '-1', '1', '1', '1', '1', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('446', 'veiculo_registro', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('447', 'sistema_deletar_arquivo', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('448', 'sistema_parametro_global', null, null, '-1', '1', '1', '1', '1', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('449', 'sistema_produto_log_erro', null, null, null, '1', '1', null, null, '1', '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('450', 'corporacao_sincronizador_php', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('451', 'usuario_posicao_rastreamento', null, null, null, '1', '1', null, null, null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('452', 'sistema_sincronizador_arquivo', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('453', 'sistema_grafo_hierarquia_banco', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('454', 'sistema_historico_sincronizador', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('455', 'sistema_operacao_crud_aleatorio', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('456', 'sistema_operacao_executa_script', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('457', 'sistema_operacao_sistema_mobile', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('458', 'sistema_corporacao_sincronizador', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('459', 'sistema_aresta_grafo_hierarquia_banco', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('460', 'sistema_operacao_download_banco_mobile', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('461', 'sistema_corporacao_sincronizador_tabela', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('462', 'sistema_registro_sincronizador_android_para_web', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('463', 'sistema_registro_sincronizador_web_para_android', null, null, null, '1', '1', null, null, '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('464', 'wifi', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('465', 'wifi_registro', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('466', 'acesso', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('467', 'operacao_sistema', null, null, '0', '1', '1', '0', '0', '1', '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('468', 'ponto_endereco', null, null, '0', '1', '1', '0', '0', null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('469', 'sistema_projetos_versao', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('470', 'sistema_registro_sincronizador', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('471', 'usuario_empresa', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('472', 'usuario_menu', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('473', 'usuario_privilegio', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('474', 'usuario_tipo_menu', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('475', 'usuario_tipo_privilegio', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('476', 'campo', null, null, '-1', '1', '1', '1', '0', null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('477', 'tela', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('478', 'tela_categoria_permissao', null, null, '-1', '1', '1', '1', '1', null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('479', 'tela_sistema_tabela', null, null, '-1', '1', '1', '1', '0', null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('480', 'tela_usuario_tipo', null, null, '0', '1', '1', '0', '0', null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('481', 'tipo_campo', null, null, '1', '1', '1', '1', '0', '1', '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('482', 'tipo_cardinalidade', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('483', 'tipo_tela', null, null, '1', '1', '1', '1', '0', '1', '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('484', 'area_menu', null, null, '-1', '1', '1', '0', '1', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('485', 'sistema_campo', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('486', 'sistema_campo_atributo', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('487', 'sistema_estado_lista', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('488', 'sistema_lista', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('489', 'sistema_tipo_campo', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('490', 'tela_componente', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('491', 'tela_estado', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('492', 'tela_sistema_lista', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('493', 'tela_tipo', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('494', 'tela_tipo_componente', null, null, '0', '1', '1', '0', '0', '1', '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('495', 'sistema_sequencia', null, null, '0', '1', '1', '0', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('496', 'sistema_crud_estado', null, null, null, '1', '1', null, null, null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('497', 'sistema_registro_historico', null, null, null, '1', '1', null, null, null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('498', 'empresa_equipe', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('499', 'pessoa_empresa_equipe', null, null, '-1', '1', '1', '1', '1', null, '1', null, null);
INSERT INTO `sistema_tabela` VALUES ('597', 'sistema_parametro_global_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('598', 'sistema_operacao_crud_mobile_baseado_web', null, null, null, '1', '1', null, null, null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('599', 'api', null, null, '-1', '1', '1', '1', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('600', 'api_id', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('601', 'atividade', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('602', 'atividade_tipo', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('603', 'atividade_tipos', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('604', 'atividade_unidade_medida', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('605', 'despesa', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('606', 'despesa_cotidiano', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('607', 'empresa_atividade', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('608', 'empresa_atividade_compra', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('609', 'empresa_atividade_venda', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('610', 'empresa_venda_template', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('611', 'mensagem', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('612', 'mensagem_envio', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('613', 'mesa', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('614', 'mesa_reserva', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('615', 'negociacao_divida', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('616', 'negociacao_divida_empresa_venda', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('617', 'nota', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('618', 'nota_tipo_nota', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('619', 'pessoa_equipe', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('620', 'produto', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('621', 'produto_tipo', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('622', 'produto_tipos', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('623', 'produto_unidade_medida', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('624', 'recebivel', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('625', 'recebivel_cotidiano', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('626', 'registro', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('627', 'registro_estado', null, null, '-1', '1', '1', '0', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('628', 'registro_estado_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('629', 'registro_fluxo_estado_corporacao', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('630', 'tarefa_cotidiano', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('631', 'tarefa_item', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('632', 'tipo_canal_envio', null, null, '-1', '1', '1', '1', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('633', 'tipo_nota', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('634', 'tipo_registro', null, null, '-1', '1', '1', '1', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('635', 'tipo_relatorio', null, null, '-1', '1', '1', '1', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('636', 'tipo_tarefa', null, null, '-1', '1', '1', '1', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('637', 'tipo_veiculo_registro', null, null, '-1', '1', '1', '1', '0', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('638', 'usuario_horario_trabalho', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('639', 'tag', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('640', 'tarefa_cotidiano_item', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('641', 'tarefa_relatorio', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('642', 'tarefa_tag', null, null, '-1', '1', '1', '1', '1', null, '0', null, null);

-- ----------------------------
-- Table structure for `tipo_operacao_banco`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_operacao_banco`;
CREATE TABLE `tipo_operacao_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_operacao_banco
-- ----------------------------
INSERT INTO `tipo_operacao_banco` VALUES ('3', 'Editar');
INSERT INTO `tipo_operacao_banco` VALUES ('2', 'Inserir');
INSERT INTO `tipo_operacao_banco` VALUES ('1', 'Remover');

-- ----------------------------
-- Table structure for `uf`
-- ----------------------------
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `sigla` char(2) NOT NULL,
  `dataCadastro` datetime DEFAULT NULL,
  `dataEdicao` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sigla` (`sigla`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uf
-- ----------------------------
INSERT INTO `uf` VALUES ('1', 'São Paulo', 'SP', '2009-10-18 16:05:17', '2009-10-18 16:05:17', '0', null);
INSERT INTO `uf` VALUES ('2', 'Minas Gerais', 'MG', '2009-10-18 16:08:39', '2009-10-18 16:08:39', '0', null);
INSERT INTO `uf` VALUES ('3', 'Pará', 'PA', '2009-10-23 11:20:19', '2009-10-23 11:20:19', '0', null);
INSERT INTO `uf` VALUES ('4', 'Espirito Santo', 'ES', '2009-11-04 19:22:18', '2009-11-04 19:22:18', '0', null);
INSERT INTO `uf` VALUES ('5', 'Rio de Janeiro', 'RJ', '2010-01-01 12:03:54', '2010-01-01 12:03:54', '0', null);
INSERT INTO `uf` VALUES ('6', 'Rio Grande do Sul', 'RS', '2010-01-01 12:04:12', '2010-01-01 12:04:12', '0', null);
INSERT INTO `uf` VALUES ('7', 'Bahia', 'BA', '2010-01-01 12:04:23', '2010-01-01 12:04:23', '0', null);
INSERT INTO `uf` VALUES ('8', 'Tocantins', 'TO', '2010-01-01 12:04:38', '2010-01-01 12:04:38', '0', null);
INSERT INTO `uf` VALUES ('9', 'Maranhão', 'MA', '2010-01-01 12:06:00', '2010-01-01 12:06:00', '0', null);
INSERT INTO `uf` VALUES ('10', 'Acre', 'AC', '2010-01-01 12:06:16', '2010-01-01 12:06:16', '0', null);
INSERT INTO `uf` VALUES ('11', 'Alagoas', 'AL', '2010-01-01 12:06:31', '2010-01-01 12:06:31', '0', null);
INSERT INTO `uf` VALUES ('12', 'Amapá', 'AP', '2010-01-01 12:06:50', '2010-01-01 12:06:50', '0', null);
INSERT INTO `uf` VALUES ('13', 'Amazonas', 'AM', '2010-01-01 12:07:01', '2010-01-01 12:07:01', '0', null);
INSERT INTO `uf` VALUES ('14', 'Ceará', 'CE', '2010-01-01 12:07:21', '2010-01-01 12:07:21', '0', null);
INSERT INTO `uf` VALUES ('15', 'Distrito Federal', 'DF', '2010-01-01 12:07:40', '2010-01-01 12:07:40', '0', null);
INSERT INTO `uf` VALUES ('16', 'Goiás', 'GO', '2010-01-01 12:08:08', '2010-01-01 12:08:08', '0', null);
INSERT INTO `uf` VALUES ('17', 'Mato Grosso', 'MT', '2010-01-01 12:08:30', '2010-01-01 12:08:30', '0', null);
INSERT INTO `uf` VALUES ('18', 'Mato Grosso do Sul', 'MS', '2010-01-01 12:08:54', '2010-01-01 12:08:54', '0', null);
INSERT INTO `uf` VALUES ('19', 'Paraíba', 'PB', '2010-01-01 12:09:15', '2010-01-01 12:09:15', '0', null);
INSERT INTO `uf` VALUES ('20', 'Paraná', 'PR', '2010-01-01 12:09:43', '2010-01-01 12:09:43', '0', null);
INSERT INTO `uf` VALUES ('21', 'Pernambuco', 'PE', '2010-01-01 12:10:09', '2010-01-01 12:10:09', '0', null);
INSERT INTO `uf` VALUES ('22', 'Piauí', 'PI', '2010-01-01 12:10:36', '2010-01-01 12:10:36', '0', null);
INSERT INTO `uf` VALUES ('23', 'Rio Grande do Norte', 'RN', '2010-01-01 12:10:55', '2010-01-01 12:10:55', '0', null);
INSERT INTO `uf` VALUES ('24', 'Rondônia', 'RO', '2010-01-01 12:11:15', '2010-01-01 12:11:15', '0', null);
INSERT INTO `uf` VALUES ('25', 'Roraima', 'RR', '2010-01-01 12:11:45', '2010-01-01 12:11:45', '0', null);
INSERT INTO `uf` VALUES ('26', 'Santa Catarina', 'SC', '2010-01-01 12:12:03', '2010-01-01 12:12:03', '0', null);
INSERT INTO `uf` VALUES ('27', 'Sergipe', 'SE', '2010-01-01 12:12:20', '2010-01-01 12:12:20', '0', null);

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `pagina_inicial` text,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_usuario_usuario` (`usuario_tipo_id_INT`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Desenvolvimento', 'eduardo@omegasoftware.com.br', '8ffbcb8b27121de4a439478002b454cd', '1', '1', null, '0', null);
INSERT INTO `usuario` VALUES ('2', 'Roger', 'roger@omegasoftware.com.br', '18563035b358c4f559f4bbc16004bf79', '1', '1', '', '0', null);

-- ----------------------------
-- Table structure for `usuario_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_menu`;
CREATE TABLE `usuario_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_menu_FK_88470459` (`usuario_id_INT`),
  CONSTRAINT `usuario_menu_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_menu
-- ----------------------------
INSERT INTO `usuario_menu` VALUES ('1', '21', 'Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('2', '21', 'Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('3', '21', 'Gráfico de Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('4', '21', 'Organogramas', '0', null);
INSERT INTO `usuario_menu` VALUES ('5', '21', 'Organograma de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('6', '21', 'Organograma para Impressão', '0', null);
INSERT INTO `usuario_menu` VALUES ('7', '21', 'Alterar Estrutura Organizacional', '0', null);
INSERT INTO `usuario_menu` VALUES ('8', '21', 'Visualizar Pendências', '0', null);
INSERT INTO `usuario_menu` VALUES ('9', '21', 'Planejamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('10', '21', 'Cadastrar Milestone', '0', null);
INSERT INTO `usuario_menu` VALUES ('11', '21', 'Gerenciar Milestones', '0', null);
INSERT INTO `usuario_menu` VALUES ('12', '21', 'Associar Milestones a Vagas', '0', null);
INSERT INTO `usuario_menu` VALUES ('13', '21', 'Seleção', '0', null);
INSERT INTO `usuario_menu` VALUES ('14', '21', 'Cadastrar Currículo', '0', null);
INSERT INTO `usuario_menu` VALUES ('15', '21', 'Currículos em Seleção', '0', null);
INSERT INTO `usuario_menu` VALUES ('16', '21', 'Acompanhamento dos Pedidos de Vaga', '0', null);
INSERT INTO `usuario_menu` VALUES ('17', '21', 'Relatórios', '0', null);
INSERT INTO `usuario_menu` VALUES ('18', '21', 'Resumo do Processo de Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('19', '21', 'Relatório de Pessoas Associadas', '0', null);
INSERT INTO `usuario_menu` VALUES ('20', '21', 'Relatório de Pessoas Desassociadas', '0', null);
INSERT INTO `usuario_menu` VALUES ('21', '21', 'Gestão do Tácito', '0', null);
INSERT INTO `usuario_menu` VALUES ('22', '21', 'Mix de Similaridades', '0', null);
INSERT INTO `usuario_menu` VALUES ('23', '21', 'Coletivos de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('24', '21', 'Criar Coletivos', '0', null);
INSERT INTO `usuario_menu` VALUES ('25', '21', 'Criar Coletivos: Força Tarefa', '0', null);
INSERT INTO `usuario_menu` VALUES ('26', '21', 'Planejar Mix', '0', null);
INSERT INTO `usuario_menu` VALUES ('27', '21', 'Acompanhar Mix', '0', null);
INSERT INTO `usuario_menu` VALUES ('28', '21', 'Padrão de Níveis de Similaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('29', '21', 'Cadastrar Padrão de NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('30', '21', 'Gerenciar Padrões de NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('31', '21', 'Gestão do Tácito', '0', null);
INSERT INTO `usuario_menu` VALUES ('32', '21', 'Aporte de Conhecimento Tácito', '0', null);
INSERT INTO `usuario_menu` VALUES ('33', '21', 'Experiência Profissional por NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('34', '21', 'Experiência Profissional', '0', null);
INSERT INTO `usuario_menu` VALUES ('35', '21', 'Organograma p/ Análise', '0', null);
INSERT INTO `usuario_menu` VALUES ('36', '21', 'Redes Relacionais', '0', null);
INSERT INTO `usuario_menu` VALUES ('37', '21', 'Visualizar Pendências', '0', null);
INSERT INTO `usuario_menu` VALUES ('38', '21', 'Padrões de NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('39', '21', 'Experiências Profissionais', '0', null);
INSERT INTO `usuario_menu` VALUES ('40', '21', 'Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('41', '21', 'Planejamento de Demanda', '0', null);
INSERT INTO `usuario_menu` VALUES ('42', '21', 'Unidades Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('43', '21', 'Cadastrar Unidade de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('44', '21', 'Gerenciar Unidades de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('45', '21', 'Grupos de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('46', '21', 'Cadastrar Grupo de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('47', '21', 'Gerenciar Grupos de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('48', '21', 'Rotas de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('49', '21', 'Cadastrar Rotas de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('50', '21', 'Gerenciar Rotas de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('51', '21', 'Duração Máxima por Função / NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('52', '21', 'Planejamento Real', '0', null);
INSERT INTO `usuario_menu` VALUES ('53', '21', 'Montar Turmas', '0', null);
INSERT INTO `usuario_menu` VALUES ('54', '21', 'Acompanhamento / Finalização', '0', null);
INSERT INTO `usuario_menu` VALUES ('55', '21', 'Lista de Chamada', '0', null);
INSERT INTO `usuario_menu` VALUES ('56', '21', 'Ajustes', '0', null);
INSERT INTO `usuario_menu` VALUES ('57', '21', 'Relatórios', '0', null);
INSERT INTO `usuario_menu` VALUES ('58', '21', 'Por Pessoa', '0', null);
INSERT INTO `usuario_menu` VALUES ('59', '21', 'Por Unidade de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('60', '21', 'Metodologia', '0', null);
INSERT INTO `usuario_menu` VALUES ('61', '21', 'Cadastrar Metodologia', '0', null);
INSERT INTO `usuario_menu` VALUES ('62', '21', 'Gerenciar Metodologias', '0', null);
INSERT INTO `usuario_menu` VALUES ('63', '21', 'Tipos de Imersão', '0', null);
INSERT INTO `usuario_menu` VALUES ('64', '21', 'Cadastrar Tipo de Imersão', '0', null);
INSERT INTO `usuario_menu` VALUES ('65', '21', 'Gerenciar Tipo de Imersão', '0', null);
INSERT INTO `usuario_menu` VALUES ('66', '21', 'Start-up', '0', null);
INSERT INTO `usuario_menu` VALUES ('67', '21', 'Áreas', '0', null);
INSERT INTO `usuario_menu` VALUES ('68', '21', 'Cadastrar Áreas', '0', null);
INSERT INTO `usuario_menu` VALUES ('69', '21', 'Gerenciar Áreas', '0', null);
INSERT INTO `usuario_menu` VALUES ('70', '21', 'Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('71', '21', 'Cadastrar Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('72', '21', 'Gerenciar Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('73', '21', 'Ordenar Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('74', '21', 'Parâmetros de Processo', '0', null);
INSERT INTO `usuario_menu` VALUES ('75', '21', 'Cadastrar Parâmetros', '0', null);
INSERT INTO `usuario_menu` VALUES ('76', '21', 'Gerenciar Parâmetros', '0', null);
INSERT INTO `usuario_menu` VALUES ('77', '21', 'Grupos', '0', null);
INSERT INTO `usuario_menu` VALUES ('78', '21', 'Cadastrar Grupo', '0', null);
INSERT INTO `usuario_menu` VALUES ('79', '21', 'Gerenciar Grupos', '0', null);
INSERT INTO `usuario_menu` VALUES ('80', '21', 'Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('81', '21', 'Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('82', '21', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('83', '21', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('84', '21', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_menu` VALUES ('85', '21', 'Textos', '0', null);
INSERT INTO `usuario_menu` VALUES ('86', '21', 'Alterar Texto do Estudo', '0', null);
INSERT INTO `usuario_menu` VALUES ('87', '21', 'Alterar Texto do Termo de Consentimento', '0', null);
INSERT INTO `usuario_menu` VALUES ('88', '21', 'Glossário (Termos)', '0', null);
INSERT INTO `usuario_menu` VALUES ('89', '21', 'Cadastrar Termo', '0', null);
INSERT INTO `usuario_menu` VALUES ('90', '21', 'Gerenciar Glossário', '0', null);
INSERT INTO `usuario_menu` VALUES ('91', '21', 'Outras Ações', '0', null);
INSERT INTO `usuario_menu` VALUES ('92', '21', 'Gerar Mapa de Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('93', '21', 'Áreas Participantes', '0', null);
INSERT INTO `usuario_menu` VALUES ('94', '21', 'Outras Ações', '0', null);
INSERT INTO `usuario_menu` VALUES ('95', '21', 'Configurações', '0', null);
INSERT INTO `usuario_menu` VALUES ('96', '21', 'Relatório de Erros', '0', null);
INSERT INTO `usuario_menu` VALUES ('97', '21', 'Análise Retroativa (Estudo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('98', '21', 'Assistente para Novo Projeto', '0', null);
INSERT INTO `usuario_menu` VALUES ('99', '21', 'Cadastros', '0', null);
INSERT INTO `usuario_menu` VALUES ('100', '21', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('101', '21', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('102', '21', 'Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('103', '21', 'Cadastrar Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('104', '21', 'Gerenciar Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('105', '21', 'Estados Civis', '0', null);
INSERT INTO `usuario_menu` VALUES ('106', '21', 'Cadastrar Estado Civil', '0', null);
INSERT INTO `usuario_menu` VALUES ('107', '21', 'Gerenciar Estados Civis', '0', null);
INSERT INTO `usuario_menu` VALUES ('108', '21', 'Escolaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('109', '21', 'Cadastrar Escolaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('110', '21', 'Gerenciar Escolaridades', '0', null);
INSERT INTO `usuario_menu` VALUES ('111', '21', 'Cadastros Gerais  ', '0', null);
INSERT INTO `usuario_menu` VALUES ('112', '21', 'Faixas Salariais', '0', null);
INSERT INTO `usuario_menu` VALUES ('113', '21', 'Cadastrar Faixa Salarial', '0', null);
INSERT INTO `usuario_menu` VALUES ('114', '21', 'Gerenciar Faixas Salariais', '0', null);
INSERT INTO `usuario_menu` VALUES ('115', '21', 'Gênero (Sexo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('116', '21', 'Cadastrar Gênero (Sexo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('117', '21', 'Gerenciar Gênero (Sexo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('118', '21', 'Legendas do Organograma', '0', null);
INSERT INTO `usuario_menu` VALUES ('119', '21', 'Cadastrar Legenda', '0', null);
INSERT INTO `usuario_menu` VALUES ('120', '21', 'Gerenciar Legendas', '0', null);
INSERT INTO `usuario_menu` VALUES ('121', '21', 'Cadastros Gerais   ', '0', null);
INSERT INTO `usuario_menu` VALUES ('122', '21', 'Linhas', '0', null);
INSERT INTO `usuario_menu` VALUES ('123', '21', 'Cadastrar Linha', '0', null);
INSERT INTO `usuario_menu` VALUES ('124', '21', 'Gerenciar Linhas', '0', null);
INSERT INTO `usuario_menu` VALUES ('125', '21', 'Locais de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('126', '21', 'Cadastrar Local de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('127', '21', 'Gerenciar Locais de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('128', '21', 'Motivos de Desassociação', '0', null);
INSERT INTO `usuario_menu` VALUES ('129', '21', 'Cadastrar Motivo de Desassociação', '0', null);
INSERT INTO `usuario_menu` VALUES ('130', '21', 'Gerenciar Motivos de Desassociação', '0', null);
INSERT INTO `usuario_menu` VALUES ('131', '21', 'Cadastros Gerais    ', '0', null);
INSERT INTO `usuario_menu` VALUES ('132', '21', 'Nível Funcional na Hierarquia', '0', null);
INSERT INTO `usuario_menu` VALUES ('133', '21', 'Cadastrar Nível', '0', null);
INSERT INTO `usuario_menu` VALUES ('134', '21', 'Gerenciar Níveis', '0', null);
INSERT INTO `usuario_menu` VALUES ('135', '21', 'Países', '0', null);
INSERT INTO `usuario_menu` VALUES ('136', '21', 'Cadastrar País', '0', null);
INSERT INTO `usuario_menu` VALUES ('137', '21', 'Gerenciar Países', '0', null);
INSERT INTO `usuario_menu` VALUES ('138', '21', 'Status do Processo dos PVs', '0', null);
INSERT INTO `usuario_menu` VALUES ('139', '21', 'Cadastrar Status', '0', null);
INSERT INTO `usuario_menu` VALUES ('140', '21', 'Gerenciar Status', '0', null);
INSERT INTO `usuario_menu` VALUES ('141', '21', 'Cadastros Gerais     ', '0', null);
INSERT INTO `usuario_menu` VALUES ('142', '21', 'Unidades de Medida', '0', null);
INSERT INTO `usuario_menu` VALUES ('143', '21', 'Cadastrar Unidades', '0', null);
INSERT INTO `usuario_menu` VALUES ('144', '21', 'Gerenciar Unidades', '0', null);
INSERT INTO `usuario_menu` VALUES ('145', '21', 'Unidades Federativas', '0', null);
INSERT INTO `usuario_menu` VALUES ('146', '21', 'Cadastrar Estado', '0', null);
INSERT INTO `usuario_menu` VALUES ('147', '21', 'Gerenciar Estados', '0', null);
INSERT INTO `usuario_menu` VALUES ('148', '21', 'Nomes dos Níveis Estrutura', '0', null);
INSERT INTO `usuario_menu` VALUES ('149', '21', 'Cadastrar Nome', '0', null);
INSERT INTO `usuario_menu` VALUES ('150', '21', 'Gerenciar Nomes', '0', null);
INSERT INTO `usuario_menu` VALUES ('151', '21', 'Cadastros Gerais      ', '0', null);
INSERT INTO `usuario_menu` VALUES ('152', '21', 'Rede Relacional', '0', null);
INSERT INTO `usuario_menu` VALUES ('153', '21', 'Cadastrar Tipo de Relação', '0', null);
INSERT INTO `usuario_menu` VALUES ('154', '21', 'Gerenciar Tipos de Relação', '0', null);
INSERT INTO `usuario_menu` VALUES ('155', '21', 'Cadastrar Tipo de Relação entre Colegas de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('156', '21', 'Gerenciar Tipos de Relação entre Colegas de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('157', '21', 'Sincronização', '0', null);
INSERT INTO `usuario_menu` VALUES ('158', '21', 'Sincronização', '0', null);
INSERT INTO `usuario_menu` VALUES ('159', '21', 'Sincronização ', '0', null);
INSERT INTO `usuario_menu` VALUES ('160', '21', 'Importar Folha de Pagamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('161', '21', 'Importar Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('162', '21', 'Importar Niveis/Estrutura Org.', '0', null);
INSERT INTO `usuario_menu` VALUES ('163', '21', 'Sincronização  ', '0', null);
INSERT INTO `usuario_menu` VALUES ('164', '21', 'Importar Dados Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('165', '21', 'Importar Vagas', '0', null);
INSERT INTO `usuario_menu` VALUES ('166', '21', 'Importar Dados Pessoais', '0', null);
INSERT INTO `usuario_menu` VALUES ('167', '21', 'Sincronização   ', '0', null);
INSERT INTO `usuario_menu` VALUES ('168', '21', 'Importar Experiências Práticas', '0', null);
INSERT INTO `usuario_menu` VALUES ('169', '21', 'Importar Associações Pessoa-Vaga', '0', null);
INSERT INTO `usuario_menu` VALUES ('170', '21', 'Importar Pedidos de Vaga', '0', null);
INSERT INTO `usuario_menu` VALUES ('171', '21', 'Sincronização     ', '0', null);
INSERT INTO `usuario_menu` VALUES ('172', '21', 'Importar Padrões de Níveis de Similaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('173', '21', 'Importar Planejamento de Treinamentos', '0', null);
INSERT INTO `usuario_menu` VALUES ('174', '2', 'Blog', '0', null);
INSERT INTO `usuario_menu` VALUES ('175', '2', 'Notícia', '0', null);
INSERT INTO `usuario_menu` VALUES ('176', '2', 'Cadastrar Notícia', '0', null);
INSERT INTO `usuario_menu` VALUES ('177', '2', 'Gerenciar Notícia', '0', null);
INSERT INTO `usuario_menu` VALUES ('178', '2', 'Categoria', '0', null);
INSERT INTO `usuario_menu` VALUES ('179', '2', 'Cadastrar Categoria', '0', null);
INSERT INTO `usuario_menu` VALUES ('180', '2', 'Gerenciar Categoria', '0', null);
INSERT INTO `usuario_menu` VALUES ('181', '2', 'Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('182', '2', 'Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('183', '2', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('184', '2', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('185', '2', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_menu` VALUES ('186', '2', 'Usuários', '0', null);
INSERT INTO `usuario_menu` VALUES ('187', '2', 'Usuários', '0', null);
INSERT INTO `usuario_menu` VALUES ('188', '2', 'Cadastrar Usuário do Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('189', '2', 'Gerenciar Usuários do Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('190', '2', 'Visualizar Operações de Usuários no sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('191', '2', 'Classes de Usuários', '0', null);
INSERT INTO `usuario_menu` VALUES ('192', '2', 'Cadastrar Classe de Usuário', '0', null);
INSERT INTO `usuario_menu` VALUES ('193', '2', 'Gerenciar Classes de Usuário', '0', null);
INSERT INTO `usuario_menu` VALUES ('194', '2', 'Sugestões', '0', null);
INSERT INTO `usuario_menu` VALUES ('195', '2', 'Cadastrar Sugestão', '0', null);
INSERT INTO `usuario_menu` VALUES ('196', '2', 'Visualizar Sugestões', '0', null);

-- ----------------------------
-- Table structure for `usuario_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_privilegio`;
CREATE TABLE `usuario_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_privilegio_FK_666168213` (`usuario_id_INT`),
  CONSTRAINT `usuario_privilegio_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo`;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_visivel` varchar(255) DEFAULT NULL,
  `status_BOOLEAN` int(11) NOT NULL,
  `pagina_inicial` text,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo
-- ----------------------------
INSERT INTO `usuario_tipo` VALUES ('1', 'Usuário Administrador', 'Usuário Administrador', '1', '', '0', null);

-- ----------------------------
-- Table structure for `usuario_tipo_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_menu`;
CREATE TABLE `usuario_tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_tipo_menu_FK_263549804` (`usuario_tipo_id_INT`),
  CONSTRAINT `usuario_tipo_menu_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10438 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_menu
-- ----------------------------
INSERT INTO `usuario_tipo_menu` VALUES ('10402', '1', 'Visualizar Lista de Clientes', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10403', '1', 'Visualizar Cobranças Geradas', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10404', '1', 'Hospedagens', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10405', '1', 'Cadastrar Nova Hospedagem', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10406', '1', 'Visualizar Lista de Hospedagens', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10407', '1', 'Serviços', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10408', '1', 'Cadastrar Novo Serviço', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10409', '1', 'Visualizar Lista de Serviços', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10410', '1', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10411', '1', 'Empresas de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10412', '1', 'Cadastrar Empresa de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10413', '1', 'Gerenciar Empresas de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10414', '1', 'Status dos Pagamentos', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10415', '1', 'Cadastrar Status', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10416', '1', 'Gerenciar Status', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10417', '1', 'Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10418', '1', 'Cadastrar Tipo de Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10419', '1', 'Gerenciar Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10420', '1', 'Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10421', '1', 'Gerar Cobranças Agora', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10422', '1', 'Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10423', '1', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10424', '1', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10425', '1', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10426', '1', 'Fazer Limpeza do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10427', '1', 'Configurações de Envio', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10428', '1', 'Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10429', '1', 'Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10430', '1', 'Cadastrar Usuário do Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10431', '1', 'Gerenciar Usuários do Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10432', '1', 'Visualizar Operações de Usuários no sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10433', '1', 'Classes de Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10434', '1', 'Cadastrar Classe de Usuário', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10435', '1', 'Gerenciar Classes de Usuário', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10436', '1', 'Relatórios', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10437', '1', 'Fluxo de Caixa', '0', null);

-- ----------------------------
-- Table structure for `usuario_tipo_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_privilegio`;
CREATE TABLE `usuario_tipo_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_tipo_privilegio_FK_997283936` (`usuario_tipo_id_INT`),
  CONSTRAINT `usuario_tipo_privilegio_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `__acesso`
-- ----------------------------
DROP TABLE IF EXISTS `__acesso`;
CREATE TABLE `__acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_login_SEC` int(10) DEFAULT NULL,
  `data_login_OFFSEC` int(6) DEFAULT NULL,
  `data_logout_SEC` int(10) DEFAULT NULL,
  `data_logout_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__acesso_FK_54473877` (`crud_mobile_id_INT`),
  CONSTRAINT `__acesso_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __acesso
-- ----------------------------

-- ----------------------------
-- Table structure for `__api`
-- ----------------------------
DROP TABLE IF EXISTS `__api`;
CREATE TABLE `__api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __api
-- ----------------------------

-- ----------------------------
-- Table structure for `__api_id`
-- ----------------------------
DROP TABLE IF EXISTS `__api_id`;
CREATE TABLE `__api_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `chave` varchar(255) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `api_id_FK_814331055` (`pessoa_id_INT`),
  KEY `api_id_FK_701324463` (`usuario_id_INT`),
  KEY `api_id_FK_867980957` (`empresa_id_INT`),
  KEY `api_id_FK_830413819` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __api_id
-- ----------------------------

-- ----------------------------
-- Table structure for `__area_menu`
-- ----------------------------
DROP TABLE IF EXISTS `__area_menu`;
CREATE TABLE `__area_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(252) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_7388916` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__area_menu_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __area_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `__atividade`
-- ----------------------------
DROP TABLE IF EXISTS `__atividade`;
CREATE TABLE `__atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `atividade_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_horas_INT` int(3) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_FK_389770508` (`empresa_id_INT`),
  KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`),
  KEY `atividade_FK_219207763` (`atividade_unidade_medida_id_INT`),
  KEY `atividade_FK_411010742` (`relatorio_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __atividade
-- ----------------------------

-- ----------------------------
-- Table structure for `__atividade_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `__atividade_tipo`;
CREATE TABLE `__atividade_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `nome_normalizado` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_098787` (`nome`,`corporacao_id_INT`),
  KEY `empresa_servico_tipo_FK_604431152` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __atividade_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `__atividade_tipos`
-- ----------------------------
DROP TABLE IF EXISTS `__atividade_tipos`;
CREATE TABLE `__atividade_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atividade_tipo_id_INT` int(11) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `atividade_tipos_FK_423400879` (`atividade_tipo_id_INT`),
  KEY `atividade_tipos_FK_996185303` (`atividade_id_INT`),
  KEY `atividade_tipos_FK_789062500` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __atividade_tipos
-- ----------------------------

-- ----------------------------
-- Table structure for `__atividade_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `__atividade_unidade_medida`;
CREATE TABLE `__atividade_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `atividade_unidade_medida_FK_833953858` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __atividade_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `__bairro`
-- ----------------------------
DROP TABLE IF EXISTS `__bairro`;
CREATE TABLE `__bairro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__bairro_FK_48767089` (`crud_mobile_id_INT`),
  CONSTRAINT `__bairro_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __bairro
-- ----------------------------

-- ----------------------------
-- Table structure for `__categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `__categoria_permissao`;
CREATE TABLE `__categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__categoria_permissao_FK_56915283` (`crud_mobile_id_INT`),
  CONSTRAINT `__categoria_permissao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `__cidade`
-- ----------------------------
DROP TABLE IF EXISTS `__cidade`;
CREATE TABLE `__cidade` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `uf_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__cidade_FK_473602295` (`crud_mobile_id_INT`),
  CONSTRAINT `__cidade_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __cidade
-- ----------------------------

-- ----------------------------
-- Table structure for `__configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `__configuracao`;
CREATE TABLE `__configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_projeto` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_2788696` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `fk_crud_mobile_904235` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __configuracao
-- ----------------------------

-- ----------------------------
-- Table structure for `__corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `__corporacao`;
CREATE TABLE `__corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `usuario_dropbox` varchar(255) DEFAULT NULL,
  `senha_dropbox` varchar(255) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__corporacao_FK_960388184` (`crud_mobile_id_INT`),
  CONSTRAINT `__corporacao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `__despesa`
-- ----------------------------
DROP TABLE IF EXISTS `__despesa`;
CREATE TABLE `__despesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_FLOAT` double DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `despesa_cotidiano_id_INT` int(11) DEFAULT NULL,
  `valor_pagamento_FLOAT` double DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `pagamento_usuario_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `despesa_FK_685638428` (`empresa_id_INT`),
  KEY `despesa_FK_939453125` (`despesa_cotidiano_id_INT`),
  KEY `despesa_FK_777130127` (`cadastro_usuario_id_INT`),
  KEY `despesa_FK_818298340` (`pagamento_usuario_id_INT`),
  KEY `despesa_FK_352508545` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __despesa
-- ----------------------------

-- ----------------------------
-- Table structure for `__despesa_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `__despesa_cotidiano`;
CREATE TABLE `__despesa_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `despesa_da_empresa_id_INT` int(11) DEFAULT NULL,
  `despesa_do_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `id_tipo_despesa_cotidiano_INT` int(3) DEFAULT NULL,
  `parametro_INT` int(11) DEFAULT NULL,
  `parametro_OFFSEC` int(10) DEFAULT NULL,
  `parametro_SEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `despesa_cotidiano_FK_33874511` (`despesa_da_empresa_id_INT`),
  KEY `despesa_cotidiano_FK_475311279` (`despesa_do_usuario_id_INT`),
  KEY `despesa_cotidiano_FK_201293945` (`cadastro_usuario_id_INT`),
  KEY `despesa_cotidiano_FK_755279541` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __despesa_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa`;
CREATE TABLE `__empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `telefone1` varchar(30) DEFAULT NULL,
  `telefone2` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `operadora_id_INT` int(11) DEFAULT NULL,
  `celular_sms` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tipo_documento_id_INT` int(11) DEFAULT NULL,
  `numero_documento` varchar(30) DEFAULT NULL,
  `tipo_empresa_id_INT` int(11) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro_id_INT` int(11) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_FK_242858886` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_atividade`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_atividade`;
CREATE TABLE `__empresa_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_horas_INT` int(3) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `preco_venda_FLOAT` double DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_FK_389770508` (`empresa_id_INT`),
  KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`),
  KEY `empresa_atividade_FK_124084472` (`atividade_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_atividade
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_atividade_compra`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_atividade_compra`;
CREATE TABLE `__empresa_atividade_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_compra_FK_892700196` (`empresa_compra_id_INT`),
  KEY `empresa_servico_compra_FK_118560791` (`corporacao_id_INT`),
  KEY `empresa_atividade_compra_FK_668334961` (`atividade_id_INT`),
  KEY `empresa_atividade_compra_FK_537750244` (`empresa_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_atividade_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_atividade_venda`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_atividade_venda`;
CREATE TABLE `__empresa_atividade_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_venda_FK_937438965` (`empresa_venda_id_INT`),
  KEY `empresa_servico_venda_FK_338836670` (`corporacao_id_INT`),
  KEY `empresa_atividade_venda_FK_500762939` (`atividade_id_INT`),
  KEY `empresa_atividade_venda_FK_194274902` (`empresa_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_atividade_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_compra`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_compra`;
CREATE TABLE `__empresa_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `minha_empresa_id_INT` int(11) DEFAULT NULL,
  `outra_empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_compra_FK_319244385` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_compra_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_compra_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_compra_parcela`;
CREATE TABLE `__empresa_compra_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `valor_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_compra_parcela_FK_649597168` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_compra_parcela_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_compra_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_equipe`;
CREATE TABLE `__empresa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_1314697` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_1314697` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_perfil`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_perfil`;
CREATE TABLE `__empresa_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `perfil_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_perfil_FK_846252442` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_perfil_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_perfil
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_produto`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_produto`;
CREATE TABLE `__empresa_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `preco_venda_FLOAT` double DEFAULT NULL,
  `estoque_atual_INT` int(11) DEFAULT NULL,
  `estoque_minimo_INT` int(11) DEFAULT NULL,
  `prazo_reposicao_estoque_dias_INT` int(6) DEFAULT NULL,
  `codigo_barra` char(13) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_produto_FK_674194336` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_produto_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_produto
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_produto_compra`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_produto_compra`;
CREATE TABLE `__empresa_produto_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_produto_compra_FK_339080810` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_produto_compra_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_produto_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_produto_foto`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_produto_foto`;
CREATE TABLE `__empresa_produto_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_produto_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_produto_foto_FK_64880371` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_8110352` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_produto_foto
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_produto_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_produto_tipo`;
CREATE TABLE `__empresa_produto_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_produto_tipo_FK_979949952` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_5295715` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_produto_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_produto_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_produto_unidade_medida`;
CREATE TABLE `__empresa_produto_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `abreviacao` varchar(10) NOT NULL,
  `is_float_BOOLEAN` int(1) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_produto_unidade_medida_FK_43701171` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_5844116` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_produto_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_produto_venda`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_produto_venda`;
CREATE TABLE `__empresa_produto_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_produto_venda_FK_713684082` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_produto_venda_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_produto_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_servico`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_servico`;
CREATE TABLE `__empresa_servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `empresa_servico_tipo_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `empresa_servico_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_meses_INT` int(3) DEFAULT NULL,
  `video` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_servico_FK_176422119` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_6088562` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_servico
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_servico_compra`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_servico_compra`;
CREATE TABLE `__empresa_servico_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_servico_id_INT` int(11) DEFAULT NULL,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_servico_compra_FK_102172851` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_3826904` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_servico_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_servico_foto`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_servico_foto`;
CREATE TABLE `__empresa_servico_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_servico_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_servico_foto_FK_831909180` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_6524964` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_servico_foto
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_servico_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_servico_tipo`;
CREATE TABLE `__empresa_servico_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `nome_normalizado` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_servico_tipo_FK_617340088` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_5457153` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_servico_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_servico_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_servico_unidade_medida`;
CREATE TABLE `__empresa_servico_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_servico_unidade_medida_FK_765167237` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_7034607` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_servico_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_servico_venda`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_servico_venda`;
CREATE TABLE `__empresa_servico_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_servico_id_INT` int(11) DEFAULT NULL,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_servico_venda_FK_12054443` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_7570801` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_servico_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_tipo_venda`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_tipo_venda`;
CREATE TABLE `__empresa_tipo_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_produto_id_INT` int(11) DEFAULT NULL,
  `empresa_servico_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_tipo_venda_FK_109436035` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_2734680` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_tipo_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_tipo_venda_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_tipo_venda_parcela`;
CREATE TABLE `__empresa_tipo_venda_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_tipo_venda_id_INT` int(11) DEFAULT NULL,
  `porcentagem_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_tipo_venda_parcela_FK_257751465` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_7941284` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_tipo_venda_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_venda`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_venda`;
CREATE TABLE `__empresa_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `cliente_empresa_id_INT` int(11) DEFAULT NULL,
  `fornecedor_empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `acrescimo_FLOAT` double DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `fechamento_SEC` int(10) DEFAULT NULL,
  `fechamento_OFFSEC` int(6) DEFAULT NULL,
  `valor_pago_FLOAT` double DEFAULT NULL,
  `mesa_id_INT` int(11) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_venda_FK_286743164` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_venda_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_venda_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_venda_parcela`;
CREATE TABLE `__empresa_venda_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `valor_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `seq_INT` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__empresa_venda_parcela_FK_393798828` (`crud_mobile_id_INT`),
  CONSTRAINT `__empresa_venda_parcela_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_venda_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `__empresa_venda_template`
-- ----------------------------
DROP TABLE IF EXISTS `__empresa_venda_template`;
CREATE TABLE `__empresa_venda_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `template_json` varchar(512) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_venda_template_FK_418518066` (`produto_id_INT`),
  KEY `empresa_venda_template_FK_963958741` (`empresa_id_INT`),
  KEY `empresa_venda_template_FK_776367188` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __empresa_venda_template
-- ----------------------------

-- ----------------------------
-- Table structure for `__estado_civil`
-- ----------------------------
DROP TABLE IF EXISTS `__estado_civil`;
CREATE TABLE `__estado_civil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__estado_civil_FK_687316895` (`crud_mobile_id_INT`),
  CONSTRAINT `__estado_civil_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __estado_civil
-- ----------------------------

-- ----------------------------
-- Table structure for `__forma_pagamento`
-- ----------------------------
DROP TABLE IF EXISTS `__forma_pagamento`;
CREATE TABLE `__forma_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__forma_pagamento_FK_403625488` (`crud_mobile_id_INT`),
  CONSTRAINT `__forma_pagamento_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __forma_pagamento
-- ----------------------------

-- ----------------------------
-- Table structure for `__horario_trabalho_pessoa_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `__horario_trabalho_pessoa_empresa`;
CREATE TABLE `__horario_trabalho_pessoa_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio_TIME` time DEFAULT NULL,
  `hora_fim_TIME` time DEFAULT NULL,
  `pessoa_empresa_id_INT` int(11) DEFAULT NULL,
  `data_DATE` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `dia_semana_INT` int(2) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__horario_trabalho_pessoa_empresa_FK_471405029` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_929870` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __horario_trabalho_pessoa_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `__mensagem`
-- ----------------------------
DROP TABLE IF EXISTS `__mensagem`;
CREATE TABLE `__mensagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `mensagem` varchar(512) DEFAULT NULL,
  `remetente_usuario_id_INT` int(11) DEFAULT NULL,
  `tipo_mensagem_id_INT` int(11) DEFAULT NULL,
  `destinatario_usuario_id_INT` int(11) DEFAULT NULL,
  `destinatario_pessoa_id_INT` int(11) DEFAULT NULL,
  `destinatario_empresa_id_INT` int(11) DEFAULT NULL,
  `data_min_envio_SEC` int(10) DEFAULT NULL,
  `data_min_envio_OFFSEC` int(6) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `empresa_para_cliente_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mensagem_FK_646789551` (`remetente_usuario_id_INT`),
  KEY `mensagem_FK_970550538` (`tipo_mensagem_id_INT`),
  KEY `mensagem_FK_119873046` (`destinatario_usuario_id_INT`),
  KEY `mensagem_FK_67901611` (`destinatario_pessoa_id_INT`),
  KEY `mensagem_FK_637390137` (`destinatario_empresa_id_INT`),
  KEY `mensagem_FK_258514404` (`registro_estado_id_INT`),
  KEY `mensagem_FK_570434570` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for `__mensagem_envio`
-- ----------------------------
DROP TABLE IF EXISTS `__mensagem_envio`;
CREATE TABLE `__mensagem_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_canal_envio_id_INT` int(11) DEFAULT NULL,
  `mensagem_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `obs` varchar(100) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `tentativa_INT` int(2) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mensagem_envio_FK_703460694` (`tipo_canal_envio_id_INT`),
  KEY `mensagem_envio_FK_757080078` (`mensagem_id_INT`),
  KEY `mensagem_envio_FK_273468017` (`registro_estado_id_INT`),
  KEY `mensagem_envio_FK_794128418` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __mensagem_envio
-- ----------------------------

-- ----------------------------
-- Table structure for `__mesa`
-- ----------------------------
DROP TABLE IF EXISTS `__mesa`;
CREATE TABLE `__mesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `template_json` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mesa_FK_462615967` (`empresa_id_INT`),
  KEY `mesa_FK_317504883` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __mesa
-- ----------------------------

-- ----------------------------
-- Table structure for `__mesa_reserva`
-- ----------------------------
DROP TABLE IF EXISTS `__mesa_reserva`;
CREATE TABLE `__mesa_reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mesa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `reserva_SEC` int(10) DEFAULT NULL,
  `reserva_OFFSEC` int(6) DEFAULT NULL,
  `confimado_SEC` int(10) DEFAULT NULL,
  `confirmado_OFFSEC` int(6) DEFAULT NULL,
  `confirmador_usuario_id_INT` int(11) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mesa_reserva_FK_235290527` (`mesa_id_INT`),
  KEY `mesa_reserva_FK_304656982` (`pessoa_id_INT`),
  KEY `mesa_reserva_FK_974975586` (`confirmador_usuario_id_INT`),
  KEY `mesa_reserva_FK_961578370` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __mesa_reserva
-- ----------------------------

-- ----------------------------
-- Table structure for `__modelo`
-- ----------------------------
DROP TABLE IF EXISTS `__modelo`;
CREATE TABLE `__modelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `ano_INT` int(4) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__modelo_FK_890014649` (`crud_mobile_id_INT`),
  CONSTRAINT `__modelo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __modelo
-- ----------------------------

-- ----------------------------
-- Table structure for `__negociacao_divida`
-- ----------------------------
DROP TABLE IF EXISTS `__negociacao_divida`;
CREATE TABLE `__negociacao_divida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `devedor_empresa_id_INT` int(11) DEFAULT NULL,
  `devedor_pessoa_id_INT` int(11) DEFAULT NULL,
  `negociador_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `resultado_empresa_venda_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `negociacao_divida_FK_620697022` (`devedor_empresa_id_INT`),
  KEY `negociacao_divida_FK_110839843` (`devedor_pessoa_id_INT`),
  KEY `negociacao_divida_FK_708282471` (`negociador_usuario_id_INT`),
  KEY `negociacao_divida_FK_563903809` (`resultado_empresa_venda_id_INT`),
  KEY `negociacao_divida_FK_873504639` (`relatorio_id_INT`),
  KEY `negociacao_divida_FK_729370117` (`registro_estado_id_INT`),
  KEY `negociacao_divida_FK_940582276` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __negociacao_divida
-- ----------------------------

-- ----------------------------
-- Table structure for `__negociacao_divida_empresa_venda`
-- ----------------------------
DROP TABLE IF EXISTS `__negociacao_divida_empresa_venda`;
CREATE TABLE `__negociacao_divida_empresa_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_protocolo_INT` bigint(11) DEFAULT NULL,
  `negociacao_divida_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `negociacao_divida_empresa_venda_FK_449615478` (`negociacao_divida_id_INT`),
  KEY `negociacao_divida_empresa_venda_FK_306701660` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __negociacao_divida_empresa_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `__nota`
-- ----------------------------
DROP TABLE IF EXISTS `__nota`;
CREATE TABLE `__nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nota_FK_922271729` (`empresa_id_INT`),
  KEY `nota_FK_826232910` (`pessoa_id_INT`),
  KEY `nota_FK_654876709` (`usuario_id_INT`),
  KEY `nota_FK_336425781` (`veiculo_id_INT`),
  KEY `nota_FK_797149659` (`relatorio_id_INT`),
  KEY `nota_FK_750427246` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __nota
-- ----------------------------

-- ----------------------------
-- Table structure for `__nota_tipo_nota`
-- ----------------------------
DROP TABLE IF EXISTS `__nota_tipo_nota`;
CREATE TABLE `__nota_tipo_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota_id_INT` int(11) DEFAULT NULL,
  `tipo_nota_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nota_id_INT` (`nota_id_INT`,`tipo_nota_id_INT`,`corporacao_id_INT`),
  KEY `nota_tipo_nota_FK_277954101` (`tipo_nota_id_INT`),
  KEY `nota_tipo_nota_FK_861236573` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __nota_tipo_nota
-- ----------------------------

-- ----------------------------
-- Table structure for `__operadora`
-- ----------------------------
DROP TABLE IF EXISTS `__operadora`;
CREATE TABLE `__operadora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `nome_normalizado` varchar(50) NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__operadora_FK_176818847` (`crud_mobile_id_INT`),
  CONSTRAINT `__operadora_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __operadora
-- ----------------------------

-- ----------------------------
-- Table structure for `__pais`
-- ----------------------------
DROP TABLE IF EXISTS `__pais`;
CREATE TABLE `__pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__pais_FK_812408448` (`crud_mobile_id_INT`),
  CONSTRAINT `__pais_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pais
-- ----------------------------

-- ----------------------------
-- Table structure for `__perfil`
-- ----------------------------
DROP TABLE IF EXISTS `__perfil`;
CREATE TABLE `__perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__perfil_FK_200836181` (`crud_mobile_id_INT`),
  CONSTRAINT `__perfil_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __perfil
-- ----------------------------

-- ----------------------------
-- Table structure for `__permissao`
-- ----------------------------
DROP TABLE IF EXISTS `__permissao`;
CREATE TABLE `__permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `pai_permissao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__permissao_FK_368743896` (`crud_mobile_id_INT`),
  CONSTRAINT `__permissao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `__permissao_categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `__permissao_categoria_permissao`;
CREATE TABLE `__permissao_categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissao_id_INT` int(11) NOT NULL,
  `categoria_permissao_id_INT` int(11) NOT NULL,
  `corporacao_id_INT` int(11) NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__permissao_categoria_permissao_FK_27008056` (`crud_mobile_id_INT`),
  CONSTRAINT `__permissao_categoria_permissao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __permissao_categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `__pessoa`
-- ----------------------------
DROP TABLE IF EXISTS `__pessoa`;
CREATE TABLE `__pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `tipo_documento_id_INT` int(11) DEFAULT NULL,
  `numero_documento` varchar(30) DEFAULT NULL,
  `is_consumidor_BOOLEAN` int(1) DEFAULT NULL,
  `sexo_id_INT` int(11) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `operadora_id_INT` int(11) DEFAULT NULL,
  `celular_sms` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(30) DEFAULT '',
  `complemento` varchar(100) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `bairro_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `data_nascimento_SEC` int(10) DEFAULT NULL,
  `data_nascimento_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `ind_email_valido_BOOLEAN` int(1) DEFAULT NULL,
  `ind_celular_valido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__pessoa_FK_564758301` (`crud_mobile_id_INT`),
  CONSTRAINT `__pessoa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pessoa
-- ----------------------------

-- ----------------------------
-- Table structure for `__pessoa_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `__pessoa_empresa`;
CREATE TABLE `__pessoa_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `profissao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__pessoa_empresa_FK_587005615` (`crud_mobile_id_INT`),
  CONSTRAINT `__pessoa_empresa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pessoa_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `__pessoa_empresa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `__pessoa_empresa_equipe`;
CREATE TABLE `__pessoa_empresa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `pessoa_empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_8944397` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_8944397` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pessoa_empresa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `__pessoa_empresa_rotina`
-- ----------------------------
DROP TABLE IF EXISTS `__pessoa_empresa_rotina`;
CREATE TABLE `__pessoa_empresa_rotina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_empresa_id_INT` int(11) DEFAULT NULL,
  `semana_INT` int(4) NOT NULL,
  `dia_semana_INT` int(4) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__pessoa_empresa_rotina_FK_675811768` (`crud_mobile_id_INT`),
  CONSTRAINT `__pessoa_empresa_rotina_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pessoa_empresa_rotina
-- ----------------------------

-- ----------------------------
-- Table structure for `__pessoa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `__pessoa_equipe`;
CREATE TABLE `__pessoa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pessoa_equipe_FK_929412842` (`empresa_equipe_id_INT`),
  KEY `pessoa_equipe_FK_198364258` (`usuario_id_INT`),
  KEY `pessoa_equipe_FK_935943604` (`pessoa_id_INT`),
  KEY `pessoa_equipe_FK_316467285` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pessoa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `__pessoa_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `__pessoa_usuario`;
CREATE TABLE `__pessoa_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__pessoa_usuario_FK_688476563` (`crud_mobile_id_INT`),
  CONSTRAINT `__pessoa_usuario_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __pessoa_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `__ponto`
-- ----------------------------
DROP TABLE IF EXISTS `__ponto`;
CREATE TABLE `__ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `profissao_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `is_entrada_BOOLEAN` int(1) NOT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `tipo_ponto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__ponto_FK_951171875` (`crud_mobile_id_INT`),
  CONSTRAINT `__ponto_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __ponto
-- ----------------------------

-- ----------------------------
-- Table structure for `__ponto_endereco`
-- ----------------------------
DROP TABLE IF EXISTS `__ponto_endereco`;
CREATE TABLE `__ponto_endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ponto_id_INT` int(11) DEFAULT NULL,
  `endereco_completo` tinytext NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__ponto_endereco_FK_920684815` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_2779541` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __ponto_endereco
-- ----------------------------

-- ----------------------------
-- Table structure for `__produto`
-- ----------------------------
DROP TABLE IF EXISTS `__produto`;
CREATE TABLE `__produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `produto_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `prazo_reposicao_estoque_dias_INT` int(6) DEFAULT NULL,
  `codigo_barra` char(13) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_FK_183288574` (`corporacao_id_INT`),
  KEY `produto_FK_485992432` (`produto_unidade_medida_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __produto
-- ----------------------------

-- ----------------------------
-- Table structure for `__produto_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `__produto_tipo`;
CREATE TABLE `__produto_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_98765` (`nome`,`corporacao_id_INT`),
  KEY `empresa_produto_tipo_FK_926544190` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __produto_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `__produto_tipos`
-- ----------------------------
DROP TABLE IF EXISTS `__produto_tipos`;
CREATE TABLE `__produto_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_tipo_id_INT` int(11) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_tipos_FK_90423584` (`produto_tipo_id_INT`),
  KEY `produto_tipos_FK_811035157` (`produto_id_INT`),
  KEY `produto_tipos_FK_529571533` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __produto_tipos
-- ----------------------------

-- ----------------------------
-- Table structure for `__produto_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `__produto_unidade_medida`;
CREATE TABLE `__produto_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `abreviacao` varchar(10) NOT NULL,
  `is_float_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __produto_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `__profissao`
-- ----------------------------
DROP TABLE IF EXISTS `__profissao`;
CREATE TABLE `__profissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__profissao_FK_647949219` (`crud_mobile_id_INT`),
  CONSTRAINT `__profissao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __profissao
-- ----------------------------

-- ----------------------------
-- Table structure for `__recebivel`
-- ----------------------------
DROP TABLE IF EXISTS `__recebivel`;
CREATE TABLE `__recebivel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_FLOAT` double DEFAULT NULL,
  `devedor_empresa_id_INT` int(11) DEFAULT NULL,
  `devedor_pessoa_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `valor_pagamento_FLOAT` double DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `recebedor_usuario_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recebivel_FK_239410400` (`devedor_empresa_id_INT`),
  KEY `recebivel_FK_19348144` (`devedor_pessoa_id_INT`),
  KEY `recebivel_FK_829437256` (`cadastro_usuario_id_INT`),
  KEY `recebivel_FK_988525391` (`recebedor_usuario_id_INT`),
  KEY `recebivel_FK_1007080` (`relatorio_id_INT`),
  KEY `recebivel_FK_595886231` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __recebivel
-- ----------------------------

-- ----------------------------
-- Table structure for `__recebivel_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `__recebivel_cotidiano`;
CREATE TABLE `__recebivel_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recebedor_empresa_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `id_tipo_despesa_recebivel_INT` int(3) DEFAULT NULL,
  `parametro_INT` int(11) DEFAULT NULL,
  `parametro_OFFSEC` int(10) DEFAULT NULL,
  `parametro_SEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recebivel_cotidiano_FK_447082519` (`recebedor_empresa_id_INT`),
  KEY `recebivel_cotidiano_FK_452484131` (`cadastro_usuario_id_INT`),
  KEY `recebivel_cotidiano_FK_900634766` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __recebivel_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `__rede`
-- ----------------------------
DROP TABLE IF EXISTS `__rede`;
CREATE TABLE `__rede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__rede_FK_101135254` (`crud_mobile_id_INT`),
  CONSTRAINT `__rede_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __rede
-- ----------------------------

-- ----------------------------
-- Table structure for `__rede_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `__rede_empresa`;
CREATE TABLE `__rede_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rede_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__rede_empresa_FK_638031006` (`crud_mobile_id_INT`),
  CONSTRAINT `__rede_empresa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __rede_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `__registro`
-- ----------------------------
DROP TABLE IF EXISTS `__registro`;
CREATE TABLE `__registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocolo_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `id_origem_chamada_INT` int(3) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_FK_991607667` (`responsavel_usuario_id_INT`),
  KEY `registro_FK_883850098` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_FK_435150146` (`tipo_registro_id_INT`),
  KEY `registro_FK_886230469` (`registro_estado_id_INT`),
  KEY `registro_FK_225860595` (`registro_estado_corporacao_id_INT`),
  KEY `registro_FK_979919434` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __registro
-- ----------------------------

-- ----------------------------
-- Table structure for `__registro_estado`
-- ----------------------------
DROP TABLE IF EXISTS `__registro_estado`;
CREATE TABLE `__registro_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `prazo_dias_INT` int(3) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_estado_FK_27282714` (`responsavel_usuario_id_INT`),
  KEY `registro_estado_FK_131805420` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_estado_FK_634155274` (`tipo_registro_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __registro_estado
-- ----------------------------

-- ----------------------------
-- Table structure for `__registro_estado_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `__registro_estado_corporacao`;
CREATE TABLE `__registro_estado_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `prazo_dias_INT` int(3) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_estado_corporacao_FK_488037109` (`responsavel_usuario_id_INT`),
  KEY `registro_estado_corporacao_FK_697784424` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_estado_corporacao_FK_794616700` (`tipo_registro_id_INT`),
  KEY `registro_estado_corporacao_FK_896209717` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __registro_estado_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `__registro_fluxo_estado_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `__registro_fluxo_estado_corporacao`;
CREATE TABLE `__registro_fluxo_estado_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origem_registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `origem_registro_estado_id_INT` int(11) DEFAULT NULL,
  `destino_registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `destino_registro_estado_id_INT` int(11) DEFAULT NULL,
  `template_JSON` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_fluxo_estado_corporacao_FK_25970459` (`origem_registro_estado_corporacao_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_785644532` (`origem_registro_estado_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_761993408` (`destino_registro_estado_corporacao_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_918395997` (`destino_registro_estado_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_13153076` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __registro_fluxo_estado_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `__relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `__relatorio`;
CREATE TABLE `__relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `tipo_relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__relatorio_FK_253204345` (`crud_mobile_id_INT`),
  CONSTRAINT `__relatorio_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `__relatorio_anexo`
-- ----------------------------
DROP TABLE IF EXISTS `__relatorio_anexo`;
CREATE TABLE `__relatorio_anexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `arquivo` varchar(50) DEFAULT NULL,
  `tipo_anexo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__relatorio_anexo_FK_965667725` (`crud_mobile_id_INT`),
  CONSTRAINT `__relatorio_anexo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __relatorio_anexo
-- ----------------------------

-- ----------------------------
-- Table structure for `__rotina`
-- ----------------------------
DROP TABLE IF EXISTS `__rotina`;
CREATE TABLE `__rotina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ultima_dia_contagem_ciclo_DATE` date NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `ultima_dia_contagem_ciclo_data_SEC` int(10) DEFAULT NULL,
  `ultima_dia_contagem_ciclo_data_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__rotina_FK_29144287` (`crud_mobile_id_INT`),
  CONSTRAINT `__rotina_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __rotina
-- ----------------------------

-- ----------------------------
-- Table structure for `__servico`
-- ----------------------------
DROP TABLE IF EXISTS `__servico`;
CREATE TABLE `__servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__servico_FK_254608154` (`crud_mobile_id_INT`),
  CONSTRAINT `__servico_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __servico
-- ----------------------------

-- ----------------------------
-- Table structure for `__sexo`
-- ----------------------------
DROP TABLE IF EXISTS `__sexo`;
CREATE TABLE `__sexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sexo_FK_869232178` (`crud_mobile_id_INT`),
  CONSTRAINT `__sexo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sexo
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_atributo`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_atributo`;
CREATE TABLE `__sistema_atributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `tipo_sql` varchar(15) DEFAULT NULL,
  `tamanho_INT` int(11) DEFAULT NULL,
  `decimal_INT` int(11) DEFAULT NULL,
  `not_null_BOOLEAN` int(1) DEFAULT NULL,
  `primary_key_BOOLEAN` int(1) DEFAULT NULL,
  `auto_increment_BOOLEAN` int(1) DEFAULT NULL,
  `valor_default` varchar(100) DEFAULT NULL,
  `fk_sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `atributo_fk` varchar(100) DEFAULT NULL,
  `fk_nome` varchar(255) DEFAULT NULL,
  `update_tipo_fk` varchar(30) DEFAULT NULL,
  `delete_tipo_fk` varchar(30) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `unique_BOOLEAN` int(1) DEFAULT NULL,
  `unique_nome` varchar(255) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  `tipo_sql_ficticio` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_atributo
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_campo`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_campo`;
CREATE TABLE `__sistema_campo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(256) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `sistema_lista_id_INT` int(11) DEFAULT NULL,
  `sistema_tipo_campo_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_1034240` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__sistema_campo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_campo
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_campo_atributo`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_campo_atributo`;
CREATE TABLE `__sistema_campo_atributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_campo_id_INT` int(11) DEFAULT NULL,
  `sistema_atributo_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_8218384` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__sistema_campo_atributo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_campo_atributo
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_estado_lista`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_estado_lista`;
CREATE TABLE `__sistema_estado_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(215) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_4258728` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__sistema_estado_lista_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_estado_lista
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_lista`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_lista`;
CREATE TABLE `__sistema_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `sistema_estado_lista_id_INT` int(11) DEFAULT NULL,
  `ultima_alteracao_usuario_id_INT` int(11) DEFAULT NULL,
  `criador_usuario_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `ultima_alteracao_SEC` int(10) DEFAULT NULL,
  `ultima_alteracao_OFFSEC` int(6) DEFAULT NULL,
  `criacao_SEC` int(10) DEFAULT NULL,
  `criacao_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_6250000` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__sistema_lista_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_lista
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_parametro_global`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_parametro_global`;
CREATE TABLE `__sistema_parametro_global` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `valor_string` varchar(250) DEFAULT NULL,
  `valor_INT` int(11) DEFAULT NULL,
  `valor_DATETIME` datetime DEFAULT NULL,
  `valor_DATE` date DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `valor_BOOLEAN` int(1) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `valor_SEC` int(10) DEFAULT NULL,
  `valor_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sistema_parametro_global_FK_800781250` (`crud_mobile_id_INT`),
  CONSTRAINT `__sistema_parametro_global_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_parametro_global
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_parametro_global_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_parametro_global_corporacao`;
CREATE TABLE `__sistema_parametro_global_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `valor_string` varchar(250) DEFAULT NULL,
  `valor_INT` int(11) DEFAULT NULL,
  `valor_DATETIME` datetime DEFAULT NULL,
  `valor_SEC` int(10) DEFAULT NULL,
  `valor_OFFSEC` int(6) DEFAULT NULL,
  `valor_DATE` date DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `valor_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_425415` (`crud_mobile_id_INT`),
  CONSTRAINT `fk_crud_mobile_425415` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_parametro_global_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_projetos_versao`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_projetos_versao`;
CREATE TABLE `__sistema_projetos_versao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projetos_versao_INT` int(11) NOT NULL,
  `data_homologacao_DATETIME` datetime DEFAULT NULL,
  `data_producao_DATETIME` datetime DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sistema_projetos_versao_FK_701080322` (`crud_mobile_id_INT`),
  CONSTRAINT `__sistema_projetos_versao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_projetos_versao
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_registro_sincronizador`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_registro_sincronizador`;
CREATE TABLE `__sistema_registro_sincronizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `id_tabela_INT` int(11) NOT NULL,
  `tipo_operacao_banco_id_INT` int(2) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `is_from_android_BOOLEAN` int(1) NOT NULL,
  `imei` varchar(30) DEFAULT NULL,
  `sistema_produto_INT` int(11) DEFAULT NULL,
  `sistema_projetos_versao_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sistema_registro_sincronizador_FK_639343262` (`crud_mobile_id_INT`),
  CONSTRAINT `__sistema_registro_sincronizador_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_registro_sincronizador
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_sequencia`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_sequencia`;
CREATE TABLE `__sistema_sequencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabela` varchar(128) DEFAULT NULL,
  `id_ultimo_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_5128784` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__sistema_sequencia_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_sequencia
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_tabela`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_tabela`;
CREATE TABLE `__sistema_tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `data_modificacao_estrutura_DATETIME` date DEFAULT NULL,
  `frequencia_sincronizador_INT` int(2) DEFAULT NULL,
  `banco_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `banco_web_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_web_para_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_mobile_para_web_BOOLEAN` int(1) DEFAULT NULL,
  `tabela_sistema_BOOLEAN` int(1) DEFAULT NULL,
  `excluida_BOOLEAN` int(1) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `atributos_chave_unica` varchar(512) DEFAULT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sistema_tabela_FK_965759278` (`crud_mobile_id_INT`),
  CONSTRAINT `__sistema_tabela_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_tabela
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_tipo_campo`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_tipo_campo`;
CREATE TABLE `__sistema_tipo_campo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(256) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_5017395` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__sistema_tipo_campo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_tipo_campo
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_tipo_download_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_tipo_download_arquivo`;
CREATE TABLE `__sistema_tipo_download_arquivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `path` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sistema_tipo_download_arquivo_FK_617492676` (`crud_mobile_id_INT`),
  CONSTRAINT `__sistema_tipo_download_arquivo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of __sistema_tipo_download_arquivo
-- ----------------------------

-- ----------------------------
-- Table structure for `__sistema_tipo_operacao_banco`
-- ----------------------------
DROP TABLE IF EXISTS `__sistema_tipo_operacao_banco`;
CREATE TABLE `__sistema_tipo_operacao_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(10) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__sistema_tipo_operacao_banco_FK_252899170` (`crud_mobile_id_INT`),
  CONSTRAINT `__sistema_tipo_operacao_banco_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __sistema_tipo_operacao_banco
-- ----------------------------

-- ----------------------------
-- Table structure for `__tag`
-- ----------------------------
DROP TABLE IF EXISTS `__tag`;
CREATE TABLE `__tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_FK_198974609` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tag
-- ----------------------------

-- ----------------------------
-- Table structure for `__tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `__tarefa`;
CREATE TABLE `__tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `origem_pessoa_id_INT` int(11) DEFAULT NULL,
  `origem_empresa_id_INT` int(11) DEFAULT NULL,
  `origem_logradouro` varchar(255) DEFAULT NULL,
  `origem_numero` varchar(30) DEFAULT NULL,
  `origem_cidade_id_INT` int(11) DEFAULT NULL,
  `origem_latitude_INT` int(6) DEFAULT NULL,
  `origem_longitude_INT` int(6) DEFAULT NULL,
  `origem_latitude_real_INT` int(6) DEFAULT NULL,
  `origem_longitude_real_INT` int(6) DEFAULT NULL,
  `destino_pessoa_id_INT` int(11) DEFAULT NULL,
  `destino_empresa_id_INT` int(11) DEFAULT NULL,
  `destino_logradouro` varchar(255) DEFAULT NULL,
  `destino_numero` varchar(30) DEFAULT NULL,
  `destino_cidade_id_INT` int(11) DEFAULT NULL,
  `destino_latitude_INT` int(6) DEFAULT NULL,
  `destino_longitude_INT` int(6) DEFAULT NULL,
  `destino_latitude_real_INT` int(6) DEFAULT NULL,
  `destino_longitude_real_INT` int(6) DEFAULT NULL,
  `tempo_estimado_carro_INT` int(11) DEFAULT NULL,
  `tempo_estimado_a_pe_INT` int(11) DEFAULT NULL,
  `distancia_estimada_carro_INT` int(11) DEFAULT NULL,
  `distancia_estimada_a_pe_INT` int(11) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `titulo_normalizado` varchar(100) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `inicio_hora_programada_SEC` int(10) DEFAULT NULL,
  `inicio_hora_programada_OFFSEC` int(6) DEFAULT NULL,
  `data_exibir_SEC` int(10) DEFAULT NULL,
  `data_exibir_OFFSEC` int(6) DEFAULT NULL,
  `inicio_SEC` int(10) DEFAULT NULL,
  `inicio_OFFSEC` int(6) DEFAULT NULL,
  `fim_SEC` int(10) DEFAULT NULL,
  `fim_OFFSEC` int(6) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `empresa_atividade_id_INT` int(11) DEFAULT NULL,
  `tipo_tarefa_id_INT` int(4) DEFAULT NULL,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `id_prioridade_INT` int(4) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `percentual_completo_INT` int(2) DEFAULT NULL,
  `prazo_SEC` int(10) DEFAULT NULL,
  `prazo_OFFSEC` int(6) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `protocolo_empresa_venda_INT` bigint(20) DEFAULT NULL,
  `protocolo_empresa_atividade_venda_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__tarefa_FK_506591797` (`crud_mobile_id_INT`),
  CONSTRAINT `__tarefa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tarefa
-- ----------------------------

-- ----------------------------
-- Table structure for `__tarefa_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `__tarefa_cotidiano`;
CREATE TABLE `__tarefa_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `origem_pessoa_id_INT` int(11) DEFAULT NULL,
  `origem_empresa_id_INT` int(11) DEFAULT NULL,
  `origem_logradouro` varchar(255) DEFAULT NULL,
  `origem_numero` varchar(30) DEFAULT NULL,
  `origem_cidade_id_INT` int(11) DEFAULT NULL,
  `origem_latitude_INT` int(6) DEFAULT NULL,
  `origem_longitude_INT` int(6) DEFAULT NULL,
  `origem_latitude_real_INT` int(6) DEFAULT NULL,
  `origem_longitude_real_INT` int(6) DEFAULT NULL,
  `destino_pessoa_id_INT` int(11) DEFAULT NULL,
  `destino_empresa_id_INT` int(11) DEFAULT NULL,
  `destino_logradouro` varchar(255) DEFAULT NULL,
  `destino_numero` varchar(30) DEFAULT NULL,
  `destino_cidade_id_INT` int(11) DEFAULT NULL,
  `destino_latitude_INT` int(6) DEFAULT NULL,
  `destino_longitude_INT` int(6) DEFAULT NULL,
  `destino_latitude_real_INT` int(6) DEFAULT NULL,
  `destino_longitude_real_INT` int(6) DEFAULT NULL,
  `tempo_estimado_carro_INT` int(11) DEFAULT NULL,
  `tempo_estimado_a_pe_INT` int(11) DEFAULT NULL,
  `distancia_estimada_carro_INT` int(11) DEFAULT NULL,
  `distancia_estimada_a_pe_INT` int(11) DEFAULT NULL,
  `is_realizada_a_pe_BOOLEAN` int(1) DEFAULT NULL,
  `inicio_hora_programada_SEC` int(10) DEFAULT NULL,
  `inicio_hora_programada_OFFSEC` int(6) DEFAULT NULL,
  `inicio_SEC` int(10) DEFAULT NULL,
  `inicio_OFFSEC` int(6) DEFAULT NULL,
  `fim_SEC` int(10) DEFAULT NULL,
  `fim_OFFSEC` int(6) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `titulo_normalizado` varchar(100) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `id_estado_INT` int(4) DEFAULT NULL,
  `id_prioridade_INT` int(4) DEFAULT NULL,
  `parametro_SEC` int(10) DEFAULT NULL,
  `parametro_OFFSEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `id_tipo_cotidiano_INT` int(4) DEFAULT NULL,
  `prazo_SEC` int(10) DEFAULT NULL,
  `prazo_OFFSEC` int(6) DEFAULT NULL,
  `percentual_completo_INT` int(2) DEFAULT NULL,
  `ultima_geracao_SEC` int(10) DEFAULT NULL,
  `ultima_geracao_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_FK_490081787` (`origem_cidade_id_INT`),
  KEY `tarefa_FK_618682861` (`destino_cidade_id_INT`),
  KEY `tarefa_FK_90545654` (`corporacao_id_INT`),
  KEY `tabela_FK_100` (`usuario_id_INT`),
  KEY `tarefa_FK_529296875` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_FK_70098877` (`categoria_permissao_id_INT`),
  KEY `tarefa_FK_751892090` (`veiculo_id_INT`),
  KEY `tarefa_FK_718139649` (`origem_pessoa_id_INT`),
  KEY `tarefa_FK_466461182` (`origem_empresa_id_INT`),
  KEY `tarefa_FK_517883301` (`destino_pessoa_id_INT`),
  KEY `tarefa_FK_919769288` (`destino_empresa_id_INT`),
  KEY `tarefa_cotidiano_FK_718597412` (`empresa_equipe_id_INT`),
  KEY `tarefa_cotidiano_FK_316833496` (`atividade_id_INT`),
  KEY `tarefa_cotidiano_FK_442840576` (`relatorio_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tarefa_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `__tarefa_cotidiano_item`
-- ----------------------------
DROP TABLE IF EXISTS `__tarefa_cotidiano_item`;
CREATE TABLE `__tarefa_cotidiano_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `tarefa_cotidiano_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_cotidiano_item_FK_381195068` (`corporacao_id_INT`),
  KEY `tarefa_cotidiano_item_FK_630126953` (`tarefa_cotidiano_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tarefa_cotidiano_item
-- ----------------------------

-- ----------------------------
-- Table structure for `__tarefa_item`
-- ----------------------------
DROP TABLE IF EXISTS `__tarefa_item`;
CREATE TABLE `__tarefa_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `data_conclusao_SEC` int(10) DEFAULT NULL,
  `data_conclusao_OFFSEC` int(6) DEFAULT NULL,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `executada_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `latitude_real_INT` int(6) DEFAULT NULL,
  `longitude_real_INT` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_item_FK_979400635` (`tarefa_id_INT`),
  KEY `tarefa_item_FK_803283692` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_item_FK_903411866` (`executada_pelo_usuario_id_INT`),
  KEY `tarefa_item_FK_176757812` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tarefa_item
-- ----------------------------

-- ----------------------------
-- Table structure for `__tarefa_relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `__tarefa_relatorio`;
CREATE TABLE `__tarefa_relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_relatorio_FK_286987304` (`tarefa_id_INT`),
  KEY `tarefa_relatorio_FK_220855713` (`relatorio_id_INT`),
  KEY `tarefa_relatorio_FK_496887207` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tarefa_relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `__tarefa_tag`
-- ----------------------------
DROP TABLE IF EXISTS `__tarefa_tag`;
CREATE TABLE `__tarefa_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `tag_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_tag_FK_665161133` (`tarefa_id_INT`),
  KEY `tarefa_tag_FK_816802979` (`tag_id_INT`),
  KEY `tarefa_tag_FK_330139160` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tarefa_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `__tela`
-- ----------------------------
DROP TABLE IF EXISTS `__tela`;
CREATE TABLE `__tela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) DEFAULT NULL,
  `tela_estado_id_INT` int(11) DEFAULT NULL,
  `tela_tipo_id_INT` int(11) DEFAULT NULL,
  `is_android_BOOLEAN` int(1) DEFAULT NULL,
  `is_web_BOOLEAN` int(1) DEFAULT NULL,
  `configuracao_json` tinytext,
  `modificacao_usuario_id_INT` int(11) DEFAULT NULL,
  `cricacao_usuario_id_INT` int(11) DEFAULT NULL,
  `ativa_BOOLEAN` int(1) DEFAULT NULL,
  `permissao_id_INT` int(11) DEFAULT NULL,
  `area_menu_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_criacao_SEC` int(10) DEFAULT NULL,
  `data_criacao_OFFSEC` int(6) DEFAULT NULL,
  `data_modificacao_SEC` int(10) DEFAULT NULL,
  `data_modificacao_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_8007202` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tela_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tela
-- ----------------------------

-- ----------------------------
-- Table structure for `__tela_componente`
-- ----------------------------
DROP TABLE IF EXISTS `__tela_componente`;
CREATE TABLE `__tela_componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `configuracao_json` tinytext,
  `sistema_campo_id_INT` int(11) DEFAULT NULL,
  `tela_id_INT` int(11) DEFAULT NULL,
  `tela_sistema_lista_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `tela_tipo_componente_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_4364929` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tela_componente_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tela_componente
-- ----------------------------

-- ----------------------------
-- Table structure for `__tela_estado`
-- ----------------------------
DROP TABLE IF EXISTS `__tela_estado`;
CREATE TABLE `__tela_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_8450928` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tela_estado_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tela_estado
-- ----------------------------

-- ----------------------------
-- Table structure for `__tela_sistema_lista`
-- ----------------------------
DROP TABLE IF EXISTS `__tela_sistema_lista`;
CREATE TABLE `__tela_sistema_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tela_id_INT` int(11) DEFAULT NULL,
  `sistema_lista_id_INT` int(11) DEFAULT NULL,
  `is_principal_BOOLEAN` int(1) DEFAULT NULL,
  `tipo_cardinalidade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_3043518` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tela_sistema_lista_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tela_sistema_lista
-- ----------------------------

-- ----------------------------
-- Table structure for `__tela_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `__tela_tipo`;
CREATE TABLE `__tela_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_8479615` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tela_tipo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tela_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `__tela_tipo_componente`
-- ----------------------------
DROP TABLE IF EXISTS `__tela_tipo_componente`;
CREATE TABLE `__tela_tipo_componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_8982850` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tela_tipo_componente_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tela_tipo_componente
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_anexo`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_anexo`;
CREATE TABLE `__tipo_anexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__tipo_anexo_FK_90301513` (`crud_mobile_id_INT`),
  CONSTRAINT `__tipo_anexo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_anexo
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_canal_envio`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_canal_envio`;
CREATE TABLE `__tipo_canal_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_canal_envio
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_cardinalidade`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_cardinalidade`;
CREATE TABLE `__tipo_cardinalidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_2429199` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `__tipo_cardinalidade_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_cardinalidade
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_documento`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_documento`;
CREATE TABLE `__tipo_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__tipo_documento_FK_925750733` (`crud_mobile_id_INT`),
  CONSTRAINT `__tipo_documento_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_documento
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_empresa`;
CREATE TABLE `__tipo_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__tipo_empresa_FK_125244140` (`crud_mobile_id_INT`),
  CONSTRAINT `__tipo_empresa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_nota`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_nota`;
CREATE TABLE `__tipo_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_nota_FK_962524415` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_nota
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_ponto`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_ponto`;
CREATE TABLE `__tipo_ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__tipo_ponto_FK_360229492` (`crud_mobile_id_INT`),
  CONSTRAINT `__tipo_ponto_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_ponto
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_registro`;
CREATE TABLE `__tipo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_relatorio`;
CREATE TABLE `__tipo_relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_tarefa`;
CREATE TABLE `__tipo_tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_tarefa
-- ----------------------------

-- ----------------------------
-- Table structure for `__tipo_veiculo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `__tipo_veiculo_registro`;
CREATE TABLE `__tipo_veiculo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __tipo_veiculo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `__uf`
-- ----------------------------
DROP TABLE IF EXISTS `__uf`;
CREATE TABLE `__uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `nome_normalizado` varchar(255) DEFAULT NULL,
  `sigla` char(2) DEFAULT NULL,
  `pais_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__uf_FK_846313477` (`crud_mobile_id_INT`),
  CONSTRAINT `__uf_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __uf
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario`;
CREATE TABLE `__usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `status_BOOLEAN` int(1) DEFAULT NULL,
  `pagina_inicial` text,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_FK_144653320` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_categoria_permissao`;
CREATE TABLE `__usuario_categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_categoria_permissao_FK_833374024` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_categoria_permissao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_corporacao`;
CREATE TABLE `__usuario_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `is_adm_BOOLEAN` int(1) NOT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_corporacao_FK_524017334` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_corporacao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_empresa`;
CREATE TABLE `__usuario_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_empresa_FK_87646484` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_empresa_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_foto`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_foto`;
CREATE TABLE `__usuario_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_interna` varchar(50) DEFAULT NULL,
  `foto_externa` varchar(50) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_foto_FK_903350830` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_foto_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_foto
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_foto_configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_foto_configuracao`;
CREATE TABLE `__usuario_foto_configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_camera_interna_BOOLEAN` int(1) NOT NULL,
  `periodo_segundo_INT` int(7) NOT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_foto_configuracao_FK_661895752` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_foto_configuracao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_foto_configuracao
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_horario_trabalho`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_horario_trabalho`;
CREATE TABLE `__usuario_horario_trabalho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio_TIME` time DEFAULT NULL,
  `hora_fim_TIME` time DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `dia_semana_INT` int(2) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `template_JSON` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_hora478455` (`corporacao_id_INT`),
  KEY `usuario_horario_trabalho_FK_788391114` (`empresa_id_INT`),
  KEY `usuario_horario_trabalho_FK_905120850` (`usuario_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_horario_trabalho
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_menu`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_menu`;
CREATE TABLE `__usuario_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `area_menu` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_menu_FK_951354981` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_menu_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_posicao`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_posicao`;
CREATE TABLE `__usuario_posicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(11) NOT NULL,
  `longitude_INT` int(11) NOT NULL,
  `velocidade_INT` int(5) DEFAULT NULL,
  `foto_interna` varchar(30) DEFAULT NULL,
  `foto_externa` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `fonte_informacao_INT` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_posicao_FK_55206298` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_posicao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_posicao
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_privilegio`;
CREATE TABLE `__usuario_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_privilegio_FK_72753906` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_privilegio_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_servico`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_servico`;
CREATE TABLE `__usuario_servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_BOOLEAN` int(1) NOT NULL,
  `servico_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_servico_FK_753601074` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_servico_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_servico
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_tipo`;
CREATE TABLE `__usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_visivel` varchar(100) DEFAULT NULL,
  `status_BOOLEAN` int(1) DEFAULT NULL,
  `pagina_inicial` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_tipo_FK_730682373` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_tipo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_tipo_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_tipo_corporacao`;
CREATE TABLE `__usuario_tipo_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_tipo_corporacao_FK_966369629` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_tipo_corporacao_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_tipo_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_tipo_menu`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_tipo_menu`;
CREATE TABLE `__usuario_tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `area_menu` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `area_menu_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_tipo_menu_FK_25787353` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_tipo_menu_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_tipo_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `__usuario_tipo_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `__usuario_tipo_privilegio`;
CREATE TABLE `__usuario_tipo_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__usuario_tipo_privilegio_FK_179779052` (`crud_mobile_id_INT`),
  CONSTRAINT `__usuario_tipo_privilegio_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __usuario_tipo_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `__veiculo`
-- ----------------------------
DROP TABLE IF EXISTS `__veiculo`;
CREATE TABLE `__veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placa` varchar(20) NOT NULL,
  `modelo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__veiculo_FK_581451416` (`crud_mobile_id_INT`),
  CONSTRAINT `__veiculo_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __veiculo
-- ----------------------------

-- ----------------------------
-- Table structure for `__veiculo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `__veiculo_registro`;
CREATE TABLE `__veiculo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entrada_BOOLEAN` int(1) DEFAULT NULL,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `quilometragem_INT` int(11) DEFAULT NULL,
  `observacao` varchar(512) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `tipo_veiculo_registro_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crud_mobile_id_INT` (`crud_mobile_id_INT`),
  CONSTRAINT `__veiculo_registro_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __veiculo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `__veiculo_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `__veiculo_usuario`;
CREATE TABLE `__veiculo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `is_ativo_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `__veiculo_usuario_FK_425506592` (`crud_mobile_id_INT`),
  CONSTRAINT `__veiculo_usuario_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __veiculo_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `__versao`
-- ----------------------------
DROP TABLE IF EXISTS `__versao`;
CREATE TABLE `__versao` (
  `id` int(11) NOT NULL,
  `versao_INT` int(5) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crud_mobile_2422790` (`crud_mobile_id_INT`) USING BTREE,
  CONSTRAINT `fk_crud_mobile_8612366` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __versao
-- ----------------------------

-- ----------------------------
-- Table structure for `__wifi`
-- ----------------------------
DROP TABLE IF EXISTS `__wifi`;
CREATE TABLE `__wifi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) DEFAULT NULL,
  `rssi` int(11) DEFAULT NULL,
  `ssid` varchar(30) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `senha` varchar(30) DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  `link_speed` int(11) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `bssid` varchar(30) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `contador_verificacao_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crud_mobile_id_INT` (`crud_mobile_id_INT`),
  CONSTRAINT `__wifi_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __wifi
-- ----------------------------

-- ----------------------------
-- Table structure for `__wifi_registro`
-- ----------------------------
DROP TABLE IF EXISTS `__wifi_registro`;
CREATE TABLE `__wifi_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wifi_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `crud_mobile_id_INT` int(11) DEFAULT NULL,
  `data_inicio_SEC` int(10) DEFAULT NULL,
  `data_inicio_OFFSEC` int(6) DEFAULT NULL,
  `data_fim_SEC` int(10) DEFAULT NULL,
  `data_fim_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crud_mobile_id_INT` (`crud_mobile_id_INT`),
  CONSTRAINT `__wifi_registro_ibfk_1` FOREIGN KEY (`crud_mobile_id_INT`) REFERENCES `crud_mobile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __wifi_registro
-- ----------------------------
