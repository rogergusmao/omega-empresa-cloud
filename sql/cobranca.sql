/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : cobranca

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-09-03 15:59:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acesso`
-- ----------------------------
DROP TABLE IF EXISTS `acesso`;
CREATE TABLE `acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `data_login_DATETIME` datetime NOT NULL,
  `data_logout_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of acesso
-- ----------------------------

-- ----------------------------
-- Table structure for `acesso_cliente`
-- ----------------------------
DROP TABLE IF EXISTS `acesso_cliente`;
CREATE TABLE `acesso_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id_INT` int(11) NOT NULL,
  `data_login_DATETIME` datetime NOT NULL,
  `data_logout_DATETIME` datetime DEFAULT NULL,
  `is_abertura_email_BOOLEAN` int(1) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acesso_cliente_FK_981201172` (`cliente_id_INT`) USING BTREE,
  CONSTRAINT `acesso_cliente_ibfk_1` FOREIGN KEY (`cliente_id_INT`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of acesso_cliente
-- ----------------------------

-- ----------------------------
-- Table structure for `cliente`
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(255) DEFAULT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `cnpj` varchar(40) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cpf` varchar(40) DEFAULT NULL,
  `tipo_pessoa_id_INT` int(11) DEFAULT NULL,
  `telefone_fixo_1` varchar(30) DEFAULT NULL,
  `telefone_fixo_2` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `email_cobranca1` varchar(100) DEFAULT NULL,
  `email_cobranca2` varchar(100) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `dia_mes_vencimento_INT` int(5) DEFAULT NULL,
  `endereco_logradouro` varchar(255) DEFAULT NULL,
  `endereco_numero` varchar(40) DEFAULT NULL,
  `endereco_complemento` varchar(40) DEFAULT NULL,
  `endereco_cep` varchar(40) DEFAULT NULL,
  `endereco_bairro` varchar(100) DEFAULT NULL,
  `endereco_cidade` varchar(40) DEFAULT NULL,
  `uf_id_INT` int(11) DEFAULT NULL,
  `data_validacao_email_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cliente
-- ----------------------------

-- ----------------------------
-- Table structure for `cliente_assinatura`
-- ----------------------------
DROP TABLE IF EXISTS `cliente_assinatura`;
CREATE TABLE `cliente_assinatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `sihop_assinatura_INT` int(11) DEFAULT NULL,
  `sistema_pacote_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL COMMENT 'Preenchida com o conteudo descricao do sistema_pacote na epoca da assinatura',
  `data_contratacao_DATE` date DEFAULT NULL,
  `estado_assinatura_id_INT` int(11) DEFAULT NULL,
  `data_cancelamento_DATETIME` datetime DEFAULT NULL,
  `motivo_cancelamento` char(255) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `data_contratacao_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_id_INT` (`cliente_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `sistema_pacote_id_INT` (`sistema_pacote_id_INT`),
  CONSTRAINT `cliente_assinatura_ibfk_1` FOREIGN KEY (`cliente_id_INT`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cliente_assinatura_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cliente_assinatura_ibfk_3` FOREIGN KEY (`sistema_pacote_id_INT`) REFERENCES `sistema_pacote` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1098 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cliente_assinatura
-- ----------------------------

-- ----------------------------
-- Table structure for `cliente_assinatura_cliente`
-- ----------------------------
DROP TABLE IF EXISTS `cliente_assinatura_cliente`;
CREATE TABLE `cliente_assinatura_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id_INT` int(11) NOT NULL,
  `cliente_assinatura_id_INT` int(11) NOT NULL,
  `data_envio_sistema_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_id_INT` (`cliente_id_INT`),
  KEY `cliente_assinatura_id_INT` (`cliente_assinatura_id_INT`),
  CONSTRAINT `cliente_assinatura_cliente_ibfk_1` FOREIGN KEY (`cliente_id_INT`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cliente_assinatura_cliente_ibfk_2` FOREIGN KEY (`cliente_assinatura_id_INT`) REFERENCES `cliente_assinatura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cliente_assinatura_cliente
-- ----------------------------

-- ----------------------------
-- Table structure for `cobranca`
-- ----------------------------
DROP TABLE IF EXISTS `cobranca`;
CREATE TABLE `cobranca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id_INT` int(11) DEFAULT NULL,
  `data_vencimento_DATE` date NOT NULL,
  `valor_FLOAT` float(11,2) DEFAULT NULL,
  `status_pagamento_id_INT` int(11) NOT NULL,
  `mes_ano_referencia` varchar(10) DEFAULT NULL,
  `id_transacao_pag_seguro` varchar(100) DEFAULT NULL,
  `status_pag_seguro` varchar(100) DEFAULT NULL,
  `tipo_pagamento_pag_seguro` varchar(100) DEFAULT NULL,
  `parcelas_pag_seguro` varchar(10) DEFAULT NULL,
  `dados_retorno_pag_seguro` mediumtext,
  `datahora_cadastro_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_id_INT` (`cliente_id_INT`),
  CONSTRAINT `cobranca_ibfk_1` FOREIGN KEY (`cliente_id_INT`) REFERENCES `cliente` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cobranca
-- ----------------------------

-- ----------------------------
-- Table structure for `cobranca_cliente_assinatura`
-- ----------------------------
DROP TABLE IF EXISTS `cobranca_cliente_assinatura`;
CREATE TABLE `cobranca_cliente_assinatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cobranca_id_INT` int(11) NOT NULL,
  `cliente_assinatura_id_INT` int(11) NOT NULL,
  `valor_FLOAT` float(11,2) DEFAULT NULL,
  `dias_periodo_cobranca_INT` int(11) NOT NULL,
  `data_inicial_DATE` date DEFAULT NULL,
  `data_final_DATE` date DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cobranca_id_INT` (`cobranca_id_INT`),
  CONSTRAINT `cobranca_cliente_assinatura_ibfk_1` FOREIGN KEY (`cobranca_id_INT`) REFERENCES `cobranca` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cobranca_cliente_assinatura
-- ----------------------------

-- ----------------------------
-- Table structure for `cobranca_mensagem`
-- ----------------------------
DROP TABLE IF EXISTS `cobranca_mensagem`;
CREATE TABLE `cobranca_mensagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cobranca_id_INT` int(11) NOT NULL,
  `datahora_cadastro_DATETIME` datetime NOT NULL,
  `datahora_agendamento_envio_DATETIME` datetime NOT NULL,
  `mensagem_enviada_BOOLEAN` int(1) DEFAULT NULL,
  `datahora_envio_mensagem_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cobranca_id_INT` (`cobranca_id_INT`),
  CONSTRAINT `cobranca_mensagem_ibfk_1` FOREIGN KEY (`cobranca_id_INT`) REFERENCES `cobranca` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cobranca_mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for `configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `configuracao`;
CREATE TABLE `configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_projeto` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configuracao
-- ----------------------------
INSERT INTO `configuracao` VALUES ('1', 'Modelo de Projetos', '0', null);

-- ----------------------------
-- Table structure for `configuracao_email`
-- ----------------------------
DROP TABLE IF EXISTS `configuracao_email`;
CREATE TABLE `configuracao_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_remetente` varchar(255) NOT NULL,
  `endereco_remetente` varchar(255) NOT NULL,
  `return_path` varchar(255) NOT NULL,
  `usuario_return_path` varchar(255) NOT NULL,
  `senha_return_path` varchar(255) NOT NULL,
  `numero_de_emails_por_vez_INT` int(11) NOT NULL,
  `intervalo_turno_em_segundos_INT` int(11) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configuracao_email
-- ----------------------------
INSERT INTO `configuracao_email` VALUES ('1', 'Omega Software', 'financeiro@omegasoftware.com.br', 'retorno@omegasoftware.com.br', 'retorno@omegasoftware.com.br', 'omega24511102', '20', '10', '0', null);

-- ----------------------------
-- Table structure for `corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `corporacao`;
CREATE TABLE `corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `identificador` varchar(100) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT NULL,
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=982 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa`
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cnpj` varchar(30) DEFAULT NULL,
  `endereco` varchar(300) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa
-- ----------------------------
INSERT INTO `empresa` VALUES ('1', 'Omega Software', '000.000.000.00', 'Av. Bias Fortes, 147, sala 402', '(31) 9297 - 5220', 'vendas@omegasfotware.com.br', '0', null);

-- ----------------------------
-- Table structure for `empresa_hosting`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_hosting`;
CREATE TABLE `empresa_hosting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `endereco_painel_controle` varchar(255) DEFAULT NULL,
  `endereco_cobranca` varchar(255) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_hosting
-- ----------------------------
INSERT INTO `empresa_hosting` VALUES ('1', 'Hostdime', 'https://187.45.181.206:2087/', 'https://core.hostdime.com.br/', '0', null);
INSERT INTO `empresa_hosting` VALUES ('2', 'Locaweb', '', 'http://painel.locaweb.com.br', '0', null);

-- ----------------------------
-- Table structure for `estado_assinatura`
-- ----------------------------
DROP TABLE IF EXISTS `estado_assinatura`;
CREATE TABLE `estado_assinatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_assinatura
-- ----------------------------
INSERT INTO `estado_assinatura` VALUES ('1', 'ATIVO', 'Pacote atualmente ativo que não é gratuito', '0', null);
INSERT INTO `estado_assinatura` VALUES ('2', 'CANCELADO PELO USUÁRIO', 'Pacote cancelado pelo usuário', '0', null);
INSERT INTO `estado_assinatura` VALUES ('3', 'CANCELADO PELO SISTEMA', 'Pacote cancelado pelo sistema', '0', null);
INSERT INTO `estado_assinatura` VALUES ('4', 'ASSINATURA RESERVADA', 'Assinatura reservada para o proximo cliente', '0', null);

-- ----------------------------
-- Table structure for `operacao_sistema`
-- ----------------------------
DROP TABLE IF EXISTS `operacao_sistema`;
CREATE TABLE `operacao_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `tipo_operacao` varchar(50) DEFAULT NULL,
  `pagina_operacao` varchar(255) DEFAULT NULL,
  `entidade_operacao` varchar(50) DEFAULT NULL,
  `chave_registro_operacao_INT` int(11) DEFAULT NULL,
  `descricao_operacao` varchar(255) DEFAULT NULL,
  `data_operacao_DATETIME` datetime NOT NULL,
  `url_completa` varchar(1000) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario_id_INT`),
  CONSTRAINT `operacao_sistema_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operacao_sistema
-- ----------------------------

-- ----------------------------
-- Table structure for `servico_modalidade`
-- ----------------------------
DROP TABLE IF EXISTS `servico_modalidade`;
CREATE TABLE `servico_modalidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of servico_modalidade
-- ----------------------------
INSERT INTO `servico_modalidade` VALUES ('1', 'Mensal', '0', null);
INSERT INTO `servico_modalidade` VALUES ('2', 'Único', '0', null);
INSERT INTO `servico_modalidade` VALUES ('3', 'Gratuito', '0', null);

-- ----------------------------
-- Table structure for `sistema`
-- ----------------------------
DROP TABLE IF EXISTS `sistema`;
CREATE TABLE `sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `sicvp_sistema_INT` int(11) NOT NULL,
  `id_sistema_sihop_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema
-- ----------------------------
INSERT INTO `sistema` VALUES ('1', 'Ponto Eletrônico ', '1', '1', '0', null);
INSERT INTO `sistema` VALUES ('3', 'Omega Empresa Corporacao', '1', '3', '0', null);

-- ----------------------------
-- Table structure for `sistema_pacote`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_pacote`;
CREATE TABLE `sistema_pacote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `data_cadastro_DATE` date NOT NULL,
  `servico_modalidade_id_INT` int(11) NOT NULL,
  `valor_FLOAT` float(11,2) NOT NULL,
  `is_ativo_BOOLEAN` int(11) DEFAULT NULL,
  `sistema_id_INT` int(11) DEFAULT NULL,
  `numero_usuario_INT` int(11) DEFAULT NULL,
  `quantidade_espaco_em_mega_INT` int(11) DEFAULT NULL,
  `numero_clinica_INT` int(11) DEFAULT NULL,
  `total_email_mensal_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_id_INT` (`sistema_id_INT`),
  KEY `servico_modalidade_id_INT` (`servico_modalidade_id_INT`),
  CONSTRAINT `sistema_pacote_ibfk_1` FOREIGN KEY (`sistema_id_INT`) REFERENCES `sistema` (`id`),
  CONSTRAINT `sistema_pacote_ibfk_2` FOREIGN KEY (`servico_modalidade_id_INT`) REFERENCES `servico_modalidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_pacote
-- ----------------------------
INSERT INTO `sistema_pacote` VALUES ('1', 'Hospedagem Allmax Informática e Only Imports', '2012-08-15', '1', '60.00', '1', null, null, null, null, null, '1', '2012-10-08 17:07:13');
INSERT INTO `sistema_pacote` VALUES ('2', 'Sistema de Email Marketing', '2012-08-15', '1', '200.00', '1', null, null, null, null, null, '1', '2012-10-08 17:07:26');
INSERT INTO `sistema_pacote` VALUES ('3', 'Hospedagem do Site', '2012-08-13', '1', '49.90', '1', null, null, null, null, null, '1', '2012-10-08 17:06:48');
INSERT INTO `sistema_pacote` VALUES ('4', 'Hospedagem mensal do portal da FETRAFI-MG', '2013-07-14', '1', '39.90', '0', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('5', 'Hospedagem do sistema de gerenciamento do consultório (rubenscmribeiro.med.br)', '2012-09-14', '1', '70.00', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('6', 'Hospedagem do domínio allmaxinformatica.com.br', '2013-05-14', '1', '40.00', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('7', 'Hospedagem e Licença de uso do Sistema de Email Marketing NYX', '2013-10-14', '1', '150.00', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('8', 'Hospedagem do sistema de questionários', '2012-09-14', '1', '29.90', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('9', 'Hospedagem do sistema de gerênciamento da Clínica da Estética Beleza em Movimento', '2012-09-14', '1', '60.00', '1', null, null, null, null, null, '1', '2012-10-30 17:31:28');
INSERT INTO `sistema_pacote` VALUES ('10', 'Serviço de Teste', '2012-09-14', '1', '3.00', '1', null, null, null, null, null, '1', '2012-11-12 06:16:58');
INSERT INTO `sistema_pacote` VALUES ('11', 'Licença de Uso do Sistema para Clínicas de Estética - Marca: Pelo e Corpo', '2012-10-17', '1', '150.00', '1', null, null, null, null, null, '1', '2013-06-05 14:12:40');
INSERT INTO `sistema_pacote` VALUES ('12', 'Licença de Uso do Software de Gerenciamento de Clínicas de Estética. Marca: Pelo e Corpo', '2012-12-01', '2', '150.00', '1', null, null, null, null, null, '1', '2013-06-05 14:49:58');
INSERT INTO `sistema_pacote` VALUES ('13', 'Licença de Uso do Software de Gerenciamento de Clínicas de Estética. Marca: Studio Bellye', '2012-12-01', '2', '200.00', '1', null, null, null, null, null, '1', '2013-06-05 14:50:03');
INSERT INTO `sistema_pacote` VALUES ('14', 'Licença de Uso do Software de Gerenciamento de Clínicas de Estética. Marca: Dermobelle', '2012-12-01', '2', '200.00', '1', null, null, null, null, null, '1', '2013-06-05 14:49:54');
INSERT INTO `sistema_pacote` VALUES ('15', 'Hospedagem do sistema de gerenciamento do consultório (rubenscmribeiro.med.br) - Ref. 05/2013', '2013-05-01', '2', '70.00', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('16', 'Hospedagem do domínio onlyimports.com.br', '2013-05-14', '1', '20.00', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('17', 'Hospedagem e suporte - Portal dos Antigos Alunos do Colégio Loyola', '2013-06-14', '1', '39.90', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('18', 'Hospedagem do Portal dos Antigos Alunos do Colégio Loyola, Ref. meses 04/05/06 de 2013', '2013-04-01', '2', '119.70', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('19', 'Hospedagem do domínio MX Power - mxpower.com.br', '2013-05-14', '1', '20.00', '1', null, null, null, null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('20', 'Omega Empresa Bronze', '2013-05-14', '1', '100.00', '1', '1', '5', '2000', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('21', 'Omega Empresa Prata', '2013-05-14', '1', '150.00', '1', '1', '10', '4000', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('22', 'Omega Empresa Ouro', '2013-05-14', '1', '200.00', '1', '1', '20', '8000', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('23', 'Omega Empresa Gratuito', '2013-05-14', '3', '0.00', '1', '1', '3', '500', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('28', 'Pacote Omega Empresa sob medida. 5 usuários e 650 MB de espaço disponível.', '2014-04-28', '1', '75.00', null, '1', '5', '650', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('31', 'Pacote Omega Empresa sob medida. 4 usuários e 570 MB de espaço disponível.', '2014-05-01', '1', '60.00', null, '1', '4', '570', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('32', 'Pacote Omega Empresa sob medida. 100 usuários e 4250.', '2014-05-03', '1', '1000.00', '1', '1', '100', '250', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('33', 'Pacote Omega Empresa sob medida. 100 usuários e 4.250 MB de espaço disponível.', '2014-05-03', '1', '1000.00', '1', '1', '100', '4', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('34', 'Pacote Omega Empresa sob medida. 100 usuários e 4.250 MB de espaço disponível.', '2014-05-03', '1', '1000.00', '1', '1', '100', '4', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('35', 'Pacote Omega Empresa sob medida. 100 usuários e 4.250 MB de espaço disponível.', '2014-05-03', '1', '1000.00', '1', '1', '100', '4', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('36', 'Pacote Omega Empresa sob medida. 100 usuários e 4.250 MB de espaço disponível.', '2014-05-03', '1', '1000.00', '1', '1', '100', '4', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('37', 'Pacote Omega Empresa sob medida. 13 usuários e 1.290 MB de espaço disponível.', '2014-05-03', '1', '195.00', '1', '1', '13', '1', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('38', 'Pacote Omega Empresa sob medida. 49 usuários e 2.210 MB de espaço disponível.', '2014-05-03', '1', '490.00', '1', '1', '49', '2', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('39', 'Pacote Omega Empresa sob medida. 29 usuários e 1.990 MB de espaço disponível.', '2014-05-03', '1', '391.50', '1', '1', '29', '1990', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('40', 'Pacote Omega Empresa sob medida. 6 usuários e 730 MB de espaço disponível.', '2014-09-08', '1', '90.00', '1', '1', '6', '730', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('41', 'Pacote Omega Empresa sob medida. 23 usuários e 1.630 MB de espaço disponível.', '2014-09-08', '1', '310.50', '1', '1', '23', '1630', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('42', 'Pacote Omega Empresa sob medida. 7 usuários e 810 MB de espaço disponível.', '2014-09-16', '1', '105.00', '1', '1', '7', '810', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('43', 'Pacote Omega Empresa sob medida. 17 usuários e 1.610 MB de espaço disponível.', '2014-09-29', '1', '255.00', '1', '1', '17', '1610', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('44', 'Pacote Omega Empresa sob medida. 8 usuários e 890 MB de espaço disponível.', '2014-10-24', '1', '120.00', '1', '1', '8', '890', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('45', 'Pacote Omega Empresa sob medida. 31 usuários e 2.110 MB de espaço disponível.', '2014-10-26', '1', '418.50', '1', '1', '31', '2110', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('46', 'Pacote Omega Empresa sob medida. 9 usuários e 970 MB de espaço disponível.', '2014-12-05', '1', '135.00', '1', '1', '9', '970', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('47', 'Pacote Omega Empresa sob medida. 15 usuários e 1.450 MB de espaço disponível.', '2015-07-19', '1', '225.00', '1', '1', '15', '1450', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('48', 'Pacote Omega Empresa sob medida. 14 usuários e 1.370 MB de espaço disponível.', '2015-07-21', '1', '210.00', '1', '1', '14', '1370', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('49', 'Omega Empresa Corporação Gratuito', '2017-02-12', '3', '0.00', '1', '3', '3', '500', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('50', 'Pacote Omega Empresa sob medida. 12 usuários e 1.210 MB de espaço disponível.', '2017-04-22', '1', '180.00', '1', '1', '12', '1210', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('51', 'Pacote Omega Empresa sob medida. 10 usuários e 1.050 MB de espaço disponível.', '2017-06-29', '1', '150.00', '1', '1', '10', '1050', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('52', 'Pacote Omega Empresa sob medida. 10 usuários e 1.050 MB de espaço disponível.', '2017-06-29', '1', '150.00', '1', '1', '10', '1050', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('53', 'Pacote Reservado Para o Sistema Omega Empresa', '2017-08-04', '1', '0.00', '1', '1', '-1', '-1', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('54', 'Pacote Reservado Para o Sistema Omega Empresa Corporacao', '2017-08-05', '1', '0.00', '1', '3', '-1', '-1', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('55', 'Pacote Omega Empresa sob medida. 16 usuários e 1.530 MB de espaço disponível.', '2017-08-05', '1', '240.00', '1', '1', '16', '1530', null, null, '0', null);
INSERT INTO `sistema_pacote` VALUES ('56', 'Pacote Omega Empresa sob medida. 16 usuários e 1.530 MB de espaço disponível.', '2017-08-05', '1', '240.00', '1', '1', '16', '1530', null, null, '0', null);

-- ----------------------------
-- Table structure for `status_pagamento`
-- ----------------------------
DROP TABLE IF EXISTS `status_pagamento`;
CREATE TABLE `status_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `numero_ordenacao_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of status_pagamento
-- ----------------------------
INSERT INTO `status_pagamento` VALUES ('1', 'Aguardando Pagamento', '1', '0', null);
INSERT INTO `status_pagamento` VALUES ('2', 'Em Análise', '2', '0', null);
INSERT INTO `status_pagamento` VALUES ('3', 'Pagamento Confirmado', '3', '0', null);
INSERT INTO `status_pagamento` VALUES ('4', 'Em Disputa', '4', '0', null);
INSERT INTO `status_pagamento` VALUES ('5', 'Devolvido para o Comprador', '5', '0', null);
INSERT INTO `status_pagamento` VALUES ('6', 'Cancelado', '6', '0', null);

-- ----------------------------
-- Table structure for `tipo_pessoa`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_pessoa`;
CREATE TABLE `tipo_pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_pessoa
-- ----------------------------
INSERT INTO `tipo_pessoa` VALUES ('1', 'Pessoa Física', '0', null);
INSERT INTO `tipo_pessoa` VALUES ('2', 'Pessoa Jurídica', '0', null);

-- ----------------------------
-- Table structure for `uf`
-- ----------------------------
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `sigla` char(2) NOT NULL,
  `dataCadastro` datetime DEFAULT NULL,
  `dataEdicao` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sigla` (`sigla`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uf
-- ----------------------------
INSERT INTO `uf` VALUES ('1', 'São Paulo', 'SP', '2009-10-18 16:05:17', '2009-10-18 16:05:17', '0', null);
INSERT INTO `uf` VALUES ('2', 'Minas Gerais', 'MG', '2009-10-18 16:08:39', '2009-10-18 16:08:39', '0', null);
INSERT INTO `uf` VALUES ('3', 'Pará', 'PA', '2009-10-23 11:20:19', '2009-10-23 11:20:19', '0', null);
INSERT INTO `uf` VALUES ('4', 'Espirito Santo', 'ES', '2009-11-04 19:22:18', '2009-11-04 19:22:18', '0', null);
INSERT INTO `uf` VALUES ('5', 'Rio de Janeiro', 'RJ', '2010-01-01 12:03:54', '2010-01-01 12:03:54', '0', null);
INSERT INTO `uf` VALUES ('6', 'Rio Grande do Sul', 'RS', '2010-01-01 12:04:12', '2010-01-01 12:04:12', '0', null);
INSERT INTO `uf` VALUES ('7', 'Bahia', 'BA', '2010-01-01 12:04:23', '2010-01-01 12:04:23', '0', null);
INSERT INTO `uf` VALUES ('8', 'Tocantins', 'TO', '2010-01-01 12:04:38', '2010-01-01 12:04:38', '0', null);
INSERT INTO `uf` VALUES ('9', 'Maranhão', 'MA', '2010-01-01 12:06:00', '2010-01-01 12:06:00', '0', null);
INSERT INTO `uf` VALUES ('10', 'Acre', 'AC', '2010-01-01 12:06:16', '2010-01-01 12:06:16', '0', null);
INSERT INTO `uf` VALUES ('11', 'Alagoas', 'AL', '2010-01-01 12:06:31', '2010-01-01 12:06:31', '0', null);
INSERT INTO `uf` VALUES ('12', 'Amapá', 'AP', '2010-01-01 12:06:50', '2010-01-01 12:06:50', '0', null);
INSERT INTO `uf` VALUES ('13', 'Amazonas', 'AM', '2010-01-01 12:07:01', '2010-01-01 12:07:01', '0', null);
INSERT INTO `uf` VALUES ('14', 'Ceará', 'CE', '2010-01-01 12:07:21', '2010-01-01 12:07:21', '0', null);
INSERT INTO `uf` VALUES ('15', 'Distrito Federal', 'DF', '2010-01-01 12:07:40', '2010-01-01 12:07:40', '0', null);
INSERT INTO `uf` VALUES ('16', 'Goiás', 'GO', '2010-01-01 12:08:08', '2010-01-01 12:08:08', '0', null);
INSERT INTO `uf` VALUES ('17', 'Mato Grosso', 'MT', '2010-01-01 12:08:30', '2010-01-01 12:08:30', '0', null);
INSERT INTO `uf` VALUES ('18', 'Mato Grosso do Sul', 'MS', '2010-01-01 12:08:54', '2010-01-01 12:08:54', '0', null);
INSERT INTO `uf` VALUES ('19', 'Paraíba', 'PB', '2010-01-01 12:09:15', '2010-01-01 12:09:15', '0', null);
INSERT INTO `uf` VALUES ('20', 'Paraná', 'PR', '2010-01-01 12:09:43', '2010-01-01 12:09:43', '0', null);
INSERT INTO `uf` VALUES ('21', 'Pernambuco', 'PE', '2010-01-01 12:10:09', '2010-01-01 12:10:09', '0', null);
INSERT INTO `uf` VALUES ('22', 'Piauí', 'PI', '2010-01-01 12:10:36', '2010-01-01 12:10:36', '0', null);
INSERT INTO `uf` VALUES ('23', 'Rio Grande do Norte', 'RN', '2010-01-01 12:10:55', '2010-01-01 12:10:55', '0', null);
INSERT INTO `uf` VALUES ('24', 'Rondônia', 'RO', '2010-01-01 12:11:15', '2010-01-01 12:11:15', '0', null);
INSERT INTO `uf` VALUES ('25', 'Roraima', 'RR', '2010-01-01 12:11:45', '2010-01-01 12:11:45', '0', null);
INSERT INTO `uf` VALUES ('26', 'Santa Catarina', 'SC', '2010-01-01 12:12:03', '2010-01-01 12:12:03', '0', null);
INSERT INTO `uf` VALUES ('27', 'Sergipe', 'SE', '2010-01-01 12:12:20', '2010-01-01 12:12:20', '0', null);

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `pagina_inicial` text,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_usuario_usuario` (`usuario_tipo_id_INT`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Desenvolvimento', 'eduardo@omegasoftware.com.br', '8ffbcb8b27121de4a439478002b454cd', '1', '1', null, '0', null);
INSERT INTO `usuario` VALUES ('2', 'Roger', 'roger@omegasoftware.com.br', '18563035b358c4f559f4bbc16004bf79', '1', '1', '', '0', null);

-- ----------------------------
-- Table structure for `usuario_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_menu`;
CREATE TABLE `usuario_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_menu
-- ----------------------------
INSERT INTO `usuario_menu` VALUES ('1', '21', 'Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('2', '21', 'Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('3', '21', 'Gráfico de Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('4', '21', 'Organogramas', '0', null);
INSERT INTO `usuario_menu` VALUES ('5', '21', 'Organograma de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('6', '21', 'Organograma para Impressão', '0', null);
INSERT INTO `usuario_menu` VALUES ('7', '21', 'Alterar Estrutura Organizacional', '0', null);
INSERT INTO `usuario_menu` VALUES ('8', '21', 'Visualizar Pendências', '0', null);
INSERT INTO `usuario_menu` VALUES ('9', '21', 'Planejamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('10', '21', 'Cadastrar Milestone', '0', null);
INSERT INTO `usuario_menu` VALUES ('11', '21', 'Gerenciar Milestones', '0', null);
INSERT INTO `usuario_menu` VALUES ('12', '21', 'Associar Milestones a Vagas', '0', null);
INSERT INTO `usuario_menu` VALUES ('13', '21', 'Seleção', '0', null);
INSERT INTO `usuario_menu` VALUES ('14', '21', 'Cadastrar Currículo', '0', null);
INSERT INTO `usuario_menu` VALUES ('15', '21', 'Currículos em Seleção', '0', null);
INSERT INTO `usuario_menu` VALUES ('16', '21', 'Acompanhamento dos Pedidos de Vaga', '0', null);
INSERT INTO `usuario_menu` VALUES ('17', '21', 'Relatórios', '0', null);
INSERT INTO `usuario_menu` VALUES ('18', '21', 'Resumo do Processo de Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('19', '21', 'Relatório de Pessoas Associadas', '0', null);
INSERT INTO `usuario_menu` VALUES ('20', '21', 'Relatório de Pessoas Desassociadas', '0', null);
INSERT INTO `usuario_menu` VALUES ('21', '21', 'Gestão do Tácito', '0', null);
INSERT INTO `usuario_menu` VALUES ('22', '21', 'Mix de Similaridades', '0', null);
INSERT INTO `usuario_menu` VALUES ('23', '21', 'Coletivos de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('24', '21', 'Criar Coletivos', '0', null);
INSERT INTO `usuario_menu` VALUES ('25', '21', 'Criar Coletivos: Força Tarefa', '0', null);
INSERT INTO `usuario_menu` VALUES ('26', '21', 'Planejar Mix', '0', null);
INSERT INTO `usuario_menu` VALUES ('27', '21', 'Acompanhar Mix', '0', null);
INSERT INTO `usuario_menu` VALUES ('28', '21', 'Padrão de Níveis de Similaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('29', '21', 'Cadastrar Padrão de NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('30', '21', 'Gerenciar Padrões de NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('31', '21', 'Gestão do Tácito', '0', null);
INSERT INTO `usuario_menu` VALUES ('32', '21', 'Aporte de Conhecimento Tácito', '0', null);
INSERT INTO `usuario_menu` VALUES ('33', '21', 'Experiência Profissional por NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('34', '21', 'Experiência Profissional', '0', null);
INSERT INTO `usuario_menu` VALUES ('35', '21', 'Organograma p/ Análise', '0', null);
INSERT INTO `usuario_menu` VALUES ('36', '21', 'Redes Relacionais', '0', null);
INSERT INTO `usuario_menu` VALUES ('37', '21', 'Visualizar Pendências', '0', null);
INSERT INTO `usuario_menu` VALUES ('38', '21', 'Padrões de NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('39', '21', 'Experiências Profissionais', '0', null);
INSERT INTO `usuario_menu` VALUES ('40', '21', 'Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('41', '21', 'Planejamento de Demanda', '0', null);
INSERT INTO `usuario_menu` VALUES ('42', '21', 'Unidades Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('43', '21', 'Cadastrar Unidade de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('44', '21', 'Gerenciar Unidades de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('45', '21', 'Grupos de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('46', '21', 'Cadastrar Grupo de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('47', '21', 'Gerenciar Grupos de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('48', '21', 'Rotas de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('49', '21', 'Cadastrar Rotas de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('50', '21', 'Gerenciar Rotas de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('51', '21', 'Duração Máxima por Função / NS', '0', null);
INSERT INTO `usuario_menu` VALUES ('52', '21', 'Planejamento Real', '0', null);
INSERT INTO `usuario_menu` VALUES ('53', '21', 'Montar Turmas', '0', null);
INSERT INTO `usuario_menu` VALUES ('54', '21', 'Acompanhamento / Finalização', '0', null);
INSERT INTO `usuario_menu` VALUES ('55', '21', 'Lista de Chamada', '0', null);
INSERT INTO `usuario_menu` VALUES ('56', '21', 'Ajustes', '0', null);
INSERT INTO `usuario_menu` VALUES ('57', '21', 'Relatórios', '0', null);
INSERT INTO `usuario_menu` VALUES ('58', '21', 'Por Pessoa', '0', null);
INSERT INTO `usuario_menu` VALUES ('59', '21', 'Por Unidade de Treinamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('60', '21', 'Metodologia', '0', null);
INSERT INTO `usuario_menu` VALUES ('61', '21', 'Cadastrar Metodologia', '0', null);
INSERT INTO `usuario_menu` VALUES ('62', '21', 'Gerenciar Metodologias', '0', null);
INSERT INTO `usuario_menu` VALUES ('63', '21', 'Tipos de Imersão', '0', null);
INSERT INTO `usuario_menu` VALUES ('64', '21', 'Cadastrar Tipo de Imersão', '0', null);
INSERT INTO `usuario_menu` VALUES ('65', '21', 'Gerenciar Tipo de Imersão', '0', null);
INSERT INTO `usuario_menu` VALUES ('66', '21', 'Start-up', '0', null);
INSERT INTO `usuario_menu` VALUES ('67', '21', 'Áreas', '0', null);
INSERT INTO `usuario_menu` VALUES ('68', '21', 'Cadastrar Áreas', '0', null);
INSERT INTO `usuario_menu` VALUES ('69', '21', 'Gerenciar Áreas', '0', null);
INSERT INTO `usuario_menu` VALUES ('70', '21', 'Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('71', '21', 'Cadastrar Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('72', '21', 'Gerenciar Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('73', '21', 'Ordenar Etapas', '0', null);
INSERT INTO `usuario_menu` VALUES ('74', '21', 'Parâmetros de Processo', '0', null);
INSERT INTO `usuario_menu` VALUES ('75', '21', 'Cadastrar Parâmetros', '0', null);
INSERT INTO `usuario_menu` VALUES ('76', '21', 'Gerenciar Parâmetros', '0', null);
INSERT INTO `usuario_menu` VALUES ('77', '21', 'Grupos', '0', null);
INSERT INTO `usuario_menu` VALUES ('78', '21', 'Cadastrar Grupo', '0', null);
INSERT INTO `usuario_menu` VALUES ('79', '21', 'Gerenciar Grupos', '0', null);
INSERT INTO `usuario_menu` VALUES ('80', '21', 'Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('81', '21', 'Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('82', '21', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('83', '21', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('84', '21', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_menu` VALUES ('85', '21', 'Textos', '0', null);
INSERT INTO `usuario_menu` VALUES ('86', '21', 'Alterar Texto do Estudo', '0', null);
INSERT INTO `usuario_menu` VALUES ('87', '21', 'Alterar Texto do Termo de Consentimento', '0', null);
INSERT INTO `usuario_menu` VALUES ('88', '21', 'Glossário (Termos)', '0', null);
INSERT INTO `usuario_menu` VALUES ('89', '21', 'Cadastrar Termo', '0', null);
INSERT INTO `usuario_menu` VALUES ('90', '21', 'Gerenciar Glossário', '0', null);
INSERT INTO `usuario_menu` VALUES ('91', '21', 'Outras Ações', '0', null);
INSERT INTO `usuario_menu` VALUES ('92', '21', 'Gerar Mapa de Mobilização', '0', null);
INSERT INTO `usuario_menu` VALUES ('93', '21', 'Áreas Participantes', '0', null);
INSERT INTO `usuario_menu` VALUES ('94', '21', 'Outras Ações', '0', null);
INSERT INTO `usuario_menu` VALUES ('95', '21', 'Configurações', '0', null);
INSERT INTO `usuario_menu` VALUES ('96', '21', 'Relatório de Erros', '0', null);
INSERT INTO `usuario_menu` VALUES ('97', '21', 'Análise Retroativa (Estudo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('98', '21', 'Assistente para Novo Projeto', '0', null);
INSERT INTO `usuario_menu` VALUES ('99', '21', 'Cadastros', '0', null);
INSERT INTO `usuario_menu` VALUES ('100', '21', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('101', '21', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('102', '21', 'Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('103', '21', 'Cadastrar Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('104', '21', 'Gerenciar Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('105', '21', 'Estados Civis', '0', null);
INSERT INTO `usuario_menu` VALUES ('106', '21', 'Cadastrar Estado Civil', '0', null);
INSERT INTO `usuario_menu` VALUES ('107', '21', 'Gerenciar Estados Civis', '0', null);
INSERT INTO `usuario_menu` VALUES ('108', '21', 'Escolaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('109', '21', 'Cadastrar Escolaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('110', '21', 'Gerenciar Escolaridades', '0', null);
INSERT INTO `usuario_menu` VALUES ('111', '21', 'Cadastros Gerais  ', '0', null);
INSERT INTO `usuario_menu` VALUES ('112', '21', 'Faixas Salariais', '0', null);
INSERT INTO `usuario_menu` VALUES ('113', '21', 'Cadastrar Faixa Salarial', '0', null);
INSERT INTO `usuario_menu` VALUES ('114', '21', 'Gerenciar Faixas Salariais', '0', null);
INSERT INTO `usuario_menu` VALUES ('115', '21', 'Gênero (Sexo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('116', '21', 'Cadastrar Gênero (Sexo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('117', '21', 'Gerenciar Gênero (Sexo)', '0', null);
INSERT INTO `usuario_menu` VALUES ('118', '21', 'Legendas do Organograma', '0', null);
INSERT INTO `usuario_menu` VALUES ('119', '21', 'Cadastrar Legenda', '0', null);
INSERT INTO `usuario_menu` VALUES ('120', '21', 'Gerenciar Legendas', '0', null);
INSERT INTO `usuario_menu` VALUES ('121', '21', 'Cadastros Gerais   ', '0', null);
INSERT INTO `usuario_menu` VALUES ('122', '21', 'Linhas', '0', null);
INSERT INTO `usuario_menu` VALUES ('123', '21', 'Cadastrar Linha', '0', null);
INSERT INTO `usuario_menu` VALUES ('124', '21', 'Gerenciar Linhas', '0', null);
INSERT INTO `usuario_menu` VALUES ('125', '21', 'Locais de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('126', '21', 'Cadastrar Local de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('127', '21', 'Gerenciar Locais de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('128', '21', 'Motivos de Desassociação', '0', null);
INSERT INTO `usuario_menu` VALUES ('129', '21', 'Cadastrar Motivo de Desassociação', '0', null);
INSERT INTO `usuario_menu` VALUES ('130', '21', 'Gerenciar Motivos de Desassociação', '0', null);
INSERT INTO `usuario_menu` VALUES ('131', '21', 'Cadastros Gerais    ', '0', null);
INSERT INTO `usuario_menu` VALUES ('132', '21', 'Nível Funcional na Hierarquia', '0', null);
INSERT INTO `usuario_menu` VALUES ('133', '21', 'Cadastrar Nível', '0', null);
INSERT INTO `usuario_menu` VALUES ('134', '21', 'Gerenciar Níveis', '0', null);
INSERT INTO `usuario_menu` VALUES ('135', '21', 'Países', '0', null);
INSERT INTO `usuario_menu` VALUES ('136', '21', 'Cadastrar País', '0', null);
INSERT INTO `usuario_menu` VALUES ('137', '21', 'Gerenciar Países', '0', null);
INSERT INTO `usuario_menu` VALUES ('138', '21', 'Status do Processo dos PVs', '0', null);
INSERT INTO `usuario_menu` VALUES ('139', '21', 'Cadastrar Status', '0', null);
INSERT INTO `usuario_menu` VALUES ('140', '21', 'Gerenciar Status', '0', null);
INSERT INTO `usuario_menu` VALUES ('141', '21', 'Cadastros Gerais     ', '0', null);
INSERT INTO `usuario_menu` VALUES ('142', '21', 'Unidades de Medida', '0', null);
INSERT INTO `usuario_menu` VALUES ('143', '21', 'Cadastrar Unidades', '0', null);
INSERT INTO `usuario_menu` VALUES ('144', '21', 'Gerenciar Unidades', '0', null);
INSERT INTO `usuario_menu` VALUES ('145', '21', 'Unidades Federativas', '0', null);
INSERT INTO `usuario_menu` VALUES ('146', '21', 'Cadastrar Estado', '0', null);
INSERT INTO `usuario_menu` VALUES ('147', '21', 'Gerenciar Estados', '0', null);
INSERT INTO `usuario_menu` VALUES ('148', '21', 'Nomes dos Níveis Estrutura', '0', null);
INSERT INTO `usuario_menu` VALUES ('149', '21', 'Cadastrar Nome', '0', null);
INSERT INTO `usuario_menu` VALUES ('150', '21', 'Gerenciar Nomes', '0', null);
INSERT INTO `usuario_menu` VALUES ('151', '21', 'Cadastros Gerais      ', '0', null);
INSERT INTO `usuario_menu` VALUES ('152', '21', 'Rede Relacional', '0', null);
INSERT INTO `usuario_menu` VALUES ('153', '21', 'Cadastrar Tipo de Relação', '0', null);
INSERT INTO `usuario_menu` VALUES ('154', '21', 'Gerenciar Tipos de Relação', '0', null);
INSERT INTO `usuario_menu` VALUES ('155', '21', 'Cadastrar Tipo de Relação entre Colegas de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('156', '21', 'Gerenciar Tipos de Relação entre Colegas de Trabalho', '0', null);
INSERT INTO `usuario_menu` VALUES ('157', '21', 'Sincronização', '0', null);
INSERT INTO `usuario_menu` VALUES ('158', '21', 'Sincronização', '0', null);
INSERT INTO `usuario_menu` VALUES ('159', '21', 'Sincronização ', '0', null);
INSERT INTO `usuario_menu` VALUES ('160', '21', 'Importar Folha de Pagamento', '0', null);
INSERT INTO `usuario_menu` VALUES ('161', '21', 'Importar Cargos', '0', null);
INSERT INTO `usuario_menu` VALUES ('162', '21', 'Importar Niveis/Estrutura Org.', '0', null);
INSERT INTO `usuario_menu` VALUES ('163', '21', 'Sincronização  ', '0', null);
INSERT INTO `usuario_menu` VALUES ('164', '21', 'Importar Dados Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('165', '21', 'Importar Vagas', '0', null);
INSERT INTO `usuario_menu` VALUES ('166', '21', 'Importar Dados Pessoais', '0', null);
INSERT INTO `usuario_menu` VALUES ('167', '21', 'Sincronização   ', '0', null);
INSERT INTO `usuario_menu` VALUES ('168', '21', 'Importar Experiências Práticas', '0', null);
INSERT INTO `usuario_menu` VALUES ('169', '21', 'Importar Associações Pessoa-Vaga', '0', null);
INSERT INTO `usuario_menu` VALUES ('170', '21', 'Importar Pedidos de Vaga', '0', null);
INSERT INTO `usuario_menu` VALUES ('171', '21', 'Sincronização     ', '0', null);
INSERT INTO `usuario_menu` VALUES ('172', '21', 'Importar Padrões de Níveis de Similaridade', '0', null);
INSERT INTO `usuario_menu` VALUES ('173', '21', 'Importar Planejamento de Treinamentos', '0', null);
INSERT INTO `usuario_menu` VALUES ('174', '2', 'Clientes', '0', null);
INSERT INTO `usuario_menu` VALUES ('175', '2', 'Cadastrar Cliente', '0', null);
INSERT INTO `usuario_menu` VALUES ('176', '2', 'Visualizar Lista de Clientes', '0', null);
INSERT INTO `usuario_menu` VALUES ('177', '2', 'Visualizar Cobranças Geradas', '0', null);
INSERT INTO `usuario_menu` VALUES ('178', '2', 'Sistemas A Venda', '0', null);
INSERT INTO `usuario_menu` VALUES ('179', '2', 'Cadastrar Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('180', '2', 'Visualizar Sistemas', '0', null);
INSERT INTO `usuario_menu` VALUES ('181', '2', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('182', '2', 'Empresas de Hosting', '0', null);
INSERT INTO `usuario_menu` VALUES ('183', '2', 'Cadastrar Empresa de Hosting', '0', null);
INSERT INTO `usuario_menu` VALUES ('184', '2', 'Gerenciar Empresas de Hosting', '0', null);
INSERT INTO `usuario_menu` VALUES ('185', '2', 'Status dos Pagamentos', '0', null);
INSERT INTO `usuario_menu` VALUES ('186', '2', 'Cadastrar Status', '0', null);
INSERT INTO `usuario_menu` VALUES ('187', '2', 'Gerenciar Status', '0', null);
INSERT INTO `usuario_menu` VALUES ('188', '2', 'Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('189', '2', 'Cadastrar Tipo de Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('190', '2', 'Gerenciar Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('191', '2', 'Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('192', '2', 'Gerar Cobranças Agora', '0', null);
INSERT INTO `usuario_menu` VALUES ('193', '2', 'Relatórios', '0', null);
INSERT INTO `usuario_menu` VALUES ('194', '2', 'Fluxo de Caixa', '0', null);

-- ----------------------------
-- Table structure for `usuario_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_privilegio`;
CREATE TABLE `usuario_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo`;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_visivel` varchar(255) DEFAULT NULL,
  `status_BOOLEAN` int(11) NOT NULL,
  `pagina_inicial` text,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo
-- ----------------------------
INSERT INTO `usuario_tipo` VALUES ('1', 'Usuário Administrador', 'Usuário Administrador', '1', '', '0', null);

-- ----------------------------
-- Table structure for `usuario_tipo_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_menu`;
CREATE TABLE `usuario_tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10438 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_menu
-- ----------------------------
INSERT INTO `usuario_tipo_menu` VALUES ('10400', '1', 'Clientes', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10401', '1', 'Cadastrar Cliente', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10402', '1', 'Visualizar Lista de Clientes', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10403', '1', 'Visualizar Cobranças Geradas', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10404', '1', 'Hospedagens', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10405', '1', 'Cadastrar Nova Hospedagem', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10406', '1', 'Visualizar Lista de Hospedagens', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10407', '1', 'Serviços', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10408', '1', 'Cadastrar Novo Serviço', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10409', '1', 'Visualizar Lista de Serviços', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10410', '1', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10411', '1', 'Empresas de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10412', '1', 'Cadastrar Empresa de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10413', '1', 'Gerenciar Empresas de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10414', '1', 'Status dos Pagamentos', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10415', '1', 'Cadastrar Status', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10416', '1', 'Gerenciar Status', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10417', '1', 'Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10418', '1', 'Cadastrar Tipo de Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10419', '1', 'Gerenciar Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10420', '1', 'Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10421', '1', 'Gerar Cobranças Agora', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10422', '1', 'Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10423', '1', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10424', '1', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10425', '1', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10426', '1', 'Fazer Limpeza do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10427', '1', 'Configurações de Envio', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10428', '1', 'Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10429', '1', 'Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10430', '1', 'Cadastrar Usuário do Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10431', '1', 'Gerenciar Usuários do Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10432', '1', 'Visualizar Operações de Usuários no sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10433', '1', 'Classes de Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10434', '1', 'Cadastrar Classe de Usuário', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10435', '1', 'Gerenciar Classes de Usuário', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10436', '1', 'Relatórios', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10437', '1', 'Fluxo de Caixa', '0', null);

-- ----------------------------
-- Table structure for `usuario_tipo_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_privilegio`;
CREATE TABLE `usuario_tipo_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_privilegio
-- ----------------------------
