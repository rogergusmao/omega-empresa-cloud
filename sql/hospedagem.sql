/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : hospedagem

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-09-03 15:59:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acesso`
-- ----------------------------
DROP TABLE IF EXISTS `acesso`;
CREATE TABLE `acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `data_login_DATETIME` datetime NOT NULL,
  `data_logout_DATETIME` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of acesso
-- ----------------------------
INSERT INTO `acesso` VALUES ('206', '2', '2017-07-05 19:54:05', null, '0', null);
INSERT INTO `acesso` VALUES ('207', '2', '2017-07-06 21:28:34', null, '0', null);
INSERT INTO `acesso` VALUES ('208', '2', '2017-07-09 10:53:20', null, '0', null);
INSERT INTO `acesso` VALUES ('209', '2', '2017-07-17 11:48:46', null, '0', null);
INSERT INTO `acesso` VALUES ('210', '2', '2017-07-18 07:43:30', null, '0', null);
INSERT INTO `acesso` VALUES ('211', '2', '2017-07-18 21:12:06', null, '0', null);
INSERT INTO `acesso` VALUES ('212', '2', '2017-07-19 00:07:32', null, '0', null);
INSERT INTO `acesso` VALUES ('213', '2', '2017-07-19 12:20:34', null, '0', null);
INSERT INTO `acesso` VALUES ('214', '2', '2017-07-19 21:36:34', null, '0', null);
INSERT INTO `acesso` VALUES ('215', '2', '2017-07-21 12:10:01', null, '0', null);
INSERT INTO `acesso` VALUES ('216', '2', '2017-07-22 20:58:16', null, '0', null);
INSERT INTO `acesso` VALUES ('217', '2', '2017-07-23 21:17:14', null, '0', null);
INSERT INTO `acesso` VALUES ('218', '2', '2017-07-23 22:04:26', null, '0', null);
INSERT INTO `acesso` VALUES ('219', '2', '2017-07-24 17:55:25', null, '0', null);
INSERT INTO `acesso` VALUES ('220', '2', '2017-07-24 18:31:49', null, '0', null);
INSERT INTO `acesso` VALUES ('221', '2', '2017-07-24 23:24:25', null, '0', null);
INSERT INTO `acesso` VALUES ('222', '2', '2017-08-03 19:55:24', null, '0', null);
INSERT INTO `acesso` VALUES ('223', '2', '2017-08-03 22:08:56', null, '0', null);
INSERT INTO `acesso` VALUES ('224', '2', '2017-08-05 08:49:30', null, '0', null);
INSERT INTO `acesso` VALUES ('225', '2', '2017-08-05 15:36:10', null, '0', null);
INSERT INTO `acesso` VALUES ('226', '2', '2017-08-05 16:39:34', null, '0', null);
INSERT INTO `acesso` VALUES ('227', '2', '2017-08-05 17:29:54', null, '0', null);
INSERT INTO `acesso` VALUES ('228', '2', '2017-08-05 17:50:59', null, '0', null);
INSERT INTO `acesso` VALUES ('229', '2', '2017-08-05 18:09:43', null, '0', null);
INSERT INTO `acesso` VALUES ('230', '2', '2017-08-05 18:34:34', null, '0', null);
INSERT INTO `acesso` VALUES ('231', '2', '2017-08-05 19:23:45', null, '0', null);
INSERT INTO `acesso` VALUES ('232', '2', '2017-08-05 19:51:10', null, '0', null);
INSERT INTO `acesso` VALUES ('233', '2', '2017-08-05 21:37:10', null, '0', null);
INSERT INTO `acesso` VALUES ('234', '2', '2017-08-06 11:18:25', null, '0', null);
INSERT INTO `acesso` VALUES ('235', '2', '2017-08-06 12:04:20', null, '0', null);
INSERT INTO `acesso` VALUES ('236', '2', '2017-08-06 12:19:07', null, '0', null);
INSERT INTO `acesso` VALUES ('237', '2', '2017-08-06 13:04:56', null, '0', null);
INSERT INTO `acesso` VALUES ('238', '2', '2017-08-06 15:21:51', null, '0', null);
INSERT INTO `acesso` VALUES ('239', '2', '2017-08-06 15:33:20', null, '0', null);
INSERT INTO `acesso` VALUES ('240', '2', '2017-08-06 15:34:13', null, '0', null);
INSERT INTO `acesso` VALUES ('241', '2', '2017-08-06 15:44:21', null, '0', null);
INSERT INTO `acesso` VALUES ('242', '2', '2017-08-06 15:45:22', null, '0', null);
INSERT INTO `acesso` VALUES ('243', '2', '2017-08-06 15:51:32', null, '0', null);
INSERT INTO `acesso` VALUES ('244', '2', '2017-08-06 16:02:14', null, '0', null);
INSERT INTO `acesso` VALUES ('245', '2', '2017-08-06 16:03:25', null, '0', null);
INSERT INTO `acesso` VALUES ('246', '2', '2017-08-06 16:10:29', null, '0', null);
INSERT INTO `acesso` VALUES ('247', '2', '2017-08-06 16:15:30', null, '0', null);
INSERT INTO `acesso` VALUES ('248', '2', '2017-08-06 16:17:01', null, '0', null);
INSERT INTO `acesso` VALUES ('249', '2', '2017-08-06 17:19:20', null, '0', null);
INSERT INTO `acesso` VALUES ('250', '2', '2017-08-06 17:23:52', null, '0', null);
INSERT INTO `acesso` VALUES ('251', '2', '2017-08-06 18:22:41', null, '0', null);
INSERT INTO `acesso` VALUES ('252', '2', '2017-08-06 18:25:00', null, '0', null);
INSERT INTO `acesso` VALUES ('253', '2', '2017-08-06 19:28:53', null, '0', null);
INSERT INTO `acesso` VALUES ('254', '2', '2017-08-12 09:37:24', null, '0', null);
INSERT INTO `acesso` VALUES ('255', '2', '2017-08-12 10:34:34', null, '0', null);
INSERT INTO `acesso` VALUES ('256', '2', '2017-08-12 14:23:51', null, '0', null);
INSERT INTO `acesso` VALUES ('257', '2', '2017-08-12 22:57:24', null, '0', null);
INSERT INTO `acesso` VALUES ('258', '2', '2017-08-12 23:19:14', null, '0', null);
INSERT INTO `acesso` VALUES ('259', '2', '2017-08-13 19:56:41', null, '0', null);
INSERT INTO `acesso` VALUES ('260', '2', '2017-08-13 20:48:19', null, '0', null);
INSERT INTO `acesso` VALUES ('261', '2', '2017-08-13 21:53:56', null, '0', null);
INSERT INTO `acesso` VALUES ('262', '2', '2017-08-16 22:33:57', null, '0', null);
INSERT INTO `acesso` VALUES ('263', '2', '2017-08-17 07:52:21', null, '0', null);
INSERT INTO `acesso` VALUES ('264', '2', '2017-08-17 12:20:47', null, '0', null);
INSERT INTO `acesso` VALUES ('265', '2', '2017-08-18 07:18:20', null, '0', null);
INSERT INTO `acesso` VALUES ('266', '2', '2017-08-19 21:33:50', null, '0', null);
INSERT INTO `acesso` VALUES ('267', '2', '2017-08-20 11:29:47', null, '0', null);
INSERT INTO `acesso` VALUES ('268', '2', '2017-08-26 12:26:43', null, '0', null);

-- ----------------------------
-- Table structure for `assinatura`
-- ----------------------------
DROP TABLE IF EXISTS `assinatura`;
CREATE TABLE `assinatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_site` varchar(255) DEFAULT NULL,
  `hospedagem_id_INT` int(11) DEFAULT NULL,
  `sistema_id_INT` int(11) DEFAULT NULL,
  `estado_assinatura_id_INT` int(11) DEFAULT NULL,
  `sicob_cliente_assinatura_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `id_sistema_versao_biblioteca_nuvem_cliente_INT` int(11) DEFAULT NULL,
  `id_corporacao_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hos` (`hospedagem_id_INT`),
  KEY `sis` (`sistema_id_INT`),
  KEY `estado_assinatura_id_INT` (`estado_assinatura_id_INT`),
  CONSTRAINT `assinatura_ibfk_1` FOREIGN KEY (`estado_assinatura_id_INT`) REFERENCES `estado_assinatura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hos` FOREIGN KEY (`hospedagem_id_INT`) REFERENCES `hospedagem` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sis` FOREIGN KEY (`sistema_id_INT`) REFERENCES `sistema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=736 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assinatura
-- ----------------------------

-- ----------------------------
-- Table structure for `assinatura_migracao`
-- ----------------------------
DROP TABLE IF EXISTS `assinatura_migracao`;
CREATE TABLE `assinatura_migracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assinatura_id_INT` int(11) DEFAULT NULL,
  `nova_assinatura_id_INT` int(11) DEFAULT NULL,
  `antigo_sistema_id_INT` int(11) DEFAULT NULL,
  `novo_sistema_id_INT` int(11) DEFAULT NULL,
  `data_cadastro_DATETIME` datetime DEFAULT NULL,
  `data_inicio_migracao_DATETIME` datetime DEFAULT NULL,
  `data_fim_migracao_DATETIME` datetime DEFAULT NULL,
  `antigo_sicob_cliente_assinatura_INT` int(11) DEFAULT NULL,
  `novo_sicob_cliente_assinatura_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `erro` varchar(256) DEFAULT NULL,
  `data_inicio_disponibilizacao_assinatura_DATETIME` datetime DEFAULT NULL,
  `data_fim_disponibilizacao_assinatura_DATETIME` datetime DEFAULT NULL,
  `id_corporacao_INT` int(11) DEFAULT NULL,
  `estado_assinatura_migracao_id_INT` int(11) DEFAULT NULL,
  `data_espera_manutencao_DATETIME` datetime DEFAULT NULL,
  `data_retorno_migracao_DATETIME` datetime DEFAULT NULL,
  `sqlite_gerado_novamente_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assinatura_id_INT` (`assinatura_id_INT`),
  KEY `nova_assinatura_id_INT` (`nova_assinatura_id_INT`),
  KEY `antigo_sistema_id_INT` (`antigo_sistema_id_INT`),
  KEY `novo_sistema_id_INT` (`novo_sistema_id_INT`),
  KEY `estado_assinatura_migracao_id_INT` (`estado_assinatura_migracao_id_INT`) USING BTREE,
  CONSTRAINT `assinatura_migracao_ibfk_1` FOREIGN KEY (`assinatura_id_INT`) REFERENCES `assinatura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assinatura_migracao_ibfk_2` FOREIGN KEY (`nova_assinatura_id_INT`) REFERENCES `assinatura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assinatura_migracao_ibfk_3` FOREIGN KEY (`antigo_sistema_id_INT`) REFERENCES `sistema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assinatura_migracao_ibfk_4` FOREIGN KEY (`novo_sistema_id_INT`) REFERENCES `sistema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assinatura_migracao
-- ----------------------------

-- ----------------------------
-- Table structure for `assinatura_migracao_dipara`
-- ----------------------------
DROP TABLE IF EXISTS `assinatura_migracao_dipara`;
CREATE TABLE `assinatura_migracao_dipara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sistema_tabela_INT` int(11) DEFAULT NULL,
  `id_antigo_INT` int(11) DEFAULT NULL,
  `id_novo_INT` int(11) DEFAULT NULL,
  `assinatura_migracao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assinatura_migracao_id_INT` (`assinatura_migracao_id_INT`),
  CONSTRAINT `assinatura_migracao_dipara_ibfk_1` FOREIGN KEY (`assinatura_migracao_id_INT`) REFERENCES `assinatura_migracao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1442 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assinatura_migracao_dipara
-- ----------------------------

-- ----------------------------
-- Table structure for `configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `configuracao`;
CREATE TABLE `configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_projeto` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configuracao
-- ----------------------------
INSERT INTO `configuracao` VALUES ('1', 'Modelo de Projetos', '0', null);

-- ----------------------------
-- Table structure for `configuracao_email`
-- ----------------------------
DROP TABLE IF EXISTS `configuracao_email`;
CREATE TABLE `configuracao_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_remetente` varchar(255) NOT NULL,
  `endereco_remetente` varchar(255) NOT NULL,
  `return_path` varchar(255) NOT NULL,
  `usuario_return_path` varchar(255) NOT NULL,
  `senha_return_path` varchar(255) NOT NULL,
  `numero_de_emails_por_vez_INT` int(11) NOT NULL,
  `intervalo_turno_em_segundos_INT` int(11) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configuracao_email
-- ----------------------------
INSERT INTO `configuracao_email` VALUES ('1', 'Omega Software', 'financeiro@omegasoftware.com.br', 'retorno@omegasoftware.com.br', 'retorno@omegasoftware.com.br', 'omega24511102', '20', '10', '0', null);

-- ----------------------------
-- Table structure for `db_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `db_tipo`;
CREATE TABLE `db_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `porta_padrao_INT` int(11) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of db_tipo
-- ----------------------------
INSERT INTO `db_tipo` VALUES ('1', 'MySQL', '3306', '0', null);

-- ----------------------------
-- Table structure for `empresa_hosting`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_hosting`;
CREATE TABLE `empresa_hosting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `endereco_painel_controle` varchar(255) DEFAULT NULL,
  `endereco_cobranca` varchar(255) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_hosting
-- ----------------------------
INSERT INTO `empresa_hosting` VALUES ('1', 'Hostdime', 'https://187.45.181.206:2087/', 'https://core.hostdime.com.br/', '0', null);
INSERT INTO `empresa_hosting` VALUES ('2', 'Locaweb', '', 'http://painel.locaweb.com.br', '0', null);
INSERT INTO `empresa_hosting` VALUES ('3', 'Localhost', 'http://localhost/', null, '0', null);

-- ----------------------------
-- Table structure for `estado_assinatura`
-- ----------------------------
DROP TABLE IF EXISTS `estado_assinatura`;
CREATE TABLE `estado_assinatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_assinatura
-- ----------------------------
INSERT INTO `estado_assinatura` VALUES ('1', 'disponível', '0', null);
INSERT INTO `estado_assinatura` VALUES ('2', 'ocupada', '0', null);
INSERT INTO `estado_assinatura` VALUES ('3', 'cancelada', '0', null);
INSERT INTO `estado_assinatura` VALUES ('4', 'EM_MIGRACAO', '0', null);
INSERT INTO `estado_assinatura` VALUES ('5', 'EM_MANUTENCAO', '0', null);
INSERT INTO `estado_assinatura` VALUES ('6', 'ASSINATURA_FOI_MIGRADA', '0', null);
INSERT INTO `estado_assinatura` VALUES ('7', 'ERRO_DURANTE_DISPONIBILIZACAO', '0', null);
INSERT INTO `estado_assinatura` VALUES ('8', 'ASSINATURA_RESERVADA', '0', null);
INSERT INTO `estado_assinatura` VALUES ('9', 'DISPONIVEL_PARA_MIGRACAO', '0', null);

-- ----------------------------
-- Table structure for `estado_assinatura_migracao`
-- ----------------------------
DROP TABLE IF EXISTS `estado_assinatura_migracao`;
CREATE TABLE `estado_assinatura_migracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_assinatura_migracao
-- ----------------------------
INSERT INTO `estado_assinatura_migracao` VALUES ('1', 'AGUARDANDO_MIGRACAO', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('4', 'MIGRADO_COM_SUCESSO', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('5', 'FALTA_ASSINATURA_DISPONIVEL', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('6', 'ERRO', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('8', 'ERRO_DURANTE_DISPONIBILIZACAO', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('7', 'ASSINATURA_DISPONIBILIZADA', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('2', 'AGUARDANDO_ESTADO_MANUTENCAO', '0', null);
INSERT INTO `estado_assinatura_migracao` VALUES ('3', 'CONTINUACAO_MIGRACAO', '0', null);

-- ----------------------------
-- Table structure for `hospedagem`
-- ----------------------------
DROP TABLE IF EXISTS `hospedagem`;
CREATE TABLE `hospedagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dominio` varchar(255) DEFAULT NULL,
  `dominio_webservice` varchar(500) DEFAULT NULL,
  `empresa_hosting_id_INT` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `dominio_sincronizador` varchar(255) DEFAULT NULL,
  `dominio_sincronizador_webservice` varchar(255) DEFAULT NULL,
  `path_sincronizador` varchar(255) DEFAULT NULL,
  `path_raiz_conteudo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=593 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hospedagem
-- ----------------------------

-- ----------------------------
-- Table structure for `hospedagem_db`
-- ----------------------------
DROP TABLE IF EXISTS `hospedagem_db`;
CREATE TABLE `hospedagem_db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hospedagem_id_INT` int(11) DEFAULT NULL,
  `nome_db` varchar(255) NOT NULL,
  `host_db` varchar(255) NOT NULL,
  `porta_db_INT` int(11) NOT NULL,
  `usuario_db` varchar(255) NOT NULL,
  `senha_db` varchar(255) DEFAULT NULL,
  `tipo_hospedagem_db_id_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hospedagem_id_INT` (`hospedagem_id_INT`),
  KEY `tipo_hospedagem_db_id_INT` (`tipo_hospedagem_db_id_INT`),
  CONSTRAINT `hospedagem_db_ibfk_1` FOREIGN KEY (`hospedagem_id_INT`) REFERENCES `hospedagem` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hospedagem_db_ibfk_2` FOREIGN KEY (`tipo_hospedagem_db_id_INT`) REFERENCES `tipo_hospedagem_db` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hospedagem_db
-- ----------------------------

-- ----------------------------
-- Table structure for `hospedagem_ftp`
-- ----------------------------
DROP TABLE IF EXISTS `hospedagem_ftp`;
CREATE TABLE `hospedagem_ftp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hospedagem_id_INT` int(11) NOT NULL,
  `host_ftp` varchar(255) NOT NULL,
  `porta_ftp_INT` int(11) NOT NULL,
  `usuario_ftp` varchar(255) NOT NULL,
  `senha_ftp` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hospedagem_ftp
-- ----------------------------

-- ----------------------------
-- Table structure for `operacao_sistema`
-- ----------------------------
DROP TABLE IF EXISTS `operacao_sistema`;
CREATE TABLE `operacao_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `tipo_operacao` varchar(50) DEFAULT NULL,
  `pagina_operacao` varchar(255) DEFAULT NULL,
  `entidade_operacao` varchar(50) DEFAULT NULL,
  `chave_registro_operacao_INT` int(11) DEFAULT NULL,
  `descricao_operacao` varchar(255) DEFAULT NULL,
  `data_operacao_DATETIME` datetime NOT NULL,
  `url_completa` varchar(1000) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario_id_INT`) USING BTREE,
  CONSTRAINT `operacao_sistema_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operacao_sistema
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema`
-- ----------------------------
DROP TABLE IF EXISTS `sistema`;
CREATE TABLE `sistema` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `sistema_biblioteca_nuvem_INT` int(11) NOT NULL,
  `path_raiz_assinatura` varchar(255) DEFAULT NULL,
  `path_raiz_dump` varchar(255) DEFAULT NULL,
  `dominio_raiz_assinatura` varchar(255) DEFAULT NULL COMMENT 'Domínio raíz a ser utilizado pela assinatura',
  `path_raiz_codigo_assinatura` varchar(255) DEFAULT NULL,
  `empresa_hosting_id_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  `sistema_tipo_id_INT` int(11) DEFAULT NULL,
  `path_raiz_codigo_sincronizador` varchar(255) DEFAULT NULL,
  `dominio_raiz_sincronizador` varchar(255) DEFAULT NULL,
  `max_assinatura_por_hospedagem_INT` int(11) DEFAULT NULL,
  `path_raiz_conteudo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_tipo_id_INT` (`sistema_tipo_id_INT`),
  CONSTRAINT `sistema_ibfk_1` FOREIGN KEY (`sistema_tipo_id_INT`) REFERENCES `sistema_tipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema
-- ----------------------------
INSERT INTO `sistema` VALUES ('1', 'OMEGA EMPRESA', '1', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\modelo', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\BancoDeDadosModelo', 'http://192.168.1.101/pontoeletroniconovo/PE10003Corporacao/modelo/', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\ModeloParaHospedagem\\', '1', '0', null, '2', 'C:\\wamp64\\www\\SincronizadorWeb\\SW10002Corporacao', 'http://192.168.1.101/SincronizadorWeb/SW10002Corporacao/', '1', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\ClusterOmegaEmpresa');
INSERT INTO `sistema` VALUES ('3', 'OMEGA EMPRESA CORPORAÇÃO', '1', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\modelo', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\BancoDeDadosModelo', 'http://192.168.1.101/pontoeletroniconovo/PE10003Corporacao/modelo/', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\ModeloParaHospedagem\\', '1', '0', null, '3', 'C:\\wamp64\\www\\SincronizadorWeb\\SW10002Corporacao', 'http://192.168.1.101/SincronizadorWeb/SW10002Corporacao/', '3', 'C:\\wamp64\\www\\PontoEletronicoNovo\\PE10003Corporacao\\ClusterOmegaEmpresaCorporacao');

-- ----------------------------
-- Table structure for `sistema_db`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_db`;
CREATE TABLE `sistema_db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(255) NOT NULL,
  `assinatura_host_db` varchar(255) NOT NULL,
  `assinatura_porta_db_INT` int(11) NOT NULL,
  `assinatura_usuario_db` varchar(255) NOT NULL,
  `assinatura_senha_db` varchar(255) DEFAULT NULL,
  `path_arquivo_dump` varchar(255) DEFAULT NULL,
  `tipo_hospedagem_db_id_INT` int(11) DEFAULT NULL,
  `sistema_id_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_id_INT` (`sistema_id_INT`),
  CONSTRAINT `sistema_db_ibfk_1` FOREIGN KEY (`sistema_id_INT`) REFERENCES `sistema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_db
-- ----------------------------
INSERT INTO `sistema_db` VALUES ('1', 'omega_empresa_web_prod', 'localhost', '3306', 'root', '', 'omega_empresa_web_prod.sql', '1', '1', '0', null);
INSERT INTO `sistema_db` VALUES ('2', 'omega_empresa_parametros', 'localhost', '3306', 'root', '', 'omega_empresa_parametros.sql', '2', '1', '0', null);
INSERT INTO `sistema_db` VALUES ('3', 'sincronizador_web_corporacao_prod', 'localhost', '3306', 'root', '', 'sincronizador_web_corporacao_prod.sql', '3', '1', '0', null);
INSERT INTO `sistema_db` VALUES ('4', 'omega_empresa_web_prod', 'localhost', '3306', 'root', null, 'omega_empresa_web_prod.sql', '1', '3', '0', null);
INSERT INTO `sistema_db` VALUES ('5', 'omega_empresa_parametros', 'localhost', '3306', 'root', null, 'omega_empresa_parametros.sql', '2', '3', '0', null);
INSERT INTO `sistema_db` VALUES ('6', 'sincronizador_web_corporacao_prod', 'localhost', '3306', 'root', '', 'sincronizador_web_corporacao_prod.sql', '3', '3', '0', null);

-- ----------------------------
-- Table structure for `sistema_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo`;
CREATE TABLE `sistema_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo
-- ----------------------------
INSERT INTO `sistema_tipo` VALUES ('1', 'COPIA ARQUIVOS E BANCO DE DADOS DO SISTEMA', '0', null);
INSERT INTO `sistema_tipo` VALUES ('2', 'COMPARTILHA ARQUIVOS E COPIA BANCO DE DADOS DO SIS', '0', null);
INSERT INTO `sistema_tipo` VALUES ('3', 'COMPARTILHA ARQUIVOS E BANCO DE DADOS DO SISTEMA', '0', null);

-- ----------------------------
-- Table structure for `tipo_hospedagem_db`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_hospedagem_db`;
CREATE TABLE `tipo_hospedagem_db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_hospedagem_db
-- ----------------------------
INSERT INTO `tipo_hospedagem_db` VALUES ('1', 'PRINCIPAL', '0', null);
INSERT INTO `tipo_hospedagem_db` VALUES ('2', 'PARAMETROS', '0', null);
INSERT INTO `tipo_hospedagem_db` VALUES ('3', 'SINCRONIZADOR', '0', null);

-- ----------------------------
-- Table structure for `uf`
-- ----------------------------
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `sigla` char(2) NOT NULL,
  `dataCadastro` datetime DEFAULT NULL,
  `dataEdicao` datetime DEFAULT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sigla` (`sigla`) USING BTREE,
  UNIQUE KEY `nome` (`nome`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uf
-- ----------------------------
INSERT INTO `uf` VALUES ('1', 'São Paulo', 'SP', '2009-10-18 16:05:17', '2009-10-18 16:05:17', '0', null);
INSERT INTO `uf` VALUES ('2', 'Minas Gerais', 'MG', '2009-10-18 16:08:39', '2009-10-18 16:08:39', '0', null);
INSERT INTO `uf` VALUES ('3', 'Pará', 'PA', '2009-10-23 11:20:19', '2009-10-23 11:20:19', '0', null);
INSERT INTO `uf` VALUES ('4', 'Espirito Santo', 'ES', '2009-11-04 19:22:18', '2009-11-04 19:22:18', '0', null);
INSERT INTO `uf` VALUES ('5', 'Rio de Janeiro', 'RJ', '2010-01-01 12:03:54', '2010-01-01 12:03:54', '0', null);
INSERT INTO `uf` VALUES ('6', 'Rio Grande do Sul', 'RS', '2010-01-01 12:04:12', '2010-01-01 12:04:12', '0', null);
INSERT INTO `uf` VALUES ('7', 'Bahia', 'BA', '2010-01-01 12:04:23', '2010-01-01 12:04:23', '0', null);
INSERT INTO `uf` VALUES ('8', 'Tocantins', 'TO', '2010-01-01 12:04:38', '2010-01-01 12:04:38', '0', null);
INSERT INTO `uf` VALUES ('9', 'Maranhão', 'MA', '2010-01-01 12:06:00', '2010-01-01 12:06:00', '0', null);
INSERT INTO `uf` VALUES ('10', 'Acre', 'AC', '2010-01-01 12:06:16', '2010-01-01 12:06:16', '0', null);
INSERT INTO `uf` VALUES ('11', 'Alagoas', 'AL', '2010-01-01 12:06:31', '2010-01-01 12:06:31', '0', null);
INSERT INTO `uf` VALUES ('12', 'Amapá', 'AP', '2010-01-01 12:06:50', '2010-01-01 12:06:50', '0', null);
INSERT INTO `uf` VALUES ('13', 'Amazonas', 'AM', '2010-01-01 12:07:01', '2010-01-01 12:07:01', '0', null);
INSERT INTO `uf` VALUES ('14', 'Ceará', 'CE', '2010-01-01 12:07:21', '2010-01-01 12:07:21', '0', null);
INSERT INTO `uf` VALUES ('15', 'Distrito Federal', 'DF', '2010-01-01 12:07:40', '2010-01-01 12:07:40', '0', null);
INSERT INTO `uf` VALUES ('16', 'Goiás', 'GO', '2010-01-01 12:08:08', '2010-01-01 12:08:08', '0', null);
INSERT INTO `uf` VALUES ('17', 'Mato Grosso', 'MT', '2010-01-01 12:08:30', '2010-01-01 12:08:30', '0', null);
INSERT INTO `uf` VALUES ('18', 'Mato Grosso do Sul', 'MS', '2010-01-01 12:08:54', '2010-01-01 12:08:54', '0', null);
INSERT INTO `uf` VALUES ('19', 'Paraíba', 'PB', '2010-01-01 12:09:15', '2010-01-01 12:09:15', '0', null);
INSERT INTO `uf` VALUES ('20', 'Paraná', 'PR', '2010-01-01 12:09:43', '2010-01-01 12:09:43', '0', null);
INSERT INTO `uf` VALUES ('21', 'Pernambuco', 'PE', '2010-01-01 12:10:09', '2010-01-01 12:10:09', '0', null);
INSERT INTO `uf` VALUES ('22', 'Piauí', 'PI', '2010-01-01 12:10:36', '2010-01-01 12:10:36', '0', null);
INSERT INTO `uf` VALUES ('23', 'Rio Grande do Norte', 'RN', '2010-01-01 12:10:55', '2010-01-01 12:10:55', '0', null);
INSERT INTO `uf` VALUES ('24', 'Rondônia', 'RO', '2010-01-01 12:11:15', '2010-01-01 12:11:15', '0', null);
INSERT INTO `uf` VALUES ('25', 'Roraima', 'RR', '2010-01-01 12:11:45', '2010-01-01 12:11:45', '0', null);
INSERT INTO `uf` VALUES ('26', 'Santa Catarina', 'SC', '2010-01-01 12:12:03', '2010-01-01 12:12:03', '0', null);
INSERT INTO `uf` VALUES ('27', 'Sergipe', 'SE', '2010-01-01 12:12:20', '2010-01-01 12:12:20', '0', null);
INSERT INTO `uf` VALUES ('28', 'asfdg541', '', null, null, '0', null);

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `pagina_inicial` text,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_usuario_usuario` (`usuario_tipo_id_INT`) USING BTREE,
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Desenvolvimento', 'eduardo@omegasoftware.com.br', '8ffbcb8b27121de4a439478002b454cd', '1', '1', null, '0', null);
INSERT INTO `usuario` VALUES ('2', 'Roger', 'rogerfsg@gmail.com', '18563035b358c4f559f4bbc16004bf79', '1', '1', '', '0', null);

-- ----------------------------
-- Table structure for `usuario_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_menu`;
CREATE TABLE `usuario_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_menu
-- ----------------------------
INSERT INTO `usuario_menu` VALUES ('275', '2', 'Assinaturas', '0', null);
INSERT INTO `usuario_menu` VALUES ('276', '2', 'Cadastrar Assinatura', '0', null);
INSERT INTO `usuario_menu` VALUES ('277', '2', 'Gerenciar Assinaturas', '0', null);
INSERT INTO `usuario_menu` VALUES ('278', '2', 'Criar Novas Assinaturas', '0', null);
INSERT INTO `usuario_menu` VALUES ('279', '2', 'Bancos de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('280', '2', 'Cadastrar Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('281', '2', 'Visualizar Bancos de Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('282', '2', 'Sistemas', '0', null);
INSERT INTO `usuario_menu` VALUES ('283', '2', 'Listar Sistemas', '0', null);
INSERT INTO `usuario_menu` VALUES ('284', '2', 'Cadastrar Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('285', '2', 'Sites', '0', null);
INSERT INTO `usuario_menu` VALUES ('286', '2', 'Listar Hostings', '0', null);
INSERT INTO `usuario_menu` VALUES ('287', '2', 'Cadastrar Hostings', '0', null);
INSERT INTO `usuario_menu` VALUES ('288', '2', 'Cadastrar Hospedagens', '0', null);
INSERT INTO `usuario_menu` VALUES ('289', '2', 'Visualizar Hospedagens', '0', null);
INSERT INTO `usuario_menu` VALUES ('290', '2', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_menu` VALUES ('291', '2', 'Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('292', '2', 'Cadastrar Tipo de Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('293', '2', 'Gerenciar Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('294', '2', 'Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('295', '2', 'Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('296', '2', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('297', '2', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('298', '2', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_menu` VALUES ('299', '2', 'Fazer Limpeza do Banco de Dados', '0', null);
INSERT INTO `usuario_menu` VALUES ('300', '2', 'Configurações de Envio', '0', null);
INSERT INTO `usuario_menu` VALUES ('301', '2', 'Usuários', '0', null);
INSERT INTO `usuario_menu` VALUES ('302', '2', 'Usuários', '0', null);
INSERT INTO `usuario_menu` VALUES ('303', '2', 'Cadastrar Usuário do Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('304', '2', 'Gerenciar Usuários do Sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('305', '2', 'Visualizar Operações de Usuários no sistema', '0', null);
INSERT INTO `usuario_menu` VALUES ('306', '2', 'Classes de Usuários', '0', null);
INSERT INTO `usuario_menu` VALUES ('307', '2', 'Cadastrar Classe de Usuário', '0', null);
INSERT INTO `usuario_menu` VALUES ('308', '2', 'Gerenciar Classes de Usuário', '0', null);

-- ----------------------------
-- Table structure for `usuario_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_privilegio`;
CREATE TABLE `usuario_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo`;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_visivel` varchar(255) DEFAULT NULL,
  `status_BOOLEAN` int(11) NOT NULL,
  `pagina_inicial` text,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo
-- ----------------------------
INSERT INTO `usuario_tipo` VALUES ('1', 'Usuário Administrador', 'Usuário Administrador', '1', '', '0', null);

-- ----------------------------
-- Table structure for `usuario_tipo_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_menu`;
CREATE TABLE `usuario_tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10438 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_menu
-- ----------------------------
INSERT INTO `usuario_tipo_menu` VALUES ('10400', '1', 'Clientes', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10401', '1', 'Cadastrar Cliente', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10402', '1', 'Visualizar Lista de Clientes', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10403', '1', 'Visualizar Cobranças Geradas', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10404', '1', 'Hospedagens', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10405', '1', 'Cadastrar Nova Hospedagem', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10406', '1', 'Visualizar Lista de Hospedagens', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10407', '1', 'Serviços', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10408', '1', 'Cadastrar Novo Serviço', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10409', '1', 'Visualizar Lista de Serviços', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10410', '1', 'Cadastros Gerais', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10411', '1', 'Empresas de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10412', '1', 'Cadastrar Empresa de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10413', '1', 'Gerenciar Empresas de Hosting', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10414', '1', 'Status dos Pagamentos', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10415', '1', 'Cadastrar Status', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10416', '1', 'Gerenciar Status', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10417', '1', 'Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10418', '1', 'Cadastrar Tipo de Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10419', '1', 'Gerenciar Tipos de Bancos de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10420', '1', 'Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10421', '1', 'Gerar Cobranças Agora', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10422', '1', 'Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10423', '1', 'Restaurar um Backup do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10424', '1', 'Fazer backup do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10425', '1', 'Gerenciar Rotinas de Backup Automático', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10426', '1', 'Fazer Limpeza do Banco de Dados', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10427', '1', 'Configurações de Envio', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10428', '1', 'Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10429', '1', 'Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10430', '1', 'Cadastrar Usuário do Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10431', '1', 'Gerenciar Usuários do Sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10432', '1', 'Visualizar Operações de Usuários no sistema', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10433', '1', 'Classes de Usuários', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10434', '1', 'Cadastrar Classe de Usuário', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10435', '1', 'Gerenciar Classes de Usuário', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10436', '1', 'Relatórios', '0', null);
INSERT INTO `usuario_tipo_menu` VALUES ('10437', '1', 'Fluxo de Caixa', '0', null);

-- ----------------------------
-- Table structure for `usuario_tipo_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_privilegio`;
CREATE TABLE `usuario_tipo_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  `excluido_BOOLEAN` int(1) NOT NULL DEFAULT '0',
  `excluido_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_privilegio
-- ----------------------------
