/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : omega_empresa_android_hmg

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-02-19 23:14:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `android_metadata`
-- ----------------------------
DROP TABLE IF EXISTS `android_metadata`;
CREATE TABLE `android_metadata` (
  `locale` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of android_metadata
-- ----------------------------
INSERT INTO `android_metadata` VALUES ('en_US');

-- ----------------------------
-- Table structure for `api`
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of api
-- ----------------------------

-- ----------------------------
-- Table structure for `api_id`
-- ----------------------------
DROP TABLE IF EXISTS `api_id`;
CREATE TABLE `api_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `chave` varchar(255) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `api_id_FK_814331055` (`pessoa_id_INT`),
  KEY `api_id_FK_701324463` (`usuario_id_INT`),
  KEY `api_id_FK_867980957` (`empresa_id_INT`),
  KEY `api_id_FK_830413819` (`corporacao_id_INT`),
  CONSTRAINT `api_id_FK_701324463` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `api_id_FK_814331055` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `api_id_FK_830413819` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `api_id_FK_867980957` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of api_id
-- ----------------------------

-- ----------------------------
-- Table structure for `app`
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `nome_normalizado` varchar(50) DEFAULT NULL,
  `data_lancamento_DATE` varchar(20) DEFAULT NULL,
  `link_download` varchar(512) DEFAULT NULL,
  `total_dia_trial_INT` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app
-- ----------------------------

-- ----------------------------
-- Table structure for `area_menu`
-- ----------------------------
DROP TABLE IF EXISTS `area_menu`;
CREATE TABLE `area_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(252) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of area_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade`
-- ----------------------------
DROP TABLE IF EXISTS `atividade`;
CREATE TABLE `atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `atividade_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_horas_INT` int(3) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_FK_389770508` (`empresa_id_INT`),
  KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`),
  KEY `atividade_FK_219207763` (`atividade_unidade_medida_id_INT`),
  KEY `atividade_FK_411010742` (`relatorio_id_INT`),
  CONSTRAINT `atividade_FK_219207763` FOREIGN KEY (`atividade_unidade_medida_id_INT`) REFERENCES `atividade_unidade_medida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_FK_411010742` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `atividade_tipo`;
CREATE TABLE `atividade_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `nome_normalizado` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_098787` (`nome`,`corporacao_id_INT`),
  KEY `empresa_servico_tipo_FK_604431152` (`corporacao_id_INT`),
  CONSTRAINT `atividade_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade_tipos`
-- ----------------------------
DROP TABLE IF EXISTS `atividade_tipos`;
CREATE TABLE `atividade_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atividade_tipo_id_INT` int(11) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `atividade_tipos_FK_423400879` (`atividade_tipo_id_INT`),
  KEY `atividade_tipos_FK_996185303` (`atividade_id_INT`),
  KEY `atividade_tipos_FK_789062500` (`corporacao_id_INT`),
  CONSTRAINT `atividade_tipos_FK_423400879` FOREIGN KEY (`atividade_tipo_id_INT`) REFERENCES `atividade_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_tipos_FK_789062500` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `atividade_tipos_FK_996185303` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade_tipos
-- ----------------------------

-- ----------------------------
-- Table structure for `atividade_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `atividade_unidade_medida`;
CREATE TABLE `atividade_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `atividade_unidade_medida_FK_833953858` (`corporacao_id_INT`),
  CONSTRAINT `atividade_unidade_medida_FK_833953858` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of atividade_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `bairro`
-- ----------------------------
DROP TABLE IF EXISTS `bairro`;
CREATE TABLE `bairro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome234234` (`nome_normalizado`,`cidade_id_INT`,`corporacao_id_INT`),
  KEY `b_cidade_FK` (`cidade_id_INT`),
  KEY `bairro_FK_545776367` (`corporacao_id_INT`),
  CONSTRAINT `bairro_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `bairro_ibfk_2` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bairro
-- ----------------------------

-- ----------------------------
-- Table structure for `categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `categoria_permissao`;
CREATE TABLE `categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_Aasd` (`nome_normalizado`,`corporacao_id_INT`),
  KEY `categoria_permissao_FK_780273438` (`corporacao_id_INT`),
  CONSTRAINT `categoria_permissao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `cidade`
-- ----------------------------
DROP TABLE IF EXISTS `cidade`;
CREATE TABLE `cidade` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `uf_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_736` (`nome_normalizado`,`uf_id_INT`,`corporacao_id_INT`),
  KEY `uf_id_INT` (`uf_id_INT`),
  KEY `cidade_FK_928863526` (`corporacao_id_INT`),
  CONSTRAINT `cidade_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `cidade_ibfk_2` FOREIGN KEY (`uf_id_INT`) REFERENCES `uf` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cidade
-- ----------------------------

-- ----------------------------
-- Table structure for `corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `corporacao`;
CREATE TABLE `corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `usuario_dropbox` varchar(255) DEFAULT NULL,
  `senha_dropbox` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporacao_nome_UNIQUE` (`nome_normalizado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `corporacao_sincronizador_php`
-- ----------------------------
DROP TABLE IF EXISTS `corporacao_sincronizador_php`;
CREATE TABLE `corporacao_sincronizador_php` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_id_sincronizador_php_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_6c8bdfc3dddcfb93` (`corporacao_id_INT`),
  CONSTRAINT `corporacao_sincronizador_php_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of corporacao_sincronizador_php
-- ----------------------------

-- ----------------------------
-- Table structure for `despesa`
-- ----------------------------
DROP TABLE IF EXISTS `despesa`;
CREATE TABLE `despesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_FLOAT` double DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `despesa_cotidiano_id_INT` int(11) DEFAULT NULL,
  `valor_pagamento_FLOAT` double DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `pagamento_usuario_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `despesa_FK_685638428` (`empresa_id_INT`),
  KEY `despesa_FK_939453125` (`despesa_cotidiano_id_INT`),
  KEY `despesa_FK_777130127` (`cadastro_usuario_id_INT`),
  KEY `despesa_FK_818298340` (`pagamento_usuario_id_INT`),
  KEY `despesa_FK_352508545` (`corporacao_id_INT`),
  CONSTRAINT `despesa_FK_352508545` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_685638428` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_777130127` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_818298340` FOREIGN KEY (`pagamento_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_FK_939453125` FOREIGN KEY (`despesa_cotidiano_id_INT`) REFERENCES `despesa_cotidiano` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of despesa
-- ----------------------------

-- ----------------------------
-- Table structure for `despesa_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `despesa_cotidiano`;
CREATE TABLE `despesa_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `despesa_da_empresa_id_INT` int(11) DEFAULT NULL,
  `despesa_do_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `id_tipo_despesa_cotidiano_INT` int(3) DEFAULT NULL,
  `parametro_INT` int(11) DEFAULT NULL,
  `parametro_OFFSEC` int(10) DEFAULT NULL,
  `parametro_SEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `despesa_cotidiano_FK_33874511` (`despesa_da_empresa_id_INT`),
  KEY `despesa_cotidiano_FK_475311279` (`despesa_do_usuario_id_INT`),
  KEY `despesa_cotidiano_FK_201293945` (`cadastro_usuario_id_INT`),
  KEY `despesa_cotidiano_FK_755279541` (`corporacao_id_INT`),
  CONSTRAINT `despesa_cotidiano_FK_201293945` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_cotidiano_FK_33874511` FOREIGN KEY (`despesa_da_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_cotidiano_FK_475311279` FOREIGN KEY (`despesa_do_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `despesa_cotidiano_FK_755279541` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of despesa_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa`
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `telefone1` varchar(30) DEFAULT NULL,
  `telefone2` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `celular_sms` varchar(30) DEFAULT NULL,
  `operadora_id_INT` int(11) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tipo_documento_id_INT` int(11) DEFAULT NULL,
  `numero_documento` varchar(30) DEFAULT NULL,
  `tipo_empresa_id_INT` int(11) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro_id_INT` int(11) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `ind_email_valido_BOOLEAN` int(1) DEFAULT NULL,
  `ind_celular_valido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`corporacao_id_INT`),
  KEY `u_bairro_FK` (`bairro_id_INT`),
  KEY `u_cidade_FK` (`cidade_id_INT`),
  KEY `empresa_FK_222015381` (`corporacao_id_INT`),
  KEY `empresa_FK_261840820` (`tipo_documento_id_INT`),
  KEY `empresa_FK_354888916` (`tipo_empresa_id_INT`),
  KEY `empresa_FK_967346192` (`operadora_id_INT`),
  KEY `key_empr766968` (`id`),
  KEY `empresa_FK_690795899` (`relatorio_id_INT`) USING BTREE,
  CONSTRAINT `empresa_FK_690795899` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_2` FOREIGN KEY (`operadora_id_INT`) REFERENCES `operadora` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_3` FOREIGN KEY (`tipo_documento_id_INT`) REFERENCES `tipo_documento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_4` FOREIGN KEY (`tipo_empresa_id_INT`) REFERENCES `tipo_empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_5` FOREIGN KEY (`bairro_id_INT`) REFERENCES `bairro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_ibfk_6` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_atividade`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_atividade`;
CREATE TABLE `empresa_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `prazo_entrega_dia_INT` int(11) DEFAULT NULL,
  `duracao_horas_INT` int(3) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `preco_venda_FLOAT` double DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_FK_389770508` (`empresa_id_INT`),
  KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`),
  KEY `empresa_atividade_FK_124084472` (`atividade_id_INT`),
  CONSTRAINT `empresa_atividade_FK_124084472` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_atividade
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_atividade_compra`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_atividade_compra`;
CREATE TABLE `empresa_atividade_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_compra_FK_892700196` (`empresa_compra_id_INT`),
  KEY `empresa_servico_compra_FK_118560791` (`corporacao_id_INT`),
  KEY `empresa_atividade_compra_FK_668334961` (`atividade_id_INT`),
  KEY `empresa_atividade_compra_FK_537750244` (`empresa_id_INT`),
  CONSTRAINT `empresa_atividade_compra_FK_537750244` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_compra_FK_668334961` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_compra_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_compra_ibfk_3` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_atividade_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_atividade_venda`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_atividade_venda`;
CREATE TABLE `empresa_atividade_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_servico_venda_FK_937438965` (`empresa_venda_id_INT`),
  KEY `empresa_servico_venda_FK_338836670` (`corporacao_id_INT`),
  KEY `empresa_atividade_venda_FK_500762939` (`atividade_id_INT`),
  KEY `empresa_atividade_venda_FK_194274902` (`empresa_id_INT`),
  CONSTRAINT `empresa_atividade_venda_FK_194274902` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_venda_FK_500762939` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_venda_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_atividade_venda_ibfk_3` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_atividade_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_compra`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_compra`;
CREATE TABLE `empresa_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `minha_empresa_id_INT` int(11) DEFAULT NULL,
  `outra_empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `fechamento_SEC` int(10) DEFAULT NULL,
  `fechamento_OFFSEC` int(6) DEFAULT NULL,
  `valor_pago_FLOAT` double DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_compra_FK_903930664` (`forma_pagamento_id_INT`),
  KEY `empresa_compra_FK_592681885` (`usuario_id_INT`),
  KEY `empresa_compra_FK_180664062` (`corporacao_id_INT`),
  KEY `minha_empresa_id_INT` (`minha_empresa_id_INT`),
  KEY `outra_empresa_id_INT` (`outra_empresa_id_INT`),
  KEY `empresa_compra_FK_584411621` (`relatorio_id_INT`) USING BTREE,
  KEY `empresa_compra_FK_608856201` (`registro_estado_id_INT`) USING BTREE,
  KEY `empresa_compra_FK_382690430` (`registro_estado_corporacao_id_INT`) USING BTREE,
  CONSTRAINT `empresa_compra_FK_382690430` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_FK_584411621` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_FK_608856201` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_3` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_5` FOREIGN KEY (`forma_pagamento_id_INT`) REFERENCES `forma_pagamento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_6` FOREIGN KEY (`minha_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_compra_ibfk_7` FOREIGN KEY (`outra_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_compra_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_compra_parcela`;
CREATE TABLE `empresa_compra_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `valor_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdhgfgh` (`empresa_compra_id_INT`),
  KEY `empresa_compra_parcela_FK_238128662` (`corporacao_id_INT`),
  CONSTRAINT `empresa_compra_parcela_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sdhgfgh` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_compra_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_equipe`;
CREATE TABLE `empresa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_equipe_FK_652496338` (`empresa_id_INT`) USING BTREE,
  KEY `empresa_equipe_FK_545715332` (`corporacao_id_INT`) USING BTREE,
  CONSTRAINT `empresa_equipe_FK_545715332` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_equipe_FK_652496338` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_perfil`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_perfil`;
CREATE TABLE `empresa_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `perfil_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `empresa_id_INT` (`empresa_id_INT`,`perfil_id_INT`,`corporacao_id_INT`),
  KEY `empresa_perfil_FK_569122315` (`empresa_id_INT`),
  KEY `empresa_perfil_FK_645446777` (`perfil_id_INT`),
  KEY `empresa_perfil_FK_11688232` (`corporacao_id_INT`),
  CONSTRAINT `empresa_perfil_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_perfil_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_perfil_ibfk_3` FOREIGN KEY (`perfil_id_INT`) REFERENCES `perfil` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_perfil
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_produto`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_produto`;
CREATE TABLE `empresa_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `preco_venda_FLOAT` double DEFAULT NULL,
  `estoque_atual_INT` int(11) DEFAULT NULL,
  `estoque_minimo_INT` int(11) DEFAULT NULL,
  `prazo_reposicao_estoque_dias_INT` int(6) DEFAULT NULL,
  `codigo_barra` char(13) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_FK_453643799` (`empresa_id_INT`),
  KEY `empresa_produto_FK_183288574` (`corporacao_id_INT`),
  KEY `empresa_produto_FK_384552002` (`produto_id_INT`) USING BTREE,
  CONSTRAINT `empresa_produto_FK_384552002` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_ibfk_4` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_produto
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_produto_compra`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_produto_compra`;
CREATE TABLE `empresa_produto_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_compra_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_compra_FK_163604736` (`empresa_compra_id_INT`),
  KEY `empresa_produto_compra_FK_69030761` (`corporacao_id_INT`),
  KEY `empresa_produto_compra_FK_968383790` (`produto_id_INT`) USING BTREE,
  KEY `empresa_produto_compra_FK_618072510` (`empresa_id_INT`) USING BTREE,
  CONSTRAINT `empresa_produto_compra_FK_618072510` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_compra_FK_968383790` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_compra_ibfk_1` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_compra_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_produto_compra
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_produto_venda`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_produto_venda`;
CREATE TABLE `empresa_produto_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `quantidade_FLOAT` double NOT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_venda_FK_890716553` (`empresa_venda_id_INT`),
  KEY `empresa_produto_venda_FK_292968750` (`corporacao_id_INT`),
  KEY `empresa_produto_venda_FK_420928955` (`produto_id_INT`) USING BTREE,
  KEY `empresa_produto_venda_FK_367370605` (`empresa_id_INT`) USING BTREE,
  CONSTRAINT `empresa_produto_venda_FK_367370605` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_venda_FK_420928955` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_venda_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_produto_venda_ibfk_3` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_produto_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_venda`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_venda`;
CREATE TABLE `empresa_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento_id_INT` int(11) DEFAULT NULL,
  `valor_total_FLOAT` double NOT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `cliente_empresa_id_INT` int(11) DEFAULT NULL,
  `fornecedor_empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `desconto_FLOAT` double DEFAULT NULL,
  `acrescimo_FLOAT` double DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `vencimento_SEC` int(10) DEFAULT NULL,
  `vencimento_OFFSEC` int(6) DEFAULT NULL,
  `fechamento_SEC` int(10) DEFAULT NULL,
  `fechamento_OFFSEC` int(6) DEFAULT NULL,
  `valor_pago_FLOAT` double DEFAULT NULL,
  `mesa_id_INT` int(11) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_empr83221` (`forma_pagamento_id_INT`),
  KEY `key_empr213501` (`usuario_id_INT`),
  KEY `key_empr835846` (`cliente_empresa_id_INT`),
  KEY `key_empr655823` (`fornecedor_empresa_id_INT`),
  KEY `key_empr330170` (`pessoa_id_INT`),
  KEY `key_empr193359` (`corporacao_id_INT`),
  KEY `empresa_venda_FK_985656739` (`registro_estado_id_INT`) USING BTREE,
  KEY `empresa_venda_FK_407073975` (`registro_estado_corporacao_id_INT`) USING BTREE,
  KEY `empresa_venda_FK_402862549` (`relatorio_id_INT`) USING BTREE,
  KEY `empresa_venda_FK_648132324` (`mesa_id_INT`) USING BTREE,
  CONSTRAINT `empresa_venda_FK_402862549` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_FK_407073975` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_FK_648132324` FOREIGN KEY (`mesa_id_INT`) REFERENCES `mesa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_FK_985656739` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_1` FOREIGN KEY (`forma_pagamento_id_INT`) REFERENCES `forma_pagamento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_3` FOREIGN KEY (`cliente_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_5` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_6` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_ibfk_7` FOREIGN KEY (`fornecedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_venda_parcela`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_venda_parcela`;
CREATE TABLE `empresa_venda_parcela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `valor_FLOAT` double NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `seq_INT` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_venda_parcela_FK_200561523` (`empresa_venda_id_INT`),
  KEY `empresa_venda_parcela_FK_50445556` (`corporacao_id_INT`),
  CONSTRAINT `empresa_venda_parcela_ibfk_1` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_parcela_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_venda_parcela
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa_venda_template`
-- ----------------------------
DROP TABLE IF EXISTS `empresa_venda_template`;
CREATE TABLE `empresa_venda_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `template_json` varchar(512) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_venda_template_FK_418518066` (`produto_id_INT`),
  KEY `empresa_venda_template_FK_963958741` (`empresa_id_INT`),
  KEY `empresa_venda_template_FK_776367188` (`corporacao_id_INT`),
  CONSTRAINT `empresa_venda_template_FK_418518066` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_template_FK_776367188` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `empresa_venda_template_FK_963958741` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa_venda_template
-- ----------------------------

-- ----------------------------
-- Table structure for `estado_civil`
-- ----------------------------
DROP TABLE IF EXISTS `estado_civil`;
CREATE TABLE `estado_civil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_63545` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_civil
-- ----------------------------

-- ----------------------------
-- Table structure for `forma_pagamento`
-- ----------------------------
DROP TABLE IF EXISTS `forma_pagamento`;
CREATE TABLE `forma_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `forma_pagamento_FK_734954834` (`corporacao_id_INT`),
  CONSTRAINT `forma_pagamento_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of forma_pagamento
-- ----------------------------

-- ----------------------------
-- Table structure for `mensagem`
-- ----------------------------
DROP TABLE IF EXISTS `mensagem`;
CREATE TABLE `mensagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `mensagem` varchar(512) DEFAULT NULL,
  `remetente_usuario_id_INT` int(11) DEFAULT NULL,
  `tipo_mensagem_id_INT` int(11) DEFAULT NULL,
  `destinatario_usuario_id_INT` int(11) DEFAULT NULL,
  `destinatario_pessoa_id_INT` int(11) DEFAULT NULL,
  `destinatario_empresa_id_INT` int(11) DEFAULT NULL,
  `data_min_envio_SEC` int(10) DEFAULT NULL,
  `data_min_envio_OFFSEC` int(6) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `empresa_para_cliente_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mensagem_FK_646789551` (`remetente_usuario_id_INT`),
  KEY `mensagem_FK_970550538` (`tipo_mensagem_id_INT`),
  KEY `mensagem_FK_119873046` (`destinatario_usuario_id_INT`),
  KEY `mensagem_FK_67901611` (`destinatario_pessoa_id_INT`),
  KEY `mensagem_FK_637390137` (`destinatario_empresa_id_INT`),
  KEY `mensagem_FK_258514404` (`registro_estado_id_INT`),
  KEY `mensagem_FK_570434570` (`corporacao_id_INT`),
  CONSTRAINT `mensagem_FK_119873046` FOREIGN KEY (`destinatario_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_258514404` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_570434570` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_637390137` FOREIGN KEY (`destinatario_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_646789551` FOREIGN KEY (`remetente_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_67901611` FOREIGN KEY (`destinatario_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_FK_970550538` FOREIGN KEY (`tipo_mensagem_id_INT`) REFERENCES `mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for `mensagem_envio`
-- ----------------------------
DROP TABLE IF EXISTS `mensagem_envio`;
CREATE TABLE `mensagem_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_canal_envio_id_INT` int(11) DEFAULT NULL,
  `mensagem_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `obs` varchar(100) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT NULL,
  `tentativa_INT` int(2) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mensagem_envio_FK_703460694` (`tipo_canal_envio_id_INT`),
  KEY `mensagem_envio_FK_757080078` (`mensagem_id_INT`),
  KEY `mensagem_envio_FK_273468017` (`registro_estado_id_INT`),
  KEY `mensagem_envio_FK_794128418` (`corporacao_id_INT`),
  CONSTRAINT `mensagem_envio_FK_273468017` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_envio_FK_703460694` FOREIGN KEY (`tipo_canal_envio_id_INT`) REFERENCES `tipo_canal_envio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_envio_FK_757080078` FOREIGN KEY (`mensagem_id_INT`) REFERENCES `mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mensagem_envio_FK_794128418` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mensagem_envio
-- ----------------------------

-- ----------------------------
-- Table structure for `mesa`
-- ----------------------------
DROP TABLE IF EXISTS `mesa`;
CREATE TABLE `mesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `template_json` varchar(255) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mesa_FK_462615967` (`empresa_id_INT`),
  KEY `mesa_FK_317504883` (`corporacao_id_INT`),
  CONSTRAINT `mesa_FK_317504883` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_FK_462615967` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mesa
-- ----------------------------

-- ----------------------------
-- Table structure for `mesa_reserva`
-- ----------------------------
DROP TABLE IF EXISTS `mesa_reserva`;
CREATE TABLE `mesa_reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mesa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `reserva_SEC` int(10) DEFAULT NULL,
  `reserva_OFFSEC` int(6) DEFAULT NULL,
  `confimado_SEC` int(10) DEFAULT NULL,
  `confirmado_OFFSEC` int(6) DEFAULT NULL,
  `confirmador_usuario_id_INT` int(11) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mesa_reserva_FK_235290527` (`mesa_id_INT`),
  KEY `mesa_reserva_FK_304656982` (`pessoa_id_INT`),
  KEY `mesa_reserva_FK_974975586` (`confirmador_usuario_id_INT`),
  KEY `mesa_reserva_FK_961578370` (`corporacao_id_INT`),
  CONSTRAINT `mesa_reserva_FK_235290527` FOREIGN KEY (`mesa_id_INT`) REFERENCES `mesa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_reserva_FK_304656982` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_reserva_FK_961578370` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mesa_reserva_FK_974975586` FOREIGN KEY (`confirmador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mesa_reserva
-- ----------------------------

-- ----------------------------
-- Table structure for `modelo`
-- ----------------------------
DROP TABLE IF EXISTS `modelo`;
CREATE TABLE `modelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `ano_INT` int(4) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome_normalizado`,`ano_INT`,`corporacao_id_INT`),
  KEY `modelo_ibfk_1` (`corporacao_id_INT`),
  CONSTRAINT `modelo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modelo
-- ----------------------------

-- ----------------------------
-- Table structure for `negociacao_divida`
-- ----------------------------
DROP TABLE IF EXISTS `negociacao_divida`;
CREATE TABLE `negociacao_divida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `devedor_empresa_id_INT` int(11) DEFAULT NULL,
  `devedor_pessoa_id_INT` int(11) DEFAULT NULL,
  `negociador_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `resultado_empresa_venda_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `negociacao_divida_FK_620697022` (`devedor_empresa_id_INT`),
  KEY `negociacao_divida_FK_110839843` (`devedor_pessoa_id_INT`),
  KEY `negociacao_divida_FK_708282471` (`negociador_usuario_id_INT`),
  KEY `negociacao_divida_FK_563903809` (`resultado_empresa_venda_id_INT`),
  KEY `negociacao_divida_FK_873504639` (`relatorio_id_INT`),
  KEY `negociacao_divida_FK_729370117` (`registro_estado_id_INT`),
  KEY `negociacao_divida_FK_940582276` (`corporacao_id_INT`),
  CONSTRAINT `negociacao_divida_FK_110839843` FOREIGN KEY (`devedor_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_563903809` FOREIGN KEY (`resultado_empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_620697022` FOREIGN KEY (`devedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_708282471` FOREIGN KEY (`negociador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_729370117` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_873504639` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_FK_940582276` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of negociacao_divida
-- ----------------------------

-- ----------------------------
-- Table structure for `negociacao_divida_empresa_venda`
-- ----------------------------
DROP TABLE IF EXISTS `negociacao_divida_empresa_venda`;
CREATE TABLE `negociacao_divida_empresa_venda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_venda_protocolo_INT` bigint(11) DEFAULT NULL,
  `negociacao_divida_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `negociacao_divida_empresa_venda_FK_449615478` (`negociacao_divida_id_INT`),
  KEY `negociacao_divida_empresa_venda_FK_306701660` (`corporacao_id_INT`),
  CONSTRAINT `negociacao_divida_empresa_venda_FK_306701660` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `negociacao_divida_empresa_venda_FK_449615478` FOREIGN KEY (`negociacao_divida_id_INT`) REFERENCES `negociacao_divida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of negociacao_divida_empresa_venda
-- ----------------------------

-- ----------------------------
-- Table structure for `nota`
-- ----------------------------
DROP TABLE IF EXISTS `nota`;
CREATE TABLE `nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nota_FK_922271729` (`empresa_id_INT`),
  KEY `nota_FK_826232910` (`pessoa_id_INT`),
  KEY `nota_FK_654876709` (`usuario_id_INT`),
  KEY `nota_FK_336425781` (`veiculo_id_INT`),
  KEY `nota_FK_797149659` (`relatorio_id_INT`),
  KEY `nota_FK_750427246` (`corporacao_id_INT`),
  CONSTRAINT `nota_FK_336425781` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_654876709` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_750427246` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_797149659` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_826232910` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_FK_922271729` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nota
-- ----------------------------

-- ----------------------------
-- Table structure for `nota_tipo_nota`
-- ----------------------------
DROP TABLE IF EXISTS `nota_tipo_nota`;
CREATE TABLE `nota_tipo_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota_id_INT` int(11) DEFAULT NULL,
  `tipo_nota_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nota_id_INT` (`nota_id_INT`,`tipo_nota_id_INT`,`corporacao_id_INT`),
  KEY `nota_tipo_nota_FK_277954101` (`tipo_nota_id_INT`),
  KEY `nota_tipo_nota_FK_861236573` (`corporacao_id_INT`),
  CONSTRAINT `nota_tipo_nota_FK_277954101` FOREIGN KEY (`tipo_nota_id_INT`) REFERENCES `tipo_nota` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_tipo_nota_FK_861236573` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nota_tipo_nota_FK_92987060` FOREIGN KEY (`nota_id_INT`) REFERENCES `nota` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nota_tipo_nota
-- ----------------------------

-- ----------------------------
-- Table structure for `operadora`
-- ----------------------------
DROP TABLE IF EXISTS `operadora`;
CREATE TABLE `operadora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `nome_normalizado` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_operadora_UNIQUE` (`nome_normalizado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operadora
-- ----------------------------

-- ----------------------------
-- Table structure for `pais`
-- ----------------------------
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_83746` (`nome_normalizado`,`corporacao_id_INT`),
  KEY `pais_FK_330535889` (`corporacao_id_INT`),
  CONSTRAINT `pais_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pais
-- ----------------------------

-- ----------------------------
-- Table structure for `perfil`
-- ----------------------------
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_perfil_UNIQUE` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of perfil
-- ----------------------------

-- ----------------------------
-- Table structure for `permissao`
-- ----------------------------
DROP TABLE IF EXISTS `permissao`;
CREATE TABLE `permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `pai_permissao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`),
  KEY `permissao_fk` (`pai_permissao_id_INT`),
  CONSTRAINT `permissao_ibfk_1` FOREIGN KEY (`pai_permissao_id_INT`) REFERENCES `permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `permissao_categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `permissao_categoria_permissao`;
CREATE TABLE `permissao_categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissao_id_INT` int(11) NOT NULL,
  `categoria_permissao_id_INT` int(11) NOT NULL,
  `corporacao_id_INT` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria_permissao_id_INT` (`categoria_permissao_id_INT`) USING BTREE,
  KEY `corporacao_id_INT` (`corporacao_id_INT`) USING BTREE,
  KEY `permissao_id_INT` (`permissao_id_INT`) USING BTREE,
  CONSTRAINT `permissao_categoria_permissao_ibfk_4` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissao_categoria_permissao_ibfk_7` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissao_categoria_permissao_ibfk_9` FOREIGN KEY (`permissao_id_INT`) REFERENCES `permissao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissao_categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa`;
CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `tipo_documento_id_INT` int(11) DEFAULT NULL,
  `numero_documento` varchar(30) DEFAULT NULL,
  `is_consumidor_BOOLEAN` int(1) DEFAULT NULL,
  `sexo_id_INT` int(11) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `celular_sms` varchar(30) DEFAULT NULL,
  `operadora_id_INT` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `bairro_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `data_nascimento_SEC` int(10) DEFAULT NULL,
  `data_nascimento_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `ind_email_valido_BOOLEAN` int(1) DEFAULT NULL,
  `ind_celular_valido_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_95867` (`email`,`corporacao_id_INT`),
  KEY `e_bairro_FK` (`bairro_id_INT`),
  KEY `e_cidade_FK` (`cidade_id_INT`),
  KEY `funcionario_sexo_FK` (`sexo_id_INT`),
  KEY `pessoa_FK_169738769` (`corporacao_id_INT`),
  KEY `pessoa_FK_224273681` (`tipo_documento_id_INT`),
  KEY `pessoa_FK_5187988` (`operadora_id_INT`),
  KEY `pessoa_FK_594970703` (`relatorio_id_INT`) USING BTREE,
  CONSTRAINT `pessoa_FK_594970703` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_2` FOREIGN KEY (`tipo_documento_id_INT`) REFERENCES `tipo_documento` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_3` FOREIGN KEY (`sexo_id_INT`) REFERENCES `sexo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_4` FOREIGN KEY (`operadora_id_INT`) REFERENCES `operadora` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_5` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_ibfk_6` FOREIGN KEY (`bairro_id_INT`) REFERENCES `bairro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_empresa`;
CREATE TABLE `pessoa_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `profissao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pessoa_empresa_UNIQUE` (`pessoa_id_INT`,`empresa_id_INT`,`profissao_id_INT`,`corporacao_id_INT`),
  KEY `empresa_id_INT` (`empresa_id_INT`),
  KEY `fe_profissao_FK` (`profissao_id_INT`),
  KEY `pessoa_empresa_FK_41381835` (`pessoa_id_INT`),
  KEY `pessoa_empresa_FK_199859619` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_ibfk_2` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_ibfk_3` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_ibfk_4` FOREIGN KEY (`profissao_id_INT`) REFERENCES `profissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_empresa_rotina`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_empresa_rotina`;
CREATE TABLE `pessoa_empresa_rotina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_empresa_id_INT` int(11) DEFAULT NULL,
  `semana_INT` int(4) NOT NULL,
  `dia_semana_INT` int(4) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pessoa_empresa_rotina_FK_188354492` (`pessoa_empresa_id_INT`),
  KEY `pessoa_empresa_rotina_FK_969879151` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_empresa_rotina_ibfk_1` FOREIGN KEY (`pessoa_empresa_id_INT`) REFERENCES `pessoa_empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_empresa_rotina_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_empresa_rotina
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_equipe`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_equipe`;
CREATE TABLE `pessoa_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pessoa_equipe_FK_929412842` (`empresa_equipe_id_INT`),
  KEY `pessoa_equipe_FK_198364258` (`usuario_id_INT`),
  KEY `pessoa_equipe_FK_935943604` (`pessoa_id_INT`),
  KEY `pessoa_equipe_FK_316467285` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_equipe_FK_198364258` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_equipe_FK_316467285` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_equipe_FK_929412842` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_equipe_FK_935943604` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_equipe
-- ----------------------------

-- ----------------------------
-- Table structure for `pessoa_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `pessoa_usuario`;
CREATE TABLE `pessoa_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pessoa_usuario_UNIQUE` (`pessoa_id_INT`,`usuario_id_INT`,`corporacao_id_INT`),
  KEY `pessoa_usuario_FK_347076416` (`pessoa_id_INT`),
  KEY `pessoa_usuario_FK_629943848` (`usuario_id_INT`),
  KEY `pessoa_usuario_FK_759368897` (`corporacao_id_INT`),
  CONSTRAINT `pessoa_usuario_ibfk_1` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_usuario_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pessoa_usuario_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pessoa_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `ponto`
-- ----------------------------
DROP TABLE IF EXISTS `ponto`;
CREATE TABLE `ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `profissao_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `is_entrada_BOOLEAN` int(1) NOT NULL,
  `tipo_ponto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ponto_FK_228240967` (`corporacao_id_INT`),
  KEY `ponto_FK_329772949` (`empresa_id_INT`),
  KEY `ponto_FK_160339355` (`profissao_id_INT`),
  KEY `ponto_FK_509979248` (`usuario_id_INT`),
  KEY `ponto_FK_1234567` (`tipo_ponto_id_INT`),
  KEY `pessoa_id_INT` (`pessoa_id_INT`),
  CONSTRAINT `ponto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_3` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_4` FOREIGN KEY (`profissao_id_INT`) REFERENCES `profissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_5` FOREIGN KEY (`tipo_ponto_id_INT`) REFERENCES `tipo_ponto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ponto_ibfk_6` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ponto
-- ----------------------------

-- ----------------------------
-- Table structure for `produto`
-- ----------------------------
DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(30) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `produto_unidade_medida_id_INT` int(11) DEFAULT NULL,
  `id_omega_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `preco_custo_FLOAT` double DEFAULT NULL,
  `prazo_reposicao_estoque_dias_INT` int(6) DEFAULT NULL,
  `codigo_barra` char(13) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_produto_FK_183288574` (`corporacao_id_INT`),
  KEY `produto_FK_485992432` (`produto_unidade_medida_id_INT`),
  CONSTRAINT `produto_FK_485992432` FOREIGN KEY (`produto_unidade_medida_id_INT`) REFERENCES `produto_unidade_medida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `produto_tipo`;
CREATE TABLE `produto_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_98765` (`nome`,`corporacao_id_INT`),
  KEY `empresa_produto_tipo_FK_926544190` (`corporacao_id_INT`),
  CONSTRAINT `produto_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_tipos`
-- ----------------------------
DROP TABLE IF EXISTS `produto_tipos`;
CREATE TABLE `produto_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_tipo_id_INT` int(11) DEFAULT NULL,
  `produto_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_tipos_FK_90423584` (`produto_tipo_id_INT`),
  KEY `produto_tipos_FK_811035157` (`produto_id_INT`),
  KEY `produto_tipos_FK_529571533` (`corporacao_id_INT`),
  CONSTRAINT `produto_tipos_FK_529571533` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `produto_tipos_FK_811035157` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `produto_tipos_FK_90423584` FOREIGN KEY (`produto_tipo_id_INT`) REFERENCES `produto_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto_tipos
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_unidade_medida`
-- ----------------------------
DROP TABLE IF EXISTS `produto_unidade_medida`;
CREATE TABLE `produto_unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `abreviacao` varchar(10) NOT NULL,
  `is_float_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produto_unidade_medida
-- ----------------------------

-- ----------------------------
-- Table structure for `profissao`
-- ----------------------------
DROP TABLE IF EXISTS `profissao`;
CREATE TABLE `profissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_34677654` (`nome_normalizado`,`corporacao_id_INT`),
  KEY `profissao_FK_779541016` (`corporacao_id_INT`),
  CONSTRAINT `profissao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of profissao
-- ----------------------------

-- ----------------------------
-- Table structure for `recebivel`
-- ----------------------------
DROP TABLE IF EXISTS `recebivel`;
CREATE TABLE `recebivel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_FLOAT` double DEFAULT NULL,
  `devedor_empresa_id_INT` int(11) DEFAULT NULL,
  `devedor_pessoa_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `valor_pagamento_FLOAT` double DEFAULT NULL,
  `pagamento_SEC` int(10) DEFAULT NULL,
  `pagamento_OFFSEC` int(6) DEFAULT NULL,
  `recebedor_usuario_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `protocolo_INT` bigint(20) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recebivel_FK_239410400` (`devedor_empresa_id_INT`),
  KEY `recebivel_FK_19348144` (`devedor_pessoa_id_INT`),
  KEY `recebivel_FK_829437256` (`cadastro_usuario_id_INT`),
  KEY `recebivel_FK_988525391` (`recebedor_usuario_id_INT`),
  KEY `recebivel_FK_1007080` (`relatorio_id_INT`),
  KEY `recebivel_FK_595886231` (`corporacao_id_INT`),
  CONSTRAINT `recebivel_FK_1007080` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_19348144` FOREIGN KEY (`devedor_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_239410400` FOREIGN KEY (`devedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_595886231` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_829437256` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_FK_988525391` FOREIGN KEY (`recebedor_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recebivel
-- ----------------------------

-- ----------------------------
-- Table structure for `recebivel_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `recebivel_cotidiano`;
CREATE TABLE `recebivel_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recebedor_empresa_id_INT` int(11) DEFAULT NULL,
  `cadastro_usuario_id_INT` int(11) DEFAULT NULL,
  `id_tipo_despesa_recebivel_INT` int(3) DEFAULT NULL,
  `parametro_INT` int(11) DEFAULT NULL,
  `parametro_OFFSEC` int(10) DEFAULT NULL,
  `parametro_SEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recebivel_cotidiano_FK_447082519` (`recebedor_empresa_id_INT`),
  KEY `recebivel_cotidiano_FK_452484131` (`cadastro_usuario_id_INT`),
  KEY `recebivel_cotidiano_FK_900634766` (`corporacao_id_INT`),
  CONSTRAINT `recebivel_cotidiano_FK_447082519` FOREIGN KEY (`recebedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_cotidiano_FK_452484131` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `recebivel_cotidiano_FK_900634766` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recebivel_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `rede`
-- ----------------------------
DROP TABLE IF EXISTS `rede`;
CREATE TABLE `rede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`,`corporacao_id_INT`),
  KEY `rede_FK_554260254` (`corporacao_id_INT`),
  KEY `rede_FK_704559326` (`relatorio_id_INT`) USING BTREE,
  CONSTRAINT `rede_FK_704559326` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rede_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rede
-- ----------------------------

-- ----------------------------
-- Table structure for `rede_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `rede_empresa`;
CREATE TABLE `rede_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rede_id_INT` int(11) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rede_empresa_UNIQUE` (`rede_id_INT`,`empresa_id_INT`),
  KEY `rede_empresa_FK_472778320` (`corporacao_id_INT`),
  KEY `key_rede788727` (`empresa_id_INT`),
  KEY `rede_id_INT` (`rede_id_INT`),
  CONSTRAINT `rede_empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rede_empresa_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rede_empresa_ibfk_3` FOREIGN KEY (`rede_id_INT`) REFERENCES `rede` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rede_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `registro`
-- ----------------------------
DROP TABLE IF EXISTS `registro`;
CREATE TABLE `registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocolo_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `id_origem_chamada_INT` int(3) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_FK_991607667` (`responsavel_usuario_id_INT`),
  KEY `registro_FK_883850098` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_FK_435150146` (`tipo_registro_id_INT`),
  KEY `registro_FK_886230469` (`registro_estado_id_INT`),
  KEY `registro_FK_225860595` (`registro_estado_corporacao_id_INT`),
  KEY `registro_FK_979919434` (`corporacao_id_INT`),
  CONSTRAINT `registro_FK_225860595` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_435150146` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_883850098` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_886230469` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_979919434` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_FK_991607667` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro
-- ----------------------------

-- ----------------------------
-- Table structure for `registro_estado`
-- ----------------------------
DROP TABLE IF EXISTS `registro_estado`;
CREATE TABLE `registro_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `prazo_dias_INT` int(3) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_estado_FK_27282714` (`responsavel_usuario_id_INT`),
  KEY `registro_estado_FK_131805420` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_estado_FK_634155274` (`tipo_registro_id_INT`),
  CONSTRAINT `registro_estado_FK_131805420` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_FK_27282714` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_FK_634155274` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro_estado
-- ----------------------------

-- ----------------------------
-- Table structure for `registro_estado_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `registro_estado_corporacao`;
CREATE TABLE `registro_estado_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `responsavel_usuario_id_INT` int(11) DEFAULT NULL,
  `responsavel_categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `prazo_dias_INT` int(3) DEFAULT NULL,
  `tipo_registro_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_estado_corporacao_FK_488037109` (`responsavel_usuario_id_INT`),
  KEY `registro_estado_corporacao_FK_697784424` (`responsavel_categoria_permissao_id_INT`),
  KEY `registro_estado_corporacao_FK_794616700` (`tipo_registro_id_INT`),
  KEY `registro_estado_corporacao_FK_896209717` (`corporacao_id_INT`),
  CONSTRAINT `registro_estado_corporacao_FK_488037109` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_corporacao_FK_697784424` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_corporacao_FK_794616700` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_estado_corporacao_FK_896209717` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro_estado_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `registro_fluxo_estado_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `registro_fluxo_estado_corporacao`;
CREATE TABLE `registro_fluxo_estado_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origem_registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `origem_registro_estado_id_INT` int(11) DEFAULT NULL,
  `destino_registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `destino_registro_estado_id_INT` int(11) DEFAULT NULL,
  `template_JSON` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_fluxo_estado_corporacao_FK_25970459` (`origem_registro_estado_corporacao_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_785644532` (`origem_registro_estado_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_761993408` (`destino_registro_estado_corporacao_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_918395997` (`destino_registro_estado_id_INT`),
  KEY `registro_fluxo_estado_corporacao_FK_13153076` (`corporacao_id_INT`),
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_13153076` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_25970459` FOREIGN KEY (`origem_registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_761993408` FOREIGN KEY (`destino_registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_785644532` FOREIGN KEY (`origem_registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `registro_fluxo_estado_corporacao_FK_918395997` FOREIGN KEY (`destino_registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of registro_fluxo_estado_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio`;
CREATE TABLE `relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `tipo_relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relatorio_FK_445983887` (`usuario_id_INT`),
  KEY `relatorio_FK_395233154` (`corporacao_id_INT`),
  KEY `relatorio_FK_984588624` (`tipo_relatorio_id_INT`) USING BTREE,
  CONSTRAINT `relatorio_FK_984588624` FOREIGN KEY (`tipo_relatorio_id_INT`) REFERENCES `tipo_relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `relatorio_anexo`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio_anexo`;
CREATE TABLE `relatorio_anexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `arquivo` varchar(50) DEFAULT NULL,
  `tipo_anexo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relatorio_anexo_FK_268829345` (`relatorio_id_INT`),
  KEY `relatorio_anexo_FK_663513184` (`tipo_anexo_id_INT`),
  KEY `relatorio_anexo_FK_730926514` (`corporacao_id_INT`),
  CONSTRAINT `relatorio_anexo_ibfk_1` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_anexo_ibfk_2` FOREIGN KEY (`tipo_anexo_id_INT`) REFERENCES `tipo_anexo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `relatorio_anexo_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio_anexo
-- ----------------------------

-- ----------------------------
-- Table structure for `rotina`
-- ----------------------------
DROP TABLE IF EXISTS `rotina`;
CREATE TABLE `rotina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ultima_dia_contagem_ciclo_DATE` date NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `ultima_dia_contagem_ciclo_data_SEC` int(10) DEFAULT NULL,
  `ultima_dia_contagem_ciclo_data_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rotina_FK_259857177` (`corporacao_id_INT`),
  CONSTRAINT `rotina_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rotina
-- ----------------------------

-- ----------------------------
-- Table structure for `servico`
-- ----------------------------
DROP TABLE IF EXISTS `servico`;
CREATE TABLE `servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of servico
-- ----------------------------

-- ----------------------------
-- Table structure for `sexo`
-- ----------------------------
DROP TABLE IF EXISTS `sexo`;
CREATE TABLE `sexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sexo
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_aresta_grafo_hierarquia_banco`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_aresta_grafo_hierarquia_banco`;
CREATE TABLE `sistema_aresta_grafo_hierarquia_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_sistema_grafo_hierarquia_banco_id_INT` int(11) DEFAULT NULL,
  `filho_sistema_grafo_hierarquia_banco_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_4448ceb723e91dae` (`pai_sistema_grafo_hierarquia_banco_id_INT`),
  KEY `key_6aa89b0c65a803a0` (`filho_sistema_grafo_hierarquia_banco_id_INT`),
  CONSTRAINT `sistema_aresta_grafo_hierarquia_banco_ibfk_1` FOREIGN KEY (`pai_sistema_grafo_hierarquia_banco_id_INT`) REFERENCES `sistema_grafo_hierarquia_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_aresta_grafo_hierarquia_banco_ibfk_2` FOREIGN KEY (`filho_sistema_grafo_hierarquia_banco_id_INT`) REFERENCES `sistema_grafo_hierarquia_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_aresta_grafo_hierarquia_banco
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_atributo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_atributo`;
CREATE TABLE `sistema_atributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `tipo_sql` varchar(15) DEFAULT NULL,
  `tamanho_INT` int(11) DEFAULT NULL,
  `decimal_INT` int(11) DEFAULT NULL,
  `not_null_BOOLEAN` int(1) DEFAULT NULL,
  `primary_key_BOOLEAN` int(1) DEFAULT NULL,
  `auto_increment_BOOLEAN` int(1) DEFAULT NULL,
  `valor_default` varchar(100) DEFAULT NULL,
  `fk_sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `atributo_fk` varchar(100) DEFAULT NULL,
  `fk_nome` varchar(255) DEFAULT NULL,
  `update_tipo_fk` varchar(30) DEFAULT NULL,
  `delete_tipo_fk` varchar(30) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `unique_BOOLEAN` int(1) DEFAULT NULL,
  `unique_nome` varchar(255) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `excluido_BOOLEAN` int(1) DEFAULT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  `tipo_sql_ficticio` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_irtu` (`nome`,`sistema_tabela_id_INT`),
  KEY `sa_sistema_tabela_FK` (`sistema_tabela_id_INT`),
  KEY `sistema_atributo_FK_622131348` (`fk_sistema_tabela_id_INT`),
  CONSTRAINT `sa_sistema_tabela_FK` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_atributo_FK_622131348` FOREIGN KEY (`fk_sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26286 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_atributo
-- ----------------------------
INSERT INTO `sistema_atributo` VALUES ('25809', 'id', '1', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25810', 'nome', '1', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25811', 'nome_normalizado', '1', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25812', 'cidade_id_INT', '1', '', '11', null, '1', '0', '0', '', '4', 'id', 'bairro_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25813', 'corporacao_id_INT', '1', '', '11', null, '1', '0', '0', '', '49', 'id', 'bairro_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25814', 'id', '2', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25815', 'nome', '2', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25816', 'nome_normalizado', '2', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25817', 'corporacao_id_INT', '2', '', '11', null, '1', '0', '0', '', '49', 'id', 'categoria_permissao_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25818', 'id', '4', '', '3', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25819', 'nome', '4', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25820', 'nome_normalizado', '4', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25821', 'uf_id_INT', '4', '', '11', null, '1', '0', '0', '', '13', 'id', 'cidade_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25822', 'corporacao_id_INT', '4', '', '11', null, '1', '0', '0', '', '49', 'id', 'cidade_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25827', 'id', '49', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25828', 'nome', '49', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25829', 'nome_normalizado', '49', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'corporacao_nome_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25830', 'usuario_dropbox', '49', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25831', 'senha_dropbox', '49', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25832', 'id', '5', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25833', 'nome', '5', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25834', 'nome_normalizado', '5', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25835', 'telefone1', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25836', 'telefone2', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25837', 'fax', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25838', 'celular', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25839', 'operadora_id_INT', '5', '', '11', null, '0', '0', '0', '', '77', 'id', 'empresa_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25840', 'celular_sms', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25841', 'email', '5', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25842', 'tipo_documento_id_INT', '5', '', '11', null, '0', '0', '0', '', '23', 'id', 'empresa_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25843', 'numero_documento', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25844', 'tipo_empresa_id_INT', '5', '', '11', null, '0', '0', '0', '', '12', 'id', 'empresa_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25845', 'logradouro', '5', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25846', 'logradouro_normalizado', '5', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25847', 'numero', '5', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25848', 'complemento', '5', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25849', 'complemento_normalizado', '5', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25850', 'bairro_id_INT', '5', '', '11', null, '0', '0', '0', '', '1', 'id', 'empresa_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25851', 'cidade_id_INT', '5', '', '11', null, '0', '0', '0', '', '4', 'id', 'empresa_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25852', 'latitude_INT', '5', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25853', 'longitude_INT', '5', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25854', 'foto', '5', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25855', 'corporacao_id_INT', '5', '', '11', null, '1', '1', '0', '', '49', 'id', 'empresa_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25856', 'id', '54', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25857', 'forma_pagamento_id_INT', '54', '', '11', null, '0', '0', '0', '', '143', 'id', 'empresa_compra_ibfk_5', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25858', 'valor_total_FLOAT', '54', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25859', 'data_DATE', '54', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25860', 'hora_TIME', '54', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25861', 'usuario_id_INT', '54', '', '11', null, '0', '0', '0', '', '14', 'id', 'empresa_compra_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25862', 'minha_empresa_id_INT', '54', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25863', 'outra_empresa_id_INT', '54', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25864', 'foto', '54', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25865', 'corporacao_id_INT', '54', '', '11', null, '0', '0', '0', '', '49', 'id', 'empresa_compra_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25866', 'id', '55', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25867', 'empresa_compra_id_INT', '55', '', '11', null, '1', '0', '0', '', '54', 'id', 'empresa_compra_parcela_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25868', 'data_DATE', '55', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25869', 'valor_FLOAT', '55', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25870', 'corporacao_id_INT', '55', '', '11', null, '0', '0', '0', '', '49', 'id', 'empresa_compra_parcela_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25871', 'id', '24', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25872', 'empresa_id_INT', '24', '', '11', null, '0', '0', '0', '', '5', 'id', 'empresa_perfil_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25873', 'perfil_id_INT', '24', '', '11', null, '0', '0', '0', '', '30', 'id', 'empresa_perfil_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25874', 'corporacao_id_INT', '24', '', '11', null, '0', '0', '0', '', '49', 'id', 'empresa_perfil_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25875', 'id', '56', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25876', 'identificador', '56', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25877', 'nome', '56', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25878', 'nome_normalizado', '56', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25879', 'descricao', '56', '', '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25880', 'empresa_produto_tipo_id_INT', '56', '', '11', null, '1', '0', '0', '', '71', 'id', 'empresa_produto_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25881', 'empresa_produto_unidade_medida_id_INT', '56', '', '11', null, '0', '0', '0', '', '59', 'id', 'empresa_produto_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25882', 'empresa_id_INT', '56', '', '11', null, '1', '0', '0', '', '5', 'id', 'empresa_produto_ibfk_4', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25883', 'video', '56', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25884', 'corporacao_id_INT', '56', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_produto_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25885', 'id', '57', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25886', 'empresa_produto_id_INT', '57', '', '11', null, '1', '0', '0', '', '56', 'id', 'empresa_produto_compra_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25887', 'empresa_compra_id_INT', '57', '', '11', null, '1', '0', '0', '', '54', 'id', 'empresa_produto_compra_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25888', 'quantidade_FLOAT', '57', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25889', 'valor_total_FLOAT', '57', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25890', 'corporacao_id_INT', '57', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_produto_compra_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25891', 'id', '58', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25892', 'empresa_produto_id_INT', '58', '', '11', null, '1', '0', '0', '', '56', 'id', 'empresa_produto_foto_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25893', 'foto', '58', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25894', 'corporacao_id_INT', '58', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_produto_foto_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25895', 'id', '71', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25896', 'nome', '71', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25897', 'nome_normalizado', '71', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25898', 'corporacao_id_INT', '71', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_produto_tipo_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25899', 'id', '59', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25900', 'nome', '59', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25901', 'abreviacao', '59', '', '10', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25902', 'is_float_BOOLEAN', '59', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25903', 'id', '60', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25904', 'empresa_produto_id_INT', '60', '', '11', null, '1', '0', '0', '', '56', 'id', 'empresa_produto_venda_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25905', 'empresa_venda_id_INT', '60', '', '11', null, '1', '0', '0', '', '68', 'id', 'empresa_produto_venda_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25906', 'quantidade_FLOAT', '60', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25907', 'valor_total_FLOAT', '60', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25908', 'corporacao_id_INT', '60', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_produto_venda_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25909', 'id', '61', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25910', 'identificador', '61', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25911', 'nome', '61', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25912', 'nome_normalizado', '61', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25913', 'descricao', '61', '', '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25914', 'empresa_servico_tipo_id_INT', '61', '', '11', null, '1', '0', '0', '', '72', 'id', 'empresa_servico_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25915', 'empresa_id_INT', '61', '', '11', null, '1', '0', '0', '', '5', 'id', 'empresa_servico_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25916', 'empresa_servico_unidade_medida_id_INT', '61', '', '11', null, '1', '0', '0', '', '64', 'id', 'empresa_servico_ibfk_4', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25917', 'prazo_entrega_dia_INT', '61', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25918', 'duracao_meses_INT', '61', '', '3', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25919', 'video', '61', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25920', 'corporacao_id_INT', '61', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_servico_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25921', 'id', '62', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25922', 'empresa_servico_id_INT', '62', '', '11', null, '1', '0', '0', '', '61', 'id', 'empresa_servico_compra_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25923', 'empresa_compra_id_INT', '62', '', '11', null, '1', '0', '0', '', '54', 'id', 'empresa_servico_compra_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25924', 'quantidade_FLOAT', '62', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25925', 'valor_total_FLOAT', '62', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25926', 'corporacao_id_INT', '62', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_servico_compra_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25927', 'id', '63', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25928', 'empresa_servico_id_INT', '63', '', '11', null, '1', '0', '0', '', '61', 'id', 'empresa_servico_foto_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25929', 'foto', '63', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25930', 'corporacao_id_INT', '63', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_servico_foto_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25931', 'id', '72', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25932', 'nome', '72', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25933', 'nome_normalizado', '72', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25934', 'corporacao_id_INT', '72', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_servico_tipo_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25935', 'id', '64', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25936', 'nome', '64', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25937', 'id', '65', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25938', 'empresa_servico_id_INT', '65', '', '11', null, '1', '0', '0', '', '61', 'id', 'empresa_servico_venda_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25939', 'empresa_venda_id_INT', '65', '', '11', null, '1', '0', '0', '', '68', 'id', 'empresa_servico_venda_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25940', 'quantidade_FLOAT', '65', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25941', 'valor_total_FLOAT', '65', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25942', 'corporacao_id_INT', '65', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_servico_venda_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25943', 'id', '66', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25944', 'empresa_produto_id_INT', '66', '', '11', null, '0', '0', '0', '', '56', 'id', 'empresa_tipo_venda_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25945', 'empresa_servico_id_INT', '66', '', '11', null, '0', '0', '0', '', '61', 'id', 'empresa_tipo_venda_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25946', 'valor_total_FLOAT', '66', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25947', 'corporacao_id_INT', '66', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_tipo_venda_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25948', 'id', '67', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25949', 'empresa_tipo_venda_id_INT', '67', '', '11', null, '1', '0', '0', '', '66', 'id', 'empresa_tipo_venda_parcela_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25950', 'porcentagem_FLOAT', '67', '', '4', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25951', 'corporacao_id_INT', '67', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_tipo_venda_parcela_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25952', 'id', '68', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25953', 'forma_pagamento_id_INT', '68', '', '11', null, '0', '0', '0', '', '143', 'id', 'empresa_venda_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25954', 'valor_total_FLOAT', '68', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25955', 'data_DATE', '68', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25956', 'hora_TIME', '68', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25957', 'usuario_id_INT', '68', '', '11', null, '1', '0', '0', '', '14', 'id', 'empresa_venda_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25958', 'cliente_empresa_id_INT', '68', '', '11', null, '0', '0', '0', '', '5', 'id', 'empresa_venda_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25959', 'fornecedor_empresa_id_INT', '68', '', '11', null, '0', '0', '0', '', '5', 'id', 'empresa_venda_ibfk_4', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25960', 'pessoa_id_INT', '68', '', '11', null, '0', '0', '0', '', '6', 'id', 'empresa_venda_ibfk_5', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25961', 'foto', '68', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25962', 'corporacao_id_INT', '68', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_venda_ibfk_6', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25963', 'id', '69', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25964', 'empresa_venda_id_INT', '69', '', '11', null, '1', '0', '0', '', '68', 'id', 'empresa_venda_parcela_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25965', 'data_DATE', '69', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25966', 'valor_FLOAT', '69', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25967', 'corporacao_id_INT', '69', '', '11', null, '1', '0', '0', '', '49', 'id', 'empresa_venda_parcela_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25968', 'id', '126', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25969', 'nome', '126', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25970', 'id', '143', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25971', 'nome', '143', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25972', 'nome_normalizado', '143', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25973', 'corporacao_id_INT', '143', '', '11', null, '0', '0', '0', '', '49', 'id', 'forma_pagamento_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25974', 'id', '441', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25975', 'hora_inicio_TIME', '441', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25976', 'hora_fim_TIME', '441', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25977', 'pessoa_empresa_id_INT', '441', '', '11', null, '0', '0', '0', '', '7', 'id', 'horario_trabalho_pessoa_empresa_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25978', 'data_DATE', '441', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25979', 'corporacao_id_INT', '441', '', '11', null, '1', '0', '0', '', '49', 'id', 'horario_trabalho_pessoa_empresa_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25980', 'id', '8', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25981', 'nome', '8', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25982', 'nome_normalizado', '8', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25983', 'ano_INT', '8', '', '4', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25984', 'corporacao_id_INT', '8', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25994', 'id', '77', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25995', 'nome', '77', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25996', 'nome_normalizado', '77', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome_operadora_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25997', 'id', '22', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25998', 'nome', '22', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('25999', 'nome_normalizado', '22', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26000', 'corporacao_id_INT', '22', '', '11', null, '1', '0', '0', '', '49', 'id', 'pais_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26001', 'id', '30', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26002', 'nome', '30', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome_perfil_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26003', 'id', '46', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26004', 'nome', '46', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26005', 'tag', '46', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'tag', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26006', 'pai_permissao_id_INT', '46', '', '11', null, '0', '0', '0', '', '46', 'id', 'permissao_ibfk_1', 'SET NULL', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26007', 'id', '3', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26008', 'permissao_id_INT', '3', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'permissao_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26009', 'categoria_permissao_id_INT', '3', '', '11', null, '1', '0', '0', '', '2', 'id', 'permissao_categoria_permissao_ibfk_10', 'CASCADE', 'CASCADE', '', '1', 'permissao_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26010', 'corporacao_id_INT', '3', '', '11', null, '1', '0', '0', '', '49', 'id', 'permissao_categoria_permissao_ibfk_3', 'CASCADE', 'CASCADE', '', '1', 'permissao_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26011', 'id', '6', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26012', 'identificador', '6', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26013', 'nome', '6', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26014', 'nome_normalizado', '6', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26015', 'tipo_documento_id_INT', '6', '', '11', null, '0', '0', '0', '', '23', 'id', 'pessoa_ibfk_2', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26016', 'numero_documento', '6', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26017', 'is_consumidor_BOOLEAN', '6', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26018', 'sexo_id_INT', '6', '', '11', null, '0', '0', '0', '', '39', 'id', 'pessoa_ibfk_3', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26019', 'telefone', '6', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26020', 'celular', '6', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26021', 'operadora_id_INT', '6', '', '11', null, '0', '0', '0', '', '77', 'id', 'pessoa_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26022', 'celular_sms', '6', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26023', 'email', '6', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26024', 'logradouro', '6', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26025', 'logradouro_normalizado', '6', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26026', 'numero', '6', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26027', 'complemento', '6', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26028', 'complemento_normalizado', '6', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26029', 'cidade_id_INT', '6', '', '11', null, '0', '0', '0', '', '4', 'id', 'pessoa_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26030', 'bairro_id_INT', '6', '', '11', null, '0', '0', '0', '', '1', 'id', 'pessoa_ibfk_6', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26031', 'latitude_INT', '6', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26032', 'longitude_INT', '6', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26033', 'foto', '6', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26034', 'corporacao_id_INT', '6', '', '11', null, '1', '0', '0', '', '49', 'id', 'pessoa_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26035', 'id', '7', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26036', 'pessoa_id_INT', '7', '', '11', null, '1', '0', '0', '', '6', 'id', 'pessoa_empresa_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'pessoa_empresa_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26037', 'empresa_id_INT', '7', '', '11', null, '1', '0', '0', '', '5', 'id', 'pessoa_empresa_ibfk_3', 'CASCADE', 'CASCADE', '', '1', 'pessoa_empresa_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26038', 'profissao_id_INT', '7', '', '11', null, '0', '0', '0', '', '10', 'id', 'pessoa_empresa_ibfk_4', 'CASCADE', 'SET NULL', '', '1', 'pessoa_empresa_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26039', 'corporacao_id_INT', '7', '', '11', null, '1', '0', '0', '', '49', 'id', 'pessoa_empresa_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'pessoa_empresa_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26040', 'id', '85', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26041', 'pessoa_empresa_id_INT', '85', '', '11', null, '1', '0', '0', '', '7', 'id', 'pessoa_empresa_rotina_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26042', 'semana_INT', '85', '', '4', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26043', 'dia_semana_INT', '85', '', '4', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26044', 'corporacao_id_INT', '85', '', '11', null, '1', '0', '0', '', '49', 'id', 'pessoa_empresa_rotina_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26045', 'id', '87', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26046', 'pessoa_id_INT', '87', '', '11', null, '1', '0', '0', '', '6', 'id', 'pessoa_usuario_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'pessoa_usuario_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26047', 'usuario_id_INT', '87', '', '11', null, '1', '0', '0', '', '14', 'id', 'pessoa_usuario_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'pessoa_usuario_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26048', 'corporacao_id_INT', '87', '', '11', null, '1', '0', '0', '', '49', 'id', 'pessoa_usuario_ibfk_3', 'CASCADE', 'CASCADE', '', '1', 'pessoa_usuario_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26049', 'id', '9', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26050', 'pessoa_id_INT', '9', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26051', 'hora_TIME', '9', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26052', 'dia_DATE', '9', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26053', 'foto', '9', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26054', 'empresa_id_INT', '9', '', '11', null, '0', '0', '0', '', '5', 'id', 'ponto_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26055', 'profissao_id_INT', '9', '', '11', null, '0', '0', '0', '', '10', 'id', 'ponto_ibfk_4', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26056', 'usuario_id_INT', '9', '', '11', null, '0', '0', '0', '', '14', 'id', 'ponto_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26057', 'is_entrada_BOOLEAN', '9', '', '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26058', 'latitude_INT', '9', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26059', 'longitude_INT', '9', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26060', 'tipo_ponto_id_INT', '9', '', '11', null, '0', '0', '0', '', '82', 'id', 'ponto_ibfk_5', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26061', 'corporacao_id_INT', '9', '', '11', null, '1', '0', '0', '', '49', 'id', 'ponto_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26065', 'id', '10', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26066', 'nome', '10', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26067', 'nome_normalizado', '10', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26068', 'corporacao_id_INT', '10', '', '11', null, '1', '0', '0', '', '49', 'id', 'profissao_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26069', 'id', '83', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26070', 'nome', '83', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'rede_unique', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26071', 'nome_normalizado', '83', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26072', 'corporacao_id_INT', '83', '', '11', null, '1', '0', '0', '', '49', 'id', 'rede_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26073', 'id', '84', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26074', 'rede_id_INT', '84', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'rede_empresa_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26075', 'empresa_id_INT', '84', '', '11', null, '1', '0', '0', '', '5', 'id', 'rede_empresa_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'rede_empresa_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26076', 'corporacao_id_INT', '84', '', '11', null, '1', '0', '0', '', '49', 'id', 'rede_empresa_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26077', 'id', '89', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26078', 'titulo', '89', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26079', 'titulo_normalizado', '89', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26080', 'descricao', '89', '', '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26081', 'descricao_normalizado', '89', '', '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26082', 'pessoa_id_INT', '89', '', '11', null, '0', '0', '0', '', '6', 'id', 'relatorio_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26083', 'empresa_id_INT', '89', '', '11', null, '0', '0', '0', '', '5', 'id', 'relatorio_ibfk_4', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26084', 'hora_TIME', '89', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26085', 'dia_DATE', '89', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26086', 'usuario_id_INT', '89', '', '11', null, '0', '0', '0', '', '14', 'id', 'relatorio_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26087', 'corporacao_id_INT', '89', '', '11', null, '1', '0', '0', '', '49', 'id', 'relatorio_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26088', 'id', '91', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26089', 'relatorio_id_INT', '91', '', '11', null, '1', '0', '0', '', '89', 'id', 'relatorio_anexo_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26090', 'arquivo', '91', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26091', 'tipo_anexo_id_INT', '91', '', '11', null, '1', '0', '0', '', '90', 'id', 'relatorio_anexo_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26092', 'corporacao_id_INT', '91', '', '11', null, '1', '0', '0', '', '49', 'id', 'relatorio_anexo_ibfk_3', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26093', 'id', '86', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26094', 'ultima_dia_contagem_ciclo_DATE', '86', '', null, null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26095', 'corporacao_id_INT', '86', '', '11', null, '1', '0', '0', '', '49', 'id', 'rotina_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26096', 'id', '31', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26097', 'nome', '31', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'tag', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26098', 'tag', '31', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26099', 'id', '39', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26100', 'nome', '39', '', '30', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26111', 'id', '139', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26112', 'nome', '139', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26113', 'data_modificacao_estrutura_DATETIME', '139', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26114', 'frequencia_sincronizador_INT', '139', '', '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26115', 'banco_mobile_BOOLEAN', '139', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26116', 'banco_web_BOOLEAN', '139', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26117', 'transmissao_web_para_mobile_BOOLEAN', '139', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26118', 'transmissao_mobile_para_web_BOOLEAN', '139', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26119', 'tabela_sistema_BOOLEAN', '139', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26120', 'excluida_BOOLEAN', '139', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26121', 'id', '343', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26122', 'nome', '343', '', '50', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'sistema_tipo_download_arquivo_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26123', 'path', '343', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26124', 'id', '171', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26125', 'nome', '171', '', '10', null, '0', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26132', 'id', '11', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26133', 'criado_pelo_usuario_id_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26134', 'categoria_permissao_id_INT', '11', '', '11', null, '0', '0', '0', '', '2', 'id', 'tarefa_ibfk_7', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26135', 'veiculo_id_INT', '11', '', '11', null, '0', '0', '0', '', '20', 'id', 'tarefa_ibfk_10', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26136', 'usuario_id_INT', '11', '', '11', null, '0', '0', '0', '', '14', 'id', 'tarefa_ibfk_13', 'CASCADE', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26137', 'veiculo_usuario_id_INT', '11', '', '11', null, '0', '0', '0', '', '21', 'id', 'tarefa_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26138', 'origem_pessoa_id_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26139', 'origem_empresa_id_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26140', 'origem_logradouro', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26141', 'origem_logradouro_normalizado', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26142', 'origem_numero', '11', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26143', 'origem_complemento', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26144', 'origem_complemento_normalizado', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26145', 'origem_cidade_id_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26146', 'origem_bairro_id_INT', '11', '', '11', null, '0', '0', '0', '', '1', 'id', 'tarefa_ibfk_9', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26147', 'origem_latitude_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26148', 'origem_longitude_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26149', 'origem_latitude_real_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26150', 'origem_longitude_real_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26151', 'destino_pessoa_id_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26152', 'destino_empresa_id_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26153', 'destino_logradouro', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26154', 'destino_logradouro_normalizado', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26155', 'destino_numero', '11', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26156', 'destino_complemento', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26157', 'destino_complemento_normalizado', '11', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26158', 'destino_cidade_id_INT', '11', '', '11', null, '0', '0', '0', '', '4', 'id', 'tarefa_ibfk_5', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26159', 'destino_bairro_id_INT', '11', '', '11', null, '0', '0', '0', '', '1', 'id', 'tarefa_ibfk_6', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26160', 'destino_latitude_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26161', 'destino_longitude_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26162', 'destino_latitude_real_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26163', 'destino_longitude_real_INT', '11', '', '6', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26164', 'tempo_estimado_carro_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26165', 'tempo_estimado_a_pe_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26166', 'distancia_estimada_carro_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26167', 'distancia_estimada_a_pe_INT', '11', '', '11', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26168', 'is_realizada_a_pe_BOOLEAN', '11', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26169', 'inicio_data_programada_DATE', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26170', 'inicio_hora_programada_TIME', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26171', 'data_exibir_DATE', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26172', 'inicio_data_DATE', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26173', 'inicio_hora_TIME', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26174', 'fim_data_DATE', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26175', 'fim_hora_TIME', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26176', 'data_cadastro_DATE', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26177', 'hora_cadastro_TIME', '11', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26178', 'titulo', '11', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26179', 'titulo_normalizado', '11', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26180', 'descricao', '11', '', '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26181', 'descricao_normalizado', '11', '', '512', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26182', 'corporacao_id_INT', '11', '', '11', null, '1', '0', '0', '', '49', 'id', 'tarefa_ibfk_11', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26183', 'id', '90', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26184', 'nome', '90', '', '30', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26185', 'id', '23', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26186', 'nome', '23', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26187', 'nome_normalizado', '23', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26188', 'corporacao_id_INT', '23', '', '11', null, '1', '0', '0', '', '49', 'id', 'tipo_documento_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26189', 'id', '12', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26190', 'nome', '12', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26191', 'nome_normalizado', '12', '', '100', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26192', 'corporacao_id_INT', '12', '', '11', null, '1', '0', '0', '', '49', 'id', 'tipo_empresa_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26193', 'id', '82', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26194', 'nome', '82', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26195', 'id', '13', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26196', 'nome', '13', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26197', 'nome_normalizado', '13', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'nome2', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26198', 'sigla', '13', '', '2', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26199', 'pais_id_INT', '13', '', '11', null, '1', '0', '0', '', '22', 'id', 'uf_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'nome2', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26200', 'corporacao_id_INT', '13', '', '11', null, '1', '0', '0', '', '49', 'id', 'uf_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'nome2', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26201', 'id', '14', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26202', 'nome', '14', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26203', 'nome_normalizado', '14', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26204', 'email', '14', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'email', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26205', 'senha', '14', '', '100', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26206', 'status_BOOLEAN', '14', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26207', 'pagina_inicial', '14', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26208', 'id', '15', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26209', 'usuario_id_INT', '15', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'usuario_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26210', 'categoria_permissao_id_INT', '15', '', '11', null, '1', '0', '0', '', '2', 'id', 'usuario_categoria_permissao_ibfk_8', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26211', 'corporacao_id_INT', '15', '', '11', null, '1', '0', '0', '', '49', 'id', 'usuario_categoria_permissao_ibfk_9', 'CASCADE', 'CASCADE', '', '1', 'usuario_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26212', 'id', '16', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26213', 'usuario_id_INT', '16', '', '11', null, '1', '0', '0', '', '14', 'id', 'usuario_corporacao_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'usuario_corporacao_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26214', 'corporacao_id_INT', '16', '', '11', null, '1', '0', '0', '', '49', 'id', 'usuario_corporacao_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'usuario_corporacao_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26215', 'status_BOOLEAN', '16', '', '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26216', 'is_adm_BOOLEAN', '16', '', '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26221', 'id', '17', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26222', 'data_DATE', '17', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26223', 'hora_TIME', '17', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26224', 'foto_interna', '17', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26225', 'foto_externa', '17', '', '50', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26226', 'usuario_id_INT', '17', '', '11', null, '0', '0', '0', '', '14', 'id', 'usuario_foto_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26227', 'corporacao_id_INT', '17', '', '11', null, '0', '0', '0', '', '49', 'id', 'usuario_foto_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26228', 'id', '168', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26229', 'is_camera_interna_BOOLEAN', '168', '', '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26230', 'periodo_segundo_INT', '168', '', '7', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26231', 'usuario_id_INT', '168', '', '11', null, '1', '0', '0', '', '14', 'id', 'usuario_foto_configuracao_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26232', 'corporacao_id_INT', '168', '', '11', null, '1', '0', '0', '', '49', 'id', 'usuario_foto_configuracao_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26237', 'id', '18', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26238', 'data_DATE', '18', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26239', 'hora_TIME', '18', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26240', 'veiculo_usuario_id_INT', '18', '', '11', null, '0', '0', '0', '', '21', 'id', 'usuario_posicao_ibfk_3', 'SET NULL', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26241', 'usuario_id_INT', '18', '', '11', null, '0', '0', '0', '', '14', 'id', 'usuario_posicao_ibfk_1', 'SET NULL', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26242', 'latitude_INT', '18', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26243', 'longitude_INT', '18', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26244', 'velocidade_INT', '18', '', '5', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26245', 'foto_interna', '18', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26246', 'foto_externa', '18', '', '30', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26247', 'corporacao_id_INT', '18', '', '11', null, '0', '0', '0', '', '49', 'id', 'usuario_posicao_ibfk_2', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26252', 'id', '19', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26253', 'status_BOOLEAN', '19', '', '1', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26254', 'servico_id_INT', '19', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'servico_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26255', 'usuario_id_INT', '19', '', '11', null, '1', '0', '0', '', '14', 'id', 'usuario_servico_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'servico_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26256', 'corporacao_id_INT', '19', '', '11', null, '0', '0', '0', '', '49', 'id', 'usuario_servico_ibfk_9', 'CASCADE', 'CASCADE', '', '1', 'servico_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26257', 'id', '73', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26258', 'nome', '73', '', '255', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'usuario_tipo_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26259', 'nome_visivel', '73', '', '255', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26260', 'status_BOOLEAN', '73', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26261', 'pagina_inicial', '73', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26262', 'corporacao_id_INT', '73', '', '11', null, '1', '0', '0', '', '49', 'id', 'usuario_tipo_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'usuario_tipo_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26263', 'id', '75', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26264', 'usuario_id_INT', '75', '', '11', null, '1', '0', '0', '', '14', 'id', 'usuario_tipo_corporacao_ibfk_2', 'CASCADE', 'CASCADE', '', '1', 'usuario_tipo_corporacao_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26265', 'usuario_tipo_id_INT', '75', '', '11', null, '1', '0', '0', '', '73', 'id', 'usuario_tipo_corporacao_ibfk_1', 'CASCADE', 'CASCADE', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26266', 'corporacao_id_INT', '75', '', '11', null, '1', '0', '0', '', '49', 'id', 'usuario_tipo_corporacao_ibfk_3', 'CASCADE', 'CASCADE', '', '1', 'usuario_tipo_corporacao_UNIQUE', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26275', 'id', '20', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26276', 'placa', '20', '', '20', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'placa', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26277', 'modelo_id_INT', '20', '', '11', null, '0', '0', '0', '', '8', 'id', 'veiculo_ibfk_2', 'SET NULL', 'SET NULL', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26278', 'corporacao_id_INT', '20', '', '11', null, '1', '0', '0', '', '49', 'id', 'veiculo_ibfk_1', 'CASCADE', 'CASCADE', '', '1', 'placa', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26279', 'id', '21', '', '11', null, '1', '1', '1', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26280', 'usuario_id_INT', '21', '', '11', null, '1', '0', '0', '', null, '', '', '', '', '', '1', 'usuario_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26281', 'data_DATE', '21', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26282', 'hora_TIME', '21', '', null, null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26283', 'veiculo_id_INT', '21', '', '11', null, '1', '0', '0', '', '20', 'id', 'veiculo_usuario_ibfk_4', 'CASCADE', 'CASCADE', '', '1', 'usuario_id_INT', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26284', 'is_ativo_BOOLEAN', '21', '', '1', null, '0', '0', '0', '', null, '', '', '', '', '', '0', '', null, null, null, null);
INSERT INTO `sistema_atributo` VALUES ('26285', 'corporacao_id_INT', '21', '', '11', null, '1', '0', '0', '', '49', 'id', 'veiculo_usuario_ibfk_6', 'CASCADE', 'CASCADE', '', '1', 'usuario_id_INT', null, null, null, null);

-- ----------------------------
-- Table structure for `sistema_banco_versao`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_banco_versao`;
CREATE TABLE `sistema_banco_versao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_inicio_estrutura_DATETIME` varchar(20) DEFAULT NULL,
  `data_fim_estrutura_DATETIME` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_banco_versao
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_corporacao_sincronizador`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_corporacao_sincronizador`;
CREATE TABLE `sistema_corporacao_sincronizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia_DATE` varchar(10) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_7f3c58196614913a` (`corporacao_id_INT`),
  CONSTRAINT `sistema_corporacao_sincronizador_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_corporacao_sincronizador
-- ----------------------------
INSERT INTO `sistema_corporacao_sincronizador` VALUES ('1', '2013-5-31', '1');

-- ----------------------------
-- Table structure for `sistema_corporacao_sincronizador_tabela`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_corporacao_sincronizador_tabela`;
CREATE TABLE `sistema_corporacao_sincronizador_tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia_DATE` varchar(10) DEFAULT NULL,
  `sistema_corporacao_sincronizador_id_INT` int(11) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_b2516f4edddfa18e` (`sistema_corporacao_sincronizador_id_INT`),
  KEY `key_fa11478dd8ccc0c8` (`sistema_tabela_id_INT`),
  CONSTRAINT `sistema_corporacao_sincronizador_tabela_ibfk_1` FOREIGN KEY (`sistema_corporacao_sincronizador_id_INT`) REFERENCES `sistema_corporacao_sincronizador` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_corporacao_sincronizador_tabela_ibfk_2` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_corporacao_sincronizador_tabela
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_crud_estado`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_crud_estado`;
CREATE TABLE `sistema_crud_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_crud_INT` int(11) DEFAULT NULL,
  `id_crud_mobile_INT` int(11) DEFAULT NULL,
  `id_estado_crud_INT` int(11) DEFAULT NULL,
  `id_estado_crud_mobile_INT` int(11) DEFAULT NULL,
  `sistema_tipo_operacao_banco_id_INT` int(11) DEFAULT NULL,
  `id_sincronizador_mobile_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_tipo_operacao_banco_id_INT` (`sistema_tipo_operacao_banco_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `id_crud_INT` (`id_crud_INT`),
  KEY `id_crud_mobile_INT` (`id_crud_mobile_INT`),
  CONSTRAINT `sistema_crud_estado_ibfk_1` FOREIGN KEY (`sistema_tipo_operacao_banco_id_INT`) REFERENCES `sistema_tipo_operacao_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_crud_estado_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_crud_estado
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_deletar_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_deletar_arquivo`;
CREATE TABLE `sistema_deletar_arquivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_deletar_arquivo
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_grafo_hierarquia_banco`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_grafo_hierarquia_banco`;
CREATE TABLE `sistema_grafo_hierarquia_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grau_INT` int(11) NOT NULL,
  `numero_dependentes_INT` int(11) NOT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_6e2ad034ab998732` (`sistema_tabela_id_INT`),
  CONSTRAINT `sistema_grafo_hierarquia_banco_ibfk_1` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_grafo_hierarquia_banco
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_historico_sincronizador`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_historico_sincronizador`;
CREATE TABLE `sistema_historico_sincronizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `id_tabela_INT` int(11) DEFAULT NULL,
  `id_tabela_mobile_INT` int(11) DEFAULT NULL,
  `sistema_tipo_operacao_id_INT` int(11) DEFAULT NULL,
  `data_DATETIME` datetime DEFAULT NULL,
  `usuario_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_b5d062ceb0542de6` (`sistema_tabela_id_INT`),
  KEY `key_27e4fcf168a77d56` (`sistema_tipo_operacao_id_INT`),
  KEY `key_a7cac0eecb5c738f` (`corporacao_id_INT`),
  CONSTRAINT `sistema_historico_sincronizador_ibfk_1` FOREIGN KEY (`sistema_tipo_operacao_id_INT`) REFERENCES `sistema_tipo_operacao_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_historico_sincronizador_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_historico_sincronizador_ibfk_3` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_historico_sincronizador
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_operacao_crud_aleatorio`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_operacao_crud_aleatorio`;
CREATE TABLE `sistema_operacao_crud_aleatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operacao_sistema_mobile_id_INT` int(11) DEFAULT NULL,
  `total_insercao_INT` int(11) DEFAULT NULL,
  `porcentagem_remocao_FLOAT` float(3,0) DEFAULT NULL,
  `porcentagem_edicao_FLOAT` float(3,0) DEFAULT NULL,
  `sincronizar_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operacao_sistema_mobile_id_INT` (`operacao_sistema_mobile_id_INT`),
  CONSTRAINT `sistema_operacao_crud_aleatorio_ibfk_1` FOREIGN KEY (`operacao_sistema_mobile_id_INT`) REFERENCES `sistema_operacao_sistema_mobile` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_operacao_crud_aleatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_operacao_crud_mobile_baseado_web`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_operacao_crud_mobile_baseado_web`;
CREATE TABLE `sistema_operacao_crud_mobile_baseado_web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operacao_sistema_mobile_id_INT` int(11) NOT NULL,
  `url_json_operacoes_web` varchar(255) NOT NULL,
  `sincronizar_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_operacao_crud_mobile_baseado_web
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_operacao_download_banco_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_operacao_download_banco_mobile`;
CREATE TABLE `sistema_operacao_download_banco_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path_script_sql_banco` varchar(255) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL,
  `operacao_sistema_mobile_id_INT` int(11) DEFAULT NULL,
  `popular_tabela_BOOLEAN` int(1) DEFAULT NULL,
  `drop_tabela_BOOLEAN` int(1) DEFAULT NULL,
  `destino_banco_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ghjfghj` (`operacao_sistema_mobile_id_INT`),
  CONSTRAINT `sistema_operacao_download_banco_mobile_ibfk_1` FOREIGN KEY (`operacao_sistema_mobile_id_INT`) REFERENCES `sistema_operacao_sistema_mobile` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_operacao_download_banco_mobile
-- ----------------------------
INSERT INTO `sistema_operacao_download_banco_mobile` VALUES ('80', 'banco_android577637.db', 'pl', '119', '1', '1', null);
INSERT INTO `sistema_operacao_download_banco_mobile` VALUES ('81', 'banco_android635224.sql', 'asdf', '120', '1', '1', null);
INSERT INTO `sistema_operacao_download_banco_mobile` VALUES ('82', 'banco_android540009.sql', 'iu', '121', '1', '1', null);
INSERT INTO `sistema_operacao_download_banco_mobile` VALUES ('83', 'banco_android283478.sql', 'sdfg', '122', '1', '1', null);
INSERT INTO `sistema_operacao_download_banco_mobile` VALUES ('84', 'banco_android760193.sql', 'sdfg', '123', '1', '1', null);
INSERT INTO `sistema_operacao_download_banco_mobile` VALUES ('85', 'banco_android589142.sql', 'asdf', '124', '1', '1', null);

-- ----------------------------
-- Table structure for `sistema_operacao_executa_script`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_operacao_executa_script`;
CREATE TABLE `sistema_operacao_executa_script` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xml_script_sql_banco_ARQUIVO` varchar(255) DEFAULT NULL,
  `operacao_sistema_mobile_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operacao_download_banco_mobile_FK_422485351` (`operacao_sistema_mobile_id_INT`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_operacao_executa_script
-- ----------------------------
INSERT INTO `sistema_operacao_executa_script` VALUES ('1', 'banco_android245453.sql', '1');
INSERT INTO `sistema_operacao_executa_script` VALUES ('2', 'banco_android472137.sql', '2');
INSERT INTO `sistema_operacao_executa_script` VALUES ('3', 'banco_android334228.sql', '3');
INSERT INTO `sistema_operacao_executa_script` VALUES ('4', 'banco_android142639.sql', '4');

-- ----------------------------
-- Table structure for `sistema_operacao_sistema_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_operacao_sistema_mobile`;
CREATE TABLE `sistema_operacao_sistema_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_operacao_sistema_id_INT` int(11) DEFAULT NULL,
  `estado_operacao_sistema_mobile_id_INT` int(11) DEFAULT NULL,
  `mobile_identificador_id_INT` int(11) DEFAULT NULL,
  `data_abertura_DATETIME` varchar(20) DEFAULT NULL,
  `data_processamento_DATETIME` varchar(20) DEFAULT NULL,
  `data_conclusao_DATETIME` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_operacao_sistema_mobile
-- ----------------------------
INSERT INTO `sistema_operacao_sistema_mobile` VALUES ('119', '1', '3', '141', '2013-05-31 23:28:27', 'null', 'null');
INSERT INTO `sistema_operacao_sistema_mobile` VALUES ('120', '1', '3', '141', '2013-05-31 23:31:30', 'null', 'null');
INSERT INTO `sistema_operacao_sistema_mobile` VALUES ('121', '1', '3', '141', '2013-05-31 23:33:57', 'null', 'null');
INSERT INTO `sistema_operacao_sistema_mobile` VALUES ('122', '1', '3', '141', '2013-05-31 23:37:09', 'null', 'null');
INSERT INTO `sistema_operacao_sistema_mobile` VALUES ('123', '1', '3', '141', '2013-05-31 23:39:59', 'null', 'null');
INSERT INTO `sistema_operacao_sistema_mobile` VALUES ('124', '1', '1', '141', '2013-05-31 23:44:46', 'null', 'null');

-- ----------------------------
-- Table structure for `sistema_parametro_global`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_parametro_global`;
CREATE TABLE `sistema_parametro_global` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `valor_string` varchar(250) DEFAULT NULL,
  `valor_INT` int(11) DEFAULT NULL,
  `valor_DATETIME` datetime DEFAULT NULL,
  `valor_DATE` date DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `valor_BOOLEAN` int(1) DEFAULT NULL,
  `valor_SEC` int(10) DEFAULT NULL,
  `valor_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_parametro_global
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_parametro_global_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_parametro_global_corporacao`;
CREATE TABLE `sistema_parametro_global_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `valor_string` varchar(250) DEFAULT NULL,
  `valor_INT` int(11) DEFAULT NULL,
  `valor_DATETIME` datetime DEFAULT NULL,
  `valor_SEC` int(10) DEFAULT NULL,
  `valor_OFFSEC` int(6) DEFAULT NULL,
  `valor_DATE` date DEFAULT NULL,
  `valor_FLOAT` double DEFAULT NULL,
  `valor_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spgc_FK_66619873` (`corporacao_id_INT`) USING BTREE,
  CONSTRAINT `spgc_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_parametro_global_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_registro_historico`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_registro_historico`;
CREATE TABLE `sistema_registro_historico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crud` text,
  `sistema_registro_sincronizador_id_INT` int(11) DEFAULT NULL,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `id_tabela_INT` int(11) DEFAULT NULL,
  `id_tabela_mobile_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sistema_registro_sincronizador_id_INT` (`sistema_registro_sincronizador_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `sistema_tabela_id_INT` (`sistema_tabela_id_INT`),
  CONSTRAINT `sistema_registro_historico_ibfk_1` FOREIGN KEY (`sistema_registro_sincronizador_id_INT`) REFERENCES `sistema_registro_sincronizador_android_para_web` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_historico_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_historico_ibfk_3` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_registro_historico
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_registro_sincronizador_android_para_web`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_registro_sincronizador_android_para_web`;
CREATE TABLE `sistema_registro_sincronizador_android_para_web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `id_tabela_INT` int(11) NOT NULL,
  `sistema_tipo_operacao_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `imei` varchar(30) DEFAULT NULL,
  `sistema_produto_INT` int(11) DEFAULT NULL,
  `sistema_projetos_versao_INT` int(11) DEFAULT NULL,
  `id_tabela_web_INT` int(11) DEFAULT NULL,
  `id_tabela_mobile_INT` int(11) DEFAULT NULL,
  `criado_em_DATETIME` datetime DEFAULT NULL,
  `sincronizando_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_9faac919037fa02e` (`sistema_tabela_id_INT`) USING BTREE,
  KEY `key_178e87b882cc0a41` (`sistema_tipo_operacao_id_INT`) USING BTREE,
  KEY `key_5c569a4bded28cbf` (`usuario_id_INT`) USING BTREE,
  KEY `key_fe8cbbbd5a6e3c8d` (`corporacao_id_INT`) USING BTREE,
  CONSTRAINT `sistema_registro_sincronizador_android_para_web_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_android_para_web_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_android_para_web_ibfk_3` FOREIGN KEY (`sistema_tipo_operacao_id_INT`) REFERENCES `sistema_tipo_operacao_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_android_para_web_ibfk_4` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_registro_sincronizador_android_para_web
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_registro_sincronizador_web_para_android`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_registro_sincronizador_web_para_android`;
CREATE TABLE `sistema_registro_sincronizador_web_para_android` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_tabela_id_INT` int(11) DEFAULT NULL,
  `id_tabela_INT` int(11) DEFAULT NULL,
  `sistema_tipo_operacao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `imei` varchar(30) DEFAULT NULL,
  `sistema_produto_INT` int(11) DEFAULT NULL,
  `sistema_projetos_versao_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_b5d062ceb0542de6` (`sistema_tabela_id_INT`),
  KEY `key_27e4fcf168a77d56` (`sistema_tipo_operacao_id_INT`),
  KEY `key_a7cac0eecb5c738f` (`corporacao_id_INT`),
  CONSTRAINT `sistema_registro_sincronizador_web_para_android_ibfk_1` FOREIGN KEY (`sistema_tipo_operacao_id_INT`) REFERENCES `sistema_tipo_operacao_banco` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_web_para_android_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_registro_sincronizador_web_para_android_ibfk_3` FOREIGN KEY (`sistema_tabela_id_INT`) REFERENCES `sistema_tabela` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_registro_sincronizador_web_para_android
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_sequencia`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_sequencia`;
CREATE TABLE `sistema_sequencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabela` varchar(128) DEFAULT NULL,
  `id_ultimo_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_sequencia
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_sincronizador_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_sincronizador_arquivo`;
CREATE TABLE `sistema_sincronizador_arquivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arquivo` varchar(50) NOT NULL,
  `is_zip_BOOLEAN` int(1) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_8875c4777b1d3c9f` (`corporacao_id_INT`),
  CONSTRAINT `sistema_sincronizador_arquivo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_sincronizador_arquivo
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_tabela`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tabela`;
CREATE TABLE `sistema_tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `data_modificacao_estrutura_DATETIME` date DEFAULT NULL,
  `frequencia_sincronizador_INT` int(2) DEFAULT NULL,
  `banco_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `banco_web_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_web_para_mobile_BOOLEAN` int(1) DEFAULT NULL,
  `transmissao_mobile_para_web_BOOLEAN` int(1) DEFAULT NULL,
  `tabela_sistema_BOOLEAN` int(1) DEFAULT NULL,
  `excluida_BOOLEAN` int(1) DEFAULT NULL,
  `atributos_chave_unica` varchar(512) DEFAULT NULL,
  `nome_exibicao` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tabela
-- ----------------------------
INSERT INTO `sistema_tabela` VALUES ('1', 'bairro', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, cidade_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('2', 'categoria_permissao', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('3', 'permissao_categoria_permissao', null, '-1', '1', '1', '1', '1', null, null, 'permissao_id_INT, categoria_permissao_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('4', 'cidade', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, uf_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('5', 'empresa', null, '-1', '1', '1', '1', '1', null, null, 'email, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('6', 'pessoa', null, '-1', '1', '1', '1', '1', null, null, 'email, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('7', 'pessoa_empresa', null, '-1', '1', '1', '1', '1', null, null, 'pessoa_id_INT, empresa_id_INT, profissao_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('8', 'modelo', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, ano_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('9', 'ponto', null, '-1', '1', '1', '1', '1', null, null, 'pessoa_id_INT, usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('10', 'profissao', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('11', 'tarefa', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('12', 'tipo_empresa', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('13', 'uf', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, corporacao_id_INT, pais_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('14', 'usuario', null, '-1', '1', '1', '1', '1', null, null, 'email', null);
INSERT INTO `sistema_tabela` VALUES ('15', 'usuario_categoria_permissao', null, '-1', '1', '1', '1', '1', null, null, 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('16', 'usuario_corporacao', null, '-1', '1', '1', '1', '1', null, null, 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('17', 'usuario_foto', null, '-1', '1', '1', '1', '1', null, null, 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('18', 'usuario_posicao', null, '-1', '0', '1', '0', '0', null, null, 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('19', 'usuario_servico', null, '-1', '1', '1', '1', '1', null, null, 'servico_id_INT, usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('20', 'veiculo', null, '-1', '1', '1', '1', '1', null, null, 'placa, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('21', 'veiculo_usuario', null, '-1', '1', '1', '1', '1', null, null, 'usuario_id_INT, veiculo_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('22', 'pais', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('23', 'tipo_documento', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('24', 'empresa_perfil', null, '-1', '1', '1', '1', '1', null, null, 'empresa_id_INT, perfil_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('30', 'perfil', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('31', 'servico', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('39', 'sexo', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('46', 'permissao', null, '-1', '1', '1', '1', '1', null, null, 'tag', null);
INSERT INTO `sistema_tabela` VALUES ('49', 'corporacao', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('54', 'empresa_compra', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('55', 'empresa_compra_parcela', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('56', 'empresa_produto', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('57', 'empresa_produto_compra', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('58', 'empresa_produto_foto', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('59', 'empresa_produto_unidade_medida', null, '0', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('60', 'empresa_produto_venda', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('61', 'empresa_servico', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('62', 'empresa_servico_compra', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('63', 'empresa_servico_foto', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('64', 'empresa_servico_unidade_medida', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('65', 'empresa_servico_venda', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('66', 'empresa_tipo_venda', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('67', 'empresa_tipo_venda_parcela', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('68', 'empresa_venda', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('69', 'empresa_venda_parcela', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('71', 'empresa_produto_tipo', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('72', 'empresa_servico_tipo', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('73', 'usuario_tipo', null, '-1', '0', '1', '1', '0', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('75', 'usuario_tipo_corporacao', null, '-1', '1', '1', '1', '1', null, null, 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('77', 'operadora', null, '-1', '1', '1', '1', '1', null, null, 'nome_normalizado', null);
INSERT INTO `sistema_tabela` VALUES ('81', 'app', null, '-1', '1', '1', '1', '1', null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('82', 'tipo_ponto', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('83', 'rede', null, '-1', '1', '1', '1', '1', null, null, 'nome, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('84', 'rede_empresa', null, '-1', '1', '1', '1', '1', null, null, 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('85', 'pessoa_empresa_rotina', null, '-1', '1', '1', '1', '1', null, null, 'pessoa_id_INT, empresa_id_INT, profissao_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('86', 'rotina', null, '-1', '1', '1', '1', '1', null, null, 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('87', 'pessoa_usuario', null, '-1', '1', '1', '1', '1', null, null, 'pessoa_id_INT, usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('89', 'relatorio', null, '-1', '1', '1', '1', '1', null, null, 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('90', 'tipo_anexo', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('91', 'relatorio_anexo', null, '-1', '1', '1', '1', '1', null, null, 'rede_id_INT, empresa_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('126', 'estado_civil', null, '1', '1', '1', '1', '0', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('139', 'sistema_tabela', null, '-1', '1', '1', '1', '0', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('143', 'forma_pagamento', null, '-1', '1', '1', '1', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('168', 'usuario_foto_configuracao', null, '-1', '1', '1', '1', '1', null, null, 'usuario_id_INT, corporacao_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('171', 'sistema_tipo_operacao_banco', null, '1', '1', '1', '1', '0', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('238', 'sistema_banco_versao', null, '-1', '1', '1', '0', '0', null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('317', 'sistema_tipo_mobile', null, '1', '1', '1', '1', '0', null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('337', 'sistema_usuario_mensagem', null, '-1', '1', '1', '0', '1', null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('343', 'sistema_tipo_download_arquivo', null, '0', '0', '1', '0', '0', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('344', 'sistema_tipo_usuario_mensagem', null, '1', '1', '1', '1', '0', null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('441', 'horario_trabalho_pessoa_empresa', null, '-1', '1', '1', '0', '1', null, null, 'nome', null);
INSERT INTO `sistema_tabela` VALUES ('442', 'tarefa_acao', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('443', 'projetos_versao', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('444', 'android_metadata', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('445', 'sistema_atributo', null, null, null, null, null, null, null, null, 'nome, sistema_tabela_id_INT', null);
INSERT INTO `sistema_tabela` VALUES ('446', 'veiculo_registro', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('447', 'sistema_deletar_arquivo', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('448', 'sistema_parametro_global', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('449', 'sistema_produto_log_erro', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('450', 'corporacao_sincronizador_php', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('451', 'usuario_posicao_rastreamento', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('452', 'sistema_sincronizador_arquivo', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('453', 'sistema_grafo_hierarquia_banco', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('454', 'sistema_historico_sincronizador', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('455', 'sistema_operacao_crud_aleatorio', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('456', 'sistema_operacao_executa_script', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('457', 'sistema_operacao_sistema_mobile', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('458', 'sistema_corporacao_sincronizador', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('459', 'sistema_aresta_grafo_hierarquia_banco', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('460', 'sistema_operacao_download_banco_mobile', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('461', 'sistema_corporacao_sincronizador_tabela', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('462', 'sistema_registro_sincronizador_android_para_web', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('463', 'sistema_registro_sincronizador_web_para_android', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `sistema_tabela` VALUES ('464', 'wifi', null, '-1', '1', '1', '1', '1', null, '0', null, null);
INSERT INTO `sistema_tabela` VALUES ('465', 'wifi_registro', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `sistema_tipo_download_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_download_arquivo`;
CREATE TABLE `sistema_tipo_download_arquivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sistema_tipo_download_arquivo_UNIQUE` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_download_arquivo
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_tipo_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_mobile`;
CREATE TABLE `sistema_tipo_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_mobile
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_tipo_operacao_banco`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_operacao_banco`;
CREATE TABLE `sistema_tipo_operacao_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_84756` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_operacao_banco
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_tipo_usuario_mensagem`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_tipo_usuario_mensagem`;
CREATE TABLE `sistema_tipo_usuario_mensagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_tipo_usuario_mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for `sistema_usuario_mensagem`
-- ----------------------------
DROP TABLE IF EXISTS `sistema_usuario_mensagem`;
CREATE TABLE `sistema_usuario_mensagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `mensagem` tinytext,
  `sistema_tipo_usuario_mensagem_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_d18df5d66c46e28e` (`sistema_tipo_usuario_mensagem_id_INT`),
  KEY `key_f65825b393b9371f` (`usuario_id_INT`),
  KEY `key_3285575d473e6dcf` (`corporacao_id_INT`),
  CONSTRAINT `sistema_usuario_mensagem_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_usuario_mensagem_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sistema_usuario_mensagem_ibfk_3` FOREIGN KEY (`sistema_tipo_usuario_mensagem_id_INT`) REFERENCES `sistema_tipo_usuario_mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sistema_usuario_mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_FK_198974609` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tag
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa`;
CREATE TABLE `tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `origem_pessoa_id_INT` int(11) DEFAULT NULL,
  `origem_empresa_id_INT` int(11) DEFAULT NULL,
  `origem_logradouro` varchar(255) DEFAULT NULL,
  `origem_numero` varchar(30) DEFAULT NULL,
  `origem_cidade_id_INT` int(11) DEFAULT NULL,
  `origem_latitude_INT` int(6) DEFAULT NULL,
  `origem_longitude_INT` int(6) DEFAULT NULL,
  `origem_latitude_real_INT` int(6) DEFAULT NULL,
  `origem_longitude_real_INT` int(6) DEFAULT NULL,
  `destino_pessoa_id_INT` int(11) DEFAULT NULL,
  `destino_empresa_id_INT` int(11) DEFAULT NULL,
  `destino_logradouro` varchar(255) DEFAULT NULL,
  `destino_numero` varchar(30) DEFAULT NULL,
  `destino_cidade_id_INT` int(11) DEFAULT NULL,
  `destino_latitude_INT` int(6) DEFAULT NULL,
  `destino_longitude_INT` int(6) DEFAULT NULL,
  `destino_latitude_real_INT` int(6) DEFAULT NULL,
  `destino_longitude_real_INT` int(6) DEFAULT NULL,
  `tempo_estimado_carro_INT` int(11) DEFAULT NULL,
  `tempo_estimado_a_pe_INT` int(11) DEFAULT NULL,
  `distancia_estimada_carro_INT` int(11) DEFAULT NULL,
  `distancia_estimada_a_pe_INT` int(11) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `titulo_normalizado` varchar(100) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `inicio_hora_programada_SEC` int(10) DEFAULT NULL,
  `inicio_hora_programada_OFFSEC` int(6) DEFAULT NULL,
  `data_exibir_SEC` int(10) DEFAULT NULL,
  `data_exibir_OFFSEC` int(6) DEFAULT NULL,
  `inicio_SEC` int(10) DEFAULT NULL,
  `inicio_OFFSEC` int(6) DEFAULT NULL,
  `fim_SEC` int(10) DEFAULT NULL,
  `fim_OFFSEC` int(6) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `empresa_atividade_id_INT` int(11) DEFAULT NULL,
  `tipo_tarefa_id_INT` int(4) DEFAULT NULL,
  `empresa_venda_id_INT` int(11) DEFAULT NULL,
  `id_prioridade_INT` int(4) DEFAULT NULL,
  `registro_estado_id_INT` int(11) DEFAULT NULL,
  `registro_estado_corporacao_id_INT` int(11) DEFAULT NULL,
  `percentual_completo_INT` int(2) DEFAULT NULL,
  `prazo_SEC` int(10) DEFAULT NULL,
  `prazo_OFFSEC` int(6) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `protocolo_empresa_venda_INT` bigint(20) DEFAULT NULL,
  `protocolo_empresa_atividade_venda_INT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_FK_490081787` (`origem_cidade_id_INT`),
  KEY `tarefa_FK_618682861` (`destino_cidade_id_INT`),
  KEY `tarefa_FK_90545654` (`corporacao_id_INT`),
  KEY `tabela_FK_100` (`usuario_id_INT`),
  KEY `tarefa_FK_529296875` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_FK_70098877` (`categoria_permissao_id_INT`),
  KEY `tarefa_FK_751892090` (`veiculo_id_INT`),
  KEY `tarefa_FK_718139649` (`origem_pessoa_id_INT`),
  KEY `tarefa_FK_466461182` (`origem_empresa_id_INT`),
  KEY `tarefa_FK_517883301` (`destino_pessoa_id_INT`),
  KEY `tarefa_FK_919769288` (`destino_empresa_id_INT`),
  KEY `tarefa_FK_603546143` (`empresa_equipe_id_INT`) USING BTREE,
  KEY `tarefa_FK_751190186` (`tipo_tarefa_id_INT`) USING BTREE,
  KEY `tarefa_FK_972564698` (`registro_estado_id_INT`) USING BTREE,
  KEY `tarefa_FK_558166504` (`registro_estado_corporacao_id_INT`) USING BTREE,
  KEY `tarefa_FK_189971924` (`empresa_atividade_id_INT`) USING BTREE,
  KEY `tarefa_FK_443054199` (`empresa_venda_id_INT`) USING BTREE,
  KEY `tarefa_FK_575897217` (`atividade_id_INT`) USING BTREE,
  CONSTRAINT `tarefa_FK_189971924` FOREIGN KEY (`empresa_atividade_id_INT`) REFERENCES `empresa_atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_FK_443054199` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_FK_558166504` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_FK_575897217` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_FK_603546143` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_FK_751190186` FOREIGN KEY (`tipo_tarefa_id_INT`) REFERENCES `tipo_tarefa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_FK_972564698` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_10` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_11` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_12` FOREIGN KEY (`destino_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_13` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_14` FOREIGN KEY (`origem_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_15` FOREIGN KEY (`origem_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_3` FOREIGN KEY (`destino_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_4` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_5` FOREIGN KEY (`destino_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_7` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_ibfk_8` FOREIGN KEY (`origem_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_cotidiano`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_cotidiano`;
CREATE TABLE `tarefa_cotidiano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `empresa_equipe_id_INT` int(11) DEFAULT NULL,
  `origem_pessoa_id_INT` int(11) DEFAULT NULL,
  `origem_empresa_id_INT` int(11) DEFAULT NULL,
  `origem_logradouro` varchar(255) DEFAULT NULL,
  `origem_numero` varchar(30) DEFAULT NULL,
  `origem_cidade_id_INT` int(11) DEFAULT NULL,
  `origem_latitude_INT` int(6) DEFAULT NULL,
  `origem_longitude_INT` int(6) DEFAULT NULL,
  `origem_latitude_real_INT` int(6) DEFAULT NULL,
  `origem_longitude_real_INT` int(6) DEFAULT NULL,
  `destino_pessoa_id_INT` int(11) DEFAULT NULL,
  `destino_empresa_id_INT` int(11) DEFAULT NULL,
  `destino_logradouro` varchar(255) DEFAULT NULL,
  `destino_numero` varchar(30) DEFAULT NULL,
  `destino_cidade_id_INT` int(11) DEFAULT NULL,
  `destino_latitude_INT` int(6) DEFAULT NULL,
  `destino_longitude_INT` int(6) DEFAULT NULL,
  `destino_latitude_real_INT` int(6) DEFAULT NULL,
  `destino_longitude_real_INT` int(6) DEFAULT NULL,
  `tempo_estimado_carro_INT` int(11) DEFAULT NULL,
  `tempo_estimado_a_pe_INT` int(11) DEFAULT NULL,
  `distancia_estimada_carro_INT` int(11) DEFAULT NULL,
  `distancia_estimada_a_pe_INT` int(11) DEFAULT NULL,
  `is_realizada_a_pe_BOOLEAN` int(1) DEFAULT NULL,
  `inicio_hora_programada_SEC` int(10) DEFAULT NULL,
  `inicio_hora_programada_OFFSEC` int(6) DEFAULT NULL,
  `inicio_SEC` int(10) DEFAULT NULL,
  `inicio_OFFSEC` int(6) DEFAULT NULL,
  `fim_SEC` int(10) DEFAULT NULL,
  `fim_OFFSEC` int(6) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `titulo_normalizado` varchar(100) DEFAULT NULL,
  `descricao` varchar(512) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `atividade_id_INT` int(11) DEFAULT NULL,
  `id_estado_INT` int(4) DEFAULT NULL,
  `id_prioridade_INT` int(4) DEFAULT NULL,
  `parametro_SEC` int(10) DEFAULT NULL,
  `parametro_OFFSEC` int(6) DEFAULT NULL,
  `parametro_json` varchar(512) DEFAULT NULL,
  `data_limite_cotidiano_SEC` int(10) DEFAULT NULL,
  `data_limite_cotidiano_OFFSEC` int(6) DEFAULT NULL,
  `id_tipo_cotidiano_INT` int(4) DEFAULT NULL,
  `prazo_SEC` int(10) DEFAULT NULL,
  `prazo_OFFSEC` int(6) DEFAULT NULL,
  `percentual_completo_INT` int(2) DEFAULT NULL,
  `ultima_geracao_SEC` int(10) DEFAULT NULL,
  `ultima_geracao_OFFSEC` int(6) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_FK_490081787` (`origem_cidade_id_INT`),
  KEY `tarefa_FK_618682861` (`destino_cidade_id_INT`),
  KEY `tarefa_FK_90545654` (`corporacao_id_INT`),
  KEY `tabela_FK_100` (`usuario_id_INT`),
  KEY `tarefa_FK_529296875` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_FK_70098877` (`categoria_permissao_id_INT`),
  KEY `tarefa_FK_751892090` (`veiculo_id_INT`),
  KEY `tarefa_FK_718139649` (`origem_pessoa_id_INT`),
  KEY `tarefa_FK_466461182` (`origem_empresa_id_INT`),
  KEY `tarefa_FK_517883301` (`destino_pessoa_id_INT`),
  KEY `tarefa_FK_919769288` (`destino_empresa_id_INT`),
  KEY `tarefa_cotidiano_FK_718597412` (`empresa_equipe_id_INT`),
  KEY `tarefa_cotidiano_FK_316833496` (`atividade_id_INT`),
  KEY `tarefa_cotidiano_FK_442840576` (`relatorio_id_INT`),
  CONSTRAINT `tarefa_cotidiano_FK_316833496` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_cotidiano_FK_442840576` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_cotidiano_FK_718597412` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_1` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_10` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_11` FOREIGN KEY (`origem_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_3` FOREIGN KEY (`destino_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_4` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_5` FOREIGN KEY (`origem_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_6` FOREIGN KEY (`origem_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_7` FOREIGN KEY (`destino_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_8` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_cotidiano_ibfk_9` FOREIGN KEY (`destino_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_cotidiano
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_cotidiano_item`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_cotidiano_item`;
CREATE TABLE `tarefa_cotidiano_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `tarefa_cotidiano_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_cotidiano_item_FK_381195068` (`corporacao_id_INT`),
  KEY `tarefa_cotidiano_item_FK_630126953` (`tarefa_cotidiano_id_INT`),
  CONSTRAINT `tarefa_cotidiano_item_FK_381195068` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tarefa_cotidiano_item_FK_630126953` FOREIGN KEY (`tarefa_cotidiano_id_INT`) REFERENCES `tarefa_cotidiano` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_cotidiano_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_item`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_item`;
CREATE TABLE `tarefa_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `data_conclusao_SEC` int(10) DEFAULT NULL,
  `data_conclusao_OFFSEC` int(6) DEFAULT NULL,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  `criado_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `executada_pelo_usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `seq_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `latitude_real_INT` int(6) DEFAULT NULL,
  `longitude_real_INT` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_item_FK_979400635` (`tarefa_id_INT`),
  KEY `tarefa_item_FK_803283692` (`criado_pelo_usuario_id_INT`),
  KEY `tarefa_item_FK_903411866` (`executada_pelo_usuario_id_INT`),
  KEY `tarefa_item_FK_176757812` (`corporacao_id_INT`),
  CONSTRAINT `tarefa_item_FK_176757812` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_item_FK_803283692` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_item_FK_903411866` FOREIGN KEY (`executada_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tarefa_item_FK_979400635` FOREIGN KEY (`tarefa_id_INT`) REFERENCES `tarefa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_relatorio`;
CREATE TABLE `tarefa_relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `relatorio_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_relatorio_FK_286987304` (`tarefa_id_INT`),
  KEY `tarefa_relatorio_FK_220855713` (`relatorio_id_INT`),
  KEY `tarefa_relatorio_FK_496887207` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `tarefa_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa_tag`;
CREATE TABLE `tarefa_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tarefa_id_INT` int(11) DEFAULT NULL,
  `tag_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarefa_tag_FK_665161133` (`tarefa_id_INT`),
  KEY `tarefa_tag_FK_816802979` (`tag_id_INT`),
  KEY `tarefa_tag_FK_330139160` (`corporacao_id_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tarefa_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_anexo`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_anexo`;
CREATE TABLE `tipo_anexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_anexo
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_canal_envio`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_canal_envio`;
CREATE TABLE `tipo_canal_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_canal_envio
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_documento`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_documento_FK_958312989` (`corporacao_id_INT`),
  CONSTRAINT `tipo_documento_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_documento
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_empresa`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_empresa`;
CREATE TABLE `tipo_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_93486ui` (`nome`,`corporacao_id_INT`),
  KEY `tipo_empresa_FK_221710205` (`corporacao_id_INT`),
  CONSTRAINT `tipo_empresa_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_nota`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_nota`;
CREATE TABLE `tipo_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_nota_FK_962524415` (`corporacao_id_INT`),
  CONSTRAINT `tipo_nota_FK_962524415` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_nota
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_ponto`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_ponto`;
CREATE TABLE `tipo_ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_ponto
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_registro`;
CREATE TABLE `tipo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_relatorio`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_relatorio`;
CREATE TABLE `tipo_relatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_relatorio
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_tarefa`;
CREATE TABLE `tipo_tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_tarefa
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_veiculo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_veiculo_registro`;
CREATE TABLE `tipo_veiculo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_veiculo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `uf`
-- ----------------------------
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `nome_normalizado` varchar(255) DEFAULT NULL,
  `sigla` char(2) DEFAULT NULL,
  `pais_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome2` (`nome_normalizado`,`corporacao_id_INT`,`pais_id_INT`),
  KEY `uf_FK_724060059` (`corporacao_id_INT`),
  KEY `pais_id_INT` (`pais_id_INT`),
  CONSTRAINT `uf_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `uf_ibfk_2` FOREIGN KEY (`pais_id_INT`) REFERENCES `pais` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uf
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `status_BOOLEAN` int(1) DEFAULT NULL,
  `pagina_inicial` text,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_categoria_permissao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_categoria_permissao`;
CREATE TABLE `usuario_categoria_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `categoria_permissao_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_id_INT` (`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_categoria_permissao_FK_734741211` (`categoria_permissao_id_INT`),
  KEY `usuario_categoria_permissao_FK_776031494` (`corporacao_id_INT`),
  CONSTRAINT `usuario_categoria_permissao_ibfk_3` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_categoria_permissao_ibfk_8` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_categoria_permissao_ibfk_9` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_categoria_permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_corporacao`;
CREATE TABLE `usuario_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `is_adm_BOOLEAN` int(1) NOT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_corporacao_UNIQUE` (`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_corporacao_FK_972991944` (`usuario_id_INT`),
  KEY `usuario_corporacao_FK_135986328` (`corporacao_id_INT`),
  CONSTRAINT `usuario_corporacao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_corporacao_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_foto`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_foto`;
CREATE TABLE `usuario_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_interna` varchar(50) DEFAULT NULL,
  `foto_externa` varchar(50) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_foto_FK_640136719` (`usuario_id_INT`),
  KEY `usuario_foto_FK_401641846` (`corporacao_id_INT`),
  CONSTRAINT `usuario_foto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_foto_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_foto
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_foto_configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_foto_configuracao`;
CREATE TABLE `usuario_foto_configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_camera_interna_BOOLEAN` int(1) NOT NULL,
  `periodo_segundo_INT` int(7) NOT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_foto_configuracao_FK_765228272` (`usuario_id_INT`),
  KEY `usuario_foto_configuracao_FK_640441895` (`corporacao_id_INT`),
  CONSTRAINT `usuario_foto_configuracao_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_foto_configuracao_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_foto_configuracao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_horario_trabalho`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_horario_trabalho`;
CREATE TABLE `usuario_horario_trabalho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio_TIME` time DEFAULT NULL,
  `hora_fim_TIME` time DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `dia_semana_INT` int(2) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `template_JSON` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_hora478455` (`corporacao_id_INT`),
  KEY `usuario_horario_trabalho_FK_788391114` (`empresa_id_INT`),
  KEY `usuario_horario_trabalho_FK_905120850` (`usuario_id_INT`),
  CONSTRAINT `usuario_horario_trabalho_FK_788391114` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_horario_trabalho_FK_905120850` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_horario_trabalho_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_horario_trabalho
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_posicao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_posicao`;
CREATE TABLE `usuario_posicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(11) NOT NULL,
  `longitude_INT` int(11) NOT NULL,
  `velocidade_INT` int(5) DEFAULT NULL,
  `foto_interna` varchar(30) DEFAULT NULL,
  `foto_externa` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `fonte_informacao_INT` int(2) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_posicao_FK_244567871` (`corporacao_id_INT`),
  KEY `usuario_posicao_FK_205352783` (`usuario_id_INT`),
  KEY `usuario_posicao_FK_64941406` (`veiculo_usuario_id_INT`),
  CONSTRAINT `usuario_posicao_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_posicao_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_posicao_ibfk_3` FOREIGN KEY (`veiculo_usuario_id_INT`) REFERENCES `veiculo_usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_posicao
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_posicao_rastreamento`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_posicao_rastreamento`;
CREATE TABLE `usuario_posicao_rastreamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_DATE` varchar(10) DEFAULT NULL,
  `hora_TIME` varchar(10) DEFAULT NULL,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(11) NOT NULL,
  `longitude_INT` int(11) NOT NULL,
  `velocidade_INT` int(5) DEFAULT NULL,
  `foto_interna` varchar(30) DEFAULT NULL,
  `foto_externa` varchar(30) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_b55d575ed2dd00e0` (`veiculo_usuario_id_INT`),
  KEY `key_b3a9a8520118b71d` (`usuario_id_INT`),
  KEY `key_f783193890e9e5ae` (`corporacao_id_INT`),
  CONSTRAINT `usuario_posicao_rastreamento_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_posicao_rastreamento_ibfk_2` FOREIGN KEY (`veiculo_usuario_id_INT`) REFERENCES `veiculo_usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_posicao_rastreamento_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_posicao_rastreamento
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_servico`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_servico`;
CREATE TABLE `usuario_servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_BOOLEAN` int(1) NOT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `servico_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servico_id_INT` (`servico_id_INT`,`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_servico_FK_158416748` (`usuario_id_INT`),
  KEY `usuario_servico_FK_909790039` (`corporacao_id_INT`),
  CONSTRAINT `usuario_servico_FK_777465821` FOREIGN KEY (`servico_id_INT`) REFERENCES `servico` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_servico_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_servico_ibfk_9` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_servico
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo`;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_visivel` varchar(100) DEFAULT NULL,
  `status_BOOLEAN` int(1) DEFAULT NULL,
  `pagina_inicial` varchar(255) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_tipo_UNIQUE` (`nome`,`corporacao_id_INT`),
  KEY `usuario_tipo_FK_915130616` (`corporacao_id_INT`),
  CONSTRAINT `usuario_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo_corporacao`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_corporacao`;
CREATE TABLE `usuario_tipo_corporacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `usuario_tipo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_tipo_corporacao_UNIQUE` (`usuario_id_INT`,`corporacao_id_INT`),
  KEY `usuario_tipo_corporacao_FK_644226074` (`usuario_id_INT`),
  KEY `usuario_tipo_corporacao_FK_503631592` (`usuario_tipo_id_INT`),
  KEY `usuario_tipo_corporacao_FK_788208008` (`corporacao_id_INT`),
  CONSTRAINT `usuario_tipo_corporacao_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_corporacao_ibfk_2` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `usuario_tipo_corporacao_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_corporacao
-- ----------------------------

-- ----------------------------
-- Table structure for `veiculo`
-- ----------------------------
DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE `veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placa` varchar(20) NOT NULL,
  `modelo_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `placa` (`placa`,`corporacao_id_INT`),
  KEY `veiculo_FK_363189697` (`corporacao_id_INT`),
  KEY `veiculo_FK_481506348` (`modelo_id_INT`),
  CONSTRAINT `veiculo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_ibfk_2` FOREIGN KEY (`modelo_id_INT`) REFERENCES `modelo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of veiculo
-- ----------------------------

-- ----------------------------
-- Table structure for `veiculo_registro`
-- ----------------------------
DROP TABLE IF EXISTS `veiculo_registro`;
CREATE TABLE `veiculo_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entrada_BOOLEAN` int(1) DEFAULT NULL,
  `veiculo_usuario_id_INT` int(11) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `quilometragem_INT` int(11) DEFAULT NULL,
  `observacao` varchar(512) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  `tipo_veiculo_registro_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `veiculo_usuario_id_INT` (`veiculo_usuario_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  KEY `veiculo_registro_FK_612823486` (`tipo_veiculo_registro_id_INT`) USING BTREE,
  CONSTRAINT `veiculo_registro_FK_612823486` FOREIGN KEY (`tipo_veiculo_registro_id_INT`) REFERENCES `tipo_veiculo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_registro_ibfk_1` FOREIGN KEY (`veiculo_usuario_id_INT`) REFERENCES `veiculo_usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_registro_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of veiculo_registro
-- ----------------------------

-- ----------------------------
-- Table structure for `veiculo_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `veiculo_usuario`;
CREATE TABLE `veiculo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `veiculo_id_INT` int(11) DEFAULT NULL,
  `is_ativo_BOOLEAN` int(1) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `cadastro_SEC` int(10) DEFAULT NULL,
  `cadastro_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_id_INT` (`usuario_id_INT`,`veiculo_id_INT`,`corporacao_id_INT`),
  KEY `veiculo_usuario_FK_266174316` (`veiculo_id_INT`),
  KEY `veiculo_usuario_FK_358489990` (`corporacao_id_INT`),
  CONSTRAINT `veiculo_usuario_ibfk_4` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_usuario_ibfk_5` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `veiculo_usuario_ibfk_6` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of veiculo_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `wifi`
-- ----------------------------
DROP TABLE IF EXISTS `wifi`;
CREATE TABLE `wifi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) DEFAULT NULL,
  `rssi` int(11) DEFAULT NULL,
  `ssid` varchar(30) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `senha` varchar(30) DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  `link_speed` int(11) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `bssid` varchar(30) DEFAULT NULL,
  `empresa_id_INT` int(11) DEFAULT NULL,
  `pessoa_id_INT` int(11) DEFAULT NULL,
  `contador_verificacao_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_SEC` int(10) DEFAULT NULL,
  `data_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_id_INT` (`empresa_id_INT`),
  KEY `pessoa_id_INT` (`pessoa_id_INT`),
  KEY `corporacao_id_INT` (`corporacao_id_INT`),
  CONSTRAINT `wifi_ibfk_1` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_ibfk_2` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wifi
-- ----------------------------

-- ----------------------------
-- Table structure for `wifi_registro`
-- ----------------------------
DROP TABLE IF EXISTS `wifi_registro`;
CREATE TABLE `wifi_registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wifi_id_INT` int(11) DEFAULT NULL,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `corporacao_id_INT` int(11) DEFAULT NULL,
  `data_inicio_SEC` int(10) DEFAULT NULL,
  `data_inicio_OFFSEC` int(6) DEFAULT NULL,
  `data_fim_SEC` int(10) DEFAULT NULL,
  `data_fim_OFFSEC` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wifi_registro_FK_898468018` (`wifi_id_INT`),
  KEY `wifi_registro_ibfk_1` (`usuario_id_INT`),
  KEY `wifi_registro_ibfk_2` (`corporacao_id_INT`),
  CONSTRAINT `wifi_registro_FK_898468018` FOREIGN KEY (`wifi_id_INT`) REFERENCES `wifi` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_registro_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `wifi_registro_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wifi_registro
-- ----------------------------
